//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <boost/test/minimal.hpp>
#include <iostream>
#include <typeinfo>
#include <type_traits>
#include <utility>
#include <string>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <boost/mpl/vector.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/at.hpp>


struct has_name {};
struct has_value {};

template<class Tag> struct name_only {
    using tag_type = Tag;
    static constexpr const char* name_string = Tag().name;
};

template<class Tag, int Value> struct name_value
{
    using tag_type = Tag;
    static constexpr const char* name_string = Tag().name;
    static constexpr int value = Value;
};

template< int CurrentValue, class Tag >
static constexpr int value_of_name(name_only<Tag>) {
    return CurrentValue;
};

template< int CurrentValue, class Tag, int TagValue>
static constexpr int value_of_name(name_value<Tag, TagValue>) {
    return TagValue;
}

/// Assigns values to Tags when given a list that includes name<Tag> and name_value<Tag> classes

template<int initial_value, class...NameTags>
struct value_assigner;

template<int CurrentValue, class NameTag>
struct value_assigner<CurrentValue, NameTag>
{
    template<class U, class = void>
    struct apply;

    template< class U >
    struct apply< U, std::enable_if_t< std::is_same<U, NameTag>::value > >
    {
        static constexpr int value = value_of_name<CurrentValue>(NameTag());
    };
    
    template<class U>
    static constexpr int value_of() { return apply<U>::value; }
};

template<int CurrentValue, class NameTag1, class...Rest>
struct value_assigner<CurrentValue, NameTag1, Rest...>
{
    template<class U, class = void>
    struct apply;
    
    template< class U >
    struct apply< U, std::enable_if_t< std::is_same<U, NameTag1>::value > >
    {
        static constexpr int value = value_of_name<CurrentValue>(NameTag1());
    };

    template< class U >
    struct apply< U, std::enable_if_t< !std::is_same<U, NameTag1>::value > >
    {
        using assign_rest = value_assigner<apply<NameTag1>::value + 1, Rest...>;
        static constexpr int value = assign_rest::template apply<U>::value;
    };

    template<class U>
    static constexpr int value_of() { return apply<U>::value; }
};

template<int InitialValue, class...NameTags>
struct enumeration_impl
{
    using integral_type = int;
    using tag_vector = boost::mpl::vector<typename NameTags::tag_type...>;
    using nametag_vector = boost::mpl::vector<NameTags...>;

    using my_value_assigner = value_assigner<InitialValue, NameTags...>;
    
    template<class Tag>
    static constexpr int value_of() {
        using ifind = typename boost::mpl::find<tag_vector, Tag>::type;
        using nametag = typename boost::mpl::at_c<nametag_vector, ifind::pos::value>::type;
        return my_value_assigner::template value_of<nametag>();
    }
    template<class Tag>
    static constexpr int value_of(Tag) {
        return value_of<Tag>();
    }

    
    static constexpr size_t size = sizeof...(NameTags);
    struct value_concept {
        constexpr value_concept(int value, const char* name)
        : value(value), name(name) {}
        const int value;
        const char* const name;
    };
    using value_concepts_array = value_concept[size+1];
    static const value_concepts_array& value_concepts() {
        static const value_concepts_array _ {
            value_concept(value_of<typename NameTags::tag_type>(), NameTags::name_string)...
            , value_concept(std::numeric_limits<integral_type>::max(), "End of List")
        };
        return _;
    }
    
    // provide a means of getting a value_type index from an integral_value (casting from int to enum)
    using map_integral_to_index = std::unordered_multimap<integral_type, size_t>;
    static map_integral_to_index build_integral_to_index_map() {
        map_integral_to_index result;
        const auto& available_values = value_concepts();
        for (size_t i = 0 ; i < size ; ++i) {
            result.emplace(available_values[i].value, i);
        }
        return result;
    };
    static const map_integral_to_index& integral_to_index_map() {
        static const map_integral_to_index _ = build_integral_to_index_map();
        return _;
    }

    // provide a means of getting a value_type index from an integral_value (casting from int to enum)
    using map_name_to_index = std::unordered_multimap<std::string, size_t>;
    static map_name_to_index build_name_to_index_map() {
        map_name_to_index result;
        const auto& available_values = value_concepts();
        for (size_t i = 0 ; i < size ; ++i) {
            result.emplace(available_values[i].name, i);
        }
        return result;
    };
    static const map_name_to_index& name_to_index_map() {
        static const map_name_to_index _ = build_name_to_index_map();
        return _;
    }
    
    template<class Tag>
    static constexpr const char* name_of() { return Tag().name; }
    template<class Tag>
    static constexpr const char* name_of(Tag t) { return t.name; }

    
    struct value_type {
        
        const char* name() const { return value_concepts()[_index].name; }
        int value() const { return value_concepts()[_index].value; }
        value_type& operator++() {
            ++_index;
            return *this;
        }
        const value_type& operator*() const {
            return *this;
        }
        
        size_t index() const {
            return _index;
        }

        template<class Switcher>
        struct switch_caller_concept {
            using result_type = typename Switcher::result_type;
            virtual result_type call(Switcher&& switcher) const = 0;
        };
        template<class Switcher, class Tag>
        struct switch_caller_model : switch_caller_concept<Switcher> {
            using result_type = typename switch_caller_concept<Switcher>::result_type;
            result_type call(Switcher&& switcher) const override {
                return switcher(Tag());
            }
            static const switch_caller_concept<Switcher>& instance() {
                static const switch_caller_model me = switch_caller_model();
                return me;
            }
        };
        
        template<class Switcher>
        auto switch_case(Switcher switcher) const -> typename Switcher::result_type
        {
            static const switch_caller_concept<Switcher>* switchers[] = {
                &switch_caller_model<Switcher, typename NameTags::tag_type>::instance()...
            };
            return switchers[_index]->call(std::forward<Switcher>(switcher));
        }
    private:
        friend enumeration_impl;
        explicit constexpr value_type(size_t index) : _index(index) {}
        size_t _index;
    };
    friend const char* name_of(const value_type& vt) { return vt.name(); }
    friend int value_of(const value_type& vt) { return vt.value(); }
    friend std::ostream& operator<<(std::ostream& os, const value_type& vt) {
        os << vt.name() << '(' << vt.value() << ')';
        return os;
    }
    friend bool operator==(const value_type& v1, const value_type& v2) {
        return v1._index == v2._index;
    }
    friend bool operator!=(const value_type& v1, const value_type& v2) {
        return v1._index != v2._index;
    }
    friend std::string to_string(const value_type& vt) {
        using namespace std;
        return string(vt.name()) + '(' + std::to_string(vt.value()) + ')';
    }
    
    template<class Tag>
    static constexpr value_type at() {
        using ifind = typename boost::mpl::find<tag_vector, Tag>::type;
        return value_type(ifind::pos::value);
    };
    
    static constexpr value_type begin() {
        return value_type(0);
    }
    
    static constexpr value_type end() {
        return value_type(size);
    }
    
    static value_type from_integral(integral_type i) {
        using namespace std;
        const auto& map = integral_to_index_map();
        auto ifind = map.find(i);
        if (ifind == map.end()) {
            throw std::invalid_argument { "no such conversion from integral value "s + to_string(i) };
        }
        return value_type(ifind->second);
    }
    
    static value_type from_name(const std::string name) {
        using namespace std;
        const auto& map = name_to_index_map();
        auto ifind = map.find(name);
        if (ifind == map.end()) {
            throw std::invalid_argument { "no such conversion from name "s + name };
        }
        return value_type(ifind->second);
    }
    
    static std::vector<value_type> all_of_integral(integral_type i) {
        using namespace std;
        const auto& map = integral_to_index_map();
        auto ipair = map.equal_range(i);
        vector<value_type> result;
        for (auto i = ipair.first ; i != ipair.second ; ++i) {
            result.push_back(value_type(i->second));
        }
        return result;
    }
    
    friend std::ostream& operator<<(std::ostream& os, const enumeration_impl& e)
    {
        os << "{";
        auto sep = " ";
        for(auto v : e) {
            os << sep << v;
            sep = ", ";
        }
        os << " }";
        return os;
    }
};

#define MakeName(Name) struct Name { const char* const name = #Name; }
#define MakeLocalName(Class, Name) struct Name { const char* const name = #Class "::" #Name; }

using namespace std;

//
// define some names
//
MakeName(Donkey);
MakeName(Gorilla);
MakeName(Moose);
MakeName(Flamingo);

template<class...NameTags>
struct enumeration_base : enumeration_impl<0, NameTags...>
{
    
};

template<class Definition>
struct enumeration_actor : Definition::enum_type, Definition
{
    using name_type = typename Definition::name_type;
    using impl_type = typename Definition::enum_type;
    
    
};

struct animals_enum_def
{
    MakeName(animals);
    using name_type = animals;
    
    MakeLocalName(animals, Donkey);
    MakeLocalName(animals, Gorilla);
    MakeLocalName(animals, Moose);
    MakeLocalName(animals, Flamingo);
    
    using enum_type = enumeration_base
    <
    name_only<Donkey>,           ///! use the last value plus 1
    name_value<Moose, 4>,   ///! use a specific value
    name_only<Gorilla>,          ///! use the next value continuing from Moose (not Donkey)
    name_value<Flamingo, 4>
    >;
};

using animals = enumeration_actor<animals_enum_def>;


int test_main( int, char *[] )             // note the name!
{
    //
    // test the basic building blocks
    //
    auto chk1 = value_of_name< 6 >(name_only<Donkey>()); BOOST_CHECK(chk1 == 6);
    auto chk2 = value_of_name< 6 >(name_value<Donkey, 10>()); BOOST_CHECK(chk2 == 10);
    
    using va1 = value_assigner< 6, name_only<Donkey> >;
    BOOST_CHECK(va1::apply<name_only<Donkey>>::value == 6);
    
    using va2 = value_assigner<3, name_value<Donkey, 7> >;
    static constexpr int va2_donkey_value = va2::apply<name_value<Donkey, 7>>::value;
    BOOST_CHECK(va2_donkey_value == 7);

    using va3 = value_assigner <0, name_only<Donkey>, name_value<Moose, 4>, name_only<Gorilla> >;
    static constexpr int va3_donkey_value = va3::apply<name_only<Donkey>>::value;
    static constexpr int va3_gorilla_value = va3::apply<name_only<Gorilla>>::value;
    static constexpr int va3_moose_value = va3::apply<name_value<Moose, 4>>::value;
    BOOST_CHECK(va3_donkey_value == 0);
    BOOST_CHECK(va3_moose_value == 4);
    BOOST_CHECK(va3_gorilla_value == 5);

    //
    // build an enumeration object
    //
    using e1 = enumeration_impl<
        0,                      ///! starting value
        name_only<Donkey>,           ///! use the last value plus 1
        name_value<Moose, 4>,   ///! use a specific value
        name_only<Gorilla>,          ///! use the next value continuing from Moose (not Donkey)
        name_value<Flamingo, 4>       ///! duplicates allowed if you really must
    >;

    auto chk10 = e1::name_of<Gorilla>(); BOOST_CHECK(chk10 == "Gorilla");
    auto chk11 = e1::name_of(Gorilla()); BOOST_CHECK(chk11 == "Gorilla");
    auto chk12 = e1::value_of<Gorilla>(); BOOST_CHECK(chk12 == 5);
    auto chk13 = e1::value_of(Gorilla()); BOOST_CHECK(chk13 == 5);
    
    auto v1 = e1::at<Gorilla>();
    BOOST_CHECK(v1.name() == "Gorilla" && name_of(v1) == "Gorilla");
    BOOST_CHECK(v1.value() == 5 && value_of(v1) == 5);
    BOOST_CHECK(to_string(v1) == "Gorilla(5)");

    BOOST_CHECK(to_string(e1::begin()) == "Donkey(0)");
    BOOST_CHECK(to_string(e1::end()) == "End of List(2147483647)");

    // enumerate all values
    cout << "all animals I know about:" << endl;
    for (auto v : e1())
    {
        cout << v << endl;
    }
    cout << endl;

    // create an enum value from an integral value
    v1 = e1::from_integral(0);
    cout << "value 0 is a " << v1 << endl;

    // create an enum value from an enum name
    v1 = e1::from_name("Moose");
    cout << "name Moose is a " << v1.value() << endl;

    // alternative to switch/case with the ability to provide the ... default case
    for (auto v : e1()) {
        struct sounder {
            using result_type = const char*;
            result_type operator()(...) const { return "no fucking idea what sound this thing makes!"; }
            result_type operator()(Donkey) const { return "Eee-Aww!"; }
            result_type operator()(Gorilla) const { return "Ooo! Ooo!"; }
        };
        cout << v << " - " << v.switch_case(sounder()) << endl;
    }
    cout << endl;

    // switch/case with parameters
    for (auto v : e1()) {
        struct sounder {
            sounder(string for_apes) : _for_apes(move(for_apes)) {}
            using result_type = string;
            result_type operator()(...) const { return "no fucking idea what sound this thing makes!"; }
            result_type operator()(Donkey) const { return "Eee-Aww!"; }
            result_type operator()(Gorilla) const { return "Ooo! Ooo! "s + _for_apes; }
            result_type operator()(Flamingo) const { return "Don't give me these, "s + _for_apes; }
        private:
            string _for_apes;
        };
        cout << v << " - " << v.switch_case(sounder("I like apes!")) << endl;
    }
    cout << endl;

    auto v2 = e1::at<Flamingo>();
    auto v3 = e1::from_integral(v2.value());
    cout << v2 << " and " << v3 << " have the same value" << endl << endl;
    
    // describe the whole enumeration
    cout << e1() << endl << endl;
    
    // get all of value 4
    auto value_4s = e1::all_of_integral(4);
    cout << "these have the value 4: ";
    copy(begin(value_4s), end(value_4s), ostream_iterator<e1::value_type>(cout, ", "));
    cout << endl;
    
    using e2 = enumeration_impl<
    1,                      ///! starting value (different to e1 so this is a different enumeration)
    name_only<Donkey>,
    name_value<Moose, 4>,
    name_only<Gorilla>,
    name_value<Flamingo, 4>
    >;
    
    // this will fail to compile, because the two enumerations are of different types;
    auto v4 = e1::begin();
//    v4 = e2::begin();

    // as will this:
    auto v5_1 = e1::at<Donkey>();
    auto v5_2 = e2::at<Donkey>();
//    v5_1 = v5_2;
    // because these are different types
    cout << v5_1 << " " << v5_2 << endl;
    
    
    //
    cout << "\n===============\n";
    
    auto a1 = animals::begin();
    cout << a1 << endl;
    
    return 0;
}
