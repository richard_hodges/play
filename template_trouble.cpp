/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <memory>
#include <string>
#include <iostream>

template<typename T, int nb = 1>
class MyClass
{
    /* ... some stuff here ... */
    
public:
    template<typename... Ts>
    MyClass(Ts&&... ts)
    : _pstr(std::make_shared<std::string>(std::forward<Ts>(ts)...))
    {
    }
    
    MyClass(MyClass<T, nb>& r)
    : MyClass(static_cast<const MyClass<T, nb>&>(r))
    {}
    
    MyClass(const MyClass<T, nb>&) = default;
    MyClass& operator=(const MyClass<T, nb>&) = default;
    MyClass(MyClass<T, nb>&&) = default;
    MyClass& operator=(MyClass<T, nb>&&) = default;

    void print() const {
        std::cout << *_pstr << std::endl;
    }
private:
    std::shared_ptr<std::string> _pstr;
    
};

int main()
{
    auto a = MyClass<int>("hello, world");
    auto b = a;
    auto c = MyClass<int>("goodbye");
    auto d = c;
    b = c;
    a.print();
    b.print();
    c.print();
    d.print();
}


