/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <valarray>
#include <iostream>

std::valarray<int> data = {
    
    1 , 2 , 3 , 4, 5, 6, 7, 8, 9,
    10, 11, 12, 13, 14, 15, 16, 17, 18,
    19, 20, 21, 22, 23, 24, 25, 26, 27,
    28, 29, 30, 31, 32, 33, 34, 35, 36,
    37, 38, 39, 40, 41, 42, 43, 44, 45,
    46, 47, 48, 49, 50, 51, 52, 53, 54,
    55, 56, 57, 58, 59, 60, 61, 62, 63,
    64, 65, 66, 67, 68, 69, 70, 71, 72,
    73, 74, 75, 76, 77, 78, 79, 80, 81
    
};


int main()
{
    auto col0 = std::slice(0, 1, 8);
    
    for (int i = 0  ; i < 9 ; ++i)
    {
        std::cout << data[col0[i]].sum() << std::endl;
    }
    
    
}
