//
//  main.cpp
//  skeleton
//
//  Created by Richard Hodges on 11/05/2016.
//  Copyright © 2016 Richard Hodges. All rights reserved.
//

#include <iostream>
#include <regex>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>

/*
 sample.exe --configuration i7_processor 5GB windows_32bit nividia
 sample.exe --configuration 5GB i7_processor nividia windows_32bit
 sample.exe --configuration nividia windows_32bit 5GB i7_processor
 */

template<class Iter>
std::pair<Iter, Iter> find_config(Iter first, Iter last)
{
    auto begin = std::find(first, last, std::string("--configuration"));
    if (begin == last)
    {
        return { last, last };
    }
    begin = std::next(begin);
    auto end = std::find_if(begin, last, [](auto& opt) {
        return opt.substr(2) == "--";
    });
    return { begin, end };
}

struct configuration
{
    std::string processor = "not set";
    unsigned long long memory = 0;
    std::string os = "not set";
    std::string graphics = "not set";
};

std::ostream& operator<<(std::ostream& os, const configuration& conf)
{
    os <<   "processor = " << conf.processor;
    os << "\nmemory    = " << conf.memory;
    os << "\nos        = " << conf.os;
    os << "\ngraphics  = " << conf.graphics;
    return os;
}

const std::regex re_processor("(.*)_processor", std::regex::icase);
const std::regex re_memory("(\\d+)(mb|gb)", std::regex::icase);

template<class Iter>
auto parse_config(Iter first, Iter last) -> configuration
{
    configuration result;
    
    for ( ; first != last ; ++first)
    {
        auto& option = *first;
        std::smatch match;
        if (std::regex_match(option, match, re_processor))
        {
            result.processor = match[1].str();
            continue;
        }
        
        if (std::regex_match(option, match, re_memory))
        {
            unsigned long long num = std::stoi(match[1].str());
            auto mult = match[2].str();
            std::transform(mult.begin(), mult.end(), mult.begin(),
                           [](auto& c) { return std::toupper(c); });
            if (mult == "GB") {
                num *= 1024 * 1024 * 1024;
            }
            else {
                num *= 1024 * 1024;
            }
            result.memory = num;
            continue;
        }
        
    }
    
    return result;
}

int main(int argc, const char* const * argv)
{
    std::vector<std::string> args(argv, argv + argc);
    const auto& progname = &args[0];
    
    auto config_range = find_config(std::next(std::begin(args)), std::end(args));
    auto configuration = parse_config(config_range.first, config_range.second);
    
    std::cout << configuration << std::endl;
    
}
