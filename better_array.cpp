/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
    flags="-O3"
	c++ ${flags} -S -std=c++1y $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <utility>
#include <cassert>
#include <string>
#include <vector>
#include <iomanip>

// a fully constexpr version of array that allows incomplete
// construction
template<size_t N, class T>
struct better_array
{
    // public constructor defers to internal one for
    // conditional handling of missing arguments
    constexpr better_array(std::initializer_list<T> list)
    : better_array(list, std::make_index_sequence<N>())
    {
        
    }
    
    constexpr T& operator[](size_t i) noexcept {
        assert(i < N);
        return _data[i];
    }
    
    constexpr const T& operator[](size_t i) const noexcept {
        assert(i < N);
        return _data[i];
    }
    
    constexpr T* begin() {
        return std::addressof(_data[0]);
    }
    
    constexpr const T* begin() const {
        return std::addressof(_data[0]);
    }
    
    constexpr T* end() {
        // todo: maybe use std::addressof and disable compiler warnings
        // about array bounds that result
        return &_data[N];
    }
    
    constexpr const T* end() const {
        return &_data[N];
    }
    
    constexpr size_t size() const {
        return N;
    }
    
private:
    
    T _data[N];
    
private:
    
    // construct each element from the initialiser list if present
    // if not, default-construct
    template<size_t...Is>
    constexpr better_array(std::initializer_list<T> list, std::integer_sequence<size_t, Is...>)
    : _data {
        (
         Is >= list.size()
         ?
         T()
         :
         std::move(*(std::next(list.begin(), Is)))
         )...
    }
    {
        
    }
};

// compute a simple factorial as a constexpr
constexpr long factorial(long x)
{
    if (x <= 0) return 0;
    
    long result = 1;
    for (long i = 2 ; i <= x ; result *= i)
        ++i;
    return result;
}

// compute an array of factorials - deliberately mutating a default-
// constructed array
template<size_t N>
constexpr better_array<N, long> factorials()
{
    better_array<N, long> result({});
    for (long i = 0 ; i < N ; ++i)
    {
        result[i] = factorial(i);
    }
    return result;
}

// convenience printer
template<size_t N, class T>
inline std::ostream& operator<<(std::ostream& os, const better_array<N, T>& a)
{
    os << "[";
    auto sep = " ";
    for (const auto& i : a) {
        os << sep << i;
        sep = ", ";
    }
    return os << " ]";
}

// for testing non-integrals
struct big_object
{
    std::string s = "defaulted";
    std::vector<std::string> v = { "defaulted1", "defaulted2" };
};

inline std::ostream& operator<<(std::ostream& os, const big_object& a)
{
    os << "{ s=" << quoted(a.s);
    os << ", v = [";
    auto sep = " ";
    for (const auto& s : a.v) {
        os << sep << quoted(s);
        sep = ", ";
    }
    return os << " ] }";
}

// test various uses of our new array
auto main() -> int
{
    using namespace std;

    // quick test
    better_array<3, int> x { 0, 3, 2 };
    cout << x << endl;
    
    // test that incomplete initialiser list results in a default-constructed object
    better_array<2, big_object> y { big_object { "a", { "b", "c" } } };
    cout << y << endl;
    
    // test constexpr construction using mutable array
    // question: how good is this optimiser anyway?
    cout << factorials<10>()[5] << endl;
    
    // answer: with apple clang7, -O3 the above line
    // compiles to:
    // 	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
    //  movl	$360, %esi              ## imm = 0x168
    //  callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEl
    // so that's pretty good!
    
    
    return 0;
}
    
    
