/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include <memory>
struct X
{
    struct impl {
        void foo() {
            ++x;
        }
        int x = 0;
    };

    X()
    : _impl(std::make_shared<impl>())
    {}

    void foo() {
        _impl->foo();
    }
    
    std::shared_ptr<impl> _impl;
};

using namespace std;

auto main() -> int
{
    X x;
    std::cout << "impl for x is " << x._impl << std::endl;
    X y = std::move(x);
    std::cout << "impl for x is " << x._impl << std::endl;
    std::cout << "impl for y is " << y._impl << std::endl;
    x.foo();
	cout << "Hello, World" << endl;
	return 0;
}

