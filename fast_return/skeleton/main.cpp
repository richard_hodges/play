#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>
#include <tuple>
#include <array>
#include <iterator>
#include <functional>
#include <typeinfo>
#include <boost/iterator/iterator_adaptor.hpp>


namespace detail {
    
    template<class Iter>
    struct unwrap_iterator
    : boost::iterator_adaptor
    <
    unwrap_iterator<Iter>,
    Iter,
    typename std::iterator_traits<Iter>::value_type::type
    >
    {
        using inherited = boost::iterator_adaptor
        <
        unwrap_iterator<Iter>,
        Iter,
        typename std::iterator_traits<Iter>::value_type::type
        >;
        
        using inherited::inherited;
        
        using base_type = typename std::iterator_traits<Iter>::value_type::type;
        
        base_type& operator*() const {
            return inherited::operator*();
        }
    };
}

template<class Iter>
auto make_unwrap_iterator(Iter iter)
{
    return detail::unwrap_iterator<Iter>(iter);
}

namespace detail {
    
    
    template<class CommonBase, std::size_t Size>
    struct common_observer_array
    {
        using array_type = std::array<std::reference_wrapper<CommonBase>, Size>;
        
        template<class...Refs>
        constexpr
        common_observer_array(Refs&...refs)
        : _array { refs... }
        {}
        
        auto begin() const {
            return make_unwrap_iterator(std::begin(_array));
        }
        
        auto begin() {
            return make_unwrap_iterator(std::begin(_array));
        }
        
        auto end() const {
            return make_unwrap_iterator(std::end(_array));
        }
        
        auto end() {
            return make_unwrap_iterator(std::end(_array));
        }
        
        array_type _array;
    };
}

template<class CommonBase, class Tuple, std::size_t...Is>
auto common_observers_impl(std::index_sequence<Is...>, Tuple&& t)
{
    using array_type = detail::common_observer_array<CommonBase, sizeof...(Is)>;
    return array_type {
        std::get<Is>(t)...
    };
}

template <typename T, template <typename...> class Tmpl>  // #1 see note
struct is_derived_from_template
{
    
    template <typename ...U>
    static std::true_type test(Tmpl<U...> const &);
    static std::false_type test(...);
    
    using type = bool;
    static type constexpr value = decltype(test(std::declval<T>()))::value;
};
template<typename T, template <typename...> class Tmpl>
static constexpr bool is_derived_from_template_v = is_derived_from_template<T, Tmpl>::value;


template<class Base> struct use_base {
    using type = Base;
};

template
<
class CommonBase,
class Tuple,
std::enable_if_t< is_derived_from_template_v<std::decay_t<Tuple>, std::tuple> >* = nullptr
>
auto common_observers(use_base<CommonBase>, Tuple&& t)
{
    using tuple_type = std::decay_t<Tuple>;
    constexpr auto size = std::tuple_size<tuple_type>();
    using array_type = std::array<std::reference_wrapper<CommonBase>, size>;
    return common_observers_impl<CommonBase>(std::make_index_sequence<size>(), t);
}

template
<
class CommonBase,
class...Rest
>
auto common_observers(use_base<CommonBase>, Rest&&...rest)
{
    using array_type = detail::common_observer_array<CommonBase, sizeof...(Rest)>;
    return array_type { rest... };
}

template<class T>
auto compute_size(T& t) {
    return t.size();
}

template<std::size_t N>
auto compute_size(const char (&)[N])
{
    return N-1;
}

template<class T>
auto compute_data(T& t)
{
    return t.data();
}

auto compute_data(const char* p) {
    return p;
}

struct const_string_observer
{
    template<class String>
    const_string_observer(const String& s)
    : _pvstr(std::addressof(s))
    , _model(get_model<String>())
    {}
    
    struct concept
    {
        virtual const char* _data(const void* pvstr) const = 0;
        virtual std::size_t _size(const void* pvstr) const = 0;
    };
    
    template<class String>
    struct model : concept
    {
        const char* _data(const void* pvstr) const override
        {
            return compute_data(get(pvstr));
        }
        
        std::size_t _size(const void* pvstr) const override
        {
            return compute_size(get(pvstr));
        }
        
    private:
        static const String& get(const void* pvstr) {
            return *reinterpret_cast<const String*>(pvstr);
        }
    };
    
    template<class String>
    const concept* get_model()
    {
        static const model<String> _ {};
        return std::addressof(_);
    }
    
    const char* begin() const {
        return _model->_data(_pvstr);
    }
    
    std::size_t size() const {
        return _model->_size(_pvstr);
    }
    
    const void* _pvstr;
    const concept* _model;
};

std::ostream& operator<<(std::ostream& os, const const_string_observer& cso)
{
    auto first = cso.begin();
    auto size = cso.size();
    return os.write(first, size);
}

int main()
{
    /*
    auto x = std::make_tuple(std::istringstream("hello world"),
                             std::istringstream("funky chicken"),
                             std::ifstream("foo.txt"));
    std::string buffer;
    for (auto& stream : common_observers(use_base<std::istream>(), x)) {
        while (stream >> buffer)
        {
            std::cout << buffer << std::endl;
        }
    }
    
    for (auto& stream : common_observers(use_base<std::istream>(),
                                         std::istringstream("hello world"),
                                         std::istringstream("funky chicken"),
                                         std::ifstream("foo.txt"),
                                         std::istringstream("bunny boiler")))
    {
        while (stream >> buffer)
        {
            std::cout << buffer << std::endl;
        }
    }
    */
    std::string s = "foo bar";
    const char a[] = "bingo bluff";
    for (auto& str : { const_string_observer(s),
        const_string_observer(a)})
    {
        std::cout << str << std::endl;
    }
    
    return 0;
}
