#include <iostream>
#include <utility>
#include <iterator>
#include <algorithm>

template<std::size_t Bits>
struct bit_iterator
{
    bit_iterator(const unsigned char* buffer, std::size_t bitnum)
    : _buffer(buffer)
    , _bitnum(bitnum)
    {}
    
    std::size_t operator*() {
        std::size_t result = 0;
        for(std::size_t count = 0 ; count < Bits ; ++count)
        {
            auto bit = 7 - (_bitnum % 8);
            auto byte = _bitnum / 8;
            auto val = _buffer[byte] & (1 << bit);
            if (val) {
                result |= 1 << ((Bits-1) - count);
            }
            ++_bitnum;
        }
        return result;
    }
    
    bit_iterator<Bits>& operator++() {
        return *this;
    }
    
    bool operator==(const bit_iterator<Bits>& r) const {
        return _buffer == r._buffer && (_bitnum + Bits) > r._bitnum;
    }
    
    bool operator!=(const bit_iterator<Bits>& r) const {
        return !(*this == r);
    }
    
    const unsigned char* _buffer;
    std::size_t _bitnum;
};

struct to_char
{

    char operator()(std::size_t index) const {
        if (index < std::extent<decltype(symbols)>::value)
        {
            return symbols[index];
        }
        else {
            return '?';
        }
    }
    static const char symbols[27];
};

const char to_char::symbols[27] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
    'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' '
};

int main()
{
    static const unsigned char bytes[25] = { 9,0,207,176,159,163,255,33,58,115,
        199,255,255,181,223,67,102,69,173,
        6,35,103,245,160,164 };
    
    
    // stream to stdout
    
    std::transform(bit_iterator<5>(bytes, 0),
                   bit_iterator<5>(bytes, std::extent<decltype(bytes)>::value * 8),
                   std::ostream_iterator<char>(std::cout),
                   to_char());
    std::cout << std::endl;
    
    // or to a string
    std::string result;
    std::transform(bit_iterator<5>(bytes, 0),
                   bit_iterator<5>(bytes, std::extent<decltype(bytes)>::value * 8),
                   std::back_inserter(result),
                   to_char());
    std::cout << result << std::endl;
    
    // or just print the codes
    std::copy(bit_iterator<5>(bytes, 0),
                   bit_iterator<5>(bytes, std::extent<decltype(bytes)>::value * 8),
                   std::ostream_iterator<std::size_t>(std::cout, ", "));
    std::cout << std::endl;
    
    return 0;
}