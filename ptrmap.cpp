//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then c++ -o "${filename}" -std=c++1y -stdlib=libc++ $BASH_SOURCE || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <memory>
#include <set>
#include <unordered_set>

using namespace std;

struct X {};


template<class T, template<class> class Comp = std::owner_less, class A>
auto
locate_in(std::weak_ptr<T> w, const std::set<weak_ptr<T>, Comp<std::weak_ptr<T>>, A>& theset)
-> std::shared_ptr<T>
{
    auto i = theset.find(w);
    if(i != end(theset)) {
        return i->lock();
    }
    else {
        return nullptr;
    }
}


auto main() -> int
{
    set<weak_ptr<X>, owner_less<weak_ptr<X>>> myset;
    unordered_set<shared_ptr<X>> uset;
    unordered_set<weak_ptr<X>> uwset;
    

    auto x = make_shared<X>();
    myset.emplace(x);
    
    auto wx = weak_ptr<X>(x);
    
    if(auto p = locate_in(wx, myset)) {
        cout << "found this: " << p.get() << endl;
        }
        else {
            cout << "didn't find it" << endl;
        }
        
        x.reset();
        
        if(auto p = locate_in(wx, myset)) {
            cout << "found this: " << p.get() << endl;
        }
        else {
            cout << "didn't find it" << endl;
        }
        
        return 0;
        }
        
