/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/asm > /dev/null
 mkdir -p ${dirname}/bin > /dev/null
 asmname="${dirname}/asm/${filename}.s"
 filename="${dirname}/bin/${filename}"
 else
 asmname="./${filename}.s"
 filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
 flags="-std=c++14"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */

#include <vector>
#include <algorithm>
#include <tuple>
#include <iostream>

using namespace std;

typedef std::vector< std::tuple<int,int,int> > vector_tuple;

namespace detail {
    template<class Pred, std::size_t...Is>
    struct order_by_parts
    {
        constexpr
        order_by_parts(Pred&& pred)
        : _pred(std::move(pred))
        {}
        
        template<class...Ts>
        constexpr
        static auto sort_order(const std::tuple<Ts...>& t)
        {
            return std::tie(std::get<Is>(t)...);
        }
        
        template<class...Ts>
        constexpr
        bool operator()(const std::tuple<Ts...>& l,
                        const std::tuple<Ts...>& r) const
        {
            return _pred(sort_order(l), sort_order(r));
        }
        
    private:
        Pred _pred;
    };
}

template<class Pred, size_t...Is>
constexpr
auto order_by_parts(Pred&& pred, std::index_sequence<Is...>)
{
    using pred_type = std::decay_t<Pred>;
    using functor_type = detail::order_by_parts<pred_type, Is...>;
    return functor_type(std::forward<Pred>(pred));
}

int main()
{
    std::vector<int> v1{1,1,1,6,6,5,4,4,5,5,5};
    std::vector<int> v2(v1);
    vector_tuple vt;
    std::tuple<int,int,int> t1;
    std::vector<int>::iterator iter;
    int sizev=v1.size();
    for(int i=0; i < sizev ; i++)
    {
        auto countnu = count(begin(v2),end(v2),v1[i]);
        if(countnu > 0)
        {
            v2.erase(std::remove(begin(v2),end(v2),v1[i]),end(v2));
            auto t = std::make_tuple(v1[i], countnu, i);
            vt.push_back(t);
        }
    }
    sort(begin(vt),end(vt),
         order_by_parts(std::less<>(),
                        std::index_sequence<1, 2>()));
    
    for (int i=0; i < vt.size(); i++)
    {
        
        cout << get<0>(vt[i]) << " " ;
        cout << get<1>(vt[i]) << " " ;
        cout << get<2>(vt[i]) << " \n" ;
    }
    
}

