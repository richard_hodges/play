/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" "$1" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <sstream>
#include <iomanip>

std::ostream& unescape(char c)
{
    if (c == '\\')
        return std::cerr << "\\\\";

    if (c == 'n')
        return std::cerr << '\n';

    if (c == 't')
        return std::cerr << '\t';
    
    return std::cerr << c;
}

int main(int argc, const char** argv)
{
    using namespace std;
    
    if (argc != 2)
    {
        cerr << "usage: unquote 'string'" << endl;
        for (auto arg = argv ; arg < (argv + argc) ; ++arg)
        {
            std::cerr << *arg << std::endl;
        }
        exit(100);
    }
    
    string s(argv[1]);
    bool in_escape = false;
    for (auto c : s)
    {
        if (in_escape)
        {
            unescape(c);
            in_escape = false;
        }
        else if (c == '\\') {
            in_escape = true;
        }
        else {
            cout << c;
        }
    }
    if (in_escape) {
        unescape('\\');
    }
    cout << endl;
	return 0;
}

