/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>

struct Animals
{
};

struct Pigs : Animals
{
};



bool operator==(const Animals&, const Animals&)
{
    return true;
}

bool operator>(const Pigs&, const Animals&)
{
    return true;
}

auto quote = [](auto pre, auto cond, auto post)
{
    if (cond) std::cout << "all ";
    else std::cout << "not all ";
    std::cout << pre << " are ";
    std::cout << post << std::endl;
};

auto not_quote = [](auto pre, auto cond, auto post)
{
    std::cout << "not all " << pre;
    if (cond) std::cout << " are ";
    else std::cout << " are not ";
    std::cout << post << std::endl;
};

int main()
{
    Elephant elephant;
    Animal animal;
    
    quote("elephants", elephant == animal, "animals");
    quote("animals", animal == elephant, "elephants");
    not_quote("elephants", elephant != animal, "animals");
    not_quote("animals", animal != elephant, "elephants");
}
