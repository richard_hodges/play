/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++11"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" --test && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>

template<class...Ts>
std::istream& acquire(std::istream& stream, const char* prompt, Ts&...ts)
{
    if (std::addressof(stream) == std::addressof(static_cast<std::iostream&>(std::cin))) {
        std::cout << prompt;
        std::cout.flush();
    }
    using expand = int[];
    void(expand { 0, ((stream >> ts),0)... });
    return stream;
}

bool args_have_test(const std::vector<std::string>& args)
{
    auto ifind = std::find(std::begin(args), std::end(args), "--test");
    return ifind != std::end(args);
}

[[noreturn]]
bool input_error(const char* context)
{
    std::cerr << "input error while " << context << std::endl;
    exit(2);
}


int main(int argc, const char* const *argv)
{
    std::istringstream stest {
        "5\n"
        "bob 5\n"
        "bill 2\n"
        "bernie 9\n"
        "bert 7\n"
        "bart 8\n"
    };
    auto args = std::vector<std::string> { argv + 1, argv + argc };
    auto& stream = args_have_test(args) ? static_cast<std::istream&>(stest)
    : static_cast<std::istream&>(std::cin);
    int count = 0;
    acquire(stream, "enter number of students: ", count)
    or input_error("entering number of students");
    
    std::vector<std::string> names;
    names.reserve(count);
    std::vector<int> grades;
    grades.reserve(count);
    std::string name;
    int grade;
    while (count--) {
        acquire(stream, "enter name and grade followed by enter: ", name, grade)
        or input_error("entering name and grade");
        
        names.push_back(name);
        grades.push_back(grade);
    }
    auto imax = std::max_element(std::begin(grades), std::end(grades));
    if (imax == std::end(grades)) {
        std::cerr << "empty list\n";
        exit(1);
    }
    auto iname = std::next(std::begin(names), std::distance(std::begin(grades),
                                                            imax));
    std::cout << "highest grade was " << *imax << " acheived by " << *iname << std::endl;
    return 0;
}


