/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <algorithm>
#include <iostream>
#include <vector>

template<typename container_ty_, class Comp>
auto where(container_ty_& V, Comp&& comp)
{
    using value_type = typename container_ty_::value_type;
    using reference =
    std::conditional_t<
      std::is_const<container_ty_>::value,
        std::reference_wrapper<const value_type>,
        std::reference_wrapper<value_type>
    >;
    
    std::vector<reference> cursor;
    
    for(auto& VAL : V)
        if(comp(VAL))
            cursor.push_back(VAL);
            
    return cursor;
}

int main(int argc, char** argv) {
    std::vector<int> tVect = {0, 5, 2, 1, 7, 9};
    
    //Why must std::vector<int> be passed...
    auto vec = where(tVect, [](const int& V) -> bool { return V > 5; });
    
    std::for_each(vec.begin(), vec.end(), [] (int& v) { std::cout << v++ << std::endl; });
    std::cout << std::endl;
    std::for_each(tVect.begin(), tVect.end(), [](const int& v) { std::cout << v << std::endl; });
}
