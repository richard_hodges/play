/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <iomanip>
#include <array>
#include <cstdint>
#include <algorithm>
#include <utility>
#include <string>
#include <sstream>

/*
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                          time_low                             |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |       time_mid                |         time_hi_and_version   |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |clk_seq_hi_res |  clk_seq_low  |         node (0-1)            |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 |                         node (2-5)                            |
 +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

std::uint8_t* to_be(std::uint32_t v, std::uint8_t * dest)
{
    dest[0] = (v >> 24) & 0xff;
    dest[1] = (v >> 16) & 0xff;
    dest[2] = (v >> 8) & 0xff;
    dest[3] = (v) & 0xff;
    return &dest[4];
}

std::uint8_t* to_be(std::uint16_t v, std::uint8_t * dest)
{
    dest[0] = (v >> 8) & 0xff;
    dest[1] = (v) & 0xff;
    return &dest[2];
}

std::uint8_t* to_le(std::uint32_t v, std::uint8_t * dest)
{
    dest[3] = (v >> 24) & 0xff;
    dest[2] = (v >> 16) & 0xff;
    dest[1] = (v >> 8) & 0xff;
    dest[0] = (v) & 0xff;
    return &dest[4];
}

std::uint8_t* to_le(std::uint16_t v, std::uint8_t * dest)
{
    dest[1] = (v >> 8) & 0xff;
    dest[0] = (v) & 0xff;
    return &dest[2];
}

std::uint32_t from_le32(const std::uint8_t* source)
{
    std::uint32_t result = 0;
    result += std::uint32_t(source[0]);
    result += std::uint32_t(source[1]) << 8;
    result += std::uint32_t(source[2]) << 16;
    result += std::uint32_t(source[3]) << 24;
    return result;
}

std::uint16_t from_le16(const std::uint8_t* source)
{
    std::uint16_t result = 0;
    result += std::uint16_t(source[0]);
    result += std::uint16_t(source[1]) << 8;
    return result;
}

std::uint32_t from_be32(const std::uint8_t* source)
{
    std::uint32_t result = 0;
    result += std::uint32_t(source[3]);
    result += std::uint32_t(source[2]) << 8;
    result += std::uint32_t(source[1]) << 16;
    result += std::uint32_t(source[0]) << 24;
    return result;
}

std::uint16_t from_be16(const std::uint8_t* source)
{
    std::uint16_t result = 0;
    result += std::uint16_t(source[1]);
    result += std::uint16_t(source[0]) << 8;
    return result;
}

struct big_endian {};
struct little_endian {};

struct uuid
{
    uuid(big_endian,
         std::uint32_t time_low, std::uint16_t time_mid,
         std::uint16_t time_hi_and_version,
         std::uint8_t clk_seq_hi_res, std::uint8_t clk_seq_low,
         std::initializer_list<std::uint8_t> node)
    {
        auto p = to_be(time_low, &_data[0]);
        p = to_be(time_mid, p);
        p = to_be(time_hi_and_version, p);
        *p++ = clk_seq_hi_res;
        *p++ = clk_seq_low;
        std::copy(std::begin(node), std::end(node), p);
    }
    
    uuid(little_endian,
         std::uint32_t time_low, std::uint16_t time_mid,
         std::uint16_t time_hi_and_version,
         std::uint8_t clk_seq_hi_res, std::uint8_t clk_seq_low,
         std::initializer_list<std::uint8_t> node)
    {
        auto p = to_le(time_low, &_data[0]);
        p = to_le(time_mid, p);
        p = to_le(time_hi_and_version, p);
        *p++ = clk_seq_hi_res;
        *p++ = clk_seq_low;
        std::copy(std::begin(node), std::end(node), p);
    }
    
    std::uint32_t time_low(little_endian) const
    {
        return from_le32(&_data[0]);
    }
    std::uint32_t time_low(big_endian) const
    {
        return from_be32(&_data[0]);
    }
    
    std::uint16_t time_mid(little_endian) const
    {
        return from_le16(&_data[4]);
    }
    std::uint16_t time_mide(big_endian) const
    {
        return from_be16(&_data[4]);
    }
    
    std::uint16_t time_mid_high_and_version(little_endian) const
    {
        return from_le16(&_data[6]);
    }
    std::uint16_t time_mid_high_and_version(big_endian) const
    {
        return from_be16(&_data[6]);
    }
    
    std::ostream& chunk(std::ostream& os, std::size_t from, std::size_t to) const
    {
        bool reverse = from > to;
        while (from != to)
        {
            if (reverse)
                --from;
            os << std::hex << std::setw(2) << std::setfill('0') << std::uint16_t(_data[from]);
            if (not reverse)
                ++from;
        }
        return os;
    }
    
    std::string as(little_endian) const
    {
        std::ostringstream ss;
        chunk(ss, 4,0) << '-';
        chunk(ss, 6,4) << '-';
        chunk(ss, 8,6) << '-';
        chunk(ss, 8,10) << '-';
        chunk(ss, 10,16);
        return ss.str();
    }
    
    std::string as(big_endian) const
    {
        std::ostringstream ss;
        chunk(ss, 0,4) << '-';
        chunk(ss, 4,6) << '-';
        chunk(ss, 6,8) << '-';
        chunk(ss, 8,10) << '-';
        chunk(ss, 10,16);
        return ss.str();
    }
    
    
    
    std::array<std::uint8_t, 16> _data;
};


int main()
{
    uuid a = { big_endian(),
        0x00112233, 0x4455, 0x6677,
        0x88, 0x99,
        { 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff } };
    uuid b = { little_endian(),
        0x00112233, 0x4455, 0x6677,
        0x88, 0x99, 
        { 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff } };
    
    std::cout << a.as(little_endian()) << std::endl;
    std::cout << a.as(big_endian()) << std::endl;
    std::cout << b.as(little_endian()) << std::endl;
    std::cout << b.as(big_endian()) << std::endl;
    
}


