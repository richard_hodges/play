/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++98"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <string>
#include <sstream>
#include <vector>
#include <iostream>

// define the template function. Users will supply overloads of emit
template<class T>
std::ostream& emit(std::ostream& os, const T& v)
{
    return os << v;
}

// define a template class emitter which by default simply calls operator<<
template<class T>
struct emitter
{
    emitter(const T& v) : _v(v) {}
    std::ostream& operator()(std::ostream& os) const {
        return emit(os, _v);
    }
    const T& _v;
};

// emitter<>'s are streamable
template<class T>
std::ostream& operator<<(std::ostream& os, const emitter<T>& e)
{
    return e(os);
}

// a factory function to generate the correct emitter
template<class T>
emitter<T> make_emitter(const T& v)
{
    return emitter<T>(v);
}

// write one version of stringify in terms of emit<>()
template <class T>
std::string stringify(const T& in) {
    std::stringstream stream;
    stream << make_emitter(in);
    return stream.str();
}
// ======= OVERLOADS PROVIDED BY THE USER =======

template<typename T, typename T2>
struct myType { T data; T2 op; };

// user must provide an emit overload for each custom type
template<class T, class T2>
std::ostream& emit(std::ostream& os, const myType<T, T2>& v)
{
    return os << "myType";
}
// and for any std:: templates he wants to support
template<class V, class A>
std::ostream& emit(std::ostream& os, const std::vector<V, A>& v)
{
    return os << "vector<T, A>";
}

// test
int main() {
    myType<int, float> a;   std::cout << stringify(a) << std::endl; // prints "myType"
    std::cout << stringify(6) << std::endl; // prints "6"
    std::vector<int> v(5);       std::cout << stringify(v) << std::endl; // prints "{?}"
    
    return 0;
}

