#include "c2f.hpp"
#include "c2f_function.cpp"


int main()
{
    // define variables
    double x, y;
    
    // create old candidates list
    list<Candidate> old_candidates;
    
    // create new candidates list
    list<Candidate> new_candidates1;
    list<Candidate> new_candidates2;
    list<Candidate> new_candidates3;
    
    // create new features list
    list<Candidate> feature_list;
    
    //=============================================================================//
    // LOAD FIRST DATA SET
    //-----------------------------------------------------------------------------//
    
    ifstream file1_("newcandidates_it0.txt");
    if (file1_.is_open())
    {
        cout << "Reading file...1 " << endl;
        while( file1_ >> x >> y)
        {
            // cout << x << "," << y << endl;
            new_candidates1.push_back(Candidate(x , y));
            
        }
        file1_.close();
    }
    else {cout << "file is not open";}
    //=============================================================================//
    
    c2f_function(new_candidates1, old_candidates, feature_list);
    
    
    //=============================================================================//
    // LOAD SECOND DATA SET
    //-----------------------------------------------------------------------------//
    
    ifstream file2_("newcandidates_it1.txt");
    if (file2_.is_open())
    {
        cout << "Reading file...2 " << endl;
        while( file2_ >> x >> y)
        {
            // cout << x << "," << y << endl;
            new_candidates2.push_back(Candidate(x , y));
            
        }
        file2_.close();
    }
    else {cout << "file is not open";}
    //=============================================================================//
    
    c2f_function(new_candidates2, old_candidates, feature_list);