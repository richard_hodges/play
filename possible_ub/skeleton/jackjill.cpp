#include <iostream>
#include <memory>

namespace std
{
    template<>
    struct owner_less<std::shared_ptr<void>>
    {
        template<class T, class U>
        bool operator()(const std::shared_ptr<T>& l, const std::shared_ptr<U>& r) const
        {
            return l.owner_before(r);
        }
    };
}

int main()
{
    
    auto p = std::make_shared<int>(10);
    auto q = std::make_shared<int>(11);
    
    auto vp = std::shared_ptr<void>(p);
    
    std::cout << std::owner_less<std::shared_ptr<int>>()(p, q) << std::endl;
    std::cout << std::owner_less<std::shared_ptr<int>>()(q, p) << std::endl;
    
    auto wp = std::weak_ptr<int>(p);
    
    std::cout << std::owner_less<std::shared_ptr<int>>()(p, wp) << std::endl;
    std::cout << std::owner_less<std::shared_ptr<int>>()(wp, p) << std::endl;

    std::cout << std::owner_less<std::shared_ptr<void>>()(p, vp) << std::endl;

}

