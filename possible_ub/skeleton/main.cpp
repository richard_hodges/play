///
/// goal:       privide partial ordering of objects derived from A on the basis
///             only of class type.
/// constraint: ordering specified by us
///

#include <vector>
#include <typeinfo>
#include <algorithm>
#include <iostream>
#include <tuple>
#include <iomanip>


class A
{
public:
    virtual bool operator< (const A &rhs) const = 0;
    
    std::ostream& print(std::ostream& os) const {
        handle_print(os);
        return os;
    }
    
private:
    virtual void handle_print(std::ostream&) const = 0;
};

std::ostream& operator<<(std::ostream& os, const A& a) {
    return a.print(os);
}

template<class T> struct impl_A : public A
{
    bool operator< (const A &rhs) const override
    {
        auto& rhs_info = typeid(rhs);
        auto& lhs_info = typeid(T);
        if (rhs_info == lhs_info) {
            // same type, so do comparison
            return static_cast<const T&>(*this).ordering_tuple() < static_cast<const T&>(rhs).ordering_tuple();
        }
        else {
            return lhs_info.before(rhs_info);
        }
    }
};

class B: public impl_A<B> {
public:
    B(int v) : _value(v) {}
    
    auto ordering_tuple() const {
        return std::tie(_value);
    }
    
private:
    
    void handle_print(std::ostream& os) const override {
        os << _value;
    }
    
    int _value;
};



class C: public impl_A<C> {
public:
    C(std::string v) : _value(std::move(v)) {}

    auto ordering_tuple() const {
        return std::tie(_value);
    }

private:
    
    void handle_print(std::ostream& os) const override {
        os << std::quoted(_value);
    }

    std::string _value;
};

// now we need to write some compare functions


void test(const A& l, const A& r)
{
    if (l < r) {
        std::cout << l << " is less than " << r << std::endl;
    }
    else {
        std::cout << l << " is not less than " << r << std::endl;
    }
}

int main()
{
    test(B(1), C("hello"));
    test(B(0), B(1));
    test(B(1), B(0));
    test(B(0), B(0));
    test(C("hello"), B(1));
    test(C("goodbye"), C("hello"));
    test(C("goodbye"), C("goodbye"));
    test(C("hello"), C("goodbye"));
    
}