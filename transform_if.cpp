/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <algorithm>
#include <type_traits>

template<class InIter, class OutIter, class Filter, class Transform>
void transform_if(InIter first, InIter last, OutIter dest, Filter&& filter, Transform&& transform)
{
    for (; first != last; ++first) {
        if (filter(*first)) {
            *dest++ = transform(*first);
        }
    }
}

template<class T,
class A,
class Filter,
class Transform,
class R = std::result_of_t<Transform(const T&)>
>
auto transform_if(const std::vector<T, A>& source, Filter&& filter, Transform&& transform)
{
    std::vector<R, typename A::template rebind<R>::other> dest;
    dest.reserve(source.size());
    transform_if(source.begin(), source.end(),
                 std::back_inserter(dest),
                 std::forward<Filter>(filter),
                 std::forward<Transform>(transform));
    return dest;
}



auto main() -> int
{
    using namespace std;

    auto s = vector<string> { "a", "anchor", "b", "banana", "c", "cabbage" };
    auto d = transform_if(s,
                          [](const auto& s) { return s.length() > 1; },
                          [](const auto& s) { return s.length(); });
    copy(begin(d), end(d), ostream_iterator<size_t>(cout, ", "));
    cout << endl;
	return 0;
}

