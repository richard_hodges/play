/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <exception>
#include <stdexcept>
#include <iostream>
#include <string>
#include <utility>
#include <typeinfo>
#include <typeindex>
#include <functional>
#include <set>

struct exception_model
{
    std::typeindex type;
    std::function<std::exception_ptr(void)> make_model;
    std::function<bool(std::exception_ptr)> can_catch;
    std::function<void(std::string message, std::exception_ptr original)> re_throw;
};

struct model_less
{
    bool operator()(const auto&l, const auto& r) const {
        return l.type.before(r.type);
    }
};

struct model_tree
{
    std::unordered_set<exception_model, model_less> _models;
};

template<class Exception> void raise(Exception e) {
    register_exception_type(typeid(e), e);
    throw e;
}

Nodes can be compared along the following lines:

`if (l.can_catch(r.make_model()))` then r is more-derived or equal to l

equality can be checked with `l.type == r.type`

Nodes would be added whenever `raise<>` fails to find a matching node (which can be done quickly by traversing the tree ckecking `r.type() == r.type()`)

raise would look something like this:

template<class E> void raise(E&& e)
{
    if (!tree.exists(typeid(e))) {
        tree.add(node {
            // type
            typeid(e),
            
            // make_model
            [e] {
                try { throw e; } catch(...) { return std::current_exception(); }
            },
            
            // can_catch
            [](std::exception_ptr ep) -> bool {
                try {
                    std::rethrow_exception(ep);
                }
                catch(E&) {
                    return true;
                }
                catch(...) { }
                return false;
            },
            
            // rethrow
            [] (std::string message, std::exception_ptr original) {
                try {
                    std::rethrow_exception(original);
                }
                catch(E&) {
                    // a specialise here if you need custom exception construction
                    std::throw_with_nested(E(message));
                }
            }
        });
    }
    throw std::move(e);
}

void reraise(std::string message, std::exception_ptr pe)
{
    if(auto pnode = tree.find(pe))
    {
        pnode->reraise(message, pe);
    }
    else
    {
        // fallback position
        std::throw_with_nested(std::runtime_error(message));
    }
}

Obviously we'd pre-register `std::runtime_error` and `std::logic_error` et.al.

What does all this give us?

This gives us the ability to dynamically determine the exact type of any exception we catch (or the best known base class of it).

Furthermore, it allows us to find the node most relevant to that exception

And having done that, allows us to rethrow the same kind of exception with a different message, with the original exception nested within it.

The ultimate goal for me is to allow the building of a call stack during exception unwinding while preserving the originating exception type.

e.g.

void foo()
try
{
    raise(some_exception("can't foo");
          }
          catch(std::exception& e)
    {
        reraise("foo()", std::current_exception());
    }
          
          However, you could store any kind of function object you like in a `node` which will give access to the exception type at the point of the catch, and the ability to handle
