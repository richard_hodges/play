//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <algorithm>
#include <iterator>
#include <memory>

using namespace std;

using array_ptr_type = std::unique_ptr<char[]>;
using array_of_arrays_type = std::unique_ptr<array_ptr_type[]>;

auto main() -> int
{
    auto x = array_ptr_type(new char[10]);
    auto y = array_ptr_type(new char[10]);
    for (int i = 0 ; i < 10 ; ++i)
    {
        x[i] = char('0' + i);
        y[i] = char('0' + 9 - i);
    }
    
    auto pxy = array_of_arrays_type(new array_ptr_type[2]);
    pxy[0] = move(x);
    pxy[1] = move(y);
    
    for (int i = 0 ; i < 2 ; ++i) {
        copy(&pxy[i][0], &pxy[i][10], ostream_iterator<char>(cout, ", "));
        cout << endl;
    }
	return 0;
}

