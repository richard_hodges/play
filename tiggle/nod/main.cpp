#include <memory>


template<class T>
struct clone_ptr;

template<class T>
auto clone(clone_ptr<T> const&) -> clone_ptr<T>;

template<class T>
struct clone_ptr : std::unique_ptr<T>
{
    using underlying_pointer = std::unique_ptr<T>;

    using underlying_pointer::underlying_pointer;

    clone_ptr(clone_ptr &&other) = default;

    clone_ptr &operator=(clone_ptr &&other) = default;

    clone_ptr(clone_ptr const &other)
        : clone_ptr(clone(other))
    {}

    clone_ptr &operator=(clone_ptr const &other)
    {
        auto tmp = clone_ptr(other);
        swap(tmp, *this);
        return *this;
    }

    void swap(clone_ptr &other)
    {
        underlying_pointer::swap(other);
    }
};

template<class T>
auto clone(clone_ptr<T> const& other)
{
    return other->clone(other);
};


struct A
{
    virtual std::unique_ptr<A> clone(std::unique_ptr<A> const& source)
    {
        return std::make_unique<A>(*this);
    }
    ~A() = default;
};

struct B : A
{
    virtual std::unique_ptr<A> clone(std::unique_ptr<A> const& source)
    {
        return std::make_unique<A>(*this);
    }

};

int main()
{
    auto ap = clone_ptr(std::make_unique<B>());
}