/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <utility>
#include <tuple>
#include <string>
#include <iostream>

// for any value I, write a comma and space to stdout
template<std::size_t I>
void emit_sep()
{
    std::cout << ", ";
}

// specialise for when I is zero... no comma in this case
template<>
void emit_sep<0>()
{
}

// emit and value at some position I. Use emit_sep<I> to determine whether
// to print a separator
template<std::size_t I, class T>
void emit(const T& t)
{
    emit_sep<I>();
    std::cout << t;
}

// given a tuple type and a sequence of integers (Is...) emit the value
// at each index position of the tuple. Take care to emit a separator only
// before each element after the first one
template<class Tuple, size_t...Is>
void impl_show_it(const Tuple& tup, std::index_sequence<Is...>)
{
    using expand = int[];

    std::cout << "here are the indexes in the index_sequence: ";
    void(expand {
        0,
        (emit<Is>(Is), 0)...
    });
    std::cout << std::endl;
    
    std::cout << "here are the values in the tuple: ";
    void(expand {
        0,
        (emit<Is>(std::get<Is>(tup)), 0)...
    });
    std::cout << std::endl;
}

// for some tuple type, compute the size of the tuple, build an index sequence
// representing each INDEX in the tuple and then use that sequence to call
// impl_show_it in order to actually perform the write
template<class Tuple>
void show_it(const Tuple& tup)
{
    constexpr auto tuple_size = std::tuple_size<Tuple>::value;
    auto sequence = std::make_index_sequence<tuple_size>();
    impl_show_it(tup, sequence);
}

// make a tuple and then show it on stdout
int main()
{
    auto t = std::make_tuple(6, std::string("hello"), 5.5);
    show_it(t);
}

