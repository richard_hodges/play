/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -S -O3 -std=c++1y $BASH_SOURCE || exit
	c++ -o ${filename} -O3 -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <utility>
#include <string>

namespace detail
{
    class CRC16
    {
    private:
        
        // the storage for the CRC table, to be computed at compile time
        std::uint16_t CRCTab[256];
        
        // private template-expanded constructor allows folded calls to SwapBits at compile time
        template<std::size_t...Is>
        constexpr CRC16(std::uint16_t poly,
                        std::integer_sequence<std::size_t, Is...>)
        : CRCTab { SwapBits(Is, 8) << 8 ... }
        {}
        
        // swap bits at compile time
        static constexpr std::uint32_t SwapBits(std::uint32_t swap, int bits)
        {
            std::uint32_t r = 0;
            for(int i = 0; i < bits; i++) {
                if(swap & 1) r |= 1 << (bits - i - 1);
                swap >>= 1;
            }
            return r;
        }
        
        
    public:
        
        // public constexpr defers to private template expansion...
        constexpr CRC16(std::uint16_t poly)
        : CRC16(poly,
                std::make_index_sequence<256>())
        {
            //... and then modifies the table - at compile time
            for(int i = 0; i < 256; i++) {
                for(int j = 0; j < 8; j++)
                    CRCTab[i] = (CRCTab[i] << 1) ^ ((CRCTab[i] & 0x8000) ? poly : 0);
                CRCTab[i] = SwapBits(CRCTab[i], 16);
            }
        }
        
        // made const so that we can instantiate constexpr CRC16 objects
        unsigned short calc_crc(const unsigned char* first, const uint8_t* last) const
        {
            unsigned short r = 0;
            for( ; first != last ; ++first) {
                r = (r >> 8) ^ CRCTab[(r & 0xFF) ^ *first];
            }
            return r;
        }
        
        // provide a means to treat signed chars as unsigned chars
        unsigned short calc_crc(const signed char* first, const signed char* last) const
        {
            return calc_crc(reinterpret_cast<const unsigned char*>(first),
                            reinterpret_cast<const unsigned char*>(last));
        }
        
        // provide a means to treat chars as unsigned chars
        unsigned short calc_crc(const char* first, const char* last) const
        {
            return calc_crc(reinterpret_cast<const unsigned char*>(first),
                            reinterpret_cast<const unsigned char*>(last));
        }
        
    };
}


template<class Iter, std::uint16_t poly = 0x1021>
constexpr std::uint16_t crc16(Iter first, Iter last)
{
    constexpr detail::CRC16 crctab(poly);
    return crctab.calc_crc(std::addressof(*first),
                           std::addressof(*last));
}



template<class Container, std::uint16_t seed = 0x1021>
constexpr std::uint16_t crc16(const Container& c)
{
    using std::begin;
    using std::end;
    return crc16<decltype(begin(c)), seed>(begin(c),
                                           end(c));
}


std::uint16_t test1()
{
    // caclulate the CRC of something...
    using namespace std;
    auto s = "hello world"s;
    
    return crc16(s.begin(), s.end());
}

std::uint16_t test2()
{
    auto s = "hello world";
    return crc16(s, s + strlen(s));
}

int main()
{
    using namespace std;
    cout << test1() << endl;
    
    cout << test2() << endl;
    
    return 0;
}
















