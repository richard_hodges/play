/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -Os -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <string>
#include <iostream>

using namespace std;

const std::string& to_string(const std::string& s) { return s; }

template<typename T>
auto create_topic(T&& v) -> std::string {
    return to_string(std::forward<T>(v));
}

template<typename T, typename... Args>
auto create_topic(T&& first, Args&&... args) -> std::string {
    return to_string(std::forward<T>(first)) + "." + create_topic(std::forward<Args>(args)...);
}

int main(int argc, char** argv)
{
    std::cout << create_topic("foo", 6, std::string("bar")) << std::endl;
    return (0);
}

