/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <tuple>
#include <iomanip>

struct json_prerry_traits
{
    json_prerry_traits(int depth = 0)
    : depth(depth)
    {}
    static constexpr bool pretty() { return true; }
    int depth = 0;
};

struct json_oneline_traits
{
    static constexpr bool pretty() { return false; }
};

template<class X>
struct json_printer
{
    json_printer(const X& x) : x(x) {}
    void operator()(std::ostream& os) const {
        os << x;
    }
    const X& x;
};

template<class X>
std::ostream& operator<<(std::ostream& os, const json_printer<X>& printer)
{
    printer(os);
    return os;
}

template<class X, class Traits = json_oneline_traits>
json_printer<X> to_json(const X& x, Traits&& = Traits())
{
    return { x };
}

namespace detail {
    template<class Name, class Value>
    struct nvp
    {
        nvp(Name name, const Value& value)
        : name(std::move(name)), value(value) {}
        Name name;
        const Value& value;
    };
}
template<class Name, class Value>
detail::nvp<Name, Value> nvp(Name&& name, const Value& value)
{
    return { std::forward<Name>(name), value };
}

template<class Name, class Value>
struct json_printer<detail::nvp<Name, Value>>
{
    using argument_type = detail::nvp<Name, Value>;
    json_printer(const argument_type& x) : x(x) {}
    void operator()(std::ostream& os) const {
        os << std::quoted(x.name);
        os << " : ";
        os << to_json(x.value);
    }
    const argument_type& x;
};

template<class...Values>
struct json_printer<std::tuple<Values...>>
{
    using argument_type = std::tuple<Values...>;
    json_printer(const argument_type& x) : x(x) {}
    template<size_t...Is>
    void do_print(std::ostream& os, std::index_sequence<Is...>) const {
        using expander = int[];
        (void)expander { 0, (os << (Is==0 ? "" : ", ") << to_json(std::get<Is>(x)), 0)... };
    }
    void operator()(std::ostream& os) const {
        os << "{";
        do_print(os, std::index_sequence_for<Values...>());
        os << " }";
    }
    const argument_type& x;
};

template<class C, class T, class A>
struct json_printer<std::basic_string<C, T, A>>
{
    using argument_type = std::basic_string<C, T, A>;
    json_printer(const argument_type& x) : x(x) {}
    void operator()(std::ostream& os) const {
        os << std::quoted(x);
    }
    const argument_type& x;
};

template<class V, class A>
struct json_printer<std::vector<V, A>>
{
    using argument_type = std::vector<V, A>;
    json_printer(const argument_type& x) : x(x) {}
    void operator()(std::ostream& os) const {
        os << "[";
        auto sep = "";
        for (const auto& y : x) {
            os << sep << to_json(y);
            sep = ", ";
        }
        os << " ]";
    }
    const argument_type& x;
};


/*
namespace aux{
    template<class Value>
    struct nvp
    {
        nvp(const char* name, const Value& Value)
        : name(name), value(value)
        {}
        
        const char* name;
        const Value& value;
        void print_json(std::ostream& os, const nvp) {
            os << nvp.name << " : ";
            print_json(os, nvp.value);
        }
    };
    template<std::size_t...> struct seq{};
    
    template<std::size_t N, std::size_t... Is>
    struct gen_seq : gen_seq<N-1, N-1, Is...>{};
    
    template<std::size_t... Is>
    struct gen_seq<0, Is...> : seq<Is...>{};
    
    template<class Ch, class Tr, class Tuple, std::size_t... Is>
    void print_tuple(std::basic_ostream<Ch,Tr>& os, Tuple const& t, seq<Is...>){
        using swallow = int[];
        (void)swallow{0, (void(os << (Is == 0? "" : ", ") << std::get<Is>(t)), 0)...};
    }
} // aux::

template<class Ch, class Tr, class... Args>
auto operator<<(std::basic_ostream<Ch, Tr>& os, std::tuple<Args...> const& t)
-> std::basic_ostream<Ch, Tr>&
{
    os << "{";
    aux::print_tuple(os, t, aux::gen_seq<sizeof...(Args)>());
    return os << " }";
}

struct B
{
    B(const char* text)
    : a(text), b(text)
    {}
    
    std::string a;
    const char* b;
};
auto to_tuple(const B& b) {
    return std::make_tuple(std::tie("a", b.a),
                           std::tie("b", b.b));
}

struct A
{
    int a = 4;
    B b { "foo" };
    std::string c = "hello";
};

auto to_tuple(const A& a) {
    return std::make_tuple(std::tie("a", a.a),
//                           std::make_tuple("b", to_tuple(a.b)),
                           std::tie("c", a.c));
}
*/
using namespace std;

auto main() -> int
{
    auto a = nvp("a", 6);
    auto b = nvp("b", 7);
    
    auto cval = std::string("hello \"foo\"");
    auto c = nvp("c", cval);

    auto dvec = vector<string>{"foo", "bar", "baz"};
    auto d = nvp("d", dvec);
    
    auto eval = tie(a, b, c, d);
    auto e = nvp("e", eval);
    
    
    cout << to_json(a) << endl;
    cout << to_json(b) << endl;
    cout << to_json(tie(a, b, c, d, e)) << endl;
    cout << endl;
	return 0;
}

