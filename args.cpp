/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -Os -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <tuple>
#include <utility>
#include <string>

// general case - print arg with preceeding dot
template<size_t I>
struct printer {
    template<class T>
    static int print(std::string& os, T&& t) {
        os += '.';
        os += t;
        return 0;
    }
};

// zeroth case - dont print the preceeding dot
template<>
struct printer<0>
{
    template<class T>
    static int print(std::string& os, T&& t) {
        os += t;
        return 0;
    }
};

const std::string& to_string(const std::string& s) {
    return s;
}

// iterate over tuple invoking a print<i> for each index
template<class Tuple, std::size_t...Is>
std::string make_topic_impl(Tuple&& t, std::index_sequence<Is...>)
{
    std::string ss;
    using std::to_string;
    using ::to_string;
    using unpack = int[];
    (void)unpack { 0, printer<Is>::print(ss, to_string(std::get<Is>(t)))... };
    return ss;
}

// turn the argument pack into a tuple and an index sequence
template<class...Args>
std::string make_topic(Args&&...args)
{
    return make_topic_impl(std::make_tuple(std::forward<Args>(args)...),
                           std::make_index_sequence<sizeof...(Args)>());
}

auto main() -> int
{
    std::cout << make_topic("foo", 6, std::string("bar")) << std::endl;
    return 0;
}
    
