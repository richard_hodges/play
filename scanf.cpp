/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <sstream>


int main()
{
    std::cout << "with operator >>" << std::endl;
    
    std::istringstream ss(" t t ");
    char c;
    while (ss >> c)
        std::cout << "[" << c << "]" << std::endl;
    
    std::cout << "with scanf" << std::endl;
    auto str = " t t ";
    for (int i = 0 ; i < 5 ; ++i)
    {
        char c;
        if (sscanf(str + i, "%c", &c)) {
            std::cout << "[" << c << "]" << std::endl;
        }
    }
}

