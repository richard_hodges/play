/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -O3"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <limits>
#include <utility>

constexpr int bits_less(unsigned int sample, int cmp)
{
    auto samples = std::numeric_limits<unsigned int>::digits;
    auto tot = 0;
    while (samples)
    {
        auto acc = sample & 0x3;
        if (acc <= cmp)
            ++tot;
        sample >>= 2;
        samples -= 2;
    }
    return tot;
    
}

struct less_table
{
    constexpr less_table()
    : less_table(std::make_index_sequence<entries>())
    {}
    
    template<std::size_t...Is>
    constexpr less_table(std::index_sequence<Is...>)
    : less_tab {
        { bits_less(Is, 0), bits_less(Is, 1), bits_less(Is, 2) }
    }
    {}
    
    constexpr int get(unsigned int sample, int pattern) const
    {
        return less_tab[sample][pattern];
    }
    
    static constexpr std::size_t entries = std::size_t(std::numeric_limits<unsigned int>::max()) + 1;
    int less_tab[entries][3];
};

int main()
{
    std::cout << bits_less(0x34237645, 0x01) << std::endl;
    
    constexpr less_table lt;
    std::cout << lt.get(0x34237645, 0x01) << std::endl;
}
