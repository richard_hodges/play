#include <Foundation/Foundation.h>
#include <CoreGraphics/CoreGraphics.h>
#include <CoreImage/CoreImage.h>

#include <iostream>
#include <chrono>
#include <vector>
#include <iomanip>
#include <algorithm>

struct PLY
{
    PLY() : x(0), y(0), z(0) {}
    PLY(float x, float y, float z) : x(x), y(y), z(z) {}
    float x, y , z;
};



template<class F>
std::vector<PLY> test(const char* name, std::vector<PLY> samples, F f)
{
    using namespace std::literals;
    std::vector<PLY> result;
    result.reserve(samples.size());
    
    auto start = std::chrono::high_resolution_clock::now();
    
    f(result, samples);
    
    auto end = std::chrono::high_resolution_clock::now();
    
    using fns = std::chrono::duration<long double, std::chrono::nanoseconds::period>;
    using fms = std::chrono::duration<long double, std::chrono::milliseconds::period>;
    using fs = std::chrono::duration<long double, std::chrono::seconds::period>;
    
    auto interval = fns(end - start);
    auto time_per_sample = interval / samples.size();
    auto samples_per_second = 1s / time_per_sample;
    
    std::cout << "testing " << name << '\n';
    std::cout << " sample size        : " << samples.size() << '\n';
    std::cout << " time taken         : " << std::fixed << fms(interval).count() << "ms\n";
    std::cout << " time per sample    : " << std::fixed << (interval / samples.size()).count() << "ns\n";
    std::cout << " samples per second : " << std::fixed << samples_per_second << "\n";
    
    return result;
}

struct zero_z_iterator : std::vector<PLY>::const_iterator
{
    using base_class = std::vector<PLY>::const_iterator;
    using value_type = PLY;
    
    using base_class::base_class;
    
    value_type operator*() const {
        auto const& src = base_class::operator*();
        return PLY{ src.x, src.y, 0.0 };
    }
};

template<class Container, class Iter, class TransformFunction>
void assign_transform(Container& target, Iter first, Iter last, TransformFunction func)
{
    struct transform_iterator : Iter
    {
        using base_class = Iter;
        using value_type = typename Iter::value_type;
        
        transform_iterator(Iter base, TransformFunction& f)
        : base_class(base), func(std::addressof(f))
        {}
        
        value_type operator*() const {
            auto const& src = base_class::operator*();
            return (*func)(src);
        }
        TransformFunction* func;
    };
    
    target.assign(transform_iterator(first, func),
                  transform_iterator(last, func));
}


namespace detail {
    
    template<class T>
    struct debug_pointer
    {
        constexpr static std::size_t pointer_digits()
        {
            return sizeof(void*) * 2;
        };
        static constexpr std::size_t width = pointer_digits();
        
        std::ostream& operator()(std::ostream& os) const
        {
            std::uintptr_t i = reinterpret_cast<std::uintptr_t>(p);
            return os << "0x" << std::hex << std::setw(width) << std::setfill('0') << i;
        }
        T* p;
        
        friend
        std::ostream& operator<<(std::ostream& os, debug_pointer const& dp) {
            return dp(os);
        }
        
    };
}

template<class T>
auto debug_pointer(T* p)
{
    return detail::debug_pointer<T>{ p };
}

int main2()
{
    int i;
    std::cout << debug_pointer(&i) << std::endl;
    
    test("transform", std::vector<PLY>(1000000), [](auto& target, auto& source)
         {
             std::transform(source.begin(), source.end(),
                            std::back_inserter(target),
                            [](auto& ply) {
                                return PLY { ply.x, ply.y, ply.z };
                            });
         });
    
    test("copy and reset z", std::vector<PLY>(1000000), [](auto& target, auto& source)
         {
             std::copy(source.begin(), source.end(),
                       std::back_inserter(target));
             for (auto& x : target)
             {
                 x.z = 0;
             }
         });
    
    test("hand_roll", std::vector<PLY>(1000000), [](auto& target, auto& source)
         {
             for(auto& x : source) {
                 target.emplace_back(x.x, x.y, 0.0);
             }
         });
    
    test("assign through custom iterator", std::vector<PLY>(1000000), [](auto& target, auto& source)
         {
             target.assign(zero_z_iterator(source.begin()),
                           zero_z_iterator(source.end()));
         });
    
    test("assign through assign_transform", std::vector<PLY>(1000000), [](auto& target, auto& source)
         {
             assign_transform(target, source.begin(), source.end(),
                              [](auto& from)
                              {
                                  return PLY(from.x, from.y, 0.0);
                              });
         });
    
    
    test("transform", std::vector<PLY>(100000000), [](auto& target, auto& source)
         {
             std::transform(source.begin(), source.end(),
                            std::back_inserter(target),
                            [](auto& ply) {
                                return PLY { ply.x, ply.y, ply.z };
                            });
         });
    
    test("copy and reset z", std::vector<PLY>(100000000), [](auto& target, auto& source)
         {
             std::copy(source.begin(), source.end(),
                       std::back_inserter(target));
             for (auto& x : target)
             {
                 x.z = 0;
             }
         });
    
    test("hand_roll", std::vector<PLY>(100000000), [](auto& target, auto& source)
         {
             for(auto& x : source) {
                 target.emplace_back(x.x, x.y, 0.0);
             }
         });
    
    test("assign through custom iterator", std::vector<PLY>(100000000), [](auto& target, auto& source)
         {
             target.assign(zero_z_iterator(source.begin()),
                           zero_z_iterator(source.end()));
         });
    return 0;
}

#include <unordered_map>
#include <vector>
#include <string>
#include <typeindex>
#include <iostream>

//
// base game object, suplpying polymorphic interface

struct GameObject
{
    virtual void identify_thyself() const = 0;
    
    GameObject() noexcept {}
    GameObject(GameObject&&) noexcept = default;
    GameObject(GameObject const&) noexcept = default;
    GameObject& operator=(GameObject&&) noexcept = default;
    GameObject& operator=(GameObject const&) noexcept = default;
    virtual ~GameObject() = default;
};

// every game should be written in character, or you're not doing it right

template<class T>
struct ye {};

// an object manager which allows static registration of creation functions

struct Manager
{
    using game_object_ptr = std::unique_ptr<GameObject>;
    using create_object_function = std::function<game_object_ptr()>;
    struct static_data
    {
        std::unordered_map<
        std::type_index,
        create_object_function
        > creation_functions;
    };
    
    static static_data& get_static_data()
    {
        static static_data sd {};
        return sd;
    }
    
    template<class T>
    static void register_object(create_object_function creator) {
        auto& statics = get_static_data();
        // do whatever registration is required here
        statics.creation_functions.emplace(typeid(T), std::move(creator));
    }
    
    template<class T>
    static game_object_ptr create_monster(ye<T>)
    {
        // this will throw an exeption if the object wasn't registered
        return get_static_data().creation_functions.at(typeid(T))();
    }
    
};

// introducing: Ye Goblins!

struct Goblin : GameObject
{
    Goblin(std::string name)
    : _name(std::move(name))
    {}
    
    void identify_thyself() const override { std::cout << _name << " the goblin, waiting to a-slit your throat!\n"; }
    static const bool registered;
    std::string _name;
};
const bool Goblin::registered = [] {
    Manager::register_object<Goblin>([]()->Manager::game_object_ptr{
        static const std::vector<std::string> namez {
            "Grunt",
            "Basher",
            "Pulveriser",
            "Squisher"
        };
        static int iname = 0;
        auto next_name = [&] {
            auto s = namez[iname];
            ++iname;
            if (iname >= namez.size()) iname = 0;
            return s;
        };
        
        return std::make_unique<Goblin>(next_name());
    });
    return true;
}();

struct Hero : GameObject
{
    void identify_thyself() const override { std::cout << "Harry the hero, waiting to degoblinise this blessed land!\n"; }
    static const bool registered;
};

const bool Hero::registered = [] {
    Manager::register_object<Hero>([]()->Manager::game_object_ptr{
        return std::make_unique<Hero>();
    });
    return true;
}();

struct appear_verb {};
static constexpr appear_verb appear {};
static constexpr appear_verb appears {};

struct goblinator
{
    auto operator,(appear_verb) const
    {
        std::vector<Manager::game_object_ptr> ye_hoard;
        std::generate_n(std::back_inserter(ye_hoard), 4,
                        [] { return Manager::create_monster(ye<Goblin>()); } );
        return ye_hoard;
    }
};

static constexpr goblinator a_number_of_merry_goblins {};

struct heroinator
{
    auto operator, (appear_verb) const
    {
        return Manager::create_monster(ye<Hero>());
    }
};
static constexpr heroinator our_hero {};

struct identify_thyselfer
{
    void operator, (const Manager::game_object_ptr& ptr) const {
        ptr->identify_thyself();
    }
};
static constexpr identify_thyselfer identify_thyself{};

struct identify_thyselvser
{
    void operator, (std::vector<Manager::game_object_ptr> const& them) const
    {
        for (auto& yeself : them)
        {
            yeself->identify_thyself();
        }
    }
};
static constexpr identify_thyselvser identify_thyselves{};

struct methinkser
{
    template<class T>
    decltype(auto) operator, (T const& musings) const
    {
        return std::cout << musings << '\n';
    }
};
static constexpr methinkser methinks {};


int main3()
{
    //    Manager ye_manager; // not necessary since manager registration is static
    
    
    methinks, "Introducing, the Goblins:";
    auto ye_goblins = (a_number_of_merry_goblins, appear);
    identify_thyselves, ye_goblins ;
    
    methinks, "";
    methinks, "Introducing, the Hero:";
    auto ye_hero = (our_hero, appears);
    identify_thyself, ye_hero;
    
    return 0;
}

template<std::size_t N>
struct immutable_string
{
    using ref = const char (&)[N+1];
    constexpr immutable_string(ref s)
    : s(s)
    {}
    
    constexpr auto begin() const { return (const char*)s; }
    constexpr auto end() const { return begin() + size(); }
    constexpr std::size_t size() const { return N; }
    constexpr ref c_str() const { return s; }
    ref s;
    
    friend std::ostream& operator<<(std::ostream& os, immutable_string s)
    {
        return os.write(s.c_str(), s.size());
    }
};

template<std::size_t NL, std::size_t NR>
std::string operator+(immutable_string<NL> l, immutable_string<NR> r)
{
    std::string result;
    result.reserve(l.size() + r.size());
    result.assign(l.begin(), l.end());
    result.insert(result.end(), r.begin(), r.end());
    return result;
}

template<std::size_t N>
auto make_immutable_string(const char (&s) [N])
{
    return immutable_string<N-1>(s);
}

int main4()
{
    auto x = make_immutable_string("hello, world");
    std::cout << x << std::endl;
    
    auto a = make_immutable_string("foo");
    auto b = make_immutable_string("bar");
    auto c = a + b;
    std::cout << c << std::endl;
    return 0;
    
}

#include <string>
#include <vector>
#include <bitset>
#include <iostream>
#include <iterator>




template<class F>
std::vector<std::string> test(const char* name, std::vector<std::string> samples, F f)
{
    using namespace std::literals;
    std::vector<std::string> result;
    result.reserve(samples.size());
    
    auto start = std::chrono::high_resolution_clock::now();
    
    f(result, samples);
    
    auto end = std::chrono::high_resolution_clock::now();
    
    using fns = std::chrono::duration<long double, std::chrono::nanoseconds::period>;
    using fms = std::chrono::duration<long double, std::chrono::milliseconds::period>;
    using fs = std::chrono::duration<long double, std::chrono::seconds::period>;
    
    auto interval = fns(end - start);
    auto time_per_sample = interval / samples.size();
    auto samples_per_second = 1s / time_per_sample;
    
    std::cout << "testing " << name << '\n';
    std::cout << " sample size        : " << samples.size() << '\n';
    std::cout << " time taken         : " << std::fixed << fms(interval).count() << "ms\n";
    std::cout << " time per sample    : " << std::fixed << (interval / samples.size()).count() << "ns\n";
    std::cout << " samples per second : " << std::fixed << samples_per_second << "\n";
    
    return result;
}
#include <map>
#include <string>
#include <utility>
#include <type_traits>
#include <iostream>
#include <boost/optional.hpp>


template<class Map>
auto maybe_get_impl(Map& map, typename Map::key_type const& key)
{
    using reference_type = std::conditional_t<
    std::is_const<std::remove_reference_t<Map>>::value,
    typename Map::mapped_type const&,
    typename Map::mapped_type&>;
    boost::optional<reference_type> result;
    auto ifind = map.find(key);
    if (ifind != map.end())
    {
        result = ifind->second;
    }
    return result;
}

template<class K, class V, class Comp, class A>
auto maybe_get(std::map<K, V, Comp, A> const& map, K const& key)
{
    return maybe_get_impl(map, key);
}

template<class K, class V, class Comp, class A>
auto maybe_get(std::map<K, V, Comp, A>& map, K const& key)
{
    return maybe_get_impl(map, key);
}

int main5()
{
    std::map<int, std::string> mymap;
    mymap.emplace(1, "hello");
    mymap.emplace(2, "world");
    
    // note: non-const because we're taking a reference from a mutable map;
    std::string part = std::string("goodbye, cruel world");
    
    std::cout << maybe_get(mymap, 1).value_or(part) << std::endl;
    std::cout << maybe_get(mymap, 2).value_or(part) << std::endl;
    std::cout << maybe_get(mymap, 0).value_or(part) << std::endl;
    return 0;
}

#include <fstream>
#include <iostream>

struct id_generator
{
    id_generator()
    {
        std::ifstream ifs("previous_id.txt");
        ifs >> _id;
    }
    
    ~id_generator()
    {
        std::ofstream ofs("previous_id.txt", std::ios_base::out | std::ios_base::trunc);
        ofs << _id;
    }
    
    int operator()() { return _id++; }
    
    int _id = 0;
};

int main6()
{
    id_generator idgen;
    
    std::cout << "next id is: " << idgen() << std::endl;
    std::cout << "next id is: " << idgen() << std::endl;
    
    return 0;
}


struct IntInterface
{
    virtual int getInt() const = 0;
    virtual void setInt(int value) = 0;
};

template<class TrueFalse>
struct IntInterfaceImpl {};

template<>
struct IntInterfaceImpl<std::true_type> : IntInterface
{
    int getInt() const override { return i_; }
    void setInt(int value) override { i_ = value; }
    
    int i_;
};

struct DoubleInterface
{
    virtual double getDouble() const = 0;
    virtual void setDouble(double value) = 0;
};

template<class TrueFalse>
struct DoubleInterfaceImpl {};

template<>
struct DoubleInterfaceImpl<std::true_type> : DoubleInterface
{
    double getDouble() const override { return i_; }
    void setDouble(double value) override { i_ = value; }
    
    double i_;
};


enum type {
    is_int,
    is_double
};

template<type T>
struct Implementation
: IntInterfaceImpl<std::integral_constant<bool, T == is_int>>
, DoubleInterfaceImpl<std::integral_constant<bool, T == is_double>>
{
    
};


int main7()
{
    Implementation<type::is_int> i {};
    i.setInt(6);
    int a = i.getInt();
    
    Implementation<type::is_double> d {};
    d.setDouble(6.0);
    int b = d.getDouble();
    return 0;
    
}

struct triangly_thing { int i; };
struct rectangly_thing { int i; };

struct shape_visitor
{
    virtual void operator()(triangly_thing thing) const
    {
        // do tiangly things
    }
    
    virtual void operator()(rectangly_thing thing) const
    {
        // do tiangly things
    }
};

struct Shape
{
    int common_variable;
    
    virtual void visit(shape_visitor const& visitor)
    {
        
    }
};


struct Triangle: Shape
{
    triangly_thing my_triangly_thing;
    
    void visit(shape_visitor const& visitor) override
    {
        visitor(my_triangly_thing);
    }
};

struct Rectangle: Shape
{
    rectangly_thing my_rectangly_thing;
    
    void visit(shape_visitor const& visitor) override
    {
        visitor(my_rectangly_thing);
    }
};

struct Domain
{
    Shape* shape;
};

int main8()
{
    Domain domain;
    
    struct my_shape_visitor_type : shape_visitor
    {
        // customise overrides in here
    } my_shape_visitor;
    
    domain.shape->visit(my_shape_visitor);
    
    return 0;
}


#include <iostream>
#include <vector>
#include <string>

void reduce_to_common(std::string& best, const std::string& sample)
{
    best.erase(std::mismatch(best.begin(), best.end(),
                             sample.begin(), sample.end()).first,
               best.end());
    
}

void remove_common_prefix(std::vector<std::string>& range)
{
    if (range.size())
    {
        auto iter = range.begin();
        auto best = *iter;
        for ( ; ++iter != range.end() ; )
        {
            reduce_to_common(best, *iter);
        }
        auto prefix_length = best.size();
        
        std::cout << "best: " << best << "\n";
        for (auto& s : range)
        {
            s.erase(s.begin(), std::next(s.begin(), prefix_length));
        }
    }
}


int main9()
{
    std::vector<std::string> samples = {
        "/home/user/foo.txt",
        "/home/user/bar.txt",
        "/home/baz.txt"
    };
    
    remove_common_prefix(samples);
    
    for (auto& s : samples)
    {
        std::cout << s << std::endl;
    }
    
    return 0;
}





#include <stdexcept>
#include <cassert>
#include <vector>


struct exception_policy
{
    [[noreturn]]
    std::size_t out_of_range() const
    {
        throw std::out_of_range("index_of_element");
    }
};

struct assertion_policy
{
    std::size_t out_of_range() const
    {
        assert(!"out of range");
        return _fallback.out_of_range();
    }
    
    exception_policy _fallback {};
};

struct zero_policy
{
    std::size_t out_of_range() const
    {
        return 0;
    }
};

template<class T, class A, class Policy = exception_policy>
std::size_t index_of_element(std::vector<T, A> const& vec,
                             typename std::vector<T, A>::const_pointer pitem,
                             Policy policy = Policy{})
{
    auto pbegin = vec.data();
    auto pend = pbegin + vec.size();
    if (pitem < pbegin or pitem > pend)
    {
        return policy.out_of_range();
    }
    else
    {
        return std::distance(pbegin, pitem);
    }
}

template<class T, class A, class Policy = exception_policy>
std::size_t index_of_element(std::vector<T, A> const& vec,
                             typename std::vector<T, A>::const_reference item, Policy policy = Policy{})
{
    return index_of_element(vec, std::addressof(item), policy);
}

int main10()
{
    std::vector<int> v = { 1, 2, 3, 4, 5, 6, 7, 8 };
    auto px = std::addressof(v[5]);
    auto& rx = *px;
    
    try {
        auto i = index_of_element(v, rx);
        assert(i == 5);
    }
    catch(...)
    {
        assert(!"should not throw");
    }
    
    try {
        auto i = index_of_element(v, px);
        assert(i == 5);
    }
    catch(...)
    {
        assert(!"should not throw");
    }
    
    auto ry = v[1000];
    try {
        auto i = index_of_element(v, ry);
        assert(!"should have thrown");
    }
    catch(std::out_of_range const& e)
    {
        // success
    }
    catch(...)
    {
        assert(!"should not throw this");
    }
    
    auto i = index_of_element(v, ry, zero_policy());
    assert(i == 0);
    
    return 0;
}

namespace detail {
    template<class T>
    struct range
    {
        constexpr range(T first, T last)
        : _begin(first), _end(last)
        {}
        
        constexpr T begin() const { return _begin; }
        constexpr T end() const { return _end; }
        
        template<class U>
        constexpr bool contains(const U& u) const
        {
            return _begin <= u and u <= _end;
        }
        
    private:
        T _begin;
        T _end;
    };
    
    template<class...Ranges>
    struct ranges
    {
        constexpr ranges(Ranges...ranges) : _ranges(std::make_tuple(ranges...)) {}
        
        template<class U>
        struct range_check
        {
            template<std::size_t I>
            bool contains_impl(std::integral_constant<std::size_t, I>,
                               const U& u,
                               const std::tuple<Ranges...>& ranges) const
            {
                return std::get<I>(ranges).contains(u)
                or contains_impl(std::integral_constant<std::size_t, I+1>(),u, ranges);
            }
            
            bool contains_impl(std::integral_constant<std::size_t, sizeof...(Ranges)>,
                               const U& u,
                               const std::tuple<Ranges...>& ranges) const
            {
                return false;
            }
            
            
            constexpr bool operator()(const U& u, std::tuple<Ranges...> const& ranges) const
            {
                return contains_impl(std::integral_constant<std::size_t, 0>(), u, ranges);
            }
        };
        
        template<class U>
        constexpr bool contains(const U& u) const
        {
            range_check<U> chck {};
            return chck(u, _ranges);
        }
        
        std::tuple<Ranges...> _ranges;
    };
}

template<class T>
constexpr auto range(T t) { return detail::range<T>(t, t); }

template<class T>
constexpr auto range(T from, T to) { return detail::range<T>(from, to); }

constexpr auto range(const char (&s)[4])
{
    return range(s[0], s[2]);
}

template<class...Rs>
constexpr auto ranges(Rs...rs)
{
    return detail::ranges<Rs...>(rs...);
}

int main11()
{
    std::cout << range(1,7).contains(5) << std::endl;
    std::cout << range("a-f").contains('b') << std::endl;
    
    auto az = ranges(range('a'), range('z'));
    std::cout << az.contains('a') << std::endl;
    std::cout << az.contains('z') << std::endl;
    std::cout << az.contains('p') << std::endl;
    
    auto rs = ranges(range("a-f"), range("p-z"));
    for (char ch = 'a' ; ch <= 'z' ; ++ch)
    {
        std::cout << ch << rs.contains(ch) << " ";
    }
    std::cout << std::endl;
    
    return 0;
}

#include <utility>
#include <type_traits>

namespace std {
    template<class...> struct disjunction : std::false_type { };
    template<class B1> struct disjunction<B1> : B1 { };
    template<class B1, class... Bn>
    struct disjunction<B1, Bn...>
    : std::conditional_t<B1::value != false, B1, disjunction<Bn...>>  { };
}

struct Foo {};
struct Bar {};
int a(Foo&) { std::cout << "a on Foo\n"; return 0; }
int a(Bar&) { std::cout << "a on Bar\n"; return 1; };
int b(Foo&) { std::cout << "b on Foo\n"; return 0; }
int b(Bar&) { std::cout << "b on Bar\n"; return 1; };


template<class F, class Sig> struct any_method;

template<class F, class Ret, class... Args>
struct any_method<F,Ret(Args...)>
{
    F f;
    template<class T>
    static Ret invoker(any_method const& self, void* data, Args... args)
    {
        return self.f(*static_cast<T*>(data), std::forward<Args>(args)...);
    }
    using invoker_type = Ret (any_method const&, void*, Args...);
};

template<class F> struct deduce_base_sig;

template<class Ret, class Arg1, class...Args>
struct deduce_base_sig<Ret(Arg1, Args...)>
{
    using type = Ret(Args...);
};

template<class Sig, class F>
auto make_any_method(F&& f)
{
    return
    any_method<std::decay_t<F>,Sig>
    {
        std::forward<F>(f)
    };
}

template<class T> struct class_tag {};

template<class...OperationsToTypeErase>
struct jump_table
{
    std::tuple<typename OperationsToTypeErase::invoker_type*...> operations = {};
    
    template<class T>
    static const jump_table* create(class_tag<T> tag)
    {
        static const jump_table table { tag };
        return std::addressof(table);
    }

private:
    template<class T>
    jump_table(class_tag<T>)
    : operations((OperationsToTypeErase::template invoker<T>)...)
    {}
    
};


template<class...OperationsToTypeErase>
struct super_any {
    void* data;
    std::tuple<typename OperationsToTypeErase::invoker_type*...> operations = {};
    
    template<class T, class ContainedType = std::decay_t<T>>
    super_any(T&& t)
    : data(std::addressof(t))
    , operations((OperationsToTypeErase::template invoker<ContainedType>)...)
    {}
    
    template<class T, class ContainedType = std::decay_t<T>>
    super_any& operator=(T&& t) {
        data = std::addressof(t);
        operations = { (OperationsToTypeErase::template invoker<ContainedType>)... };
        return *this;
    }
};

template<class...Ops, class F, class Sig,
// SFINAE filter that an op matches:
std::enable_if_t< std::disjunction< std::is_same<Ops, any_method<F,Sig>>... >{}, int> = 0
>
auto operator->*( super_any<Ops...>& a, any_method<F,Sig> f) {
    auto fptr = std::get<typename any_method<F,Sig>::invoker_type*>(a.operations);
    return [fptr,f, &a](auto&&... args) mutable {
        return fptr(f, a.data, std::forward<decltype(args)>(args)...);
    };
}

    static auto invoke_a = make_any_method<int()>([](auto&& object){ return a(object); });
    static auto invoke_b = make_any_method<int()>([](auto&& object){ return b(object); });
    using ab_interface = super_any<decltype(invoke_a), decltype(invoke_b)>;
    

struct handle
{
    using jump_table_type = jump_table<decltype(invoke_a), decltype(invoke_b)>;
    jump_table_type const* jump_table_ = nullptr;
    void * pobject_ = nullptr;
    
    template<class T>
    handle(T& t)
    : jump_table_ (jump_table_type::create(class_tag<T>()))
    , pobject_(std::addressof(t))
    {}
    
};

int main20()
{
    
    Foo f;
    Bar fb;
    auto ac = handle(f);
    
//    ac.a();
//    ac.b();
    
    auto ab = handle(fb);
//    ab.a();
//    ab.b();
    return 0;
}


#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>
#include <iomanip>
#include <regex>

bool IsValid1(std::string const& rString)
{
    static const std::regex regex("[0-9A-Z<]+");
    
    return std::regex_match(rString, regex);
}

bool IsValid2(std::string const& rString)
{
    for (std::string::size_type i = 0; i < rString.size(); ++i)
        if (!std::isupper(rString[i]))
            if (!std::isdigit(rString[i]))
                if (rString[i] != '<')
                    return false;
    return true;
}

auto make_samples = []()
{
    std::vector<std::string> result;
    result.reserve(100000);
    std::generate_n(std::back_inserter(result), 100000, []
                    {
                        if (rand() < (RAND_MAX / 2))
                        {
                            return std::string("ABCDEF34<63DFGS");
                        }
                        else
                        {
                            return std::string("ABCDEF34<63DfGS");
                        }
                    });
    return result;
};

int main() {
    auto samples = make_samples();
    
    auto time = [](const char* message, auto&& func)
    {
        clock_t tStart = clock();
        auto result = func();
        clock_t tEnd = clock();
        std::cout << message << " yields " << result << " in " << std::fixed << std::setprecision(2) << (double(tEnd - tStart) / CLOCKS_PER_SEC) << '\n';
    };
    

    
    time("regex method: ", [&]()->std::size_t
         {
             return std::count_if(samples.begin(), samples.end(), IsValid1);
         });
    
    time("search method: ", [&]()->std::size_t
         {
             return std::count_if(samples.begin(), samples.end(), IsValid2);
         });
}
