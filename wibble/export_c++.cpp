#include "export_c++.h"

std::complex<double> foo(const std::complex<double> a){
    return a;
}


struct complex_proxy proxy_foo(struct complex_proxy a)
{
    auto result = foo({a.real, a.imaginary});
    return { result.real(), result.imag() };
}
