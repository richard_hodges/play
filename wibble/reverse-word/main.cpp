#include <boost/variant.hpp>
#include <boost/optional.hpp>
#include <string>
#include <iostream>
#include <type_traits>
#include <iomanip>
#include <cstdlib>

namespace notstd {
    using boost::variant;
    using boost::optional;
    struct monostate
    {
    };

    using std::integral_constant;
    using std::enable_if_t;
    using std::is_integral_v;
    using std::decay_t;
    using std::is_same_v;
    using std::string;

    template<class...Args>
    decltype(auto) visit(Args&& ...args)
    {
        return boost::apply_visitor(std::forward<Args>(args)...);
    }

    template<class X, class Y>
    using decay_common =
    integral_constant
        <
            bool,
            is_same_v
                <
                    decay_t<X>,
                    decay_t<Y>
                >
        >;

    auto maybe_long(std::string const& s) -> optional<long>
    {
        optional<long> result;
        std::size_t len = 0;
        try
        {
            long l = std::stol(s, &len, 10);
            if (len == s.size())
                result = l;
        }
        catch(...) {

        }
        return result;
    }

    auto maybe_double(std::string const& s) -> optional<double>
    {
        optional<double> result;
        std::size_t len = 0;
        try
        {
            double l = std::stod(s, &len);
            if (len == s.size())
                result = l;
        }
        catch(...) {

        }
        return result;
    }

    using value_storage_type = variant<monostate, string, long, double>;

    struct addition_operation {
        template<class L, class R>
        void operator()(L& l, R const& r) const
        {
            throw "not implemented";
        }

        template<class R>
        void operator()(monostate&, R const& r) const
        {
            // nop
        }

        void operator()(long& l, monostate const& r) const
        {
            // nop
        }

        void operator()(long& l, long const& r) const
        {
            l += r;
        }

        void operator()(long& l, double const& r) const
        {
            target = double(l) + r;
        }

        void operator()(long& l, std::string const& r) const
        {
            if(auto opt_r = maybe_long(r))
            {
                (*this)(l, opt_r.value());
            }
            else if (auto opt_r = maybe_double(r))
            {
                (*this)(l, opt_r.value());
            }
            else {
                //nop
            }
        }


        value_storage_type& target;
    };

    struct value
    {
        using storage_type = value_storage_type;

        value()
            : storage_(monostate()) {}

        template
            <
                class T,
                enable_if_t<is_integral_v<T>> * = nullptr
            >
        explicit value(T&& l)
            : storage_(long(l)) {}

        explicit value(double l)
            : storage_(l) {}

        explicit value(string&& s)
            : storage_(move(s)) {}

        explicit value(string const& s)
            : storage_(s) {}

        // assignment
        template
            <
                class T,
                enable_if_t<is_integral_v<T>> * = nullptr
            >
        value& operator =(T l)
        {
            storage_ = long(l);
            return *this;
        }

        value& operator =(double const& l)
        {
            storage_ = l;
            return *this;
        }

        value& operator =(std::string s)
        {
            storage_ = std::move(s);
            return *this;
        }

        value& reset()
        {
            storage_ = monostate();
            return *this;
        }

        // arithmetic

        value& operator+=(value const& other)
        {
            visit(addition_operation{storage_}, storage_, other.storage_);
            return *this;
        }

        const storage_type& get_storage() const { return storage_; }

        storage_type storage_;
    };

    std::ostream& operator <<(std::ostream& os, value const& val)
    {
        visit([&](auto&& x)
              {
                  if constexpr(decay_common<monostate, decltype(x)>())
                  {
                      os << "null";
                  }
                  else if constexpr(decay_common<string, decltype(x)>())
                  {
                      os << std::quoted(x);
                  }
                  else
                  {
                      os << x;
                  }
              }, val.get_storage());

        return os;
    }



    std::istream& operator >>(std::istream& is, value& val)
    {
        char ch;
        if (not (is >> ch)) goto error;
        is.putback(ch);
        if (ch == '"')
        {
            std::string buffer;
            if (not (is >> std::quoted(buffer))) goto error;
            val = std::move(buffer);
        }
        else
        {
            std::string buffer;
            if (not(is >> buffer)) goto error;
            if (buffer == "null")
                val.reset();
            else if (auto ol = maybe_long(buffer))
            {
                val = ol.value();
            }
            else if (auto od = maybe_double(buffer))
            {
                val = od.value();
            }
            else
            {
                val.reset();
            }
        }
        goto done;
        error:
        val.reset();

        done:
        return is;
    }


}

void arithmetic_test(notstd::value const& val, notstd::value& (notstd::value::* op)(notstd::value const&))
{
    using notstd::value;
    using namespace std::literals;
    std::cout << "op=null : " << val << " -> " << (value(val).*op)(notstd::value()) << std::endl;
    std::cout << "op=1 : " << val << " -> " << (value(val).*op)(notstd::value(1)) << std::endl;
    std::cout << "op=1.1 : " << val << " -> " << (value(val).*op)(notstd::value(1.1)) << std::endl;
    std::cout << "op=\"foo\" : " << val << " -> " << (value(val).*op)(notstd::value("foo"s)) << std::endl;
}

int main()
{
    std::stringstream ss;

    using namespace notstd;
    value v;

    std::cout << v << std::endl;

    v = 1;
    std::cout << v << std::endl;

    v = 1.1;
    std::cout << " " << v;
    ss << v;
    v.reset();
    ss >> v;
    std::cout << " - " << v << std::endl;

    v = 1;
    std::cout << " " << v;
    ss.str("");
    ss.clear();
    ss << v;
    v.reset();
    ss >> v;
    std::cout << " - " << v << std::endl;

    v = "foo";
    std::cout << " " << v;
    ss.str("");
    ss.clear();
    ss << v;
    v.reset();
    ss >> v;
    std::cout << " - " << v << std::endl;

    v = value();
    std::cout << " " << v;
    ss.str("");
    ss.clear();
    ss << v;
    v.reset();
    ss >> v;
    std::cout << " - " << v << std::endl;

    arithmetic_test(value(), &value::operator+=);

    v += value(1);
    std::cout << v << std::endl;



}