//
// c and c++
//
struct complex_proxy
{
    double real;
    double imaginary;
};

#ifdef __cplusplus
extern "C" {
#endif

struct complex_proxy proxy_foo(struct complex_proxy a);

#ifdef __cplusplus
}
#endif

//
// c++ only
//
#ifdef __cplusplus
#include <complex>

std::complex<double> foo(const std::complex<double> a);

#else

#include <complex.h>

double _Complex c_foo(const double _Complex a);

#endif

