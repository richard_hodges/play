#include "export_c++.h"

double _Complex c_foo(const double _Complex a)
{

    struct complex_proxy a_proxy = { creal(a), cimag(a) };
    struct complex_proxy result = proxy_foo(a_proxy);
    return result.real + result.imaginary * I;
}