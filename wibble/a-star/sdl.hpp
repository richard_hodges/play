//
// Created by Richard Hodges on 16/10/2017.
//

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_test_font.h>

struct sdl_initialisation
{
    sdl_initialisation(int flags = SDL_INIT_VIDEO)
        : init_result(SDL_Init(flags))
    {
        if (init_result < 0)
            throw_error("init SDL");
    }

    ~sdl_initialisation()
    {
        if (init_result)
            SDL_Quit();
    }

    static void throw_error(std::string const& context)
    {
        std::string message = "SDL Error : " + context;
        if (auto p = SDL_GetError())
        {
            message += " : ";
            message += p;
        }
        throw std::runtime_error(std::move(message));
    }

    int init_result;
};


struct sdl_surface_deleter
{
    void operator()(SDL_Surface* p) const noexcept {
        SDL_FreeSurface(p);
    }
};
using sdl_surface_ptr = std::unique_ptr<SDL_Surface, sdl_surface_deleter>;

struct sdl_window_deleter
{
    void operator()(SDL_Window* p) const noexcept {
        SDL_DestroyWindow(p);
    }
};

using sdl_window_ptr = std::unique_ptr<SDL_Window, sdl_window_deleter>;

template<class...Args>
auto sdl_create_window(Args&&...args) -> sdl_window_ptr
{
    using namespace std::literals;

    auto result = sdl_window_ptr(SDL_CreateWindow(std::forward<Args>(args)...));
    if (not result.get())
    {
        throw std::runtime_error("sdl error : sdl_create_window : "s + SDL_GetError());
    }
    return result;
}

namespace sdl {

    class renderer
    {
        struct deleter
        {
            void operator ()(SDL_Renderer *r) const noexcept
            {
                SDL_DestroyRenderer(r);
            }
        };

        using implementation_type = std::unique_ptr<SDL_Renderer, deleter>;

        implementation_type impl_;
    public:

        explicit renderer(std::nullptr_t)
            : impl_(nullptr, deleter())
        {}

        explicit renderer(SDL_Renderer* r)
            : impl_(r, deleter())
        {}

        SDL_Renderer* release()
        {
            return impl_.release();
        }

        void reset(SDL_Renderer* r)
        {
            impl_.reset(r);
        }
    };


}
