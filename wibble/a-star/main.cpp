#include <iostream>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <boost/functional/hash.hpp>
#include <stdexcept>
#include <queue>
#include <limits>
#include <cmath>
#include <iterator>
#include <boost/operators.hpp>
#include <map>
#include "sdl.hpp"
#include <GL/glew.h>

struct universal_hash
{
    template<class T>
    std::size_t operator ()(T&& t) const
    {
        boost::hash<std::decay_t<T>> hasher;
        return hasher(t);
    }
};

struct point
    : boost::equality_comparable<point>,
      boost::less_than_comparable<point>
{
    point()
        : x(0)
        , y(0) {}

    point(int x, int y)
        : x(x)
        , y(y) {}

    // note - points are ordered in ascending y then ascending x
    auto as_tuple() const { return std::tie(y, x); }

    int x, y;
};

bool operator <(point const& l, point const& r)
{
    return l.as_tuple() < r.as_tuple();
}

std::ostream& operator <<(std::ostream& os, const point& p)
{
    return os << "(" << p.x << "," << p.y << ")";
}

std::string to_string(const point& p)
{
    std::ostringstream ss;
    ss << "(" << p.x << "," << p.y << ")";
    return ss.str();
}


bool operator ==(const point& l, const point& r)
{
    return l.as_tuple() == r.as_tuple();
}

std::size_t hash_value(const point& p)
{
    std::size_t seed = 0;
    boost::hash_combine(seed, p.as_tuple());
    return seed;
}

double distance(point const& from, point const& to)
{
    double xd = to.x - from.x;
    double yd = to.y - from.y;
    return std::sqrt(xd * xd + yd * yd);
}

struct map
{
    map()
        : cells(width * height, '.') {}

    std::size_t width  = 10;
    std::size_t height = 10;

    void reset()
    {
        cells.assign(width * height, '.');
    }

    char& at(int x, int y)
    {
        return cells[y * width + x];
    }

    char& at(point const& p)
    {
        return at(p.x, p.y);
    }

    char const& at(point const& p) const
    {
        return at(p.x, p.y);
    }

    char const& at(int x, int y) const
    {
        return cells[y * width + x];
    }

    auto neighbours(point p) const
    {
        std::vector<point> results;

        for (auto dy : {-1, 0, 1})
            for (auto dx : {-1, 0, 1})
            {
                auto candidate = point(p.x + dx, p.y + dy);
                if (candidate == p
                    or candidate.x < 0
                    or candidate.x >= width
                    or candidate.y < 0
                    or candidate.y >= height
                    or at(candidate) != '.')
                    continue;
                results.push_back(candidate);
            }

        return results;
    }

    template<class F>
    decltype(auto) enumerate_cells(F&& f)
    {
        for (int x = 0; x < width; ++x)
            for (int y = 0; y < height; ++y)
                f(x, y);
        return std::forward<F>(f);
    }

    template<class F>
    decltype(auto) enumerate_cells(F&& f) const
    {
        for (int x = 0; x < width; ++x)
            for (int y = 0; y < height; ++y)
                f(point(x, y));
        return std::forward<F>(f);
    }

    std::vector<char> cells;
};

std::ostream& operator <<(std::ostream& os, const map& m)
{
    for (int y = m.height; y--;)
    {
        for (int x = 0; x < m.width; ++x)
        {
            os.put(m.at(x, y));
        }
        os << '\n';
    }
    return os;
}

struct point_and_f_score
{
    point_and_f_score(point const& p, double f) : p(p), f(f) {}

    point p;
    double f;
};


struct closed_set
{
    using value_type = point_and_f_score;
    using storage_type = std::list<value_type>;
    using identity_index_type = std::unordered_map<point, storage_type::iterator, universal_hash>;
    using f_index_type = std::multimap<double, storage_type::iterator>;

    void add(point const& p, double f)
    {
        auto ipos = storage_.emplace(storage_.end(), p, f);
        identity_index_.emplace(p, ipos);
        f_index_.emplace(f, ipos);
    }

    auto remove_lowest_f() -> point
    {
        auto ifpos = f_index_.begin();
        auto ipos = ifpos->second;
        auto p = ipos->p;
        storage_.erase(ipos);
        f_index_.erase(ifpos);
        identity_index_.erase(p);
        return p;
    }

    void remove(point const& p)
    {
        auto ipoint = identity_index_.find(p);
        if (ipoint != identity_index_.end()) return;

        auto ipos = ipoint->second;
        remove_order(ipos);
        identity_index_.erase(ipoint);
        storage_.erase(ipos);
    }


    void remove_order(storage_type::iterator iter)
    {
        auto rng = f_index_.equal_range(iter->f);
        for (auto current = rng.first ; current != rng.second ; ++current)
        {
            if (current->second == iter) {
                f_index_.erase(current);
                break;
            }
        }
    }

    void reorder(storage_type::iterator iter, double f)
    {
        remove_order(iter);
        f_index_.emplace(f, iter);
    }

    void update_f(point const& p, double f)
    {
        remove(p);
        add(p, f);
    }

    bool empty() const {
        return storage_.empty();
    }


    storage_type storage_;
    identity_index_type identity_index_;
    f_index_type f_index_;
};


struct a_star_search
{
    a_star_search(map const& m)
        : m(m) {}

    using position_type = point;
    using score_type = double;

    struct position_info
    {
        score_type g         = std::numeric_limits<score_type>::infinity();
        score_type f         = std::numeric_limits<score_type>::infinity();
        point      came_from = position_type();
    };

private:
    map const& m;

    using point_f_score = std::tuple<point, double>;


    std::unordered_set<point, universal_hash>                closed_set;
    std::vector<point>                                       open_set;
    std::unordered_map<point, position_info, universal_hash> position_info_;

    void reset(point start)
    {
        closed_set.clear();
        open_set.push_back(start);
        position_info_.clear();
        position_info_[start].g = 0;
    }

    auto reconstruct_path(point start, point goal)
    {
        auto               current = goal;
        std::vector<point> result;
        while (current != start)
        {
            result.push_back(current);
            auto iter = position_info_.find(current);
            if (iter == position_info_.end()) break;
            current = iter->second.came_from;
        }
        result.push_back(current);

        reverse(begin(result), end(result));
        return result;
    }


public:
    auto operator ()(point start, point goal) -> std::vector<point>
    {
        reset(start);

        while (not open_set.empty())
        {
            auto lowest_f = [&](auto&& l, auto&& r)
            {
                return position_info_[l].f < position_info_[r].f;
            };
            auto i        = min_element(begin(open_set), end(open_set), lowest_f);
            auto current  = std::move(*i);
            std::swap(*i, open_set.back());
            open_set.pop_back();
            closed_set.insert(current);
            if (current == goal)
            {
                return reconstruct_path(start, goal);
            }

            auto& current_info = position_info_[current];

            for (auto neighbour : m.neighbours(current))
            {
                if (closed_set.count(neighbour))
                    continue;
                if (not any_of(begin(open_set), end(open_set), [&](auto&& x) { return x == neighbour; }))
                    open_set.push_back(neighbour);
                auto& neighbour_info = position_info_[neighbour];
                auto tentative_score = current_info.g + distance(current, neighbour);
                if (tentative_score >= neighbour_info.g)
                    continue;        // This is not a better path.
                neighbour_info.came_from = current;
                neighbour_info.g         = tentative_score;
                auto heuristic_cost_estimate = [](auto&& neighbour, auto&& goal)
                {
                    return distance(neighbour, goal);
                };
                neighbour_info.f = neighbour_info.g + heuristic_cost_estimate(neighbour, goal);

            }
        }


        throw std::runtime_error("no path");
    }

    auto const& position_data() const
    {
        return position_info_;
    }
};

struct position_view
{
    position_view()
        : came_from()
        , f()
        , g() {}

    position_view(a_star_search::position_info const& info)
        : came_from("from: " + to_string(info.came_from))
        , f("f: " + std::to_string(info.f))
        , g("g: " + std::to_string(info.g))
        , dirty_(true) {}

    std::size_t width()
    {
        recompute();
        return width_;
    }

    std::size_t height()
    {
        recompute();
        return height_;
    }

    std::string came_from;
    std::string f;
    std::string g;

private:
    void recompute()
    {
        if (dirty_)
        {
            width_  = std::max(std::max(came_from.size(), f.size()), g.size());
            height_ = 3;
            dirty_  = false;
        }
    }

    bool        dirty_  = false;
    std::size_t height_ = 0;
    std::size_t width_  = 0;
};

std::ostream& operator <<(std::ostream& os, position_view const& r)
{
    return os << r.came_from << ", " << r.f << ", " << r.g;
}


int main()
try
{
    sdl_initialisation sdl_init;
    auto               window = sdl_create_window("Hello",
                                                  SDL_WINDOWPOS_UNDEFINED,
                                                  SDL_WINDOWPOS_UNDEFINED, 640, 480,
                                                  SDL_WINDOW_OPENGL);



    auto renderer = SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED );

    std::cout << "renderer: " << renderer << std::endl;


    bool done = false;
    while (!done)
    {
        SDL_Event event;

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
        SDL_RenderClear(renderer);

        SDL_SetRenderDrawColor(renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
        SDL_RenderDrawLine(renderer, 320, 200, 300, 240);
        SDL_RenderDrawLine(renderer, 300, 240, 340, 240);
        SDL_RenderDrawLine(renderer, 340, 240, 320, 200);
        SDL_RenderPresent(renderer);

        while (SDL_WaitEvent(&event))
        {
            if (event.type == SDL_QUIT)
            {
                done = SDL_TRUE;
            }
        }
    }

    map  m;
    auto reset_map            = [&]
    {
        m.reset();
        for (int x = 0; x < 9; ++x)
            m.at(x, 4) = '-';
    };

    reset_map();
    std::cout << m;

    auto searcher = a_star_search(m);
    auto result   = searcher(point(0, 0), point(5, 7));
    copy(begin(result), end(result), std::ostream_iterator<point>(std::cout, ", "));
    std::cout << std::endl;
    for (auto&& p : result)
        m.at(p) = '*';
    std::cout << m << std::endl;

    reset_map();
    result = searcher(point(0, 6), point(0, 2));
    copy(begin(result), end(result), std::ostream_iterator<point>(std::cout, ", "));
    std::cout << std::endl;
    for (auto&& p : result)
        m.at(p) = '*';
    std::cout << m << std::endl;

    std::map<a_star_search::position_type, position_view> views;
    for (auto&& pos : searcher.position_data())
    {
        views[std::get<0>(pos)] = std::get<1>(pos);
    }


    for (auto&& v : views)
    {
        if (m.at(v.first) == '*')
            std::cout << v.first << " : " << v.second << std::endl;
    }

}
catch (std::exception const& e)
{
    std::cerr << e.what() << std::endl;
    std::exit(100);
}