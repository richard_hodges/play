#define TESTING 1

#include <iostream>
#include <string>
#if TESTING
  #include <sstream>
#endif
#include <cstdint>

using namespace std::literals;

#if TESTING

const char test_data[] =
R"__(baab)__";
#endif

template<class Iter>
Iter super_reduce(Iter org_first, Iter last)
{
    bool changed = true;
    while(changed)
    {
        changed = false;
        auto dest = org_first;

        for (auto first = org_first ; first != last ; )
        {
            auto next = std::find_if(first + 1, last, [&](auto&& c) { return c != *first; });
            auto dist = std::distance(first, next);
            if(dist == 1)
            {
                *dest = *first;
            }
            else
            {
                changed = true;
                if (dist & 1)
                {
                    *dest++ = *first;
                }
            }
            first = next;
        }
        last = dest;
    }
    return last;
}

void algo(std::istream& in, std::ostream& out)
{
    std::string s;
    in >> s;

    s.erase(super_reduce(s.begin(), s.end()), s.end());

    if (s.empty())
        out << "Empty String";
    else
        out << s;
    out << std::endl;
}

int main()
{
    #if TESTING
    auto in = std::istringstream(test_data);
    #else
    auto& in = std::cin;
    #endif

    algo(in, std::cout);

}