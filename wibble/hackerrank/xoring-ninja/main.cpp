#define TESTING 1
#if TESTING
  #include <sstream>
#endif

#include <iostream>
#include <vector>
#include <unordered_map>

#include <boost/stacktrace.hpp>
#if TESTING

const char test_data[] =
               R"__(
1
3
1 2 3
)__";
#endif


constexpr int MOD = 1000000007;

template<class T>
void read(std::istream& in, std::vector<T>& vec)
{
    for (auto&& v : vec)
    {
        in >> v;
    }
}

//
// 000   0     = 00^00 = 00
// 001   1     = 01    = 01
// 010     2   = 10    = 10
// 011   1^2   = 01^10 = 11
// 100       3 = 11    = 11
// 101   1^  3 = 01^11 = 10
// 110     2^3 = 10^11 = 01
// 111   1^2^3 = 01^10^11 = 00

struct compute_xor_algo
{
    compute_xor_algo(std::vector<int> const& a) : a(a) {}
    int recurse(int set_id)
    {
        return (*this)(set_id);
    }

    int operator()(int set_id)
    {
        auto iter = cache.find(set_id);
        if (iter != cache.end())
            return iter->second;

        int result = 0;
        int i = 0;
        while (set_id)
        {
            if(set_id & 1)
                result ^= a[i];
            ++i;
            set_id >>= 1;
        }
        cache[set_id] = result;
        return result;
    }

    std::vector<int> const& a;
    std::unordered_map<int, int> cache;
};

void algo(std::istream& in, std::ostream& out)
{
    std::cout << boost::stacktrace::stacktrace() << std::endl;
    int n;
    in >> n;

    auto a = std::vector<int>(n);
    read(in, a);


    auto compute_xor = compute_xor_algo(a);

    auto pow2 = [](int n)
    {
        return 1 << n;
    };

    auto result = 0;
    result += compute_xor(pow2(n)-1);

    out << result % MOD;

}

void run_tests(std::istream& in, std::ostream& out)
{
    int n;
    in >> n;

    while (n--)
    {
        algo(in, out);
    }

}

int main()
{
    #if TESTING
    auto in = std::istringstream(test_data);
    #else
    auto& in = std::cin;
    #endif

    run_tests(in, std::cout);

}