#define TESTING 1

#include <iostream>
#include <string>
#if TESTING
  #include <sstream>
#endif
#include <cstdint>

using namespace std::literals;

#if TESTING

const char test_data[] =
R"__(
4
12 15
2 3
8 13
9 25
)__";
#endif

bool has_bit(const std::uint32_t x, int bitnum)
{
    return (x & (1 << bitnum)) != 0;
}

constexpr std::uint32_t mask_here_down(int bitnum)
{
    std::uint32_t result = 0;
    while (bitnum--)
    {
        result |= std::uint32_t(1) << bitnum;
    }
    return result;
}

void algo(std::istream& in, std::ostream& out)
{
    std::uint32_t a,b;
    in >> a >> b;

    for (int bit = 32 ; bit-- ; )
    {
        if (has_bit(b, bit) and not has_bit(a, bit))
        {
            std::cout << (a & ~mask_here_down(bit)) << std::endl;
            break;
        }
    }



}

void run_tests(std::istream& in, std::ostream& out)
{
    int n;
    in >> n;

    while (n--)
    {
        algo(in, out);
    }

}

int main()
{
    #if TESTING
    auto in = std::istringstream(test_data);
    #else
    auto& in = std::cin;
    #endif

    run_tests(in, std::cout);

}