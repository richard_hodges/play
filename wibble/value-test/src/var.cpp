#include <varvalue/var.hpp>
#include "detail/empty_impl.hpp"

namespace varvalue {

    namespace {
        var_storage string_or_empty(const char *s)
        {
            var_storage result;
            if (s)
                result = std::string(s);
            return result;
        }
    }

    var::var()
        : storage_(empty_value) {}

    var::var(integer_type l)
        : storage_(l) {}

    var::var(float_type f)
        : storage_(f) {}

    var::var(char c)
        : storage_(std::string(1, c)) {}

    var::var(string_type&& s)
        : storage_(std::move(s)) {}

    var::var(string_type const& s)
        : storage_(s) {}

    var::var(const char *s)
        : storage_(string_or_empty(s)) {}

    bool var::empty() const
    {
        return visit(detail::empty_impl(), storage_);

    }
};