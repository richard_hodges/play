//
// Created by Richard Hodges on 20/10/2017.
//

#include <varvalue/repr.hpp>

namespace varvalue {

    auto repr(var const& v) -> detail::repr_impl
    {
        return detail::repr_impl(v.storage());
    }

}