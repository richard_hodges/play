//
// Created by Richard Hodges on 20/10/2017.
//

#include <varvalue/detail/repr_impl.hpp>
#include "repr_stream_impl.hpp"
#include <sstream>

namespace varvalue { namespace detail {

    repr_impl::operator std::string() const
    {
        std::ostringstream ss;
        ss << *this;
        return ss.str();
    }

    std::ostream& operator <<(std::ostream& os, repr_impl const& r)
    {
        visit(repr_stream_impl(os), r.storage_);
        return os;
    }

    std::string to_string(repr_impl const& r)
    {
        return {r};
    }

}}