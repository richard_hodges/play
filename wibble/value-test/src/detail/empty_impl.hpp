//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once

#include <varvalue/config.hpp>

namespace varvalue { namespace detail {

    struct empty_impl
    {
        bool operator ()(empty_type const&) const { return true; }

        template<class T>
        bool operator ()(T const&) const { return false; }

    };
}}