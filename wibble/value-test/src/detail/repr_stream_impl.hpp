//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once

#include <varvalue/config.hpp>
#include <ostream>
#include <iomanip>

namespace varvalue { namespace detail {
    struct repr_stream_impl
    {
        repr_stream_impl(std::ostream& os)
            : os(os) {}

        void operator ()(integer_type l) const { os << l; }

        void operator ()(float_type d) const { os << d; }

        void operator ()(string_type const& s) const { os << std::quoted(s); }

        void operator ()(empty_type const&) const { os << "null"; }

        std::ostream& os;
    };


}}