//
// Created by Richard Hodges on 20/10/2017.
//

#include <gtest/gtest.h>

#include "varvalue.hpp"

TEST(var_basics, emptiness_on_construction)
{
    using varvalue::var;

    auto x = var();

    EXPECT_TRUE(x.empty());

    auto y = var(1);
    EXPECT_FALSE(y.empty());
}
