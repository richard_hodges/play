#include <gtest/gtest.h>
#include <varvalue/var.hpp>
#include <varvalue/repr.hpp>

TEST(var_repr, to_string)
{
    using varvalue::var;

    auto a = var();
    EXPECT_EQ("null", to_string(repr(a)));
    EXPECT_EQ("42", to_string(repr(var(42))));
    EXPECT_EQ("-42", to_string(repr(var(-42))));
    EXPECT_EQ("42.42", to_string(repr(var(42.42))));
    EXPECT_EQ("-42.42", to_string(repr(var(-42.42))));
    EXPECT_EQ(R"__("bongo the bear")__", to_string(repr(var("bongo the bear"))));
    EXPECT_EQ(R"__("42")__", to_string(repr(var("42"))));
}
