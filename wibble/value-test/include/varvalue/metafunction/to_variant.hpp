//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once


#include <varvalue/config.hpp>


namespace varvalue {
    namespace metafunction {
        template<class T>
        struct to_variant;

        template<class...Ts>
        struct to_variant<std::tuple<Ts...>>
        {
            using result = variant<Ts...>;
        };
    }
    template<class T> using ToVariant = typename metafunction::to_variant<T>::result;
}
