//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once

#include <boost/variant.hpp>

namespace varvalue {
    template<class...Ts> using variant = boost::variant<Ts...>;
    template<class F, class...Ts> decltype(auto) visit(F&&f, Ts&&...ts)
    {
        return boost::apply_visitor(std::forward<F>(f), std::forward<Ts>(ts)...);
    };
    struct empty_type {};
    constexpr auto empty_value = empty_type {};

    using integer_type = std::int64_t;
    using float_type = double;
    using string_type = std::string;
}