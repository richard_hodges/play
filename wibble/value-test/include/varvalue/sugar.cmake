sugar_include(detail)
sugar_include(metafunction)

sugar_files(INCLUDE_VARVALUE_HEADERS
        config.hpp
        repr.hpp
        var.hpp
        var_storage.hpp)
list(APPEND ALL_HEADERS ${INCLUDE_VARVALUE_HEADERS})

