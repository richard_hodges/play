//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once

#include <varvalue/config.hpp>
#include <varvalue/var.hpp>
#include <varvalue/detail/repr_impl.hpp>

namespace varvalue {

    /**
     * return an object that conceptually represents the var.
     * This object can either be streamed to an ostream or converted to a string.
     * The output format is designed so that the represntation is both human readable and
     * machine readable.
     */

    auto repr(var const& v) -> detail::repr_impl;

}