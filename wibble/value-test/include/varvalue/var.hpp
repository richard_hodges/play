#pragma once

#include <varvalue/config.hpp>
#include <varvalue/var_storage.hpp>

namespace varvalue {
    template<class T>
    constexpr auto IsIntegralValue = std::is_integral<std::decay_t<T>>::value
                                     and not std::is_same<std::decay_t<T>, bool>::value
                                     and not std::is_same<std::decay_t<T>, char>::value
                                     and not std::is_same<std::decay_t<T>, unsigned char>::value;


    struct var
    {
        /**
         * Empty construction
         */
        var();

        /** Integer construction
         *
         * @tparam T
         * @param val
         */
        template<class T, std::enable_if_t<IsIntegralValue<T>> * = nullptr>
        explicit var(T val)
            : var(integer_type(val)) {};

        explicit var(integer_type);

        /** float construction
         *
         */
        explicit var(float_type);

        /** String construction
         *
         */
        explicit var(char);

        explicit var(string_type&&);

        explicit var(string_type const&);

        explicit var(const char *);

        /** test whether the var is explicitly empty (rather than simply containing zero or "")
         *
         * @return boolean
         */
        bool empty() const;


        /**
         * access underlying storage
         */
        var_storage const& storage() const { return storage_; }
        var_storage& storage() { return storage_; }
    private:
        var_storage storage_;
    };


}