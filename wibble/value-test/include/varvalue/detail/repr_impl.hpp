//
// Created by Richard Hodges on 20/10/2017.
//

#pragma once

#include <varvalue/config.hpp>
#include <varvalue/var_storage.hpp>

namespace varvalue { namespace detail {

    struct stream_impl
    {

    };

    struct repr_impl
    {
        repr_impl(var_storage const& storage)
            : storage_(storage) {}

        friend std::ostream& operator <<(std::ostream& os, repr_impl const& r);

        friend std::string to_string(repr_impl const& r);

        operator std::string() const;

        var_storage const& storage_;
    };
}}