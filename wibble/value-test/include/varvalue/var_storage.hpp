#include <varvalue/config.hpp>
#include <varvalue/metafunction/to_variant.hpp>

namespace varvalue{

    using var_fundamentals = std::tuple<empty_type, integer_type, float_type, string_type>;
    using var_storage = ToVariant<var_fundamentals>;

}