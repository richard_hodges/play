if (__imbue_with_boost_defined__)
    return()
else ()
    set(__imbue_with_boost_defined__ 1)
endif ()


function(ImbueWithBoost target)
    hunter_add_package(Boost)
    find_package(Boost CONFIG REQUIRED)
    target_link_libraries(${target} PUBLIC Boost::boost)
endfunction()