#include <tuple>
#include <unordered_map>
#include <iostream>
#include <boost/functional/hash.hpp>
#include <chrono>
#include <nlohmann/json.hpp>
#include <deque>
#include <random>

using json = nlohmann::json;

enum suit
{
    hearts,
    diamonds,
    clubs,
    spades
};

char to_string(suit s)
{
    switch (s)
    {
        case suit::hearts:
            return 'H';
        case suit::diamonds:
            return 'D';
        case suit::clubs:
            return 'C';
        case suit::spades:
            return 'S';
    }
}

enum value
{
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine,
    ten,
    jack,
    queen,
    king,
    ace
};

char to_string(value v)
{
    switch (v)
    {
        case value::two:
            return '2';
        case value::three:
            return '3';
        case value::four:
            return '4';
        case value::five:
            return '5';
        case value::six:
            return '6';
        case value::seven:
            return '7';
        case value::eight:
            return '8';
        case value::nine:
            return '9';
        case value::ten:
            return 't';
        case value::jack:
            return 'j';
        case value::queen:
            return 'q';
        case value::king:
            return 'k';
        case value::ace:
            return 'a';
    }
}

struct card
{

    card(int index)
        : index_(index) {}

    suit get_suit() const
    {
        return static_cast<suit>(index_ / 13);
    }

    value get_value() const
    {
        return static_cast<value>(index_ % 13);
    }

    int index() const
    {
        return index_;
    }

    int index_;
};

std::ostream& operator <<(std::ostream& os, card const& c)
{
    return os << to_string(c.get_value()) << to_string(c.get_suit());
}

struct deck
{
    using store_type = std::deque<card>;

    deck()
        : cards_(generate(std::make_integer_sequence<int, 52>()))
    {

    }

    template<int...Is>
    static store_type generate(std::integer_sequence<int, Is...>)
    {
        return store_type
            {
                card(Is)...
            };
    }

    auto begin() const { return cards_.begin(); }

    auto end() const { return cards_.end(); }

    auto begin() { return cards_.begin(); }

    auto end() { return cards_.end(); }


    std::deque<card> cards_;
};

struct random_deck
{
    using store_type = std::vector<int>;

    random_deck(int packs = 1)
        : cards_(generate(std::make_integer_sequence<int, 52>(), packs))
        , total_cards(packs * 52)
    {

    }

    template<int...Is>
    static store_type generate(std::integer_sequence<int, Is...>, int packs)
    {
        return store_type
            {
                (void(Is), packs)...
            };
    }

    template<class Engine>
    card draw(Engine& eng)
    {
        assert(total_cards);
        auto dist = std::uniform_int_distribution<int>(0, total_cards - 1);
        auto n = dist(eng);

        for (int i = 0 ; i < 52 ; ++i)
        {
            auto& left = cards_[i];
            if (left > n) {
                --left;
                --total_cards;
                return card(i);
            }
            else {
                n -= left;
            }
        }
        assert(!"bollocks");
        return card(0);
    }

    bool empty() const { return total_cards == 0; }


    store_type cards_;
    int        total_cards;
};


std::ostream& operator <<(std::ostream& os, deck const& d)
{
    for (auto&& c : d)
    {
        os << c;
    }
    return os;
}


int main()
{
    std::random_device rdev;
    std::default_random_engine reng(rdev());

    for (int i = 0 ; i < 50 ; ++i)
    {
        auto d = random_deck();

        while (not d.empty())
        {
            auto mycard = d.draw(reng);
            std::cout << mycard;
        }
        std::cout << std::endl;
    }
}