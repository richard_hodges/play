/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include <vector>
#include <memory>
#include <type_traits>
#include <mutex>
#include <typeinfo>
#include <typeindex>
#include <set>

template<class T>
void make_noise_of(const T& t) {
    std::cout << "no idea what noise this makes!" << std::endl;
}

template<class T>
void kill_living_thing(T&) {
    std::cout << "this kind of thing is immortal" << std::endl;
}

class animal;

using may_eat_index = std::tuple<std::type_index, std::type_index>;
std::set<may_eat_index> allowed_to_eat_set;

bool may_x_eat_y(const std::type_index& x, const std::type_index& y)
{
    std::clog << "querying: " << x.name() << ", " << y.name() << std::endl;
    return allowed_to_eat_set.count(std::make_tuple(x,y));
}

bool register_x_eats_y(const std::type_index& x, const std::type_index& y)
{
    std::clog << "registering: " << x.name() << ", " << y.name() << std::endl;
    return allowed_to_eat_set.insert(std::make_tuple(x,y)).second;
}


class animal {
    
    
    struct concept;
    using ptr_type = std::unique_ptr<concept>;

    struct concept {
        virtual void make_noise() const = 0;
        virtual void be_killed() = 0;
        virtual ptr_type clone() const = 0;
        virtual void eat(animal&& a) = 0;
        virtual const std::type_info& type() const = 0;
        
    };

    
    
    template<class AnimalModel>
    struct model : concept
    {
        model(AnimalModel am)
        : _model(std::move(am))
        {}
        
        void make_noise() const override
        {
            make_noise_of(_model);
        }
        
        void be_killed() override
        {
            std::unique_lock<std::mutex> l(_m);
            kill_living_thing(_model);
        }
        
        ptr_type clone() const override {
            return generate(_model);
        }
        
        void eat(animal&& a) {
            eat_an_animal(_model, std::move(a));
        }

        const std::type_info& type() const override{
            return typeid(AnimalModel);
        };
        
        AnimalModel _model;
        std::mutex _m;
    };
    
    template<class AnimalModel>
    static ptr_type generate(AnimalModel&& m)
    {
        using animal_type = std::decay_t<AnimalModel>;
        using model_type = model<animal_type>;
        
        return ptr_type(new model_type(std::forward<AnimalModel>(m)));
    }

    
    ptr_type _impl;
    
public:
    
    animal(const animal& other)
    : _impl(other._impl->clone())
    {
    }

    animal(animal&& other) = default;

    animal& operator=(const animal& other)
    {
        animal(other).swap(*this);
        return *this;
    }
    animal& operator=(animal&& other) = default;

    template<class AnimalModel
    , typename = std::enable_if_t< !std::is_base_of<animal, std::decay_t<AnimalModel>>::value >
    >
    animal(AnimalModel&& am)
    : _impl(generate(std::forward<AnimalModel>(am)))
    {}

    void swap(animal& other) noexcept {
        std::swap(_impl, other._impl);
    }
    
    void make_noise() const
    {
        if(_impl) {
            _impl->make_noise();
        }
        else {
            std::cout << "this animal is imprisoned" << std::endl;
        }
    }
    
    void be_killed()
    {
        _impl->be_killed();
    }
    
    void eat(animal& a) {
        if (may_eat(a))
            _impl->eat(std::move(a));
        else {
            std::cout << "i can't eat that!" << std::endl;
        }
    }
    
    bool may_eat(const animal& a) const
    {
        const auto& my_type = _impl->type();
        const auto& his_type = a._impl->type();
        return may_x_eat_y(my_type, his_type);
    }

};

void kill(animal& a) {
    a.be_killed();
}

struct cage {
    void imprison(animal&& a) {
        _animals.push_back(std::move(a));
    }
    
    void poke() const
    {
        for(const auto& a : _animals) {
            a.make_noise();
        }
    }
    
    bool empty() const {
        return _animals.empty();
    }
    
private:
    std::vector<animal> _animals;
};


struct cat {

    cat(std::string name)
    : name(std::move(name))
    {}
    
    cat(const cat& other)
    : name(std::string("son of ") + other.name)
    , lives(9)
    {
    }
    
    cat(cat&& other) = default;
    
    const std::string name;
    int lives = 9;
    cage _stomach;
};

void make_noise_of(const cat& c) {
    std::cout << "meow! my name is " << c.name << " and i have " << c.lives << " lives" << std::endl;
    if (c._stomach.empty()) {
        std::cout << "--i am hungry" << std::endl;
    }
    else {
        std::cout << "in my stomach:" << std::endl;
        c._stomach.poke();
        std::cout << std::endl;
    }
}

void kill_living_thing(cat& c) {
    --c.lives;
}

void eat_an_animal(cat& c, animal&& a)
{
    kill(a);
    c._stomach.imprison(std::move(a));
}


struct duck {
    bool alive = true;
};

void make_noise_of(const duck& d) {
    if (d.alive) {
        std::cout << "quack!!" << std::endl;
    }
    else {
        std::cout << "silence..." << std::endl;
    }
}

void kill_living_thing(duck& d) {
    d.alive = false;
}

void eat_an_animal(duck& c, animal&& a)
{
    kill(a);
}



using namespace std;



auto main() -> int
{
    register_x_eats_y(typeid(cat), typeid(duck));
    
    auto felix = animal(cat("felix"));
    auto bill = animal(duck());
    
    felix.eat(bill);
    felix.make_noise();
    
    auto son_of_felix = felix;
    auto bob = animal(duck());
    bob.eat(son_of_felix);
    
    /*
    felix.make_noise();
    auto son_of_felix = felix;
    son_of_felix.make_noise();
    
    auto bill = animal(duck());
    bill.make_noise();
    
    auto p = animal(std::string("x"));
    p.make_noise();
    p.kill();
    p.make_noise();
    
    felix.kill();
    son_of_felix.kill();
    bill.kill();
    
    felix.make_noise();
    son_of_felix.make_noise();
    bill.make_noise();
    
    cage mycage;
    mycage.imprison(std::move(felix));
    mycage.imprison(std::move(son_of_felix));
    mycage.imprison(std::move(bill));
    mycage.poke();
    felix.make_noise();
     */
    
	return 0;
}

