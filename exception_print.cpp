/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <exception>
#include <stdexcept>
#include <iomanip>
#include <typeinfo>

struct error_a : std::runtime_error {
    error_a(const std::string& problem)
    : runtime_error(std::string("error_a : ") + problem)
    {}
};

struct error_b : std::runtime_error {
    error_b(const std::string& problem)
    : runtime_error(std::string("error_b : ") + problem)
    {}
};

using namespace std;

void multithrow(size_t depth)
{
    if (depth) {
        try {
            std::throw_with_nested( error_a(std::string("depth is " + std::to_string(depth))));
        }
        catch(...)
        {
            multithrow(depth-1);
            throw;
        }
    }
}
namespace detail {
    template<class Formatter>
    struct exception_unwrapper
    {
        exception_unwrapper(const std::exception& e, Formatter fmt)
        : e(e)
        , formatter(std::move(fmt))
        {}
    private:
        friend std::ostream& operator<<(std::ostream& os, const exception_unwrapper& unwrapper)
        {
            return unwrapper(os);
        }
        
        void print(std::ostream& os, const std::exception& e, int depth = 0) const
        {
            formatter(os, e, depth);
            try {
                std::rethrow_if_nested(e);
            }
            catch(const std::exception& e)
            {
                print(os, e, depth + 1);
            }
            catch(...) {}
        }
        
        std::ostream& operator()(std::ostream& os) const {
            print(os, e, 0);
            return os;
        }
        
        const std::exception& e;
        Formatter formatter;
    };
}

struct multiline_indented_numbered
{
    void operator()(std::ostream& os, const std::exception& e, int depth) const {
        if (depth)
        {
            os << "\n " << to_string(depth) << ":";
        }
        os << e.what();
    }
};

struct multiline_because
{
    void operator()(std::ostream& os, const std::exception& e, int depth) const {
        if (depth)
        {
            os << "\n because ";
        }
        os << e.what();
    }
};

struct format_for_csv
{
    void operator()(std::ostream& os, const std::exception& e, int depth) const {
        if (depth)
        {
            os << ',';
        }
        os << std::quoted(e.what());
    }
};

template<class Formatter = format_for_csv>
detail::exception_unwrapper<Formatter> format_exception(const std::exception& e,
                                                        Formatter fmt = Formatter())
{
    return { e, std::move(fmt) };
}

void c()
{
    throw std::logic_error(R"txt(program is "fucked")txt");
}

void b()
{
    try {
        c();
    }
    catch(...)
    {
        std::throw_with_nested(error_b("while in b"));
    }
}

void a()
{
    try {
        b();
    }
    catch(...) {
        std::throw_with_nested(error_a("while in a"));
    }
}

auto main() -> int
{
    try
    {
        a();
    }
    catch(const std::exception& e)
    {
        cout << format_exception(e) << endl;
        cout << endl;
        cout << format_exception(e, multiline_indented_numbered()) << endl;
        cout << endl;
        cout << format_exception(e, multiline_because()) << endl;
        cout << endl;
        cout << format_exception(e,
                                 [](auto& os, const auto& e, int depth) {
                                     if (depth)
                                         os << std::endl << string(depth-1, '-') << '>';
                                     os << std::quoted(e.what());
                                 }) << endl;
        cout << endl;
        cout << format_exception(e,
                                 [](auto& os, const auto& e, int depth) {
                                     if (depth)
                                         os << std::endl << string(depth-1, '-') << '>';
                                     os << typeid(e).name() << " - " << std::quoted(e.what());
                                 }) << endl;
        }
        catch(...)
        {}
        return 0;
        }
