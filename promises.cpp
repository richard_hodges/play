/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <future>
#include <thread>
#include <iostream>
#include <vector>

using namespace std;


struct ForThread_struct {
    int var = 0;
};

const char* MyFunc(struct ForThread_struct for_thread)
{
    if (for_thread.var == 0) {
        throw runtime_error("ERROR");
    }
    return "FINE";
}


int main()
{
    ForThread_struct all_structs[] = {
        { 0 },
        { 1 },
        { 0 },
        { 2 },
        { 0 }
    };

    std::vector<std::future<const char *>> futures;
    
    for (const auto& ts : all_structs)
    {
        futures.push_back(std::async(std::launch::async,
                                     &MyFunc,
                                     ts));
    }

    for (auto& f : futures)
    {
        try
        {
            const char * res = f.get();
            std::cout << res << std::endl;
        }
        catch(exception & e)
        {
            cout<<e.what()<<endl;
        }
    }
}
