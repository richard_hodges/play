/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -O3"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <cstdint>
#include <cstring>

void * clearmem(void* p, std::size_t len)
{
    auto vp = reinterpret_cast<volatile char*>(p);
    while (len--) {
        *vp++ = 0;
    }
    return p;
}

struct A
{
    char sensitive[100];
    
    A(const char* p)
    {
        std::strcpy(sensitive, p);
    }
    
    ~A() {
        clearmem(&sensitive[0], 100);
    }
};

void use_privacy(A a)
{
    auto b = a;
}


int main()
{
    A a("very private");
    use_privacy(a);
}

