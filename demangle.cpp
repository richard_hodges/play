/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <cxxabi.h>
#include <string>
#include <stdexcept>
#include <exception>
#include <typeinfo>

std::string demangle(const char* mangled_name)
{
    using namespace std;

    size_t len = 0;
    int stat = 0;

    struct deleter {
        void operator()(const char* p) const {
            if (p) {
                auto p1 = const_cast<char*>(p);
                free(p1);
            }
        }
    };
    using ptr_type = std::unique_ptr<const char, deleter>;
    auto pname = ptr_type(abi::__cxa_demangle(mangled_name,
                                              nullptr,
                                              &len,
                                              &stat),
                          deleter());

    if (stat)
    {
        switch(stat) {
            case -1:
                throw std::bad_alloc();
                break;
            case -2:
                throw std::logic_error("invalid name: "s + mangled_name);
                break;
            case -3:
                throw std::invalid_argument("demangle");
            default:
                throw std::logic_error("unknown error code "s + to_string(stat));
        }
    }

    return string(pname.get(), len);
}

template<class X, class Y> struct D {};
struct E {};

template <typename T>
struct B {
    typedef B< D<E, E> > C;
};

int main()
{
    using namespace std;

    cout << demangle(typeid(B<int>::C).name()) << endl;

	return 0;
}

