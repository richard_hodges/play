/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
 flags="-O3"
	c++ ${flags} -S -std=c++1y $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#pragma once

#include <iostream>
#include <utility>
#include <cassert>
#include <string>
#include <vector>
#include <iomanip>

namespace cpputil {
    
    // a fully constexpr version of array that allows incomplete
    // construction
    template<size_t N, class T>
    struct array
    {
        // public constructor defers to internal one for
        // conditional handling of missing arguments
        constexpr array(std::initializer_list<T> list)
        : array(list, std::make_index_sequence<N>())
        {
            
        }
        
        constexpr T& operator[](size_t i) noexcept {
            assert(i < N);
            return _data[i];
        }
        
        constexpr const T& operator[](size_t i) const noexcept {
            assert(i < N);
            return _data[i];
        }
        
        constexpr T& at(size_t i) noexcept {
            assert(i < N);
            return _data[i];
        }
        
        constexpr const T& at(size_t i) const noexcept {
            assert(i < N);
            return _data[i];
        }
        
        constexpr T* begin() {
            return std::addressof(_data[0]);
        }
        
        constexpr const T* begin() const {
            return std::addressof(_data[0]);
        }
        
        constexpr T* end() {
            // todo: maybe use std::addressof and disable compiler warnings
            // about array bounds that result
            return &_data[N];
        }
        
        constexpr const T* end() const {
            return &_data[N];
        }
        
        constexpr size_t size() const {
            return N;
        }
        
    private:
        
        T _data[N];
        
    private:
        
        // construct each element from the initialiser list if present
        // if not, default-construct
        template<size_t...Is>
        constexpr array(std::initializer_list<T> list, std::integer_sequence<size_t, Is...>)
        : _data {
            (
             Is >= list.size()
             ?
             T()
             :
             std::move(*(std::next(list.begin(), Is)))
             )...
        }
        {
            
        }
    };
    
    // convenience printer
    template<size_t N, class T>
    inline std::ostream& operator<<(std::ostream& os, const array<N, T>& a)
    {
        os << "[";
        auto sep = " ";
        for (const auto& i : a) {
            os << sep << i;
            sep = ", ";
        }
        return os << " ]";
    }

}


