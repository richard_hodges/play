/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/asm > /dev/null
 mkdir -p ${dirname}/bin > /dev/null
 asmname="${dirname}/asm/${filename}.s"
 filename="${dirname}/bin/${filename}"
 else
 asmname="./${filename}.s"
 filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
 flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>


struct success_marker
{
    success_marker(bool& b)
    : _bool_to_mark(std::addressof(b))
    {}
    
    void mark(bool value) const {
        *_bool_to_mark = value;
    }
    bool* _bool_to_mark;
};

std::istream& operator>>(std::istream& is, success_marker marker)
{
    marker.mark(bool(is));
    return is;
}

success_marker mark_success(bool& b) {
    return success_marker(b);
}

void test(const std::string& test_name, std::istream& input)
{
    bool have_a = false, have_b = false;
    std::string a, b;
    
    input >> std::quoted(a) >> mark_success(have_a) >> std::quoted(b) >> mark_success(have_b);
    
    std::cout << test_name << std::endl;
    std::cout << std::string(test_name.length(), '=') << std::endl;
    std::cout << have_a << " : " << a << std::endl;
    std::cout << have_b << " : " << b << std::endl;
    std::cout << std::endl;
}

int main()
{
    std::istringstream input("\"we have an a but no b\"");
    test("just a", input);

    // reset any error state so the stream can be re-used
    // for another test
    input.clear();

    // put new data in the stream
    input.str("\"the cat sat on\" \"the splendid mat\"");
    // test again
    test("both a and b", input);
    
    return 0;
}

