/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -I${HOME}/local/include"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <map>
#include <iostream>

typedef std::map<std::string, std::string> header_fields_t;

struct HttpRequestHeader
{
    std::string _method;
    std::string _uri;
    std::string _http_version;
    header_fields_t _header_fields;
};

BOOST_FUSION_ADAPT_STRUCT(HttpRequestHeader, _method, _uri, _http_version, _header_fields)

namespace qi = boost::spirit::qi;

template <typename Iterator, typename Skipper = qi::ascii::blank_type>
struct HttpHeaderGrammar: qi::grammar <Iterator, HttpRequestHeader(), Skipper> {
    HttpHeaderGrammar() : HttpHeaderGrammar::base_type(http_header, "HttpHeaderGrammar Grammar") {
        method        = +qi::alpha;
        uri           = +qi::graph;
        http_ver      = "HTTP/" >> +qi::char_("0-9.");
        
        field_key     = +qi::char_("0-9a-zA-Z-");
        field_value   = +~qi::char_("\r\n");
        
        fields = *(field_key >> ':' >> field_value >> qi::lexeme["\r\n"]);
        
        http_header = method >> uri >> http_ver >> qi::lexeme["\r\n"] >> fields;
        
        OCTET = *qi::char_;
        
        BOOST_SPIRIT_DEBUG_NODES((method)(uri)(http_ver)(fields)(http_header))
    }
    
private:
    qi::rule<Iterator, std::string()> OCTET;
    qi::rule<Iterator, std::map<std::string, std::string>(), Skipper> fields;
    qi::rule<Iterator, HttpRequestHeader(), Skipper> http_header;
    // lexemes
    qi::rule<Iterator, std::string()> method, uri, http_ver;
    qi::rule<Iterator, std::string()> field_key, field_value;
};

int main()
{
    typedef std::string::const_iterator It;
    HttpHeaderGrammar<It> httpGrammar;
    
    HttpRequestHeader httpHeader;
    
    std::string str(
                    "CONNECT www.tutorialspoint.com HTTP/1.1\r\n"
                    "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\r\n");
    
    It iter = str.begin(), end = str.end();
    bool r = phrase_parse(iter, end, httpGrammar, qi::ascii::blank, httpHeader);
    
    if (r && iter == end) {
        std::cout << "Parsing succeeded\n";
    } else {
        std::cout << "Parsing failed\n";
        std::cout << "stopped at: \"" << std::string(iter, end) << "\"\n";
    }
    
    std::cout << "Bye... :-) \n\n";
}
