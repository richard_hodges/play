/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <memory>
#include <string>

struct A { virtual ~A(void) { } };
struct B { virtual ~B(void) { } };
struct C : public A, public B { virtual ~C(void) override { } };

struct D
{
    virtual ~D(void) { }
    virtual B *func(void) = 0;
};

struct E : public D
{
    virtual ~E(void) override { }
    virtual C *func(void) override { return nullptr; }
};

struct F : public A, public E
{
    virtual ~F(void) override { }
    C *func(void) override
    {
        m_string = "Why does the act of setting this field cause a crash?";
        
        return nullptr;
    }
    
    std::string m_string;
};

int main(int argc, char **argv)
{
    std::unique_ptr<F> pF(new F());
    (dynamic_cast<D *>(pF.get()))->func();
    pF->func();
    
    return 0;
}
