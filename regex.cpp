//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include<iostream>
#include<regex>
#include<string>
using namespace std;

int main()
{
    bool found;
    cmatch m;
    try
    {
        found = regex_search("<html>blah blah blah </html>",m,regex("<(.*)>(.*)</\\1>"));
        cout << "***whole match***\n";
        cout << "found=" << found << endl;
        cout << m.str() << endl;

        cout << "\n*** parts ***" << endl;
        for (const auto& c : m) {
            cout << c << endl;
        }

    }
    catch(exception & e)
    {
        
        cout<<e.what() << endl;
    }
    
    return 0;
    
    
}

template<unsigned int tries>
struct locker_impl
{
    
};

template<class unsigned attempts> using locker = locker_impl<attempts>;
template<> using locker<0> = std::mutex;
