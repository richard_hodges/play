/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>

using namespace std;

int main()
{
    cin >> noskipws; //noskipws request
    int number; //If this int is replaced with char then it works fine
    while (cin) {
        if (cin >> number) {
            std::cout << number << std::endl;
        }
        else {
            std::cout << "invalid" << std::endl;
        }
        cout << "Enter ctrl + D (ctrl + Z for windows) to set cin stream to end of file " << endl;
    }
    cout << "End of file encountered" << endl;
    
    return 0;
}

