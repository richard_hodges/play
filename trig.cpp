/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
    flags="-O3 -fconstexpr-steps=12712420"
	c++ ${flags} -S -std=c++1z $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" -std=c++1z $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include "cpputil_array.hpp"
#include <cmath>

using namespace std;


namespace trig
{
    constexpr double pi() {
        return M_PI;
    }
    
    template<class T>
    auto constexpr to_radians(T degs) {
        return degs / 180.0 * pi();
    }

    constexpr double factorial(size_t x) {
        double result = 1.0;
        for (int i = 2 ; i <= x ; ++i)
            result *= double(i);
        return result;
    }
    
    constexpr double power(double x, size_t n)
    {
        double result = 1;
        while (n--) {
            result *= x;
        }
        return result;
    }
    
    constexpr double taylor_term(double x, size_t i)
    {
        int powers = 1 + (2 * i);
        double top = power(x, powers);
        double bottom = factorial(powers);
        auto term = top / bottom;
        if (i % 2 == 1)
            term = -term;
        return term;
    }
    
    constexpr double taylor_expansion(double x, size_t terms)
    {
        auto result = x;
        for (int term = 1 ; term < terms ; ++term)
        {
            result += taylor_term(x, term);
        }
        return result;
    }
    
    template<size_t N = 2048>
    struct sin_table : cpputil::array<N, double>
    {
        static constexpr size_t cache_size = N;
        static constexpr double step_size = (pi() / 2) / cache_size;
        static constexpr double _360 = pi() * 2;
        static constexpr double _270 = pi() * 1.5;
        static constexpr double _180 = pi();
        static constexpr double _90 = pi() / 2;

        constexpr sin_table()
        : cpputil::array<N, double>({})
        {
            for(int slot = 0 ; slot < cache_size ; ++slot)
            {
                double val = trig::taylor_expansion(step_size * slot, 20);
                (*this)[slot] = val;
            }
        }

        double checked_interp_fw(double rads) const {
            size_t slot0 = size_t(rads / step_size);
            auto v0 = (slot0 >= this->size()) ? 1.0 : this->at(slot0);
            
            size_t slot1 = slot0 + 1;
            auto v1 = (slot1 >= this->size()) ? 1.0 : this->at(slot1);
            
            auto ratio = (rads - (slot0 * step_size)) / step_size;
            
            return (v1 * ratio) + (v0 * (1.0-ratio));
        }
        
        double interpolate(double rads) const
        {
            rads = std::fmod(rads, _360);
            if (rads < 0)
                rads = std::fmod(_360 - rads, _360);
         
            if (rads < _90) {
                return checked_interp_fw(rads);
            }
            else if (rads < _180) {
                return checked_interp_fw(_90 - (rads - _90));
            }
            else if (rads < _270) {
                return -checked_interp_fw(rads - _180);
            }
            else {
                return -checked_interp_fw(_90 - (rads - _270));
            }
        }
        
        
    };

    double sine(double x)
    {
        if (x < 0) {
            return -sine(-x);
        }
        else {
            constexpr sin_table<> table;
            return table.interpolate(x);
        }
    }
    
    double cosine(double x)
    {
        return sine((M_PI/2) + std::fmod(x, M_PI * 2));
    }
}


static constexpr trig::sin_table<> my_table;

void check(float degs) {
    using namespace std;

    cout << "checking : " << degs << endl;
    auto mysin = trig::sine(trig::to_radians(degs));
    auto stdsin = std::sin(trig::to_radians(degs));
    auto error = stdsin - mysin;
    cout << "mine=" << mysin << ", std=" << stdsin << ", error=" << error << endl;
    cout << endl;
}

auto main() -> int
{
    
    cout << my_table << endl;
    
    check(0.5);
    check(30);
    check(45.4);
    check(90);
    check(151);
    check(180);
    check(195);
    check(89.5);
    check(91);
    check(270);
    check(305);
    check(360);
	return 0;
}

