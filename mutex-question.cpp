//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>

#include <mutex>

// general form of the spin_locker
template<unsigned SPIN_LIMIT, class Mutex>
struct spinner
{
    static void lock(Mutex& m) {
        for (unsigned i = 0 ; i < SPIN_LIMIT ; ++i)
            if (m.try_lock())
                return;
        m.lock();
    }
};

// optmised partial specialisation for zero spins
template<class Mutex>
struct spinner<0, Mutex>
{
    static void lock(Mutex& m) {
        m.lock();
    }

};

template<unsigned SPIN_LIMIT, class Mutex = std::mutex>
class hybrid_lock {

    using spinner_type = spinner<SPIN_LIMIT, Mutex>;
    
public:
    void lock(){
        spinner_type::lock(mMutex);
    }
    
    void unlock(){
        mMutex.unlock();
    }
    
    std::unique_lock<Mutex> make_lock() {
        return std::unique_lock<Mutex>(mMutex);
    }

private:
    Mutex mMutex;
};

// since only the 'spinner' functor object needs specialising there is now no need to specialise the main logic

using namespace std;

auto main() -> int
{
    hybrid_lock<100> m1;
    hybrid_lock<0> m2;
    hybrid_lock<100, std::recursive_mutex> m3;
    hybrid_lock<0, std::recursive_mutex> m4;
    
    auto l1 = m1.make_lock();
    auto l2 = m2.make_lock();
    auto l3 = m3.make_lock();
    auto l4 = m4.make_lock();

    return 0;
}

