/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>

struct exception_swallower
{
    void set_error(std::string msg)
    {
        success = false;
        message = std::move(msg);
    }
    bool success = true;
    std::string message;
};

std::ostream& operator <<(std::ostream& os, const exception_swallower& es)
{
    if (es.success) {
        return os << "success";
    }
    else {
        return os << "failure: " << es.message;
    }
}

template<class Stream, class Op>
exception_swallower perform_op_on_stream(Stream stream, Op op)
{
    exception_swallower ret;
    try {
        if (!stream) {
            throw std::runtime_error("stream didn't open");
        }
        op(stream);
    }
    catch(const std::exception& e)
    {
        ret.set_error(e.what());
    }
    return ret;
}

auto null_op = [](auto& ios)
{
    // do nothing
};

auto error_op = [](auto& ios)
{
    // throw an exception
    throw std::runtime_error("error in stream");
};


int main(int argc, char** argv)
{
    std::cout << perform_op_on_stream(std::ifstream(argv[0]), null_op) << std::endl;
    std::cout << perform_op_on_stream(std::ifstream(argv[0]), error_op) << std::endl;
}