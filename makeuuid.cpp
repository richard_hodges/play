/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y -stdlib=libc++ $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/
#include <iostream>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <algorithm>
#include <vector>

using namespace std;

bool is_minus(char c) {
    return c == '-';
}

void error(const char* str)
{
    cerr << str << endl;
    exit(4);
}

using namespace std;

typedef vector< string > split_vector_type;

auto split_namespaces(std::string source) ->split_vector_type
{
    static const auto delimiter = "::"s;
    split_vector_type result;

    size_t pos = 0;
    std::string token;
    while ((pos = source.find(delimiter)) != std::string::npos) {
        result.push_back(source.substr(0, pos));
        source.erase(0, pos + delimiter.length());
    }
    
    result.push_back(source);
    
    return result;
}

auto main(int argc, const char* const* argv) -> int
{
    if (argc != 2) {
        error("syntax: makeuuid <qualified_classname>");
    }
    auto raw_string = to_string(boost::uuids::random_generator()());
    raw_string.erase(remove_if(begin(raw_string), end(raw_string), is_minus), end(raw_string));
    
    string name = argv[1];
    auto namespaces = split_namespaces(name);
    if (namespaces.size() == 0) {
        error("no class");
    }

    const auto nof_namespaces = namespaces.size() - 1;
    
    auto mangled = boost::replace_all_copy(name, "::", "_n_");
    auto guard = "included_"s + raw_string + "_" + mangled;
    
    cout << "#ifndef " << guard << endl;
    cout << "#define " << guard << endl;
    cout << endl;
    auto sep = "";
    for (size_t i = 0 ; i < nof_namespaces ; ++i) {
        cout << sep << "namespace " << namespaces[i] << " {";
        sep = " ";
    }
    cout << "\n";
    cout << endl;
    cout << "\tstruct " << namespaces.back() << " {\n\t};";
    cout << endl;
    for (size_t i = 0 ; i < nof_namespaces ; ++i) {
        cout << "}";
    }
    cout << "\n";
    cout << endl;
    cout << endl;
    cout << "#endif" << endl;
	return 0;
}

