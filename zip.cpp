/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <vector>
#include <utility>
#include <tuple>
#include <iostream>
#include <iterator>
#include <algorithm>


template<class...Iters>
struct iterators
{
    iterators(Iters... iters) : _iterators { iters... } {}
    
    bool operator==(const iterators& r) const {
        return !(_iterators != r._iterators);
    }
    
    bool operator!=(const iterators& r) const {
        return _iterators != r._iterators;
    }
    
    template<std::size_t...Is>
    auto refs(std::index_sequence<Is...>)
    {
        return std::tie(*std::get<Is>(_iterators)...);
    }
    
    auto operator*() {
        return refs(std::index_sequence_for<Iters...>());
    }
    
    template<std::size_t...Is>
    auto& plus(std::size_t n, std::index_sequence<Is...>)
    {
        using expand = int[];
        void(expand{0,
            ((std::get<Is>(_iterators) += n),0)...
        });
        return *this;
    }
    
    auto& operator+=(std::size_t n) {
        return plus(n, std::index_sequence_for<Iters...>());
    }
    
    auto& operator++() {
        return operator+=(1);
    }
    
    std::tuple<Iters...> _iterators;
};

template<class...Ranges>
auto begins(Ranges&...ranges)
{
    using iters_type = iterators<decltype(std::begin(ranges))...>;
    return iters_type(std::begin(ranges)...);
}

template<class...Ranges>
auto ends(Ranges&...ranges)
{
    using iters_type = iterators<decltype(std::begin(ranges))...>;
    return iters_type(std::end(ranges)...);
}

template<class...Ranges>
struct ranges
{
    ranges(Ranges&...rs)
    : _ranges(rs...)
    {}
    
    template<std::size_t...Is>
    auto make_begins(std::index_sequence<Is...>)
    {
        return begins(std::get<Is>(_ranges)...);
    }
    
    template<std::size_t...Is>
    auto make_ends(std::index_sequence<Is...>)
    {
        return ends(std::get<Is>(_ranges)...);
    }
    
    auto begin() {
        return make_begins(std::index_sequence_for<Ranges...>());
    }
    
    auto end() {
        return make_ends(std::index_sequence_for<Ranges...>());
    }
    
    std::tuple<Ranges&...> _ranges;
};

template<class...Ranges>
auto zip(Ranges&...rs) {
    return ranges<Ranges...>(rs...);
}

auto t2 = [](auto x) { return 2 * x; };

int input[] = {1, 2, 3, 4};
int output[4];
int (*functions[])(int) = { t2, t2, t2, t2 };

void test()
{
    for (auto values : zip(input, functions, output))
    {
        auto &in = std::get<0>(values);
        auto &func = std::get<1>(values);
        auto &out = std::get<2>(values);
        
        out = func(in); // error, i is not declared
        // won't compile:
        // i = func(in);
    }
}

int main()
{
    test();
    std::copy(std::begin(output), std::end(output), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;
    
}
