/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>

constexpr int bit_count(int val)
{
    int count = 0;
    for (int i = 0 ; i < 31 ; ++i) {
        if (val & (1 << i))
            ++count;
    }
    return count;
}

constexpr int next_bitset(int last)
{
    int candidate = last + 1;
    if (bit_count(candidate) & 1)
        return next_bitset(candidate);
    return candidate;
}
enum values
{
    a,
    b = next_bitset(a),
    c = next_bitset(b),
    d = next_bitset(c)
};

int main()
{
    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << c << std::endl;
    std::cout << "d = " << d << std::endl;
}



