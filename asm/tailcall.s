	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z6rerev1NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4, 0x90
__Z6rerev1NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @_Z6rerev1NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp9:
	.cfi_offset %rbx, -40
Ltmp10:
	.cfi_offset %r14, -32
Ltmp11:
	.cfi_offset %r15, -24
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movzbl	(%rbx), %eax
	testb	$1, %al
	jne	LBB0_1
## BB#2:
	shrq	%rax
	jmp	LBB0_3
LBB0_1:
	movq	8(%rbx), %rax
LBB0_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv.exit
	testq	%rax, %rax
	je	LBB0_4
## BB#6:
	leaq	-72(%rbp), %r15
	movl	$1, %edx
	movq	$-1, %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rbx, %r8
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_mmRKS4_
Ltmp0:
	leaq	-48(%rbp), %rdi
	movq	%r15, %rsi
	callq	__Z6rerev1NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp1:
## BB#7:
	movzwl	(%rbx), %eax
	testb	$1, %al
	jne	LBB0_8
## BB#9:
	shrl	$8, %eax
	jmp	LBB0_10
LBB0_4:
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	movq	(%rbx), %rax
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%r14)
	movq	%rax, (%r14)
	movq	$0, 16(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
	jmp	LBB0_5
LBB0_8:
	movq	16(%rbx), %rax
	movb	(%rax), %al
LBB0_10:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEixEm.exit
Ltmp3:
	movsbl	%al, %esi
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp4:
## BB#11:
	movq	-32(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rcx
	movq	%rcx, 8(%r14)
	movq	%rax, (%r14)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-72(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_5:
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_14:
Ltmp2:
	movq	%rax, %rbx
	jmp	LBB0_13
LBB0_12:
Ltmp5:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_13:
	leaq	-72(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp3-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset6
Lset7 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp4-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Lfunc_end0-Ltmp4                ##   Call between Ltmp4 and Lfunc_end0
	.long	Lset9
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z6rerev2NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES5_
	.align	4, 0x90
__Z6rerev2NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES5_: ## @_Z6rerev2NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES5_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp20:
	.cfi_def_cfa_offset 16
Ltmp21:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp22:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp23:
	.cfi_offset %rbx, -40
Ltmp24:
	.cfi_offset %r14, -32
Ltmp25:
	.cfi_offset %r15, -24
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movzbl	(%rbx), %eax
	testb	$1, %al
	jne	LBB1_1
## BB#2:
	shrq	%rax
	jmp	LBB1_3
LBB1_1:
	movq	8(%rbx), %rax
LBB1_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv.exit
	testq	%rax, %rax
	je	LBB1_4
## BB#6:
	leaq	-56(%rbp), %rdi
	movl	$1, %edx
	movq	$-1, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %r8
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_mmRKS4_
	movzwl	(%rbx), %eax
	testb	$1, %al
	jne	LBB1_7
## BB#8:
	shrl	$8, %eax
	jmp	LBB1_9
LBB1_4:
	movq	16(%r15), %rax
	movq	%rax, 16(%r14)
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	movq	%rcx, 8(%r14)
	movq	%rax, (%r14)
	movq	$0, 16(%r15)
	movq	$0, 8(%r15)
	movq	$0, (%r15)
	jmp	LBB1_5
LBB1_7:
	movq	16(%rbx), %rax
	movb	(%rax), %al
LBB1_9:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEixEm.exit
	movb	%al, -25(%rbp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	movzbl	(%r15), %eax
	movq	%rax, %rbx
	shrq	%rbx
	testb	$1, %al
	cmovneq	8(%r15), %rbx
	leaq	1(%rbx), %rcx
Ltmp12:
	leaq	-80(%rbp), %rdi
	leaq	-25(%rbp), %rsi
	movl	$1, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcmm
Ltmp13:
## BB#10:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit.i
	leaq	1(%r15), %rsi
	testb	$1, (%r15)
	cmovneq	16(%r15), %rsi
Ltmp14:
	leaq	-80(%rbp), %rdi
	movq	%rbx, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKcm
Ltmp15:
## BB#11:
Ltmp17:
	leaq	-56(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	movq	%r14, %rdi
	callq	__Z6rerev2NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES5_
Ltmp18:
## BB#12:
	leaq	-80(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB1_5:
	movq	%r14, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB1_15:                                ## %.body
Ltmp16:
	jmp	LBB1_14
LBB1_13:
Ltmp19:
LBB1_14:
	movq	%rax, %rbx
	leaq	-80(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset10 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset10
Lset11 = Ltmp12-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp12
	.long	Lset11
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp12-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset12
Lset13 = Ltmp15-Ltmp12                  ##   Call between Ltmp12 and Ltmp15
	.long	Lset13
Lset14 = Ltmp16-Lfunc_begin1            ##     jumps to Ltmp16
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp17-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset15
Lset16 = Ltmp18-Ltmp17                  ##   Call between Ltmp17 and Ltmp18
	.long	Lset16
Lset17 = Ltmp19-Lfunc_begin1            ##     jumps to Ltmp19
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp18-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset18
Lset19 = Lfunc_end1-Ltmp18              ##   Call between Ltmp18 and Lfunc_end1
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI2_0:
	.space	16
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp63:
	.cfi_def_cfa_offset 16
Ltmp64:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp65:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$144, %rsp
Ltmp66:
	.cfi_offset %rbx, -32
Ltmp67:
	.cfi_offset %r14, -24
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	movq	$0, -64(%rbp)
	leaq	L_.str(%rip), %rsi
	leaq	-80(%rbp), %rbx
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp26:
	leaq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z6rerev1NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp27:
## BB#1:
	movzbl	-56(%rbp), %edx
	movb	%dl, %al
	andb	$1, %al
	leaq	-55(%rbp), %rsi
	cmovneq	-40(%rbp), %rsi
	shrq	%rdx
	testb	%al, %al
	cmovneq	-48(%rbp), %rdx
Ltmp29:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp30:
## BB#2:                                ## %_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE.exit
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp31:
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp32:
## BB#3:                                ## %.noexc16
Ltmp33:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp34:
## BB#4:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp35:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp36:
## BB#5:                                ## %.noexc
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp38:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp39:
## BB#6:                                ## %.noexc5
Ltmp40:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp41:
## BB#7:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-80(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	$0, -112(%rbp)
	leaq	L_.str(%rip), %rsi
	leaq	-128(%rbp), %rdi
	movl	$7, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	movq	$0, -144(%rbp)
Ltmp43:
	leaq	L_.str.1(%rip), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp44:
## BB#8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
Ltmp46:
	leaq	-104(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	leaq	-160(%rbp), %rdx
	callq	__Z6rerev2NSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEES5_
Ltmp47:
## BB#9:
	movzbl	-104(%rbp), %edx
	movb	%dl, %al
	andb	$1, %al
	leaq	-103(%rbp), %rsi
	cmovneq	-88(%rbp), %rsi
	shrq	%rdx
	testb	%al, %al
	cmovneq	-96(%rbp), %rdx
Ltmp49:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp50:
## BB#10:                               ## %_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE.exit9
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp51:
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp52:
## BB#11:                               ## %.noexc17
Ltmp53:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp54:
## BB#12:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp55:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp56:
## BB#13:                               ## %.noexc12
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp58:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp59:
## BB#14:                               ## %.noexc13
Ltmp60:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp61:
## BB#15:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit11
	leaq	-104(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-160(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-128(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	addq	$144, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB2_17:
Ltmp42:
	movq	%rax, %rbx
	jmp	LBB2_18
LBB2_22:
Ltmp62:
	movq	%rax, %rbx
	jmp	LBB2_23
LBB2_27:
Ltmp37:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB2_18:                                ## %.body
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-80(%rbp), %rdi
	jmp	LBB2_26
LBB2_28:
Ltmp57:
	movq	%rax, %rbx
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB2_23:                                ## %.body18
	leaq	-104(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB2_24:
	leaq	-160(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB2_25:
	leaq	-128(%rbp), %rdi
LBB2_26:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB2_16:
Ltmp28:
	movq	%rax, %rbx
	leaq	-80(%rbp), %rdi
	jmp	LBB2_26
LBB2_20:
Ltmp45:
	movq	%rax, %rbx
	jmp	LBB2_25
LBB2_21:
Ltmp48:
	movq	%rax, %rbx
	jmp	LBB2_24
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\237\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\234\001"              ## Call site table length
Lset20 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset20
Lset21 = Ltmp26-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp26
	.long	Lset21
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset22 = Ltmp26-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset22
Lset23 = Ltmp27-Ltmp26                  ##   Call between Ltmp26 and Ltmp27
	.long	Lset23
Lset24 = Ltmp28-Lfunc_begin2            ##     jumps to Ltmp28
	.long	Lset24
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp29-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset25
Lset26 = Ltmp32-Ltmp29                  ##   Call between Ltmp29 and Ltmp32
	.long	Lset26
Lset27 = Ltmp42-Lfunc_begin2            ##     jumps to Ltmp42
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp33-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset28
Lset29 = Ltmp36-Ltmp33                  ##   Call between Ltmp33 and Ltmp36
	.long	Lset29
Lset30 = Ltmp37-Lfunc_begin2            ##     jumps to Ltmp37
	.long	Lset30
	.byte	0                       ##   On action: cleanup
Lset31 = Ltmp38-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset31
Lset32 = Ltmp41-Ltmp38                  ##   Call between Ltmp38 and Ltmp41
	.long	Lset32
Lset33 = Ltmp42-Lfunc_begin2            ##     jumps to Ltmp42
	.long	Lset33
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp41-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset34
Lset35 = Ltmp43-Ltmp41                  ##   Call between Ltmp41 and Ltmp43
	.long	Lset35
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp43-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset36
Lset37 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset37
Lset38 = Ltmp45-Lfunc_begin2            ##     jumps to Ltmp45
	.long	Lset38
	.byte	0                       ##   On action: cleanup
Lset39 = Ltmp46-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset39
Lset40 = Ltmp47-Ltmp46                  ##   Call between Ltmp46 and Ltmp47
	.long	Lset40
Lset41 = Ltmp48-Lfunc_begin2            ##     jumps to Ltmp48
	.long	Lset41
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp49-Lfunc_begin2            ## >> Call Site 9 <<
	.long	Lset42
Lset43 = Ltmp52-Ltmp49                  ##   Call between Ltmp49 and Ltmp52
	.long	Lset43
Lset44 = Ltmp62-Lfunc_begin2            ##     jumps to Ltmp62
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp53-Lfunc_begin2            ## >> Call Site 10 <<
	.long	Lset45
Lset46 = Ltmp56-Ltmp53                  ##   Call between Ltmp53 and Ltmp56
	.long	Lset46
Lset47 = Ltmp57-Lfunc_begin2            ##     jumps to Ltmp57
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp58-Lfunc_begin2            ## >> Call Site 11 <<
	.long	Lset48
Lset49 = Ltmp61-Ltmp58                  ##   Call between Ltmp58 and Ltmp61
	.long	Lset49
Lset50 = Ltmp62-Lfunc_begin2            ##     jumps to Ltmp62
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp61-Lfunc_begin2            ## >> Call Site 12 <<
	.long	Lset51
Lset52 = Lfunc_end2-Ltmp61              ##   Call between Ltmp61 and Lfunc_end2
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp89:
	.cfi_def_cfa_offset 16
Ltmp90:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp91:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp92:
	.cfi_offset %rbx, -56
Ltmp93:
	.cfi_offset %r12, -48
Ltmp94:
	.cfi_offset %r13, -40
Ltmp95:
	.cfi_offset %r14, -32
Ltmp96:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp68:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp69:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB4_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB4_7
## BB#3:
Ltmp71:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp72:
## BB#4:                                ## %.noexc
Ltmp73:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp74:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp75:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp76:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB4_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp78:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp79:
## BB#8:
	testq	%rax, %rax
	jne	LBB4_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp80:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp81:
LBB4_10:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB4_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB4_11:
Ltmp82:
	movq	%rax, %r14
	jmp	LBB4_12
LBB4_20:
Ltmp70:
	movq	%rax, %r14
	jmp	LBB4_13
LBB4_19:
Ltmp77:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB4_12:                                ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB4_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp83:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp84:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB4_15
LBB4_16:
Ltmp85:
	movq	%rax, %rbx
Ltmp86:
	callq	___cxa_end_catch
Ltmp87:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB4_18:
Ltmp88:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset53 = Ltmp68-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp69-Ltmp68                  ##   Call between Ltmp68 and Ltmp69
	.long	Lset54
Lset55 = Ltmp70-Lfunc_begin3            ##     jumps to Ltmp70
	.long	Lset55
	.byte	1                       ##   On action: 1
Lset56 = Ltmp71-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset56
Lset57 = Ltmp72-Ltmp71                  ##   Call between Ltmp71 and Ltmp72
	.long	Lset57
Lset58 = Ltmp82-Lfunc_begin3            ##     jumps to Ltmp82
	.long	Lset58
	.byte	1                       ##   On action: 1
Lset59 = Ltmp73-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset59
Lset60 = Ltmp76-Ltmp73                  ##   Call between Ltmp73 and Ltmp76
	.long	Lset60
Lset61 = Ltmp77-Lfunc_begin3            ##     jumps to Ltmp77
	.long	Lset61
	.byte	1                       ##   On action: 1
Lset62 = Ltmp78-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset62
Lset63 = Ltmp81-Ltmp78                  ##   Call between Ltmp78 and Ltmp81
	.long	Lset63
Lset64 = Ltmp82-Lfunc_begin3            ##     jumps to Ltmp82
	.long	Lset64
	.byte	1                       ##   On action: 1
Lset65 = Ltmp81-Lfunc_begin3            ## >> Call Site 5 <<
	.long	Lset65
Lset66 = Ltmp83-Ltmp81                  ##   Call between Ltmp81 and Ltmp83
	.long	Lset66
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset67 = Ltmp83-Lfunc_begin3            ## >> Call Site 6 <<
	.long	Lset67
Lset68 = Ltmp84-Ltmp83                  ##   Call between Ltmp83 and Ltmp84
	.long	Lset68
Lset69 = Ltmp85-Lfunc_begin3            ##     jumps to Ltmp85
	.long	Lset69
	.byte	0                       ##   On action: cleanup
Lset70 = Ltmp84-Lfunc_begin3            ## >> Call Site 7 <<
	.long	Lset70
Lset71 = Ltmp86-Ltmp84                  ##   Call between Ltmp84 and Ltmp86
	.long	Lset71
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset72 = Ltmp86-Lfunc_begin3            ## >> Call Site 8 <<
	.long	Lset72
Lset73 = Ltmp87-Ltmp86                  ##   Call between Ltmp86 and Ltmp87
	.long	Lset73
Lset74 = Ltmp88-Lfunc_begin3            ##     jumps to Ltmp88
	.long	Lset74
	.byte	1                       ##   On action: 1
Lset75 = Ltmp87-Lfunc_begin3            ## >> Call Site 9 <<
	.long	Lset75
Lset76 = Lfunc_end3-Ltmp87              ##   Call between Ltmp87 and Lfunc_end3
	.long	Lset76
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp103:
	.cfi_offset %rbx, -56
Ltmp104:
	.cfi_offset %r12, -48
Ltmp105:
	.cfi_offset %r13, -40
Ltmp106:
	.cfi_offset %r14, -32
Ltmp107:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB5_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB5_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB5_9
LBB5_3:
	testq	%rbx, %rbx
	jle	LBB5_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp97:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp98:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB5_9
LBB5_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB5_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB5_9
LBB5_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB5_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB5_10:
Ltmp99:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset77 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset77
Lset78 = Ltmp97-Lfunc_begin4            ##   Call between Lfunc_begin4 and Ltmp97
	.long	Lset78
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset79 = Ltmp97-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset79
Lset80 = Ltmp98-Ltmp97                  ##   Call between Ltmp97 and Ltmp98
	.long	Lset80
Lset81 = Ltmp99-Lfunc_begin4            ##     jumps to Ltmp99
	.long	Lset81
	.byte	0                       ##   On action: cleanup
Lset82 = Ltmp98-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset82
Lset83 = Lfunc_end4-Ltmp98              ##   Call between Ltmp98 and Lfunc_end4
	.long	Lset83
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"testing"

L_.str.1:                               ## @.str.1
	.space	1


.subsections_via_symbols
