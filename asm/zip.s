	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init:                 ## @__cxx_global_var_init
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	leaq	_t2(%rip), %rdi
	callq	__ZNK3$_0cvPFDaT_EIiEEv
	leaq	_t2(%rip), %rdi
	movq	%rax, _functions(%rip)
	callq	__ZNK3$_0cvPFDaT_EIiEEv
	leaq	_t2(%rip), %rdi
	movq	%rax, _functions+8(%rip)
	callq	__ZNK3$_0cvPFDaT_EIiEEv
	leaq	_t2(%rip), %rdi
	movq	%rax, _functions+16(%rip)
	callq	__ZNK3$_0cvPFDaT_EIiEEv
	movq	%rax, _functions+24(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZNK3$_0cvPFDaT_EIiEEv:                ## @"_ZNK3$_0cvPFDaT_EIiEEv"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	leaq	__ZN3$_08__invokeIiEEDaT_(%rip), %rax
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z4testv
	.align	4, 0x90
__Z4testv:                              ## @_Z4testv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	leaq	-80(%rbp), %rdi
	leaq	_input(%rip), %rsi
	leaq	_functions(%rip), %rdx
	leaq	_output(%rip), %rcx
	callq	__Z3zipIJA4_iA4_PFiiES0_EEDaDpRT_
	leaq	-104(%rbp), %rdi
	leaq	-80(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rsi
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EE5beginEv
	leaq	-128(%rbp), %rdi
	movq	-56(%rbp), %rsi
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EE3endEv
LBB2_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-104(%rbp), %rdi
	leaq	-128(%rbp), %rsi
	callq	__ZNK9iteratorsIJPiPPFiiES0_EEneERKS4_
	testb	$1, %al
	jne	LBB2_2
	jmp	LBB2_4
LBB2_2:                                 ##   in Loop: Header=BB2_1 Depth=1
	leaq	-152(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	callq	__ZN9iteratorsIJPiPPFiiES0_EEdeEv
	leaq	-152(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -160(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rdi
	addq	$8, %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -168(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -176(%rbp)
	movq	-168(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-160(%rbp), %rdi
	movl	(%rdi), %edi
	callq	*%rsi
	movq	-176(%rbp), %rsi
	movl	%eax, (%rsi)
## BB#3:                                ##   in Loop: Header=BB2_1 Depth=1
	leaq	-104(%rbp), %rdi
	callq	__ZN9iteratorsIJPiPPFiiES0_EEppEv
	movq	%rax, -184(%rbp)        ## 8-byte Spill
	jmp	LBB2_1
LBB2_4:
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z3zipIJA4_iA4_PFiiES0_EEDaDpRT_
	.weak_def_can_be_hidden	__Z3zipIJA4_iA4_PFiiES0_EEDaDpRT_
	.align	4, 0x90
__Z3zipIJA4_iA4_PFiiES0_EEDaDpRT_:      ## @_Z3zipIJA4_iA4_PFiiES0_EEDaDpRT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rsi, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdx
	movq	-24(%rbp), %rcx
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EEC1ERS0_RS3_S5_
	movq	-32(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EE5beginEv
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EE5beginEv
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EE5beginEv: ## @_ZN6rangesIJA4_iA4_PFiiES0_EE5beginEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp14:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EE11make_beginsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EE3endEv
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EE3endEv
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EE3endEv:   ## @_ZN6rangesIJA4_iA4_PFiiES0_EE3endEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EE9make_endsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK9iteratorsIJPiPPFiiES0_EEneERKS4_
	.weak_def_can_be_hidden	__ZNK9iteratorsIJPiPPFiiES0_EEneERKS4_
	.align	4, 0x90
__ZNK9iteratorsIJPiPPFiiES0_EEneERKS4_: ## @_ZNK9iteratorsIJPiPPFiiES0_EEneERKS4_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp20:
	.cfi_def_cfa_register %rbp
	subq	$152, %rsp
	xorl	%eax, %eax
	movb	%al, %cl
	leaq	-152(%rbp), %rdx
	leaq	-184(%rbp), %r8
	leaq	-216(%rbp), %r9
	leaq	-240(%rbp), %r10
	movq	%rdi, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	-264(%rbp), %rsi
	movq	-272(%rbp), %rdi
	movq	%rsi, -248(%rbp)
	movq	%rdi, -256(%rbp)
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdi, -232(%rbp)
	movq	-224(%rbp), %rsi
	movq	-232(%rbp), %rdi
	movq	%r10, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %rdi
	movq	%r9, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movq	%r8, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -120(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-144(%rbp), %rsi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rsi
	movq	%rsi, -88(%rbp)
	movq	-88(%rbp), %rsi
	cmpq	(%rsi), %rdx
	movb	%cl, -273(%rbp)         ## 1-byte Spill
	jne	LBB6_2
## BB#1:
	movq	-168(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	-176(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	addq	$8, %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	movb	%dl, -273(%rbp)         ## 1-byte Spill
LBB6_2:                                 ## %_ZNSt3__113__tuple_equalILm2EEclINS_5tupleIJPiPPFiiES4_EEES8_EEbRKT_RKT0_.exit.i.i.i
	movb	-273(%rbp), %al         ## 1-byte Reload
	xorl	%ecx, %ecx
	movb	%cl, %dl
	testb	$1, %al
	movb	%dl, -274(%rbp)         ## 1-byte Spill
	jne	LBB6_3
	jmp	LBB6_4
LBB6_3:
	movq	-200(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	-208(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	movb	%dl, -274(%rbp)         ## 1-byte Spill
LBB6_4:                                 ## %_ZNSt3__1neIJPiPPFiiES1_EJS1_S4_S1_EEEbRKNS_5tupleIJDpT_EEERKNS5_IJDpT0_EEE.exit
	movb	-274(%rbp), %al         ## 1-byte Reload
	xorb	$-1, %al
	andb	$1, %al
	movzbl	%al, %eax
	addq	$152, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EEdeEv
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EEdeEv
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EEdeEv:      ## @_ZN9iteratorsIJPiPPFiiES0_EEdeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	callq	__ZN9iteratorsIJPiPPFiiES0_EE4refsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EEppEv
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EEppEv
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EEppEv:      ## @_ZN9iteratorsIJPiPPFiiES0_EEppEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	$1, %eax
	movl	%eax, %esi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN9iteratorsIJPiPPFiiES0_EEpLEm
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp27:
	.cfi_def_cfa_offset 16
Ltmp28:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp29:
	.cfi_def_cfa_register %rbp
	subq	$352, %rsp              ## imm = 0x160
	callq	__Z4testv
	leaq	_output(%rip), %rax
	movq	%rax, -288(%rbp)
	movq	%rax, -264(%rbp)
	leaq	_output+16(%rip), %rcx
	leaq	-304(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdx
	movq	%rdx, -40(%rbp)
	leaq	L_.str(%rip), %rdx
	movq	%rdx, -48(%rbp)
	movq	-32(%rbp), %rsi
	movq	-40(%rbp), %rdi
	movq	%rsi, -8(%rbp)
	movq	%rdi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdx
	movq	-16(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-24(%rbp), %rsi
	movq	%rsi, 8(%rdx)
	movq	-304(%rbp), %rdx
	movq	-296(%rbp), %rsi
	movq	%rdx, -208(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-208(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	%rsi, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-256(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rsi, -56(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rdx, -240(%rbp)
	movq	%rsi, -232(%rbp)
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
LBB9_1:                                 ## =>This Inner Loop Header: Depth=1
	movq	-160(%rbp), %rax
	cmpq	-168(%rbp), %rax
	je	LBB9_5
## BB#2:                                ##   in Loop: Header=BB9_1 Depth=1
	leaq	-152(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, -104(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rdi
	movq	-104(%rbp), %rcx
	movl	(%rcx), %esi
	movq	%rax, -328(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	movq	-328(%rbp), %rcx        ## 8-byte Reload
	cmpq	$0, 8(%rcx)
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	je	LBB9_4
## BB#3:                                ##   in Loop: Header=BB9_1 Depth=1
	movq	-328(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	%rax, -344(%rbp)        ## 8-byte Spill
LBB9_4:                                 ## %_ZNSt3__116ostream_iteratorIicNS_11char_traitsIcEEEaSERKi.exit.i.i
                                        ##   in Loop: Header=BB9_1 Depth=1
	leaq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	addq	$4, %rcx
	movq	%rcx, -160(%rbp)
	movq	%rax, -112(%rbp)
	jmp	LBB9_1
LBB9_5:                                 ## %_ZNSt3__14copyIPiNS_16ostream_iteratorIicNS_11char_traitsIcEEEEEET0_T_S7_S6_.exit
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rax
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rcx
	movq	-152(%rbp), %rdx
	movq	-144(%rbp), %rsi
	movq	%rsi, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rsi, -184(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -320(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rdi
	callq	*-280(%rbp)
	xorl	%r8d, %r8d
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%r8d, %eax
	addq	$352, %rsp              ## imm = 0x160
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp35:
	.cfi_def_cfa_offset 16
Ltmp36:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp37:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp30:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp31:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB10_1
LBB10_1:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp32:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp33:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB10_3
LBB10_2:
Ltmp34:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB10_3:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table10:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp30-Lfunc_begin0             ##   Call between Lfunc_begin0 and Ltmp30
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp30-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp33-Ltmp30                   ##   Call between Ltmp30 and Ltmp33
	.long	Lset3
Lset4 = Ltmp34-Lfunc_begin0             ##     jumps to Ltmp34
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp33-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Lfunc_end0-Ltmp33               ##   Call between Ltmp33 and Lfunc_end0
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init.1:               ## @__cxx_global_var_init.1
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp38:
	.cfi_def_cfa_offset 16
Ltmp39:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp40:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZN3$_08__invokeIiEEDaT_:              ## @"_ZN3$_08__invokeIiEEDaT_"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp41:
	.cfi_def_cfa_offset 16
Ltmp42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp43:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
                                        ## implicit-def: %RAX
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %esi
	movq	%rax, %rdi
	callq	__ZNK3$_0clIiEEDaT_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.align	4, 0x90
__ZNK3$_0clIiEEDaT_:                    ## @"_ZNK3$_0clIiEEDaT_"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	-12(%rbp), %esi
	shll	$1, %esi
	movl	%esi, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EEC1ERS0_RS3_S5_
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EEC1ERS0_RS3_S5_
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EEC1ERS0_RS3_S5_: ## @_ZN6rangesIJA4_iA4_PFiiES0_EEC1ERS0_RS3_S5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZN6rangesIJA4_iA4_PFiiES0_EEC2ERS0_RS3_S5_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EEC2ERS0_RS3_S5_
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EEC2ERS0_RS3_S5_
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EEC2ERS0_RS3_S5_: ## @_ZN6rangesIJA4_iA4_PFiiES0_EEC2ERS0_RS3_S5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp50:
	.cfi_def_cfa_offset 16
Ltmp51:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp52:
	.cfi_def_cfa_register %rbp
	subq	$224, %rsp
	movq	%rdi, -328(%rbp)
	movq	%rsi, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	-328(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdi
	movq	%rcx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rsi, -312(%rbp)
	movq	%rdi, -320(%rbp)
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	-312(%rbp), %rsi
	movq	-320(%rbp), %rdi
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rdi, -256(%rbp)
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	%rcx, -200(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdi, -224(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rdi
	movq	%rcx, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, %rdx
	movq	-144(%rbp), %rsi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rsi
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-24(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, %rdx
	addq	$8, %rdx
	movq	-152(%rbp), %rsi
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rdx, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, (%rdx)
	addq	$16, %rcx
	movq	-160(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rcx)
	addq	$224, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EE11make_beginsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EE11make_beginsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EE11make_beginsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE: ## @_ZN6rangesIJA4_iA4_PFiiES0_EE11make_beginsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, %rax
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rdx
	addq	$8, %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsi
	movq	-72(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__Z6beginsIJA4_iA4_PFiiES0_EEDaDpRT_
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z6beginsIJA4_iA4_PFiiES0_EEDaDpRT_
	.weak_def_can_be_hidden	__Z6beginsIJA4_iA4_PFiiES0_EEDaDpRT_
	.align	4, 0x90
__Z6beginsIJA4_iA4_PFiiES0_EEDaDpRT_:   ## @_Z6beginsIJA4_iA4_PFiiES0_EEDaDpRT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp56:
	.cfi_def_cfa_offset 16
Ltmp57:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp58:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, %rax
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	-40(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	-48(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	callq	__ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_
	movq	-56(%rbp), %rax         ## 8-byte Reload
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_: ## @_ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp59:
	.cfi_def_cfa_offset 16
Ltmp60:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp61:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZN9iteratorsIJPiPPFiiES0_EEC2ES0_S3_S0_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EEC2ES0_S3_S0_
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EEC2ES0_S3_S0_
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EEC2ES0_S3_S0_: ## @_ZN9iteratorsIJPiPPFiiES0_EEC2ES0_S3_S0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp62:
	.cfi_def_cfa_offset 16
Ltmp63:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp64:
	.cfi_def_cfa_register %rbp
	subq	$248, %rsp
	leaq	-376(%rbp), %rax
	leaq	-368(%rbp), %r8
	leaq	-360(%rbp), %r9
	movq	%rdi, -352(%rbp)
	movq	%rsi, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rcx, -376(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	%r9, -328(%rbp)
	movq	%r8, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	-320(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	%rax, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rsi, -280(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rax, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rsi, -240(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rcx
	movq	-232(%rbp), %rdx
	movq	-240(%rbp), %rsi
	movq	%rax, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-152(%rbp), %rax
	movq	%rax, %rcx
	movq	-160(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	%rax, %rcx
	addq	$8, %rcx
	movq	-168(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	addq	$16, %rax
	movq	-176(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rax, -96(%rbp)
	movq	%rcx, -104(%rbp)
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	addq	$248, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6rangesIJA4_iA4_PFiiES0_EE9make_endsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.weak_def_can_be_hidden	__ZN6rangesIJA4_iA4_PFiiES0_EE9make_endsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.align	4, 0x90
__ZN6rangesIJA4_iA4_PFiiES0_EE9make_endsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE: ## @_ZN6rangesIJA4_iA4_PFiiES0_EE9make_endsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp65:
	.cfi_def_cfa_offset 16
Ltmp66:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp67:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, %rax
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rdx
	addq	$8, %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -72(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsi
	movq	-72(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__Z4endsIJA4_iA4_PFiiES0_EEDaDpRT_
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z4endsIJA4_iA4_PFiiES0_EEDaDpRT_
	.weak_def_can_be_hidden	__Z4endsIJA4_iA4_PFiiES0_EEDaDpRT_
	.align	4, 0x90
__Z4endsIJA4_iA4_PFiiES0_EEDaDpRT_:     ## @_Z4endsIJA4_iA4_PFiiES0_EEDaDpRT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp68:
	.cfi_def_cfa_offset 16
Ltmp69:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp70:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, %rax
	movq	%rsi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$16, %rcx
	movq	-40(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	addq	$32, %rdx
	movq	-48(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -56(%rbp)         ## 8-byte Spill
	movq	%rcx, %rsi
	movq	-56(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	callq	__ZN9iteratorsIJPiPPFiiES0_EEC1ES0_S3_S0_
	movq	-64(%rbp), %rax         ## 8-byte Reload
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EE4refsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EE4refsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EE4refsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE: ## @_ZN9iteratorsIJPiPPFiiES0_EE4refsIJLm0ELm1ELm2EEEEDaNSt3__116integer_sequenceImJXspT_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp71:
	.cfi_def_cfa_offset 16
Ltmp72:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp73:
	.cfi_def_cfa_register %rbp
	subq	$280, %rsp              ## imm = 0x118
	movq	%rdi, %rax
	movq	%rsi, -408(%rbp)
	movq	-408(%rbp), %rsi
	movq	%rsi, -392(%rbp)
	movq	-392(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdx
	addq	$8, %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	-360(%rbp), %rcx
	movq	-368(%rbp), %rdx
	movq	-376(%rbp), %rsi
	movq	%rdi, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	-328(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	-344(%rbp), %rsi
	movq	-352(%rbp), %rdi
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -288(%rbp)
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	-280(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%rdi, -256(%rbp)
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	-248(%rbp), %rsi
	movq	-256(%rbp), %rdi
	movq	%rcx, -168(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdi, -192(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, %rdx
	movq	-176(%rbp), %rsi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movq	%rdx, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, %rdx
	addq	$8, %rdx
	movq	-184(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rsi, (%rdx)
	addq	$16, %rcx
	movq	-192(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	%rdx, (%rcx)
	addq	$280, %rsp              ## imm = 0x118
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EEpLEm
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EEpLEm
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EEpLEm:      ## @_ZN9iteratorsIJPiPPFiiES0_EEpLEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp74:
	.cfi_def_cfa_offset 16
Ltmp75:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp76:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN9iteratorsIJPiPPFiiES0_EE4plusIJLm0ELm1ELm2EEEERDamNSt3__116integer_sequenceImJXspT_EEEE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN9iteratorsIJPiPPFiiES0_EE4plusIJLm0ELm1ELm2EEEERDamNSt3__116integer_sequenceImJXspT_EEEE
	.weak_def_can_be_hidden	__ZN9iteratorsIJPiPPFiiES0_EE4plusIJLm0ELm1ELm2EEEERDamNSt3__116integer_sequenceImJXspT_EEEE
	.align	4, 0x90
__ZN9iteratorsIJPiPPFiiES0_EE4plusIJLm0ELm1ELm2EEEERDamNSt3__116integer_sequenceImJXspT_EEEE: ## @_ZN9iteratorsIJPiPPFiiES0_EE4plusIJLm0ELm1ELm2EEEERDamNSt3__116integer_sequenceImJXspT_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp77:
	.cfi_def_cfa_offset 16
Ltmp78:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp79:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rcx
	movq	%rcx, -8(%rbp)
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	-88(%rbp), %rcx
	movl	$0, -24(%rbp)
	movq	-96(%rbp), %rsi
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	shlq	$2, %rsi
	addq	(%rdi), %rsi
	movq	%rsi, (%rdi)
	movl	$0, -20(%rbp)
	movq	-96(%rbp), %rsi
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rdi
	addq	$8, %rdi
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	shlq	$3, %rsi
	addq	(%rdi), %rsi
	movq	%rsi, (%rdi)
	movl	$0, -16(%rbp)
	movq	-96(%rbp), %rsi
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -48(%rbp)
	movq	-48(%rbp), %rdi
	shlq	$2, %rsi
	addq	(%rdi), %rsi
	movq	%rsi, (%rdi)
	movl	$0, -12(%rbp)
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	movq	%rcx, -104(%rbp)        ## 8-byte Spill
	jne	LBB24_2
## BB#1:                                ## %SP_return
	movq	-104(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rsp
	popq	%rbp
	retq
LBB24_2:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp80:
	.cfi_def_cfa_offset 16
Ltmp81:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp82:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp104:
	.cfi_def_cfa_offset 16
Ltmp105:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp106:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp83:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp84:
	jmp	LBB26_1
LBB26_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB26_3
	jmp	LBB26_26
LBB26_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB26_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB26_7
LBB26_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB26_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB26_8
	jmp	LBB26_13
LBB26_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp86:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp87:
	jmp	LBB26_9
LBB26_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp88:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp89:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB26_10
LBB26_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp90:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp91:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB26_12
LBB26_11:
Ltmp92:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB26_21
LBB26_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB26_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp93:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp94:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB26_15
LBB26_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB26_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp95:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp96:
	jmp	LBB26_17
LBB26_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB26_18
LBB26_18:
	jmp	LBB26_25
LBB26_19:
Ltmp85:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB26_22
LBB26_20:
Ltmp97:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB26_21
LBB26_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB26_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp98:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp99:
	jmp	LBB26_23
LBB26_23:
	callq	___cxa_end_catch
LBB26_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB26_25:
	jmp	LBB26_26
LBB26_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB26_24
LBB26_27:
Ltmp100:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp101:
	callq	___cxa_end_catch
Ltmp102:
	jmp	LBB26_28
LBB26_28:
	jmp	LBB26_29
LBB26_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB26_30:
Ltmp103:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table26:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset7 = Ltmp83-Lfunc_begin1             ## >> Call Site 1 <<
	.long	Lset7
Lset8 = Ltmp84-Ltmp83                   ##   Call between Ltmp83 and Ltmp84
	.long	Lset8
Lset9 = Ltmp85-Lfunc_begin1             ##     jumps to Ltmp85
	.long	Lset9
	.byte	5                       ##   On action: 3
Lset10 = Ltmp86-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset10
Lset11 = Ltmp87-Ltmp86                  ##   Call between Ltmp86 and Ltmp87
	.long	Lset11
Lset12 = Ltmp97-Lfunc_begin1            ##     jumps to Ltmp97
	.long	Lset12
	.byte	5                       ##   On action: 3
Lset13 = Ltmp88-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset13
Lset14 = Ltmp91-Ltmp88                  ##   Call between Ltmp88 and Ltmp91
	.long	Lset14
Lset15 = Ltmp92-Lfunc_begin1            ##     jumps to Ltmp92
	.long	Lset15
	.byte	3                       ##   On action: 2
Lset16 = Ltmp93-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset16
Lset17 = Ltmp96-Ltmp93                  ##   Call between Ltmp93 and Ltmp96
	.long	Lset17
Lset18 = Ltmp97-Lfunc_begin1            ##     jumps to Ltmp97
	.long	Lset18
	.byte	5                       ##   On action: 3
Lset19 = Ltmp96-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset19
Lset20 = Ltmp98-Ltmp96                  ##   Call between Ltmp96 and Ltmp98
	.long	Lset20
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp98-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset21
Lset22 = Ltmp99-Ltmp98                  ##   Call between Ltmp98 and Ltmp99
	.long	Lset22
Lset23 = Ltmp100-Lfunc_begin1           ##     jumps to Ltmp100
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp99-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset24
Lset25 = Ltmp101-Ltmp99                 ##   Call between Ltmp99 and Ltmp101
	.long	Lset25
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset26 = Ltmp101-Lfunc_begin1           ## >> Call Site 8 <<
	.long	Lset26
Lset27 = Ltmp102-Ltmp101                ##   Call between Ltmp101 and Ltmp102
	.long	Lset27
Lset28 = Ltmp103-Lfunc_begin1           ##     jumps to Ltmp103
	.long	Lset28
	.byte	5                       ##   On action: 3
Lset29 = Ltmp102-Lfunc_begin1           ## >> Call Site 9 <<
	.long	Lset29
Lset30 = Lfunc_end1-Ltmp102             ##   Call between Ltmp102 and Lfunc_end1
	.long	Lset30
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp107:
	.cfi_def_cfa_offset 16
Ltmp108:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp109:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp113:
	.cfi_def_cfa_offset 16
Ltmp114:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp115:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB28_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB28_26
LBB28_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB28_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB28_5
LBB28_4:
	movq	$0, -368(%rbp)
LBB28_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB28_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB28_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB28_26
LBB28_8:
	jmp	LBB28_9
LBB28_9:
	cmpq	$0, -368(%rbp)
	jle	LBB28_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB28_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB28_13
LBB28_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB28_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp110:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp111:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB28_14
LBB28_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB28_15
LBB28_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB28_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB28_19
LBB28_17:
Ltmp112:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB28_27
LBB28_18:
	movl	$0, -416(%rbp)
LBB28_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB28_20
	jmp	LBB28_29
LBB28_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB28_26
	jmp	LBB28_28
LBB28_20:
	jmp	LBB28_21
LBB28_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB28_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB28_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB28_26
LBB28_24:
	jmp	LBB28_25
LBB28_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB28_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB28_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB28_28:
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table28:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset31 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset31
Lset32 = Ltmp110-Lfunc_begin2           ##   Call between Lfunc_begin2 and Ltmp110
	.long	Lset32
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp110-Lfunc_begin2           ## >> Call Site 2 <<
	.long	Lset33
Lset34 = Ltmp111-Ltmp110                ##   Call between Ltmp110 and Ltmp111
	.long	Lset34
Lset35 = Ltmp112-Lfunc_begin2           ##     jumps to Ltmp112
	.long	Lset35
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp111-Lfunc_begin2           ## >> Call Site 3 <<
	.long	Lset36
Lset37 = Lfunc_end2-Ltmp111             ##   Call between Ltmp111 and Lfunc_end2
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp116:
	.cfi_def_cfa_offset 16
Ltmp117:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp118:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp119:
	.cfi_def_cfa_offset 16
Ltmp120:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp121:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
__GLOBAL__sub_I_zip.cpp:                ## @_GLOBAL__sub_I_zip.cpp
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp122:
	.cfi_def_cfa_offset 16
Ltmp123:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp124:
	.cfi_def_cfa_register %rbp
	callq	___cxx_global_var_init.1
	callq	___cxx_global_var_init
	popq	%rbp
	retq
	.cfi_endproc

	.section	__DATA,__data
	.globl	_input                  ## @input
	.align	4
_input:
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	3                       ## 0x3
	.long	4                       ## 0x4

	.globl	_output                 ## @output
.zerofill __DATA,__common,_output,16,4
	.globl	_functions              ## @functions
.zerofill __DATA,__common,_functions,32,4
.zerofill __DATA,__bss,_t2,1,0          ## @t2
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	", "

	.section	__DATA,__mod_init_func,mod_init_funcs
	.align	3
	.quad	__GLOBAL__sub_I_zip.cpp

.subsections_via_symbols
