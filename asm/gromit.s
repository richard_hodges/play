	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp20:
	.cfi_def_cfa_register %rbp
	subq	$2384, %rsp             ## imm = 0x950
	leaq	-1984(%rbp), %rax
	leaq	-2008(%rbp), %rcx
	leaq	-2088(%rbp), %rdx
	movl	$0, -2060(%rbp)
	movq	%rdx, -2056(%rbp)
	movq	-2056(%rbp), %rdx
	movq	%rdx, -2048(%rbp)
	movq	-2048(%rbp), %rdx
	movq	%rdx, -2040(%rbp)
	movq	-2040(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -2032(%rbp)
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	addq	$16, %rdx
	movq	%rdx, -2016(%rbp)
	movq	$0, -2024(%rbp)
	movq	-2016(%rbp), %rdx
	movq	-2024(%rbp), %rsi
	movq	%rdx, -2000(%rbp)
	movq	%rsi, -2008(%rbp)
	movq	-2000(%rbp), %rdx
	movq	%rcx, -1992(%rbp)
	movq	-1992(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -1976(%rbp)
	movq	%rcx, -1984(%rbp)
	movq	-1976(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -1968(%rbp)
	movq	%rax, -1960(%rbp)
	movq	-1960(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movl	$0, -2092(%rbp)
LBB0_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$5, -2092(%rbp)
	jge	LBB0_14
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
Ltmp8:
	movl	$4, %eax
	movl	%eax, %edi
	callq	__Znwm
Ltmp9:
	movq	%rax, -2328(%rbp)       ## 8-byte Spill
	jmp	LBB0_3
LBB0_3:                                 ##   in Loop: Header=BB0_1 Depth=1
	movq	-2328(%rbp), %rax       ## 8-byte Reload
	movl	-2092(%rbp), %esi
Ltmp10:
	movq	-2328(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, -2336(%rbp)       ## 8-byte Spill
	callq	__ZN6GromitC1Ei
Ltmp11:
	jmp	LBB0_4
LBB0_4:                                 ##   in Loop: Header=BB0_1 Depth=1
	leaq	-2104(%rbp), %rax
	leaq	-2088(%rbp), %rcx
	movq	-2336(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -2104(%rbp)
	movq	%rcx, -1936(%rbp)
	movq	%rax, -1944(%rbp)
	movq	-1936(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, %rsi
	movq	%rsi, -1928(%rbp)
	movq	-1928(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -1920(%rbp)
	movq	-1920(%rbp), %rsi
	movq	%rsi, -1912(%rbp)
	movq	-1912(%rbp), %rsi
	cmpq	(%rsi), %rcx
	movq	%rax, -2344(%rbp)       ## 8-byte Spill
	jae	LBB0_7
## BB#5:                                ##   in Loop: Header=BB0_1 Depth=1
Ltmp15:
	movl	$1, %eax
	movl	%eax, %edx
	leaq	-1952(%rbp), %rdi
	movq	-2344(%rbp), %rsi       ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC1ERKS8_m
Ltmp16:
	jmp	LBB0_6
LBB0_6:                                 ## %.noexc
                                        ##   in Loop: Header=BB0_1 Depth=1
	leaq	-1952(%rbp), %rdi
	leaq	-1664(%rbp), %rax
	leaq	-1688(%rbp), %rcx
	leaq	-1728(%rbp), %rdx
	movq	-2344(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -1904(%rbp)
	movq	-1904(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -1896(%rbp)
	movq	-1896(%rbp), %rsi
	movq	%rsi, -1888(%rbp)
	movq	-1888(%rbp), %rsi
	movq	-2344(%rbp), %r8        ## 8-byte Reload
	movq	8(%r8), %r9
	movq	%r9, -1872(%rbp)
	movq	-1872(%rbp), %r9
	movq	-1944(%rbp), %r10
	movq	%r10, -1640(%rbp)
	movq	-1640(%rbp), %r10
	movq	%rsi, -1832(%rbp)
	movq	%r9, -1840(%rbp)
	movq	%r10, -1848(%rbp)
	movq	-1832(%rbp), %rsi
	movq	-1840(%rbp), %r9
	movq	-1848(%rbp), %r10
	movq	%r10, -1824(%rbp)
	movq	-1824(%rbp), %r10
	movq	%rsi, -1800(%rbp)
	movq	%r9, -1808(%rbp)
	movq	%r10, -1816(%rbp)
	movq	-1800(%rbp), %rsi
	movq	-1808(%rbp), %r9
	movq	-1816(%rbp), %r10
	movq	%r10, -1784(%rbp)
	movq	-1784(%rbp), %r10
	movq	%rsi, -1760(%rbp)
	movq	%r9, -1768(%rbp)
	movq	%r10, -1776(%rbp)
	movq	-1768(%rbp), %rsi
	movq	-1776(%rbp), %r9
	movq	%r9, -1752(%rbp)
	movq	-1752(%rbp), %r9
	movq	(%r9), %r9
	movq	%rsi, -1736(%rbp)
	movq	%r9, -1744(%rbp)
	movq	-1736(%rbp), %rsi
	movq	-1744(%rbp), %r9
	movq	%rsi, -1720(%rbp)
	movq	%r9, -1728(%rbp)
	movq	-1720(%rbp), %rsi
	movq	%rdx, -1712(%rbp)
	movq	-1712(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -1696(%rbp)
	movq	%rdx, -1704(%rbp)
	movq	-1696(%rbp), %rdx
	movq	-1704(%rbp), %rsi
	movq	%rdx, -1680(%rbp)
	movq	%rsi, -1688(%rbp)
	movq	-1680(%rbp), %rdx
	movq	%rcx, -1672(%rbp)
	movq	-1672(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -1656(%rbp)
	movq	%rcx, -1664(%rbp)
	movq	-1656(%rbp), %rcx
	movq	%rax, -1648(%rbp)
	movq	-1648(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotator6__doneEv
	movq	-2344(%rbp), %rax       ## 8-byte Reload
	movq	8(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, 8(%rax)
	jmp	LBB0_9
LBB0_7:                                 ##   in Loop: Header=BB0_1 Depth=1
	movq	-1944(%rbp), %rax
	movq	%rax, -1880(%rbp)
Ltmp13:
	movq	-2344(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__emplace_back_slow_pathIJPS2_EEEvDpOT_
Ltmp14:
	jmp	LBB0_8
LBB0_8:                                 ## %.noexc1
                                        ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_9
LBB0_9:                                 ## %_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE12emplace_backIJPS2_EEEvDpOT_.exit
                                        ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_10
LBB0_10:                                ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_11
LBB0_11:                                ##   in Loop: Header=BB0_1 Depth=1
	movl	-2092(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -2092(%rbp)
	jmp	LBB0_1
LBB0_12:
Ltmp17:
	movl	%edx, %ecx
	movq	%rax, -2112(%rbp)
	movl	%ecx, -2116(%rbp)
	jmp	LBB0_23
LBB0_13:
Ltmp12:
	movl	%edx, %ecx
	movq	%rax, -2112(%rbp)
	movl	%ecx, -2116(%rbp)
	movq	-2328(%rbp), %rdi       ## 8-byte Reload
	callq	__ZdlPv
	jmp	LBB0_23
LBB0_14:
	movq	__ZN6Gromit5speakEi@GOTPCREL(%rip), %rax
	movq	%rax, -2160(%rbp)
	movq	$0, -2152(%rbp)
	movl	$1, -2164(%rbp)
	leaq	-2160(%rbp), %rax
	movq	%rax, -1616(%rbp)
	movq	__ZNSt3__112placeholders2_1E@GOTPCREL(%rip), %rax
	movq	%rax, -1624(%rbp)
	leaq	-2164(%rbp), %rax
	movq	%rax, -1632(%rbp)
	movq	-1616(%rbp), %rax
	movq	%rax, -1608(%rbp)
	movq	-1624(%rbp), %rcx
	movq	%rcx, -1232(%rbp)
	movq	-1632(%rbp), %rdx
	movq	%rdx, -1240(%rbp)
	leaq	-2144(%rbp), %rsi
	movq	%rsi, -1576(%rbp)
	movq	%rax, -1584(%rbp)
	movq	%rcx, -1592(%rbp)
	movq	%rdx, -1600(%rbp)
	movq	-1576(%rbp), %rax
	movq	-1584(%rbp), %rcx
	movq	-1592(%rbp), %rsi
	movq	%rax, -1544(%rbp)
	movq	%rcx, -1552(%rbp)
	movq	%rsi, -1560(%rbp)
	movq	%rdx, -1568(%rbp)
	movq	-1544(%rbp), %rax
	movq	-1552(%rbp), %rcx
	movq	%rcx, -1536(%rbp)
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rdx, (%rax)
	addq	$16, %rax
	movq	-1560(%rbp), %rcx
	movq	%rcx, -1248(%rbp)
	movq	-1248(%rbp), %rcx
	movq	-1568(%rbp), %rdx
	movq	%rdx, -1256(%rbp)
	movq	-1256(%rbp), %rdx
	movq	%rax, -1512(%rbp)
	movq	%rcx, -1520(%rbp)
	movq	%rdx, -1528(%rbp)
	movq	-1512(%rbp), %rax
	movq	-1528(%rbp), %rcx
	movq	-1520(%rbp), %rdx
	movq	%rax, -1456(%rbp)
	movq	%rdx, -1464(%rbp)
	movq	%rcx, -1472(%rbp)
	movq	-1456(%rbp), %rax
	movq	-1464(%rbp), %rcx
	movq	%rcx, -1448(%rbp)
	movq	-1448(%rbp), %rcx
	movq	-1472(%rbp), %rdx
	movq	%rdx, -1264(%rbp)
	movq	-1264(%rbp), %rdx
	movq	%rax, -1424(%rbp)
	movq	%rcx, -1432(%rbp)
	movq	%rdx, -1440(%rbp)
	movq	-1424(%rbp), %rax
	movq	-1440(%rbp), %rcx
	movq	-1432(%rbp), %rdx
	movq	%rax, -1368(%rbp)
	movq	%rdx, -1376(%rbp)
	movq	%rcx, -1384(%rbp)
	movq	-1368(%rbp), %rax
	movq	%rax, %rcx
	movq	-1376(%rbp), %rdx
	movq	%rdx, -1328(%rbp)
	movq	-1328(%rbp), %rdx
	movq	%rcx, -1280(%rbp)
	movq	%rdx, -1288(%rbp)
	movq	-1288(%rbp), %rcx
	movq	%rcx, -1272(%rbp)
	movq	-1384(%rbp), %rcx
	movq	%rcx, -1296(%rbp)
	movq	-1296(%rbp), %rcx
	movq	%rax, -1312(%rbp)
	movq	%rcx, -1320(%rbp)
	movq	-1312(%rbp), %rax
	movq	-1320(%rbp), %rcx
	movq	%rcx, -1304(%rbp)
	movq	-1304(%rbp), %rcx
	movl	(%rcx), %edi
	movl	%edi, (%rax)
## BB#15:
Ltmp0:
	movq	-2128(%rbp), %rax
	movq	%rsp, %rcx
	movq	%rax, 16(%rcx)
	movq	-2144(%rbp), %rax
	movq	-2136(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	leaq	-2088(%rbp), %rdi
	callq	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FviEJRNS0_12placeholders4__phILi1EEEiEEEEvRT_T0_
Ltmp1:
	jmp	LBB0_16
LBB0_16:
	movq	__ZN6Gromit3eatEf@GOTPCREL(%rip), %rax
	movq	%rax, -2208(%rbp)
	movq	$0, -2200(%rbp)
	movl	$1073741824, -2212(%rbp) ## imm = 0x40000000
	leaq	-2208(%rbp), %rax
	movq	%rax, -1208(%rbp)
	movq	__ZNSt3__112placeholders2_1E@GOTPCREL(%rip), %rax
	movq	%rax, -1216(%rbp)
	leaq	-2212(%rbp), %rax
	movq	%rax, -1224(%rbp)
	movq	-1208(%rbp), %rax
	movq	%rax, -1200(%rbp)
	movq	-1216(%rbp), %rcx
	movq	%rcx, -824(%rbp)
	movq	-1224(%rbp), %rdx
	movq	%rdx, -832(%rbp)
	leaq	-2192(%rbp), %rsi
	movq	%rsi, -1168(%rbp)
	movq	%rax, -1176(%rbp)
	movq	%rcx, -1184(%rbp)
	movq	%rdx, -1192(%rbp)
	movq	-1168(%rbp), %rax
	movq	-1176(%rbp), %rcx
	movq	-1184(%rbp), %rsi
	movq	%rax, -1136(%rbp)
	movq	%rcx, -1144(%rbp)
	movq	%rsi, -1152(%rbp)
	movq	%rdx, -1160(%rbp)
	movq	-1136(%rbp), %rax
	movq	-1144(%rbp), %rcx
	movq	%rcx, -1128(%rbp)
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rdx, (%rax)
	addq	$16, %rax
	movq	-1152(%rbp), %rcx
	movq	%rcx, -840(%rbp)
	movq	-840(%rbp), %rcx
	movq	-1160(%rbp), %rdx
	movq	%rdx, -848(%rbp)
	movq	-848(%rbp), %rdx
	movq	%rax, -1104(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	%rdx, -1120(%rbp)
	movq	-1104(%rbp), %rax
	movq	-1120(%rbp), %rcx
	movq	-1112(%rbp), %rdx
	movq	%rax, -1048(%rbp)
	movq	%rdx, -1056(%rbp)
	movq	%rcx, -1064(%rbp)
	movq	-1048(%rbp), %rax
	movq	-1056(%rbp), %rcx
	movq	%rcx, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	-1064(%rbp), %rdx
	movq	%rdx, -856(%rbp)
	movq	-856(%rbp), %rdx
	movq	%rax, -1016(%rbp)
	movq	%rcx, -1024(%rbp)
	movq	%rdx, -1032(%rbp)
	movq	-1016(%rbp), %rax
	movq	-1032(%rbp), %rcx
	movq	-1024(%rbp), %rdx
	movq	%rax, -960(%rbp)
	movq	%rdx, -968(%rbp)
	movq	%rcx, -976(%rbp)
	movq	-960(%rbp), %rax
	movq	%rax, %rcx
	movq	-968(%rbp), %rdx
	movq	%rdx, -920(%rbp)
	movq	-920(%rbp), %rdx
	movq	%rcx, -872(%rbp)
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rcx
	movq	%rcx, -864(%rbp)
	movq	-976(%rbp), %rcx
	movq	%rcx, -888(%rbp)
	movq	-888(%rbp), %rcx
	movq	%rax, -904(%rbp)
	movq	%rcx, -912(%rbp)
	movq	-904(%rbp), %rax
	movq	-912(%rbp), %rcx
	movq	%rcx, -896(%rbp)
	movq	-896(%rbp), %rcx
	movss	(%rcx), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, (%rax)
## BB#17:
Ltmp2:
	movq	-2176(%rbp), %rax
	movq	%rsp, %rcx
	movq	%rax, 16(%rcx)
	movq	-2192(%rbp), %rax
	movq	-2184(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	leaq	-2088(%rbp), %rdi
	callq	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvfEJRNS0_12placeholders4__phILi1EEEfEEEEvRT_T0_
Ltmp3:
	jmp	LBB0_18
LBB0_18:
	movq	__ZN6Gromit5sleepEcd@GOTPCREL(%rip), %rax
	movq	%rax, -2264(%rbp)
	movq	$0, -2256(%rbp)
	movb	$122, -2265(%rbp)
	movabsq	$4613937818241073152, %rax ## imm = 0x4008000000000000
	movq	%rax, -2280(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, -792(%rbp)
	movq	__ZNSt3__112placeholders2_1E@GOTPCREL(%rip), %rax
	movq	%rax, -800(%rbp)
	leaq	-2265(%rbp), %rax
	movq	%rax, -808(%rbp)
	leaq	-2280(%rbp), %rax
	movq	%rax, -816(%rbp)
	movq	-792(%rbp), %rax
	movq	%rax, -784(%rbp)
	movq	-800(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -312(%rbp)
	movq	-816(%rbp), %rsi
	movq	%rsi, -320(%rbp)
	leaq	-2248(%rbp), %rdi
	movq	%rdi, -744(%rbp)
	movq	%rax, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%rdx, -768(%rbp)
	movq	%rsi, -776(%rbp)
	movq	-744(%rbp), %rax
	movq	-752(%rbp), %rcx
	movq	-768(%rbp), %rdx
	movq	-760(%rbp), %rdi
	movq	%rax, -704(%rbp)
	movq	%rcx, -712(%rbp)
	movq	%rdi, -720(%rbp)
	movq	%rdx, -728(%rbp)
	movq	%rsi, -736(%rbp)
	movq	-704(%rbp), %rax
	movq	-712(%rbp), %rcx
	movq	%rcx, -696(%rbp)
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rdx, (%rax)
	addq	$16, %rax
	movq	-720(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	-728(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%rsi, -344(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rax, -664(%rbp)
	movq	%rcx, -672(%rbp)
	movq	%rdx, -680(%rbp)
	movq	%rsi, -688(%rbp)
	movq	-664(%rbp), %rax
	movq	-680(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	-672(%rbp), %rsi
	movq	%rax, -600(%rbp)
	movq	%rsi, -608(%rbp)
	movq	%rcx, -616(%rbp)
	movq	%rdx, -624(%rbp)
	movq	-600(%rbp), %rax
	movq	-608(%rbp), %rcx
	movq	%rcx, -592(%rbp)
	movq	-592(%rbp), %rcx
	movq	-616(%rbp), %rdx
	movq	%rdx, -352(%rbp)
	movq	-352(%rbp), %rdx
	movq	-624(%rbp), %rsi
	movq	%rsi, -360(%rbp)
	movq	-360(%rbp), %rsi
	movq	%rax, -560(%rbp)
	movq	%rcx, -568(%rbp)
	movq	%rdx, -576(%rbp)
	movq	%rsi, -584(%rbp)
	movq	-560(%rbp), %rax
	movq	-576(%rbp), %rcx
	movq	-584(%rbp), %rdx
	movq	-568(%rbp), %rsi
	movq	%rax, -496(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rcx, -512(%rbp)
	movq	%rdx, -520(%rbp)
	movq	-496(%rbp), %rax
	movq	%rax, %rcx
	movq	-504(%rbp), %rdx
	movq	%rdx, -456(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -368(%rbp)
	movq	%rax, %rcx
	movq	-512(%rbp), %rdx
	movq	%rdx, -392(%rbp)
	movq	-392(%rbp), %rdx
	movq	%rcx, -408(%rbp)
	movq	%rdx, -416(%rbp)
	movq	-408(%rbp), %rcx
	movq	-416(%rbp), %rdx
	movq	%rdx, -400(%rbp)
	movq	-400(%rbp), %rdx
	movb	(%rdx), %r8b
	movb	%r8b, (%rcx)
	addq	$8, %rax
	movq	-520(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rax, -440(%rbp)
	movq	%rcx, -448(%rbp)
	movq	-440(%rbp), %rax
	movq	-448(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movsd	(%rcx), %xmm0           ## xmm0 = mem[0],zero
	movsd	%xmm0, (%rax)
## BB#19:
Ltmp4:
	movq	-2224(%rbp), %rax
	movq	%rsp, %rcx
	movq	%rax, 24(%rcx)
	movq	-2232(%rbp), %rax
	movq	%rax, 16(%rcx)
	movq	-2248(%rbp), %rax
	movq	-2240(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	leaq	-2088(%rbp), %rdi
	callq	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvcdEJRNS0_12placeholders4__phILi1EEEcdEEEEvRT_T0_
Ltmp5:
	jmp	LBB0_20
LBB0_20:
	movq	__ZN6Gromit3sitEv@GOTPCREL(%rip), %rax
	movq	%rax, -2320(%rbp)
	movq	$0, -2312(%rbp)
	leaq	-2320(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	__ZNSt3__112placeholders2_1E@GOTPCREL(%rip), %rax
	movq	%rax, -296(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	leaq	-2304(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rcx, -272(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rdx
	movq	%rax, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	%rcx, -248(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	%rdx, (%rax)
	addq	$16, %rax
	movq	-248(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rax, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-208(%rbp), %rax
	movq	-216(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -24(%rbp)
## BB#21:
Ltmp6:
	movq	-2288(%rbp), %rax
	movq	%rsp, %rcx
	movq	%rax, 16(%rcx)
	movq	-2304(%rbp), %rax
	movq	-2296(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	leaq	-2088(%rbp), %rdi
	callq	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvvEJRNS0_12placeholders4__phILi1EEEEEEEvRT_T0_
Ltmp7:
	jmp	LBB0_22
LBB0_22:
	leaq	-2088(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev
	movl	-2060(%rbp), %eax
	addq	$2384, %rsp             ## imm = 0x950
	popq	%rbp
	retq
LBB0_23:
	leaq	-2088(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev
## BB#24:
	movq	-2112(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset0 = Ltmp8-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp9-Ltmp8                     ##   Call between Ltmp8 and Ltmp9
	.long	Lset1
Lset2 = Ltmp17-Lfunc_begin0             ##     jumps to Ltmp17
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp10-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp11-Ltmp10                   ##   Call between Ltmp10 and Ltmp11
	.long	Lset4
Lset5 = Ltmp12-Lfunc_begin0             ##     jumps to Ltmp12
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp15-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp7-Ltmp15                    ##   Call between Ltmp15 and Ltmp7
	.long	Lset7
Lset8 = Ltmp17-Lfunc_begin0             ##     jumps to Ltmp17
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp7-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Lfunc_end0-Ltmp7               ##   Call between Ltmp7 and Lfunc_end0
	.long	Lset10
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN6GromitC1Ei
	.weak_def_can_be_hidden	__ZN6GromitC1Ei
	.align	4, 0x90
__ZN6GromitC1Ei:                        ## @_ZN6GromitC1Ei
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-8(%rbp), %rdi
	movl	-12(%rbp), %esi
	callq	__ZN6GromitC2Ei
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FviEJRNS0_12placeholders4__phILi1EEEiEEEEvRT_T0_
	.weak_def_can_be_hidden	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FviEJRNS0_12placeholders4__phILi1EEEiEEEEvRT_T0_
	.align	4, 0x90
__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FviEJRNS0_12placeholders4__phILi1EEEiEEEEvRT_T0_: ## @_Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FviEJRNS0_12placeholders4__phILi1EEEiEEEEvRT_T0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$704, %rsp              ## imm = 0x2C0
	leaq	16(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-600(%rbp), %rdx
	movq	%rdi, -640(%rbp)
	movq	-640(%rbp), %rdi
	movq	%rdi, -648(%rbp)
	movq	-648(%rbp), %rdi
	movq	%rdi, -632(%rbp)
	movq	-632(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -608(%rbp)
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -584(%rbp)
	movq	%rsi, -592(%rbp)
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%rdx, -568(%rbp)
	movq	%rsi, -576(%rbp)
	movq	-568(%rbp), %rdx
	movq	-576(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-600(%rbp), %rdx
	movq	%rdx, -624(%rbp)
	movq	-624(%rbp), %rdx
	movq	%rdx, -656(%rbp)
	movq	-648(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-96(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	%rax, -680(%rbp)        ## 8-byte Spill
LBB2_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-664(%rbp), %rax
	leaq	-656(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB2_2
	jmp	LBB2_7
LBB2_2:                                 ##   in Loop: Header=BB2_1 Depth=1
	leaq	-656(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-656(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	%rax, -536(%rbp)
	movq	-528(%rbp), %rdx
	movq	%rdx, %rsi
	addq	$16, %rsi
	movq	%rax, -520(%rbp)
	leaq	-552(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	%rax, -320(%rbp)
	movq	-312(%rbp), %r8
	movq	%r8, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-200(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	%rax, (%r8)
	movq	%rdx, -496(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rdi, -512(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	%rdx, -472(%rbp)
	movq	-512(%rbp), %rsi
	movq	%rdx, -384(%rbp)
	movq	%rsi, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	-392(%rbp), %rdx
	movq	%rdx, -376(%rbp)
	movq	%rdx, -368(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -360(%rbp)
	movq	-504(%rbp), %rsi
	movq	%rsi, -336(%rbp)
	movq	%rsi, -328(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rax, -448(%rbp)
	movq	%rdx, -456(%rbp)
	movq	%rsi, -464(%rbp)
	movq	-448(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	-456(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	-440(%rbp), %rsi
	movq	%rsi, -432(%rbp)
	movq	-432(%rbp), %rsi
	movq	%rsi, -424(%rbp)
	movq	-424(%rbp), %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movq	(%rsi), %rsi
	addq	%rax, %rsi
	movq	%rdx, %rax
	andq	$1, %rax
	cmpq	$0, %rax
	movq	%rdx, -688(%rbp)        ## 8-byte Spill
	movq	%rsi, -696(%rbp)        ## 8-byte Spill
	je	LBB2_4
## BB#3:                                ##   in Loop: Header=BB2_1 Depth=1
	movq	-696(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	subq	$1, %rdx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
	jmp	LBB2_5
LBB2_4:                                 ##   in Loop: Header=BB2_1 Depth=1
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -704(%rbp)        ## 8-byte Spill
LBB2_5:                                 ## %_ZNSt3__16__bindIM6GromitFviEJRNS_12placeholders4__phILi1EEEiEEclIJRKNS_10unique_ptrIS1_NS_14default_deleteIS1_EEEEEEENS_13__bind_returnIS3_NS_5tupleIJS6_iEEENSH_IJDpOT_EEEXsr22__is_valid_bind_returnIS3_SI_SM_EE5valueEE4typeESL_.exit
                                        ##   in Loop: Header=BB2_1 Depth=1
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	-464(%rbp), %rcx
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movl	(%rcx), %esi
	movq	-696(%rbp), %rdi        ## 8-byte Reload
	callq	*%rax
## BB#6:                                ##   in Loop: Header=BB2_1 Depth=1
	leaq	-656(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB2_1
LBB2_7:
	addq	$704, %rsp              ## imm = 0x2C0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Gromit5speakEi
	.weak_definition	__ZN6Gromit5speakEi
	.align	4, 0x90
__ZN6Gromit5speakEi:                    ## @_ZN6Gromit5speakEi
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movq	-24(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	callq	__ZNK6Gromit4nameEv
Ltmp27:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp28:
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	jmp	LBB3_1
LBB3_1:
Ltmp29:
	leaq	L_.str(%rip), %rsi
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp30:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB3_2
LBB3_2:
	movl	-28(%rbp), %esi
Ltmp31:
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp32:
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	jmp	LBB3_3
LBB3_3:
	movq	-104(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp33:
	callq	*%rcx
Ltmp34:
	movq	%rax, -112(%rbp)        ## 8-byte Spill
	jmp	LBB3_4
LBB3_4:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB3_5
LBB3_5:
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$112, %rsp
	popq	%rbp
	retq
LBB3_6:
Ltmp35:
	leaq	-56(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#7:
	movq	-64(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset11 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset11
Lset12 = Ltmp27-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp27
	.long	Lset12
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset13 = Ltmp27-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset13
Lset14 = Ltmp34-Ltmp27                  ##   Call between Ltmp27 and Ltmp34
	.long	Lset14
Lset15 = Ltmp35-Lfunc_begin1            ##     jumps to Ltmp35
	.long	Lset15
	.byte	0                       ##   On action: cleanup
Lset16 = Ltmp34-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset16
Lset17 = Lfunc_end1-Ltmp34              ##   Call between Ltmp34 and Lfunc_end1
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvfEJRNS0_12placeholders4__phILi1EEEfEEEEvRT_T0_
	.weak_def_can_be_hidden	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvfEJRNS0_12placeholders4__phILi1EEEfEEEEvRT_T0_
	.align	4, 0x90
__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvfEJRNS0_12placeholders4__phILi1EEEfEEEEvRT_T0_: ## @_Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvfEJRNS0_12placeholders4__phILi1EEEfEEEEvRT_T0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp39:
	.cfi_def_cfa_offset 16
Ltmp40:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp41:
	.cfi_def_cfa_register %rbp
	subq	$704, %rsp              ## imm = 0x2C0
	leaq	16(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-600(%rbp), %rdx
	movq	%rdi, -640(%rbp)
	movq	-640(%rbp), %rdi
	movq	%rdi, -648(%rbp)
	movq	-648(%rbp), %rdi
	movq	%rdi, -632(%rbp)
	movq	-632(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -608(%rbp)
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rdx, -584(%rbp)
	movq	%rsi, -592(%rbp)
	movq	-584(%rbp), %rdx
	movq	-592(%rbp), %rsi
	movq	%rdx, -568(%rbp)
	movq	%rsi, -576(%rbp)
	movq	-568(%rbp), %rdx
	movq	-576(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-600(%rbp), %rdx
	movq	%rdx, -624(%rbp)
	movq	-624(%rbp), %rdx
	movq	%rdx, -656(%rbp)
	movq	-648(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-96(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	%rax, -680(%rbp)        ## 8-byte Spill
LBB4_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-664(%rbp), %rax
	leaq	-656(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB4_2
	jmp	LBB4_7
LBB4_2:                                 ##   in Loop: Header=BB4_1 Depth=1
	leaq	-656(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-656(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	%rax, -536(%rbp)
	movq	-528(%rbp), %rdx
	movq	%rdx, %rsi
	addq	$16, %rsi
	movq	%rax, -520(%rbp)
	leaq	-552(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	%rax, -320(%rbp)
	movq	-312(%rbp), %r8
	movq	%r8, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-200(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	%rax, (%r8)
	movq	%rdx, -496(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rdi, -512(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	%rdx, -472(%rbp)
	movq	-512(%rbp), %rsi
	movq	%rdx, -384(%rbp)
	movq	%rsi, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	-392(%rbp), %rdx
	movq	%rdx, -376(%rbp)
	movq	%rdx, -368(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -360(%rbp)
	movq	-504(%rbp), %rsi
	movq	%rsi, -336(%rbp)
	movq	%rsi, -328(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rax, -448(%rbp)
	movq	%rdx, -456(%rbp)
	movq	%rsi, -464(%rbp)
	movq	-448(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	-456(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	-440(%rbp), %rsi
	movq	%rsi, -432(%rbp)
	movq	-432(%rbp), %rsi
	movq	%rsi, -424(%rbp)
	movq	-424(%rbp), %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movq	(%rsi), %rsi
	addq	%rax, %rsi
	movq	%rdx, %rax
	andq	$1, %rax
	cmpq	$0, %rax
	movq	%rdx, -688(%rbp)        ## 8-byte Spill
	movq	%rsi, -696(%rbp)        ## 8-byte Spill
	je	LBB4_4
## BB#3:                                ##   in Loop: Header=BB4_1 Depth=1
	movq	-696(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	subq	$1, %rdx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
	jmp	LBB4_5
LBB4_4:                                 ##   in Loop: Header=BB4_1 Depth=1
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -704(%rbp)        ## 8-byte Spill
LBB4_5:                                 ## %_ZNSt3__16__bindIM6GromitFvfEJRNS_12placeholders4__phILi1EEEfEEclIJRKNS_10unique_ptrIS1_NS_14default_deleteIS1_EEEEEEENS_13__bind_returnIS3_NS_5tupleIJS6_fEEENSH_IJDpOT_EEEXsr22__is_valid_bind_returnIS3_SI_SM_EE5valueEE4typeESL_.exit
                                        ##   in Loop: Header=BB4_1 Depth=1
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	-464(%rbp), %rcx
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movss	(%rcx), %xmm0           ## xmm0 = mem[0],zero,zero,zero
	movq	-696(%rbp), %rdi        ## 8-byte Reload
	callq	*%rax
## BB#6:                                ##   in Loop: Header=BB4_1 Depth=1
	leaq	-656(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB4_1
LBB4_7:
	addq	$704, %rsp              ## imm = 0x2C0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Gromit3eatEf
	.weak_definition	__ZN6Gromit3eatEf
	.align	4, 0x90
__ZN6Gromit3eatEf:                      ## @_ZN6Gromit3eatEf
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp51:
	.cfi_def_cfa_offset 16
Ltmp52:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp53:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -24(%rbp)
	movss	%xmm0, -28(%rbp)
	movq	-24(%rbp), %rsi
	leaq	-56(%rbp), %rdi
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	callq	__ZNK6Gromit4nameEv
Ltmp42:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp43:
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	jmp	LBB5_1
LBB5_1:
Ltmp44:
	leaq	L_.str.2(%rip), %rsi
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp45:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB5_2
LBB5_2:
	movss	-28(%rbp), %xmm0        ## xmm0 = mem[0],zero,zero,zero
Ltmp46:
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEf
Ltmp47:
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	jmp	LBB5_3
LBB5_3:
	movq	-104(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp48:
	callq	*%rcx
Ltmp49:
	movq	%rax, -112(%rbp)        ## 8-byte Spill
	jmp	LBB5_4
LBB5_4:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB5_5
LBB5_5:
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$112, %rsp
	popq	%rbp
	retq
LBB5_6:
Ltmp50:
	leaq	-56(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#7:
	movq	-64(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset18 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset18
Lset19 = Ltmp42-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp42
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp42-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset20
Lset21 = Ltmp49-Ltmp42                  ##   Call between Ltmp42 and Ltmp49
	.long	Lset21
Lset22 = Ltmp50-Lfunc_begin2            ##     jumps to Ltmp50
	.long	Lset22
	.byte	0                       ##   On action: cleanup
Lset23 = Ltmp49-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset23
Lset24 = Lfunc_end2-Ltmp49              ##   Call between Ltmp49 and Lfunc_end2
	.long	Lset24
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvcdEJRNS0_12placeholders4__phILi1EEEcdEEEEvRT_T0_
	.weak_def_can_be_hidden	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvcdEJRNS0_12placeholders4__phILi1EEEcdEEEEvRT_T0_
	.align	4, 0x90
__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvcdEJRNS0_12placeholders4__phILi1EEEcdEEEEvRT_T0_: ## @_Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvcdEJRNS0_12placeholders4__phILi1EEEcdEEEEvRT_T0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp54:
	.cfi_def_cfa_offset 16
Ltmp55:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp56:
	.cfi_def_cfa_register %rbp
	subq	$752, %rsp              ## imm = 0x2F0
	leaq	16(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-648(%rbp), %rdx
	movq	%rdi, -688(%rbp)
	movq	-688(%rbp), %rdi
	movq	%rdi, -696(%rbp)
	movq	-696(%rbp), %rdi
	movq	%rdi, -680(%rbp)
	movq	-680(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -656(%rbp)
	movq	%rsi, -664(%rbp)
	movq	-664(%rbp), %rsi
	movq	%rdx, -632(%rbp)
	movq	%rsi, -640(%rbp)
	movq	-632(%rbp), %rdx
	movq	-640(%rbp), %rsi
	movq	%rdx, -616(%rbp)
	movq	%rsi, -624(%rbp)
	movq	-616(%rbp), %rdx
	movq	-624(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-648(%rbp), %rdx
	movq	%rdx, -672(%rbp)
	movq	-672(%rbp), %rdx
	movq	%rdx, -704(%rbp)
	movq	-696(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-96(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -712(%rbp)
	movq	%rax, -728(%rbp)        ## 8-byte Spill
LBB6_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-712(%rbp), %rax
	leaq	-704(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB6_2
	jmp	LBB6_7
LBB6_2:                                 ##   in Loop: Header=BB6_1 Depth=1
	leaq	-704(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-704(%rbp), %rax
	movq	%rax, -720(%rbp)
	movq	-728(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -576(%rbp)
	movq	%rax, -584(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, %rsi
	addq	$16, %rsi
	movq	%rax, -568(%rbp)
	leaq	-600(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	%rax, -320(%rbp)
	movq	-312(%rbp), %r8
	movq	%r8, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-200(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	%rax, (%r8)
	movq	%rdx, -544(%rbp)
	movq	%rsi, -552(%rbp)
	movq	%rdi, -560(%rbp)
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rdx
	movq	%rdx, -528(%rbp)
	movq	%rdx, -520(%rbp)
	movq	-560(%rbp), %rsi
	movq	%rdx, -416(%rbp)
	movq	%rsi, -424(%rbp)
	movq	$0, -432(%rbp)
	movq	-424(%rbp), %rdx
	movq	%rdx, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -392(%rbp)
	movq	-552(%rbp), %rsi
	movq	%rsi, -336(%rbp)
	movq	%rsi, -328(%rbp)
	movq	-560(%rbp), %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	-552(%rbp), %rdi
	movq	%rdi, -368(%rbp)
	addq	$8, %rdi
	movq	%rdi, -360(%rbp)
	movq	-560(%rbp), %r8
	movq	%rdi, -376(%rbp)
	movq	%r8, -384(%rbp)
	movq	-376(%rbp), %rdi
	movq	%rax, -488(%rbp)
	movq	%rdx, -496(%rbp)
	movq	%rsi, -504(%rbp)
	movq	%rdi, -512(%rbp)
	movq	-488(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	-496(%rbp), %rsi
	movq	%rsi, -480(%rbp)
	movq	-480(%rbp), %rsi
	movq	%rsi, -472(%rbp)
	movq	-472(%rbp), %rsi
	movq	%rsi, -464(%rbp)
	movq	-464(%rbp), %rsi
	movq	%rsi, -456(%rbp)
	movq	-456(%rbp), %rsi
	movq	(%rsi), %rsi
	addq	%rax, %rsi
	movq	%rdx, %rax
	andq	$1, %rax
	cmpq	$0, %rax
	movq	%rdx, -736(%rbp)        ## 8-byte Spill
	movq	%rsi, -744(%rbp)        ## 8-byte Spill
	je	LBB6_4
## BB#3:                                ##   in Loop: Header=BB6_1 Depth=1
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-736(%rbp), %rdx        ## 8-byte Reload
	subq	$1, %rdx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, -752(%rbp)        ## 8-byte Spill
	jmp	LBB6_5
LBB6_4:                                 ##   in Loop: Header=BB6_1 Depth=1
	movq	-736(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -752(%rbp)        ## 8-byte Spill
LBB6_5:                                 ## %_ZNSt3__16__bindIM6GromitFvcdEJRNS_12placeholders4__phILi1EEEcdEEclIJRKNS_10unique_ptrIS1_NS_14default_deleteIS1_EEEEEEENS_13__bind_returnIS3_NS_5tupleIJS6_cdEEENSH_IJDpOT_EEEXsr22__is_valid_bind_returnIS3_SI_SM_EE5valueEE4typeESL_.exit
                                        ##   in Loop: Header=BB6_1 Depth=1
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	-504(%rbp), %rcx
	movq	%rcx, -440(%rbp)
	movq	-440(%rbp), %rcx
	movb	(%rcx), %dl
	movq	-512(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movsd	(%rcx), %xmm0           ## xmm0 = mem[0],zero
	movq	-744(%rbp), %rdi        ## 8-byte Reload
	movsbl	%dl, %esi
	callq	*%rax
## BB#6:                                ##   in Loop: Header=BB6_1 Depth=1
	leaq	-704(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB6_1
LBB6_7:
	addq	$752, %rsp              ## imm = 0x2F0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Gromit5sleepEcd
	.weak_definition	__ZN6Gromit5sleepEcd
	.align	4, 0x90
__ZN6Gromit5sleepEcd:                   ## @_ZN6Gromit5sleepEcd
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp70:
	.cfi_def_cfa_offset 16
Ltmp71:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp72:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movb	%sil, %al
	movq	%rdi, -24(%rbp)
	movb	%al, -25(%rbp)
	movsd	%xmm0, -40(%rbp)
	movq	-24(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%rdi, -88(%rbp)         ## 8-byte Spill
	callq	__ZNK6Gromit4nameEv
Ltmp57:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp58:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB7_1
LBB7_1:
Ltmp59:
	leaq	L_.str.3(%rip), %rsi
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp60:
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	jmp	LBB7_2
LBB7_2:
	movsbl	-25(%rbp), %esi
Ltmp61:
	movq	-104(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp62:
	movq	%rax, -112(%rbp)        ## 8-byte Spill
	jmp	LBB7_3
LBB7_3:
Ltmp63:
	leaq	L_.str.4(%rip), %rsi
	movq	-112(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp64:
	movq	%rax, -120(%rbp)        ## 8-byte Spill
	jmp	LBB7_4
LBB7_4:
	movsd	-40(%rbp), %xmm0        ## xmm0 = mem[0],zero
Ltmp65:
	movq	-120(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEd
Ltmp66:
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	jmp	LBB7_5
LBB7_5:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp67:
	callq	*%rcx
Ltmp68:
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	jmp	LBB7_6
LBB7_6:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB7_7
LBB7_7:
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$144, %rsp
	popq	%rbp
	retq
LBB7_8:
Ltmp69:
	leaq	-64(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -72(%rbp)
	movl	%ecx, -76(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#9:
	movq	-72(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table7:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset25 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset25
Lset26 = Ltmp57-Lfunc_begin3            ##   Call between Lfunc_begin3 and Ltmp57
	.long	Lset26
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset27 = Ltmp57-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset27
Lset28 = Ltmp68-Ltmp57                  ##   Call between Ltmp57 and Ltmp68
	.long	Lset28
Lset29 = Ltmp69-Lfunc_begin3            ##     jumps to Ltmp69
	.long	Lset29
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp68-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset30
Lset31 = Lfunc_end3-Ltmp68              ##   Call between Ltmp68 and Lfunc_end3
	.long	Lset31
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvvEJRNS0_12placeholders4__phILi1EEEEEEEvRT_T0_
	.weak_def_can_be_hidden	__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvvEJRNS0_12placeholders4__phILi1EEEEEEEvRT_T0_
	.align	4, 0x90
__Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvvEJRNS0_12placeholders4__phILi1EEEEEEEvRT_T0_: ## @_Z5applyINSt3__16vectorINS0_10unique_ptrI6GromitNS0_14default_deleteIS3_EEEENS0_9allocatorIS6_EEEENS0_6__bindIMS3_FvvEJRNS0_12placeholders4__phILi1EEEEEEEvRT_T0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp73:
	.cfi_def_cfa_offset 16
Ltmp74:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp75:
	.cfi_def_cfa_register %rbp
	subq	$656, %rsp              ## imm = 0x290
	leaq	16(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-552(%rbp), %rdx
	movq	%rdi, -592(%rbp)
	movq	-592(%rbp), %rdi
	movq	%rdi, -600(%rbp)
	movq	-600(%rbp), %rdi
	movq	%rdi, -584(%rbp)
	movq	-584(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	%rdi, -560(%rbp)
	movq	%rsi, -568(%rbp)
	movq	-568(%rbp), %rsi
	movq	%rdx, -536(%rbp)
	movq	%rsi, -544(%rbp)
	movq	-536(%rbp), %rdx
	movq	-544(%rbp), %rsi
	movq	%rdx, -520(%rbp)
	movq	%rsi, -528(%rbp)
	movq	-520(%rbp), %rdx
	movq	-528(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-552(%rbp), %rdx
	movq	%rdx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -608(%rbp)
	movq	-600(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-96(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -616(%rbp)
	movq	%rax, -632(%rbp)        ## 8-byte Spill
LBB8_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-616(%rbp), %rax
	leaq	-608(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB8_2
	jmp	LBB8_7
LBB8_2:                                 ##   in Loop: Header=BB8_1 Depth=1
	leaq	-608(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-608(%rbp), %rax
	movq	%rax, -624(%rbp)
	movq	-632(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -480(%rbp)
	movq	%rax, -488(%rbp)
	movq	-480(%rbp), %rdx
	movq	%rdx, %rsi
	addq	$16, %rsi
	movq	%rax, -472(%rbp)
	leaq	-504(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	%rax, -320(%rbp)
	movq	-312(%rbp), %r8
	movq	%r8, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-200(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%r8, -144(%rbp)
	movq	%rax, -152(%rbp)
	movq	-144(%rbp), %r8
	movq	%rax, -136(%rbp)
	movq	%rax, (%r8)
	movq	%rdx, -448(%rbp)
	movq	%rsi, -456(%rbp)
	movq	%rdi, -464(%rbp)
	movq	-448(%rbp), %rax
	movq	-456(%rbp), %rdx
	movq	%rdx, -432(%rbp)
	movq	%rdx, -424(%rbp)
	movq	-464(%rbp), %rsi
	movq	%rdx, -352(%rbp)
	movq	%rsi, -360(%rbp)
	movq	$0, -368(%rbp)
	movq	-360(%rbp), %rdx
	movq	%rdx, -344(%rbp)
	movq	%rdx, -336(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, -328(%rbp)
	movq	%rax, -408(%rbp)
	movq	%rdx, -416(%rbp)
	movq	-408(%rbp), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	-416(%rbp), %rsi
	movq	%rsi, -400(%rbp)
	movq	-400(%rbp), %rsi
	movq	%rsi, -392(%rbp)
	movq	-392(%rbp), %rsi
	movq	%rsi, -384(%rbp)
	movq	-384(%rbp), %rsi
	movq	%rsi, -376(%rbp)
	movq	-376(%rbp), %rsi
	movq	(%rsi), %rsi
	addq	%rax, %rsi
	movq	%rdx, %rax
	andq	$1, %rax
	cmpq	$0, %rax
	movq	%rdx, -640(%rbp)        ## 8-byte Spill
	movq	%rsi, -648(%rbp)        ## 8-byte Spill
	je	LBB8_4
## BB#3:                                ##   in Loop: Header=BB8_1 Depth=1
	movq	-648(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-640(%rbp), %rdx        ## 8-byte Reload
	subq	$1, %rdx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, -656(%rbp)        ## 8-byte Spill
	jmp	LBB8_5
LBB8_4:                                 ##   in Loop: Header=BB8_1 Depth=1
	movq	-640(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -656(%rbp)        ## 8-byte Spill
LBB8_5:                                 ## %_ZNSt3__16__bindIM6GromitFvvEJRNS_12placeholders4__phILi1EEEEEclIJRKNS_10unique_ptrIS1_NS_14default_deleteIS1_EEEEEEENS_13__bind_returnIS3_NS_5tupleIJS6_EEENSH_IJDpOT_EEEXsr22__is_valid_bind_returnIS3_SI_SM_EE5valueEE4typeESL_.exit
                                        ##   in Loop: Header=BB8_1 Depth=1
	movq	-656(%rbp), %rax        ## 8-byte Reload
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	*%rax
## BB#6:                                ##   in Loop: Header=BB8_1 Depth=1
	leaq	-608(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB8_1
LBB8_7:
	addq	$656, %rsp              ## imm = 0x290
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Gromit3sitEv
	.weak_definition	__ZN6Gromit3sitEv
	.align	4, 0x90
__ZN6Gromit3sitEv:                      ## @_ZN6Gromit3sitEv
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp83:
	.cfi_def_cfa_offset 16
Ltmp84:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp85:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -24(%rbp)
	leaq	-48(%rbp), %rax
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	-72(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__ZNK6Gromit4nameEv
Ltmp76:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp77:
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	jmp	LBB9_1
LBB9_1:
Ltmp78:
	leaq	L_.str.5(%rip), %rsi
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp79:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB9_2
LBB9_2:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp80:
	callq	*%rcx
Ltmp81:
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	jmp	LBB9_3
LBB9_3:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB9_4
LBB9_4:
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$112, %rsp
	popq	%rbp
	retq
LBB9_5:
Ltmp82:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#6:
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table9:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset32 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset32
Lset33 = Ltmp76-Lfunc_begin4            ##   Call between Lfunc_begin4 and Ltmp76
	.long	Lset33
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp76-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset34
Lset35 = Ltmp81-Ltmp76                  ##   Call between Ltmp76 and Ltmp81
	.long	Lset35
Lset36 = Ltmp82-Lfunc_begin4            ##     jumps to Ltmp82
	.long	Lset36
	.byte	0                       ##   On action: cleanup
Lset37 = Ltmp81-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset37
Lset38 = Lfunc_end4-Ltmp81              ##   Call between Ltmp81 and Lfunc_end4
	.long	Lset38
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp86:
	.cfi_def_cfa_offset 16
Ltmp87:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp88:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6GromitC2Ei
	.weak_def_can_be_hidden	__ZN6GromitC2Ei
	.align	4, 0x90
__ZN6GromitC2Ei:                        ## @_ZN6GromitC2Ei
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp89:
	.cfi_def_cfa_offset 16
Ltmp90:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp91:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-8(%rbp), %rdi
	movl	-12(%rbp), %esi
	movl	%esi, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp92:
	.cfi_def_cfa_offset 16
Ltmp93:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp94:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp95:
	.cfi_def_cfa_offset 16
Ltmp96:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp97:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB13_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB13_3
LBB13_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB13_3:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB13_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB13_6
LBB13_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB13_6:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK6Gromit4nameEv
	.weak_def_can_be_hidden	__ZNK6Gromit4nameEv
	.align	4, 0x90
__ZNK6Gromit4nameEv:                    ## @_ZNK6Gromit4nameEv
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp101:
	.cfi_def_cfa_offset 16
Ltmp102:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp103:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, %rax
	movq	%rsi, -112(%rbp)
	movl	(%rsi), %esi
	leaq	-136(%rbp), %rcx
	movq	%rdi, -160(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -168(%rbp)        ## 8-byte Spill
	movq	%rcx, -176(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__19to_stringEi
	leaq	L_.str.1(%rip), %rax
	movq	%rax, -96(%rbp)
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -104(%rbp)
	movq	-96(%rbp), %rdx
Ltmp98:
	xorl	%esi, %esi
                                        ## 
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6insertEmPKc
Ltmp99:
	movq	%rax, -184(%rbp)        ## 8-byte Spill
	jmp	LBB14_1
LBB14_1:                                ## %.noexc
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	-160(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	movq	%rcx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, (%rcx)
	movq	8(%rsi), %rdi
	movq	%rdi, 8(%rcx)
	movq	16(%rsi), %rsi
	movq	%rsi, 16(%rcx)
	movq	-64(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB14_2:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB14_4
## BB#3:                                ##   in Loop: Header=BB14_2 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB14_2
LBB14_4:                                ## %_ZNSt3__1plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEPKS6_OS9_.exit
	jmp	LBB14_5
LBB14_5:
	leaq	-136(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$192, %rsp
	popq	%rbp
	retq
LBB14_6:
Ltmp100:
	leaq	-136(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -144(%rbp)
	movl	%ecx, -148(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#7:
	movq	-144(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table14:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset39 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset39
Lset40 = Ltmp98-Lfunc_begin5            ##   Call between Lfunc_begin5 and Ltmp98
	.long	Lset40
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset41 = Ltmp98-Lfunc_begin5            ## >> Call Site 2 <<
	.long	Lset41
Lset42 = Ltmp99-Ltmp98                  ##   Call between Ltmp98 and Ltmp99
	.long	Lset42
Lset43 = Ltmp100-Lfunc_begin5           ##     jumps to Ltmp100
	.long	Lset43
	.byte	0                       ##   On action: cleanup
Lset44 = Ltmp99-Lfunc_begin5            ## >> Call Site 3 <<
	.long	Lset44
Lset45 = Lfunc_end5-Ltmp99              ##   Call between Ltmp99 and Lfunc_end5
	.long	Lset45
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp109:
	.cfi_def_cfa_offset 16
Ltmp110:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp111:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp104:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp105:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB15_1
LBB15_1:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp106:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp107:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB15_3
LBB15_2:
Ltmp108:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB15_3:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table15:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset46 = Lfunc_begin6-Lfunc_begin6      ## >> Call Site 1 <<
	.long	Lset46
Lset47 = Ltmp104-Lfunc_begin6           ##   Call between Lfunc_begin6 and Ltmp104
	.long	Lset47
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp104-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset48
Lset49 = Ltmp107-Ltmp104                ##   Call between Ltmp104 and Ltmp107
	.long	Lset49
Lset50 = Ltmp108-Lfunc_begin6           ##     jumps to Ltmp108
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp107-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset51
Lset52 = Lfunc_end6-Ltmp107             ##   Call between Ltmp107 and Lfunc_end6
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp133:
	.cfi_def_cfa_offset 16
Ltmp134:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp135:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp112:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp113:
	jmp	LBB16_1
LBB16_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB16_3
	jmp	LBB16_26
LBB16_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB16_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB16_7
LBB16_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB16_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB16_8
	jmp	LBB16_13
LBB16_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp115:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp116:
	jmp	LBB16_9
LBB16_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp117:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp118:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB16_10
LBB16_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp119:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp120:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB16_12
LBB16_11:
Ltmp121:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB16_21
LBB16_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB16_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp122:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp123:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB16_15
LBB16_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB16_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp124:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp125:
	jmp	LBB16_17
LBB16_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB16_18
LBB16_18:
	jmp	LBB16_25
LBB16_19:
Ltmp114:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB16_22
LBB16_20:
Ltmp126:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB16_21
LBB16_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB16_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp127:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp128:
	jmp	LBB16_23
LBB16_23:
	callq	___cxa_end_catch
LBB16_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB16_25:
	jmp	LBB16_26
LBB16_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB16_24
LBB16_27:
Ltmp129:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp130:
	callq	___cxa_end_catch
Ltmp131:
	jmp	LBB16_28
LBB16_28:
	jmp	LBB16_29
LBB16_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB16_30:
Ltmp132:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table16:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset53 = Ltmp112-Lfunc_begin7           ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp113-Ltmp112                ##   Call between Ltmp112 and Ltmp113
	.long	Lset54
Lset55 = Ltmp114-Lfunc_begin7           ##     jumps to Ltmp114
	.long	Lset55
	.byte	5                       ##   On action: 3
Lset56 = Ltmp115-Lfunc_begin7           ## >> Call Site 2 <<
	.long	Lset56
Lset57 = Ltmp116-Ltmp115                ##   Call between Ltmp115 and Ltmp116
	.long	Lset57
Lset58 = Ltmp126-Lfunc_begin7           ##     jumps to Ltmp126
	.long	Lset58
	.byte	5                       ##   On action: 3
Lset59 = Ltmp117-Lfunc_begin7           ## >> Call Site 3 <<
	.long	Lset59
Lset60 = Ltmp120-Ltmp117                ##   Call between Ltmp117 and Ltmp120
	.long	Lset60
Lset61 = Ltmp121-Lfunc_begin7           ##     jumps to Ltmp121
	.long	Lset61
	.byte	3                       ##   On action: 2
Lset62 = Ltmp122-Lfunc_begin7           ## >> Call Site 4 <<
	.long	Lset62
Lset63 = Ltmp125-Ltmp122                ##   Call between Ltmp122 and Ltmp125
	.long	Lset63
Lset64 = Ltmp126-Lfunc_begin7           ##     jumps to Ltmp126
	.long	Lset64
	.byte	5                       ##   On action: 3
Lset65 = Ltmp125-Lfunc_begin7           ## >> Call Site 5 <<
	.long	Lset65
Lset66 = Ltmp127-Ltmp125                ##   Call between Ltmp125 and Ltmp127
	.long	Lset66
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset67 = Ltmp127-Lfunc_begin7           ## >> Call Site 6 <<
	.long	Lset67
Lset68 = Ltmp128-Ltmp127                ##   Call between Ltmp127 and Ltmp128
	.long	Lset68
Lset69 = Ltmp129-Lfunc_begin7           ##     jumps to Ltmp129
	.long	Lset69
	.byte	0                       ##   On action: cleanup
Lset70 = Ltmp128-Lfunc_begin7           ## >> Call Site 7 <<
	.long	Lset70
Lset71 = Ltmp130-Ltmp128                ##   Call between Ltmp128 and Ltmp130
	.long	Lset71
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset72 = Ltmp130-Lfunc_begin7           ## >> Call Site 8 <<
	.long	Lset72
Lset73 = Ltmp131-Ltmp130                ##   Call between Ltmp130 and Ltmp131
	.long	Lset73
Lset74 = Ltmp132-Lfunc_begin7           ##     jumps to Ltmp132
	.long	Lset74
	.byte	5                       ##   On action: 3
Lset75 = Ltmp131-Lfunc_begin7           ## >> Call Site 9 <<
	.long	Lset75
Lset76 = Lfunc_end7-Ltmp131             ##   Call between Ltmp131 and Lfunc_end7
	.long	Lset76
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp136:
	.cfi_def_cfa_offset 16
Ltmp137:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp138:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp142:
	.cfi_def_cfa_offset 16
Ltmp143:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp144:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB18_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB18_26
LBB18_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB18_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB18_5
LBB18_4:
	movq	$0, -368(%rbp)
LBB18_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB18_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB18_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB18_26
LBB18_8:
	jmp	LBB18_9
LBB18_9:
	cmpq	$0, -368(%rbp)
	jle	LBB18_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB18_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB18_13
LBB18_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB18_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp139:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp140:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB18_14
LBB18_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB18_15
LBB18_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB18_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB18_19
LBB18_17:
Ltmp141:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB18_27
LBB18_18:
	movl	$0, -416(%rbp)
LBB18_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB18_20
	jmp	LBB18_29
LBB18_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB18_26
	jmp	LBB18_28
LBB18_20:
	jmp	LBB18_21
LBB18_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB18_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB18_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB18_26
LBB18_24:
	jmp	LBB18_25
LBB18_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB18_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB18_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB18_28:
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table18:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset77 = Lfunc_begin8-Lfunc_begin8      ## >> Call Site 1 <<
	.long	Lset77
Lset78 = Ltmp139-Lfunc_begin8           ##   Call between Lfunc_begin8 and Ltmp139
	.long	Lset78
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset79 = Ltmp139-Lfunc_begin8           ## >> Call Site 2 <<
	.long	Lset79
Lset80 = Ltmp140-Ltmp139                ##   Call between Ltmp139 and Ltmp140
	.long	Lset80
Lset81 = Ltmp141-Lfunc_begin8           ##     jumps to Ltmp141
	.long	Lset81
	.byte	0                       ##   On action: cleanup
Lset82 = Ltmp140-Lfunc_begin8           ## >> Call Site 3 <<
	.long	Lset82
Lset83 = Lfunc_end8-Ltmp140             ##   Call between Ltmp140 and Lfunc_end8
	.long	Lset83
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp145:
	.cfi_def_cfa_offset 16
Ltmp146:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp147:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp148:
	.cfi_def_cfa_offset 16
Ltmp149:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp150:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp151:
	.cfi_def_cfa_offset 16
Ltmp152:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp153:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movb	%sil, %al
	leaq	-9(%rbp), %rsi
	movl	$1, %ecx
	movl	%ecx, %edx
	movq	%rdi, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp154:
	.cfi_def_cfa_offset 16
Ltmp155:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp156:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev: ## @_ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp157:
	.cfi_def_cfa_offset 16
Ltmp158:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp159:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -360(%rbp)        ## 8-byte Spill
	je	LBB24_9
## BB#1:
	movq	-360(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -368(%rbp)        ## 8-byte Spill
LBB24_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-336(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB24_8
## BB#3:                                ##   in Loop: Header=BB24_2 Depth=1
	movq	-368(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -296(%rbp)
	movq	-296(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-248(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -200(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rsi
	movq	%rsi, -136(%rbp)
	movq	-136(%rbp), %rsi
	movq	%rdx, (%rsi)
	cmpq	$0, -200(%rbp)
	movq	%rcx, -376(%rbp)        ## 8-byte Spill
	je	LBB24_7
## BB#4:                                ##   in Loop: Header=BB24_2 Depth=1
	movq	-376(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movq	%rcx, -152(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -384(%rbp)        ## 8-byte Spill
	je	LBB24_6
## BB#5:                                ##   in Loop: Header=BB24_2 Depth=1
	movq	-384(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB24_6:                                ## %_ZNKSt3__114default_deleteI6GromitEclEPS1_.exit.i.i.i.i.i.i.i.i
                                        ##   in Loop: Header=BB24_2 Depth=1
	jmp	LBB24_7
LBB24_7:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_10unique_ptrI6GromitNS_14default_deleteIS3_EEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i
                                        ##   in Loop: Header=BB24_2 Depth=1
	jmp	LBB24_2
LBB24_8:                                ## %_ZNSt3__113__vector_baseINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE5clearEv.exit
	movq	-360(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rdi
	callq	__ZdlPv
LBB24_9:
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC1ERKS8_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC1ERKS8_m
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC1ERKS8_m: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC1ERKS8_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp160:
	.cfi_def_cfa_offset 16
Ltmp161:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp162:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC2ERKS8_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp163:
	.cfi_def_cfa_offset 16
Ltmp164:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp165:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__emplace_back_slow_pathIJPS2_EEEvDpOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__emplace_back_slow_pathIJPS2_EEEvDpOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__emplace_back_slow_pathIJPS2_EEEvDpOT_: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__emplace_back_slow_pathIJPS2_EEEvDpOT_
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp169:
	.cfi_def_cfa_offset 16
Ltmp170:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp171:
	.cfi_def_cfa_register %rbp
	subq	$544, %rsp              ## imm = 0x220
	movq	%rdi, -448(%rbp)
	movq	%rsi, -456(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -440(%rbp)
	movq	-440(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -432(%rbp)
	movq	-432(%rbp), %rdi
	movq	%rdi, -424(%rbp)
	movq	-424(%rbp), %rdi
	movq	%rdi, -464(%rbp)
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	subq	%rdi, %rax
	sarq	$3, %rax
	addq	$1, %rax
	movq	%rsi, -376(%rbp)
	movq	%rax, -384(%rbp)
	movq	-376(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -528(%rbp)        ## 8-byte Spill
	movq	%rax, -536(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE8max_sizeEv
	movq	%rax, -392(%rbp)
	movq	-384(%rbp), %rax
	cmpq	-392(%rbp), %rax
	jbe	LBB27_2
## BB#1:
	movq	-536(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB27_2:
	movq	-536(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rdx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	-392(%rbp), %rdx
	shrq	$1, %rdx
	cmpq	%rdx, %rcx
	jb	LBB27_4
## BB#3:
	movq	-392(%rbp), %rax
	movq	%rax, -368(%rbp)
	jmp	LBB27_8
LBB27_4:
	leaq	-280(%rbp), %rax
	leaq	-384(%rbp), %rcx
	leaq	-408(%rbp), %rdx
	movq	-400(%rbp), %rsi
	shlq	$1, %rsi
	movq	%rsi, -408(%rbp)
	movq	%rdx, -304(%rbp)
	movq	%rcx, -312(%rbp)
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%rcx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	-288(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	-272(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB27_6
## BB#5:
	movq	-296(%rbp), %rax
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	jmp	LBB27_7
LBB27_6:
	movq	-288(%rbp), %rax
	movq	%rax, -544(%rbp)        ## 8-byte Spill
LBB27_7:                                ## %_ZNSt3__13maxImEERKT_S3_S3_.exit.i
	movq	-544(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -368(%rbp)
LBB27_8:                                ## %_ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE11__recommendEm.exit
	leaq	-504(%rbp), %rdi
	movq	-368(%rbp), %rsi
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	-464(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC1EmmS8_
	leaq	-24(%rbp), %rax
	leaq	-48(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	-488(%rbp), %rdi
	movq	%rdi, -240(%rbp)
	movq	-240(%rbp), %rdi
	movq	-456(%rbp), %r8
	movq	%r8, -232(%rbp)
	movq	-232(%rbp), %r8
	movq	%rsi, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	%r8, -208(%rbp)
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %r8
	movq	%r8, -184(%rbp)
	movq	-184(%rbp), %r8
	movq	%rsi, -160(%rbp)
	movq	%rdi, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	-160(%rbp), %rsi
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r8
	movq	%r8, -144(%rbp)
	movq	-144(%rbp), %r8
	movq	%rsi, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%r8, -136(%rbp)
	movq	-128(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rsi, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%rsi, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-80(%rbp), %rsi
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movq	%rdx, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
## BB#9:
	movq	-488(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -488(%rbp)
Ltmp166:
	leaq	-504(%rbp), %rsi
	movq	-528(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS5_RS7_EE
Ltmp167:
	jmp	LBB27_10
LBB27_10:
	leaq	-504(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev
	addq	$544, %rsp              ## imm = 0x220
	popq	%rbp
	retq
LBB27_11:
Ltmp168:
	leaq	-504(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -512(%rbp)
	movl	%ecx, -516(%rbp)
	callq	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev
## BB#12:
	movq	-512(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table27:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset84 = Lfunc_begin9-Lfunc_begin9      ## >> Call Site 1 <<
	.long	Lset84
Lset85 = Ltmp166-Lfunc_begin9           ##   Call between Lfunc_begin9 and Ltmp166
	.long	Lset85
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp166-Lfunc_begin9           ## >> Call Site 2 <<
	.long	Lset86
Lset87 = Ltmp167-Ltmp166                ##   Call between Ltmp166 and Ltmp167
	.long	Lset87
Lset88 = Ltmp168-Lfunc_begin9           ##     jumps to Ltmp168
	.long	Lset88
	.byte	0                       ##   On action: cleanup
Lset89 = Ltmp167-Lfunc_begin9           ## >> Call Site 3 <<
	.long	Lset89
Lset90 = Lfunc_end9-Ltmp167             ##   Call between Ltmp167 and Lfunc_end9
	.long	Lset90
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC2ERKS8_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC2ERKS8_m
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC2ERKS8_m: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE24__RAII_IncreaseAnnotatorC2ERKS8_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp172:
	.cfi_def_cfa_offset 16
Ltmp173:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp174:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC1EmmS8_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC1EmmS8_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC1EmmS8_: ## @_ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC1EmmS8_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp175:
	.cfi_def_cfa_offset 16
Ltmp176:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp177:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC2EmmS8_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS5_RS7_EE
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS5_RS7_EE
	.align	4, 0x90
__ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS5_RS7_EE: ## @_ZNSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS5_RS7_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp178:
	.cfi_def_cfa_offset 16
Ltmp179:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp180:
	.cfi_def_cfa_register %rbp
	subq	$672, %rsp              ## imm = 0x2A0
	movq	%rdi, -648(%rbp)
	movq	%rsi, -656(%rbp)
	movq	-648(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -664(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE17__annotate_deleteEv
	movq	-664(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -640(%rbp)
	movq	-640(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -632(%rbp)
	movq	-632(%rbp), %rsi
	movq	%rsi, -624(%rbp)
	movq	-624(%rbp), %rsi
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rcx
	movq	-656(%rbp), %rdx
	addq	$8, %rdx
	movq	%rsi, -584(%rbp)
	movq	%rax, -592(%rbp)
	movq	%rcx, -600(%rbp)
	movq	%rdx, -608(%rbp)
LBB30_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-600(%rbp), %rax
	cmpq	-592(%rbp), %rax
	je	LBB30_3
## BB#2:                                ##   in Loop: Header=BB30_1 Depth=1
	leaq	-272(%rbp), %rax
	leaq	-256(%rbp), %rcx
	leaq	-288(%rbp), %rdx
	leaq	-304(%rbp), %rsi
	movq	-584(%rbp), %rdi
	movq	-608(%rbp), %r8
	movq	(%r8), %r8
	addq	$-8, %r8
	movq	%r8, -576(%rbp)
	movq	-576(%rbp), %r8
	movq	-600(%rbp), %r9
	addq	$-8, %r9
	movq	%r9, -600(%rbp)
	movq	%r9, -216(%rbp)
	movq	-216(%rbp), %r9
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	%rdi, -536(%rbp)
	movq	%r8, -544(%rbp)
	movq	%r9, -552(%rbp)
	movq	-536(%rbp), %rdi
	movq	-544(%rbp), %r8
	movq	-552(%rbp), %r9
	movq	%r9, -528(%rbp)
	movq	-528(%rbp), %r9
	movq	%rdi, -504(%rbp)
	movq	%r8, -512(%rbp)
	movq	%r9, -520(%rbp)
	movq	-504(%rbp), %rdi
	movq	-512(%rbp), %r8
	movq	-520(%rbp), %r9
	movq	%r9, -488(%rbp)
	movq	-488(%rbp), %r9
	movq	%rdi, -464(%rbp)
	movq	%r8, -472(%rbp)
	movq	%r9, -480(%rbp)
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %r8
	movq	%r8, -456(%rbp)
	movq	-456(%rbp), %r8
	movq	%rdi, -440(%rbp)
	movq	%r8, -448(%rbp)
	movq	-440(%rbp), %rdi
	movq	-448(%rbp), %r8
	movq	%rdi, -416(%rbp)
	movq	%r8, -424(%rbp)
	movq	-416(%rbp), %rdi
	movq	-424(%rbp), %r8
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movq	%r8, -392(%rbp)
	movq	-392(%rbp), %r9
	movq	%r9, -384(%rbp)
	movq	-384(%rbp), %r9
	movq	(%r9), %r9
	movq	%r9, -408(%rbp)
	movq	%r8, -376(%rbp)
	movq	-376(%rbp), %r8
	movq	%r8, -368(%rbp)
	movq	-368(%rbp), %r8
	movq	$0, (%r8)
	movq	-408(%rbp), %r8
	movq	-424(%rbp), %r9
	movq	%r9, -360(%rbp)
	movq	-360(%rbp), %r9
	movq	%r9, -352(%rbp)
	movq	-352(%rbp), %r9
	movq	%r9, -344(%rbp)
	movq	-344(%rbp), %r9
	movq	%r9, -224(%rbp)
	movq	%rdi, -328(%rbp)
	movq	%r8, -336(%rbp)
	movq	-328(%rbp), %rdi
	movq	-336(%rbp), %r8
	movq	%rdi, -296(%rbp)
	movq	%r8, -304(%rbp)
	movq	-296(%rbp), %rdi
	movq	%rsi, -280(%rbp)
	movq	-280(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rdx, -232(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-608(%rbp), %rax
	movq	(%rax), %rcx
	addq	$-8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB30_1
LBB30_3:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_10unique_ptrI6GromitNS_14default_deleteIS3_EEEEEEE20__construct_backwardIPS6_EEvRS7_T_SC_RSC_.exit
	leaq	-192(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	-664(%rbp), %rsi        ## 8-byte Reload
	movq	-656(%rbp), %rdi
	addq	$8, %rdi
	movq	%rsi, -32(%rbp)
	movq	%rdi, -40(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-32(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-40(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-664(%rbp), %rdx        ## 8-byte Reload
	addq	$8, %rdx
	movq	-656(%rbp), %rsi
	addq	$16, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-80(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	-656(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rcx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-176(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-656(%rbp), %rax
	movq	8(%rax), %rax
	movq	-656(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	callq	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE14__annotate_newEm
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -616(%rbp)
	addq	$672, %rsp              ## imm = 0x2A0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev: ## @_ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp181:
	.cfi_def_cfa_offset 16
Ltmp182:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp183:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE8max_sizeEv: ## @_ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp184:
	.cfi_def_cfa_offset 16
Ltmp185:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp186:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$2305843009213693951, %r8 ## imm = 0x1FFFFFFFFFFFFFFF
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB32_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB32_3
LBB32_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB32_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC2EmmS8_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC2EmmS8_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC2EmmS8_: ## @_ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEEC2EmmS8_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp187:
	.cfi_def_cfa_offset 16
Ltmp188:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp189:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	leaq	-136(%rbp), %rax
	leaq	-168(%rbp), %r8
	movq	%rdi, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	-232(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	-160(%rbp), %rdx
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-176(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-144(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, 8(%rdx)
	cmpq	$0, -216(%rbp)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	je	LBB33_2
## BB#1:
	movq	-240(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	-216(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rcx
	shlq	$3, %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	jmp	LBB33_3
LBB33_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	jmp	LBB33_3
LBB33_3:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	-240(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-224(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	-216(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE17__annotate_deleteEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE17__annotate_deleteEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE17__annotate_deleteEv: ## @_ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE17__annotate_deleteEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp190:
	.cfi_def_cfa_offset 16
Ltmp191:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp192:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	subq	%rdx, %rsi
	sarq	$3, %rsi
	shlq	$3, %rsi
	addq	%rsi, %rcx
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	8(%rsi), %r8
	movq	(%rsi), %rsi
	subq	%rsi, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, -104(%rbp)
	movq	-104(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -96(%rbp)
	movq	-96(%rbp), %r9
	movq	%r9, -88(%rbp)
	movq	-88(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$3, %r9
	shlq	$3, %r9
	addq	%r9, %rsi
	movq	%rsi, -168(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	-168(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE14__annotate_newEm: ## @_ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp193:
	.cfi_def_cfa_offset 16
Ltmp194:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp195:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdx), %rdx
	subq	%rdx, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	-160(%rbp), %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-168(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-176(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_
	.align	4, 0x90
__ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_: ## @_ZNKSt3__16vectorINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEENS_9allocatorIS5_EEE31__annotate_contiguous_containerEPKvSA_SA_SA_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp196:
	.cfi_def_cfa_offset 16
Ltmp197:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp198:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED2Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED2Ev: ## @_ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp199:
	.cfi_def_cfa_offset 16
Ltmp200:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp201:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	movq	%rdi, -384(%rbp)
	movq	-384(%rbp), %rdi
	movq	%rdi, -376(%rbp)
	movq	-376(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	-352(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, -336(%rbp)
	movq	%rcx, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	%rdi, -392(%rbp)        ## 8-byte Spill
	movq	%rax, -400(%rbp)        ## 8-byte Spill
LBB37_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-344(%rbp), %rax
	movq	-400(%rbp), %rcx        ## 8-byte Reload
	cmpq	16(%rcx), %rax
	je	LBB37_7
## BB#2:                                ##   in Loop: Header=BB37_1 Depth=1
	movq	-400(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, -296(%rbp)
	movq	-296(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-248(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -200(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rsi
	movq	%rsi, -136(%rbp)
	movq	-136(%rbp), %rsi
	movq	%rdx, (%rsi)
	cmpq	$0, -200(%rbp)
	movq	%rcx, -408(%rbp)        ## 8-byte Spill
	je	LBB37_6
## BB#3:                                ##   in Loop: Header=BB37_1 Depth=1
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	-200(%rbp), %rdx
	movq	%rcx, -152(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -416(%rbp)        ## 8-byte Spill
	je	LBB37_5
## BB#4:                                ##   in Loop: Header=BB37_1 Depth=1
	movq	-416(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB37_5:                                ## %_ZNKSt3__114default_deleteI6GromitEclEPS1_.exit.i.i.i.i.i.i.i.i.i
                                        ##   in Loop: Header=BB37_1 Depth=1
	jmp	LBB37_6
LBB37_6:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_10unique_ptrI6GromitNS_14default_deleteIS3_EEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i
                                        ##   in Loop: Header=BB37_1 Depth=1
	jmp	LBB37_1
LBB37_7:                                ## %_ZNSt3__114__split_bufferINS_10unique_ptrI6GromitNS_14default_deleteIS2_EEEERNS_9allocatorIS5_EEE5clearEv.exit
	movq	-392(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, (%rax)
	je	LBB37_10
## BB#8:
	movq	-392(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	(%rax), %rdx
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	movq	%rdx, -432(%rbp)        ## 8-byte Spill
	movq	%rdi, -440(%rbp)        ## 8-byte Spill
## BB#9:
	movq	-424(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-432(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-440(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -56(%rbp)
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	-56(%rbp), %r8
	movq	%rsi, -16(%rbp)
	movq	%rdi, -24(%rbp)
	movq	%r8, -32(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZdlPv
LBB37_10:
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	" speaking "

L_.str.1:                               ## @.str.1
	.asciz	"Gromit "

L_.str.2:                               ## @.str.2
	.asciz	" eating "

L_.str.3:                               ## @.str.3
	.asciz	" sleeping "

L_.str.4:                               ## @.str.4
	.asciz	" "

L_.str.5:                               ## @.str.5
	.asciz	" sitting"


.subsections_via_symbols
