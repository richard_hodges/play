	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI0_0:
	.quad	4746794007240114176     ## double 2147483646
LCPI0_1:
	.quad	4886405595680210944     ## double 4.6116860098374533E+18
LCPI0_2:
	.quad	4701340746312056832     ## double 2.0E+6
LCPI0_3:
	.quad	-4526534890170089472    ## double -1.0E+6
	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI0_4:
	.space	16
	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z16build_test_arraym
	.align	4, 0x90
__Z16build_test_arraym:                 ## @_Z16build_test_arraym
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
Ltmp18:
	.cfi_offset %rbx, -56
Ltmp19:
	.cfi_offset %r12, -48
Ltmp20:
	.cfi_offset %r13, -40
Ltmp21:
	.cfi_offset %r14, -32
Ltmp22:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r12
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, (%r12)
	movq	$0, 16(%r12)
	vmovapd	%xmm0, -96(%rbp)
	movq	$0, -80(%rbp)
Ltmp0:
	leaq	L_.str(%rip), %rsi
	leaq	-96(%rbp), %rdi
	movl	$12, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp1:
## BB#1:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
Ltmp3:
	leaq	-72(%rbp), %rdi
	leaq	-96(%rbp), %rsi
	callq	__ZNSt3__113random_deviceC1ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp4:
## BB#2:
	leaq	-96(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp6:
	leaq	-72(%rbp), %rdi
	callq	__ZNSt3__113random_deviceclEv
Ltmp7:
## BB#3:
	movl	%eax, %ecx
	testq	%r14, %r14
	je	LBB0_10
## BB#4:                                ## %.lr.ph.i
	leaq	(%rcx,%rcx,2), %rcx
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$30, %edx
	imull	$2147483647, %edx, %ecx ## imm = 0x7FFFFFFF
	subl	%ecx, %eax
	movl	$1, %r13d
	cmovnel	%eax, %r13d
	leaq	-64(%rbp), %r15
	.align	4, 0x90
LBB0_5:                                 ## =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	movl	$3163493265, %edi       ## imm = 0xBC8F1391
	imulq	%rdi, %rax
	shrq	$47, %rax
	imull	$44488, %eax, %ecx      ## imm = 0xADC8
	subl	%ecx, %r13d
	imull	$48271, %r13d, %ecx     ## imm = 0xBC8F
	imull	$3399, %eax, %eax       ## imm = 0xD47
	subl	%eax, %ecx
	movl	$0, %eax
	movl	$2147483647, %r8d       ## imm = 0x7FFFFFFF
	cmovbl	%r8d, %eax
	leal	(%rax,%rcx), %edx
	movq	%rdx, %rsi
	imulq	%rdi, %rsi
	shrq	$47, %rsi
	imull	$44488, %esi, %edi      ## imm = 0xADC8
	subl	%edi, %edx
	imull	$48271, %edx, %ebx      ## imm = 0xBC8F
	imull	$3399, %esi, %edx       ## imm = 0xD47
	leal	-1(%rax,%rcx), %eax
	vcvtsi2sdq	%rax, %xmm0, %xmm0
	subl	%edx, %ebx
	movl	$0, %r13d
	cmovbl	%r8d, %r13d
	leal	-1(%r13,%rbx), %eax
	vcvtsi2sdq	%rax, %xmm0, %xmm1
	vmulsd	LCPI0_0(%rip), %xmm1, %xmm1
	vaddsd	%xmm1, %xmm0, %xmm0
	vdivsd	LCPI0_1(%rip), %xmm0, %xmm0
	vmulsd	LCPI0_2(%rip), %xmm0, %xmm0
	vaddsd	LCPI0_3(%rip), %xmm0, %xmm0
Ltmp9:
	movq	%r15, %rdi
	callq	__ZNSt3__19to_stringEd
Ltmp10:
## BB#6:                                ## %.noexc
                                        ##   in Loop: Header=BB0_5 Depth=1
	movq	8(%r12), %rax
	cmpq	16(%r12), %rax
	jae	LBB0_8
## BB#7:                                ##   in Loop: Header=BB0_5 Depth=1
	movq	-48(%rbp), %rcx
	movq	%rcx, 16(%rax)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rax)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	addq	$24, 8(%r12)
	jmp	LBB0_9
	.align	4, 0x90
LBB0_8:                                 ##   in Loop: Header=BB0_5 Depth=1
Ltmp12:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_
Ltmp13:
LBB0_9:                                 ## %_ZNSt3__120back_insert_iteratorINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS5_IS7_EEEEEaSEOS7_.exit.i
                                        ##   in Loop: Header=BB0_5 Depth=1
	addl	%ebx, %r13d
	movq	%r15, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$-1, %r14
	jne	LBB0_5
LBB0_10:                                ## %.loopexit4
	leaq	-72(%rbp), %rdi
	callq	__ZNSt3__113random_deviceD1Ev
	movq	%r12, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_13:                                ## %.loopexit
Ltmp11:
LBB0_15:                                ## %.body
	movq	%rax, %r14
	jmp	LBB0_16
LBB0_23:
Ltmp14:
	movq	%rax, %r14
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_16:                                ## %.body
	leaq	-72(%rbp), %rdi
	callq	__ZNSt3__113random_deviceD1Ev
LBB0_17:
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	LBB0_22
## BB#18:
	movq	8(%r12), %rdi
	cmpq	%rbx, %rdi
	je	LBB0_21
	.align	4, 0x90
LBB0_19:                                ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, 8(%r12)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	8(%r12), %rdi
	cmpq	%rbx, %rdi
	jne	LBB0_19
## BB#20:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i
	movq	(%r12), %rbx
LBB0_21:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB0_22:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB0_11:
Ltmp2:
	movq	%rax, %r14
	jmp	LBB0_17
LBB0_12:
Ltmp5:
	movq	%rax, %r14
	leaq	-96(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB0_17
LBB0_14:                                ## %.loopexit.split-lp
Ltmp8:
	jmp	LBB0_15
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset1
Lset2 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp3-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset4
Lset5 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp6-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp7-Ltmp6                     ##   Call between Ltmp6 and Ltmp7
	.long	Lset7
Lset8 = Ltmp8-Lfunc_begin0              ##     jumps to Ltmp8
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp9-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp10-Ltmp9                   ##   Call between Ltmp9 and Ltmp10
	.long	Lset10
Lset11 = Ltmp11-Lfunc_begin0            ##     jumps to Ltmp11
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp12-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset12
Lset13 = Ltmp13-Ltmp12                  ##   Call between Ltmp12 and Ltmp13
	.long	Lset13
Lset14 = Ltmp14-Lfunc_begin0            ##     jumps to Ltmp14
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp13-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset15
Lset16 = Lfunc_end0-Ltmp13              ##   Call between Ltmp13 and Lfunc_end0
	.long	Lset16
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp60:
	.cfi_def_cfa_offset 16
Ltmp61:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp62:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
Ltmp63:
	.cfi_offset %rbx, -56
Ltmp64:
	.cfi_offset %r12, -48
Ltmp65:
	.cfi_offset %r13, -40
Ltmp66:
	.cfi_offset %r14, -32
Ltmp67:
	.cfi_offset %r15, -24
	leaq	l_.ref.tmp(%rip), %rbx
	leaq	-80(%rbp), %r14
	leaq	-104(%rbp), %r12
LBB1_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB1_19 Depth 2
                                        ##     Child Loop BB1_24 Depth 2
	movl	(%rbx), %r15d
	movslq	%r15d, %rsi
	movq	%r14, %rdi
	callq	__Z16build_test_arraym
Ltmp23:
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ERKS8_
Ltmp24:
## BB#2:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC1ERKS8_.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, -160(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	movq	%rax, %r12
Ltmp26:
	movq	%r14, %rdi
	callq	__Z30sort_numeric_single_conversionINSt3__16vectorINS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEENS5_IS7_EEEEEvRT_
Ltmp27:
## BB#3:                                ##   in Loop: Header=BB1_1 Depth=1
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	movq	%rax, %rbx
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	movq	%rax, %r14
	movq	-104(%rbp), %rdi
	movq	-96(%rbp), %rsi
Ltmp28:
	leaq	-56(%rbp), %rdx
	callq	__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
Ltmp29:
## BB#4:                                ##   in Loop: Header=BB1_1 Depth=1
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	movq	%rax, %r13
Ltmp30:
	movl	$6, %edx
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.1(%rip), %rsi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp31:
## BB#5:                                ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	movq	$7, 24(%rax,%rcx)
Ltmp32:
	movq	%rax, %rdi
	movl	%r15d, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp33:
## BB#6:                                ##   in Loop: Header=BB1_1 Depth=1
Ltmp34:
	movl	$9, %edx
	movq	%rax, %rdi
	leaq	L_.str.2(%rip), %rsi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %r15
Ltmp35:
## BB#7:                                ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit7
                                        ##   in Loop: Header=BB1_1 Depth=1
	subq	%r12, %rbx
Ltmp36:
	leaq	-128(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_
Ltmp37:
## BB#8:                                ##   in Loop: Header=BB1_1 Depth=1
	movzbl	-128(%rbp), %edx
	movb	%dl, %al
	andb	$1, %al
	movq	-112(%rbp), %rsi
	leaq	-127(%rbp), %rcx
	cmoveq	%rcx, %rsi
	shrq	%rdx
	testb	%al, %al
	cmovneq	-120(%rbp), %rdx
Ltmp39:
	movq	%r15, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp40:
## BB#9:                                ## %_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
Ltmp41:
	movl	$9, %edx
	movq	%rax, %rdi
	leaq	L_.str.3(%rip), %rsi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp42:
## BB#10:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit8
                                        ##   in Loop: Header=BB1_1 Depth=1
	subq	%r14, %r13
Ltmp43:
	leaq	-152(%rbp), %rdi
	movq	%r13, %rsi
	callq	__Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_
Ltmp44:
## BB#11:                               ##   in Loop: Header=BB1_1 Depth=1
	movzbl	-152(%rbp), %edx
	movb	%dl, %al
	andb	$1, %al
	movq	-136(%rbp), %rsi
	leaq	-151(%rbp), %rcx
	cmoveq	%rcx, %rsi
	shrq	%rdx
	testb	%al, %al
	cmovneq	-144(%rbp), %rdx
Ltmp46:
	movq	%rbx, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %r14
Ltmp47:
	leaq	-104(%rbp), %r12
	leaq	-48(%rbp), %r15
## BB#12:                               ## %_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE.exit9
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	(%r14), %rax
	movq	-24(%rax), %rsi
	addq	%r14, %rsi
Ltmp48:
	movq	%r15, %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp49:
## BB#13:                               ## %.noexc30
                                        ##   in Loop: Header=BB1_1 Depth=1
Ltmp50:
	movq	%r15, %rdi
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp51:
## BB#14:                               ##   in Loop: Header=BB1_1 Depth=1
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp52:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %bl
Ltmp53:
## BB#15:                               ## %.noexc
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	%r15, %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp55:
	movsbl	%bl, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp56:
## BB#16:                               ## %.noexc10
                                        ##   in Loop: Header=BB1_1 Depth=1
Ltmp57:
	movq	%r14, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp58:
## BB#17:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
	leaq	-152(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-128(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	leaq	-80(%rbp), %r14
	je	LBB1_22
## BB#18:                               ##   in Loop: Header=BB1_1 Depth=1
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB1_21
	.align	4, 0x90
LBB1_19:                                ## %.lr.ph.i.i.i.i.i.12
                                        ##   Parent Loop BB1_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	addq	$-24, %rdi
	movq	%rdi, -96(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB1_19
## BB#20:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i.15
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	-104(%rbp), %rbx
LBB1_21:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i.16
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB1_22:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit17
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	-80(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB1_27
## BB#23:                               ##   in Loop: Header=BB1_1 Depth=1
	movq	-72(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB1_26
	.align	4, 0x90
LBB1_24:                                ## %.lr.ph.i.i.i.i.i.18
                                        ##   Parent Loop BB1_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	addq	$-24, %rdi
	movq	%rdi, -72(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-72(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB1_24
## BB#25:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i.21
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	-80(%rbp), %rbx
LBB1_26:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i.22
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB1_27:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit23
                                        ##   in Loop: Header=BB1_1 Depth=1
	movq	-160(%rbp), %rbx        ## 8-byte Reload
	addq	$4, %rbx
	leaq	l_.ref.tmp+28(%rip), %rax
	cmpq	%rax, %rbx
	jne	LBB1_1
## BB#28:
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB1_30:
Ltmp38:
	movq	%rax, %r14
	jmp	LBB1_35
LBB1_32:
Ltmp59:
	movq	%rax, %r14
	jmp	LBB1_33
LBB1_31:
Ltmp45:
	movq	%rax, %r14
	jmp	LBB1_34
LBB1_46:
Ltmp54:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB1_33:                                ## %.body
	leaq	-152(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB1_34:
	leaq	-128(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB1_35:
	movq	-104(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB1_40
## BB#36:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB1_39
	.align	4, 0x90
LBB1_37:                                ## %.lr.ph.i.i.i.i.i.24
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, -96(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB1_37
## BB#38:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i.27
	movq	-104(%rbp), %rbx
LBB1_39:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i.28
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB1_40:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit29
	movq	-80(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB1_45
## BB#41:
	movq	-72(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB1_44
	.align	4, 0x90
LBB1_42:                                ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, -72(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-72(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB1_42
## BB#43:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i
	movq	-80(%rbp), %rbx
LBB1_44:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB1_45:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB1_29:
Ltmp25:
	movq	%rax, %r14
	jmp	LBB1_40
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\352\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	104                     ## Call site table length
Lset17 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset17
Lset18 = Ltmp23-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp23
	.long	Lset18
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset19 = Ltmp23-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset19
Lset20 = Ltmp24-Ltmp23                  ##   Call between Ltmp23 and Ltmp24
	.long	Lset20
Lset21 = Ltmp25-Lfunc_begin1            ##     jumps to Ltmp25
	.long	Lset21
	.byte	0                       ##   On action: cleanup
Lset22 = Ltmp26-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset22
Lset23 = Ltmp37-Ltmp26                  ##   Call between Ltmp26 and Ltmp37
	.long	Lset23
Lset24 = Ltmp38-Lfunc_begin1            ##     jumps to Ltmp38
	.long	Lset24
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp39-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset25
Lset26 = Ltmp44-Ltmp39                  ##   Call between Ltmp39 and Ltmp44
	.long	Lset26
Lset27 = Ltmp45-Lfunc_begin1            ##     jumps to Ltmp45
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp46-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset28
Lset29 = Ltmp49-Ltmp46                  ##   Call between Ltmp46 and Ltmp49
	.long	Lset29
Lset30 = Ltmp59-Lfunc_begin1            ##     jumps to Ltmp59
	.long	Lset30
	.byte	0                       ##   On action: cleanup
Lset31 = Ltmp50-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset31
Lset32 = Ltmp53-Ltmp50                  ##   Call between Ltmp50 and Ltmp53
	.long	Lset32
Lset33 = Ltmp54-Lfunc_begin1            ##     jumps to Ltmp54
	.long	Lset33
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp55-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset34
Lset35 = Ltmp58-Ltmp55                  ##   Call between Ltmp55 and Ltmp58
	.long	Lset35
Lset36 = Ltmp59-Lfunc_begin1            ##     jumps to Ltmp59
	.long	Lset36
	.byte	0                       ##   On action: cleanup
Lset37 = Ltmp58-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset37
Lset38 = Lfunc_end1-Ltmp58              ##   Call between Ltmp58 and Lfunc_end1
	.long	Lset38
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_
	.weak_def_can_be_hidden	__Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_
	.align	4, 0x90
__Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_: ## @_Z5to_msINSt3__16chrono8durationIxNS0_5ratioILl1ELl1000000000EEEEEENS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEET_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp84:
	.cfi_def_cfa_offset 16
Ltmp85:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp86:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp              ## imm = 0x128
Ltmp87:
	.cfi_offset %rbx, -56
Ltmp88:
	.cfi_offset %r12, -48
Ltmp89:
	.cfi_offset %r13, -40
Ltmp90:
	.cfi_offset %r14, -32
Ltmp91:
	.cfi_offset %r15, -24
	movabsq	$4835703278458516699, %rcx ## imm = 0x431BDE82D7B634DB
	movq	%rsi, %rax
	imulq	%rcx
	movq	%rdx, %rbx
	movq	%rdi, %r13
	leaq	-216(%rbp), %rdi
	leaq	-320(%rbp), %r15
	movq	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, -328(%rbp)
	addq	$64, %rax
	movq	%rax, -216(%rbp)
Ltmp68:
	movq	%r15, %rsi
	callq	__ZNSt3__18ios_base4initEPv
Ltmp69:
## BB#1:
	movq	$0, -80(%rbp)
	movl	$-1, -72(%rbp)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r14
	leaq	24(%r14), %r12
	movq	%r12, -328(%rbp)
	addq	$64, %r14
	movq	%r14, -216(%rbp)
Ltmp71:
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp72:
	movq	%r15, %rdi
## BB#2:                                ## %.noexc.i
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r15
	addq	$16, %r15
	movq	%r15, -320(%rbp)
	vxorps	%ymm0, %ymm0, %ymm0
	vmovups	%ymm0, -256(%rbp)
	movl	$16, -224(%rbp)
	vxorps	%xmm0, %xmm0, %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
Ltmp74:
	leaq	-64(%rbp), %rsi
	vzeroupper
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp75:
## BB#3:
	movq	%rbx, %rax
	shrq	$63, %rax
	sarq	$18, %rbx
	addq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-328(%rbp), %rax
	movq	-24(%rax), %rax
	movq	$7, -304(%rbp,%rax)
Ltmp77:
	leaq	-328(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEx
Ltmp78:
## BB#4:
Ltmp79:
	leaq	L_.str.4(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp80:
## BB#5:                                ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
Ltmp81:
	movq	%r13, %rdi
	leaq	-320(%rbp), %rsi
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp82:
## BB#6:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit
	movq	%r12, -328(%rbp)
	movq	%r14, -216(%rbp)
	movq	%r15, -320(%rbp)
	leaq	-256(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-320(%rbp), %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	-328(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%r13, %rax
	addq	$296, %rsp              ## imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB2_10:
Ltmp83:
	movq	%rax, %rbx
	movq	%r12, -328(%rbp)
	movq	%r14, -216(%rbp)
	movq	%r15, -320(%rbp)
	jmp	LBB2_11
LBB2_8:
Ltmp70:
	movq	%rax, %rbx
	jmp	LBB2_13
LBB2_9:
Ltmp73:
	movq	%rax, %rbx
	jmp	LBB2_12
LBB2_7:
Ltmp76:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB2_11:                                ## %unwind_resume
	leaq	-256(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-320(%rbp), %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
LBB2_12:                                ## %unwind_resume
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	-328(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB2_13:                                ## %unwind_resume
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset39 = Ltmp68-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset39
Lset40 = Ltmp69-Ltmp68                  ##   Call between Ltmp68 and Ltmp69
	.long	Lset40
Lset41 = Ltmp70-Lfunc_begin2            ##     jumps to Ltmp70
	.long	Lset41
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp71-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset42
Lset43 = Ltmp72-Ltmp71                  ##   Call between Ltmp71 and Ltmp72
	.long	Lset43
Lset44 = Ltmp73-Lfunc_begin2            ##     jumps to Ltmp73
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp74-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset45
Lset46 = Ltmp75-Ltmp74                  ##   Call between Ltmp74 and Ltmp75
	.long	Lset46
Lset47 = Ltmp76-Lfunc_begin2            ##     jumps to Ltmp76
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp77-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset48
Lset49 = Ltmp82-Ltmp77                  ##   Call between Ltmp77 and Ltmp82
	.long	Lset49
Lset50 = Ltmp83-Lfunc_begin2            ##     jumps to Ltmp83
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp82-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset51
Lset52 = Lfunc_end2-Ltmp82              ##   Call between Ltmp82 and Lfunc_end2
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI4_0:
	.space	16
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z30sort_numeric_single_conversionINSt3__16vectorINS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEENS5_IS7_EEEEEvRT_
	.weak_def_can_be_hidden	__Z30sort_numeric_single_conversionINSt3__16vectorINS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEENS5_IS7_EEEEEvRT_
	.align	4, 0x90
__Z30sort_numeric_single_conversionINSt3__16vectorINS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEENS5_IS7_EEEEEvRT_: ## @_Z30sort_numeric_single_conversionINSt3__16vectorINS0_12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEENS5_IS7_EEEEEvRT_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp106:
	.cfi_def_cfa_offset 16
Ltmp107:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp108:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
Ltmp109:
	.cfi_offset %rbx, -56
Ltmp110:
	.cfi_offset %r12, -48
Ltmp111:
	.cfi_offset %r13, -40
Ltmp112:
	.cfi_offset %r14, -32
Ltmp113:
	.cfi_offset %r15, -24
	movq	%rdi, %r14
	movq	(%r14), %rbx
	movq	8(%r14), %r12
	movq	%r12, %r15
	subq	%rbx, %r15
	sarq	$3, %r15
	movabsq	$-6148914691236517205, %r13 ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%r13, %r15
	xorl	%esi, %esi
	movq	%r12, %rax
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	subq	%rbx, %rax
	movq	%rax, -120(%rbp)        ## 8-byte Spill
	je	LBB4_4
## BB#1:
	xorl	%esi, %esi
	cmpq	%rbx, %r12
	je	LBB4_3
## BB#2:
	movq	%r15, %rdi
	shlq	$4, %rdi
Ltmp92:
	callq	__Znwm
	movq	%rax, %rsi
Ltmp93:
LBB4_3:                                 ## %_ZNSt3__114__split_bufferINS_5tupleIJdmEEERNS_9allocatorIS2_EEE5clearEv.exit.i.i.i
	movq	%r15, %rax
	shlq	$4, %rax
	addq	%rsi, %rax
	movq	%rsi, -64(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rax, -48(%rbp)
LBB4_4:                                 ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE7reserveEm.exit.preheader
	cmpq	%r12, %rbx
	je	LBB4_5
## BB#9:                                ## %.lr.ph28
	movq	%r15, -128(%rbp)        ## 8-byte Spill
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rbx, %r15
	.align	4, 0x90
LBB4_10:                                ## =>This Inner Loop Header: Depth=1
Ltmp95:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
Ltmp96:
## BB#11:                               ##   in Loop: Header=BB4_10 Depth=1
	movq	%r15, %rax
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%r13, %rax
	vmovsd	%xmm0, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	-48(%rbp), %rcx
	jae	LBB4_13
## BB#12:                               ##   in Loop: Header=BB4_10 Depth=1
	vmovsd	%xmm0, (%rcx)
	movq	%rax, 8(%rcx)
	addq	$16, %rcx
	movq	%rcx, -56(%rbp)
	jmp	LBB4_14
	.align	4, 0x90
LBB4_13:                                ##   in Loop: Header=BB4_10 Depth=1
Ltmp97:
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	callq	__ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE24__emplace_back_slow_pathIJdlEEEvDpOT_
Ltmp98:
LBB4_14:                                ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE12emplace_backIJdlEEEvDpOT_.exit
                                        ##   in Loop: Header=BB4_10 Depth=1
	addq	$24, %r15
	cmpq	%r15, %r12
	jne	LBB4_10
## BB#15:                               ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE7reserveEm.exit._crit_edge.loopexit
	movq	-64(%rbp), %rdi
	movq	-56(%rbp), %rsi
	movq	-128(%rbp), %r15        ## 8-byte Reload
	jmp	LBB4_16
LBB4_5:                                 ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE7reserveEm.exit.preheader._ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE7reserveEm.exit._crit_edge_crit_edge
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
LBB4_16:                                ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE7reserveEm.exit._crit_edge
	leaq	-112(%rbp), %rdx
	callq	__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm0, -112(%rbp)
	movq	$0, -96(%rbp)
	xorl	%ecx, %ecx
	movl	$0, %eax
	cmpq	%rbx, %r12
	je	LBB4_20
## BB#17:
	xorl	%eax, %eax
	cmpq	%rbx, %r12
	je	LBB4_19
## BB#18:
Ltmp100:
	movq	-120(%rbp), %rdi        ## 8-byte Reload
	callq	__Znwm
Ltmp101:
LBB4_19:                                ## %_ZNSt3__114__split_bufferINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS4_IS6_EEE5clearEv.exit.i.i.i
	leaq	(%r15,%r15,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rcx, -96(%rbp)
LBB4_20:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE7reserveEm.exit
	movq	-64(%rbp), %rbx
	movq	-56(%rbp), %r12
	cmpq	%r12, %rbx
	je	LBB4_24
## BB#21:
	leaq	-112(%rbp), %r15
	jmp	LBB4_22
	.align	4, 0x90
LBB4_41:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9push_backEOS6_.exit._crit_edge
                                        ##   in Loop: Header=BB4_22 Depth=1
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rcx
LBB4_22:                                ## =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdx
	leaq	(%rdx,%rdx,2), %rsi
	shlq	$3, %rsi
	addq	(%r14), %rsi
	cmpq	%rcx, %rax
	jae	LBB4_39
## BB#23:                               ##   in Loop: Header=BB4_22 Depth=1
	movq	16(%rsi), %rcx
	movq	%rcx, 16(%rax)
	vmovupd	(%rsi), %xmm0
	vmovupd	%xmm0, (%rax)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, (%rsi)
	movq	$0, 16(%rsi)
	addq	$24, -104(%rbp)
	jmp	LBB4_40
	.align	4, 0x90
LBB4_39:                                ##   in Loop: Header=BB4_22 Depth=1
Ltmp103:
	movq	%r15, %rdi
	callq	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_
Ltmp104:
LBB4_40:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE9push_backEOS6_.exit
                                        ##   in Loop: Header=BB4_22 Depth=1
	addq	$16, %rbx
	cmpq	%rbx, %r12
	jne	LBB4_41
LBB4_24:                                ## %._crit_edge
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	LBB4_29
## BB#25:
	movq	8(%r14), %rdi
	cmpq	%rbx, %rdi
	je	LBB4_28
	.align	4, 0x90
LBB4_26:                                ## %.lr.ph.i.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, 8(%r14)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	8(%r14), %rdi
	cmpq	%rbx, %rdi
	jne	LBB4_26
## BB#27:                               ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i
	movq	(%r14), %rbx
LBB4_28:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, (%r14)
	movq	$0, 16(%r14)
LBB4_29:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit14
	vmovaps	-112(%rbp), %xmm0
	vmovups	%xmm0, (%r14)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%r14)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB4_33
## BB#30:
	movq	-56(%rbp), %rax
	cmpq	%rdi, %rax
	je	LBB4_32
## BB#31:                               ## %.lr.ph.preheader.i.i.i.i.i.6
	leaq	-16(%rax), %rcx
	subq	%rdi, %rcx
	movq	$-16, %rdx
	andnq	%rdx, %rcx, %rcx
	addq	%rax, %rcx
	movq	%rcx, -56(%rbp)
LBB4_32:                                ## %_ZNSt3__113__vector_baseINS_5tupleIJdmEEENS_9allocatorIS2_EEE5clearEv.exit.i.i.i.7
	callq	__ZdlPv
LBB4_33:                                ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEED1Ev.exit8
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB4_34:
Ltmp105:
	movq	%rax, %r14
	movq	-112(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB4_43
## BB#35:
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB4_38
	.align	4, 0x90
LBB4_36:                                ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, -104(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-104(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB4_36
## BB#37:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i
	movq	-112(%rbp), %rbx
LBB4_38:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB4_43:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit
	movq	-136(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	LBB4_47
## BB#44:
	movq	-56(%rbp), %rax
	cmpq	%rdi, %rax
	je	LBB4_46
## BB#45:                               ## %.lr.ph.preheader.i.i.i.i.i
	leaq	-16(%rax), %rcx
	subq	%rdi, %rcx
	movq	$-16, %rdx
	andnq	%rdx, %rcx, %rcx
	addq	%rax, %rcx
	movq	%rcx, -56(%rbp)
LBB4_46:                                ## %_ZNSt3__113__vector_baseINS_5tupleIJdmEEENS_9allocatorIS2_EEE5clearEv.exit.i.i.i
	callq	__ZdlPv
LBB4_47:                                ## %_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB4_6:                                 ## %.loopexit21
Ltmp99:
LBB4_8:
	movq	%rax, %r14
	leaq	-64(%rbp), %rax
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	jmp	LBB4_43
LBB4_7:                                 ## %.loopexit.split-lp22
Ltmp94:
	jmp	LBB4_8
LBB4_42:                                ## %.thread
Ltmp102:
	movq	%rax, %r14
	jmp	LBB4_43
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset53 = Ltmp92-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp93-Ltmp92                  ##   Call between Ltmp92 and Ltmp93
	.long	Lset54
Lset55 = Ltmp94-Lfunc_begin3            ##     jumps to Ltmp94
	.long	Lset55
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp95-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset56
Lset57 = Ltmp98-Ltmp95                  ##   Call between Ltmp95 and Ltmp98
	.long	Lset57
Lset58 = Ltmp99-Lfunc_begin3            ##     jumps to Ltmp99
	.long	Lset58
	.byte	0                       ##   On action: cleanup
Lset59 = Ltmp100-Lfunc_begin3           ## >> Call Site 3 <<
	.long	Lset59
Lset60 = Ltmp101-Ltmp100                ##   Call between Ltmp100 and Ltmp101
	.long	Lset60
Lset61 = Ltmp102-Lfunc_begin3           ##     jumps to Ltmp102
	.long	Lset61
	.byte	0                       ##   On action: cleanup
Lset62 = Ltmp103-Lfunc_begin3           ## >> Call Site 4 <<
	.long	Lset62
Lset63 = Ltmp104-Ltmp103                ##   Call between Ltmp103 and Ltmp104
	.long	Lset63
Lset64 = Ltmp105-Lfunc_begin3           ##     jumps to Ltmp105
	.long	Lset64
	.byte	0                       ##   On action: cleanup
Lset65 = Ltmp104-Lfunc_begin3           ## >> Call Site 5 <<
	.long	Lset65
Lset66 = Lfunc_end3-Ltmp104             ##   Call between Ltmp104 and Lfunc_end3
	.long	Lset66
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE24__emplace_back_slow_pathIJdlEEEvDpOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE24__emplace_back_slow_pathIJdlEEEvDpOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE24__emplace_back_slow_pathIJdlEEEvDpOT_: ## @_ZNSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE24__emplace_back_slow_pathIJdlEEEvDpOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp114:
	.cfi_def_cfa_offset 16
Ltmp115:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp116:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
Ltmp117:
	.cfi_offset %rbx, -56
Ltmp118:
	.cfi_offset %r12, -48
Ltmp119:
	.cfi_offset %r13, -40
Ltmp120:
	.cfi_offset %r14, -32
Ltmp121:
	.cfi_offset %r15, -24
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	(%r14), %rdx
	movq	8(%r14), %rbx
	subq	%rdx, %rbx
	sarq	$4, %rbx
	addq	$1, %rbx
	movq	%rbx, %rax
	shrq	$60, %rax
	je	LBB5_2
## BB#1:
	movq	%r14, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
	movq	(%r14), %rdx
LBB5_2:
	movq	16(%r14), %r15
	subq	%rdx, %r15
	movq	%r15, %rax
	sarq	$4, %rax
	movabsq	$576460752303423487, %rcx ## imm = 0x7FFFFFFFFFFFFFF
	cmpq	%rcx, %rax
	jae	LBB5_3
## BB#4:                                ## %_ZNKSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE11__recommendEm.exit
	movq	%r13, -64(%rbp)         ## 8-byte Spill
	movq	%r12, -56(%rbp)         ## 8-byte Spill
	sarq	$3, %r15
	cmpq	%rbx, %r15
	cmovbq	%rbx, %r15
	movq	%r14, -48(%rbp)         ## 8-byte Spill
	movq	8(%r14), %r14
	movq	%r14, %r12
	subq	%rdx, %r12
	sarq	$4, %r12
	xorl	%r13d, %r13d
	movl	$0, %eax
	testq	%r15, %r15
	jne	LBB5_5
	jmp	LBB5_6
LBB5_3:                                 ## %_ZNKSt3__16vectorINS_5tupleIJdmEEENS_9allocatorIS2_EEE11__recommendEm.exit.thread
	movq	%r13, -64(%rbp)         ## 8-byte Spill
	movq	%r12, -56(%rbp)         ## 8-byte Spill
	movabsq	$1152921504606846975, %r15 ## imm = 0xFFFFFFFFFFFFFFF
	movq	%r14, -48(%rbp)         ## 8-byte Spill
	movq	8(%r14), %r14
	movq	%r14, %r12
	subq	%rdx, %r12
	sarq	$4, %r12
LBB5_5:
	movq	%r15, %rdi
	shlq	$4, %rdi
	movq	%rdx, %rbx
	callq	__Znwm
	movq	%rbx, %rdx
	movq	%r15, %r13
LBB5_6:
	shlq	$4, %r12
	leaq	(%rax,%r12), %r15
	shlq	$4, %r13
	addq	%rax, %r13
	movq	-64(%rbp), %rcx         ## 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, (%rax,%r12)
	movq	-56(%rbp), %rcx         ## 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, 8(%rax,%r12)
	leaq	16(%rax,%r12), %r12
	subq	%rdx, %r14
	subq	%r14, %r15
	testq	%r14, %r14
	jle	LBB5_8
## BB#7:
	movq	%r15, %rdi
	movq	%rdx, %rbx
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	_memcpy
	movq	%rbx, %rdx
LBB5_8:                                 ## %_ZNSt3__114__split_bufferINS_5tupleIJdmEEERNS_9allocatorIS2_EEE5clearEv.exit.i.i
	movq	-48(%rbp), %rax         ## 8-byte Reload
	movq	%r15, (%rax)
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	testq	%rdx, %rdx
	je	LBB5_9
## BB#10:
	movq	%rdx, %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB5_9:                                 ## %_ZNSt3__114__split_bufferINS_5tupleIJdmEEERNS_9allocatorIS2_EEED1Ev.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.align	4, 0x90
__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_: ## @_ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp122:
	.cfi_def_cfa_offset 16
Ltmp123:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp124:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp125:
	.cfi_offset %rbx, -56
Ltmp126:
	.cfi_offset %r12, -48
Ltmp127:
	.cfi_offset %r13, -40
Ltmp128:
	.cfi_offset %r14, -32
Ltmp129:
	.cfi_offset %r15, -24
	movq	%rdx, -56(%rbp)         ## 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	jmp	LBB6_1
LBB6_77:                                ##   in Loop: Header=BB6_1 Depth=1
	leaq	16(%r15), %rdi
	movq	%r12, %rsi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	callq	__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	movq	%r15, %r12
	.align	4, 0x90
LBB6_1:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit.thread20.outer
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB6_2 Depth 2
                                        ##       Child Loop BB6_46 Depth 3
                                        ##       Child Loop BB6_50 Depth 3
                                        ##       Child Loop BB6_53 Depth 3
                                        ##         Child Loop BB6_54 Depth 4
                                        ##         Child Loop BB6_55 Depth 4
                                        ##       Child Loop BB6_62 Depth 3
                                        ##         Child Loop BB6_63 Depth 4
                                        ##         Child Loop BB6_64 Depth 4
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	leaq	-16(%r12), %rax
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	leaq	-32(%r12), %rax
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	movq	%r13, %rbx
	jmp	LBB6_2
	.align	4, 0x90
LBB6_76:                                ##   in Loop: Header=BB6_2 Depth=2
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	callq	__ZNSt3__16__sortIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	addq	$16, %r15
	movq	%r15, %rbx
LBB6_2:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit.thread20
                                        ##   Parent Loop BB6_1 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB6_46 Depth 3
                                        ##       Child Loop BB6_50 Depth 3
                                        ##       Child Loop BB6_53 Depth 3
                                        ##         Child Loop BB6_54 Depth 4
                                        ##         Child Loop BB6_55 Depth 4
                                        ##       Child Loop BB6_62 Depth 3
                                        ##         Child Loop BB6_63 Depth 4
                                        ##         Child Loop BB6_64 Depth 4
	movq	%rbx, %r13
	movq	%r12, %rcx
	subq	%r13, %rcx
	movq	%rcx, %rax
	sarq	$4, %rax
	cmpq	$5, %rax
	jbe	LBB6_3
## BB#21:                               ##   in Loop: Header=BB6_2 Depth=2
	cmpq	$111, %rcx
	jle	LBB6_80
## BB#22:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%rax, %r14
	shrq	$63, %r14
	addq	%rax, %r14
	sarq	%r14
	shlq	$4, %r14
	leaq	(%r13,%r14), %rbx
	cmpq	$15985, %rcx            ## imm = 0x3E71
	jl	LBB6_32
## BB#23:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%rax, %r15
	sarq	$63, %r15
	shrq	$62, %r15
	addq	%rax, %r15
	sarq	$2, %r15
	shlq	$4, %r15
	leaq	(%r13,%r15), %rsi
	movq	%rsi, -72(%rbp)         ## 8-byte Spill
	leaq	(%rbx,%r15), %r12
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	movq	-56(%rbp), %r8          ## 8-byte Reload
	callq	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	movq	-48(%rbp), %r8          ## 8-byte Reload
	vmovsd	(%r8), %xmm0            ## xmm0 = mem[0],zero
	vmovsd	(%rbx,%r15), %xmm1      ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_24
## BB#25:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	-72(%rbp), %rdi         ## 8-byte Reload
	vmovsd	%xmm0, (%r12)
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	vmovsd	%xmm1, -16(%rsi)
	movq	8(%r12), %rcx
	movq	-8(%rsi), %rdx
	movq	%rdx, 8(%r12)
	movq	%rcx, -8(%rsi)
	movq	%rsi, %r9
	vmovsd	(%r12), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_26
## BB#27:                               ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	%xmm0, (%rbx)
	vmovsd	%xmm1, (%r12)
	movq	8(%r13,%r14), %rcx
	movq	8(%r12), %rdx
	movq	%rdx, 8(%r13,%r14)
	movq	%rcx, 8(%r12)
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	movq	%rdi, %rdx
	vmovsd	(%rdx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_28
## BB#29:                               ##   in Loop: Header=BB6_2 Depth=2
	leaq	8(%r13,%r14), %rcx
	vmovsd	%xmm0, (%rdx)
	vmovsd	%xmm1, (%rbx)
	movq	8(%r13,%r15), %rdi
	movq	(%rcx), %rsi
	movq	%rsi, 8(%r13,%r15)
	movq	%rdi, (%rcx)
	vmovsd	(%rdx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%r13), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_30
## BB#31:                               ##   in Loop: Header=BB6_2 Depth=2
	leaq	8(%r13,%r15), %rcx
	vmovsd	%xmm0, (%r13)
	vmovsd	%xmm1, (%rdx)
	movq	8(%r13), %rdx
	movq	(%rcx), %rsi
	movq	%rsi, 8(%r13)
	movq	%rdx, (%rcx)
	addl	$4, %eax
	movq	%r9, %r12
	jmp	LBB6_44
	.align	4, 0x90
LBB6_32:                                ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vmovsd	(%r13), %xmm0           ## xmm0 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	movq	-48(%rbp), %r8          ## 8-byte Reload
	vmovsd	(%r8), %xmm2            ## xmm2 = mem[0],zero
	jbe	LBB6_33
## BB#38:                               ##   in Loop: Header=BB6_2 Depth=2
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_40
## BB#39:                               ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	%xmm2, (%r13)
	vmovsd	%xmm0, -16(%r12)
	movq	8(%r13), %rax
	movq	-8(%r12), %rcx
	movq	%rcx, 8(%r13)
	movq	%rax, -8(%r12)
	movl	$1, %eax
	jmp	LBB6_44
	.align	4, 0x90
LBB6_24:                                ## %._ZNSt3__17__sort5IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SD_SB_.exit13_crit_edge
                                        ##   in Loop: Header=BB6_2 Depth=2
	movq	-80(%rbp), %r12         ## 8-byte Reload
	jmp	LBB6_44
	.align	4, 0x90
LBB6_33:                                ##   in Loop: Header=BB6_2 Depth=2
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_34
## BB#35:                               ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	%xmm2, (%rbx)
	vmovsd	%xmm1, -16(%r12)
	movq	8(%r13,%r14), %rax
	movq	-8(%r12), %rcx
	movq	%rcx, 8(%r13,%r14)
	movq	%rax, -8(%r12)
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%r13), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_36
## BB#37:                               ##   in Loop: Header=BB6_2 Depth=2
	leaq	8(%r13,%r14), %rax
	vmovsd	%xmm0, (%r13)
	vmovsd	%xmm1, (%rbx)
	movq	8(%r13), %rcx
	movq	(%rax), %rdx
	movq	%rdx, 8(%r13)
	movq	%rcx, (%rax)
	jmp	LBB6_43
LBB6_26:                                ##   in Loop: Header=BB6_2 Depth=2
	addl	$1, %eax
	movq	%r9, %r12
	jmp	LBB6_44
LBB6_40:                                ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	%xmm1, (%r13)
	vmovsd	%xmm0, (%rbx)
	movq	8(%r13), %rax
	movq	8(%r13,%r14), %rcx
	movq	%rcx, 8(%r13)
	movq	%rax, 8(%r13,%r14)
	vmovsd	(%r8), %xmm1            ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB6_41
## BB#42:                               ##   in Loop: Header=BB6_2 Depth=2
	leaq	8(%r13,%r14), %rcx
	vmovsd	%xmm1, (%rbx)
	vmovsd	%xmm0, -16(%r12)
	movq	-8(%r12), %rdx
	movq	%rdx, (%rcx)
	movq	%rax, -8(%r12)
LBB6_43:                                ## %_ZNSt3__17__sort5IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SD_SB_.exit13
                                        ##   in Loop: Header=BB6_2 Depth=2
	movl	$2, %eax
	jmp	LBB6_44
LBB6_34:                                ##   in Loop: Header=BB6_2 Depth=2
	xorl	%eax, %eax
	jmp	LBB6_44
LBB6_28:                                ##   in Loop: Header=BB6_2 Depth=2
	addl	$2, %eax
	movq	%r9, %r12
	jmp	LBB6_44
LBB6_36:                                ##   in Loop: Header=BB6_2 Depth=2
	movl	$1, %eax
	jmp	LBB6_44
LBB6_41:                                ##   in Loop: Header=BB6_2 Depth=2
	movl	$1, %eax
	jmp	LBB6_44
LBB6_30:                                ##   in Loop: Header=BB6_2 Depth=2
	addl	$3, %eax
	movq	%r9, %r12
	.align	4, 0x90
LBB6_44:                                ## %_ZNSt3__17__sort5IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SD_SB_.exit13
                                        ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	(%r13), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	movq	-64(%rbp), %rdx         ## 8-byte Reload
	jbe	LBB6_46
## BB#45:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, %rcx
	jmp	LBB6_61
	.align	4, 0x90
LBB6_46:                                ## %.preheader26
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	%rdx, %rcx
	cmpq	%rcx, %r13
	je	LBB6_47
## BB#59:                               ##   in Loop: Header=BB6_46 Depth=3
	vmovsd	(%rcx), %xmm2           ## xmm2 = mem[0],zero
	leaq	-16(%rcx), %rdx
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_46
## BB#60:                               ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	%xmm2, (%r13)
	vmovsd	%xmm0, 16(%rdx)
	movq	8(%r13), %rsi
	movq	24(%rdx), %rdi
	movq	%rdi, 8(%r13)
	movq	%rsi, 24(%rdx)
	addl	$1, %eax
LBB6_61:                                ##   in Loop: Header=BB6_2 Depth=2
	leaq	16(%r13), %rdx
	movq	%rdx, %r15
	cmpq	%rcx, %rdx
	jae	LBB6_67
	jmp	LBB6_62
	.align	4, 0x90
LBB6_66:                                ##   in Loop: Header=BB6_62 Depth=3
	vmovsd	%xmm2, (%r15)
	vmovsd	%xmm0, (%rcx)
	movq	8(%r15), %rsi
	movq	8(%rcx), %rdi
	movq	%rdi, 8(%r15)
	movq	%rsi, 8(%rcx)
	addl	$1, %eax
	cmpq	%r15, %rbx
	cmoveq	%rcx, %rbx
LBB6_62:                                ## %.outer
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ## =>    This Loop Header: Depth=3
                                        ##         Child Loop BB6_63 Depth 4
                                        ##         Child Loop BB6_64 Depth 4
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	.align	4, 0x90
LBB6_63:                                ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ##       Parent Loop BB6_62 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	movq	%rdx, %r15
	vmovsd	(%r15), %xmm0           ## xmm0 = mem[0],zero
	leaq	16(%r15), %rdx
	vucomisd	%xmm0, %xmm1
	ja	LBB6_63
	.align	4, 0x90
LBB6_64:                                ## %.preheader
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ##       Parent Loop BB6_62 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	vmovsd	-16(%rcx), %xmm2        ## xmm2 = mem[0],zero
	addq	$-16, %rcx
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_64
## BB#65:                               ##   in Loop: Header=BB6_62 Depth=3
	cmpq	%rcx, %r15
	jbe	LBB6_66
LBB6_67:                                ## %.loopexit
                                        ##   in Loop: Header=BB6_2 Depth=2
	cmpq	%rbx, %r15
	je	LBB6_68
## BB#69:                               ##   in Loop: Header=BB6_2 Depth=2
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%r15), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_70
## BB#71:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, -48(%rbp)          ## 8-byte Spill
	vmovsd	%xmm0, (%r15)
	vmovsd	%xmm1, (%rbx)
	movq	8(%r15), %rcx
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%r15)
	movq	%rcx, 8(%rbx)
	addl	$1, %eax
	jmp	LBB6_72
	.align	4, 0x90
LBB6_68:                                ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, -48(%rbp)          ## 8-byte Spill
	jmp	LBB6_72
	.align	4, 0x90
LBB6_47:                                ##   in Loop: Header=BB6_2 Depth=2
	leaq	16(%r13), %rax
	vmovsd	(%r8), %xmm1            ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	ja	LBB6_52
## BB#48:                               ## %.preheader25
                                        ##   in Loop: Header=BB6_2 Depth=2
	cmpq	%r8, %rax
	je	LBB6_79
## BB#49:                               ## %.lr.ph.preheader
                                        ##   in Loop: Header=BB6_2 Depth=2
	leaq	32(%r13), %rax
	.align	4, 0x90
LBB6_50:                                ## %.lr.ph
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	vmovsd	-16(%rax), %xmm2        ## xmm2 = mem[0],zero
	vucomisd	%xmm0, %xmm2
	ja	LBB6_51
## BB#81:                               ##   in Loop: Header=BB6_50 Depth=3
	addq	$16, %rax
	cmpq	%rax, %r12
	jne	LBB6_50
	jmp	LBB6_79
	.align	4, 0x90
LBB6_70:                                ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, -48(%rbp)          ## 8-byte Spill
LBB6_72:                                ##   in Loop: Header=BB6_2 Depth=2
	testl	%eax, %eax
	jne	LBB6_75
## BB#73:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rbx
	movq	-56(%rbp), %r12         ## 8-byte Reload
	movq	%r12, %rdx
	callq	__ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_
	movq	%rbx, %rsi
	movb	%al, -72(%rbp)          ## 1-byte Spill
	leaq	16(%r15), %rbx
	movq	%rbx, %rdi
	movq	%rsi, %r14
	movq	%r12, %rdx
	callq	__ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_
	testb	%al, %al
	jne	LBB6_78
## BB#74:                               ##   in Loop: Header=BB6_2 Depth=2
	cmpb	$0, -72(%rbp)           ## 1-byte Folded Reload
	movq	%r14, %r12
	jne	LBB6_2
LBB6_75:                                ##   in Loop: Header=BB6_2 Depth=2
	movq	%r15, %rax
	subq	%r13, %rax
	movq	%r12, %rcx
	subq	%r15, %rcx
	cmpq	%rcx, %rax
	jl	LBB6_76
	jmp	LBB6_77
LBB6_51:                                ##   in Loop: Header=BB6_2 Depth=2
	addq	$-16, %rax
	vmovsd	%xmm1, (%rax)
	vmovsd	%xmm2, -16(%r12)
	movq	8(%rax), %rcx
	movq	-8(%r12), %rdx
	movq	%rdx, 8(%rax)
	movq	%rcx, -8(%r12)
	addq	$16, %rax
LBB6_52:                                ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, %rcx
	cmpq	%r8, %rax
	jne	LBB6_53
	jmp	LBB6_79
	.align	4, 0x90
LBB6_58:                                ##   in Loop: Header=BB6_53 Depth=3
	vmovsd	%xmm2, (%rbx)
	vmovsd	%xmm0, (%rcx)
	movq	8(%rbx), %rdx
	movq	8(%rcx), %rsi
	movq	%rsi, 8(%rbx)
	movq	%rdx, 8(%rcx)
LBB6_53:                                ## %.outer24
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ## =>    This Loop Header: Depth=3
                                        ##         Child Loop BB6_54 Depth 4
                                        ##         Child Loop BB6_55 Depth 4
	vmovsd	(%r13), %xmm1           ## xmm1 = mem[0],zero
	.align	4, 0x90
LBB6_54:                                ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ##       Parent Loop BB6_53 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	movq	%rax, %rbx
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	leaq	16(%rbx), %rax
	vucomisd	%xmm1, %xmm0
	jbe	LBB6_54
	.align	4, 0x90
LBB6_55:                                ## %.preheader21
                                        ##   Parent Loop BB6_1 Depth=1
                                        ##     Parent Loop BB6_2 Depth=2
                                        ##       Parent Loop BB6_53 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	vmovsd	-16(%rcx), %xmm2        ## xmm2 = mem[0],zero
	addq	$-16, %rcx
	vucomisd	%xmm1, %xmm2
	ja	LBB6_55
## BB#56:                               ##   in Loop: Header=BB6_53 Depth=3
	cmpq	%rcx, %rbx
	jb	LBB6_58
## BB#57:                               ##   in Loop: Header=BB6_2 Depth=2
	movq	%r8, -48(%rbp)          ## 8-byte Spill
	jmp	LBB6_2
LBB6_78:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
                                        ##   in Loop: Header=BB6_1 Depth=1
	cmpb	$0, -72(%rbp)           ## 1-byte Folded Reload
	movq	%r15, %r12
	je	LBB6_1
	jmp	LBB6_79
LBB6_3:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit.thread20
	leaq	LJTI6_0(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	jmpq	*%rax
LBB6_4:
	vmovsd	(%rcx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%r13), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_79
## BB#5:
	vmovsd	%xmm0, (%r13)
	vmovsd	%xmm1, -16(%r12)
	jmp	LBB6_6
LBB6_80:
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	-56(%rbp), %rdx         ## 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__118__insertion_sort_3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_ ## TAILCALL
LBB6_7:
	vmovsd	(%r13), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	16(%r13), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	vmovsd	-16(%r12), %xmm2        ## xmm2 = mem[0],zero
	jbe	LBB6_8
## BB#11:
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_13
## BB#12:
	vmovsd	%xmm2, (%r13)
	vmovsd	%xmm0, -16(%r12)
LBB6_6:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit.thread
	movq	8(%r13), %rax
	movq	-8(%r12), %rcx
	movq	%rcx, 8(%r13)
	movq	%rax, -8(%r12)
	jmp	LBB6_79
LBB6_15:
	leaq	16(%r13), %rsi
	leaq	32(%r13), %rdx
	movq	%r13, %rdi
	movq	-56(%rbp), %r8          ## 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_ ## TAILCALL
LBB6_16:
	leaq	16(%r13), %rsi
	leaq	32(%r13), %rdx
	leaq	48(%r13), %rcx
	movq	%r13, %rdi
	movq	-56(%rbp), %r8          ## 8-byte Reload
	callq	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	vmovsd	-16(%r12), %xmm0        ## xmm0 = mem[0],zero
	vmovsd	48(%r13), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_79
## BB#17:
	vmovsd	%xmm0, 48(%r13)
	vmovsd	%xmm1, -16(%r12)
	movq	56(%r13), %rax
	movq	-8(%r12), %rcx
	movq	%rcx, 56(%r13)
	movq	%rax, -8(%r12)
	vmovsd	32(%r13), %xmm1         ## xmm1 = mem[0],zero
	vmovsd	48(%r13), %xmm0         ## xmm0 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_79
## BB#18:
	vmovsd	%xmm0, 32(%r13)
	vmovsd	%xmm1, 48(%r13)
	movq	40(%r13), %rcx
	movq	56(%r13), %rax
	movq	%rax, 40(%r13)
	movq	%rcx, 56(%r13)
	vmovsd	16(%r13), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_79
## BB#19:
	vmovsd	%xmm0, 16(%r13)
	vmovsd	%xmm1, 32(%r13)
	movq	24(%r13), %rcx
	movq	%rax, 24(%r13)
	movq	%rcx, 40(%r13)
	vmovsd	(%r13), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB6_79
## BB#20:
	vmovsd	%xmm0, (%r13)
	vmovsd	%xmm1, 16(%r13)
	movq	8(%r13), %rcx
	movq	%rax, 8(%r13)
	movq	%rcx, 24(%r13)
	jmp	LBB6_79
LBB6_8:
	vucomisd	%xmm2, %xmm1
	jbe	LBB6_79
## BB#9:
	vmovsd	%xmm2, 16(%r13)
	vmovsd	%xmm1, -16(%r12)
	movq	24(%r13), %rax
	movq	-8(%r12), %rcx
	movq	%rcx, 24(%r13)
	movq	%rax, -8(%r12)
	vmovsd	(%r13), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	16(%r13), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB6_79
## BB#10:
	vmovsd	%xmm1, (%r13)
	vmovsd	%xmm0, 16(%r13)
	movq	8(%r13), %rax
	movq	24(%r13), %rcx
	movq	%rcx, 8(%r13)
	movq	%rax, 24(%r13)
	jmp	LBB6_79
LBB6_13:
	vmovsd	%xmm1, (%r13)
	vmovsd	%xmm0, 16(%r13)
	movq	8(%r13), %rax
	movq	24(%r13), %rcx
	movq	%rcx, 8(%r13)
	movq	%rax, 24(%r13)
	vmovsd	-16(%r12), %xmm1        ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB6_79
## BB#14:
	vmovsd	%xmm1, 16(%r13)
	vmovsd	%xmm0, -16(%r12)
	movq	-8(%r12), %rcx
	movq	%rcx, 24(%r13)
	movq	%rax, -8(%r12)
LBB6_79:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit.thread
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L6_0_set_79 = LBB6_79-LJTI6_0
L6_0_set_4 = LBB6_4-LJTI6_0
L6_0_set_7 = LBB6_7-LJTI6_0
L6_0_set_15 = LBB6_15-LJTI6_0
L6_0_set_16 = LBB6_16-LJTI6_0
LJTI6_0:
	.long	L6_0_set_79
	.long	L6_0_set_79
	.long	L6_0_set_4
	.long	L6_0_set_7
	.long	L6_0_set_15
	.long	L6_0_set_16

	.globl	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	.align	4, 0x90
__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_: ## @_ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp130:
	.cfi_def_cfa_offset 16
Ltmp131:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp132:
	.cfi_def_cfa_register %rbp
	vmovsd	(%rsi), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%rdi), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	vmovsd	(%rdx), %xmm2           ## xmm2 = mem[0],zero
	jbe	LBB7_1
## BB#5:
	vucomisd	%xmm2, %xmm0
	jbe	LBB7_7
## BB#6:
	vmovsd	%xmm2, (%rdi)
	vmovsd	%xmm1, (%rdx)
	movq	8(%rdi), %r8
	movq	8(%rdx), %rax
	movq	%rax, 8(%rdi)
	movq	%r8, 8(%rdx)
	movl	$1, %eax
	jmp	LBB7_9
LBB7_1:
	xorl	%eax, %eax
	vucomisd	%xmm2, %xmm0
	jbe	LBB7_2
## BB#3:
	vmovsd	%xmm2, (%rsi)
	vmovsd	%xmm0, (%rdx)
	movq	8(%rsi), %r8
	movq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	movq	%r8, 8(%rdx)
	vmovsd	(%rsi), %xmm1           ## xmm1 = mem[0],zero
	vmovsd	(%rdi), %xmm2           ## xmm2 = mem[0],zero
	movl	$1, %eax
	vucomisd	%xmm1, %xmm2
	jbe	LBB7_10
## BB#4:
	vmovsd	%xmm1, (%rdi)
	vmovsd	%xmm2, (%rsi)
	movq	8(%rdi), %r8
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	%r8, 8(%rsi)
	vmovsd	(%rdx), %xmm0           ## xmm0 = mem[0],zero
	movl	$2, %eax
	jmp	LBB7_10
LBB7_7:
	vmovsd	%xmm0, (%rdi)
	vmovsd	%xmm1, (%rsi)
	movq	8(%rdi), %r8
	movq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	%r8, 8(%rsi)
	vmovsd	(%rdx), %xmm0           ## xmm0 = mem[0],zero
	movl	$1, %eax
	vucomisd	%xmm0, %xmm1
	jbe	LBB7_10
## BB#8:
	vmovsd	%xmm0, (%rsi)
	vmovsd	%xmm1, (%rdx)
	movq	8(%rdx), %rax
	movq	%rax, 8(%rsi)
	movq	%r8, 8(%rdx)
	movl	$2, %eax
LBB7_9:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	vmovapd	%xmm1, %xmm0
	jmp	LBB7_10
LBB7_2:
	vmovapd	%xmm2, %xmm0
LBB7_10:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	vmovsd	(%rcx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB7_16
## BB#11:
	vmovsd	%xmm1, (%rdx)
	vmovsd	%xmm0, (%rcx)
	movq	8(%rdx), %r8
	movq	8(%rcx), %r9
	movq	%r9, 8(%rdx)
	movq	%r8, 8(%rcx)
	vmovsd	(%rdx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%rsi), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB7_12
## BB#13:
	vmovsd	%xmm0, (%rsi)
	vmovsd	%xmm1, (%rdx)
	movq	8(%rsi), %r8
	movq	8(%rdx), %rcx
	movq	%rcx, 8(%rsi)
	movq	%r8, 8(%rdx)
	vmovsd	(%rsi), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	(%rdi), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB7_14
## BB#15:
	vmovsd	%xmm0, (%rdi)
	vmovsd	%xmm1, (%rsi)
	movq	8(%rdi), %rcx
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rdi)
	movq	%rcx, 8(%rsi)
	addl	$3, %eax
LBB7_16:
	popq	%rbp
	retq
LBB7_12:
	addl	$1, %eax
	popq	%rbp
	retq
LBB7_14:
	addl	$2, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__118__insertion_sort_3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__118__insertion_sort_3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.align	4, 0x90
__ZNSt3__118__insertion_sort_3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_: ## @_ZNSt3__118__insertion_sort_3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEvSD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp133:
	.cfi_def_cfa_offset 16
Ltmp134:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp135:
	.cfi_def_cfa_register %rbp
	vmovsd	(%rdi), %xmm1           ## xmm1 = mem[0],zero
	vmovsd	16(%rdi), %xmm0         ## xmm0 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	vmovsd	32(%rdi), %xmm2         ## xmm2 = mem[0],zero
	jbe	LBB8_1
## BB#5:
	vucomisd	%xmm2, %xmm0
	jbe	LBB8_7
## BB#6:
	vmovsd	%xmm2, (%rdi)
	vmovsd	%xmm1, 32(%rdi)
	movq	8(%rdi), %rax
	movq	40(%rdi), %rcx
	movq	%rcx, 8(%rdi)
	jmp	LBB8_10
LBB8_1:
	vucomisd	%xmm2, %xmm0
	jbe	LBB8_2
## BB#3:
	vmovsd	%xmm2, 16(%rdi)
	vmovsd	%xmm0, 32(%rdi)
	movq	24(%rdi), %rcx
	movq	40(%rdi), %rax
	movq	%rax, 24(%rdi)
	movq	%rcx, 40(%rdi)
	vucomisd	%xmm2, %xmm1
	jbe	LBB8_11
## BB#4:
	vmovsd	%xmm2, (%rdi)
	vmovsd	%xmm1, 16(%rdi)
	movq	8(%rdi), %rcx
	movq	%rax, 8(%rdi)
	movq	%rcx, 24(%rdi)
	jmp	LBB8_11
LBB8_7:
	vmovsd	%xmm0, (%rdi)
	vmovsd	%xmm1, 16(%rdi)
	movq	8(%rdi), %rax
	movq	24(%rdi), %rcx
	movq	%rcx, 8(%rdi)
	movq	%rax, 24(%rdi)
	vucomisd	%xmm2, %xmm1
	jbe	LBB8_8
## BB#9:
	vmovsd	%xmm2, 16(%rdi)
	vmovsd	%xmm1, 32(%rdi)
	movq	40(%rdi), %rcx
	movq	%rcx, 24(%rdi)
LBB8_10:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	movq	%rax, 40(%rdi)
	vmovapd	%xmm1, %xmm0
	jmp	LBB8_11
LBB8_2:
	vmovapd	%xmm2, %xmm0
	jmp	LBB8_11
LBB8_8:
	vmovapd	%xmm2, %xmm0
LBB8_11:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	leaq	48(%rdi), %rcx
	xorl	%r9d, %r9d
	cmpq	%rsi, %rcx
	jne	LBB8_12
	jmp	LBB8_19
	.align	4, 0x90
LBB8_18:                                ## %..lr.ph_crit_edge
                                        ##   in Loop: Header=BB8_12 Depth=1
	vmovsd	(%rcx), %xmm0           ## xmm0 = mem[0],zero
	addq	$16, %r9
	movq	%rdx, %rcx
LBB8_12:                                ## %.lr.ph
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB8_14 Depth 2
	vmovsd	(%rcx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB8_17
## BB#13:                               ##   in Loop: Header=BB8_12 Depth=1
	movq	8(%rcx), %r8
	movq	%r9, %rax
	.align	4, 0x90
LBB8_14:                                ##   Parent Loop BB8_12 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	%rax, %rdx
	vmovupd	32(%rdi,%rdx), %xmm0
	vmovupd	%xmm0, 48(%rdi,%rdx)
	cmpq	$-32, %rdx
	je	LBB8_16
## BB#15:                               ##   in Loop: Header=BB8_14 Depth=2
	vmovsd	16(%rdi,%rdx), %xmm0    ## xmm0 = mem[0],zero
	leaq	-16(%rdx), %rax
	vucomisd	%xmm1, %xmm0
	ja	LBB8_14
LBB8_16:                                ## %..critedge_crit_edge
                                        ##   in Loop: Header=BB8_12 Depth=1
	leaq	32(%rdi,%rdx), %rdx
	vmovsd	%xmm1, (%rdx)
	movq	%r8, 8(%rdx)
LBB8_17:                                ##   in Loop: Header=BB8_12 Depth=1
	leaq	16(%rcx), %rdx
	cmpq	%rsi, %rdx
	jne	LBB8_18
LBB8_19:                                ## %._crit_edge
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_
	.align	4, 0x90
__ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_: ## @_ZNSt3__127__insertion_sort_incompleteIRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEbSD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp136:
	.cfi_def_cfa_offset 16
Ltmp137:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp138:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp139:
	.cfi_offset %rbx, -40
Ltmp140:
	.cfi_offset %r14, -32
Ltmp141:
	.cfi_offset %r15, -24
	movq	%rdx, %rax
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rcx
	subq	%rbx, %rcx
	sarq	$4, %rcx
	cmpq	$5, %rcx
	ja	LBB9_20
## BB#1:
	movb	$1, %r15b
	leaq	LJTI9_0(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rcx
	addq	%rdx, %rcx
	jmpq	*%rcx
LBB9_2:
	vmovsd	-16(%r14), %xmm0        ## xmm0 = mem[0],zero
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB9_19
## BB#3:
	vmovsd	%xmm0, (%rbx)
	vmovsd	%xmm1, -16(%r14)
	jmp	LBB9_4
LBB9_5:
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	16(%rbx), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	vmovsd	-16(%r14), %xmm2        ## xmm2 = mem[0],zero
	jbe	LBB9_6
## BB#9:
	vucomisd	%xmm2, %xmm1
	jbe	LBB9_11
## BB#10:
	vmovsd	%xmm2, (%rbx)
	vmovsd	%xmm0, -16(%r14)
LBB9_4:                                 ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit6
	movq	8(%rbx), %rax
	movq	-8(%r14), %rcx
	movq	%rcx, 8(%rbx)
	movq	%rax, -8(%r14)
	jmp	LBB9_19
LBB9_13:
	leaq	16(%rbx), %rsi
	leaq	32(%rbx), %rdx
	addq	$-16, %r14
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movq	%rax, %r8
	callq	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	jmp	LBB9_19
LBB9_20:
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vmovsd	16(%rbx), %xmm0         ## xmm0 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	vmovsd	32(%rbx), %xmm2         ## xmm2 = mem[0],zero
	jbe	LBB9_21
## BB#25:
	vucomisd	%xmm2, %xmm0
	jbe	LBB9_27
## BB#26:
	vmovsd	%xmm2, (%rbx)
	vmovsd	%xmm1, 32(%rbx)
	movq	8(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rcx, 8(%rbx)
	jmp	LBB9_30
LBB9_14:
	leaq	16(%rbx), %rsi
	leaq	32(%rbx), %rdx
	leaq	48(%rbx), %rcx
	movq	%rbx, %rdi
	movq	%rax, %r8
	callq	__ZNSt3__17__sort4IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SD_SB_
	vmovsd	-16(%r14), %xmm0        ## xmm0 = mem[0],zero
	vmovsd	48(%rbx), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB9_19
## BB#15:
	vmovsd	%xmm0, 48(%rbx)
	vmovsd	%xmm1, -16(%r14)
	movq	56(%rbx), %rax
	movq	-8(%r14), %rcx
	movq	%rcx, 56(%rbx)
	movq	%rax, -8(%r14)
	vmovsd	32(%rbx), %xmm1         ## xmm1 = mem[0],zero
	vmovsd	48(%rbx), %xmm0         ## xmm0 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB9_19
## BB#16:
	vmovsd	%xmm0, 32(%rbx)
	vmovsd	%xmm1, 48(%rbx)
	movq	40(%rbx), %rcx
	movq	56(%rbx), %rax
	movq	%rax, 40(%rbx)
	movq	%rcx, 56(%rbx)
	vmovsd	16(%rbx), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB9_19
## BB#17:
	vmovsd	%xmm0, 16(%rbx)
	vmovsd	%xmm1, 32(%rbx)
	movq	24(%rbx), %rcx
	movq	%rax, 24(%rbx)
	movq	%rcx, 40(%rbx)
	vmovsd	(%rbx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm0, %xmm1
	jbe	LBB9_19
## BB#18:
	vmovsd	%xmm0, (%rbx)
	vmovsd	%xmm1, 16(%rbx)
	movq	8(%rbx), %rcx
	movq	%rax, 8(%rbx)
	movq	%rcx, 24(%rbx)
	jmp	LBB9_19
LBB9_6:
	vucomisd	%xmm2, %xmm1
	jbe	LBB9_19
## BB#7:
	vmovsd	%xmm2, 16(%rbx)
	vmovsd	%xmm1, -16(%r14)
	movq	24(%rbx), %rax
	movq	-8(%r14), %rcx
	movq	%rcx, 24(%rbx)
	movq	%rax, -8(%r14)
	vmovsd	(%rbx), %xmm0           ## xmm0 = mem[0],zero
	vmovsd	16(%rbx), %xmm1         ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB9_19
## BB#8:
	vmovsd	%xmm1, (%rbx)
	vmovsd	%xmm0, 16(%rbx)
	movq	8(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	%rcx, 8(%rbx)
	movq	%rax, 24(%rbx)
	jmp	LBB9_19
LBB9_21:
	vucomisd	%xmm2, %xmm0
	jbe	LBB9_22
## BB#23:
	vmovsd	%xmm2, 16(%rbx)
	vmovsd	%xmm0, 32(%rbx)
	movq	24(%rbx), %rcx
	movq	40(%rbx), %rax
	movq	%rax, 24(%rbx)
	movq	%rcx, 40(%rbx)
	vucomisd	%xmm2, %xmm1
	jbe	LBB9_31
## BB#24:
	vmovsd	%xmm2, (%rbx)
	vmovsd	%xmm1, 16(%rbx)
	movq	8(%rbx), %rcx
	movq	%rax, 8(%rbx)
	movq	%rcx, 24(%rbx)
	jmp	LBB9_31
LBB9_11:
	vmovsd	%xmm1, (%rbx)
	vmovsd	%xmm0, 16(%rbx)
	movq	8(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	%rcx, 8(%rbx)
	movq	%rax, 24(%rbx)
	vmovsd	-16(%r14), %xmm1        ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB9_19
## BB#12:
	vmovsd	%xmm1, 16(%rbx)
	vmovsd	%xmm0, -16(%r14)
	movq	-8(%r14), %rcx
	movq	%rcx, 24(%rbx)
	movq	%rax, -8(%r14)
	jmp	LBB9_19
LBB9_27:
	vmovsd	%xmm0, (%rbx)
	vmovsd	%xmm1, 16(%rbx)
	movq	8(%rbx), %rax
	movq	24(%rbx), %rcx
	movq	%rcx, 8(%rbx)
	movq	%rax, 24(%rbx)
	vucomisd	%xmm2, %xmm1
	jbe	LBB9_28
## BB#29:
	vmovsd	%xmm2, 16(%rbx)
	vmovsd	%xmm1, 32(%rbx)
	movq	40(%rbx), %rcx
	movq	%rcx, 24(%rbx)
LBB9_30:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	movq	%rax, 40(%rbx)
	vmovapd	%xmm1, %xmm0
	jmp	LBB9_31
LBB9_22:
	vmovapd	%xmm2, %xmm0
	jmp	LBB9_31
LBB9_28:
	vmovapd	%xmm2, %xmm0
LBB9_31:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit
	leaq	48(%rbx), %rdx
	xorl	%ecx, %ecx
	movb	$1, %r8b
	cmpq	%r14, %rdx
	je	LBB9_39
## BB#32:
	xorl	%esi, %esi
	jmp	LBB9_33
	.align	4, 0x90
LBB9_41:                                ## %..lr.ph_crit_edge
                                        ##   in Loop: Header=BB9_33 Depth=1
	vmovsd	(%rdx), %xmm0           ## xmm0 = mem[0],zero
	addq	$16, %rcx
	movq	%rax, %rdx
LBB9_33:                                ## %.lr.ph
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB9_35 Depth 2
	vmovsd	(%rdx), %xmm1           ## xmm1 = mem[0],zero
	vucomisd	%xmm1, %xmm0
	jbe	LBB9_40
## BB#34:                               ##   in Loop: Header=BB9_33 Depth=1
	movq	8(%rdx), %r9
	movq	%rcx, %rdi
	.align	4, 0x90
LBB9_35:                                ##   Parent Loop BB9_33 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rax
	vmovupd	32(%rbx,%rax), %xmm0
	vmovupd	%xmm0, 48(%rbx,%rax)
	cmpq	$-32, %rax
	je	LBB9_37
## BB#36:                               ##   in Loop: Header=BB9_35 Depth=2
	vmovsd	16(%rbx,%rax), %xmm0    ## xmm0 = mem[0],zero
	leaq	-16(%rax), %rdi
	vucomisd	%xmm1, %xmm0
	ja	LBB9_35
LBB9_37:                                ## %..critedge_crit_edge
                                        ##   in Loop: Header=BB9_33 Depth=1
	leaq	32(%rbx,%rax), %rax
	vmovsd	%xmm1, (%rax)
	movq	%r9, 8(%rax)
	addl	$1, %esi
	leaq	16(%rdx), %rax
	cmpq	%r14, %rax
	sete	%r15b
	cmpl	$8, %esi
	je	LBB9_38
LBB9_40:                                ##   in Loop: Header=BB9_33 Depth=1
	leaq	16(%rdx), %rax
	cmpq	%r14, %rax
	jne	LBB9_41
	jmp	LBB9_39
LBB9_38:
	xorl	%r8d, %r8d
LBB9_39:                                ## %.critedge._crit_edge
	orb	%r8b, %r15b
LBB9_19:                                ## %_ZNSt3__17__sort3IRZ30sort_numeric_single_conversionINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PNS_5tupleIJdmEEEEEjSD_SD_SD_SB_.exit6
	andb	$1, %r15b
	movb	%r15b, %al
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L9_0_set_19 = LBB9_19-LJTI9_0
L9_0_set_2 = LBB9_2-LJTI9_0
L9_0_set_5 = LBB9_5-LJTI9_0
L9_0_set_13 = LBB9_13-LJTI9_0
L9_0_set_14 = LBB9_14-LJTI9_0
LJTI9_0:
	.long	L9_0_set_19
	.long	L9_0_set_19
	.long	L9_0_set_2
	.long	L9_0_set_5
	.long	L9_0_set_13
	.long	L9_0_set_14

	.globl	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_: ## @_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__push_back_slow_pathIS6_EEvOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp142:
	.cfi_def_cfa_offset 16
Ltmp143:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp144:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp145:
	.cfi_offset %rbx, -56
Ltmp146:
	.cfi_offset %r12, -48
Ltmp147:
	.cfi_offset %r13, -40
Ltmp148:
	.cfi_offset %r14, -32
Ltmp149:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r15
	movabsq	$768614336404564650, %r13 ## imm = 0xAAAAAAAAAAAAAAA
	movq	(%r15), %rax
	movq	8(%r15), %rbx
	subq	%rax, %rbx
	sarq	$3, %rbx
	movabsq	$-6148914691236517205, %r12 ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%r12, %rbx
	addq	$1, %rbx
	cmpq	%r13, %rbx
	jbe	LBB10_2
## BB#1:
	movq	%r15, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
	movq	(%r15), %rax
LBB10_2:
	movq	16(%r15), %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	imulq	%r12, %rcx
	movabsq	$384307168202282325, %rdx ## imm = 0x555555555555555
	cmpq	%rdx, %rcx
	jae	LBB10_3
## BB#4:                                ## %_ZNKSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE11__recommendEm.exit
	addq	%rcx, %rcx
	cmpq	%rbx, %rcx
	cmovbq	%rbx, %rcx
	movq	8(%r15), %rbx
	subq	%rax, %rbx
	sarq	$3, %rbx
	imulq	%r12, %rbx
	xorl	%edx, %edx
	movq	%rcx, %r13
	movl	$0, %eax
	testq	%rcx, %rcx
	jne	LBB10_5
	jmp	LBB10_6
LBB10_3:                                ## %_ZNKSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE11__recommendEm.exit.thread
	movq	8(%r15), %rbx
	subq	%rax, %rbx
	sarq	$3, %rbx
	imulq	%r12, %rbx
LBB10_5:
	leaq	(,%r13,8), %rax
	leaq	(%rax,%rax,2), %rdi
	callq	__Znwm
	movq	%r13, %rdx
LBB10_6:
	leaq	(%rbx,%rbx,2), %rsi
	leaq	(%rax,%rsi,8), %rcx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,8), %rdx
	movq	16(%r14), %rdi
	movq	%rdi, 16(%rax,%rsi,8)
	vmovups	(%r14), %xmm0
	vmovups	%xmm0, (%rax,%rsi,8)
	vxorps	%xmm0, %xmm0, %xmm0
	vmovups	%xmm0, (%r14)
	movq	$0, 16(%r14)
	leaq	24(%rax,%rsi,8), %rax
	movq	(%r15), %rbx
	movq	8(%r15), %rsi
	cmpq	%rbx, %rsi
	je	LBB10_7
	.align	4, 0x90
LBB10_8:                                ## %.lr.ph.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movq	-8(%rsi), %rdi
	movq	%rdi, -8(%rcx)
	vmovups	-24(%rsi), %xmm1
	vmovups	%xmm1, -24(%rcx)
	vmovups	%xmm0, -24(%rsi)
	movq	$0, -8(%rsi)
	leaq	-24(%rsi), %rsi
	addq	$-24, %rcx
	cmpq	%rsi, %rbx
	jne	LBB10_8
## BB#9:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE20__construct_backwardIPS6_EEvRS7_T_SC_RSC_.exit.loopexit.i
	movq	(%r15), %r14
	movq	8(%r15), %rbx
	jmp	LBB10_10
LBB10_7:                                ## %._ZNSt3__116allocator_traitsINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEEE20__construct_backwardIPS6_EEvRS7_T_SC_RSC_.exit_crit_edge.i
	movq	%rbx, %r14
LBB10_10:
	movq	%rcx, (%r15)
	movq	%rax, 8(%r15)
	movq	%rdx, 16(%r15)
	cmpq	%r14, %rbx
	je	LBB10_12
	.align	4, 0x90
LBB10_11:                               ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rbx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	cmpq	%rbx, %r14
	jne	LBB10_11
LBB10_12:                               ## %_ZNSt3__114__split_bufferINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS4_IS6_EEE5clearEv.exit.i.i
	testq	%r14, %r14
	je	LBB10_13
## BB#14:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB10_13:                               ## %_ZNSt3__114__split_bufferINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS4_IS6_EEED1Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	.align	4, 0x90
__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_: ## @_ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp150:
	.cfi_def_cfa_offset 16
Ltmp151:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp152:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
Ltmp153:
	.cfi_offset %rbx, -56
Ltmp154:
	.cfi_offset %r12, -48
Ltmp155:
	.cfi_offset %r13, -40
Ltmp156:
	.cfi_offset %r14, -32
Ltmp157:
	.cfi_offset %r15, -24
	movq	%rdx, %r15
	movq	%r15, -112(%rbp)        ## 8-byte Spill
	movq	%rdi, %r13
	jmp	LBB11_1
LBB11_41:                               ##   in Loop: Header=BB11_1 Depth=1
	leaq	24(%r14), %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	movq	%r15, %rdx
	callq	__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	movq	%r14, %rsi
	.align	4, 0x90
LBB11_1:                                ## %.thread.outer
                                        ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB11_2 Depth 2
                                        ##       Child Loop BB11_15 Depth 3
                                        ##       Child Loop BB11_19 Depth 3
                                        ##       Child Loop BB11_22 Depth 3
                                        ##         Child Loop BB11_23 Depth 4
                                        ##       Child Loop BB11_29 Depth 3
                                        ##         Child Loop BB11_30 Depth 4
	movq	%rsi, -88(%rbp)         ## 8-byte Spill
	leaq	-24(%rsi), %rax
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	leaq	-48(%rsi), %rax
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	movq	%r13, %r12
	jmp	LBB11_2
	.align	4, 0x90
LBB11_40:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	__ZNSt3__16__sortIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	addq	$24, %r14
	movq	%r14, %r12
LBB11_2:                                ## %.thread
                                        ##   Parent Loop BB11_1 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB11_15 Depth 3
                                        ##       Child Loop BB11_19 Depth 3
                                        ##       Child Loop BB11_22 Depth 3
                                        ##         Child Loop BB11_23 Depth 4
                                        ##       Child Loop BB11_29 Depth 3
                                        ##         Child Loop BB11_30 Depth 4
	movq	%r12, %r13
	movq	-88(%rbp), %rcx         ## 8-byte Reload
	subq	%r13, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rdx ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rdx, %rax
	cmpq	$5, %rax
	jbe	LBB11_3
## BB#9:                                ##   in Loop: Header=BB11_2 Depth=2
	cmpq	$167, %rcx
	jle	LBB11_44
## BB#10:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%rcx, %rax
	movabsq	$3074457345618258603, %rsi ## imm = 0x2AAAAAAAAAAAAAAB
	imulq	%rsi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$3, %rdx
	addq	%rax, %rdx
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%r13,%rax,8), %rbx
	cmpq	$23977, %rcx            ## imm = 0x5DA9
	jl	LBB11_12
## BB#11:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%rcx, %rax
	imulq	%rsi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$4, %rdx
	addq	%rax, %rdx
	shlq	$3, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	leaq	(%r13,%rcx), %rsi
	addq	%rbx, %rcx
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	-96(%rbp), %r8          ## 8-byte Reload
	movq	%r15, %r9
	callq	__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_
	jmp	LBB11_13
	.align	4, 0x90
LBB11_12:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	movq	%r15, %rcx
	callq	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
LBB11_13:                               ##   in Loop: Header=BB11_2 Depth=2
	movl	%eax, %r14d
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	movq	-104(%rbp), %rax        ## 8-byte Reload
	jbe	LBB11_15
## BB#14:                               ##   in Loop: Header=BB11_2 Depth=2
	movl	%r14d, -76(%rbp)        ## 4-byte Spill
	movq	-96(%rbp), %r12         ## 8-byte Reload
	jmp	LBB11_28
	.align	4, 0x90
LBB11_15:                               ## %.preheader28
                                        ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	%rax, %r12
	cmpq	%r12, %r13
	je	LBB11_16
## BB#26:                               ##   in Loop: Header=BB11_15 Depth=3
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	leaq	-24(%r12), %rax
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB11_15
## BB#27:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	16(%r13), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r13), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r12), %rax
	movq	%rax, 16(%r13)
	vmovups	(%r12), %xmm0
	vmovups	%xmm0, (%r13)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r12)
	addl	$1, %r14d
	movl	%r14d, -76(%rbp)        ## 4-byte Spill
LBB11_28:                               ##   in Loop: Header=BB11_2 Depth=2
	leaq	24(%r13), %r15
	movq	%r15, %r14
	cmpq	%r12, %r15
	jae	LBB11_33
	jmp	LBB11_29
LBB11_32:                               ##   in Loop: Header=BB11_29 Depth=3
	movq	16(%r14), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r14), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r12), %rax
	movq	%rax, 16(%r14)
	vmovups	(%r12), %xmm0
	vmovups	%xmm0, (%r14)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r12)
	addl	$1, -76(%rbp)           ## 4-byte Folded Spill
	cmpq	%r14, %rbx
	cmoveq	%r12, %rbx
	.align	4, 0x90
LBB11_29:                               ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ## =>    This Loop Header: Depth=3
                                        ##         Child Loop BB11_30 Depth 4
	movq	%r15, %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	leaq	24(%r14), %r15
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	ja	LBB11_29
	.align	4, 0x90
LBB11_30:                               ## %.preheader
                                        ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ##       Parent Loop BB11_29 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	addq	$-24, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB11_30
## BB#31:                               ##   in Loop: Header=BB11_29 Depth=3
	cmpq	%r12, %r14
	jbe	LBB11_32
LBB11_33:                               ## %.loopexit
                                        ##   in Loop: Header=BB11_2 Depth=2
	cmpq	%rbx, %r14
	movq	-112(%rbp), %r15        ## 8-byte Reload
	je	LBB11_36
## BB#34:                               ##   in Loop: Header=BB11_2 Depth=2
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB11_36
## BB#35:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	16(%r14), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r14), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r14)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	addl	$1, -76(%rbp)           ## 4-byte Folded Spill
LBB11_36:                               ##   in Loop: Header=BB11_2 Depth=2
	cmpl	$0, -76(%rbp)           ## 4-byte Folded Reload
	jne	LBB11_39
## BB#37:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	__ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_
	movb	%al, %bl
	leaq	24(%r14), %r12
	movq	%r12, %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	movq	%r15, %rdx
	callq	__ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_
	testb	%al, %al
	jne	LBB11_42
## BB#38:                               ##   in Loop: Header=BB11_2 Depth=2
	testb	%bl, %bl
	jne	LBB11_2
LBB11_39:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%r14, %rax
	subq	%r13, %rax
	movq	-88(%rbp), %rcx         ## 8-byte Reload
	subq	%r14, %rcx
	cmpq	%rcx, %rax
	jl	LBB11_40
	jmp	LBB11_41
	.align	4, 0x90
LBB11_16:                               ##   in Loop: Header=BB11_2 Depth=2
	leaq	24(%r13), %r14
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	-96(%rbp), %r12         ## 8-byte Reload
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	ja	LBB11_21
## BB#17:                               ## %.preheader27
                                        ##   in Loop: Header=BB11_2 Depth=2
	cmpq	%r12, %r14
	je	LBB11_43
## BB#18:                               ## %.lr.ph.preheader
                                        ##   in Loop: Header=BB11_2 Depth=2
	leaq	48(%r13), %rbx
	.align	4, 0x90
LBB11_19:                               ## %.lr.ph
                                        ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	leaq	-24(%rbx), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	ja	LBB11_20
## BB#45:                               ##   in Loop: Header=BB11_19 Depth=3
	addq	$24, %rbx
	cmpq	%rbx, -88(%rbp)         ## 8-byte Folded Reload
	jne	LBB11_19
	jmp	LBB11_43
LBB11_20:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	16(%r14), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r14), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r12), %rax
	movq	%rax, 16(%r14)
	vmovups	(%r12), %xmm0
	vmovups	%xmm0, (%r14)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r12)
	addq	$24, %r14
LBB11_21:                               ##   in Loop: Header=BB11_2 Depth=2
	movq	%r12, -96(%rbp)         ## 8-byte Spill
	movq	%r12, %rbx
	cmpq	%r12, %r14
	jne	LBB11_22
	jmp	LBB11_43
LBB11_25:                               ##   in Loop: Header=BB11_22 Depth=3
	movq	16(%r12), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r12), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	.align	4, 0x90
LBB11_22:                               ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ## =>    This Loop Header: Depth=3
                                        ##         Child Loop BB11_23 Depth 4
	movq	%r14, %r12
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	leaq	24(%r12), %r14
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB11_22
	.align	4, 0x90
LBB11_23:                               ## %.preheader23
                                        ##   Parent Loop BB11_1 Depth=1
                                        ##     Parent Loop BB11_2 Depth=2
                                        ##       Parent Loop BB11_22 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	addq	$-24, %rbx
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	ja	LBB11_23
## BB#24:                               ##   in Loop: Header=BB11_22 Depth=3
	cmpq	%rbx, %r12
	jb	LBB11_25
	jmp	LBB11_2
LBB11_42:                               ##   in Loop: Header=BB11_1 Depth=1
	movq	%r14, %rsi
	testb	%bl, %bl
	je	LBB11_1
	jmp	LBB11_43
LBB11_3:                                ## %.thread
	leaq	LJTI11_0(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmpq	*%rax
LBB11_4:
	xorl	%esi, %esi
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB11_43
## BB#5:
	movq	16(%r13), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r13), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	16(%rcx), %rax
	movq	%rax, 16(%r13)
	vmovups	(%rcx), %xmm0
	vmovups	%xmm0, (%r13)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rcx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rcx)
LBB11_43:                               ## %.thread19
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB11_44:
	movq	%r13, %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	movq	%r15, %rdx
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__118__insertion_sort_3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_ ## TAILCALL
LBB11_7:
	leaq	24(%r13), %rsi
	leaq	48(%r13), %rdx
	movq	%r13, %rdi
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%r15, %r8
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_ ## TAILCALL
LBB11_6:
	leaq	24(%r13), %rsi
	movq	%r13, %rdi
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	movq	%r15, %rcx
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_ ## TAILCALL
LBB11_8:
	leaq	48(%r13), %rdx
	leaq	72(%r13), %rcx
	leaq	24(%r13), %rsi
	movq	%r13, %rdi
	movq	-96(%rbp), %r8          ## 8-byte Reload
	movq	%r15, %r9
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_ ## TAILCALL
	.cfi_endproc
	.align	2, 0x90
L11_0_set_43 = LBB11_43-LJTI11_0
L11_0_set_4 = LBB11_4-LJTI11_0
L11_0_set_6 = LBB11_6-LJTI11_0
L11_0_set_7 = LBB11_7-LJTI11_0
L11_0_set_8 = LBB11_8-LJTI11_0
LJTI11_0:
	.long	L11_0_set_43
	.long	L11_0_set_43
	.long	L11_0_set_4
	.long	L11_0_set_6
	.long	L11_0_set_7
	.long	L11_0_set_8

	.globl	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	.align	4, 0x90
__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_: ## @_ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp158:
	.cfi_def_cfa_offset 16
Ltmp159:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp160:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
Ltmp161:
	.cfi_offset %rbx, -48
Ltmp162:
	.cfi_offset %r12, -40
Ltmp163:
	.cfi_offset %r14, -32
Ltmp164:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -80(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -88(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	-80(%rbp), %xmm1        ## 8-byte Reload
                                        ## xmm1 = mem[0],zero
	vucomisd	-72(%rbp), %xmm1 ## 8-byte Folded Reload
	jbe	LBB12_1
## BB#4:
	vucomisd	-88(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB12_6
## BB#5:
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r15), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r14), %rax
	movq	%rax, 16(%r15)
	vmovups	(%r14), %xmm0
	vmovups	%xmm0, (%r15)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r14)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r14)
	movl	$1, %r12d
	jmp	LBB12_9
LBB12_1:
	vucomisd	-88(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB12_9
## BB#2:
	movq	16(%rbx), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%rbx), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	vmovups	(%r14), %xmm0
	vmovups	%xmm0, (%rbx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r14)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r14)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	movl	$1, %r12d
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB12_9
## BB#3:
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r15), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r15)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r15)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	jmp	LBB12_8
LBB12_6:
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r15), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r15)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r15)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	movl	$1, %r12d
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB12_9
## BB#7:
	movq	16(%rbx), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%rbx), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	vmovups	(%r14), %xmm0
	vmovups	%xmm0, (%rbx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r14)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r14)
LBB12_8:
	movl	$2, %r12d
LBB12_9:
	movl	%r12d, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_
	.align	4, 0x90
__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_: ## @_ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp165:
	.cfi_def_cfa_offset 16
Ltmp166:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp167:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp168:
	.cfi_offset %rbx, -56
Ltmp169:
	.cfi_offset %r12, -48
Ltmp170:
	.cfi_offset %r13, -40
Ltmp171:
	.cfi_offset %r14, -32
Ltmp172:
	.cfi_offset %r15, -24
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	%r8, %rcx
	callq	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	movl	%eax, %r14d
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB13_6
## BB#1:
	movq	16(%rbx), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%rbx), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbx)
	vmovups	(%r13), %xmm0
	vmovups	%xmm0, (%rbx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r13)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB13_2
## BB#3:
	movq	16(%r12), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r12), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r12)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB13_4
## BB#5:
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r15), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r12), %rax
	movq	%rax, 16(%r15)
	vmovups	(%r12), %xmm0
	vmovups	%xmm0, (%r15)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r12)
	addl	$3, %r14d
	jmp	LBB13_6
LBB13_2:
	addl	$1, %r14d
	jmp	LBB13_6
LBB13_4:
	addl	$2, %r14d
LBB13_6:
	movl	%r14d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_
	.align	4, 0x90
__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_: ## @_ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp173:
	.cfi_def_cfa_offset 16
Ltmp174:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp175:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp176:
	.cfi_offset %rbx, -56
Ltmp177:
	.cfi_offset %r12, -48
Ltmp178:
	.cfi_offset %r13, -40
Ltmp179:
	.cfi_offset %r14, -32
Ltmp180:
	.cfi_offset %r15, -24
	movq	%r8, %r15
	movq	%rcx, %rbx
	movq	%rdx, -80(%rbp)         ## 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%r9, %r8
	callq	__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_
	movl	%eax, %r12d
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB14_8
## BB#1:
	movq	16(%rbx), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%rbx), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r15), %rax
	movq	%rax, 16(%rbx)
	vmovups	(%r15), %xmm0
	vmovups	%xmm0, (%rbx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r15)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r15)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	-80(%rbp), %r15         ## 8-byte Reload
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB14_2
## BB#3:
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r15), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r15)
	vmovups	(%rbx), %xmm0
	vmovups	%xmm0, (%r15)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB14_4
## BB#5:
	movq	16(%r13), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r13), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r15), %rax
	movq	%rax, 16(%r13)
	vmovups	(%r15), %xmm0
	vmovups	%xmm0, (%r13)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r15)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r15)
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB14_6
## BB#7:
	movq	16(%r14), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%r14), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r13), %rax
	movq	%rax, 16(%r14)
	vmovups	(%r13), %xmm0
	vmovups	%xmm0, (%r14)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r13)
	addl	$4, %r12d
	jmp	LBB14_8
LBB14_2:
	addl	$1, %r12d
	jmp	LBB14_8
LBB14_4:
	addl	$2, %r12d
	jmp	LBB14_8
LBB14_6:
	addl	$3, %r12d
LBB14_8:
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI15_0:
	.space	16
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__118__insertion_sort_3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__118__insertion_sort_3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
	.align	4, 0x90
__ZNSt3__118__insertion_sort_3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_: ## @_ZNSt3__118__insertion_sort_3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEvSD_SD_SB_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp192:
	.cfi_def_cfa_offset 16
Ltmp193:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp194:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp195:
	.cfi_offset %rbx, -56
Ltmp196:
	.cfi_offset %r12, -48
Ltmp197:
	.cfi_offset %r13, -40
Ltmp198:
	.cfi_offset %r14, -32
Ltmp199:
	.cfi_offset %r15, -24
	movq	%rdx, %rax
	movq	%rsi, %rbx
	movq	%rbx, -80(%rbp)         ## 8-byte Spill
	movq	%rdi, %r14
	leaq	48(%r14), %r13
	leaq	24(%r14), %rsi
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	leaq	72(%r14), %r15
	cmpq	%rbx, %r15
	je	LBB15_19
## BB#1:                                ## %.lr.ph
	xorl	%r12d, %r12d
	.align	4, 0x90
LBB15_2:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB15_4 Depth 2
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB15_18
## BB#3:                                ##   in Loop: Header=BB15_2 Depth=1
	movq	16(%r15), %rax
	movq	%rax, -48(%rbp)
	vmovupd	(%r15), %xmm0
	vmovapd	%xmm0, -64(%rbp)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, (%r15)
	movq	$0, 16(%r15)
	xorl	%eax, %eax
	movq	%r12, %rcx
	.align	4, 0x90
LBB15_4:                                ## %._crit_edge.12
                                        ##   Parent Loop BB15_2 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	leaq	72(%r14,%rbx), %r13
	testb	$1, %al
	jne	LBB15_5
## BB#6:                                ##   in Loop: Header=BB15_4 Depth=2
	movw	$0, 72(%r14,%rbx)
	jmp	LBB15_7
	.align	4, 0x90
LBB15_5:                                ##   in Loop: Header=BB15_4 Depth=2
	movq	88(%r14,%rbx), %rax
	movb	$0, (%rax)
	movq	$0, 80(%r14,%rbx)
LBB15_7:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i.1
                                        ##   in Loop: Header=BB15_4 Depth=2
Ltmp181:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp182:
## BB#8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit2
                                        ##   in Loop: Header=BB15_4 Depth=2
	movq	64(%r14,%rbx), %rax
	movq	%rax, 16(%r13)
	vmovupd	48(%r14,%rbx), %xmm0
	vmovupd	%xmm0, (%r13)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, 48(%r14,%rbx)
	movq	$0, 64(%r14,%rbx)
	cmpq	$-48, %rbx
	je	LBB15_9
## BB#10:                               ##   in Loop: Header=BB15_4 Depth=2
Ltmp184:
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
Ltmp185:
## BB#11:                               ## %.noexc
                                        ##   in Loop: Header=BB15_4 Depth=2
	leaq	24(%r14,%rbx), %rdi
Ltmp186:
	xorl	%esi, %esi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
Ltmp187:
## BB#12:                               ##   in Loop: Header=BB15_4 Depth=2
	leaq	-24(%rbx), %rcx
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	movb	48(%r14,%rbx), %al
	ja	LBB15_4
## BB#13:                               ## %.critedge
                                        ##   in Loop: Header=BB15_2 Depth=1
	leaq	48(%r14,%rbx), %rbx
	testb	$1, %al
	jne	LBB15_21
## BB#14:                               ##   in Loop: Header=BB15_2 Depth=1
	movq	%rbx, %rdi
	jmp	LBB15_15
	.align	4, 0x90
LBB15_9:                                ##   in Loop: Header=BB15_2 Depth=1
	leaq	48(%r14,%rbx), %rbx
	movq	%r14, %rdi
LBB15_15:                               ## %.critedge.thread
                                        ##   in Loop: Header=BB15_2 Depth=1
	movb	$0, 1(%rdi)
	movb	$0, (%rbx)
	jmp	LBB15_16
LBB15_21:                               ##   in Loop: Header=BB15_2 Depth=1
	movq	16(%rbx), %rax
	movb	$0, (%rax)
	movq	$0, 8(%rbx)
	movq	%rbx, %rdi
LBB15_16:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i
                                        ##   in Loop: Header=BB15_2 Depth=1
Ltmp189:
	xorl	%esi, %esi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp190:
## BB#17:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit
                                        ##   in Loop: Header=BB15_2 Depth=1
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%rbx)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-80(%rbp), %rbx         ## 8-byte Reload
LBB15_18:                               ##   in Loop: Header=BB15_2 Depth=1
	movq	%r15, %r13
	leaq	24(%r15), %r15
	addq	$24, %r12
	cmpq	%rbx, %r15
	jne	LBB15_2
LBB15_19:                               ## %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB15_23:
Ltmp188:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB15_20:
Ltmp183:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB15_22:
Ltmp191:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table15:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset67 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset67
Lset68 = Ltmp181-Lfunc_begin4           ##   Call between Lfunc_begin4 and Ltmp181
	.long	Lset68
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset69 = Ltmp181-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset69
Lset70 = Ltmp182-Ltmp181                ##   Call between Ltmp181 and Ltmp182
	.long	Lset70
Lset71 = Ltmp183-Lfunc_begin4           ##     jumps to Ltmp183
	.long	Lset71
	.byte	1                       ##   On action: 1
Lset72 = Ltmp184-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset72
Lset73 = Ltmp187-Ltmp184                ##   Call between Ltmp184 and Ltmp187
	.long	Lset73
Lset74 = Ltmp188-Lfunc_begin4           ##     jumps to Ltmp188
	.long	Lset74
	.byte	0                       ##   On action: cleanup
Lset75 = Ltmp189-Lfunc_begin4           ## >> Call Site 4 <<
	.long	Lset75
Lset76 = Ltmp190-Ltmp189                ##   Call between Ltmp189 and Ltmp190
	.long	Lset76
Lset77 = Ltmp191-Lfunc_begin4           ##     jumps to Ltmp191
	.long	Lset77
	.byte	1                       ##   On action: 1
Lset78 = Ltmp190-Lfunc_begin4           ## >> Call Site 5 <<
	.long	Lset78
Lset79 = Lfunc_end4-Ltmp190             ##   Call between Ltmp190 and Lfunc_end4
	.long	Lset79
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI16_0:
	.space	16
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_
	.weak_def_can_be_hidden	__ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_
	.align	4, 0x90
__ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_: ## @_ZNSt3__127__insertion_sort_incompleteIRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEbSD_SD_SB_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp211:
	.cfi_def_cfa_offset 16
Ltmp212:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp213:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
Ltmp214:
	.cfi_offset %rbx, -56
Ltmp215:
	.cfi_offset %r12, -48
Ltmp216:
	.cfi_offset %r13, -40
Ltmp217:
	.cfi_offset %r14, -32
Ltmp218:
	.cfi_offset %r15, -24
	movq	%rdx, %rax
	movq	%rsi, %r13
	movq	%r13, -88(%rbp)         ## 8-byte Spill
	movq	%rdi, %rbx
	movq	%r13, %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
	movabsq	$-6148914691236517205, %rcx ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rdx, %rcx
	cmpq	$5, %rcx
	ja	LBB16_7
## BB#1:
	movb	$1, %r15b
	leaq	LJTI16_0(%rip), %rdx
	movslq	(%rdx,%rcx,4), %rcx
	addq	%rdx, %rcx
	jmpq	*%rcx
LBB16_2:
	addq	$-24, %r13
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB16_30
## BB#3:
	movq	16(%rbx), %rax
	movq	%rax, -48(%rbp)
	vmovups	(%rbx), %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbx)
	vmovups	(%r13), %xmm0
	vmovups	%xmm0, (%rbx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r13)
	jmp	LBB16_30
LBB16_7:
	leaq	48(%rbx), %r12
	leaq	24(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%rax, %rcx
	callq	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	leaq	72(%rbx), %rax
	movb	$1, %r15b
	cmpq	%r13, %rax
	je	LBB16_29
## BB#8:                                ## %.lr.ph
	xorl	%r15d, %r15d
	movl	$0, -76(%rbp)           ## 4-byte Folded Spill
	.align	4, 0x90
LBB16_9:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB16_11 Depth 2
	movq	%r12, %r14
	movq	%rax, %r12
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	jbe	LBB16_27
## BB#10:                               ##   in Loop: Header=BB16_9 Depth=1
	movq	16(%r12), %rax
	movq	%rax, -48(%rbp)
	vmovupd	(%r12), %xmm0
	vmovapd	%xmm0, -64(%rbp)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, (%r12)
	movq	$0, 16(%r12)
	xorl	%eax, %eax
	movq	%r15, %rcx
	.align	4, 0x90
LBB16_11:                               ## %._crit_edge
                                        ##   Parent Loop BB16_9 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	%rcx, %r13
	leaq	72(%rbx,%r13), %r14
	testb	$1, %al
	jne	LBB16_12
## BB#13:                               ##   in Loop: Header=BB16_11 Depth=2
	movw	$0, 72(%rbx,%r13)
	jmp	LBB16_14
	.align	4, 0x90
LBB16_12:                               ##   in Loop: Header=BB16_11 Depth=2
	movq	88(%rbx,%r13), %rax
	movb	$0, (%rax)
	movq	$0, 80(%rbx,%r13)
LBB16_14:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i.5
                                        ##   in Loop: Header=BB16_11 Depth=2
Ltmp200:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp201:
## BB#15:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit6
                                        ##   in Loop: Header=BB16_11 Depth=2
	movq	64(%rbx,%r13), %rax
	movq	%rax, 16(%r14)
	vmovupd	48(%rbx,%r13), %xmm0
	vmovupd	%xmm0, (%r14)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovupd	%xmm0, 48(%rbx,%r13)
	movq	$0, 64(%rbx,%r13)
	cmpq	$-48, %r13
	je	LBB16_16
## BB#17:                               ##   in Loop: Header=BB16_11 Depth=2
Ltmp203:
	xorl	%esi, %esi
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
	vmovsd	%xmm0, -72(%rbp)        ## 8-byte Spill
Ltmp204:
## BB#18:                               ## %.noexc
                                        ##   in Loop: Header=BB16_11 Depth=2
	leaq	24(%rbx,%r13), %rdi
Ltmp205:
	xorl	%esi, %esi
	callq	__ZNSt3__14stodERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEPm
Ltmp206:
## BB#19:                               ##   in Loop: Header=BB16_11 Depth=2
	leaq	-24(%r13), %rcx
	vucomisd	-72(%rbp), %xmm0 ## 8-byte Folded Reload
	movb	48(%rbx,%r13), %al
	ja	LBB16_11
## BB#20:                               ## %.critedge
                                        ##   in Loop: Header=BB16_9 Depth=1
	leaq	48(%rbx,%r13), %r14
	testb	$1, %al
	jne	LBB16_32
## BB#21:                               ##   in Loop: Header=BB16_9 Depth=1
	movq	%r14, %rdi
	jmp	LBB16_22
	.align	4, 0x90
LBB16_16:                               ##   in Loop: Header=BB16_9 Depth=1
	leaq	48(%rbx,%r13), %r14
	movq	%rbx, %rdi
LBB16_22:                               ## %.critedge.thread
                                        ##   in Loop: Header=BB16_9 Depth=1
	movb	$0, 1(%rdi)
	movb	$0, (%r14)
	jmp	LBB16_23
LBB16_32:                               ##   in Loop: Header=BB16_9 Depth=1
	movq	16(%r14), %rax
	movb	$0, (%rax)
	movq	$0, 8(%r14)
	movq	%r14, %rdi
LBB16_23:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i
                                        ##   in Loop: Header=BB16_9 Depth=1
Ltmp208:
	xorl	%esi, %esi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp209:
## BB#24:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit
                                        ##   in Loop: Header=BB16_9 Depth=1
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r14)
	vmovapd	-64(%rbp), %xmm0
	vmovupd	%xmm0, (%r14)
	vxorpd	%xmm0, %xmm0, %xmm0
	vmovapd	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movl	-76(%rbp), %r13d        ## 4-byte Reload
	addl	$1, %r13d
	movl	%r13d, -76(%rbp)        ## 4-byte Spill
	leaq	24(%r12), %rax
	movq	%r12, -72(%rbp)         ## 8-byte Spill
	movq	-88(%rbp), %r12         ## 8-byte Reload
	cmpq	%r12, %rax
	sete	%r14b
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	cmpl	$8, %r13d
	movq	%r12, %r13
	movq	-72(%rbp), %r12         ## 8-byte Reload
	je	LBB16_25
LBB16_27:                               ##   in Loop: Header=BB16_9 Depth=1
	leaq	24(%r12), %rax
	addq	$24, %r15
	cmpq	%r13, %rax
	jne	LBB16_9
## BB#28:
                                        ## implicit-def: %R14B
	movb	$1, %r15b
	jmp	LBB16_29
LBB16_4:
	leaq	24(%rbx), %rsi
	addq	$-24, %r13
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	%rax, %rcx
	callq	__ZNSt3__17__sort3IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SB_
	jmp	LBB16_30
LBB16_5:
	leaq	24(%rbx), %rsi
	leaq	48(%rbx), %rdx
	addq	$-24, %r13
	movq	%rbx, %rdi
	movq	%r13, %rcx
	movq	%rax, %r8
	callq	__ZNSt3__17__sort4IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SB_
	jmp	LBB16_30
LBB16_6:
	leaq	48(%rbx), %rdx
	leaq	72(%rbx), %rcx
	addq	$-24, %r13
	leaq	24(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%r13, %r8
	movq	%rax, %r9
	callq	__ZNSt3__17__sort5IRZ18sort_numeric_naiveINS_6vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS6_IS8_EEEEEvRT_EUlSC_RT0_E_PS8_EEjSD_SD_SD_SD_SD_SB_
	jmp	LBB16_30
LBB16_25:
	xorl	%r15d, %r15d
LBB16_29:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit._crit_edge
	orb	%r14b, %r15b
LBB16_30:
	andb	$1, %r15b
	movb	%r15b, %al
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB16_26:
Ltmp207:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB16_31:
Ltmp202:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB16_33:
Ltmp210:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.align	2, 0x90
L16_0_set_30 = LBB16_30-LJTI16_0
L16_0_set_2 = LBB16_2-LJTI16_0
L16_0_set_4 = LBB16_4-LJTI16_0
L16_0_set_5 = LBB16_5-LJTI16_0
L16_0_set_6 = LBB16_6-LJTI16_0
LJTI16_0:
	.long	L16_0_set_30
	.long	L16_0_set_30
	.long	L16_0_set_2
	.long	L16_0_set_4
	.long	L16_0_set_5
	.long	L16_0_set_6
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table16:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset80 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset80
Lset81 = Ltmp200-Lfunc_begin5           ##   Call between Lfunc_begin5 and Ltmp200
	.long	Lset81
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset82 = Ltmp200-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset82
Lset83 = Ltmp201-Ltmp200                ##   Call between Ltmp200 and Ltmp201
	.long	Lset83
Lset84 = Ltmp202-Lfunc_begin5           ##     jumps to Ltmp202
	.long	Lset84
	.byte	1                       ##   On action: 1
Lset85 = Ltmp203-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset85
Lset86 = Ltmp206-Ltmp203                ##   Call between Ltmp203 and Ltmp206
	.long	Lset86
Lset87 = Ltmp207-Lfunc_begin5           ##     jumps to Ltmp207
	.long	Lset87
	.byte	0                       ##   On action: cleanup
Lset88 = Ltmp208-Lfunc_begin5           ## >> Call Site 4 <<
	.long	Lset88
Lset89 = Ltmp209-Ltmp208                ##   Call between Ltmp208 and Ltmp209
	.long	Lset89
Lset90 = Ltmp210-Lfunc_begin5           ##     jumps to Ltmp210
	.long	Lset90
	.byte	1                       ##   On action: 1
Lset91 = Ltmp209-Lfunc_begin5           ## >> Call Site 5 <<
	.long	Lset91
Lset92 = Lfunc_end5-Ltmp209             ##   Call between Ltmp209 and Lfunc_end5
	.long	Lset92
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ERKS8_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ERKS8_
	.align	4, 0x90
__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ERKS8_: ## @_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2ERKS8_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp227:
	.cfi_def_cfa_offset 16
Ltmp228:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp229:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp230:
	.cfi_offset %rbx, -48
Ltmp231:
	.cfi_offset %r12, -40
Ltmp232:
	.cfi_offset %r14, -32
Ltmp233:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r12
	vxorps	%xmm0, %xmm0, %xmm0
	vmovups	%xmm0, (%r12)
	movq	$0, 16(%r12)
	movq	(%r14), %rcx
	movq	8(%r14), %rbx
	movq	%rbx, %rax
	subq	%rcx, %rax
	subq	%rcx, %rbx
	je	LBB17_7
## BB#1:
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %r15 ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %r15
	movabsq	$768614336404564651, %rax ## imm = 0xAAAAAAAAAAAAAAB
	cmpq	%rax, %r15
	jb	LBB17_3
## BB#2:
Ltmp219:
	movq	%r12, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
Ltmp220:
LBB17_3:                                ## %.noexc1
Ltmp221:
	movq	%rbx, %rdi
	callq	__Znwm
	movq	%rax, %rdi
Ltmp222:
## BB#4:
	movq	%rdi, 8(%r12)
	movq	%rdi, (%r12)
	leaq	(%r15,%r15,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movq	%rax, 16(%r12)
	movq	(%r14), %r15
	movq	8(%r14), %rbx
	cmpq	%rbx, %r15
	je	LBB17_7
	.align	4, 0x90
LBB17_5:                                ## %.lr.ph.i.i
                                        ## =>This Inner Loop Header: Depth=1
Ltmp224:
	movq	%r15, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
Ltmp225:
## BB#6:                                ## %.noexc
                                        ##   in Loop: Header=BB17_5 Depth=1
	addq	$24, %r15
	movq	8(%r12), %rdi
	addq	$24, %rdi
	movq	%rdi, 8(%r12)
	cmpq	%r15, %rbx
	jne	LBB17_5
LBB17_7:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE18__construct_at_endIPS6_EENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeESC_SC_m.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB17_8:                                ## %.loopexit
Ltmp226:
LBB17_10:
	movq	%rax, %r14
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	LBB17_15
## BB#11:
	movq	8(%r12), %rdi
	cmpq	%rbx, %rdi
	je	LBB17_14
	.align	4, 0x90
LBB17_12:                               ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, 8(%r12)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	8(%r12), %rdi
	cmpq	%rbx, %rdi
	jne	LBB17_12
## BB#13:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i
	movq	(%r12), %rbx
LBB17_14:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB17_15:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB17_9:                                ## %.loopexit.split-lp
Ltmp223:
	jmp	LBB17_10
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table17:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset93 = Ltmp219-Lfunc_begin6           ## >> Call Site 1 <<
	.long	Lset93
Lset94 = Ltmp222-Ltmp219                ##   Call between Ltmp219 and Ltmp222
	.long	Lset94
Lset95 = Ltmp223-Lfunc_begin6           ##     jumps to Ltmp223
	.long	Lset95
	.byte	0                       ##   On action: cleanup
Lset96 = Ltmp224-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset96
Lset97 = Ltmp225-Ltmp224                ##   Call between Ltmp224 and Ltmp225
	.long	Lset97
Lset98 = Ltmp226-Lfunc_begin6           ##     jumps to Ltmp226
	.long	Lset98
	.byte	0                       ##   On action: cleanup
Lset99 = Ltmp225-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset99
Lset100 = Lfunc_end6-Ltmp225            ##   Call between Ltmp225 and Lfunc_end6
	.long	Lset100
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp255:
	.cfi_def_cfa_offset 16
Ltmp256:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp257:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp258:
	.cfi_offset %rbx, -56
Ltmp259:
	.cfi_offset %r12, -48
Ltmp260:
	.cfi_offset %r13, -40
Ltmp261:
	.cfi_offset %r14, -32
Ltmp262:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp234:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp235:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB18_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB18_7
## BB#3:
Ltmp237:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp238:
## BB#4:                                ## %.noexc
Ltmp239:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp240:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp241:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp242:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB18_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp244:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp245:
## BB#8:
	testq	%rax, %rax
	jne	LBB18_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp246:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp247:
LBB18_10:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB18_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB18_11:
Ltmp248:
	movq	%rax, %r14
	jmp	LBB18_12
LBB18_20:
Ltmp236:
	movq	%rax, %r14
	jmp	LBB18_13
LBB18_19:
Ltmp243:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB18_12:                               ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB18_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp249:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp250:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB18_15
LBB18_16:
Ltmp251:
	movq	%rax, %rbx
Ltmp252:
	callq	___cxa_end_catch
Ltmp253:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB18_18:
Ltmp254:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table18:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset101 = Ltmp234-Lfunc_begin7          ## >> Call Site 1 <<
	.long	Lset101
Lset102 = Ltmp235-Ltmp234               ##   Call between Ltmp234 and Ltmp235
	.long	Lset102
Lset103 = Ltmp236-Lfunc_begin7          ##     jumps to Ltmp236
	.long	Lset103
	.byte	1                       ##   On action: 1
Lset104 = Ltmp237-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset104
Lset105 = Ltmp238-Ltmp237               ##   Call between Ltmp237 and Ltmp238
	.long	Lset105
Lset106 = Ltmp248-Lfunc_begin7          ##     jumps to Ltmp248
	.long	Lset106
	.byte	1                       ##   On action: 1
Lset107 = Ltmp239-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset107
Lset108 = Ltmp242-Ltmp239               ##   Call between Ltmp239 and Ltmp242
	.long	Lset108
Lset109 = Ltmp243-Lfunc_begin7          ##     jumps to Ltmp243
	.long	Lset109
	.byte	1                       ##   On action: 1
Lset110 = Ltmp244-Lfunc_begin7          ## >> Call Site 4 <<
	.long	Lset110
Lset111 = Ltmp247-Ltmp244               ##   Call between Ltmp244 and Ltmp247
	.long	Lset111
Lset112 = Ltmp248-Lfunc_begin7          ##     jumps to Ltmp248
	.long	Lset112
	.byte	1                       ##   On action: 1
Lset113 = Ltmp247-Lfunc_begin7          ## >> Call Site 5 <<
	.long	Lset113
Lset114 = Ltmp249-Ltmp247               ##   Call between Ltmp247 and Ltmp249
	.long	Lset114
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset115 = Ltmp249-Lfunc_begin7          ## >> Call Site 6 <<
	.long	Lset115
Lset116 = Ltmp250-Ltmp249               ##   Call between Ltmp249 and Ltmp250
	.long	Lset116
Lset117 = Ltmp251-Lfunc_begin7          ##     jumps to Ltmp251
	.long	Lset117
	.byte	0                       ##   On action: cleanup
Lset118 = Ltmp250-Lfunc_begin7          ## >> Call Site 7 <<
	.long	Lset118
Lset119 = Ltmp252-Ltmp250               ##   Call between Ltmp250 and Ltmp252
	.long	Lset119
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset120 = Ltmp252-Lfunc_begin7          ## >> Call Site 8 <<
	.long	Lset120
Lset121 = Ltmp253-Ltmp252               ##   Call between Ltmp252 and Ltmp253
	.long	Lset121
Lset122 = Ltmp254-Lfunc_begin7          ##     jumps to Ltmp254
	.long	Lset122
	.byte	1                       ##   On action: 1
Lset123 = Ltmp253-Lfunc_begin7          ## >> Call Site 9 <<
	.long	Lset123
Lset124 = Lfunc_end7-Ltmp253            ##   Call between Ltmp253 and Lfunc_end7
	.long	Lset124
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp266:
	.cfi_def_cfa_offset 16
Ltmp267:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp268:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp269:
	.cfi_offset %rbx, -56
Ltmp270:
	.cfi_offset %r12, -48
Ltmp271:
	.cfi_offset %r13, -40
Ltmp272:
	.cfi_offset %r14, -32
Ltmp273:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB19_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB19_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB19_9
LBB19_3:
	testq	%rbx, %rbx
	jle	LBB19_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	vxorps	%xmm0, %xmm0, %xmm0
	vmovaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp263:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp264:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB19_9
LBB19_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB19_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB19_9
LBB19_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB19_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB19_10:
Ltmp265:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table19:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset125 = Lfunc_begin8-Lfunc_begin8     ## >> Call Site 1 <<
	.long	Lset125
Lset126 = Ltmp263-Lfunc_begin8          ##   Call between Lfunc_begin8 and Ltmp263
	.long	Lset126
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset127 = Ltmp263-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset127
Lset128 = Ltmp264-Ltmp263               ##   Call between Ltmp263 and Ltmp264
	.long	Lset128
Lset129 = Ltmp265-Lfunc_begin8          ##     jumps to Ltmp265
	.long	Lset129
	.byte	0                       ##   On action: cleanup
Lset130 = Ltmp264-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset130
Lset131 = Lfunc_end8-Ltmp264            ##   Call between Ltmp264 and Lfunc_end8
	.long	Lset131
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp274:
	.cfi_def_cfa_offset 16
Ltmp275:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp276:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp277:
	.cfi_offset %rbx, -32
Ltmp278:
	.cfi_offset %r14, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%rbx)
	addq	$64, %rax
	movq	%rax, 112(%rbx)
	leaq	8(%rbx), %r14
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 8(%rbx)
	leaq	72(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r14, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	112(%rbx), %r14
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp282:
	.cfi_offset %rbx, -40
Ltmp283:
	.cfi_offset %r14, -32
Ltmp284:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%rbx)
	leaq	112(%rbx), %r14
	addq	$64, %rax
	movq	%rax, 112(%rbx)
	leaq	8(%rbx), %r15
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 8(%rbx)
	leaq	72(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp285:
	.cfi_def_cfa_offset 16
Ltmp286:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp287:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp288:
	.cfi_offset %rbx, -40
Ltmp289:
	.cfi_offset %r14, -32
Ltmp290:
	.cfi_offset %r15, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	leaq	(%rdi,%rax), %r15
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	leaq	24(%rcx), %rdx
	movq	%rdx, (%rdi,%rax)
	leaq	112(%rdi,%rax), %r14
	addq	$64, %rcx
	movq	%rcx, 112(%rdi,%rax)
	leaq	8(%rdi,%rax), %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 8(%rdi,%rax)
	leaq	72(%rdi,%rax), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%r15, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp291:
	.cfi_def_cfa_offset 16
Ltmp292:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp293:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp294:
	.cfi_offset %rbx, -40
Ltmp295:
	.cfi_offset %r14, -32
Ltmp296:
	.cfi_offset %r15, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	leaq	(%rdi,%rax), %r15
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	leaq	24(%rcx), %rdx
	movq	%rdx, (%rdi,%rax)
	leaq	112(%rdi,%rax), %r14
	addq	$64, %rcx
	movq	%rcx, 112(%rdi,%rax)
	leaq	8(%rdi,%rax), %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 8(%rdi,%rax)
	leaq	72(%rdi,%rax), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%r15, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp297:
	.cfi_def_cfa_offset 16
Ltmp298:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp299:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp300:
	.cfi_offset %rbx, -40
Ltmp301:
	.cfi_offset %r14, -32
Ltmp302:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	leaq	64(%rbx), %r14
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	$0, 88(%rbx)
	movl	96(%rbx), %eax
	testb	$8, %al
	je	LBB24_5
## BB#1:
	movzbl	(%r14), %ecx
	testb	$1, %cl
	jne	LBB24_2
## BB#3:
	shrq	%rcx
	leaq	1(%r14,%rcx), %rcx
	leaq	1(%r14), %rdx
	jmp	LBB24_4
LBB24_2:
	movq	80(%rbx), %rdx
	movq	72(%rbx), %rcx
	addq	%rdx, %rcx
LBB24_4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	%rcx, 88(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rcx, 32(%rbx)
LBB24_5:
	testb	$16, %al
	je	LBB24_14
## BB#6:
	movzbl	(%r14), %r15d
	testb	$1, %r15b
	jne	LBB24_8
## BB#7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4.thread
	shrq	%r15
	leaq	1(%r14,%r15), %rax
	movq	%rax, 88(%rbx)
	movl	$22, %esi
	jmp	LBB24_9
LBB24_8:
	movq	72(%rbx), %r15
	movq	80(%rbx), %rax
	addq	%r15, %rax
	movq	%rax, 88(%rbx)
	movq	64(%rbx), %rsi
	andq	$-2, %rsi
	addq	$-1, %rsi
LBB24_9:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movzbl	(%r14), %eax
	testb	$1, %al
	jne	LBB24_10
## BB#11:
	addq	$1, %r14
	shrq	%rax
	jmp	LBB24_12
LBB24_10:
	movq	72(%rbx), %rax
	movq	80(%rbx), %r14
LBB24_12:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	%r14, %rcx
	addq	%rax, %r14
	movq	%rcx, 48(%rbx)
	movq	%rcx, 40(%rbx)
	movq	%r14, 56(%rbx)
	testb	$3, 96(%rbx)
	je	LBB24_14
## BB#13:
	movslq	%r15d, %rax
	addq	%rax, %rcx
	movq	%rcx, 48(%rbx)
LBB24_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp303:
	.cfi_def_cfa_offset 16
Ltmp304:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp305:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp306:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp307:
	.cfi_def_cfa_offset 16
Ltmp308:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp309:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp310:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp311:
	.cfi_def_cfa_offset 16
Ltmp312:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp313:
	.cfi_def_cfa_register %rbp
	movq	48(%rsi), %r11
	movq	88(%rsi), %r9
	cmpq	%r11, %r9
	jae	LBB27_2
## BB#1:
	movq	%r11, 88(%rsi)
	movq	%r11, %r9
LBB27_2:
	movl	%r8d, %eax
	andl	$24, %eax
	je	LBB27_3
## BB#4:
	cmpl	$1, %ecx
	jne	LBB27_6
## BB#5:
	cmpl	$24, %eax
	je	LBB27_3
LBB27_6:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	je	LBB27_15
## BB#7:
	cmpl	$2, %ecx
	je	LBB27_11
## BB#8:
	cmpl	$1, %ecx
	jne	LBB27_3
## BB#9:
	testb	$8, %r8b
	jne	LBB27_10
## BB#14:
	movq	%r11, %r10
	subq	40(%rsi), %r10
	jmp	LBB27_15
LBB27_11:
	testb	$1, 64(%rsi)
	jne	LBB27_12
## BB#13:
	leaq	64(%rsi), %rax
	addq	$1, %rax
	movq	%r9, %r10
	subq	%rax, %r10
	jmp	LBB27_15
LBB27_10:
	movq	24(%rsi), %r10
	subq	16(%rsi), %r10
	jmp	LBB27_15
LBB27_12:
	movq	%r9, %r10
	subq	80(%rsi), %r10
LBB27_15:
	addq	%rdx, %r10
	js	LBB27_3
## BB#16:
	testb	$1, 64(%rsi)
	jne	LBB27_17
## BB#18:
	leaq	64(%rsi), %rcx
	addq	$1, %rcx
	jmp	LBB27_19
LBB27_17:
	movq	80(%rsi), %rcx
LBB27_19:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r9, %rax
	subq	%rcx, %rax
	cmpq	%r10, %rax
	jl	LBB27_3
## BB#20:
	movl	%r8d, %ecx
	andl	$8, %ecx
	testq	%r10, %r10
	je	LBB27_25
## BB#21:
	testl	%ecx, %ecx
	je	LBB27_23
## BB#22:
	cmpq	$0, 24(%rsi)
	je	LBB27_3
LBB27_23:
	testb	$16, %r8b
	je	LBB27_25
## BB#24:
	testq	%r11, %r11
	jne	LBB27_25
LBB27_3:
	vxorps	%ymm0, %ymm0, %ymm0
	vmovups	%ymm0, 96(%rdi)
	vmovups	%ymm0, 64(%rdi)
	vmovups	%ymm0, 32(%rdi)
	vmovups	%ymm0, (%rdi)
	movq	$-1, 128(%rdi)
LBB27_30:
	movq	%rdi, %rax
	popq	%rbp
	vzeroupper
	retq
LBB27_25:                               ## %._crit_edge
	testl	%ecx, %ecx
	je	LBB27_27
## BB#26:
	movq	16(%rsi), %rax
	addq	%r10, %rax
	movq	%rax, 24(%rsi)
	movq	%r9, 32(%rsi)
LBB27_27:
	testb	$16, %r8b
	je	LBB27_29
## BB#28:
	movslq	%r10d, %rax
	addq	40(%rsi), %rax
	movq	%rax, 48(%rsi)
LBB27_29:
	vxorps	%ymm0, %ymm0, %ymm0
	vmovups	%ymm0, 96(%rdi)
	vmovups	%ymm0, 64(%rdi)
	vmovups	%ymm0, 32(%rdi)
	vmovups	%ymm0, (%rdi)
	movq	%r10, 128(%rdi)
	jmp	LBB27_30
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp314:
	.cfi_def_cfa_offset 16
Ltmp315:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp316:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp317:
	.cfi_offset %rbx, -24
	movl	%edx, %r8d
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movq	144(%rbp), %rdx
	xorl	%ecx, %ecx
	callq	*32(%rax)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp318:
	.cfi_def_cfa_offset 16
Ltmp319:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp320:
	.cfi_def_cfa_register %rbp
	movq	48(%rdi), %rax
	movq	88(%rdi), %rcx
	cmpq	%rax, %rcx
	jae	LBB29_2
## BB#1:
	movq	%rax, 88(%rdi)
	movq	%rax, %rcx
LBB29_2:
	movl	$-1, %eax
	testb	$8, 96(%rdi)
	je	LBB29_7
## BB#3:
	movq	24(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rcx, %rsi
	jae	LBB29_5
## BB#4:
	movq	%rcx, 32(%rdi)
	movq	%rcx, %rsi
LBB29_5:                                ## %._crit_edge
	cmpq	%rsi, %rdx
	jae	LBB29_7
## BB#6:
	movzbl	(%rdx), %eax
LBB29_7:
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp321:
	.cfi_def_cfa_offset 16
Ltmp322:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp323:
	.cfi_def_cfa_register %rbp
	movq	48(%rdi), %rax
	movq	88(%rdi), %r10
	cmpq	%rax, %r10
	jae	LBB30_2
## BB#1:
	movq	%rax, 88(%rdi)
	movq	%rax, %r10
LBB30_2:
	movq	16(%rdi), %r8
	movq	24(%rdi), %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r8
	jae	LBB30_8
## BB#3:
	cmpl	$-1, %esi
	je	LBB30_4
## BB#5:
	testb	$16, 96(%rdi)
	jne	LBB30_7
## BB#6:
	movzbl	-1(%rdx), %r9d
	movzbl	%sil, %ecx
	cmpl	%r9d, %ecx
	jne	LBB30_8
LBB30_7:
	addq	$-1, %rdx
	movq	%r8, 16(%rdi)
	movq	%rdx, 24(%rdi)
	movq	%r10, 32(%rdi)
	movb	%sil, (%rdx)
	movl	%esi, %eax
LBB30_8:
	popq	%rbp
	retq
LBB30_4:
	addq	$-1, %rdx
	movq	%rdx, 24(%rdi)
	movq	%r10, 32(%rdi)
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp329:
	.cfi_def_cfa_offset 16
Ltmp330:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp331:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp332:
	.cfi_offset %rbx, -56
Ltmp333:
	.cfi_offset %r12, -48
Ltmp334:
	.cfi_offset %r13, -40
Ltmp335:
	.cfi_offset %r14, -32
Ltmp336:
	.cfi_offset %r15, -24
	movl	%esi, %r14d
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	cmpl	$-1, %r14d
	je	LBB31_19
## BB#1:
	movq	24(%rbx), %r15
	movq	48(%rbx), %r13
	subq	16(%rbx), %r15
	movq	56(%rbx), %rax
	cmpq	%rax, %r13
	je	LBB31_3
## BB#2:                                ## %._crit_edge
	leaq	88(%rbx), %rcx
	movq	88(%rbx), %rdi
	leaq	96(%rbx), %rdx
	jmp	LBB31_12
LBB31_3:
	movl	$-1, %r12d
	testb	$16, 96(%rbx)
	je	LBB31_19
## BB#4:
	movq	40(%rbx), %rax
	movq	%rax, -72(%rbp)         ## 8-byte Spill
	movq	88(%rbx), %rax
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	leaq	64(%rbx), %rdi
	movq	%rdi, -56(%rbp)         ## 8-byte Spill
Ltmp324:
	xorl	%esi, %esi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp325:
## BB#5:
	movl	$22, %esi
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	testb	$1, (%rdi)
	je	LBB31_7
## BB#6:
	movq	(%rdi), %rsi
	andq	$-2, %rsi
	addq	$-1, %rsi
LBB31_7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
Ltmp326:
	xorl	%edx, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp327:
## BB#8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	movq	-72(%rbp), %rax         ## 8-byte Reload
	subq	%rax, %r13
	leaq	88(%rbx), %rcx
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	subq	%rax, %rdi
	movq	-56(%rbp), %rsi         ## 8-byte Reload
	movzbl	(%rsi), %eax
	testb	$1, %al
	jne	LBB31_9
## BB#10:
	addq	$1, %rsi
	shrq	%rax
	jmp	LBB31_11
LBB31_9:
	movq	72(%rbx), %rax
	movq	80(%rbx), %rsi
LBB31_11:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	leaq	96(%rbx), %rdx
	addq	%rsi, %rax
	movq	%rsi, 40(%rbx)
	movq	%rax, 56(%rbx)
	movslq	%r13d, %r13
	addq	%rsi, %r13
	movq	%r13, 48(%rbx)
	addq	%rsi, %rdi
	movq	%rdi, 88(%rbx)
LBB31_12:
	leaq	1(%r13), %rsi
	movq	%rsi, -48(%rbp)
	cmpq	%rdi, %rsi
	leaq	-48(%rbp), %rdi
	cmovbq	%rcx, %rdi
	movq	(%rdi), %rdi
	movq	%rdi, (%rcx)
	testb	$8, (%rdx)
	je	LBB31_17
## BB#13:
	testb	$1, 64(%rbx)
	jne	LBB31_14
## BB#15:
	leaq	64(%rbx), %rcx
	addq	$1, %rcx
	jmp	LBB31_16
LBB31_14:
	movq	80(%rbx), %rcx
LBB31_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	addq	%rcx, %r15
	movq	%rcx, 16(%rbx)
	movq	%r15, 24(%rbx)
	movq	%rdi, 32(%rbx)
LBB31_17:
	cmpq	%rax, %r13
	je	LBB31_21
## BB#18:
	movq	%rsi, 48(%rbx)
	movb	%r14b, (%r13)
	movzbl	%r14b, %r12d
LBB31_19:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB31_21:
	movq	(%rbx), %rax
	movq	104(%rax), %rax
	movzbl	%r14b, %esi
	movq	%rbx, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   ## TAILCALL
LBB31_20:
Ltmp328:
	movq	%rax, %rdi
	callq	___cxa_begin_catch
	callq	___cxa_end_catch
	jmp	LBB31_19
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table31:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset132 = Ltmp324-Lfunc_begin9          ## >> Call Site 1 <<
	.long	Lset132
Lset133 = Ltmp327-Ltmp324               ##   Call between Ltmp324 and Ltmp327
	.long	Lset133
Lset134 = Ltmp328-Lfunc_begin9          ##     jumps to Ltmp328
	.long	Lset134
	.byte	1                       ##   On action: 1
Lset135 = Ltmp327-Lfunc_begin9          ## >> Call Site 2 <<
	.long	Lset135
Lset136 = Lfunc_end9-Ltmp327            ##   Call between Ltmp327 and Lfunc_end9
	.long	Lset136
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.weak_def_can_be_hidden	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.align	4, 0x90
__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv: ## @_ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp337:
	.cfi_def_cfa_offset 16
Ltmp338:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp339:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
Ltmp340:
	.cfi_offset %rbx, -56
Ltmp341:
	.cfi_offset %r12, -48
Ltmp342:
	.cfi_offset %r13, -40
Ltmp343:
	.cfi_offset %r14, -32
Ltmp344:
	.cfi_offset %r15, -24
	movl	96(%rsi), %eax
	testb	$16, %al
	jne	LBB32_1
## BB#27:
	testb	$8, %al
	jne	LBB32_28
## BB#53:
	vxorps	%xmm0, %xmm0, %xmm0
	vmovups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%rdi, %rax
	jmp	LBB32_52
LBB32_1:
	movq	48(%rsi), %r15
	movq	88(%rsi), %rax
	movq	%rax, %rcx
	cmpq	%r15, %rax
	jae	LBB32_3
## BB#2:
	movq	%r15, 88(%rsi)
	movq	%r15, %rcx
LBB32_3:
	movq	40(%rsi), %rbx
	vxorps	%xmm0, %xmm0, %xmm0
	vmovups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%rcx, %r14
	subq	%rbx, %r14
	cmpq	$-16, %r14
	jb	LBB32_6
## BB#4:                                ## %.thread.i.i.i.1
	movq	%rcx, -56(%rbp)         ## 8-byte Spill
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	movq	%rdi, %r13
	callq	__ZNKSt3__121__basic_string_commonILb1EE20__throw_length_errorEv
	jmp	LBB32_5
LBB32_28:
	movq	16(%rsi), %rbx
	movq	32(%rsi), %r12
	vxorps	%xmm0, %xmm0, %xmm0
	vmovups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	movq	%r12, %r13
	subq	%rbx, %r13
	cmpq	$-16, %r13
	jb	LBB32_31
## BB#29:                               ## %.thread.i.i.i
	movq	%rdi, %r14
	callq	__ZNKSt3__121__basic_string_commonILb1EE20__throw_length_errorEv
	jmp	LBB32_30
LBB32_6:
	movq	%rcx, -56(%rbp)         ## 8-byte Spill
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	cmpq	$22, %r14
	movq	%rdi, %r13
	ja	LBB32_5
## BB#7:
	leaq	(%r14,%r14), %rax
	movb	%al, (%r13)
	movq	%r13, %rax
	addq	$1, %rax
	jmp	LBB32_8
LBB32_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm.exit.i.i.i.2
	leaq	16(%r14), %r12
	andq	$-16, %r12
	movq	%r12, %rdi
	callq	__Znwm
	movq	%rax, 16(%r13)
	orq	$1, %r12
	movq	%r12, (%r13)
	movq	%r14, 8(%r13)
LBB32_8:                                ## %.preheader.i.i.i.4
	movq	%r13, %r10
	movq	-48(%rbp), %r11         ## 8-byte Reload
	movq	-56(%rbp), %rcx         ## 8-byte Reload
	cmpq	%rcx, %rbx
	je	LBB32_51
## BB#9:                                ## %.lr.ph.i.i.i.7.preheader
	cmpq	%r15, %r11
	movq	%r15, %rsi
	cmovaq	%r11, %rsi
	movq	%rsi, %r8
	subq	%rbx, %r8
	cmpq	$128, %r8
	jae	LBB32_11
## BB#10:
	movq	%rax, %rcx
	jmp	LBB32_24
LBB32_11:                               ## %min.iters.checked
	movq	%r8, %rcx
	andq	$-128, %rcx
	je	LBB32_12
## BB#13:                               ## %vector.memcheck
	cmpq	%r15, %r11
	movq	%r15, %rdx
	cmovaq	%r11, %rdx
	leaq	-1(%rdx), %rdi
	cmpq	%rdi, %rax
	ja	LBB32_16
## BB#14:                               ## %vector.memcheck
	movq	%rbx, %rdi
	notq	%rdi
	addq	%rdi, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	LBB32_16
## BB#15:
	movq	%rax, %rcx
	jmp	LBB32_24
LBB32_31:
	cmpq	$22, %r13
	movq	%rdi, %r14
	ja	LBB32_30
## BB#32:
	leaq	(%r13,%r13), %rax
	movb	%al, (%r14)
	movq	%r14, %rax
	addq	$1, %rax
	jmp	LBB32_33
LBB32_30:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm.exit.i.i.i
	leaq	16(%r13), %r15
	andq	$-16, %r15
	movq	%r15, %rdi
	callq	__Znwm
	movq	%rax, 16(%r14)
	orq	$1, %r15
	movq	%r15, (%r14)
	movq	%r13, 8(%r14)
LBB32_33:                               ## %.preheader.i.i.i
	movq	%r14, %r10
	cmpq	%r12, %rbx
	je	LBB32_51
## BB#34:                               ## %.lr.ph.i.i.i.preheader
	cmpq	$128, %r13
	jae	LBB32_36
## BB#35:
	movq	%rax, %rcx
	jmp	LBB32_49
LBB32_36:                               ## %min.iters.checked538
	movq	%r13, %rcx
	andq	$-128, %rcx
	je	LBB32_37
## BB#38:                               ## %vector.memcheck551
	leaq	-1(%r12), %rdx
	cmpq	%rdx, %rax
	ja	LBB32_41
## BB#39:                               ## %vector.memcheck551
	movq	%rbx, %rdx
	notq	%rdx
	addq	%r12, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rbx
	ja	LBB32_41
## BB#40:
	movq	%rax, %rcx
	jmp	LBB32_49
LBB32_12:
	movq	%rax, %rcx
	jmp	LBB32_24
LBB32_16:                               ## %vector.body.preheader
	leaq	-128(%r8), %r9
	movl	%r9d, %edi
	shrl	$7, %edi
	addl	$1, %edi
	xorl	%edx, %edx
	testb	$3, %dil
	je	LBB32_19
## BB#17:                               ## %vector.body.prol.preheader
	addl	$-128, %esi
	subl	%ebx, %esi
	shrl	$7, %esi
	addl	$1, %esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edx, %edx
	.align	4, 0x90
LBB32_18:                               ## %vector.body.prol
                                        ## =>This Inner Loop Header: Depth=1
	vmovups	(%rbx,%rdx), %ymm0
	vmovups	32(%rbx,%rdx), %ymm1
	vmovups	64(%rbx,%rdx), %ymm2
	vmovups	96(%rbx,%rdx), %ymm3
	vmovups	%ymm0, (%rax,%rdx)
	vmovups	%ymm1, 32(%rax,%rdx)
	vmovups	%ymm2, 64(%rax,%rdx)
	vmovups	%ymm3, 96(%rax,%rdx)
	subq	$-128, %rdx
	addq	$1, %rsi
	jne	LBB32_18
LBB32_19:                               ## %vector.body.preheader.split
	cmpq	$384, %r9               ## imm = 0x180
	jb	LBB32_22
## BB#20:                               ## %vector.body.preheader.split.split
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	leaq	480(%rax,%rdx), %rdi
	leaq	480(%rbx,%rdx), %rdx
	.align	4, 0x90
LBB32_21:                               ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	vmovups	-480(%rdx), %ymm0
	vmovups	-448(%rdx), %ymm1
	vmovups	-416(%rdx), %ymm2
	vmovups	-384(%rdx), %ymm3
	vmovups	%ymm0, -480(%rdi)
	vmovups	%ymm1, -448(%rdi)
	vmovups	%ymm2, -416(%rdi)
	vmovups	%ymm3, -384(%rdi)
	vmovups	-352(%rdx), %ymm0
	vmovups	-320(%rdx), %ymm1
	vmovups	-288(%rdx), %ymm2
	vmovups	-256(%rdx), %ymm3
	vmovups	%ymm0, -352(%rdi)
	vmovups	%ymm1, -320(%rdi)
	vmovups	%ymm2, -288(%rdi)
	vmovups	%ymm3, -256(%rdi)
	vmovups	-224(%rdx), %ymm0
	vmovups	-192(%rdx), %ymm1
	vmovups	-160(%rdx), %ymm2
	vmovups	-128(%rdx), %ymm3
	vmovups	%ymm0, -224(%rdi)
	vmovups	%ymm1, -192(%rdi)
	vmovups	%ymm2, -160(%rdi)
	vmovups	%ymm3, -128(%rdi)
	vmovups	-96(%rdx), %ymm0
	vmovups	-64(%rdx), %ymm1
	vmovups	-32(%rdx), %ymm2
	vmovups	(%rdx), %ymm3
	vmovups	%ymm0, -96(%rdi)
	vmovups	%ymm1, -64(%rdi)
	vmovups	%ymm2, -32(%rdi)
	vmovups	%ymm3, (%rdi)
	addq	$512, %rdi              ## imm = 0x200
	addq	$512, %rdx              ## imm = 0x200
	addq	$-512, %rsi             ## imm = 0xFFFFFFFFFFFFFE00
	jne	LBB32_21
LBB32_22:                               ## %middle.block
	cmpq	%rcx, %r8
	je	LBB32_26
## BB#23:
	addq	%rcx, %rbx
	addq	%rax, %rcx
LBB32_24:                               ## %.lr.ph.i.i.i.7.preheader1079
	cmpq	%r15, %r11
	cmovaq	%r11, %r15
	.align	4, 0x90
LBB32_25:                               ## %.lr.ph.i.i.i.7
                                        ## =>This Inner Loop Header: Depth=1
	movb	(%rbx), %dl
	movb	%dl, (%rcx)
	addq	$1, %rbx
	addq	$1, %rcx
	cmpq	%rbx, %r15
	jne	LBB32_25
LBB32_26:                               ## %._crit_edge.loopexit.i.i.i.9
	addq	%r14, %rax
	jmp	LBB32_51
LBB32_37:
	movq	%rax, %rcx
	jmp	LBB32_49
LBB32_41:                               ## %vector.body532.preheader
	leaq	-128(%r13), %rdx
	movl	%edx, %esi
	shrl	$7, %esi
	addl	$1, %esi
	xorl	%edi, %edi
	testb	$3, %sil
	je	LBB32_44
## BB#42:                               ## %vector.body532.prol.preheader
	leal	-128(%r12), %esi
	subl	%ebx, %esi
	shrl	$7, %esi
	addl	$1, %esi
	andl	$3, %esi
	negq	%rsi
	xorl	%edi, %edi
	.align	4, 0x90
LBB32_43:                               ## %vector.body532.prol
                                        ## =>This Inner Loop Header: Depth=1
	vmovups	(%rbx,%rdi), %ymm0
	vmovups	32(%rbx,%rdi), %ymm1
	vmovups	64(%rbx,%rdi), %ymm2
	vmovups	96(%rbx,%rdi), %ymm3
	vmovups	%ymm0, (%rax,%rdi)
	vmovups	%ymm1, 32(%rax,%rdi)
	vmovups	%ymm2, 64(%rax,%rdi)
	vmovups	%ymm3, 96(%rax,%rdi)
	subq	$-128, %rdi
	addq	$1, %rsi
	jne	LBB32_43
LBB32_44:                               ## %vector.body532.preheader.split
	cmpq	$384, %rdx              ## imm = 0x180
	jb	LBB32_47
## BB#45:                               ## %vector.body532.preheader.split.split
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	leaq	480(%rax,%rdi), %rsi
	leaq	480(%rbx,%rdi), %rdi
	.align	4, 0x90
LBB32_46:                               ## %vector.body532
                                        ## =>This Inner Loop Header: Depth=1
	vmovups	-480(%rdi), %ymm0
	vmovups	-448(%rdi), %ymm1
	vmovups	-416(%rdi), %ymm2
	vmovups	-384(%rdi), %ymm3
	vmovups	%ymm0, -480(%rsi)
	vmovups	%ymm1, -448(%rsi)
	vmovups	%ymm2, -416(%rsi)
	vmovups	%ymm3, -384(%rsi)
	vmovups	-352(%rdi), %ymm0
	vmovups	-320(%rdi), %ymm1
	vmovups	-288(%rdi), %ymm2
	vmovups	-256(%rdi), %ymm3
	vmovups	%ymm0, -352(%rsi)
	vmovups	%ymm1, -320(%rsi)
	vmovups	%ymm2, -288(%rsi)
	vmovups	%ymm3, -256(%rsi)
	vmovups	-224(%rdi), %ymm0
	vmovups	-192(%rdi), %ymm1
	vmovups	-160(%rdi), %ymm2
	vmovups	-128(%rdi), %ymm3
	vmovups	%ymm0, -224(%rsi)
	vmovups	%ymm1, -192(%rsi)
	vmovups	%ymm2, -160(%rsi)
	vmovups	%ymm3, -128(%rsi)
	vmovups	-96(%rdi), %ymm0
	vmovups	-64(%rdi), %ymm1
	vmovups	-32(%rdi), %ymm2
	vmovups	(%rdi), %ymm3
	vmovups	%ymm0, -96(%rsi)
	vmovups	%ymm1, -64(%rsi)
	vmovups	%ymm2, -32(%rsi)
	vmovups	%ymm3, (%rsi)
	addq	$512, %rsi              ## imm = 0x200
	addq	$512, %rdi              ## imm = 0x200
	addq	$-512, %rdx             ## imm = 0xFFFFFFFFFFFFFE00
	jne	LBB32_46
LBB32_47:                               ## %middle.block533
	cmpq	%rcx, %r13
	je	LBB32_50
## BB#48:
	addq	%rcx, %rbx
	addq	%rax, %rcx
	.align	4, 0x90
LBB32_49:                               ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movb	(%rbx), %dl
	movb	%dl, (%rcx)
	addq	$1, %rbx
	addq	$1, %rcx
	cmpq	%rbx, %r12
	jne	LBB32_49
LBB32_50:                               ## %._crit_edge.loopexit.i.i.i
	addq	%r13, %rax
LBB32_51:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1IPcEET_S8_RKS4_.exit
	movb	$0, (%rax)
	movq	%r10, %rax
LBB32_52:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	vzeroupper
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"/dev/urandom"

	.section	__TEXT,__const
	.align	2                       ## @.ref.tmp
l_.ref.tmp:
	.long	10                      ## 0xa
	.long	100                     ## 0x64
	.long	1000                    ## 0x3e8
	.long	10000                   ## 0x2710
	.long	100000                  ## 0x186a0
	.long	1000000                 ## 0xf4240
	.long	10000000                ## 0x989680

	.section	__TEXT,__cstring,cstring_literals
L_.str.1:                               ## @.str.1
	.asciz	"size: "

L_.str.2:                               ## @.str.2
	.asciz	" single: "

L_.str.3:                               ## @.str.3
	.asciz	", naive: "

L_.str.4:                               ## @.str.4
	.asciz	"ms"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE ## @_ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE


.subsections_via_symbols
