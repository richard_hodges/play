	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__ZrsRNSt3__113basic_istreamIcNS_11char_traitsIcEEEE14success_marker
	.align	4, 0x90
__ZrsRNSt3__113basic_istreamIcNS_11char_traitsIcEEEE14success_marker: ## @_ZrsRNSt3__113basic_istreamIcNS_11char_traitsIcEEEE14success_marker
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	leaq	-32(%rbp), %rax
	movq	%rsi, -32(%rbp)
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movl	32(%rsi), %ecx
	andl	$5, %ecx
	cmpl	$0, %ecx
	setne	%dl
	xorb	$-1, %dl
	andb	$1, %dl
	movzbl	%dl, %esi
	leaq	-32(%rbp), %rdi
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	__ZNK14success_marker4markEb
	movq	-40(%rbp), %rax
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK14success_marker4markEb
	.weak_def_can_be_hidden	__ZNK14success_marker4markEb
	.align	4, 0x90
__ZNK14success_marker4markEb:           ## @_ZNK14success_marker4markEb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movq	%rdi, -8(%rbp)
	andb	$1, %al
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdi
	movb	-9(%rbp), %al
	movq	(%rdi), %rdi
	andb	$1, %al
	movb	%al, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z12mark_successRb
	.align	4, 0x90
__Z12mark_successRb:                    ## @_Z12mark_successRb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	-8(%rbp), %rax
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZN14success_markerC1ERb
	movq	-8(%rbp), %rax
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN14success_markerC1ERb
	.weak_def_can_be_hidden	__ZN14success_markerC1ERb
	.align	4, 0x90
__ZN14success_markerC1ERb:              ## @_ZN14success_markerC1ERb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN14success_markerC2ERb
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z4testRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIcS2_EE
	.align	4, 0x90
__Z4testRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIcS2_EE: ## @_Z4testRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIcS2_EE
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp58:
	.cfi_def_cfa_offset 16
Ltmp59:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp60:
	.cfi_def_cfa_register %rbp
	subq	$848, %rsp              ## imm = 0x350
	xorl	%eax, %eax
	movl	$24, %ecx
	movl	%ecx, %edx
	leaq	-504(%rbp), %r8
	movq	%rdi, -464(%rbp)
	movq	%rsi, -472(%rbp)
	movb	$0, -473(%rbp)
	movb	$0, -474(%rbp)
	movq	%r8, -456(%rbp)
	movq	-456(%rbp), %rsi
	movq	%rsi, -448(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	-440(%rbp), %rdi
	movq	%rdi, -432(%rbp)
	movq	-432(%rbp), %rdi
	movq	%rdi, -424(%rbp)
	movq	-424(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -416(%rbp)
	movq	%rsi, -624(%rbp)        ## 8-byte Spill
	movl	%eax, %esi
	callq	_memset
	movq	-624(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -392(%rbp)
	movq	-392(%rbp), %rdi
	movq	%rdi, -384(%rbp)
	movq	-384(%rbp), %rdi
	movq	%rdi, -376(%rbp)
	movq	-376(%rbp), %rdi
	movq	%rdi, -400(%rbp)
	movl	$0, -404(%rbp)
LBB4_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -404(%rbp)
	jae	LBB4_3
## BB#2:                                ##   in Loop: Header=BB4_1 Depth=1
	movl	-404(%rbp), %eax
	movl	%eax, %ecx
	movq	-400(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-404(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -404(%rbp)
	jmp	LBB4_1
LBB4_3:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-528(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -312(%rbp)
	movq	%rcx, -632(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-632(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rdx, -272(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rdx, -296(%rbp)
	movl	$0, -300(%rbp)
LBB4_4:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -300(%rbp)
	jae	LBB4_6
## BB#5:                                ##   in Loop: Header=BB4_4 Depth=1
	movl	-300(%rbp), %eax
	movl	%eax, %ecx
	movq	-296(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-300(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -300(%rbp)
	jmp	LBB4_4
LBB4_6:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3
	movq	-472(%rbp), %rax
Ltmp12:
	leaq	-504(%rbp), %rdi
	movl	$34, %esi
	movl	$92, %edx
	movq	%rax, -640(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_
Ltmp13:
	movq	%rdx, -648(%rbp)        ## 8-byte Spill
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	jmp	LBB4_7
LBB4_7:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -544(%rbp)
	movq	-648(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -536(%rbp)
	movq	-640(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -240(%rbp)
	leaq	-544(%rbp), %rsi
	movq	%rsi, -248(%rbp)
	movq	-240(%rbp), %rdi
	movq	-544(%rbp), %rsi
	movsbl	-535(%rbp), %ecx
	movsbl	-536(%rbp), %edx
Ltmp14:
	callq	__ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_
Ltmp15:
	movq	%rax, -664(%rbp)        ## 8-byte Spill
	jmp	LBB4_8
LBB4_8:                                 ## %_ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RKNS_14__quoted_proxyIS6_S7_T1_EE.exit
	jmp	LBB4_9
LBB4_9:
Ltmp16:
	leaq	-473(%rbp), %rdi
	callq	__Z12mark_successRb
Ltmp17:
	movq	%rax, -672(%rbp)        ## 8-byte Spill
	jmp	LBB4_10
LBB4_10:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -568(%rbp)
Ltmp18:
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZrsRNSt3__113basic_istreamIcNS_11char_traitsIcEEEE14success_marker
Ltmp19:
	movq	%rax, -680(%rbp)        ## 8-byte Spill
	jmp	LBB4_11
LBB4_11:
Ltmp20:
	leaq	-528(%rbp), %rdi
	movl	$34, %esi
	movl	$92, %edx
	callq	__ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_
Ltmp21:
	movq	%rdx, -688(%rbp)        ## 8-byte Spill
	movq	%rax, -696(%rbp)        ## 8-byte Spill
	jmp	LBB4_12
LBB4_12:
	movq	-696(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -584(%rbp)
	movq	-688(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -576(%rbp)
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -224(%rbp)
	leaq	-584(%rbp), %rsi
	movq	%rsi, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	-584(%rbp), %rsi
	movsbl	-575(%rbp), %ecx
	movsbl	-576(%rbp), %edx
Ltmp22:
	callq	__ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_
Ltmp23:
	movq	%rax, -704(%rbp)        ## 8-byte Spill
	jmp	LBB4_13
LBB4_13:                                ## %_ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RKNS_14__quoted_proxyIS6_S7_T1_EE.exit5
	jmp	LBB4_14
LBB4_14:
Ltmp24:
	leaq	-474(%rbp), %rdi
	callq	__Z12mark_successRb
Ltmp25:
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	jmp	LBB4_15
LBB4_15:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -592(%rbp)
Ltmp26:
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZrsRNSt3__113basic_istreamIcNS_11char_traitsIcEEEE14success_marker
Ltmp27:
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	jmp	LBB4_16
LBB4_16:
	movq	-464(%rbp), %rsi
Ltmp28:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp29:
	movq	%rax, -728(%rbp)        ## 8-byte Spill
	jmp	LBB4_17
LBB4_17:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -208(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -216(%rbp)
	movq	-208(%rbp), %rdi
Ltmp30:
	callq	*%rcx
Ltmp31:
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB4_18
LBB4_18:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit6
	jmp	LBB4_19
LBB4_19:
	movq	-464(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -744(%rbp)        ## 8-byte Spill
	je	LBB4_21
## BB#20:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -752(%rbp)        ## 8-byte Spill
	jmp	LBB4_22
LBB4_21:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -752(%rbp)        ## 8-byte Spill
LBB4_22:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6lengthEv.exit
	movq	-752(%rbp), %rax        ## 8-byte Reload
	leaq	-616(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	%rax, -104(%rbp)
	movb	$61, -105(%rbp)
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movb	$61, -81(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	%rax, -48(%rbp)
	movq	%rax, -40(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-80(%rbp), %rsi
	movsbl	-81(%rbp), %edx
Ltmp32:
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
Ltmp33:
	jmp	LBB4_23
LBB4_23:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Emc.exit
	jmp	LBB4_24
LBB4_24:
Ltmp34:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	-616(%rbp), %rsi
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp35:
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	jmp	LBB4_25
LBB4_25:
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rdi
Ltmp36:
	callq	*%rcx
Ltmp37:
	movq	%rax, -768(%rbp)        ## 8-byte Spill
	jmp	LBB4_26
LBB4_26:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit7
	jmp	LBB4_27
LBB4_27:
	leaq	-616(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movzbl	-473(%rbp), %eax
Ltmp39:
	andl	$1, %eax
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movl	%eax, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEb
Ltmp40:
	movq	%rax, -776(%rbp)        ## 8-byte Spill
	jmp	LBB4_28
LBB4_28:
Ltmp41:
	leaq	L_.str(%rip), %rsi
	movq	-776(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp42:
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	jmp	LBB4_29
LBB4_29:
Ltmp43:
	leaq	-504(%rbp), %rsi
	movq	-784(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp44:
	movq	%rax, -792(%rbp)        ## 8-byte Spill
	jmp	LBB4_30
LBB4_30:
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp45:
	callq	*%rcx
Ltmp46:
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	jmp	LBB4_31
LBB4_31:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit8
	jmp	LBB4_32
LBB4_32:
	movzbl	-474(%rbp), %eax
Ltmp47:
	andl	$1, %eax
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movl	%eax, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEb
Ltmp48:
	movq	%rax, -808(%rbp)        ## 8-byte Spill
	jmp	LBB4_33
LBB4_33:
Ltmp49:
	leaq	L_.str(%rip), %rsi
	movq	-808(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp50:
	movq	%rax, -816(%rbp)        ## 8-byte Spill
	jmp	LBB4_34
LBB4_34:
Ltmp51:
	leaq	-528(%rbp), %rsi
	movq	-816(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp52:
	movq	%rax, -824(%rbp)        ## 8-byte Spill
	jmp	LBB4_35
LBB4_35:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -264(%rbp)
	movq	-256(%rbp), %rdi
Ltmp53:
	callq	*%rcx
Ltmp54:
	movq	%rax, -832(%rbp)        ## 8-byte Spill
	jmp	LBB4_36
LBB4_36:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit4
	jmp	LBB4_37
LBB4_37:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rax
	movq	%rax, -360(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rax
	movq	%rax, -368(%rbp)
	movq	-360(%rbp), %rdi
Ltmp55:
	callq	*%rax
Ltmp56:
	movq	%rax, -840(%rbp)        ## 8-byte Spill
	jmp	LBB4_38
LBB4_38:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB4_39
LBB4_39:
	leaq	-528(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-504(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$848, %rsp              ## imm = 0x350
	popq	%rbp
	retq
LBB4_40:
Ltmp57:
	movl	%edx, %ecx
	movq	%rax, -552(%rbp)
	movl	%ecx, -556(%rbp)
	jmp	LBB4_42
LBB4_41:
Ltmp38:
	leaq	-616(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -552(%rbp)
	movl	%ecx, -556(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB4_42:
	leaq	-528(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-504(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#43:
	movq	-552(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp12-Lfunc_begin0             ##   Call between Lfunc_begin0 and Ltmp12
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp12-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp33-Ltmp12                   ##   Call between Ltmp12 and Ltmp33
	.long	Lset3
Lset4 = Ltmp57-Lfunc_begin0             ##     jumps to Ltmp57
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp34-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp37-Ltmp34                   ##   Call between Ltmp34 and Ltmp37
	.long	Lset6
Lset7 = Ltmp38-Lfunc_begin0             ##     jumps to Ltmp38
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp39-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp56-Ltmp39                   ##   Call between Ltmp39 and Ltmp56
	.long	Lset9
Lset10 = Ltmp57-Lfunc_begin0            ##     jumps to Ltmp57
	.long	Lset10
	.byte	0                       ##   On action: cleanup
Lset11 = Ltmp56-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Lfunc_end0-Ltmp56              ##   Call between Ltmp56 and Lfunc_end0
	.long	Lset12
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_
	.weak_def_can_be_hidden	__ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_
	.align	4, 0x90
__ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_: ## @_ZNSt3__16quotedIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_14__quoted_proxyIT_T0_T1_EERNS_12basic_stringIS6_S7_S8_EES6_S6_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movb	%dl, %al
	movb	%sil, %cl
	movq	%rdi, -24(%rbp)
	movb	%cl, -25(%rbp)
	movb	%al, -26(%rbp)
	movq	-24(%rbp), %rsi
	movsbl	-26(%rbp), %ecx
	movsbl	-25(%rbp), %edx
	leaq	-16(%rbp), %rdi
	callq	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERNS_12basic_stringIcS2_S4_EEcc
	movq	-16(%rbp), %rax
	movq	-8(%rbp), %rdx
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp64:
	.cfi_def_cfa_offset 16
Ltmp65:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp66:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB6_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB6_3
LBB6_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB6_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB6_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB6_6
LBB6_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB6_6:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp67:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp68:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB7_1
LBB7_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp69:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp70:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB7_3
LBB7_2:
Ltmp71:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB7_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table7:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset13 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset13
Lset14 = Ltmp67-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp67
	.long	Lset14
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp67-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset15
Lset16 = Ltmp70-Ltmp67                  ##   Call between Ltmp67 and Ltmp70
	.long	Lset16
Lset17 = Ltmp71-Lfunc_begin1            ##     jumps to Ltmp71
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp70-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset18
Lset19 = Lfunc_end1-Ltmp70              ##   Call between Ltmp70 and Lfunc_end1
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp75:
	.cfi_def_cfa_offset 16
Ltmp76:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp77:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp111:
	.cfi_def_cfa_offset 16
Ltmp112:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp113:
	.cfi_def_cfa_register %rbp
	subq	$1104, %rsp             ## imm = 0x450
	movl	$0, -540(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, -528(%rbp)
	leaq	L_.str.1(%rip), %rcx
	movq	%rcx, -536(%rbp)
	movq	-528(%rbp), %rdx
	movq	%rdx, -512(%rbp)
	movq	%rcx, -520(%rbp)
	movq	-512(%rbp), %rcx
	movq	%rcx, -504(%rbp)
	movq	%rcx, -496(%rbp)
	movq	%rcx, -488(%rbp)
	movq	%rcx, -480(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-520(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rax, -936(%rbp)        ## 8-byte Spill
	movq	%rcx, -944(%rbp)        ## 8-byte Spill
	movq	%rdx, -952(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-944(%rbp), %rdi        ## 8-byte Reload
	movq	-952(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
	leaq	-816(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-936(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -448(%rbp)
	movl	$8, -452(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$120, %rdx
	movq	%rdx, -432(%rbp)
	movq	%rdx, -424(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 120(%rcx)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 120(%rcx)
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	%rsi, (%rcx)
	addq	$64, %rdx
	movq	%rdx, 120(%rcx)
	movq	%rcx, %rdx
	addq	$16, %rdx
	movq	%rcx, -248(%rbp)
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%rsi, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rdx
	movq	-256(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rsi
	movq	-24(%rdi), %rdi
	movq	%rsi, (%rdx,%rdi)
	movq	$0, 8(%rdx)
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-264(%rbp), %rsi
	movq	%rdx, -232(%rbp)
	movq	%rsi, -240(%rbp)
	movq	-232(%rbp), %rdx
Ltmp78:
	movq	%rdx, %rdi
	movq	%rcx, -960(%rbp)        ## 8-byte Spill
	movq	%rdx, -968(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp79:
	jmp	LBB9_1
LBB9_1:                                 ## %_ZNSt3__113basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-968(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-968(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-960(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 120(%rdi)
	addq	$16, %rdi
	movq	-448(%rbp), %rdx
	movl	-452(%rbp), %eax
	orl	$8, %eax
	movq	%rdi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movl	%eax, -412(%rbp)
	movq	-400(%rbp), %rdx
	movq	-408(%rbp), %rsi
	movq	%rdx, -360(%rbp)
	movq	%rsi, -368(%rbp)
	movl	%eax, -372(%rbp)
	movq	-360(%rbp), %rdx
Ltmp81:
	movq	%rdx, %rdi
	movq	%rdx, -976(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp82:
	jmp	LBB9_2
LBB9_2:                                 ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-976(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %r8
	movq	%r8, -328(%rbp)
	movq	-328(%rbp), %r8
	movq	%r8, -320(%rbp)
	movq	-320(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -312(%rbp)
	movq	%rdi, -984(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -992(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-992(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rdx, -272(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rdx, -296(%rbp)
	movl	$0, -300(%rbp)
LBB9_3:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -300(%rbp)
	jae	LBB9_5
## BB#4:                                ##   in Loop: Header=BB9_3 Depth=1
	movl	-300(%rbp), %eax
	movl	%eax, %ecx
	movq	-296(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-300(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -300(%rbp)
	jmp	LBB9_3
LBB9_5:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	movq	-976(%rbp), %rax        ## 8-byte Reload
	movq	$0, 88(%rax)
	movl	-372(%rbp), %ecx
	movl	%ecx, 96(%rax)
	movq	-368(%rbp), %rsi
Ltmp84:
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp85:
	jmp	LBB9_11
LBB9_6:
Ltmp86:
	movl	%edx, %ecx
	movq	%rax, -384(%rbp)
	movl	%ecx, -388(%rbp)
	movq	-984(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-976(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-384(%rbp), %rax
	movl	-388(%rbp), %ecx
	movq	%rax, -1000(%rbp)       ## 8-byte Spill
	movl	%ecx, -1004(%rbp)       ## 4-byte Spill
	jmp	LBB9_9
LBB9_7:
Ltmp80:
	movl	%edx, %ecx
	movq	%rax, -464(%rbp)
	movl	%ecx, -468(%rbp)
	jmp	LBB9_10
LBB9_8:
Ltmp83:
	movl	%edx, %ecx
	movq	%rax, -1000(%rbp)       ## 8-byte Spill
	movl	%ecx, -1004(%rbp)       ## 4-byte Spill
	jmp	LBB9_9
LBB9_9:                                 ## %.body.i
	movl	-1004(%rbp), %eax       ## 4-byte Reload
	movq	-1000(%rbp), %rcx       ## 8-byte Reload
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -464(%rbp)
	movl	%eax, -468(%rbp)
	movq	-960(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
LBB9_10:
	movq	-960(%rbp), %rax        ## 8-byte Reload
	addq	$120, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-464(%rbp), %rax
	movl	-468(%rbp), %ecx
	movq	%rax, -1016(%rbp)       ## 8-byte Spill
	movl	%ecx, -1020(%rbp)       ## 4-byte Spill
	jmp	LBB9_28
LBB9_11:                                ## %_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKNS_12basic_stringIcS2_S4_EEj.exit
	jmp	LBB9_12
LBB9_12:
	leaq	-840(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-880(%rbp), %rdi
	movq	%rdi, -216(%rbp)
	leaq	L_.str.2(%rip), %rdi
	movq	%rdi, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-208(%rbp), %rdi
Ltmp87:
	movq	%rdi, -1032(%rbp)       ## 8-byte Spill
	movq	%rax, -1040(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp88:
	movq	%rax, -1048(%rbp)       ## 8-byte Spill
	jmp	LBB9_13
LBB9_13:                                ## %.noexc
Ltmp89:
	movq	-1040(%rbp), %rdi       ## 8-byte Reload
	movq	-1032(%rbp), %rsi       ## 8-byte Reload
	movq	-1048(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp90:
	jmp	LBB9_14
LBB9_14:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
	jmp	LBB9_15
LBB9_15:
Ltmp91:
	leaq	-880(%rbp), %rdi
	leaq	-816(%rbp), %rsi
	callq	__Z4testRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIcS2_EE
Ltmp92:
	jmp	LBB9_16
LBB9_16:
	leaq	-880(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-816(%rbp), %rdi
	movq	-24(%rdi), %rdi
	leaq	-816(%rbp,%rdi), %rdi
	movq	%rdi, -152(%rbp)
	movl	$0, -156(%rbp)
	movq	-152(%rbp), %rdi
Ltmp94:
	xorl	%esi, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp95:
	jmp	LBB9_17
LBB9_17:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE5clearEj.exit
	jmp	LBB9_18
LBB9_18:
	leaq	-904(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	L_.str.3(%rip), %rax
	movq	%rax, -144(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-128(%rbp), %rcx
Ltmp96:
	movq	%rcx, %rdi
	movq	%rax, -1056(%rbp)       ## 8-byte Spill
	movq	%rcx, -1064(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp97:
	movq	%rax, -1072(%rbp)       ## 8-byte Spill
	jmp	LBB9_19
LBB9_19:                                ## %.noexc3
Ltmp98:
	movq	-1056(%rbp), %rdi       ## 8-byte Reload
	movq	-1064(%rbp), %rsi       ## 8-byte Reload
	movq	-1072(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp99:
	jmp	LBB9_20
LBB9_20:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit5
	jmp	LBB9_21
LBB9_21:
	leaq	-816(%rbp), %rax
	movq	%rax, -72(%rbp)
	leaq	-904(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rcx
	addq	$16, %rcx
Ltmp100:
	movq	%rcx, %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp101:
	jmp	LBB9_22
LBB9_22:                                ## %_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE.exit
	jmp	LBB9_23
LBB9_23:
	leaq	-904(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-928(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	leaq	L_.str.4(%rip), %rdi
	movq	%rdi, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-48(%rbp), %rdi
Ltmp103:
	movq	%rdi, -1080(%rbp)       ## 8-byte Spill
	movq	%rax, -1088(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp104:
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	jmp	LBB9_24
LBB9_24:                                ## %.noexc7
Ltmp105:
	movq	-1088(%rbp), %rdi       ## 8-byte Reload
	movq	-1080(%rbp), %rsi       ## 8-byte Reload
	movq	-1096(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp106:
	jmp	LBB9_25
LBB9_25:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit9
	jmp	LBB9_26
LBB9_26:
Ltmp108:
	leaq	-928(%rbp), %rdi
	leaq	-816(%rbp), %rsi
	callq	__Z4testRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIcS2_EE
Ltmp109:
	jmp	LBB9_27
LBB9_27:
	leaq	-928(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-816(%rbp), %rdi
	movl	$0, -540(%rbp)
	callq	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-540(%rbp), %eax
	addq	$1104, %rsp             ## imm = 0x450
	popq	%rbp
	retq
LBB9_28:                                ## %.body
	leaq	-840(%rbp), %rdi
	movq	-1016(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movl	-1020(%rbp), %ecx       ## 4-byte Reload
	movl	%ecx, -852(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB9_34
LBB9_29:
Ltmp107:
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	jmp	LBB9_33
LBB9_30:
Ltmp93:
	leaq	-880(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB9_33
LBB9_31:
Ltmp102:
	leaq	-904(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB9_33
LBB9_32:
Ltmp110:
	leaq	-928(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB9_33:
	leaq	-816(%rbp), %rdi
	callq	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB9_34:
	movq	-848(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table9:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\237\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\234\001"              ## Call site table length
Lset20 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset20
Lset21 = Ltmp78-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp78
	.long	Lset21
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset22 = Ltmp78-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset22
Lset23 = Ltmp79-Ltmp78                  ##   Call between Ltmp78 and Ltmp79
	.long	Lset23
Lset24 = Ltmp80-Lfunc_begin2            ##     jumps to Ltmp80
	.long	Lset24
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp81-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset25
Lset26 = Ltmp82-Ltmp81                  ##   Call between Ltmp81 and Ltmp82
	.long	Lset26
Lset27 = Ltmp83-Lfunc_begin2            ##     jumps to Ltmp83
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp82-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset28
Lset29 = Ltmp84-Ltmp82                  ##   Call between Ltmp82 and Ltmp84
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp84-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset30
Lset31 = Ltmp85-Ltmp84                  ##   Call between Ltmp84 and Ltmp85
	.long	Lset31
Lset32 = Ltmp86-Lfunc_begin2            ##     jumps to Ltmp86
	.long	Lset32
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp87-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset33
Lset34 = Ltmp90-Ltmp87                  ##   Call between Ltmp87 and Ltmp90
	.long	Lset34
Lset35 = Ltmp107-Lfunc_begin2           ##     jumps to Ltmp107
	.long	Lset35
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp91-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset36
Lset37 = Ltmp92-Ltmp91                  ##   Call between Ltmp91 and Ltmp92
	.long	Lset37
Lset38 = Ltmp93-Lfunc_begin2            ##     jumps to Ltmp93
	.long	Lset38
	.byte	0                       ##   On action: cleanup
Lset39 = Ltmp94-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset39
Lset40 = Ltmp99-Ltmp94                  ##   Call between Ltmp94 and Ltmp99
	.long	Lset40
Lset41 = Ltmp107-Lfunc_begin2           ##     jumps to Ltmp107
	.long	Lset41
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp100-Lfunc_begin2           ## >> Call Site 9 <<
	.long	Lset42
Lset43 = Ltmp101-Ltmp100                ##   Call between Ltmp100 and Ltmp101
	.long	Lset43
Lset44 = Ltmp102-Lfunc_begin2           ##     jumps to Ltmp102
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp103-Lfunc_begin2           ## >> Call Site 10 <<
	.long	Lset45
Lset46 = Ltmp106-Ltmp103                ##   Call between Ltmp103 and Ltmp106
	.long	Lset46
Lset47 = Ltmp107-Lfunc_begin2           ##     jumps to Ltmp107
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp108-Lfunc_begin2           ## >> Call Site 11 <<
	.long	Lset48
Lset49 = Ltmp109-Ltmp108                ##   Call between Ltmp108 and Ltmp109
	.long	Lset49
Lset50 = Ltmp110-Lfunc_begin2           ##     jumps to Ltmp110
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp109-Lfunc_begin2           ## >> Call Site 12 <<
	.long	Lset51
Lset52 = Lfunc_end2-Ltmp109             ##   Call between Ltmp109 and Lfunc_end2
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp114:
	.cfi_def_cfa_offset 16
Ltmp115:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp116:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$120, %rsi
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN14success_markerC2ERb
	.weak_def_can_be_hidden	__ZN14success_markerC2ERb
	.align	4, 0x90
__ZN14success_markerC2ERb:              ## @_ZN14success_markerC2ERb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp117:
	.cfi_def_cfa_offset 16
Ltmp118:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp119:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$16, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp123:
	.cfi_def_cfa_offset 16
Ltmp124:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp125:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp126:
	.cfi_def_cfa_offset 16
Ltmp127:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp128:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp129:
	.cfi_def_cfa_offset 16
Ltmp130:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp131:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp132:
	.cfi_def_cfa_offset 16
Ltmp133:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp134:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp135:
	.cfi_def_cfa_offset 16
Ltmp136:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp137:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$64, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp138:
	.cfi_def_cfa_offset 16
Ltmp139:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp140:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp141:
	.cfi_def_cfa_offset 16
Ltmp142:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp143:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movl	%ecx, -636(%rbp)
	movl	%r8d, -640(%rbp)
	movq	-624(%rbp), %rdx
	movq	88(%rdx), %rsi
	movq	%rdx, %r9
	movq	%r9, -616(%rbp)
	movq	-616(%rbp), %r9
	cmpq	48(%r9), %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdi, -664(%rbp)        ## 8-byte Spill
	movq	%rdx, -672(%rbp)        ## 8-byte Spill
	jae	LBB19_2
## BB#1:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB19_2:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$0, %eax
	jne	LBB19_4
## BB#3:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	$-1, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %r8
	movq	%rdi, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-24(%rbp), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_4:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$24, %eax
	jne	LBB19_7
## BB#5:
	cmpl	$1, -636(%rbp)
	jne	LBB19_7
## BB#6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	$-1, -72(%rbp)
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r8
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-56(%rbp), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_7:
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -692(%rbp)        ## 4-byte Spill
	je	LBB19_8
	jmp	LBB19_38
LBB19_38:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -696(%rbp)        ## 4-byte Spill
	je	LBB19_9
	jmp	LBB19_39
LBB19_39:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -700(%rbp)        ## 4-byte Spill
	je	LBB19_13
	jmp	LBB19_17
LBB19_8:
	movq	$0, -648(%rbp)
	jmp	LBB19_18
LBB19_9:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB19_11
## BB#10:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	24(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	jmp	LBB19_12
LBB19_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	40(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
LBB19_12:
	jmp	LBB19_18
LBB19_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -712(%rbp)        ## 8-byte Spill
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	je	LBB19_15
## BB#14:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB19_16
LBB19_15:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB19_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, -648(%rbp)
	jmp	LBB19_18
LBB19_17:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	$-1, -248(%rbp)
	movq	-240(%rbp), %rdi
	movq	-248(%rbp), %r8
	movq	%rdi, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -736(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-232(%rbp), %rcx
	movq	-736(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_18:
	movq	-632(%rbp), %rax
	addq	-648(%rbp), %rax
	movq	%rax, -648(%rbp)
	cmpq	$0, -648(%rbp)
	jl	LBB19_23
## BB#19:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	je	LBB19_21
## BB#20:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
	jmp	LBB19_22
LBB19_21:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
LBB19_22:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	-744(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	cmpq	-648(%rbp), %rcx
	jge	LBB19_24
LBB19_23:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -384(%rbp)
	movq	$-1, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	-392(%rbp), %r8
	movq	%rdi, -368(%rbp)
	movq	%r8, -376(%rbp)
	movq	-368(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-376(%rbp), %rcx
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_24:
	cmpq	$0, -648(%rbp)
	je	LBB19_32
## BB#25:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB19_28
## BB#26:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	-400(%rbp), %rax
	cmpq	$0, 24(%rax)
	jne	LBB19_28
## BB#27:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-776(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_28:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB19_31
## BB#29:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB19_31
## BB#30:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -464(%rbp)
	movq	$-1, -472(%rbp)
	movq	-464(%rbp), %rdi
	movq	-472(%rbp), %r8
	movq	%rdi, -448(%rbp)
	movq	%r8, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -784(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-456(%rbp), %rcx
	movq	-784(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB19_37
LBB19_31:
	jmp	LBB19_32
LBB19_32:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB19_34
## BB#33:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-648(%rbp), %rdx
	movq	-672(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -496(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdi, -520(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-520(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB19_34:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB19_36
## BB#35:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -536(%rbp)
	movq	-536(%rbp), %rdx
	movq	56(%rdx), %rdx
	movq	%rax, -544(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-560(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-648(%rbp), %rcx
	movl	%ecx, %esi
	movq	%rax, -568(%rbp)
	movl	%esi, -572(%rbp)
	movq	-568(%rbp), %rax
	movl	-572(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB19_36:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-648(%rbp), %rcx
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	-600(%rbp), %rcx
	movq	-608(%rbp), %r8
	movq	%rcx, -584(%rbp)
	movq	%r8, -592(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, %rdi
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-592(%rbp), %rcx
	movq	-792(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
LBB19_37:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp144:
	.cfi_def_cfa_offset 16
Ltmp145:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp146:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-16(%rbp), %rsi
	movq	(%rsi), %r9
	movq	32(%r9), %r9
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	128(%rcx), %rdx
	movl	-20(%rbp), %r10d
	movl	%r8d, %ecx
	movl	%r10d, %r8d
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	*%r9
	movq	-32(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp147:
	.cfi_def_cfa_offset 16
Ltmp148:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp149:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jae	LBB21_2
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	48(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB21_2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB21_8
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	cmpq	88(%rcx), %rax
	jae	LBB21_5
## BB#4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-120(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	24(%rdx), %rdx
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-40(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB21_5:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	24(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	32(%rcx), %rax
	jae	LBB21_7
## BB#6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -100(%rbp)
	jmp	LBB21_9
LBB21_7:
	jmp	LBB21_8
LBB21_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -100(%rbp)
LBB21_9:
	movl	-100(%rbp), %eax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp150:
	.cfi_def_cfa_offset 16
Ltmp151:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp152:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -160(%rbp)
	movl	%esi, -164(%rbp)
	movq	-160(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -176(%rbp)        ## 8-byte Spill
	jae	LBB22_2
## BB#1:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	48(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB22_2:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB22_9
## BB#3:
	movl	-164(%rbp), %edi
	movl	%edi, -180(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-180(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB22_4
	jmp	LBB22_5
LBB22_4:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-32(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -148(%rbp)
	jmp	LBB22_10
LBB22_5:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB22_7
## BB#6:
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB22_7
	jmp	LBB22_8
LBB22_7:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movb	%al, (%rcx)
	movl	-164(%rbp), %edi
	movl	%edi, -148(%rbp)
	jmp	LBB22_10
LBB22_8:
	jmp	LBB22_9
LBB22_9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -148(%rbp)
LBB22_10:
	movl	-148(%rbp), %eax
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp158:
	.cfi_def_cfa_offset 16
Ltmp159:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp160:
	.cfi_def_cfa_register %rbp
	subq	$896, %rsp              ## imm = 0x380
	movq	%rdi, -632(%rbp)
	movl	%esi, -636(%rbp)
	movq	-632(%rbp), %rdi
	movl	-636(%rbp), %esi
	movq	%rdi, -712(%rbp)        ## 8-byte Spill
	movl	%esi, -716(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-716(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB23_38
## BB#1:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -616(%rbp)
	movq	-616(%rbp), %rax
	movq	24(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -608(%rbp)
	movq	-608(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -576(%rbp)
	movq	-576(%rbp), %rax
	movq	48(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -568(%rbp)
	movq	-568(%rbp), %rcx
	cmpq	56(%rcx), %rax
	jne	LBB23_26
## BB#2:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB23_4
## BB#3:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	jmp	LBB23_39
LBB23_4:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
## BB#6:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	-736(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -656(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rdx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
## BB#7:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	-752(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -680(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
Ltmp153:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp154:
	jmp	LBB23_8
LBB23_8:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	movq	%rcx, -768(%rbp)        ## 8-byte Spill
	je	LBB23_10
## BB#9:
	movq	-768(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB23_11
LBB23_10:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB23_11
LBB23_11:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-776(%rbp), %rax        ## 8-byte Reload
	decq	%rax
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rdi
Ltmp155:
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp156:
	jmp	LBB23_12
LBB23_12:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	jmp	LBB23_13
LBB23_13:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	je	LBB23_15
## BB#14:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	jmp	LBB23_16
LBB23_15:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
LBB23_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-688(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	addq	$64, %rsi
	movq	%rsi, -272(%rbp)
	movq	-272(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movq	-264(%rbp), %rdi
	movq	%rdi, -256(%rbp)
	movq	-256(%rbp), %rdi
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	movzbl	(%rdi), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rcx, -808(%rbp)        ## 8-byte Spill
	movq	%rdx, -816(%rbp)        ## 8-byte Spill
	movq	%rsi, -824(%rbp)        ## 8-byte Spill
	je	LBB23_18
## BB#17:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
	jmp	LBB23_19
LBB23_18:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
LBB23_19:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-832(%rbp), %rax        ## 8-byte Reload
	movq	-816(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-800(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-808(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-296(%rbp), %rsi
	movq	%rsi, 56(%rcx)
## BB#20:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-656(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -304(%rbp)
	movl	%edx, -308(%rbp)
	movq	-304(%rbp), %rax
	movl	-308(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
## BB#21:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -840(%rbp)        ## 8-byte Spill
## BB#22:
	movq	-840(%rbp), %rax        ## 8-byte Reload
	addq	-680(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	jmp	LBB23_25
LBB23_23:
Ltmp157:
	movl	%edx, %ecx
	movq	%rax, -664(%rbp)
	movl	%ecx, -668(%rbp)
## BB#24:
	movq	-664(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -848(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	callq	___cxa_end_catch
	jmp	LBB23_39
LBB23_25:
	jmp	LBB23_26
LBB23_26:
	leaq	-368(%rbp), %rax
	leaq	-696(%rbp), %rcx
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	48(%rdx), %rdx
	addq	$1, %rdx
	movq	%rdx, -696(%rbp)
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	addq	$88, %rdx
	movq	%rcx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rax, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	-360(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB23_28
## BB#27:
	movq	-384(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	jmp	LBB23_29
LBB23_28:
	movq	-376(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
LBB23_29:                               ## %_ZNSt3__13maxIPcEERKT_S4_S4_.exit
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	movl	96(%rcx), %edx
	andl	$8, %edx
	cmpl	$0, %edx
	je	LBB23_34
## BB#30:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -520(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	-496(%rbp), %rcx
	movq	%rcx, -488(%rbp)
	movq	-488(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	je	LBB23_32
## BB#31:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
	jmp	LBB23_33
LBB23_32:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
LBB23_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-872(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -704(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rcx
	movq	-704(%rbp), %rdx
	addq	-648(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -528(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	movq	%rdi, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB23_34:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	-636(%rbp), %ecx
	movb	%cl, %dl
	movq	%rax, -592(%rbp)
	movb	%dl, -593(%rbp)
	movq	-592(%rbp), %rax
	movq	48(%rax), %rsi
	cmpq	56(%rax), %rsi
	movq	%rax, -880(%rbp)        ## 8-byte Spill
	jne	LBB23_36
## BB#35:
	movq	-880(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movsbl	-593(%rbp), %edi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movq	-880(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -580(%rbp)
	jmp	LBB23_37
LBB23_36:
	movb	-593(%rbp), %al
	movq	-880(%rbp), %rcx        ## 8-byte Reload
	movq	48(%rcx), %rdx
	movq	%rdx, %rsi
	addq	$1, %rsi
	movq	%rsi, 48(%rcx)
	movb	%al, (%rdx)
	movsbl	-593(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -580(%rbp)
LBB23_37:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	-580(%rbp), %eax
	movl	%eax, -620(%rbp)
	jmp	LBB23_39
LBB23_38:
	movl	-636(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -620(%rbp)
LBB23_39:
	movl	-620(%rbp), %eax
	addq	$896, %rsp              ## imm = 0x380
	popq	%rbp
	retq
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table23:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset53 = Ltmp153-Lfunc_begin3           ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp156-Ltmp153                ##   Call between Ltmp153 and Ltmp156
	.long	Lset54
Lset55 = Ltmp157-Lfunc_begin3           ##     jumps to Ltmp157
	.long	Lset55
	.byte	1                       ##   On action: 1
Lset56 = Ltmp156-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset56
Lset57 = Lfunc_end3-Ltmp156             ##   Call between Ltmp156 and Lfunc_end3
	.long	Lset57
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp161:
	.cfi_def_cfa_offset 16
Ltmp162:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp163:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp164:
	.cfi_def_cfa_offset 16
Ltmp165:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp166:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp167:
	.cfi_def_cfa_offset 16
Ltmp168:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp169:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp170:
	.cfi_def_cfa_offset 16
Ltmp171:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp172:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB27_1
	jmp	LBB27_2
LBB27_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB27_3
LBB27_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB27_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp173:
	.cfi_def_cfa_offset 16
Ltmp174:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp175:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp176:
	.cfi_def_cfa_offset 16
Ltmp177:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp178:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERNS_12basic_stringIcS2_S4_EEcc
	.weak_def_can_be_hidden	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERNS_12basic_stringIcS2_S4_EEcc
	.align	4, 0x90
__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERNS_12basic_stringIcS2_S4_EEcc: ## @_ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERNS_12basic_stringIcS2_S4_EEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp179:
	.cfi_def_cfa_offset 16
Ltmp180:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp181:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movb	%cl, %al
	movb	%dl, %r8b
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movb	%r8b, -17(%rbp)
	movb	%al, -18(%rbp)
	movq	-8(%rbp), %rdi
	movb	-17(%rbp), %al
	movb	-18(%rbp), %r8b
	movq	-16(%rbp), %rsi
	movsbl	%al, %edx
	movsbl	%r8b, %ecx
	callq	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERNS_12basic_stringIcS2_S4_EEcc
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERNS_12basic_stringIcS2_S4_EEcc
	.weak_def_can_be_hidden	__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERNS_12basic_stringIcS2_S4_EEcc
	.align	4, 0x90
__ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERNS_12basic_stringIcS2_S4_EEcc: ## @_ZNSt3__114__quoted_proxyIcNS_11char_traitsIcEENS_9allocatorIcEEEC2ERNS_12basic_stringIcS2_S4_EEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp182:
	.cfi_def_cfa_offset 16
Ltmp183:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp184:
	.cfi_def_cfa_register %rbp
	movb	%cl, %al
	movb	%dl, %r8b
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movb	%r8b, -17(%rbp)
	movb	%al, -18(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, (%rsi)
	movb	-17(%rbp), %al
	movb	%al, 8(%rsi)
	movb	-18(%rbp), %al
	movb	%al, 9(%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_
	.weak_def_can_be_hidden	__ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_
	.align	4, 0x90
__ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_: ## @_ZNSt3__114__quoted_inputIcNS_11char_traitsIcEENS_12basic_stringIcS2_NS_9allocatorIcEEEEEERNS_13basic_istreamIT_T0_EESB_RT1_S8_S8_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp197:
	.cfi_def_cfa_offset 16
Ltmp198:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp199:
	.cfi_def_cfa_register %rbp
	subq	$656, %rsp              ## imm = 0x290
	movb	%cl, %al
	movb	%dl, %r8b
	movq	%rdi, -472(%rbp)
	movq	%rsi, -480(%rbp)
	movb	%r8b, -481(%rbp)
	movb	%al, -482(%rbp)
	movq	-480(%rbp), %rsi
	movq	%rsi, -448(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	%rsi, -432(%rbp)
	movq	-432(%rbp), %rdi
	movq	%rdi, -424(%rbp)
	movq	-424(%rbp), %rdi
	movq	%rdi, -416(%rbp)
	movq	-416(%rbp), %rdi
	movzbl	(%rdi), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rsi, -528(%rbp)        ## 8-byte Spill
	je	LBB33_2
## BB#1:
	leaq	-449(%rbp), %rsi
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	16(%rcx), %rdi
	movb	$0, -449(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rsi, 8(%rcx)
	jmp	LBB33_3
LBB33_2:
	leaq	-450(%rbp), %rsi
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rdi
	movb	$0, -450(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	$0, -408(%rbp)
	movq	-400(%rbp), %rcx
	movq	-408(%rbp), %rsi
	shlq	$1, %rsi
	movb	%sil, %dl
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movb	%dl, (%rcx)
LBB33_3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit
	leaq	-483(%rbp), %rsi
	movq	-472(%rbp), %rdi
	callq	__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
	movq	-472(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -280(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rsi, -272(%rbp)
	movq	-272(%rbp), %rsi
	movl	32(%rsi), %ecx
	andl	$5, %ecx
	cmpl	$0, %ecx
	movq	%rax, -536(%rbp)        ## 8-byte Spill
	je	LBB33_5
## BB#4:
	movq	-472(%rbp), %rax
	movq	%rax, -464(%rbp)
	jmp	LBB33_32
LBB33_5:
	movb	-483(%rbp), %al
	movsbl	%al, %edi
	movsbl	-481(%rbp), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB33_7
## BB#6:
	movq	-472(%rbp), %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEE5ungetEv
	movq	-472(%rbp), %rdi
	movq	-480(%rbp), %rsi
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
	movq	-472(%rbp), %rsi
	movq	%rsi, -464(%rbp)
	movq	%rax, -552(%rbp)        ## 8-byte Spill
	jmp	LBB33_32
LBB33_7:
	leaq	-504(%rbp), %rax
	movq	-472(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rax, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rax, -240(%rbp)
	movq	%rcx, -248(%rbp)
	movq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-248(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movl	8(%rcx), %esi
	movl	%esi, 8(%rax)
	addq	$12, %rax
	movq	-248(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rax, -560(%rbp)        ## 8-byte Spill
	movq	%rcx, -568(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-568(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB33_8
	jmp	LBB33_12
LBB33_8:
	movq	-568(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)
	movb	$32, -185(%rbp)
	movq	-184(%rbp), %rsi
	leaq	-200(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -576(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-576(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
Ltmp185:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp186:
	movq	%rax, -584(%rbp)        ## 8-byte Spill
	jmp	LBB33_9
LBB33_9:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i.i.i
	movb	-185(%rbp), %al
	movq	-584(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -160(%rbp)
	movb	%al, -161(%rbp)
	movq	-160(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-161(%rbp), %edi
Ltmp187:
	movl	%edi, -588(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-588(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -600(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-600(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp188:
	movb	%al, -601(%rbp)         ## 1-byte Spill
	jmp	LBB33_11
LBB33_10:
Ltmp189:
	leaq	-200(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -208(%rbp)
	movl	%ecx, -212(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-208(%rbp), %rax
	movq	%rax, -616(%rbp)        ## 8-byte Spill
	jmp	LBB33_34
LBB33_11:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i.i.i
	leaq	-200(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-601(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-568(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB33_12:                               ## %_ZNSt3__112__save_flagsIcNS_11char_traitsIcEEEC1ERNS_9basic_iosIcS2_EE.exit
	movq	-568(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movq	-560(%rbp), %rsi        ## 8-byte Reload
	movb	%dl, (%rsi)
	movq	-472(%rbp), %rdi
	movq	(%rdi), %r8
	movq	-24(%r8), %r8
	addq	%r8, %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movl	$4096, -140(%rbp)       ## imm = 0x1000
	movq	-136(%rbp), %rdi
	movl	-140(%rbp), %ecx
	xorl	$-1, %ecx
	andl	8(%rdi), %ecx
	movl	%ecx, 8(%rdi)
## BB#13:
	jmp	LBB33_14
LBB33_14:                               ## =>This Inner Loop Header: Depth=1
	jmp	LBB33_15
LBB33_15:                               ##   in Loop: Header=BB33_14 Depth=1
	movq	-472(%rbp), %rdi
Ltmp190:
	leaq	-483(%rbp), %rsi
	callq	__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
Ltmp191:
	movq	%rax, -624(%rbp)        ## 8-byte Spill
	jmp	LBB33_16
LBB33_16:                               ##   in Loop: Header=BB33_14 Depth=1
	movq	-472(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movl	32(%rax), %edx
	andl	$5, %edx
	cmpl	$0, %edx
	setne	%sil
	movb	%sil, -625(%rbp)        ## 1-byte Spill
## BB#17:                               ##   in Loop: Header=BB33_14 Depth=1
	movb	-625(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB33_18
	jmp	LBB33_20
LBB33_18:
	jmp	LBB33_31
LBB33_19:
Ltmp196:
	leaq	-504(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -512(%rbp)
	movl	%esi, -516(%rbp)
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movq	(%rax), %rcx
	movl	8(%rax), %esi
	movq	%rcx, -88(%rbp)
	movl	%esi, -92(%rbp)
	movq	-88(%rbp), %rcx
	movl	8(%rcx), %esi
	movl	%esi, -96(%rbp)
	movl	-92(%rbp), %esi
	movl	%esi, 8(%rcx)
	movq	(%rax), %rcx
	movb	12(%rax), %dil
	movq	%rcx, -72(%rbp)
	movb	%dil, -73(%rbp)
	movq	-72(%rbp), %rax
	movl	144(%rax), %esi
	movb	%sil, %dil
	movb	%dil, -74(%rbp)
	movsbl	-73(%rbp), %esi
	movl	%esi, 144(%rax)
	jmp	LBB33_33
LBB33_20:                               ##   in Loop: Header=BB33_14 Depth=1
	movb	-483(%rbp), %al
	movsbl	%al, %edi
	movsbl	-482(%rbp), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB33_21
	jmp	LBB33_26
LBB33_21:                               ##   in Loop: Header=BB33_14 Depth=1
	movq	-472(%rbp), %rdi
Ltmp192:
	leaq	-483(%rbp), %rsi
	callq	__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
Ltmp193:
	movq	%rax, -640(%rbp)        ## 8-byte Spill
	jmp	LBB33_22
LBB33_22:                               ##   in Loop: Header=BB33_14 Depth=1
	movq	-472(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movl	32(%rax), %edx
	andl	$5, %edx
	cmpl	$0, %edx
	setne	%sil
	movb	%sil, -641(%rbp)        ## 1-byte Spill
## BB#23:                               ##   in Loop: Header=BB33_14 Depth=1
	movb	-641(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB33_24
	jmp	LBB33_25
LBB33_24:
	jmp	LBB33_31
LBB33_25:                               ##   in Loop: Header=BB33_14 Depth=1
	jmp	LBB33_29
LBB33_26:                               ##   in Loop: Header=BB33_14 Depth=1
	movb	-483(%rbp), %al
	movsbl	%al, %edi
	movsbl	-481(%rbp), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB33_27
	jmp	LBB33_28
LBB33_27:
	jmp	LBB33_31
LBB33_28:                               ##   in Loop: Header=BB33_14 Depth=1
	jmp	LBB33_29
LBB33_29:                               ##   in Loop: Header=BB33_14 Depth=1
	movq	-480(%rbp), %rdi
	movsbl	-483(%rbp), %esi
Ltmp194:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp195:
	jmp	LBB33_30
LBB33_30:                               ##   in Loop: Header=BB33_14 Depth=1
	jmp	LBB33_14
LBB33_31:
	leaq	-504(%rbp), %rax
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rcx
	movl	8(%rax), %edx
	movq	%rcx, -24(%rbp)
	movl	%edx, -28(%rbp)
	movq	-24(%rbp), %rcx
	movl	8(%rcx), %edx
	movl	%edx, -32(%rbp)
	movl	-28(%rbp), %edx
	movl	%edx, 8(%rcx)
	movq	(%rax), %rcx
	movb	12(%rax), %sil
	movq	%rcx, -8(%rbp)
	movb	%sil, -9(%rbp)
	movq	-8(%rbp), %rax
	movl	144(%rax), %edx
	movb	%dl, %sil
	movb	%sil, -10(%rbp)
	movsbl	-9(%rbp), %edx
	movl	%edx, 144(%rax)
LBB33_32:
	movq	-464(%rbp), %rax
	addq	$656, %rsp              ## imm = 0x290
	popq	%rbp
	retq
LBB33_33:
	movq	-512(%rbp), %rax
	movq	%rax, -616(%rbp)        ## 8-byte Spill
LBB33_34:                               ## %unwind_resume
	movq	-616(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table33:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset58 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset58
Lset59 = Ltmp185-Lfunc_begin4           ##   Call between Lfunc_begin4 and Ltmp185
	.long	Lset59
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset60 = Ltmp185-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset60
Lset61 = Ltmp188-Ltmp185                ##   Call between Ltmp185 and Ltmp188
	.long	Lset61
Lset62 = Ltmp189-Lfunc_begin4           ##     jumps to Ltmp189
	.long	Lset62
	.byte	0                       ##   On action: cleanup
Lset63 = Ltmp190-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset63
Lset64 = Ltmp195-Ltmp190                ##   Call between Ltmp190 and Ltmp195
	.long	Lset64
Lset65 = Ltmp196-Lfunc_begin4           ##     jumps to Ltmp196
	.long	Lset65
	.byte	0                       ##   On action: cleanup
Lset66 = Ltmp195-Lfunc_begin4           ## >> Call Site 4 <<
	.long	Lset66
Lset67 = Lfunc_end4-Ltmp195             ##   Call between Ltmp195 and Lfunc_end4
	.long	Lset67
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
	.weak_def_can_be_hidden	__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
	.align	4, 0x90
__ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_: ## @_ZNSt3__1rsIcNS_11char_traitsIcEEEERNS_13basic_istreamIT_T0_EES7_RS4_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp213:
	.cfi_def_cfa_offset 16
Ltmp214:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp215:
	.cfi_def_cfa_register %rbp
	subq	$160, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	-72(%rbp), %rsi
Ltmp200:
	leaq	-88(%rbp), %rdi
	xorl	%edx, %edx
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEE6sentryC1ERS3_b
Ltmp201:
	jmp	LBB34_1
LBB34_1:
	leaq	-88(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -105(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-105(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB34_3
	jmp	LBB34_19
LBB34_3:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -120(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	24(%rcx), %rdx
	cmpq	32(%rcx), %rdx
	movq	%rcx, -128(%rbp)        ## 8-byte Spill
	jne	LBB34_7
## BB#5:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	80(%rcx), %rcx
Ltmp202:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp203:
	movl	%eax, -132(%rbp)        ## 4-byte Spill
	jmp	LBB34_6
LBB34_6:                                ## %.noexc
	movl	-132(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -32(%rbp)
	jmp	LBB34_8
LBB34_7:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	24(%rax), %rcx
	movq	%rcx, %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movsbl	(%rcx), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -32(%rbp)
LBB34_8:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv.exit
	movl	-32(%rbp), %eax
	movl	%eax, -136(%rbp)        ## 4-byte Spill
## BB#9:
	movl	-136(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -104(%rbp)
	movl	-104(%rbp), %edi
	movl	%edi, -140(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-140(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB34_10
	jmp	LBB34_17
LBB34_10:
	movq	-72(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -24(%rbp)
	movl	$6, -28(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movl	$6, -12(%rbp)
	movq	-8(%rbp), %rax
	movl	32(%rax), %edx
	orl	$6, %edx
Ltmp204:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp205:
	jmp	LBB34_11
LBB34_11:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB34_12
LBB34_12:
	jmp	LBB34_18
LBB34_13:
Ltmp206:
	movl	%edx, %ecx
	movq	%rax, -96(%rbp)
	movl	%ecx, -100(%rbp)
## BB#14:
	movq	-96(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp207:
	movq	%rax, -152(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp208:
	jmp	LBB34_15
LBB34_15:
	callq	___cxa_end_catch
LBB34_16:
	movq	-72(%rbp), %rax
	addq	$160, %rsp
	popq	%rbp
	retq
LBB34_17:
	movl	-104(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-80(%rbp), %rcx
	movb	%al, (%rcx)
LBB34_18:
	jmp	LBB34_19
LBB34_19:
	jmp	LBB34_16
LBB34_20:
Ltmp209:
	movl	%edx, %ecx
	movq	%rax, -96(%rbp)
	movl	%ecx, -100(%rbp)
Ltmp210:
	callq	___cxa_end_catch
Ltmp211:
	jmp	LBB34_21
LBB34_21:
	jmp	LBB34_22
LBB34_22:
	movq	-96(%rbp), %rdi
	callq	__Unwind_Resume
LBB34_23:
Ltmp212:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -156(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table34:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset68 = Ltmp200-Lfunc_begin5           ## >> Call Site 1 <<
	.long	Lset68
Lset69 = Ltmp205-Ltmp200                ##   Call between Ltmp200 and Ltmp205
	.long	Lset69
Lset70 = Ltmp206-Lfunc_begin5           ##     jumps to Ltmp206
	.long	Lset70
	.byte	1                       ##   On action: 1
Lset71 = Ltmp205-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset71
Lset72 = Ltmp207-Ltmp205                ##   Call between Ltmp205 and Ltmp207
	.long	Lset72
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset73 = Ltmp207-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset73
Lset74 = Ltmp208-Ltmp207                ##   Call between Ltmp207 and Ltmp208
	.long	Lset74
Lset75 = Ltmp209-Lfunc_begin5           ##     jumps to Ltmp209
	.long	Lset75
	.byte	0                       ##   On action: cleanup
Lset76 = Ltmp208-Lfunc_begin5           ## >> Call Site 4 <<
	.long	Lset76
Lset77 = Ltmp210-Ltmp208                ##   Call between Ltmp208 and Ltmp210
	.long	Lset77
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset78 = Ltmp210-Lfunc_begin5           ## >> Call Site 5 <<
	.long	Lset78
Lset79 = Ltmp211-Ltmp210                ##   Call between Ltmp210 and Ltmp211
	.long	Lset79
Lset80 = Ltmp212-Lfunc_begin5           ##     jumps to Ltmp212
	.long	Lset80
	.byte	1                       ##   On action: 1
Lset81 = Ltmp211-Lfunc_begin5           ## >> Call Site 6 <<
	.long	Lset81
Lset82 = Lfunc_end5-Ltmp211             ##   Call between Ltmp211 and Lfunc_end5
	.long	Lset82
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp240:
	.cfi_def_cfa_offset 16
Ltmp241:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp242:
	.cfi_def_cfa_register %rbp
	subq	$656, %rsp              ## imm = 0x290
	movq	%rdi, -464(%rbp)
	movq	%rsi, -472(%rbp)
	movq	-464(%rbp), %rsi
Ltmp216:
	leaq	-480(%rbp), %rdi
	xorl	%edx, %edx
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEE6sentryC1ERS3_b
Ltmp217:
	jmp	LBB35_1
LBB35_1:
	leaq	-480(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	-456(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -538(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-538(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB35_3
	jmp	LBB35_49
LBB35_3:
	movq	-472(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -552(%rbp)        ## 8-byte Spill
	je	LBB35_5
## BB#4:
	leaq	-441(%rbp), %rsi
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	16(%rcx), %rdi
	movb	$0, -441(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -296(%rbp)
	movq	$0, -304(%rbp)
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %rsi
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rsi, 8(%rcx)
	jmp	LBB35_6
LBB35_5:
	leaq	-442(%rbp), %rsi
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	movq	-328(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rdi
	movb	$0, -442(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rsi
	shlq	$1, %rsi
	movb	%sil, %dl
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movb	%dl, (%rcx)
LBB35_6:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -560(%rbp)        ## 8-byte Spill
## BB#7:
	movq	-560(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -504(%rbp)
	cmpq	$0, -504(%rbp)
	jg	LBB35_10
## BB#8:
	movq	-472(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	$-1, -264(%rbp)
	movq	-264(%rbp), %rax
	subq	$16, %rax
	movq	%rax, -504(%rbp)
	jmp	LBB35_10
LBB35_9:
Ltmp233:
	movl	%edx, %ecx
	movq	%rax, -488(%rbp)
	movl	%ecx, -492(%rbp)
	jmp	LBB35_26
LBB35_10:
	cmpq	$0, -504(%rbp)
	jg	LBB35_12
## BB#11:
	movabsq	$9223372036854775807, %rax ## imm = 0x7FFFFFFFFFFFFFFF
	movq	%rax, -504(%rbp)
LBB35_12:
	movq	$0, -512(%rbp)
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
Ltmp220:
	leaq	-528(%rbp), %rdi
	movq	%rax, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp221:
	jmp	LBB35_13
LBB35_13:
	leaq	-528(%rbp), %rax
	movq	%rax, -112(%rbp)
Ltmp222:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp223:
	movq	%rax, -568(%rbp)        ## 8-byte Spill
	jmp	LBB35_14
LBB35_14:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit
	movq	-568(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -576(%rbp)        ## 8-byte Spill
## BB#15:
	leaq	-528(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-576(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -520(%rbp)
	movl	$0, -532(%rbp)
LBB35_16:                               ## =>This Inner Loop Header: Depth=1
	movq	-512(%rbp), %rax
	cmpq	-504(%rbp), %rax
	jge	LBB35_43
## BB#17:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -584(%rbp)        ## 8-byte Spill
## BB#18:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-584(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	24(%rcx), %rdx
	cmpq	32(%rcx), %rdx
	movq	%rcx, -592(%rbp)        ## 8-byte Spill
	jne	LBB35_21
## BB#19:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	72(%rcx), %rcx
Ltmp225:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp226:
	movl	%eax, -596(%rbp)        ## 4-byte Spill
	jmp	LBB35_20
LBB35_20:                               ## %.noexc2
                                        ##   in Loop: Header=BB35_16 Depth=1
	movl	-596(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -36(%rbp)
	jmp	LBB35_22
LBB35_21:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	24(%rax), %rcx
	movsbl	(%rcx), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -36(%rbp)
LBB35_22:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sgetcEv.exit
                                        ##   in Loop: Header=BB35_16 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, -600(%rbp)        ## 4-byte Spill
## BB#23:                               ##   in Loop: Header=BB35_16 Depth=1
	movl	-600(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -536(%rbp)
	movl	-536(%rbp), %edi
	movl	%edi, -604(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-604(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB35_24
	jmp	LBB35_29
LBB35_24:
	movl	-532(%rbp), %eax
	orl	$2, %eax
	movl	%eax, -532(%rbp)
	jmp	LBB35_43
LBB35_25:
Ltmp224:
	leaq	-528(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -488(%rbp)
	movl	%ecx, -492(%rbp)
	callq	__ZNSt3__16localeD1Ev
LBB35_26:
	movq	-488(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-464(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp234:
	movq	%rax, -616(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp235:
	jmp	LBB35_27
LBB35_27:
	callq	___cxa_end_catch
LBB35_28:
	movq	-464(%rbp), %rax
	addq	$656, %rsp              ## imm = 0x290
	popq	%rbp
	retq
LBB35_29:                               ##   in Loop: Header=BB35_16 Depth=1
	movl	-536(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movb	%al, -537(%rbp)
	movq	-520(%rbp), %rcx
	movb	-537(%rbp), %al
	movq	%rcx, -24(%rbp)
	movl	$16384, -28(%rbp)       ## imm = 0x4000
	movb	%al, -29(%rbp)
	movq	-24(%rbp), %rcx
	movsbl	-29(%rbp), %edi
	movq	%rcx, -624(%rbp)        ## 8-byte Spill
	callq	__Z7isasciii
	cmpl	$0, %eax
	je	LBB35_31
## BB#30:                               ##   in Loop: Header=BB35_16 Depth=1
	movsbl	-29(%rbp), %eax
	movslq	%eax, %rcx
	movq	-624(%rbp), %rdx        ## 8-byte Reload
	movq	16(%rdx), %rsi
	movl	(%rsi,%rcx,4), %eax
	andl	-28(%rbp), %eax
	cmpl	$0, %eax
	setne	%dil
	movb	%dil, -625(%rbp)        ## 1-byte Spill
	jmp	LBB35_32
LBB35_31:                               ##   in Loop: Header=BB35_16 Depth=1
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, -625(%rbp)         ## 1-byte Spill
	jmp	LBB35_32
LBB35_32:                               ## %_ZNKSt3__15ctypeIcE2isEjc.exit
                                        ##   in Loop: Header=BB35_16 Depth=1
	movb	-625(%rbp), %al         ## 1-byte Reload
	movb	%al, -626(%rbp)         ## 1-byte Spill
## BB#33:                               ##   in Loop: Header=BB35_16 Depth=1
	movb	-626(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB35_34
	jmp	LBB35_35
LBB35_34:
	jmp	LBB35_43
LBB35_35:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-472(%rbp), %rdi
	movsbl	-537(%rbp), %esi
Ltmp227:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp228:
	jmp	LBB35_36
LBB35_36:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-512(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -512(%rbp)
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -640(%rbp)        ## 8-byte Spill
## BB#37:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-640(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	24(%rcx), %rdx
	cmpq	32(%rcx), %rdx
	movq	%rcx, -648(%rbp)        ## 8-byte Spill
	jne	LBB35_40
## BB#38:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-648(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	80(%rcx), %rcx
Ltmp229:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp230:
	movl	%eax, -652(%rbp)        ## 4-byte Spill
	jmp	LBB35_39
LBB35_39:                               ## %.noexc
                                        ##   in Loop: Header=BB35_16 Depth=1
	movl	-652(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -52(%rbp)
	jmp	LBB35_41
LBB35_40:                               ##   in Loop: Header=BB35_16 Depth=1
	movq	-648(%rbp), %rax        ## 8-byte Reload
	movq	24(%rax), %rcx
	movq	%rcx, %rdx
	addq	$1, %rdx
	movq	%rdx, 24(%rax)
	movsbl	(%rcx), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -52(%rbp)
LBB35_41:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv.exit
                                        ##   in Loop: Header=BB35_16 Depth=1
## BB#42:                               ##   in Loop: Header=BB35_16 Depth=1
	jmp	LBB35_16
LBB35_43:
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -72(%rbp)
	movq	$0, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, 24(%rax)
## BB#44:
	cmpq	$0, -512(%rbp)
	jne	LBB35_46
## BB#45:
	movl	-532(%rbp), %eax
	orl	$4, %eax
	movl	%eax, -532(%rbp)
LBB35_46:
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movl	-532(%rbp), %edx
	movq	%rax, -136(%rbp)
	movl	%edx, -140(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movl	%edx, -124(%rbp)
	movq	-120(%rbp), %rax
	movl	32(%rax), %esi
	orl	%edx, %esi
Ltmp231:
	movq	%rax, %rdi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp232:
	jmp	LBB35_47
LBB35_47:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit1
	jmp	LBB35_48
LBB35_48:
	jmp	LBB35_52
LBB35_49:
	movq	-464(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -168(%rbp)
	movl	$4, -172(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -152(%rbp)
	movl	$4, -156(%rbp)
	movq	-152(%rbp), %rax
	movl	32(%rax), %edx
	orl	$4, %edx
Ltmp218:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp219:
	jmp	LBB35_50
LBB35_50:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB35_51
LBB35_51:
	jmp	LBB35_52
LBB35_52:
	jmp	LBB35_28
LBB35_53:
Ltmp236:
	movl	%edx, %ecx
	movq	%rax, -488(%rbp)
	movl	%ecx, -492(%rbp)
Ltmp237:
	callq	___cxa_end_catch
Ltmp238:
	jmp	LBB35_54
LBB35_54:
	jmp	LBB35_55
LBB35_55:
	movq	-488(%rbp), %rdi
	callq	__Unwind_Resume
LBB35_56:
Ltmp239:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -656(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table35:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset83 = Ltmp216-Lfunc_begin6           ## >> Call Site 1 <<
	.long	Lset83
Lset84 = Ltmp221-Ltmp216                ##   Call between Ltmp216 and Ltmp221
	.long	Lset84
Lset85 = Ltmp233-Lfunc_begin6           ##     jumps to Ltmp233
	.long	Lset85
	.byte	1                       ##   On action: 1
Lset86 = Ltmp222-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset86
Lset87 = Ltmp223-Ltmp222                ##   Call between Ltmp222 and Ltmp223
	.long	Lset87
Lset88 = Ltmp224-Lfunc_begin6           ##     jumps to Ltmp224
	.long	Lset88
	.byte	1                       ##   On action: 1
Lset89 = Ltmp225-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset89
Lset90 = Ltmp226-Ltmp225                ##   Call between Ltmp225 and Ltmp226
	.long	Lset90
Lset91 = Ltmp233-Lfunc_begin6           ##     jumps to Ltmp233
	.long	Lset91
	.byte	1                       ##   On action: 1
Lset92 = Ltmp226-Lfunc_begin6           ## >> Call Site 4 <<
	.long	Lset92
Lset93 = Ltmp234-Ltmp226                ##   Call between Ltmp226 and Ltmp234
	.long	Lset93
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset94 = Ltmp234-Lfunc_begin6           ## >> Call Site 5 <<
	.long	Lset94
Lset95 = Ltmp235-Ltmp234                ##   Call between Ltmp234 and Ltmp235
	.long	Lset95
Lset96 = Ltmp236-Lfunc_begin6           ##     jumps to Ltmp236
	.long	Lset96
	.byte	0                       ##   On action: cleanup
Lset97 = Ltmp235-Lfunc_begin6           ## >> Call Site 6 <<
	.long	Lset97
Lset98 = Ltmp227-Ltmp235                ##   Call between Ltmp235 and Ltmp227
	.long	Lset98
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset99 = Ltmp227-Lfunc_begin6           ## >> Call Site 7 <<
	.long	Lset99
Lset100 = Ltmp219-Ltmp227               ##   Call between Ltmp227 and Ltmp219
	.long	Lset100
Lset101 = Ltmp233-Lfunc_begin6          ##     jumps to Ltmp233
	.long	Lset101
	.byte	1                       ##   On action: 1
Lset102 = Ltmp237-Lfunc_begin6          ## >> Call Site 8 <<
	.long	Lset102
Lset103 = Ltmp238-Ltmp237               ##   Call between Ltmp237 and Ltmp238
	.long	Lset103
Lset104 = Ltmp239-Lfunc_begin6          ##     jumps to Ltmp239
	.long	Lset104
	.byte	1                       ##   On action: 1
Lset105 = Ltmp238-Lfunc_begin6          ## >> Call Site 9 <<
	.long	Lset105
Lset106 = Lfunc_end6-Ltmp238            ##   Call between Ltmp238 and Lfunc_end6
	.long	Lset106
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6assignERcRKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6assignERcRKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6assignERcRKc: ## @_ZNSt3__111char_traitsIcE6assignERcRKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp243:
	.cfi_def_cfa_offset 16
Ltmp244:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp245:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movb	(%rsi), %al
	movq	-8(%rbp), %rsi
	movb	%al, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z7isasciii
	.weak_def_can_be_hidden	__Z7isasciii
	.align	4, 0x90
__Z7isasciii:                           ## @_Z7isasciii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp246:
	.cfi_def_cfa_offset 16
Ltmp247:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp248:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	andl	$-128, %edi
	cmpl	$0, %edi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp270:
	.cfi_def_cfa_offset 16
Ltmp271:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp272:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp249:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp250:
	jmp	LBB38_1
LBB38_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB38_3
	jmp	LBB38_26
LBB38_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB38_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB38_7
LBB38_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB38_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB38_8
	jmp	LBB38_13
LBB38_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp252:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp253:
	jmp	LBB38_9
LBB38_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp254:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp255:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB38_10
LBB38_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp256:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp257:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB38_12
LBB38_11:
Ltmp258:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB38_21
LBB38_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB38_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp259:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp260:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB38_15
LBB38_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB38_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp261:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp262:
	jmp	LBB38_17
LBB38_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB38_18
LBB38_18:
	jmp	LBB38_25
LBB38_19:
Ltmp251:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB38_22
LBB38_20:
Ltmp263:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB38_21
LBB38_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB38_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp264:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp265:
	jmp	LBB38_23
LBB38_23:
	callq	___cxa_end_catch
LBB38_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB38_25:
	jmp	LBB38_26
LBB38_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB38_24
LBB38_27:
Ltmp266:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp267:
	callq	___cxa_end_catch
Ltmp268:
	jmp	LBB38_28
LBB38_28:
	jmp	LBB38_29
LBB38_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB38_30:
Ltmp269:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table38:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset107 = Ltmp249-Lfunc_begin7          ## >> Call Site 1 <<
	.long	Lset107
Lset108 = Ltmp250-Ltmp249               ##   Call between Ltmp249 and Ltmp250
	.long	Lset108
Lset109 = Ltmp251-Lfunc_begin7          ##     jumps to Ltmp251
	.long	Lset109
	.byte	5                       ##   On action: 3
Lset110 = Ltmp252-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset110
Lset111 = Ltmp253-Ltmp252               ##   Call between Ltmp252 and Ltmp253
	.long	Lset111
Lset112 = Ltmp263-Lfunc_begin7          ##     jumps to Ltmp263
	.long	Lset112
	.byte	5                       ##   On action: 3
Lset113 = Ltmp254-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset113
Lset114 = Ltmp257-Ltmp254               ##   Call between Ltmp254 and Ltmp257
	.long	Lset114
Lset115 = Ltmp258-Lfunc_begin7          ##     jumps to Ltmp258
	.long	Lset115
	.byte	3                       ##   On action: 2
Lset116 = Ltmp259-Lfunc_begin7          ## >> Call Site 4 <<
	.long	Lset116
Lset117 = Ltmp262-Ltmp259               ##   Call between Ltmp259 and Ltmp262
	.long	Lset117
Lset118 = Ltmp263-Lfunc_begin7          ##     jumps to Ltmp263
	.long	Lset118
	.byte	5                       ##   On action: 3
Lset119 = Ltmp262-Lfunc_begin7          ## >> Call Site 5 <<
	.long	Lset119
Lset120 = Ltmp264-Ltmp262               ##   Call between Ltmp262 and Ltmp264
	.long	Lset120
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset121 = Ltmp264-Lfunc_begin7          ## >> Call Site 6 <<
	.long	Lset121
Lset122 = Ltmp265-Ltmp264               ##   Call between Ltmp264 and Ltmp265
	.long	Lset122
Lset123 = Ltmp266-Lfunc_begin7          ##     jumps to Ltmp266
	.long	Lset123
	.byte	0                       ##   On action: cleanup
Lset124 = Ltmp265-Lfunc_begin7          ## >> Call Site 7 <<
	.long	Lset124
Lset125 = Ltmp267-Ltmp265               ##   Call between Ltmp265 and Ltmp267
	.long	Lset125
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset126 = Ltmp267-Lfunc_begin7          ## >> Call Site 8 <<
	.long	Lset126
Lset127 = Ltmp268-Ltmp267               ##   Call between Ltmp267 and Ltmp268
	.long	Lset127
Lset128 = Ltmp269-Lfunc_begin7          ##     jumps to Ltmp269
	.long	Lset128
	.byte	5                       ##   On action: 3
Lset129 = Ltmp268-Lfunc_begin7          ## >> Call Site 9 <<
	.long	Lset129
Lset130 = Lfunc_end7-Ltmp268            ##   Call between Ltmp268 and Lfunc_end7
	.long	Lset130
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp276:
	.cfi_def_cfa_offset 16
Ltmp277:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp278:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB39_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB39_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB39_5
LBB39_4:
	movq	$0, -368(%rbp)
LBB39_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB39_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB39_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_8:
	jmp	LBB39_9
LBB39_9:
	cmpq	$0, -368(%rbp)
	jle	LBB39_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB39_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB39_13
LBB39_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB39_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp273:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp274:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB39_14
LBB39_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB39_15
LBB39_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB39_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB39_19
LBB39_17:
Ltmp275:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB39_27
LBB39_18:
	movl	$0, -416(%rbp)
LBB39_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB39_20
	jmp	LBB39_29
LBB39_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB39_26
	jmp	LBB39_28
LBB39_20:
	jmp	LBB39_21
LBB39_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB39_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB39_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_24:
	jmp	LBB39_25
LBB39_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB39_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB39_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB39_28:
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table39:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset131 = Lfunc_begin8-Lfunc_begin8     ## >> Call Site 1 <<
	.long	Lset131
Lset132 = Ltmp273-Lfunc_begin8          ##   Call between Lfunc_begin8 and Ltmp273
	.long	Lset132
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset133 = Ltmp273-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset133
Lset134 = Ltmp274-Ltmp273               ##   Call between Ltmp273 and Ltmp274
	.long	Lset134
Lset135 = Ltmp275-Lfunc_begin8          ##     jumps to Ltmp275
	.long	Lset135
	.byte	0                       ##   On action: cleanup
Lset136 = Ltmp274-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset136
Lset137 = Lfunc_end8-Ltmp274            ##   Call between Ltmp274 and Lfunc_end8
	.long	Lset137
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp282:
	.cfi_def_cfa_offset 16
Ltmp283:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp284:
	.cfi_def_cfa_register %rbp
	subq	$1312, %rsp             ## imm = 0x520
	movq	%rdi, -1064(%rbp)
	movq	%rsi, -1072(%rbp)
	movq	-1064(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$64, %rdi
	movq	-1072(%rbp), %rax
	movq	%rsi, -1088(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	-1088(%rbp), %rsi       ## 8-byte Reload
	movq	$0, 88(%rsi)
	movl	96(%rsi), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	je	LBB41_14
## BB#1:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rax
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	movq	%rcx, -1024(%rbp)
	movq	-1024(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1104(%rbp)       ## 8-byte Spill
	je	LBB41_3
## BB#2:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rcx
	movq	%rcx, -968(%rbp)
	movq	-968(%rbp), %rcx
	movq	%rcx, -960(%rbp)
	movq	-960(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
	jmp	LBB41_4
LBB41_3:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1016(%rbp)
	movq	-1016(%rbp), %rcx
	movq	%rcx, -1008(%rbp)
	movq	-1008(%rbp), %rcx
	movq	%rcx, -1000(%rbp)
	movq	-1000(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -992(%rbp)
	movq	-992(%rbp), %rcx
	movq	%rcx, -984(%rbp)
	movq	-984(%rbp), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
LBB41_4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-1112(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -584(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rdx, -560(%rbp)
	movq	-560(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1120(%rbp)       ## 8-byte Spill
	movq	%rcx, -1128(%rbp)       ## 8-byte Spill
	je	LBB41_6
## BB#5:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	movq	%rcx, -512(%rbp)
	movq	-512(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
	jmp	LBB41_7
LBB41_6:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -552(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	%rcx, -536(%rbp)
	movq	-536(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
LBB41_7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit3
	movq	-1136(%rbp), %rax       ## 8-byte Reload
	movq	-1120(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1144(%rbp)       ## 8-byte Spill
	movq	%rcx, -1152(%rbp)       ## 8-byte Spill
	je	LBB41_9
## BB#8:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
	jmp	LBB41_10
LBB41_9:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
LBB41_10:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit7
	movq	-1160(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1168(%rbp)       ## 8-byte Spill
	movq	%rcx, -1176(%rbp)       ## 8-byte Spill
	je	LBB41_12
## BB#11:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
	jmp	LBB41_13
LBB41_12:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
LBB41_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	-1184(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	88(%rcx), %rdx
	movq	-1144(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -232(%rbp)
	movq	-1168(%rbp), %rdi       ## 8-byte Reload
	movq	%rdi, -240(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-256(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB41_14:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB41_36
## BB#15:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1192(%rbp)       ## 8-byte Spill
	je	LBB41_17
## BB#16:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
	jmp	LBB41_18
LBB41_17:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
LBB41_18:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit5
	movq	-1200(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1080(%rbp)
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1208(%rbp)       ## 8-byte Spill
	je	LBB41_20
## BB#19:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
	jmp	LBB41_21
LBB41_20:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
LBB41_21:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4
	movq	-1216(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rax
	addq	-1080(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	%rax, 88(%rcx)
	addq	$64, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	-496(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -1224(%rbp)       ## 8-byte Spill
	movq	%rax, -1232(%rbp)       ## 8-byte Spill
	je	LBB41_23
## BB#22:
	movq	-1232(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB41_24
LBB41_23:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB41_24
LBB41_24:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-1240(%rbp), %rax       ## 8-byte Reload
	xorl	%edx, %edx
	subq	$1, %rax
	movq	-1224(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movq	-592(%rbp), %rdi
	movq	-600(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -712(%rbp)
	movq	-712(%rbp), %rcx
	movq	%rcx, -704(%rbp)
	movq	-704(%rbp), %rcx
	movq	%rcx, -696(%rbp)
	movq	-696(%rbp), %rsi
	movq	%rsi, -688(%rbp)
	movq	-688(%rbp), %rsi
	movq	%rsi, -680(%rbp)
	movq	-680(%rbp), %rsi
	movzbl	(%rsi), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1248(%rbp)       ## 8-byte Spill
	movq	%rcx, -1256(%rbp)       ## 8-byte Spill
	je	LBB41_26
## BB#25:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -632(%rbp)
	movq	-632(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-624(%rbp), %rcx
	movq	%rcx, -616(%rbp)
	movq	-616(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
	jmp	LBB41_27
LBB41_26:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -672(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	-664(%rbp), %rcx
	movq	%rcx, -656(%rbp)
	movq	-656(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -648(%rbp)
	movq	-648(%rbp), %rcx
	movq	%rcx, -640(%rbp)
	movq	-640(%rbp), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
LBB41_27:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-1264(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -824(%rbp)
	movq	-824(%rbp), %rcx
	movq	%rcx, -816(%rbp)
	movq	-816(%rbp), %rcx
	movq	%rcx, -808(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	-800(%rbp), %rdx
	movq	%rdx, -792(%rbp)
	movq	-792(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1272(%rbp)       ## 8-byte Spill
	movq	%rcx, -1280(%rbp)       ## 8-byte Spill
	je	LBB41_29
## BB#28:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -744(%rbp)
	movq	-744(%rbp), %rcx
	movq	%rcx, -736(%rbp)
	movq	-736(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	-728(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
	jmp	LBB41_30
LBB41_29:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -776(%rbp)
	movq	-776(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	-768(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -760(%rbp)
	movq	-760(%rbp), %rcx
	movq	%rcx, -752(%rbp)
	movq	-752(%rbp), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
LBB41_30:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-1288(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -720(%rbp)
	movq	-720(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -904(%rbp)
	movq	-904(%rbp), %rcx
	movq	%rcx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1296(%rbp)       ## 8-byte Spill
	movq	%rcx, -1304(%rbp)       ## 8-byte Spill
	je	LBB41_32
## BB#31:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movq	-848(%rbp), %rcx
	movq	%rcx, -840(%rbp)
	movq	-840(%rbp), %rcx
	movq	%rcx, -832(%rbp)
	movq	-832(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
	jmp	LBB41_33
LBB41_32:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -872(%rbp)
	movq	-872(%rbp), %rcx
	movq	%rcx, -864(%rbp)
	movq	-864(%rbp), %rcx
	movq	%rcx, -856(%rbp)
	movq	-856(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
LBB41_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-1312(%rbp), %rax       ## 8-byte Reload
	movq	-1296(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1248(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -912(%rbp)
	movq	-1272(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -920(%rbp)
	movq	%rcx, -928(%rbp)
	movq	-912(%rbp), %rcx
	movq	-920(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-928(%rbp), %rsi
	movq	%rsi, 56(%rcx)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movl	96(%rcx), %edi
	andl	$3, %edi
	cmpl	$0, %edi
	je	LBB41_35
## BB#34:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1080(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -936(%rbp)
	movl	%edx, -940(%rbp)
	movq	-936(%rbp), %rax
	movl	-940(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
LBB41_35:
	jmp	LBB41_36
LBB41_36:
	addq	$1312, %rsp             ## imm = 0x520
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	" : "

L_.str.1:                               ## @.str.1
	.asciz	"\"we have an a but no b\""

L_.str.2:                               ## @.str.2
	.asciz	"just a"

L_.str.3:                               ## @.str.3
	.asciz	"\"the cat sat on\" \"the splendid mat\""

L_.str.4:                               ## @.str.4
	.asciz	"both a and b"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	120
	.quad	0
	.quad	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-120
	.quad	-120
	.quad	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE ## @_ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE:
	.quad	120
	.quad	0
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev
	.quad	-120
	.quad	-120
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE


.subsections_via_symbols
