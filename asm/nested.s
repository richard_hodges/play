	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z15print_exceptionRKSt9exceptionm
	.align	4, 0x90
__Z15print_exceptionRKSt9exceptionm:    ## @_Z15print_exceptionRKSt9exceptionm
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp16:
	.cfi_def_cfa_offset 16
Ltmp17:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp18:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	movq	__ZNSt3__14cerrE@GOTPCREL(%rip), %rdi
	leaq	L_.str(%rip), %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-120(%rbp), %rsi
	leaq	-144(%rbp), %rdi
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movb	$32, -97(%rbp)
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rcx
	movq	%rsi, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movb	$32, -73(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rcx, -32(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-72(%rbp), %rsi
	movsbl	-73(%rbp), %edx
	movq	%rdi, -176(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -184(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
Ltmp0:
	movq	-184(%rbp), %rdi        ## 8-byte Reload
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp1:
	movq	%rax, -192(%rbp)        ## 8-byte Spill
	jmp	LBB0_1
LBB0_1:
	movq	-112(%rbp), %rax
	movq	(%rax), %rcx
	movq	16(%rcx), %rcx
	movq	%rax, %rdi
	callq	*%rcx
Ltmp2:
	movq	-192(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp3:
	movq	%rax, -200(%rbp)        ## 8-byte Spill
	jmp	LBB0_2
LBB0_2:
Ltmp4:
	movl	$10, %esi
	movq	-200(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp5:
	movq	%rax, -208(%rbp)        ## 8-byte Spill
	jmp	LBB0_3
LBB0_3:
	leaq	-144(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-112(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	$0, -16(%rbp)
	movq	-8(%rbp), %rdi
	cmpq	$0, %rdi
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	je	LBB0_5
## BB#4:
	movq	__ZTISt9exception@GOTPCREL(%rip), %rax
	movq	__ZTISt16nested_exception@GOTPCREL(%rip), %rcx
	movq	$-2, %rdx
	movq	-216(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, %rdi
	movq	%rax, %rsi
	movq	%rdx, -224(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-224(%rbp), %rcx        ## 8-byte Reload
	callq	___dynamic_cast
	movq	%rax, -232(%rbp)        ## 8-byte Spill
	jmp	LBB0_6
LBB0_5:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB0_6
LBB0_6:
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	cmpq	$0, -24(%rbp)
	je	LBB0_9
## BB#7:
	movq	-24(%rbp), %rdi
Ltmp7:
	callq	__ZNKSt16nested_exception14rethrow_nestedEv
Ltmp8:
	jmp	LBB0_8
LBB0_8:                                 ## %.noexc
LBB0_9:                                 ## %_ZSt17rethrow_if_nestedISt9exceptionEvRKT_PNSt3__19enable_ifIXsr14is_polymorphicIS1_EE5valueEvE4typeE.exit
	jmp	LBB0_10
LBB0_10:
	jmp	LBB0_16
LBB0_11:
Ltmp6:
	leaq	-144(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -152(%rbp)
	movl	%ecx, -156(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB0_19
LBB0_12:
Ltmp9:
	movl	%edx, %ecx
	movq	%rax, -152(%rbp)
	movl	%ecx, -156(%rbp)
## BB#13:
	movl	-156(%rbp), %eax
	movl	$1, %ecx
	cmpl	%ecx, %eax
	jne	LBB0_19
## BB#14:
	movq	-152(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -168(%rbp)
	movq	-120(%rbp), %rdi
	incq	%rdi
Ltmp10:
	movq	%rdi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	callq	__Z15print_exceptionRKSt9exceptionm
Ltmp11:
	jmp	LBB0_15
LBB0_15:
	callq	___cxa_end_catch
LBB0_16:
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
LBB0_17:
Ltmp12:
	movl	%edx, %ecx
	movq	%rax, -152(%rbp)
	movl	%ecx, -156(%rbp)
Ltmp13:
	callq	___cxa_end_catch
Ltmp14:
	jmp	LBB0_18
LBB0_18:
	jmp	LBB0_19
LBB0_19:
	movq	-152(%rbp), %rdi
	callq	__Unwind_Resume
LBB0_20:
Ltmp15:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -244(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\221\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\202\001"              ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp6-Lfunc_begin0              ##     jumps to Ltmp6
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp1-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp2-Ltmp1                     ##   Call between Ltmp1 and Ltmp2
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset7 = Ltmp2-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset7
Lset8 = Ltmp5-Ltmp2                     ##   Call between Ltmp2 and Ltmp5
	.long	Lset8
Lset9 = Ltmp6-Lfunc_begin0              ##     jumps to Ltmp6
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp7-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset10
Lset11 = Ltmp8-Ltmp7                    ##   Call between Ltmp7 and Ltmp8
	.long	Lset11
Lset12 = Ltmp9-Lfunc_begin0             ##     jumps to Ltmp9
	.long	Lset12
	.byte	1                       ##   On action: 1
Lset13 = Ltmp8-Lfunc_begin0             ## >> Call Site 6 <<
	.long	Lset13
Lset14 = Ltmp10-Ltmp8                   ##   Call between Ltmp8 and Ltmp10
	.long	Lset14
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp10-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset15
Lset16 = Ltmp11-Ltmp10                  ##   Call between Ltmp10 and Ltmp11
	.long	Lset16
Lset17 = Ltmp12-Lfunc_begin0            ##     jumps to Ltmp12
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp11-Lfunc_begin0            ## >> Call Site 8 <<
	.long	Lset18
Lset19 = Ltmp13-Ltmp11                  ##   Call between Ltmp11 and Ltmp13
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp13-Lfunc_begin0            ## >> Call Site 9 <<
	.long	Lset20
Lset21 = Ltmp14-Ltmp13                  ##   Call between Ltmp13 and Ltmp14
	.long	Lset21
Lset22 = Ltmp15-Lfunc_begin0            ##     jumps to Ltmp15
	.long	Lset22
	.byte	3                       ##   On action: 2
Lset23 = Ltmp14-Lfunc_begin0            ## >> Call Site 10 <<
	.long	Lset23
Lset24 = Lfunc_end0-Ltmp14              ##   Call between Ltmp14 and Lfunc_end0
	.long	Lset24
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
	.byte	2                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 2
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 2
	.long	__ZTISt9exception@GOTPCREL+4 ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp19:
	.cfi_def_cfa_offset 16
Ltmp20:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp21:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movb	%sil, %al
	leaq	-9(%rbp), %rsi
	movl	$1, %ecx
	movl	%ecx, %edx
	movq	%rdi, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp22:
	.cfi_def_cfa_offset 16
Ltmp23:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp24:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp25:
	.cfi_def_cfa_offset 16
Ltmp26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp27:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB3_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB3_3
LBB3_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB3_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB3_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB3_6
LBB3_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB3_6:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z12really_innerm
	.align	4, 0x90
__Z12really_innerm:                     ## @_Z12really_innerm
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp40:
	.cfi_def_cfa_offset 16
Ltmp41:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp42:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movq	%rdi, -40(%rbp)
	cmpq	$6, -40(%rbp)
	jbe	LBB5_9
## BB#1:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -24(%rbp)
	leaq	L_.str.1(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	-8(%rbp), %rcx
Ltmp28:
	movq	%rdi, -64(%rbp)         ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -72(%rbp)         ## 8-byte Spill
	callq	__ZNSt11logic_errorC2EPKc
Ltmp29:
	jmp	LBB5_2
LBB5_2:                                 ## %_ZNSt16invalid_argumentC1EPKc.exit
	movq	__ZTVSt16invalid_argument@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-72(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, (%rcx)
## BB#3:
Ltmp31:
	movq	__ZTISt16invalid_argument@GOTPCREL(%rip), %rsi
	movq	__ZNSt16invalid_argumentD1Ev@GOTPCREL(%rip), %rdx
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	callq	___cxa_throw
Ltmp32:
	jmp	LBB5_14
LBB5_4:
Ltmp30:
	movl	%edx, %ecx
	movq	%rax, -48(%rbp)
	movl	%ecx, -52(%rbp)
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	callq	___cxa_free_exception
	jmp	LBB5_6
LBB5_5:
Ltmp33:
	movl	%edx, %ecx
	movq	%rax, -48(%rbp)
	movl	%ecx, -52(%rbp)
LBB5_6:
	movq	-48(%rbp), %rdi
	callq	___cxa_begin_catch
Ltmp34:
	leaq	L___func__._Z12really_innerm(%rip), %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__Z7rethrowIRA13_KcJEEvOT_DpOT0_
Ltmp35:
	jmp	LBB5_7
LBB5_7:
	callq	___cxa_end_catch
LBB5_8:
	addq	$96, %rsp
	popq	%rbp
	retq
LBB5_9:
	jmp	LBB5_8
LBB5_10:
Ltmp36:
	movl	%edx, %ecx
	movq	%rax, -48(%rbp)
	movl	%ecx, -52(%rbp)
Ltmp37:
	callq	___cxa_end_catch
Ltmp38:
	jmp	LBB5_11
LBB5_11:
	jmp	LBB5_12
LBB5_12:
	movq	-48(%rbp), %rdi
	callq	__Unwind_Resume
LBB5_13:
Ltmp39:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -84(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
LBB5_14:
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\360"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	104                     ## Call site table length
Lset25 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset25
Lset26 = Ltmp28-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp28
	.long	Lset26
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset27 = Ltmp28-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset27
Lset28 = Ltmp29-Ltmp28                  ##   Call between Ltmp28 and Ltmp29
	.long	Lset28
Lset29 = Ltmp30-Lfunc_begin1            ##     jumps to Ltmp30
	.long	Lset29
	.byte	1                       ##   On action: 1
Lset30 = Ltmp31-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset30
Lset31 = Ltmp32-Ltmp31                  ##   Call between Ltmp31 and Ltmp32
	.long	Lset31
Lset32 = Ltmp33-Lfunc_begin1            ##     jumps to Ltmp33
	.long	Lset32
	.byte	1                       ##   On action: 1
Lset33 = Ltmp32-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset33
Lset34 = Ltmp34-Ltmp32                  ##   Call between Ltmp32 and Ltmp34
	.long	Lset34
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset35 = Ltmp34-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset35
Lset36 = Ltmp35-Ltmp34                  ##   Call between Ltmp34 and Ltmp35
	.long	Lset36
Lset37 = Ltmp36-Lfunc_begin1            ##     jumps to Ltmp36
	.long	Lset37
	.byte	0                       ##   On action: cleanup
Lset38 = Ltmp35-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset38
Lset39 = Ltmp37-Ltmp35                  ##   Call between Ltmp35 and Ltmp37
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp37-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset40
Lset41 = Ltmp38-Ltmp37                  ##   Call between Ltmp37 and Ltmp38
	.long	Lset41
Lset42 = Ltmp39-Lfunc_begin1            ##     jumps to Ltmp39
	.long	Lset42
	.byte	1                       ##   On action: 1
Lset43 = Ltmp38-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset43
Lset44 = Lfunc_end1-Ltmp38              ##   Call between Ltmp38 and Lfunc_end1
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z7rethrowIRA13_KcJEEvOT_DpOT0_
	.weak_def_can_be_hidden	__Z7rethrowIRA13_KcJEEvOT_DpOT0_
	.align	4, 0x90
__Z7rethrowIRA13_KcJEEvOT_DpOT0_:       ## @_Z7rethrowIRA13_KcJEEvOT_DpOT0_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp92:
	.cfi_def_cfa_offset 16
Ltmp93:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp94:
	.cfi_def_cfa_register %rbp
	subq	$960, %rsp              ## imm = 0x3C0
	movq	%rdi, -400(%rbp)
	leaq	-664(%rbp), %rdi
	movq	%rdi, -368(%rbp)
	movl	$16, -372(%rbp)
	movq	-368(%rbp), %rdi
	movq	%rdi, %rax
	addq	$112, %rax
	movq	%rax, -360(%rbp)
	movq	%rax, -352(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 112(%rdi)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 112(%rdi)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	movq	%rax, %rcx
	addq	$24, %rcx
	movq	%rcx, (%rdi)
	addq	$64, %rax
	movq	%rax, 112(%rdi)
	movq	%rdi, %rax
	addq	$8, %rax
	movq	%rdi, -80(%rbp)
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$8, %rcx
	movq	%rcx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rcx
	movq	-24(%rdx), %rdx
	movq	%rcx, (%rax,%rdx)
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rax
Ltmp43:
	movq	%rdi, -848(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp44:
	jmp	LBB6_1
LBB6_1:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-856(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-848(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 112(%rdi)
	addq	$8, %rdi
	movl	-372(%rbp), %eax
	orl	$16, %eax
	movq	%rdi, -336(%rbp)
	movl	%eax, -340(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movl	%eax, -284(%rbp)
	movq	-280(%rbp), %rdx
Ltmp46:
	movq	%rdx, %rdi
	movq	%rdx, -864(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp47:
	jmp	LBB6_2
LBB6_2:                                 ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-864(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -240(%rbp)
	movq	-240(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -232(%rbp)
	movq	%rdi, -872(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -880(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-880(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movl	$0, -220(%rbp)
LBB6_3:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -220(%rbp)
	jae	LBB6_5
## BB#4:                                ##   in Loop: Header=BB6_3 Depth=1
	movl	-220(%rbp), %eax
	movl	%eax, %ecx
	movq	-216(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-220(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -220(%rbp)
	jmp	LBB6_3
LBB6_5:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-312(%rbp), %rcx
	movq	-864(%rbp), %rdi        ## 8-byte Reload
	movq	$0, 88(%rdi)
	movl	-284(%rbp), %eax
	movl	%eax, 96(%rdi)
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %r8
	movq	%r8, -160(%rbp)
	movq	-160(%rbp), %r8
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -144(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movl	$0, -132(%rbp)
LBB6_6:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -132(%rbp)
	jae	LBB6_8
## BB#7:                                ##   in Loop: Header=BB6_6 Depth=1
	movl	-132(%rbp), %eax
	movl	%eax, %ecx
	movq	-128(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-132(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -132(%rbp)
	jmp	LBB6_6
LBB6_8:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3.i.i.i
Ltmp49:
	leaq	-312(%rbp), %rsi
	movq	-864(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp50:
	jmp	LBB6_14
LBB6_9:
Ltmp51:
	movl	%edx, %ecx
	movq	%rax, -320(%rbp)
	movl	%ecx, -324(%rbp)
	leaq	-312(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-872(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-864(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-320(%rbp), %rax
	movl	-324(%rbp), %ecx
	movq	%rax, -896(%rbp)        ## 8-byte Spill
	movl	%ecx, -900(%rbp)        ## 4-byte Spill
	jmp	LBB6_12
LBB6_10:
Ltmp45:
	movl	%edx, %ecx
	movq	%rax, -384(%rbp)
	movl	%ecx, -388(%rbp)
	jmp	LBB6_13
LBB6_11:
Ltmp48:
	movl	%edx, %ecx
	movq	%rax, -896(%rbp)        ## 8-byte Spill
	movl	%ecx, -900(%rbp)        ## 4-byte Spill
	jmp	LBB6_12
LBB6_12:                                ## %.body.i
	movl	-900(%rbp), %eax        ## 4-byte Reload
	movq	-896(%rbp), %rcx        ## 8-byte Reload
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -384(%rbp)
	movl	%eax, -388(%rbp)
	movq	-848(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB6_13:
	movq	-848(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-384(%rbp), %rax
	movq	%rax, -912(%rbp)        ## 8-byte Spill
	jmp	LBB6_58
LBB6_14:                                ## %_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej.exit
	leaq	-312(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-400(%rbp), %rsi
Ltmp52:
	leaq	-664(%rbp), %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp53:
	movq	%rax, -920(%rbp)        ## 8-byte Spill
	jmp	LBB6_15
LBB6_15:
	leaq	L_.str.4(%rip), %rax
	movq	%rax, -688(%rbp)
	movl	$0, -692(%rbp)
	leaq	-704(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -928(%rbp)        ## 8-byte Spill
	callq	__ZSt17current_exceptionv
Ltmp55:
	movq	-928(%rbp), %rdi        ## 8-byte Reload
	callq	__ZSt17rethrow_exceptionSt13exception_ptr
Ltmp56:
	jmp	LBB6_16
LBB6_16:
LBB6_17:
Ltmp54:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_55
LBB6_18:
Ltmp57:
	leaq	-704(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	callq	__ZNSt13exception_ptrD1Ev
## BB#19:
	movl	-676(%rbp), %eax
	movl	$3, %ecx
	cmpl	%ecx, %eax
	movl	%eax, -932(%rbp)        ## 4-byte Spill
	jne	LBB6_26
## BB#20:
	movq	-672(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -800(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, -56(%rbp)
	leaq	-656(%rbp), %rsi
Ltmp80:
	leaq	-840(%rbp), %rdi
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp81:
	jmp	LBB6_21
LBB6_21:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit
	jmp	LBB6_22
LBB6_22:
	leaq	-816(%rbp), %rax
	movq	%rax, -40(%rbp)
	leaq	-840(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	%rax, -32(%rbp)
	movq	-24(%rbp), %rcx
Ltmp83:
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -944(%rbp)        ## 8-byte Spill
	callq	__ZNSt11logic_errorC2ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp84:
	jmp	LBB6_23
LBB6_23:                                ## %_ZNSt16invalid_argumentC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE.exit
	movq	__ZTVSt16invalid_argument@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-944(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
## BB#24:
Ltmp86:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-816(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp87:
	jmp	LBB6_25
LBB6_25:
LBB6_26:
	movl	$2, %eax
	movl	-932(%rbp), %ecx        ## 4-byte Reload
	cmpl	%eax, %ecx
	jne	LBB6_32
## BB#27:
	movq	-672(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -752(%rbp)
	leaq	-664(%rbp), %rax
	movq	%rax, -16(%rbp)
	leaq	-656(%rbp), %rsi
Ltmp69:
	leaq	-792(%rbp), %rdi
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp70:
	jmp	LBB6_28
LBB6_28:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit2
	jmp	LBB6_29
LBB6_29:
Ltmp72:
	leaq	-768(%rbp), %rdi
	leaq	-792(%rbp), %rsi
	callq	__ZNSt11logic_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp73:
	jmp	LBB6_30
LBB6_30:
Ltmp75:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-768(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp76:
	jmp	LBB6_31
LBB6_31:
LBB6_32:
	movq	-672(%rbp), %rdi
	callq	___cxa_begin_catch
	leaq	-664(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	leaq	-656(%rbp), %rsi
Ltmp58:
	leaq	-744(%rbp), %rdi
	movq	%rax, -952(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp59:
	jmp	LBB6_33
LBB6_33:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit3
	jmp	LBB6_34
LBB6_34:
Ltmp61:
	leaq	-720(%rbp), %rdi
	leaq	-744(%rbp), %rsi
	callq	__ZNSt13runtime_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp62:
	jmp	LBB6_35
LBB6_35:
Ltmp64:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-720(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp65:
	jmp	LBB6_36
LBB6_36:
LBB6_37:
Ltmp60:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_41
LBB6_38:
Ltmp63:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_40
LBB6_39:
Ltmp66:
	leaq	-720(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	callq	__ZNSt13runtime_errorD1Ev
LBB6_40:
	leaq	-744(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB6_41:
Ltmp67:
	callq	___cxa_end_catch
Ltmp68:
	jmp	LBB6_42
LBB6_42:
	jmp	LBB6_55
LBB6_43:
Ltmp71:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_47
LBB6_44:
Ltmp74:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_46
LBB6_45:
Ltmp77:
	leaq	-768(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	callq	__ZNSt11logic_errorD1Ev
LBB6_46:
	leaq	-792(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB6_47:
Ltmp78:
	callq	___cxa_end_catch
Ltmp79:
	jmp	LBB6_48
LBB6_48:
	jmp	LBB6_55
LBB6_49:
Ltmp82:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_53
LBB6_50:
Ltmp85:
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	jmp	LBB6_52
LBB6_51:
Ltmp88:
	leaq	-816(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -672(%rbp)
	movl	%ecx, -676(%rbp)
	callq	__ZNSt16invalid_argumentD1Ev
LBB6_52:
	leaq	-840(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB6_53:
Ltmp89:
	callq	___cxa_end_catch
Ltmp90:
	jmp	LBB6_54
LBB6_54:
	jmp	LBB6_55
LBB6_55:
	leaq	-664(%rbp), %rdi
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#56:
	movq	-672(%rbp), %rax
	movq	%rax, -912(%rbp)        ## 8-byte Spill
	jmp	LBB6_58
LBB6_57:
Ltmp91:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -956(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB6_58:                                ## %unwind_resume
	movq	-912(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table6:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\231\202\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\204\002"              ## Call site table length
Lset45 = Ltmp43-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset45
Lset46 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset46
Lset47 = Ltmp45-Lfunc_begin2            ##     jumps to Ltmp45
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp46-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset48
Lset49 = Ltmp47-Ltmp46                  ##   Call between Ltmp46 and Ltmp47
	.long	Lset49
Lset50 = Ltmp48-Lfunc_begin2            ##     jumps to Ltmp48
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp47-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset51
Lset52 = Ltmp49-Ltmp47                  ##   Call between Ltmp47 and Ltmp49
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset53 = Ltmp49-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset53
Lset54 = Ltmp50-Ltmp49                  ##   Call between Ltmp49 and Ltmp50
	.long	Lset54
Lset55 = Ltmp51-Lfunc_begin2            ##     jumps to Ltmp51
	.long	Lset55
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp52-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset56
Lset57 = Ltmp53-Ltmp52                  ##   Call between Ltmp52 and Ltmp53
	.long	Lset57
Lset58 = Ltmp54-Lfunc_begin2            ##     jumps to Ltmp54
	.long	Lset58
	.byte	0                       ##   On action: cleanup
Lset59 = Ltmp55-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset59
Lset60 = Ltmp56-Ltmp55                  ##   Call between Ltmp55 and Ltmp56
	.long	Lset60
Lset61 = Ltmp57-Lfunc_begin2            ##     jumps to Ltmp57
	.long	Lset61
	.byte	5                       ##   On action: 3
Lset62 = Ltmp56-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset62
Lset63 = Ltmp80-Ltmp56                  ##   Call between Ltmp56 and Ltmp80
	.long	Lset63
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset64 = Ltmp80-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset64
Lset65 = Ltmp81-Ltmp80                  ##   Call between Ltmp80 and Ltmp81
	.long	Lset65
Lset66 = Ltmp82-Lfunc_begin2            ##     jumps to Ltmp82
	.long	Lset66
	.byte	0                       ##   On action: cleanup
Lset67 = Ltmp83-Lfunc_begin2            ## >> Call Site 9 <<
	.long	Lset67
Lset68 = Ltmp84-Ltmp83                  ##   Call between Ltmp83 and Ltmp84
	.long	Lset68
Lset69 = Ltmp85-Lfunc_begin2            ##     jumps to Ltmp85
	.long	Lset69
	.byte	0                       ##   On action: cleanup
Lset70 = Ltmp86-Lfunc_begin2            ## >> Call Site 10 <<
	.long	Lset70
Lset71 = Ltmp87-Ltmp86                  ##   Call between Ltmp86 and Ltmp87
	.long	Lset71
Lset72 = Ltmp88-Lfunc_begin2            ##     jumps to Ltmp88
	.long	Lset72
	.byte	0                       ##   On action: cleanup
Lset73 = Ltmp87-Lfunc_begin2            ## >> Call Site 11 <<
	.long	Lset73
Lset74 = Ltmp69-Ltmp87                  ##   Call between Ltmp87 and Ltmp69
	.long	Lset74
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset75 = Ltmp69-Lfunc_begin2            ## >> Call Site 12 <<
	.long	Lset75
Lset76 = Ltmp70-Ltmp69                  ##   Call between Ltmp69 and Ltmp70
	.long	Lset76
Lset77 = Ltmp71-Lfunc_begin2            ##     jumps to Ltmp71
	.long	Lset77
	.byte	0                       ##   On action: cleanup
Lset78 = Ltmp72-Lfunc_begin2            ## >> Call Site 13 <<
	.long	Lset78
Lset79 = Ltmp73-Ltmp72                  ##   Call between Ltmp72 and Ltmp73
	.long	Lset79
Lset80 = Ltmp74-Lfunc_begin2            ##     jumps to Ltmp74
	.long	Lset80
	.byte	0                       ##   On action: cleanup
Lset81 = Ltmp75-Lfunc_begin2            ## >> Call Site 14 <<
	.long	Lset81
Lset82 = Ltmp76-Ltmp75                  ##   Call between Ltmp75 and Ltmp76
	.long	Lset82
Lset83 = Ltmp77-Lfunc_begin2            ##     jumps to Ltmp77
	.long	Lset83
	.byte	0                       ##   On action: cleanup
Lset84 = Ltmp76-Lfunc_begin2            ## >> Call Site 15 <<
	.long	Lset84
Lset85 = Ltmp58-Ltmp76                  ##   Call between Ltmp76 and Ltmp58
	.long	Lset85
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp58-Lfunc_begin2            ## >> Call Site 16 <<
	.long	Lset86
Lset87 = Ltmp59-Ltmp58                  ##   Call between Ltmp58 and Ltmp59
	.long	Lset87
Lset88 = Ltmp60-Lfunc_begin2            ##     jumps to Ltmp60
	.long	Lset88
	.byte	0                       ##   On action: cleanup
Lset89 = Ltmp61-Lfunc_begin2            ## >> Call Site 17 <<
	.long	Lset89
Lset90 = Ltmp62-Ltmp61                  ##   Call between Ltmp61 and Ltmp62
	.long	Lset90
Lset91 = Ltmp63-Lfunc_begin2            ##     jumps to Ltmp63
	.long	Lset91
	.byte	0                       ##   On action: cleanup
Lset92 = Ltmp64-Lfunc_begin2            ## >> Call Site 18 <<
	.long	Lset92
Lset93 = Ltmp65-Ltmp64                  ##   Call between Ltmp64 and Ltmp65
	.long	Lset93
Lset94 = Ltmp66-Lfunc_begin2            ##     jumps to Ltmp66
	.long	Lset94
	.byte	0                       ##   On action: cleanup
Lset95 = Ltmp67-Lfunc_begin2            ## >> Call Site 19 <<
	.long	Lset95
Lset96 = Ltmp90-Ltmp67                  ##   Call between Ltmp67 and Ltmp90
	.long	Lset96
Lset97 = Ltmp91-Lfunc_begin2            ##     jumps to Ltmp91
	.long	Lset97
	.byte	1                       ##   On action: 1
Lset98 = Ltmp90-Lfunc_begin2            ## >> Call Site 20 <<
	.long	Lset98
Lset99 = Lfunc_end2-Ltmp90              ##   Call between Ltmp90 and Lfunc_end2
	.long	Lset99
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
	.byte	2                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 2
	.byte	125                     ##   Continue to action 1
	.byte	3                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 3
	.byte	125                     ##   Continue to action 2
                                        ## >> Catch TypeInfos <<
	.long	__ZTISt16invalid_argument@GOTPCREL+4 ## TypeInfo 3
	.long	__ZTISt11logic_error@GOTPCREL+4 ## TypeInfo 2
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4, 0x90
__Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @_Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp104:
	.cfi_def_cfa_offset 16
Ltmp105:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp106:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -88(%rbp)
	movq	-88(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -112(%rbp)        ## 8-byte Spill
	je	LBB7_2
## BB#1:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -120(%rbp)        ## 8-byte Spill
	jmp	LBB7_3
LBB7_2:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -120(%rbp)        ## 8-byte Spill
LBB7_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
Ltmp95:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Z12really_innerm
Ltmp96:
	jmp	LBB7_4
LBB7_4:
	jmp	LBB7_8
LBB7_5:
Ltmp97:
	movl	%edx, %ecx
	movq	%rax, -96(%rbp)
	movl	%ecx, -100(%rbp)
## BB#6:
	movq	-96(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-88(%rbp), %rsi
Ltmp98:
	leaq	L___func__._Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE(%rip), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_
Ltmp99:
	jmp	LBB7_7
LBB7_7:
	callq	___cxa_end_catch
LBB7_8:
	addq	$144, %rsp
	popq	%rbp
	retq
LBB7_9:
Ltmp100:
	movl	%edx, %ecx
	movq	%rax, -96(%rbp)
	movl	%ecx, -100(%rbp)
Ltmp101:
	callq	___cxa_end_catch
Ltmp102:
	jmp	LBB7_10
LBB7_10:
	jmp	LBB7_11
LBB7_11:
	movq	-96(%rbp), %rdi
	callq	__Unwind_Resume
LBB7_12:
Ltmp103:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -132(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table7:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset100 = Ltmp95-Lfunc_begin3           ## >> Call Site 1 <<
	.long	Lset100
Lset101 = Ltmp96-Ltmp95                 ##   Call between Ltmp95 and Ltmp96
	.long	Lset101
Lset102 = Ltmp97-Lfunc_begin3           ##     jumps to Ltmp97
	.long	Lset102
	.byte	1                       ##   On action: 1
Lset103 = Ltmp96-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset103
Lset104 = Ltmp98-Ltmp96                 ##   Call between Ltmp96 and Ltmp98
	.long	Lset104
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset105 = Ltmp98-Lfunc_begin3           ## >> Call Site 3 <<
	.long	Lset105
Lset106 = Ltmp99-Ltmp98                 ##   Call between Ltmp98 and Ltmp99
	.long	Lset106
Lset107 = Ltmp100-Lfunc_begin3          ##     jumps to Ltmp100
	.long	Lset107
	.byte	0                       ##   On action: cleanup
Lset108 = Ltmp99-Lfunc_begin3           ## >> Call Site 4 <<
	.long	Lset108
Lset109 = Ltmp101-Ltmp99                ##   Call between Ltmp99 and Ltmp101
	.long	Lset109
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset110 = Ltmp101-Lfunc_begin3          ## >> Call Site 5 <<
	.long	Lset110
Lset111 = Ltmp102-Ltmp101               ##   Call between Ltmp101 and Ltmp102
	.long	Lset111
Lset112 = Ltmp103-Lfunc_begin3          ##     jumps to Ltmp103
	.long	Lset112
	.byte	1                       ##   On action: 1
Lset113 = Ltmp102-Lfunc_begin3          ## >> Call Site 6 <<
	.long	Lset113
Lset114 = Lfunc_end3-Ltmp102            ##   Call between Ltmp102 and Lfunc_end3
	.long	Lset114
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_
	.weak_def_can_be_hidden	__Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_
	.align	4, 0x90
__Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_: ## @_Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp160:
	.cfi_def_cfa_offset 16
Ltmp161:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp162:
	.cfi_def_cfa_register %rbp
	subq	$992, %rsp              ## imm = 0x3E0
	movq	%rdi, -400(%rbp)
	movq	%rsi, -408(%rbp)
	leaq	-672(%rbp), %rsi
	movq	%rsi, -368(%rbp)
	movl	$16, -372(%rbp)
	movq	-368(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$112, %rdi
	movq	%rdi, -360(%rbp)
	movq	%rdi, -352(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, 112(%rsi)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, 112(%rsi)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdi
	movq	%rdi, %rax
	addq	$24, %rax
	movq	%rax, (%rsi)
	addq	$64, %rdi
	movq	%rdi, 112(%rsi)
	movq	%rsi, %rax
	addq	$8, %rax
	movq	%rsi, -80(%rbp)
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdi
	addq	$8, %rdi
	movq	%rdi, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	%rcx, (%rax)
	movq	8(%rdi), %rdi
	movq	-24(%rcx), %rcx
	movq	%rdi, (%rax,%rcx)
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	-96(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rax
Ltmp107:
	movq	%rax, %rdi
	movq	%rsi, -856(%rbp)        ## 8-byte Spill
	movq	%rcx, %rsi
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp108:
	jmp	LBB8_1
LBB8_1:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-864(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-856(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 112(%rdi)
	addq	$8, %rdi
	movl	-372(%rbp), %eax
	orl	$16, %eax
	movq	%rdi, -336(%rbp)
	movl	%eax, -340(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movl	%eax, -284(%rbp)
	movq	-280(%rbp), %rdx
Ltmp110:
	movq	%rdx, %rdi
	movq	%rdx, -872(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp111:
	jmp	LBB8_2
LBB8_2:                                 ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-872(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %r8
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	%r8, -240(%rbp)
	movq	-240(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -232(%rbp)
	movq	%rdi, -880(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movl	$0, -220(%rbp)
LBB8_3:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -220(%rbp)
	jae	LBB8_5
## BB#4:                                ##   in Loop: Header=BB8_3 Depth=1
	movl	-220(%rbp), %eax
	movl	%eax, %ecx
	movq	-216(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-220(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -220(%rbp)
	jmp	LBB8_3
LBB8_5:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-312(%rbp), %rcx
	movq	-872(%rbp), %rdi        ## 8-byte Reload
	movq	$0, 88(%rdi)
	movl	-284(%rbp), %eax
	movl	%eax, 96(%rdi)
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %r8
	movq	%r8, -160(%rbp)
	movq	-160(%rbp), %r8
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -144(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -896(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-896(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movl	$0, -132(%rbp)
LBB8_6:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -132(%rbp)
	jae	LBB8_8
## BB#7:                                ##   in Loop: Header=BB8_6 Depth=1
	movl	-132(%rbp), %eax
	movl	%eax, %ecx
	movq	-128(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-132(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -132(%rbp)
	jmp	LBB8_6
LBB8_8:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3.i.i.i
Ltmp113:
	leaq	-312(%rbp), %rsi
	movq	-872(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp114:
	jmp	LBB8_14
LBB8_9:
Ltmp115:
	movl	%edx, %ecx
	movq	%rax, -320(%rbp)
	movl	%ecx, -324(%rbp)
	leaq	-312(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-880(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-872(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-320(%rbp), %rax
	movl	-324(%rbp), %ecx
	movq	%rax, -904(%rbp)        ## 8-byte Spill
	movl	%ecx, -908(%rbp)        ## 4-byte Spill
	jmp	LBB8_12
LBB8_10:
Ltmp109:
	movl	%edx, %ecx
	movq	%rax, -384(%rbp)
	movl	%ecx, -388(%rbp)
	jmp	LBB8_13
LBB8_11:
Ltmp112:
	movl	%edx, %ecx
	movq	%rax, -904(%rbp)        ## 8-byte Spill
	movl	%ecx, -908(%rbp)        ## 4-byte Spill
	jmp	LBB8_12
LBB8_12:                                ## %.body.i
	movl	-908(%rbp), %eax        ## 4-byte Reload
	movq	-904(%rbp), %rcx        ## 8-byte Reload
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -384(%rbp)
	movl	%eax, -388(%rbp)
	movq	-856(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB8_13:
	movq	-856(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-384(%rbp), %rax
	movq	%rax, -920(%rbp)        ## 8-byte Spill
	jmp	LBB8_60
LBB8_14:                                ## %_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej.exit
	leaq	-312(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-400(%rbp), %rsi
Ltmp116:
	leaq	-672(%rbp), %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp117:
	movq	%rax, -928(%rbp)        ## 8-byte Spill
	jmp	LBB8_15
LBB8_15:
	leaq	L_.str.4(%rip), %rax
	movq	%rax, -696(%rbp)
	movl	$0, -704(%rbp)
	leaq	-700(%rbp), %rax
	movq	-696(%rbp), %rsi
Ltmp118:
	leaq	-672(%rbp), %rdi
	movq	%rax, -936(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp119:
	movq	%rax, -944(%rbp)        ## 8-byte Spill
	jmp	LBB8_16
LBB8_16:
	movq	-408(%rbp), %rsi
Ltmp120:
	movq	-944(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp121:
	movq	%rax, -952(%rbp)        ## 8-byte Spill
	jmp	LBB8_17
LBB8_17:
	leaq	L_.str.5(%rip), %rax
	movq	%rax, -696(%rbp)
	movq	-936(%rbp), %rax        ## 8-byte Reload
	movl	$0, (%rax)
	leaq	-712(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -960(%rbp)        ## 8-byte Spill
	callq	__ZSt17current_exceptionv
Ltmp123:
	movq	-960(%rbp), %rdi        ## 8-byte Reload
	callq	__ZSt17rethrow_exceptionSt13exception_ptr
Ltmp124:
	jmp	LBB8_18
LBB8_18:
LBB8_19:
Ltmp122:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_57
LBB8_20:
Ltmp125:
	leaq	-712(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	callq	__ZNSt13exception_ptrD1Ev
## BB#21:
	movl	-684(%rbp), %eax
	movl	$3, %ecx
	cmpl	%ecx, %eax
	movl	%eax, -964(%rbp)        ## 4-byte Spill
	jne	LBB8_28
## BB#22:
	movq	-680(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -808(%rbp)
	leaq	-672(%rbp), %rax
	movq	%rax, -56(%rbp)
	leaq	-664(%rbp), %rsi
Ltmp148:
	leaq	-848(%rbp), %rdi
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp149:
	jmp	LBB8_23
LBB8_23:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit
	jmp	LBB8_24
LBB8_24:
	leaq	-824(%rbp), %rax
	movq	%rax, -40(%rbp)
	leaq	-848(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	%rax, -32(%rbp)
	movq	-24(%rbp), %rcx
Ltmp151:
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -976(%rbp)        ## 8-byte Spill
	callq	__ZNSt11logic_errorC2ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp152:
	jmp	LBB8_25
LBB8_25:                                ## %_ZNSt16invalid_argumentC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE.exit
	movq	__ZTVSt16invalid_argument@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-976(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
## BB#26:
Ltmp154:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-824(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp155:
	jmp	LBB8_27
LBB8_27:
LBB8_28:
	movl	$2, %eax
	movl	-964(%rbp), %ecx        ## 4-byte Reload
	cmpl	%eax, %ecx
	jne	LBB8_34
## BB#29:
	movq	-680(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -760(%rbp)
	leaq	-672(%rbp), %rax
	movq	%rax, -16(%rbp)
	leaq	-664(%rbp), %rsi
Ltmp137:
	leaq	-800(%rbp), %rdi
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp138:
	jmp	LBB8_30
LBB8_30:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit2
	jmp	LBB8_31
LBB8_31:
Ltmp140:
	leaq	-776(%rbp), %rdi
	leaq	-800(%rbp), %rsi
	callq	__ZNSt11logic_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp141:
	jmp	LBB8_32
LBB8_32:
Ltmp143:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-776(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp144:
	jmp	LBB8_33
LBB8_33:
LBB8_34:
	movq	-680(%rbp), %rdi
	callq	___cxa_begin_catch
	leaq	-672(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	leaq	-664(%rbp), %rsi
Ltmp126:
	leaq	-752(%rbp), %rdi
	movq	%rax, -984(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp127:
	jmp	LBB8_35
LBB8_35:                                ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit3
	jmp	LBB8_36
LBB8_36:
Ltmp129:
	leaq	-728(%rbp), %rdi
	leaq	-752(%rbp), %rsi
	callq	__ZNSt13runtime_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp130:
	jmp	LBB8_37
LBB8_37:
Ltmp132:
	xorl	%eax, %eax
	movl	%eax, %esi
	leaq	-728(%rbp), %rdi
	callq	__ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
Ltmp133:
	jmp	LBB8_38
LBB8_38:
LBB8_39:
Ltmp128:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_43
LBB8_40:
Ltmp131:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_42
LBB8_41:
Ltmp134:
	leaq	-728(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	callq	__ZNSt13runtime_errorD1Ev
LBB8_42:
	leaq	-752(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB8_43:
Ltmp135:
	callq	___cxa_end_catch
Ltmp136:
	jmp	LBB8_44
LBB8_44:
	jmp	LBB8_57
LBB8_45:
Ltmp139:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_49
LBB8_46:
Ltmp142:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_48
LBB8_47:
Ltmp145:
	leaq	-776(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	callq	__ZNSt11logic_errorD1Ev
LBB8_48:
	leaq	-800(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB8_49:
Ltmp146:
	callq	___cxa_end_catch
Ltmp147:
	jmp	LBB8_50
LBB8_50:
	jmp	LBB8_57
LBB8_51:
Ltmp150:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_55
LBB8_52:
Ltmp153:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	jmp	LBB8_54
LBB8_53:
Ltmp156:
	leaq	-824(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	callq	__ZNSt16invalid_argumentD1Ev
LBB8_54:
	leaq	-848(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB8_55:
Ltmp157:
	callq	___cxa_end_catch
Ltmp158:
	jmp	LBB8_56
LBB8_56:
	jmp	LBB8_57
LBB8_57:
	leaq	-672(%rbp), %rdi
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#58:
	movq	-680(%rbp), %rax
	movq	%rax, -920(%rbp)        ## 8-byte Spill
	jmp	LBB8_60
LBB8_59:
Ltmp159:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -988(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB8_60:                                ## %unwind_resume
	movq	-920(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table8:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\231\202\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\204\002"              ## Call site table length
Lset115 = Ltmp107-Lfunc_begin4          ## >> Call Site 1 <<
	.long	Lset115
Lset116 = Ltmp108-Ltmp107               ##   Call between Ltmp107 and Ltmp108
	.long	Lset116
Lset117 = Ltmp109-Lfunc_begin4          ##     jumps to Ltmp109
	.long	Lset117
	.byte	0                       ##   On action: cleanup
Lset118 = Ltmp110-Lfunc_begin4          ## >> Call Site 2 <<
	.long	Lset118
Lset119 = Ltmp111-Ltmp110               ##   Call between Ltmp110 and Ltmp111
	.long	Lset119
Lset120 = Ltmp112-Lfunc_begin4          ##     jumps to Ltmp112
	.long	Lset120
	.byte	0                       ##   On action: cleanup
Lset121 = Ltmp111-Lfunc_begin4          ## >> Call Site 3 <<
	.long	Lset121
Lset122 = Ltmp113-Ltmp111               ##   Call between Ltmp111 and Ltmp113
	.long	Lset122
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset123 = Ltmp113-Lfunc_begin4          ## >> Call Site 4 <<
	.long	Lset123
Lset124 = Ltmp114-Ltmp113               ##   Call between Ltmp113 and Ltmp114
	.long	Lset124
Lset125 = Ltmp115-Lfunc_begin4          ##     jumps to Ltmp115
	.long	Lset125
	.byte	0                       ##   On action: cleanup
Lset126 = Ltmp116-Lfunc_begin4          ## >> Call Site 5 <<
	.long	Lset126
Lset127 = Ltmp121-Ltmp116               ##   Call between Ltmp116 and Ltmp121
	.long	Lset127
Lset128 = Ltmp122-Lfunc_begin4          ##     jumps to Ltmp122
	.long	Lset128
	.byte	0                       ##   On action: cleanup
Lset129 = Ltmp123-Lfunc_begin4          ## >> Call Site 6 <<
	.long	Lset129
Lset130 = Ltmp124-Ltmp123               ##   Call between Ltmp123 and Ltmp124
	.long	Lset130
Lset131 = Ltmp125-Lfunc_begin4          ##     jumps to Ltmp125
	.long	Lset131
	.byte	5                       ##   On action: 3
Lset132 = Ltmp124-Lfunc_begin4          ## >> Call Site 7 <<
	.long	Lset132
Lset133 = Ltmp148-Ltmp124               ##   Call between Ltmp124 and Ltmp148
	.long	Lset133
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset134 = Ltmp148-Lfunc_begin4          ## >> Call Site 8 <<
	.long	Lset134
Lset135 = Ltmp149-Ltmp148               ##   Call between Ltmp148 and Ltmp149
	.long	Lset135
Lset136 = Ltmp150-Lfunc_begin4          ##     jumps to Ltmp150
	.long	Lset136
	.byte	0                       ##   On action: cleanup
Lset137 = Ltmp151-Lfunc_begin4          ## >> Call Site 9 <<
	.long	Lset137
Lset138 = Ltmp152-Ltmp151               ##   Call between Ltmp151 and Ltmp152
	.long	Lset138
Lset139 = Ltmp153-Lfunc_begin4          ##     jumps to Ltmp153
	.long	Lset139
	.byte	0                       ##   On action: cleanup
Lset140 = Ltmp154-Lfunc_begin4          ## >> Call Site 10 <<
	.long	Lset140
Lset141 = Ltmp155-Ltmp154               ##   Call between Ltmp154 and Ltmp155
	.long	Lset141
Lset142 = Ltmp156-Lfunc_begin4          ##     jumps to Ltmp156
	.long	Lset142
	.byte	0                       ##   On action: cleanup
Lset143 = Ltmp155-Lfunc_begin4          ## >> Call Site 11 <<
	.long	Lset143
Lset144 = Ltmp137-Ltmp155               ##   Call between Ltmp155 and Ltmp137
	.long	Lset144
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset145 = Ltmp137-Lfunc_begin4          ## >> Call Site 12 <<
	.long	Lset145
Lset146 = Ltmp138-Ltmp137               ##   Call between Ltmp137 and Ltmp138
	.long	Lset146
Lset147 = Ltmp139-Lfunc_begin4          ##     jumps to Ltmp139
	.long	Lset147
	.byte	0                       ##   On action: cleanup
Lset148 = Ltmp140-Lfunc_begin4          ## >> Call Site 13 <<
	.long	Lset148
Lset149 = Ltmp141-Ltmp140               ##   Call between Ltmp140 and Ltmp141
	.long	Lset149
Lset150 = Ltmp142-Lfunc_begin4          ##     jumps to Ltmp142
	.long	Lset150
	.byte	0                       ##   On action: cleanup
Lset151 = Ltmp143-Lfunc_begin4          ## >> Call Site 14 <<
	.long	Lset151
Lset152 = Ltmp144-Ltmp143               ##   Call between Ltmp143 and Ltmp144
	.long	Lset152
Lset153 = Ltmp145-Lfunc_begin4          ##     jumps to Ltmp145
	.long	Lset153
	.byte	0                       ##   On action: cleanup
Lset154 = Ltmp144-Lfunc_begin4          ## >> Call Site 15 <<
	.long	Lset154
Lset155 = Ltmp126-Ltmp144               ##   Call between Ltmp144 and Ltmp126
	.long	Lset155
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset156 = Ltmp126-Lfunc_begin4          ## >> Call Site 16 <<
	.long	Lset156
Lset157 = Ltmp127-Ltmp126               ##   Call between Ltmp126 and Ltmp127
	.long	Lset157
Lset158 = Ltmp128-Lfunc_begin4          ##     jumps to Ltmp128
	.long	Lset158
	.byte	0                       ##   On action: cleanup
Lset159 = Ltmp129-Lfunc_begin4          ## >> Call Site 17 <<
	.long	Lset159
Lset160 = Ltmp130-Ltmp129               ##   Call between Ltmp129 and Ltmp130
	.long	Lset160
Lset161 = Ltmp131-Lfunc_begin4          ##     jumps to Ltmp131
	.long	Lset161
	.byte	0                       ##   On action: cleanup
Lset162 = Ltmp132-Lfunc_begin4          ## >> Call Site 18 <<
	.long	Lset162
Lset163 = Ltmp133-Ltmp132               ##   Call between Ltmp132 and Ltmp133
	.long	Lset163
Lset164 = Ltmp134-Lfunc_begin4          ##     jumps to Ltmp134
	.long	Lset164
	.byte	0                       ##   On action: cleanup
Lset165 = Ltmp135-Lfunc_begin4          ## >> Call Site 19 <<
	.long	Lset165
Lset166 = Ltmp158-Ltmp135               ##   Call between Ltmp135 and Ltmp158
	.long	Lset166
Lset167 = Ltmp159-Lfunc_begin4          ##     jumps to Ltmp159
	.long	Lset167
	.byte	1                       ##   On action: 1
Lset168 = Ltmp158-Lfunc_begin4          ## >> Call Site 20 <<
	.long	Lset168
Lset169 = Lfunc_end4-Ltmp158            ##   Call between Ltmp158 and Lfunc_end4
	.long	Lset169
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
	.byte	2                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 2
	.byte	125                     ##   Continue to action 1
	.byte	3                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 3
	.byte	125                     ##   Continue to action 2
                                        ## >> Catch TypeInfos <<
	.long	__ZTISt16invalid_argument@GOTPCREL+4 ## TypeInfo 3
	.long	__ZTISt11logic_error@GOTPCREL+4 ## TypeInfo 2
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4, 0x90
__Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @_Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp177:
	.cfi_def_cfa_offset 16
Ltmp178:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp179:
	.cfi_def_cfa_register %rbp
	subq	$528, %rsp              ## imm = 0x210
	movq	%rdi, -376(%rbp)
Ltmp163:
	leaq	-400(%rbp), %rax
	movq	%rdi, -440(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-440(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
Ltmp164:
	jmp	LBB9_1
LBB9_1:
	movq	-376(%rbp), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rcx
	movq	%rcx, -328(%rbp)
	movq	-328(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -448(%rbp)        ## 8-byte Spill
	je	LBB9_3
## BB#2:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	jmp	LBB9_4
LBB9_3:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
LBB9_4:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5beginEv.exit
	movq	-456(%rbp), %rax        ## 8-byte Reload
	leaq	-360(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-360(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	-376(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	movq	%rcx, -472(%rbp)        ## 8-byte Spill
	je	LBB9_6
## BB#5:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -480(%rbp)        ## 8-byte Spill
	jmp	LBB9_7
LBB9_6:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -480(%rbp)        ## 8-byte Spill
LBB9_7:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv.exit.i
	movq	-480(%rbp), %rax        ## 8-byte Reload
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rsi
	movzbl	(%rsi), %edi
	andl	$1, %edi
	cmpl	$0, %edi
	movq	%rax, -488(%rbp)        ## 8-byte Spill
	movq	%rdx, -496(%rbp)        ## 8-byte Spill
	je	LBB9_9
## BB#8:
	movq	-496(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -504(%rbp)        ## 8-byte Spill
	jmp	LBB9_10
LBB9_9:
	movq	-496(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -504(%rbp)        ## 8-byte Spill
LBB9_10:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE3endEv.exit
	movq	-504(%rbp), %rax        ## 8-byte Reload
	movq	-488(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	leaq	-216(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	%rcx, (%rax)
	movq	-216(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	-424(%rbp), %rsi
Ltmp166:
	leaq	-400(%rbp), %rdi
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendINS_11__wrap_iterIPKcEEEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueERS5_E4typeESC_SC_
Ltmp167:
	movq	%rax, -512(%rbp)        ## 8-byte Spill
	jmp	LBB9_11
LBB9_11:
Ltmp168:
	leaq	-400(%rbp), %rdi
	callq	__Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp169:
	jmp	LBB9_12
LBB9_12:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB9_17
LBB9_13:
Ltmp165:
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	jmp	LBB9_15
LBB9_14:
Ltmp170:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB9_15:
	movq	-408(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-376(%rbp), %rsi
Ltmp171:
	leaq	L___func__._Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE(%rip), %rdi
	movq	%rax, -520(%rbp)        ## 8-byte Spill
	callq	__Z7rethrowIRA6_KcJRKNSt3__112basic_stringIcNS3_11char_traitsIcEENS3_9allocatorIcEEEEEEvOT_DpOT0_
Ltmp172:
	jmp	LBB9_16
LBB9_16:
	callq	___cxa_end_catch
LBB9_17:
	addq	$528, %rsp              ## imm = 0x210
	popq	%rbp
	retq
LBB9_18:
Ltmp173:
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
Ltmp174:
	callq	___cxa_end_catch
Ltmp175:
	jmp	LBB9_19
LBB9_19:
	jmp	LBB9_20
LBB9_20:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB9_21:
Ltmp176:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -524(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table9:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\343\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	91                      ## Call site table length
Lset170 = Ltmp163-Lfunc_begin5          ## >> Call Site 1 <<
	.long	Lset170
Lset171 = Ltmp164-Ltmp163               ##   Call between Ltmp163 and Ltmp164
	.long	Lset171
Lset172 = Ltmp165-Lfunc_begin5          ##     jumps to Ltmp165
	.long	Lset172
	.byte	1                       ##   On action: 1
Lset173 = Ltmp166-Lfunc_begin5          ## >> Call Site 2 <<
	.long	Lset173
Lset174 = Ltmp169-Ltmp166               ##   Call between Ltmp166 and Ltmp169
	.long	Lset174
Lset175 = Ltmp170-Lfunc_begin5          ##     jumps to Ltmp170
	.long	Lset175
	.byte	1                       ##   On action: 1
Lset176 = Ltmp169-Lfunc_begin5          ## >> Call Site 3 <<
	.long	Lset176
Lset177 = Ltmp171-Ltmp169               ##   Call between Ltmp169 and Ltmp171
	.long	Lset177
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset178 = Ltmp171-Lfunc_begin5          ## >> Call Site 4 <<
	.long	Lset178
Lset179 = Ltmp172-Ltmp171               ##   Call between Ltmp171 and Ltmp172
	.long	Lset179
Lset180 = Ltmp173-Lfunc_begin5          ##     jumps to Ltmp173
	.long	Lset180
	.byte	0                       ##   On action: cleanup
Lset181 = Ltmp172-Lfunc_begin5          ## >> Call Site 5 <<
	.long	Lset181
Lset182 = Ltmp174-Ltmp172               ##   Call between Ltmp172 and Ltmp174
	.long	Lset182
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset183 = Ltmp174-Lfunc_begin5          ## >> Call Site 6 <<
	.long	Lset183
Lset184 = Ltmp175-Ltmp174               ##   Call between Ltmp174 and Ltmp175
	.long	Lset184
Lset185 = Ltmp176-Lfunc_begin5          ##     jumps to Ltmp176
	.long	Lset185
	.byte	1                       ##   On action: 1
Lset186 = Ltmp175-Lfunc_begin5          ## >> Call Site 7 <<
	.long	Lset186
Lset187 = Lfunc_end5-Ltmp175            ##   Call between Ltmp175 and Lfunc_end5
	.long	Lset187
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendINS_11__wrap_iterIPKcEEEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueERS5_E4typeESC_SC_
	.weak_def_can_be_hidden	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendINS_11__wrap_iterIPKcEEEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueERS5_E4typeESC_SC_
	.align	4, 0x90
__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendINS_11__wrap_iterIPKcEEEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueERS5_E4typeESC_SC_: ## @_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendINS_11__wrap_iterIPKcEEEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueERS5_E4typeESC_SC_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp180:
	.cfi_def_cfa_offset 16
Ltmp181:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp182:
	.cfi_def_cfa_register %rbp
	subq	$656, %rsp              ## imm = 0x290
	movq	%rsi, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdi, -520(%rbp)
	movq	-520(%rbp), %rdx
	movq	%rdx, -496(%rbp)
	movq	-496(%rbp), %rsi
	movq	%rsi, -488(%rbp)
	movq	-488(%rbp), %rdi
	movq	%rdi, -480(%rbp)
	movq	-480(%rbp), %rdi
	movq	%rdi, -472(%rbp)
	movq	-472(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rdx, -584(%rbp)        ## 8-byte Spill
	movq	%rsi, -592(%rbp)        ## 8-byte Spill
	je	LBB10_2
## BB#1:
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -600(%rbp)        ## 8-byte Spill
	jmp	LBB10_3
LBB10_2:
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -600(%rbp)        ## 8-byte Spill
LBB10_3:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	-584(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rdx
	movq	%rdx, -296(%rbp)
	movq	-296(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -608(%rbp)        ## 8-byte Spill
	je	LBB10_5
## BB#4:
	movq	-608(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -616(%rbp)        ## 8-byte Spill
	jmp	LBB10_6
LBB10_5:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -616(%rbp)        ## 8-byte Spill
	jmp	LBB10_6
LBB10_6:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-616(%rbp), %rax        ## 8-byte Reload
	leaq	-192(%rbp), %rcx
	leaq	-200(%rbp), %rdx
	subq	$1, %rax
	movq	%rax, -536(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	-512(%rbp), %rax
	movq	%rax, -560(%rbp)
	movq	-552(%rbp), %rax
	movq	-560(%rbp), %rsi
	movq	%rax, -216(%rbp)
	movq	%rsi, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -176(%rbp)
	movq	%rcx, -184(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	(%rax), %rax
	movq	-184(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -544(%rbp)
	cmpq	$0, -544(%rbp)
	je	LBB10_20
## BB#7:
	movq	-536(%rbp), %rax
	subq	-528(%rbp), %rax
	cmpq	-544(%rbp), %rax
	jae	LBB10_9
## BB#8:
	xorl	%eax, %eax
	movl	%eax, %r9d
	movq	-536(%rbp), %rsi
	movq	-528(%rbp), %rcx
	addq	-544(%rbp), %rcx
	subq	-536(%rbp), %rcx
	movq	-528(%rbp), %rdx
	movq	-528(%rbp), %r8
	movq	-584(%rbp), %rdi        ## 8-byte Reload
	movq	%rdx, -624(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-624(%rbp), %rcx        ## 8-byte Reload
	movq	$0, (%rsp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9__grow_byEmmmmmm
LBB10_9:
	movq	-584(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -632(%rbp)        ## 8-byte Spill
	je	LBB10_11
## BB#10:
	movq	-632(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -640(%rbp)        ## 8-byte Spill
	jmp	LBB10_12
LBB10_11:
	movq	-632(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -640(%rbp)        ## 8-byte Spill
LBB10_12:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13__get_pointerEv.exit
	movq	-640(%rbp), %rax        ## 8-byte Reload
	addq	-528(%rbp), %rax
	movq	%rax, -568(%rbp)
LBB10_13:                               ## =>This Inner Loop Header: Depth=1
	leaq	-512(%rbp), %rax
	leaq	-504(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	-128(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB10_14
	jmp	LBB10_16
LBB10_14:                               ##   in Loop: Header=BB10_13 Depth=1
	leaq	-504(%rbp), %rax
	movq	-568(%rbp), %rdi
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	(%rax), %rsi
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
## BB#15:                               ##   in Loop: Header=BB10_13 Depth=1
	leaq	-504(%rbp), %rax
	movq	-568(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -568(%rbp)
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	(%rax), %rcx
	addq	$1, %rcx
	movq	%rcx, (%rax)
	jmp	LBB10_13
LBB10_16:
	leaq	-569(%rbp), %rsi
	movq	-568(%rbp), %rdi
	movb	$0, -569(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-528(%rbp), %rsi
	addq	-544(%rbp), %rsi
	movq	-584(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -408(%rbp)
	movq	%rsi, -416(%rbp)
	movq	-408(%rbp), %rsi
	movq	%rsi, -400(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	-384(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rsi, -648(%rbp)        ## 8-byte Spill
	je	LBB10_18
## BB#17:
	movq	-416(%rbp), %rax
	movq	-648(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rdx
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	%rdx, 8(%rax)
	jmp	LBB10_19
LBB10_18:
	movq	-416(%rbp), %rax
	movq	-648(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -368(%rbp)
	movq	%rax, -376(%rbp)
	movq	-368(%rbp), %rax
	movq	-376(%rbp), %rdx
	shlq	$1, %rdx
	movb	%dl, %sil
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movb	%sil, (%rax)
LBB10_19:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE10__set_sizeEm.exit
	jmp	LBB10_20
LBB10_20:
	movq	-584(%rbp), %rax        ## 8-byte Reload
	addq	$656, %rsp              ## imm = 0x290
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp204:
	.cfi_def_cfa_offset 16
Ltmp205:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp206:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movl	$0, -132(%rbp)
	leaq	-160(%rbp), %rax
	movq	%rax, -120(%rbp)
	leaq	L_.str.2(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-112(%rbp), %rcx
Ltmp183:
	movq	%rcx, %rdi
	movq	%rax, -216(%rbp)        ## 8-byte Spill
	movq	%rcx, -224(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp184:
	movq	%rax, -232(%rbp)        ## 8-byte Spill
	jmp	LBB11_1
LBB11_1:                                ## %.noexc
Ltmp185:
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-224(%rbp), %rsi        ## 8-byte Reload
	movq	-232(%rbp), %rdx        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp186:
	jmp	LBB11_2
LBB11_2:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
	jmp	LBB11_3
LBB11_3:
Ltmp187:
	leaq	-160(%rbp), %rdi
	callq	__Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp188:
	jmp	LBB11_4
LBB11_4:
	leaq	-160(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-200(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	leaq	L_.str.3(%rip), %rdi
	movq	%rdi, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-48(%rbp), %rdi
Ltmp190:
	movq	%rdi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp191:
	movq	%rax, -256(%rbp)        ## 8-byte Spill
	jmp	LBB11_5
LBB11_5:                                ## %.noexc2
Ltmp192:
	movq	-248(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	-256(%rbp), %rdx        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp193:
	jmp	LBB11_6
LBB11_6:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit4
	jmp	LBB11_7
LBB11_7:
Ltmp195:
	leaq	-200(%rbp), %rdi
	callq	__Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp196:
	jmp	LBB11_8
LBB11_8:
	leaq	-200(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB11_15
LBB11_9:
Ltmp194:
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
	jmp	LBB11_12
LBB11_10:
Ltmp189:
	leaq	-160(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB11_12
LBB11_11:
Ltmp197:
	leaq	-200(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB11_12:
	movl	-172(%rbp), %eax
	movl	$1, %ecx
	cmpl	%ecx, %eax
	jne	LBB11_18
## BB#13:
	movq	-168(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -208(%rbp)
Ltmp198:
	xorl	%ecx, %ecx
	movl	%ecx, %esi
	movq	%rax, %rdi
	callq	__Z15print_exceptionRKSt9exceptionm
Ltmp199:
	jmp	LBB11_14
LBB11_14:
	callq	___cxa_end_catch
LBB11_15:
	xorl	%eax, %eax
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
LBB11_16:
Ltmp200:
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
Ltmp201:
	callq	___cxa_end_catch
Ltmp202:
	jmp	LBB11_17
LBB11_17:
	jmp	LBB11_18
LBB11_18:
	movq	-168(%rbp), %rdi
	callq	__Unwind_Resume
LBB11_19:
Ltmp203:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -260(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table11:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\207\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset188 = Ltmp183-Lfunc_begin6          ## >> Call Site 1 <<
	.long	Lset188
Lset189 = Ltmp186-Ltmp183               ##   Call between Ltmp183 and Ltmp186
	.long	Lset189
Lset190 = Ltmp194-Lfunc_begin6          ##     jumps to Ltmp194
	.long	Lset190
	.byte	5                       ##   On action: 3
Lset191 = Ltmp187-Lfunc_begin6          ## >> Call Site 2 <<
	.long	Lset191
Lset192 = Ltmp188-Ltmp187               ##   Call between Ltmp187 and Ltmp188
	.long	Lset192
Lset193 = Ltmp189-Lfunc_begin6          ##     jumps to Ltmp189
	.long	Lset193
	.byte	3                       ##   On action: 2
Lset194 = Ltmp190-Lfunc_begin6          ## >> Call Site 3 <<
	.long	Lset194
Lset195 = Ltmp193-Ltmp190               ##   Call between Ltmp190 and Ltmp193
	.long	Lset195
Lset196 = Ltmp194-Lfunc_begin6          ##     jumps to Ltmp194
	.long	Lset196
	.byte	5                       ##   On action: 3
Lset197 = Ltmp195-Lfunc_begin6          ## >> Call Site 4 <<
	.long	Lset197
Lset198 = Ltmp196-Ltmp195               ##   Call between Ltmp195 and Ltmp196
	.long	Lset198
Lset199 = Ltmp197-Lfunc_begin6          ##     jumps to Ltmp197
	.long	Lset199
	.byte	3                       ##   On action: 2
Lset200 = Ltmp196-Lfunc_begin6          ## >> Call Site 5 <<
	.long	Lset200
Lset201 = Ltmp198-Ltmp196               ##   Call between Ltmp196 and Ltmp198
	.long	Lset201
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset202 = Ltmp198-Lfunc_begin6          ## >> Call Site 6 <<
	.long	Lset202
Lset203 = Ltmp199-Ltmp198               ##   Call between Ltmp198 and Ltmp199
	.long	Lset203
Lset204 = Ltmp200-Lfunc_begin6          ##     jumps to Ltmp200
	.long	Lset204
	.byte	0                       ##   On action: cleanup
Lset205 = Ltmp199-Lfunc_begin6          ## >> Call Site 7 <<
	.long	Lset205
Lset206 = Ltmp201-Ltmp199               ##   Call between Ltmp199 and Ltmp201
	.long	Lset206
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset207 = Ltmp201-Lfunc_begin6          ## >> Call Site 8 <<
	.long	Lset207
Lset208 = Ltmp202-Ltmp201               ##   Call between Ltmp201 and Ltmp202
	.long	Lset208
Lset209 = Ltmp203-Lfunc_begin6          ##     jumps to Ltmp203
	.long	Lset209
	.byte	7                       ##   On action: 4
Lset210 = Ltmp202-Lfunc_begin6          ## >> Call Site 9 <<
	.long	Lset210
Lset211 = Lfunc_end6-Ltmp202            ##   Call between Ltmp202 and Lfunc_end6
	.long	Lset211
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
	.byte	2                       ## >> Action Record 4 <<
                                        ##   Catch TypeInfo 2
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 2
	.long	__ZTISt9exception@GOTPCREL+4 ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp228:
	.cfi_def_cfa_offset 16
Ltmp229:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp230:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp207:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp208:
	jmp	LBB12_1
LBB12_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB12_3
	jmp	LBB12_26
LBB12_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB12_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB12_7
LBB12_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB12_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB12_8
	jmp	LBB12_13
LBB12_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp210:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp211:
	jmp	LBB12_9
LBB12_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp212:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp213:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB12_10
LBB12_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp214:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp215:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB12_12
LBB12_11:
Ltmp216:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB12_21
LBB12_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB12_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp217:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp218:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB12_15
LBB12_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB12_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp219:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp220:
	jmp	LBB12_17
LBB12_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB12_18
LBB12_18:
	jmp	LBB12_25
LBB12_19:
Ltmp209:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB12_22
LBB12_20:
Ltmp221:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB12_21
LBB12_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB12_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp222:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp223:
	jmp	LBB12_23
LBB12_23:
	callq	___cxa_end_catch
LBB12_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB12_25:
	jmp	LBB12_26
LBB12_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB12_24
LBB12_27:
Ltmp224:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp225:
	callq	___cxa_end_catch
Ltmp226:
	jmp	LBB12_28
LBB12_28:
	jmp	LBB12_29
LBB12_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB12_30:
Ltmp227:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table12:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset212 = Ltmp207-Lfunc_begin7          ## >> Call Site 1 <<
	.long	Lset212
Lset213 = Ltmp208-Ltmp207               ##   Call between Ltmp207 and Ltmp208
	.long	Lset213
Lset214 = Ltmp209-Lfunc_begin7          ##     jumps to Ltmp209
	.long	Lset214
	.byte	5                       ##   On action: 3
Lset215 = Ltmp210-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset215
Lset216 = Ltmp211-Ltmp210               ##   Call between Ltmp210 and Ltmp211
	.long	Lset216
Lset217 = Ltmp221-Lfunc_begin7          ##     jumps to Ltmp221
	.long	Lset217
	.byte	5                       ##   On action: 3
Lset218 = Ltmp212-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset218
Lset219 = Ltmp215-Ltmp212               ##   Call between Ltmp212 and Ltmp215
	.long	Lset219
Lset220 = Ltmp216-Lfunc_begin7          ##     jumps to Ltmp216
	.long	Lset220
	.byte	3                       ##   On action: 2
Lset221 = Ltmp217-Lfunc_begin7          ## >> Call Site 4 <<
	.long	Lset221
Lset222 = Ltmp220-Ltmp217               ##   Call between Ltmp217 and Ltmp220
	.long	Lset222
Lset223 = Ltmp221-Lfunc_begin7          ##     jumps to Ltmp221
	.long	Lset223
	.byte	5                       ##   On action: 3
Lset224 = Ltmp220-Lfunc_begin7          ## >> Call Site 5 <<
	.long	Lset224
Lset225 = Ltmp222-Ltmp220               ##   Call between Ltmp220 and Ltmp222
	.long	Lset225
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset226 = Ltmp222-Lfunc_begin7          ## >> Call Site 6 <<
	.long	Lset226
Lset227 = Ltmp223-Ltmp222               ##   Call between Ltmp222 and Ltmp223
	.long	Lset227
Lset228 = Ltmp224-Lfunc_begin7          ##     jumps to Ltmp224
	.long	Lset228
	.byte	0                       ##   On action: cleanup
Lset229 = Ltmp223-Lfunc_begin7          ## >> Call Site 7 <<
	.long	Lset229
Lset230 = Ltmp225-Ltmp223               ##   Call between Ltmp223 and Ltmp225
	.long	Lset230
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset231 = Ltmp225-Lfunc_begin7          ## >> Call Site 8 <<
	.long	Lset231
Lset232 = Ltmp226-Ltmp225               ##   Call between Ltmp225 and Ltmp226
	.long	Lset232
Lset233 = Ltmp227-Lfunc_begin7          ##     jumps to Ltmp227
	.long	Lset233
	.byte	5                       ##   On action: 3
Lset234 = Ltmp226-Lfunc_begin7          ## >> Call Site 9 <<
	.long	Lset234
Lset235 = Lfunc_end7-Ltmp226            ##   Call between Ltmp226 and Lfunc_end7
	.long	Lset235
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp231:
	.cfi_def_cfa_offset 16
Ltmp232:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp233:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp237:
	.cfi_def_cfa_offset 16
Ltmp238:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp239:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB14_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB14_26
LBB14_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB14_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB14_5
LBB14_4:
	movq	$0, -368(%rbp)
LBB14_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB14_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB14_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB14_26
LBB14_8:
	jmp	LBB14_9
LBB14_9:
	cmpq	$0, -368(%rbp)
	jle	LBB14_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB14_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB14_13
LBB14_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB14_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp234:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp235:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB14_14
LBB14_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB14_15
LBB14_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB14_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB14_19
LBB14_17:
Ltmp236:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB14_27
LBB14_18:
	movl	$0, -416(%rbp)
LBB14_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB14_20
	jmp	LBB14_29
LBB14_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB14_26
	jmp	LBB14_28
LBB14_20:
	jmp	LBB14_21
LBB14_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB14_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB14_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB14_26
LBB14_24:
	jmp	LBB14_25
LBB14_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB14_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB14_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB14_28:
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table14:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset236 = Lfunc_begin8-Lfunc_begin8     ## >> Call Site 1 <<
	.long	Lset236
Lset237 = Ltmp234-Lfunc_begin8          ##   Call between Lfunc_begin8 and Ltmp234
	.long	Lset237
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset238 = Ltmp234-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset238
Lset239 = Ltmp235-Ltmp234               ##   Call between Ltmp234 and Ltmp235
	.long	Lset239
Lset240 = Ltmp236-Lfunc_begin8          ##     jumps to Ltmp236
	.long	Lset240
	.byte	0                       ##   On action: cleanup
Lset241 = Ltmp235-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset241
Lset242 = Lfunc_end8-Ltmp235            ##   Call between Ltmp235 and Lfunc_end8
	.long	Lset242
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp240:
	.cfi_def_cfa_offset 16
Ltmp241:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp242:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp243:
	.cfi_def_cfa_offset 16
Ltmp244:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp245:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.weak_def_can_be_hidden	__ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.align	4, 0x90
__ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE: ## @_ZSt17throw_with_nestedISt13runtime_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp246:
	.cfi_def_cfa_offset 16
Ltmp247:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp248:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$32, %eax
	movl	%eax, %ecx
	movq	%rdi, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rcx, %rdi
	callq	___cxa_allocate_exception
	movq	%rax, %rcx
	movq	-48(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rcx, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movq	%rcx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	movq	%rcx, -96(%rbp)         ## 8-byte Spill
	callq	__ZNSt13runtime_errorC2ERKS_
	movq	-96(%rbp), %rax         ## 8-byte Reload
	addq	$16, %rax
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionC2Ev
	movq	__ZTVSt8__nestedISt13runtime_errorE@GOTPCREL(%rip), %rax
	movq	%rax, %rcx
	addq	$56, %rcx
	addq	$16, %rax
	movq	-96(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, (%rsi)
	movq	%rcx, 16(%rsi)
## BB#1:
	movq	__ZTISt8__nestedISt13runtime_errorE@GOTPCREL(%rip), %rax
	movq	__ZNSt8__nestedISt13runtime_errorED1Ev@GOTPCREL(%rip), %rcx
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
	.cfi_endproc

	.globl	__ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.weak_def_can_be_hidden	__ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.align	4, 0x90
__ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE: ## @_ZSt17throw_with_nestedISt11logic_errorEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp249:
	.cfi_def_cfa_offset 16
Ltmp250:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp251:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$32, %eax
	movl	%eax, %ecx
	movq	%rdi, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rcx, %rdi
	callq	___cxa_allocate_exception
	movq	%rax, %rcx
	movq	-48(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rcx, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movq	%rcx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	movq	%rcx, -96(%rbp)         ## 8-byte Spill
	callq	__ZNSt11logic_errorC2ERKS_
	movq	-96(%rbp), %rax         ## 8-byte Reload
	addq	$16, %rax
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionC2Ev
	movq	__ZTVSt8__nestedISt11logic_errorE@GOTPCREL(%rip), %rax
	movq	%rax, %rcx
	addq	$56, %rcx
	addq	$16, %rax
	movq	-96(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, (%rsi)
	movq	%rcx, 16(%rsi)
## BB#1:
	movq	__ZTISt8__nestedISt11logic_errorE@GOTPCREL(%rip), %rax
	movq	__ZNSt8__nestedISt11logic_errorED1Ev@GOTPCREL(%rip), %rcx
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
	.cfi_endproc

	.globl	__ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.weak_def_can_be_hidden	__ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.align	4, 0x90
__ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE: ## @_ZSt17throw_with_nestedISt16invalid_argumentEvOT_PNSt3__19enable_ifIXaaaasr8is_classINS3_16remove_referenceIS1_E4typeEEE5valuentsr10is_base_ofISt16nested_exceptionS7_EE5valuentsr17__libcpp_is_finalIS7_EE5valueEvE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp252:
	.cfi_def_cfa_offset 16
Ltmp253:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp254:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$32, %eax
	movl	%eax, %ecx
	movq	%rdi, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rcx, %rdi
	callq	___cxa_allocate_exception
	movq	%rax, %rcx
	movq	-48(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rcx, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	-32(%rbp), %rsi
	movq	%rcx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	movq	%rcx, -96(%rbp)         ## 8-byte Spill
	callq	__ZNSt16invalid_argumentC2ERKS_
	movq	-96(%rbp), %rax         ## 8-byte Reload
	addq	$16, %rax
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionC2Ev
	movq	__ZTVSt8__nestedISt16invalid_argumentE@GOTPCREL(%rip), %rax
	movq	%rax, %rcx
	addq	$56, %rcx
	addq	$16, %rax
	movq	-96(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, (%rsi)
	movq	%rcx, 16(%rsi)
## BB#1:
	movq	__ZTISt8__nestedISt16invalid_argumentE@GOTPCREL(%rip), %rax
	movq	__ZNSt8__nestedISt16invalid_argumentED1Ev@GOTPCREL(%rip), %rcx
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp255:
	.cfi_def_cfa_offset 16
Ltmp256:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp257:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$112, %rsi
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp258:
	.cfi_def_cfa_offset 16
Ltmp259:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp260:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp261:
	.cfi_def_cfa_offset 16
Ltmp262:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp263:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp264:
	.cfi_def_cfa_offset 16
Ltmp265:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp266:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp267:
	.cfi_def_cfa_offset 16
Ltmp268:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp269:
	.cfi_def_cfa_register %rbp
	subq	$1312, %rsp             ## imm = 0x520
	movq	%rdi, -1064(%rbp)
	movq	%rsi, -1072(%rbp)
	movq	-1064(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$64, %rdi
	movq	-1072(%rbp), %rax
	movq	%rsi, -1088(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	-1088(%rbp), %rsi       ## 8-byte Reload
	movq	$0, 88(%rsi)
	movl	96(%rsi), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	je	LBB24_14
## BB#1:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rax
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	movq	%rcx, -1024(%rbp)
	movq	-1024(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1104(%rbp)       ## 8-byte Spill
	je	LBB24_3
## BB#2:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rcx
	movq	%rcx, -968(%rbp)
	movq	-968(%rbp), %rcx
	movq	%rcx, -960(%rbp)
	movq	-960(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
	jmp	LBB24_4
LBB24_3:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1016(%rbp)
	movq	-1016(%rbp), %rcx
	movq	%rcx, -1008(%rbp)
	movq	-1008(%rbp), %rcx
	movq	%rcx, -1000(%rbp)
	movq	-1000(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -992(%rbp)
	movq	-992(%rbp), %rcx
	movq	%rcx, -984(%rbp)
	movq	-984(%rbp), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
LBB24_4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-1112(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -584(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rdx, -560(%rbp)
	movq	-560(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1120(%rbp)       ## 8-byte Spill
	movq	%rcx, -1128(%rbp)       ## 8-byte Spill
	je	LBB24_6
## BB#5:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	movq	%rcx, -512(%rbp)
	movq	-512(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
	jmp	LBB24_7
LBB24_6:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -552(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	%rcx, -536(%rbp)
	movq	-536(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
LBB24_7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit3
	movq	-1136(%rbp), %rax       ## 8-byte Reload
	movq	-1120(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1144(%rbp)       ## 8-byte Spill
	movq	%rcx, -1152(%rbp)       ## 8-byte Spill
	je	LBB24_9
## BB#8:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
	jmp	LBB24_10
LBB24_9:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
LBB24_10:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit7
	movq	-1160(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1168(%rbp)       ## 8-byte Spill
	movq	%rcx, -1176(%rbp)       ## 8-byte Spill
	je	LBB24_12
## BB#11:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
	jmp	LBB24_13
LBB24_12:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
LBB24_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	-1184(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	88(%rcx), %rdx
	movq	-1144(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -232(%rbp)
	movq	-1168(%rbp), %rdi       ## 8-byte Reload
	movq	%rdi, -240(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-256(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB24_14:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB24_36
## BB#15:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1192(%rbp)       ## 8-byte Spill
	je	LBB24_17
## BB#16:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
	jmp	LBB24_18
LBB24_17:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
LBB24_18:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit5
	movq	-1200(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1080(%rbp)
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1208(%rbp)       ## 8-byte Spill
	je	LBB24_20
## BB#19:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
	jmp	LBB24_21
LBB24_20:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
LBB24_21:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4
	movq	-1216(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rax
	addq	-1080(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	%rax, 88(%rcx)
	addq	$64, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	-496(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -1224(%rbp)       ## 8-byte Spill
	movq	%rax, -1232(%rbp)       ## 8-byte Spill
	je	LBB24_23
## BB#22:
	movq	-1232(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB24_24
LBB24_23:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB24_24
LBB24_24:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-1240(%rbp), %rax       ## 8-byte Reload
	xorl	%edx, %edx
	subq	$1, %rax
	movq	-1224(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movq	-592(%rbp), %rdi
	movq	-600(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -712(%rbp)
	movq	-712(%rbp), %rcx
	movq	%rcx, -704(%rbp)
	movq	-704(%rbp), %rcx
	movq	%rcx, -696(%rbp)
	movq	-696(%rbp), %rsi
	movq	%rsi, -688(%rbp)
	movq	-688(%rbp), %rsi
	movq	%rsi, -680(%rbp)
	movq	-680(%rbp), %rsi
	movzbl	(%rsi), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1248(%rbp)       ## 8-byte Spill
	movq	%rcx, -1256(%rbp)       ## 8-byte Spill
	je	LBB24_26
## BB#25:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -632(%rbp)
	movq	-632(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-624(%rbp), %rcx
	movq	%rcx, -616(%rbp)
	movq	-616(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
	jmp	LBB24_27
LBB24_26:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -672(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	-664(%rbp), %rcx
	movq	%rcx, -656(%rbp)
	movq	-656(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -648(%rbp)
	movq	-648(%rbp), %rcx
	movq	%rcx, -640(%rbp)
	movq	-640(%rbp), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
LBB24_27:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-1264(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -824(%rbp)
	movq	-824(%rbp), %rcx
	movq	%rcx, -816(%rbp)
	movq	-816(%rbp), %rcx
	movq	%rcx, -808(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	-800(%rbp), %rdx
	movq	%rdx, -792(%rbp)
	movq	-792(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1272(%rbp)       ## 8-byte Spill
	movq	%rcx, -1280(%rbp)       ## 8-byte Spill
	je	LBB24_29
## BB#28:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -744(%rbp)
	movq	-744(%rbp), %rcx
	movq	%rcx, -736(%rbp)
	movq	-736(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	-728(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
	jmp	LBB24_30
LBB24_29:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -776(%rbp)
	movq	-776(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	-768(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -760(%rbp)
	movq	-760(%rbp), %rcx
	movq	%rcx, -752(%rbp)
	movq	-752(%rbp), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
LBB24_30:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-1288(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -720(%rbp)
	movq	-720(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -904(%rbp)
	movq	-904(%rbp), %rcx
	movq	%rcx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1296(%rbp)       ## 8-byte Spill
	movq	%rcx, -1304(%rbp)       ## 8-byte Spill
	je	LBB24_32
## BB#31:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movq	-848(%rbp), %rcx
	movq	%rcx, -840(%rbp)
	movq	-840(%rbp), %rcx
	movq	%rcx, -832(%rbp)
	movq	-832(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
	jmp	LBB24_33
LBB24_32:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -872(%rbp)
	movq	-872(%rbp), %rcx
	movq	%rcx, -864(%rbp)
	movq	-864(%rbp), %rcx
	movq	%rcx, -856(%rbp)
	movq	-856(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
LBB24_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-1312(%rbp), %rax       ## 8-byte Reload
	movq	-1296(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1248(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -912(%rbp)
	movq	-1272(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -920(%rbp)
	movq	%rcx, -928(%rbp)
	movq	-912(%rbp), %rcx
	movq	-920(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-928(%rbp), %rsi
	movq	%rsi, 56(%rcx)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movl	96(%rcx), %edi
	andl	$3, %edi
	cmpl	$0, %edi
	je	LBB24_35
## BB#34:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1080(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -936(%rbp)
	movl	%edx, -940(%rbp)
	movq	-936(%rbp), %rax
	movl	-940(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
LBB24_35:
	jmp	LBB24_36
LBB24_36:
	addq	$1312, %rsp             ## imm = 0x520
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp270:
	.cfi_def_cfa_offset 16
Ltmp271:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp272:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp273:
	.cfi_def_cfa_offset 16
Ltmp274:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp275:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp276:
	.cfi_def_cfa_offset 16
Ltmp277:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp278:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movl	%ecx, -636(%rbp)
	movl	%r8d, -640(%rbp)
	movq	-624(%rbp), %rdx
	movq	88(%rdx), %rsi
	movq	%rdx, %r9
	movq	%r9, -616(%rbp)
	movq	-616(%rbp), %r9
	cmpq	48(%r9), %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdi, -664(%rbp)        ## 8-byte Spill
	movq	%rdx, -672(%rbp)        ## 8-byte Spill
	jae	LBB27_2
## BB#1:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB27_2:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$0, %eax
	jne	LBB27_4
## BB#3:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	$-1, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %r8
	movq	%rdi, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-24(%rbp), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_4:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$24, %eax
	jne	LBB27_7
## BB#5:
	cmpl	$1, -636(%rbp)
	jne	LBB27_7
## BB#6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	$-1, -72(%rbp)
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r8
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-56(%rbp), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_7:
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -692(%rbp)        ## 4-byte Spill
	je	LBB27_8
	jmp	LBB27_38
LBB27_38:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -696(%rbp)        ## 4-byte Spill
	je	LBB27_9
	jmp	LBB27_39
LBB27_39:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -700(%rbp)        ## 4-byte Spill
	je	LBB27_13
	jmp	LBB27_17
LBB27_8:
	movq	$0, -648(%rbp)
	jmp	LBB27_18
LBB27_9:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_11
## BB#10:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	24(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	jmp	LBB27_12
LBB27_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	40(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
LBB27_12:
	jmp	LBB27_18
LBB27_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -712(%rbp)        ## 8-byte Spill
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	je	LBB27_15
## BB#14:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB27_16
LBB27_15:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB27_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, -648(%rbp)
	jmp	LBB27_18
LBB27_17:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	$-1, -248(%rbp)
	movq	-240(%rbp), %rdi
	movq	-248(%rbp), %r8
	movq	%rdi, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -736(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-232(%rbp), %rcx
	movq	-736(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_18:
	movq	-632(%rbp), %rax
	addq	-648(%rbp), %rax
	movq	%rax, -648(%rbp)
	cmpq	$0, -648(%rbp)
	jl	LBB27_23
## BB#19:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	je	LBB27_21
## BB#20:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
	jmp	LBB27_22
LBB27_21:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
LBB27_22:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	-744(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	cmpq	-648(%rbp), %rcx
	jge	LBB27_24
LBB27_23:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -384(%rbp)
	movq	$-1, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	-392(%rbp), %r8
	movq	%rdi, -368(%rbp)
	movq	%r8, -376(%rbp)
	movq	-368(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-376(%rbp), %rcx
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_24:
	cmpq	$0, -648(%rbp)
	je	LBB27_32
## BB#25:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_28
## BB#26:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	-400(%rbp), %rax
	cmpq	$0, 24(%rax)
	jne	LBB27_28
## BB#27:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-776(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_28:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB27_31
## BB#29:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB27_31
## BB#30:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -464(%rbp)
	movq	$-1, -472(%rbp)
	movq	-464(%rbp), %rdi
	movq	-472(%rbp), %r8
	movq	%rdi, -448(%rbp)
	movq	%r8, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -784(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-456(%rbp), %rcx
	movq	-784(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_31:
	jmp	LBB27_32
LBB27_32:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_34
## BB#33:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-648(%rbp), %rdx
	movq	-672(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -496(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdi, -520(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-520(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB27_34:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB27_36
## BB#35:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -536(%rbp)
	movq	-536(%rbp), %rdx
	movq	56(%rdx), %rdx
	movq	%rax, -544(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-560(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-648(%rbp), %rcx
	movl	%ecx, %esi
	movq	%rax, -568(%rbp)
	movl	%esi, -572(%rbp)
	movq	-568(%rbp), %rax
	movl	-572(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB27_36:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-648(%rbp), %rcx
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	-600(%rbp), %rcx
	movq	-608(%rbp), %r8
	movq	%rcx, -584(%rbp)
	movq	%r8, -592(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, %rdi
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-592(%rbp), %rcx
	movq	-792(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
LBB27_37:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-16(%rbp), %rsi
	movq	(%rsi), %r9
	movq	32(%r9), %r9
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	128(%rcx), %rdx
	movl	-20(%rbp), %r10d
	movl	%r8d, %ecx
	movl	%r10d, %r8d
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	*%r9
	movq	-32(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp282:
	.cfi_def_cfa_offset 16
Ltmp283:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp284:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jae	LBB29_2
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	48(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB29_2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB29_8
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	cmpq	88(%rcx), %rax
	jae	LBB29_5
## BB#4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-120(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	24(%rdx), %rdx
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-40(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB29_5:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	24(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	32(%rcx), %rax
	jae	LBB29_7
## BB#6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -100(%rbp)
	jmp	LBB29_9
LBB29_7:
	jmp	LBB29_8
LBB29_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -100(%rbp)
LBB29_9:
	movl	-100(%rbp), %eax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp285:
	.cfi_def_cfa_offset 16
Ltmp286:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp287:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -160(%rbp)
	movl	%esi, -164(%rbp)
	movq	-160(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -176(%rbp)        ## 8-byte Spill
	jae	LBB30_2
## BB#1:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	48(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB30_2:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB30_9
## BB#3:
	movl	-164(%rbp), %edi
	movl	%edi, -180(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-180(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB30_4
	jmp	LBB30_5
LBB30_4:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-32(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -148(%rbp)
	jmp	LBB30_10
LBB30_5:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB30_7
## BB#6:
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB30_7
	jmp	LBB30_8
LBB30_7:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movb	%al, (%rcx)
	movl	-164(%rbp), %edi
	movl	%edi, -148(%rbp)
	jmp	LBB30_10
LBB30_8:
	jmp	LBB30_9
LBB30_9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -148(%rbp)
LBB30_10:
	movl	-148(%rbp), %eax
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp293:
	.cfi_def_cfa_offset 16
Ltmp294:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp295:
	.cfi_def_cfa_register %rbp
	subq	$896, %rsp              ## imm = 0x380
	movq	%rdi, -632(%rbp)
	movl	%esi, -636(%rbp)
	movq	-632(%rbp), %rdi
	movl	-636(%rbp), %esi
	movq	%rdi, -712(%rbp)        ## 8-byte Spill
	movl	%esi, -716(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-716(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB31_38
## BB#1:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -616(%rbp)
	movq	-616(%rbp), %rax
	movq	24(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -608(%rbp)
	movq	-608(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -576(%rbp)
	movq	-576(%rbp), %rax
	movq	48(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -568(%rbp)
	movq	-568(%rbp), %rcx
	cmpq	56(%rcx), %rax
	jne	LBB31_26
## BB#2:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB31_4
## BB#3:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	jmp	LBB31_39
LBB31_4:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
## BB#6:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	-736(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -656(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rdx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
## BB#7:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	-752(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -680(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
Ltmp288:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp289:
	jmp	LBB31_8
LBB31_8:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	movq	%rcx, -768(%rbp)        ## 8-byte Spill
	je	LBB31_10
## BB#9:
	movq	-768(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB31_11
LBB31_10:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB31_11
LBB31_11:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-776(%rbp), %rax        ## 8-byte Reload
	decq	%rax
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rdi
Ltmp290:
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp291:
	jmp	LBB31_12
LBB31_12:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	jmp	LBB31_13
LBB31_13:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	je	LBB31_15
## BB#14:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	jmp	LBB31_16
LBB31_15:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
LBB31_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-688(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	addq	$64, %rsi
	movq	%rsi, -272(%rbp)
	movq	-272(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movq	-264(%rbp), %rdi
	movq	%rdi, -256(%rbp)
	movq	-256(%rbp), %rdi
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	movzbl	(%rdi), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rcx, -808(%rbp)        ## 8-byte Spill
	movq	%rdx, -816(%rbp)        ## 8-byte Spill
	movq	%rsi, -824(%rbp)        ## 8-byte Spill
	je	LBB31_18
## BB#17:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
	jmp	LBB31_19
LBB31_18:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
LBB31_19:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-832(%rbp), %rax        ## 8-byte Reload
	movq	-816(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-800(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-808(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-296(%rbp), %rsi
	movq	%rsi, 56(%rcx)
## BB#20:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-656(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -304(%rbp)
	movl	%edx, -308(%rbp)
	movq	-304(%rbp), %rax
	movl	-308(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
## BB#21:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -840(%rbp)        ## 8-byte Spill
## BB#22:
	movq	-840(%rbp), %rax        ## 8-byte Reload
	addq	-680(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	jmp	LBB31_25
LBB31_23:
Ltmp292:
	movl	%edx, %ecx
	movq	%rax, -664(%rbp)
	movl	%ecx, -668(%rbp)
## BB#24:
	movq	-664(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -848(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	callq	___cxa_end_catch
	jmp	LBB31_39
LBB31_25:
	jmp	LBB31_26
LBB31_26:
	leaq	-368(%rbp), %rax
	leaq	-696(%rbp), %rcx
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	48(%rdx), %rdx
	addq	$1, %rdx
	movq	%rdx, -696(%rbp)
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	addq	$88, %rdx
	movq	%rcx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rax, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	-360(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB31_28
## BB#27:
	movq	-384(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	jmp	LBB31_29
LBB31_28:
	movq	-376(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
LBB31_29:                               ## %_ZNSt3__13maxIPcEERKT_S4_S4_.exit
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	movl	96(%rcx), %edx
	andl	$8, %edx
	cmpl	$0, %edx
	je	LBB31_34
## BB#30:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -520(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	-496(%rbp), %rcx
	movq	%rcx, -488(%rbp)
	movq	-488(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	je	LBB31_32
## BB#31:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
	jmp	LBB31_33
LBB31_32:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
LBB31_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-872(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -704(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rcx
	movq	-704(%rbp), %rdx
	addq	-648(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -528(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	movq	%rdi, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB31_34:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	-636(%rbp), %ecx
	movb	%cl, %dl
	movq	%rax, -592(%rbp)
	movb	%dl, -593(%rbp)
	movq	-592(%rbp), %rax
	movq	48(%rax), %rsi
	cmpq	56(%rax), %rsi
	movq	%rax, -880(%rbp)        ## 8-byte Spill
	jne	LBB31_36
## BB#35:
	movq	-880(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movsbl	-593(%rbp), %edi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movq	-880(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -580(%rbp)
	jmp	LBB31_37
LBB31_36:
	movb	-593(%rbp), %al
	movq	-880(%rbp), %rcx        ## 8-byte Reload
	movq	48(%rcx), %rdx
	movq	%rdx, %rsi
	addq	$1, %rsi
	movq	%rsi, 48(%rcx)
	movb	%al, (%rdx)
	movsbl	-593(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -580(%rbp)
LBB31_37:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	-580(%rbp), %eax
	movl	%eax, -620(%rbp)
	jmp	LBB31_39
LBB31_38:
	movl	-636(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -620(%rbp)
LBB31_39:
	movl	-620(%rbp), %eax
	addq	$896, %rsp              ## imm = 0x380
	popq	%rbp
	retq
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table31:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset243 = Ltmp288-Lfunc_begin9          ## >> Call Site 1 <<
	.long	Lset243
Lset244 = Ltmp291-Ltmp288               ##   Call between Ltmp288 and Ltmp291
	.long	Lset244
Lset245 = Ltmp292-Lfunc_begin9          ##     jumps to Ltmp292
	.long	Lset245
	.byte	1                       ##   On action: 1
Lset246 = Ltmp291-Lfunc_begin9          ## >> Call Site 2 <<
	.long	Lset246
Lset247 = Lfunc_end9-Ltmp291            ##   Call between Ltmp291 and Lfunc_end9
	.long	Lset247
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp296:
	.cfi_def_cfa_offset 16
Ltmp297:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp298:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$64, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp299:
	.cfi_def_cfa_offset 16
Ltmp300:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp301:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp302:
	.cfi_def_cfa_offset 16
Ltmp303:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp304:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB34_1
	jmp	LBB34_2
LBB34_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB34_3
LBB34_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB34_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp305:
	.cfi_def_cfa_offset 16
Ltmp306:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp307:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp308:
	.cfi_def_cfa_offset 16
Ltmp309:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp310:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt13runtime_errorED1Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt13runtime_errorED1Ev
	.align	4, 0x90
__ZNSt8__nestedISt13runtime_errorED1Ev: ## @_ZNSt8__nestedISt13runtime_errorED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp311:
	.cfi_def_cfa_offset 16
Ltmp312:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp313:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt8__nestedISt13runtime_errorED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt13runtime_errorED0Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt13runtime_errorED0Ev
	.align	4, 0x90
__ZNSt8__nestedISt13runtime_errorED0Ev: ## @_ZNSt8__nestedISt13runtime_errorED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp314:
	.cfi_def_cfa_offset 16
Ltmp315:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp316:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt8__nestedISt13runtime_errorED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt13runtime_errorED1Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt13runtime_errorED1Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt13runtime_errorED1Ev: ## @_ZThn16_NSt8__nestedISt13runtime_errorED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp317:
	.cfi_def_cfa_offset 16
Ltmp318:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp319:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt13runtime_errorED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt13runtime_errorED0Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt13runtime_errorED0Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt13runtime_errorED0Ev: ## @_ZThn16_NSt8__nestedISt13runtime_errorED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp320:
	.cfi_def_cfa_offset 16
Ltmp321:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp322:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt13runtime_errorED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt8__nestedISt13runtime_errorED2Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt13runtime_errorED2Ev
	.align	4, 0x90
__ZNSt8__nestedISt13runtime_errorED2Ev: ## @_ZNSt8__nestedISt13runtime_errorED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp323:
	.cfi_def_cfa_offset 16
Ltmp324:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp325:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$16, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionD2Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.weak_def_can_be_hidden	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.align	4, 0x90
__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv: ## @_ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp326:
	.cfi_def_cfa_offset 16
Ltmp327:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp328:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$712, %rsp              ## imm = 0x2C8
Ltmp329:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movl	96(%rsi), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	movq	%rax, -672(%rbp)        ## 8-byte Spill
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%rsi, -688(%rbp)        ## 8-byte Spill
	je	LBB42_4
## BB#1:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	cmpq	48(%rax), %rcx
	jae	LBB42_3
## BB#2:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	48(%rax), %rax
	movq	-688(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB42_3:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-72(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	leaq	-624(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -56(%rbp)
	movq	-56(%rbp), %r9
	movq	40(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	88(%r10), %r11
	addq	$64, %r10
	movq	%r10, -48(%rbp)
	movq	-48(%rbp), %r10
	movq	%r10, -32(%rbp)
	movq	-32(%rbp), %r10
	movq	%r10, -24(%rbp)
	movq	-24(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-680(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -176(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	-200(%rbp), %rbx
	movq	%r8, -136(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r11, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movq	-136(%rbp), %r8
	movq	%r8, -128(%rbp)
	movq	-128(%rbp), %r9
	movq	%r9, -104(%rbp)
	movq	-104(%rbp), %r9
	movq	%rdi, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -696(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-144(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	-696(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB42_11
LBB42_4:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB42_6
## BB#5:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-280(%rbp), %rcx
	leaq	-304(%rbp), %rdi
	leaq	-640(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	16(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	32(%r10), %r10
	movq	-688(%rbp), %r11        ## 8-byte Reload
	addq	$64, %r11
	movq	%r11, -256(%rbp)
	movq	-256(%rbp), %r11
	movq	%r11, -240(%rbp)
	movq	-240(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r11, -224(%rbp)
	movq	-680(%rbp), %r11        ## 8-byte Reload
	movq	%r11, -384(%rbp)
	movq	%r9, -392(%rbp)
	movq	%r10, -400(%rbp)
	movq	%r8, -408(%rbp)
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %r9
	movq	-400(%rbp), %r10
	movq	-408(%rbp), %rbx
	movq	%r8, -344(%rbp)
	movq	%r9, -352(%rbp)
	movq	%r10, -360(%rbp)
	movq	%rbx, -368(%rbp)
	movq	-344(%rbp), %r8
	movq	%r8, -336(%rbp)
	movq	-336(%rbp), %r9
	movq	%r9, -312(%rbp)
	movq	-312(%rbp), %r9
	movq	%rdi, -296(%rbp)
	movq	%r9, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rcx, -272(%rbp)
	movq	%r8, -704(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-352(%rbp), %rsi
	movq	-360(%rbp), %rdx
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB42_11
LBB42_6:
	jmp	LBB42_7
LBB42_7:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-504(%rbp), %rcx
	leaq	-528(%rbp), %rdi
	leaq	-656(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	addq	$64, %r9
	movq	%r9, -448(%rbp)
	movq	-448(%rbp), %r9
	movq	%r9, -432(%rbp)
	movq	-432(%rbp), %r9
	movq	%r9, -424(%rbp)
	movq	-424(%rbp), %r9
	movq	%r9, -416(%rbp)
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -592(%rbp)
	movq	%r8, -600(%rbp)
	movq	-592(%rbp), %r8
	movq	-600(%rbp), %r10
	movq	%r8, -568(%rbp)
	movq	%r10, -576(%rbp)
	movq	-568(%rbp), %r8
	movq	%r8, -560(%rbp)
	movq	-560(%rbp), %r10
	movq	%r10, -536(%rbp)
	movq	-536(%rbp), %r10
	movq	%rdi, -520(%rbp)
	movq	%r10, -512(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rcx, -496(%rbp)
	movq	%r8, -712(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -464(%rbp)
	movq	-464(%rbp), %rdx
	movq	%rdx, -456(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movl	$0, -484(%rbp)
LBB42_8:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -484(%rbp)
	jae	LBB42_10
## BB#9:                                ##   in Loop: Header=BB42_8 Depth=1
	movl	-484(%rbp), %eax
	movl	%eax, %ecx
	movq	-480(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-484(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -484(%rbp)
	jmp	LBB42_8
LBB42_10:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS4_.exit
	jmp	LBB42_11
LBB42_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	addq	$712, %rsp              ## imm = 0x2C8
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.weak_def_can_be_hidden	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.align	4, 0x90
__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_: ## @_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp330:
	.cfi_def_cfa_offset 16
Ltmp331:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp332:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -392(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	-392(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movq	-408(%rbp), %rdi
	movq	%rsi, -368(%rbp)
	movq	%rdi, -376(%rbp)
	movq	-368(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%rsi, -352(%rbp)
	movq	%rdi, -360(%rbp)
	movq	-360(%rbp), %rsi
	movq	-352(%rbp), %rdi
	subq	%rdi, %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rdi, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -264(%rbp)
	movq	$-1, -344(%rbp)
	movq	-344(%rbp), %rdi
	subq	$16, %rdi
	cmpq	%rdi, %rsi
	movq	%rdx, -448(%rbp)        ## 8-byte Spill
	jbe	LBB43_2
## BB#1:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__121__basic_string_commonILb1EE20__throw_length_errorEv
LBB43_2:
	cmpq	$23, -416(%rbp)
	jae	LBB43_4
## BB#3:
	movq	-416(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdx
	shlq	$1, %rdx
	movb	%dl, %sil
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rax
	movb	%sil, (%rax)
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -424(%rbp)
	jmp	LBB43_8
LBB43_4:
	movq	-416(%rbp), %rax
	movq	%rax, -16(%rbp)
	cmpq	$23, -16(%rbp)
	jae	LBB43_6
## BB#5:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	jmp	LBB43_7
LBB43_6:
	movq	-16(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$15, %rax
	andq	$-16, %rax
	movq	%rax, -456(%rbp)        ## 8-byte Spill
LBB43_7:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm.exit
	movq	-456(%rbp), %rax        ## 8-byte Reload
	subq	$1, %rax
	movq	%rax, -432(%rbp)
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	-432(%rbp), %rdx
	addq	$1, %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-432(%rbp), %rax
	addq	$1, %rax
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rdx
	orq	$1, %rdx
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-416(%rbp), %rax
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rdx, 8(%rax)
LBB43_8:
	jmp	LBB43_9
LBB43_9:                                ## =>This Inner Loop Header: Depth=1
	movq	-400(%rbp), %rax
	cmpq	-408(%rbp), %rax
	je	LBB43_12
## BB#10:                               ##   in Loop: Header=BB43_9 Depth=1
	movq	-424(%rbp), %rdi
	movq	-400(%rbp), %rsi
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
## BB#11:                               ##   in Loop: Header=BB43_9 Depth=1
	movq	-400(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -400(%rbp)
	movq	-424(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -424(%rbp)
	jmp	LBB43_9
LBB43_12:
	leaq	-433(%rbp), %rsi
	movq	-424(%rbp), %rdi
	movb	$0, -433(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6assignERcRKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6assignERcRKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6assignERcRKc: ## @_ZNSt3__111char_traitsIcE6assignERcRKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp333:
	.cfi_def_cfa_offset 16
Ltmp334:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp335:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movb	(%rsi), %al
	movq	-8(%rbp), %rsi
	movb	%al, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt11logic_errorED1Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt11logic_errorED1Ev
	.align	4, 0x90
__ZNSt8__nestedISt11logic_errorED1Ev:   ## @_ZNSt8__nestedISt11logic_errorED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp336:
	.cfi_def_cfa_offset 16
Ltmp337:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp338:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt8__nestedISt11logic_errorED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt11logic_errorED0Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt11logic_errorED0Ev
	.align	4, 0x90
__ZNSt8__nestedISt11logic_errorED0Ev:   ## @_ZNSt8__nestedISt11logic_errorED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp339:
	.cfi_def_cfa_offset 16
Ltmp340:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp341:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt8__nestedISt11logic_errorED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt11logic_errorED1Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt11logic_errorED1Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt11logic_errorED1Ev: ## @_ZThn16_NSt8__nestedISt11logic_errorED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp342:
	.cfi_def_cfa_offset 16
Ltmp343:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp344:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt11logic_errorED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt11logic_errorED0Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt11logic_errorED0Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt11logic_errorED0Ev: ## @_ZThn16_NSt8__nestedISt11logic_errorED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp345:
	.cfi_def_cfa_offset 16
Ltmp346:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp347:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt11logic_errorED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt8__nestedISt11logic_errorED2Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt11logic_errorED2Ev
	.align	4, 0x90
__ZNSt8__nestedISt11logic_errorED2Ev:   ## @_ZNSt8__nestedISt11logic_errorED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp348:
	.cfi_def_cfa_offset 16
Ltmp349:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp350:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$16, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionD2Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt11logic_errorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt16invalid_argumentED1Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt16invalid_argumentED1Ev
	.align	4, 0x90
__ZNSt8__nestedISt16invalid_argumentED1Ev: ## @_ZNSt8__nestedISt16invalid_argumentED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp351:
	.cfi_def_cfa_offset 16
Ltmp352:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp353:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt8__nestedISt16invalid_argumentED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt16invalid_argumentC2ERKS_
	.weak_def_can_be_hidden	__ZNSt16invalid_argumentC2ERKS_
	.align	4, 0x90
__ZNSt16invalid_argumentC2ERKS_:        ## @_ZNSt16invalid_argumentC2ERKS_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp354:
	.cfi_def_cfa_offset 16
Ltmp355:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp356:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rsi, %rdi
	movq	-16(%rbp), %rax
	movq	%rsi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt11logic_errorC2ERKS_
	movq	__ZTVSt16invalid_argument@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-24(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, (%rsi)
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt8__nestedISt16invalid_argumentED0Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt16invalid_argumentED0Ev
	.align	4, 0x90
__ZNSt8__nestedISt16invalid_argumentED0Ev: ## @_ZNSt8__nestedISt16invalid_argumentED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp357:
	.cfi_def_cfa_offset 16
Ltmp358:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp359:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt8__nestedISt16invalid_argumentED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt16invalid_argumentED1Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt16invalid_argumentED1Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt16invalid_argumentED1Ev: ## @_ZThn16_NSt8__nestedISt16invalid_argumentED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp360:
	.cfi_def_cfa_offset 16
Ltmp361:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp362:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt16invalid_argumentED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZThn16_NSt8__nestedISt16invalid_argumentED0Ev
	.weak_def_can_be_hidden	__ZThn16_NSt8__nestedISt16invalid_argumentED0Ev
	.align	4, 0x90
__ZThn16_NSt8__nestedISt16invalid_argumentED0Ev: ## @_ZThn16_NSt8__nestedISt16invalid_argumentED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp363:
	.cfi_def_cfa_offset 16
Ltmp364:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp365:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-16, %rdi
	popq	%rbp
	jmp	__ZNSt8__nestedISt16invalid_argumentED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt8__nestedISt16invalid_argumentED2Ev
	.weak_def_can_be_hidden	__ZNSt8__nestedISt16invalid_argumentED2Ev
	.align	4, 0x90
__ZNSt8__nestedISt16invalid_argumentED2Ev: ## @_ZNSt8__nestedISt16invalid_argumentED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp366:
	.cfi_def_cfa_offset 16
Ltmp367:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp368:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$16, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt16nested_exceptionD2Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt16invalid_argumentD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp369:
	.cfi_def_cfa_offset 16
Ltmp370:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp371:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$8, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"exception: "

L_.str.1:                               ## @.str.1
	.asciz	"too long"

L___func__._Z12really_innerm:           ## @__func__._Z12really_innerm
	.asciz	"really_inner"

L___func__._Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @__func__._Z5innerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.asciz	"inner"

L___func__._Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @__func__._Z5outerRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.asciz	"outer"

L_.str.2:                               ## @.str.2
	.asciz	"xyz"

L_.str.3:                               ## @.str.3
	.asciz	"abcd"

L_.str.4:                               ## @.str.4
	.asciz	" : "

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE ## @_ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSSt8__nestedISt13runtime_errorE ## @_ZTSSt8__nestedISt13runtime_errorE
	.weak_definition	__ZTSSt8__nestedISt13runtime_errorE
	.align	4
__ZTSSt8__nestedISt13runtime_errorE:
	.asciz	"St8__nestedISt13runtime_errorE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTISt8__nestedISt13runtime_errorE ## @_ZTISt8__nestedISt13runtime_errorE
	.weak_definition	__ZTISt8__nestedISt13runtime_errorE
	.align	4
__ZTISt8__nestedISt13runtime_errorE:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTSSt8__nestedISt13runtime_errorE
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTISt13runtime_error
	.quad	2                       ## 0x2
	.quad	__ZTISt16nested_exception
	.quad	4098                    ## 0x1002

	.globl	__ZTVSt8__nestedISt13runtime_errorE ## @_ZTVSt8__nestedISt13runtime_errorE
	.weak_def_can_be_hidden	__ZTVSt8__nestedISt13runtime_errorE
	.align	3
__ZTVSt8__nestedISt13runtime_errorE:
	.quad	0
	.quad	__ZTISt8__nestedISt13runtime_errorE
	.quad	__ZNSt8__nestedISt13runtime_errorED1Ev
	.quad	__ZNSt8__nestedISt13runtime_errorED0Ev
	.quad	__ZNKSt13runtime_error4whatEv
	.quad	-16
	.quad	__ZTISt8__nestedISt13runtime_errorE
	.quad	__ZThn16_NSt8__nestedISt13runtime_errorED1Ev
	.quad	__ZThn16_NSt8__nestedISt13runtime_errorED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSSt8__nestedISt11logic_errorE ## @_ZTSSt8__nestedISt11logic_errorE
	.weak_definition	__ZTSSt8__nestedISt11logic_errorE
	.align	4
__ZTSSt8__nestedISt11logic_errorE:
	.asciz	"St8__nestedISt11logic_errorE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTISt8__nestedISt11logic_errorE ## @_ZTISt8__nestedISt11logic_errorE
	.weak_definition	__ZTISt8__nestedISt11logic_errorE
	.align	4
__ZTISt8__nestedISt11logic_errorE:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTSSt8__nestedISt11logic_errorE
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTISt11logic_error
	.quad	2                       ## 0x2
	.quad	__ZTISt16nested_exception
	.quad	4098                    ## 0x1002

	.globl	__ZTVSt8__nestedISt11logic_errorE ## @_ZTVSt8__nestedISt11logic_errorE
	.weak_def_can_be_hidden	__ZTVSt8__nestedISt11logic_errorE
	.align	3
__ZTVSt8__nestedISt11logic_errorE:
	.quad	0
	.quad	__ZTISt8__nestedISt11logic_errorE
	.quad	__ZNSt8__nestedISt11logic_errorED1Ev
	.quad	__ZNSt8__nestedISt11logic_errorED0Ev
	.quad	__ZNKSt11logic_error4whatEv
	.quad	-16
	.quad	__ZTISt8__nestedISt11logic_errorE
	.quad	__ZThn16_NSt8__nestedISt11logic_errorED1Ev
	.quad	__ZThn16_NSt8__nestedISt11logic_errorED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSSt8__nestedISt16invalid_argumentE ## @_ZTSSt8__nestedISt16invalid_argumentE
	.weak_definition	__ZTSSt8__nestedISt16invalid_argumentE
	.align	4
__ZTSSt8__nestedISt16invalid_argumentE:
	.asciz	"St8__nestedISt16invalid_argumentE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTISt8__nestedISt16invalid_argumentE ## @_ZTISt8__nestedISt16invalid_argumentE
	.weak_definition	__ZTISt8__nestedISt16invalid_argumentE
	.align	4
__ZTISt8__nestedISt16invalid_argumentE:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTSSt8__nestedISt16invalid_argumentE
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTISt16invalid_argument
	.quad	2                       ## 0x2
	.quad	__ZTISt16nested_exception
	.quad	4098                    ## 0x1002

	.globl	__ZTVSt8__nestedISt16invalid_argumentE ## @_ZTVSt8__nestedISt16invalid_argumentE
	.weak_def_can_be_hidden	__ZTVSt8__nestedISt16invalid_argumentE
	.align	3
__ZTVSt8__nestedISt16invalid_argumentE:
	.quad	0
	.quad	__ZTISt8__nestedISt16invalid_argumentE
	.quad	__ZNSt8__nestedISt16invalid_argumentED1Ev
	.quad	__ZNSt8__nestedISt16invalid_argumentED0Ev
	.quad	__ZNKSt11logic_error4whatEv
	.quad	-16
	.quad	__ZTISt8__nestedISt16invalid_argumentE
	.quad	__ZThn16_NSt8__nestedISt16invalid_argumentED1Ev
	.quad	__ZThn16_NSt8__nestedISt16invalid_argumentED0Ev

	.section	__TEXT,__cstring,cstring_literals
L_.str.5:                               ## @.str.5
	.asciz	", "


.subsections_via_symbols
