	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	subq	$912, %rsp              ## imm = 0x390
	leaq	-664(%rbp), %rax
	leaq	-592(%rbp), %rcx
	leaq	-616(%rbp), %rdx
	leaq	-752(%rbp), %r8
	leaq	l_.ref.tmp(%rip), %r9
	movl	%edi, -716(%rbp)
	movq	%rsi, -728(%rbp)
	movq	%r9, -768(%rbp)
	movq	$6, -760(%rbp)
	movq	-768(%rbp), %rsi
	movq	-760(%rbp), %r9
	movq	%rsi, -704(%rbp)
	movq	%r9, -696(%rbp)
	movq	%r8, -712(%rbp)
	movq	-712(%rbp), %rsi
	movq	-704(%rbp), %r8
	movq	-696(%rbp), %r9
	movq	%r8, -664(%rbp)
	movq	%r9, -656(%rbp)
	movq	%rsi, -672(%rbp)
	movq	-672(%rbp), %rsi
	movq	%rsi, %r8
	movq	%r8, -648(%rbp)
	movq	-648(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -640(%rbp)
	movq	$0, (%r8)
	movq	$0, 8(%r8)
	addq	$16, %r8
	movq	%r8, -624(%rbp)
	movq	$0, -632(%rbp)
	movq	-624(%rbp), %r8
	movq	-632(%rbp), %r9
	movq	%r8, -608(%rbp)
	movq	%r9, -616(%rbp)
	movq	-608(%rbp), %r8
	movq	%rdx, -600(%rbp)
	movq	-600(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%r8, -584(%rbp)
	movq	%rdx, -592(%rbp)
	movq	-584(%rbp), %rdx
	movq	%rdx, %r8
	movq	%r8, -576(%rbp)
	movq	%rcx, -568(%rbp)
	movq	-568(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	cmpq	$0, 8(%rax)
	movq	%rsi, -888(%rbp)        ## 8-byte Spill
	jbe	LBB0_5
## BB#1:
	leaq	-664(%rbp), %rax
	movq	%rax, -552(%rbp)
	movq	-656(%rbp), %rsi
Ltmp0:
	movq	-888(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
Ltmp1:
	jmp	LBB0_2
LBB0_2:
	leaq	-664(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	-664(%rbp), %rsi
	movq	%rax, -536(%rbp)
	movq	-664(%rbp), %rcx
	movq	-656(%rbp), %rdx
	leaq	(%rcx,%rdx,4), %rdx
	movq	%rax, -544(%rbp)
	movq	-656(%rbp), %rcx
Ltmp2:
	movq	-888(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
Ltmp3:
	jmp	LBB0_3
LBB0_3:
	jmp	LBB0_5
LBB0_4:
Ltmp4:
	movl	%edx, %ecx
	movq	%rax, -680(%rbp)
	movl	%ecx, -684(%rbp)
	movq	-888(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	movq	-680(%rbp), %rax
	movq	%rax, -896(%rbp)        ## 8-byte Spill
	jmp	LBB0_23
LBB0_5:                                 ## %_ZNSt3__16vectorIiNS_9allocatorIiEEEC1ESt16initializer_listIiE.exit
Ltmp5:
	leaq	-792(%rbp), %rdi
	leaq	-752(%rbp), %rsi
	leaq	-800(%rbp), %rdx
	callq	__Z5whereINSt3__16vectorIiNS0_9allocatorIiEEEEZ4mainE3$_0EDaRT_OT0_
Ltmp6:
	jmp	LBB0_6
LBB0_6:
	leaq	-416(%rbp), %rax
	leaq	-792(%rbp), %rcx
	leaq	-488(%rbp), %rdx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rsi, -496(%rbp)
	movq	%rdi, -504(%rbp)
	movq	-504(%rbp), %rsi
	movq	%rdx, -472(%rbp)
	movq	%rsi, -480(%rbp)
	movq	-472(%rbp), %rdx
	movq	-480(%rbp), %rsi
	movq	%rdx, -456(%rbp)
	movq	%rsi, -464(%rbp)
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-488(%rbp), %rdx
	movq	%rdx, -512(%rbp)
	movq	-512(%rbp), %rdx
	movq	%rdx, -824(%rbp)
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -424(%rbp)
	movq	%rdx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rax, -400(%rbp)
	movq	%rcx, -408(%rbp)
	movq	-400(%rbp), %rax
	movq	-408(%rbp), %rcx
	movq	%rax, -384(%rbp)
	movq	%rcx, -392(%rbp)
	movq	-384(%rbp), %rax
	movq	-392(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-416(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -832(%rbp)
	movq	-824(%rbp), %rax
	movq	-832(%rbp), %rcx
	movq	%rax, -360(%rbp)
	movq	%rcx, -368(%rbp)
LBB0_7:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-368(%rbp), %rax
	leaq	-360(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	%rax, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	(%rax), %rax
	movq	-328(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB0_8
	jmp	LBB0_10
LBB0_8:                                 ##   in Loop: Header=BB0_7 Depth=1
	leaq	-360(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	(%rax), %rsi
Ltmp12:
	leaq	-376(%rbp), %rdi
	callq	__ZZ4mainENK3$_1clERi
Ltmp13:
	jmp	LBB0_9
LBB0_9:                                 ## %.noexc
                                        ##   in Loop: Header=BB0_7 Depth=1
	leaq	-360(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-288(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB0_7
LBB0_10:                                ## %_ZNSt3__18for_eachINS_11__wrap_iterIPNS_17reference_wrapperIiEEEEZ4mainE3$_1EET0_T_S8_S7_.exit
	leaq	-376(%rbp), %rax
	movq	%rax, -296(%rbp)
## BB#11:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rax
	movq	%rax, -256(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rax
	movq	%rax, -264(%rbp)
	movq	-256(%rbp), %rdi
Ltmp8:
	callq	*%rax
Ltmp9:
	movq	%rax, -904(%rbp)        ## 8-byte Spill
	jmp	LBB0_12
LBB0_12:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB0_13
LBB0_13:
	leaq	-144(%rbp), %rax
	leaq	-752(%rbp), %rcx
	leaq	-216(%rbp), %rdx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdi, -232(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rdx, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-216(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	%rdx, -856(%rbp)
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -152(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rcx
	movq	%rax, -112(%rbp)
	movq	%rcx, -120(%rbp)
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-144(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -864(%rbp)
	movq	-856(%rbp), %rax
	movq	-864(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
LBB0_14:                                ## =>This Inner Loop Header: Depth=1
	leaq	-96(%rbp), %rax
	leaq	-88(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	%rax, -72(%rbp)
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	%rcx, -56(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	-56(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB0_15
	jmp	LBB0_17
LBB0_15:                                ##   in Loop: Header=BB0_14 Depth=1
	leaq	-88(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-88(%rbp), %rsi
Ltmp10:
	leaq	-104(%rbp), %rdi
	callq	__ZZ4mainENK3$_2clERKi
Ltmp11:
	jmp	LBB0_16
LBB0_16:                                ## %.noexc5
                                        ##   in Loop: Header=BB0_14 Depth=1
	leaq	-88(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB0_14
LBB0_17:                                ## %_ZNSt3__18for_eachINS_11__wrap_iterIPiEEZ4mainE3$_2EET0_T_S6_S5_.exit
	leaq	-104(%rbp), %rax
	movq	%rax, -24(%rbp)
## BB#18:
	leaq	-792(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	leaq	-752(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	xorl	%eax, %eax
	addq	$912, %rsp              ## imm = 0x390
	popq	%rbp
	retq
LBB0_19:
Ltmp7:
	movl	%edx, %ecx
	movq	%rax, -808(%rbp)
	movl	%ecx, -812(%rbp)
	jmp	LBB0_21
LBB0_20:
Ltmp14:
	leaq	-792(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -808(%rbp)
	movl	%ecx, -812(%rbp)
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
LBB0_21:
	leaq	-752(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
## BB#22:
	movq	-808(%rbp), %rax
	movq	%rax, -896(%rbp)        ## 8-byte Spill
LBB0_23:                                ## %unwind_resume
	movq	-896(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp3-Ltmp0                     ##   Call between Ltmp0 and Ltmp3
	.long	Lset1
Lset2 = Ltmp4-Lfunc_begin0              ##     jumps to Ltmp4
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp5-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp6-Ltmp5                     ##   Call between Ltmp5 and Ltmp6
	.long	Lset4
Lset5 = Ltmp7-Lfunc_begin0              ##     jumps to Ltmp7
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp12-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp11-Ltmp12                   ##   Call between Ltmp12 and Ltmp11
	.long	Lset7
Lset8 = Ltmp14-Lfunc_begin0             ##     jumps to Ltmp14
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp11-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Lfunc_end0-Ltmp11              ##   Call between Ltmp11 and Lfunc_end0
	.long	Lset10
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__Z5whereINSt3__16vectorIiNS0_9allocatorIiEEEEZ4mainE3$_0EDaRT_OT0_: ## @"_Z5whereINSt3__16vectorIiNS0_9allocatorIiEEEEZ4mainE3$_0EDaRT_OT0_"
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp25:
	.cfi_def_cfa_offset 16
Ltmp26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp27:
	.cfi_def_cfa_register %rbp
	subq	$976, %rsp              ## imm = 0x3D0
	movq	%rdi, %rax
	leaq	-352(%rbp), %rcx
	leaq	-712(%rbp), %r8
	leaq	-776(%rbp), %r9
	leaq	-800(%rbp), %r10
	leaq	-888(%rbp), %r11
	movq	%rsi, -856(%rbp)
	movq	%rdx, -864(%rbp)
	movq	%r11, -848(%rbp)
	movq	-848(%rbp), %rdx
	movq	%rdx, -840(%rbp)
	movq	-840(%rbp), %rdx
	movq	%rdx, -832(%rbp)
	movq	-832(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -824(%rbp)
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	addq	$16, %rdx
	movq	%rdx, -808(%rbp)
	movq	$0, -816(%rbp)
	movq	-808(%rbp), %rdx
	movq	-816(%rbp), %rsi
	movq	%rdx, -792(%rbp)
	movq	%rsi, -800(%rbp)
	movq	-792(%rbp), %rdx
	movq	%r10, -784(%rbp)
	movq	-784(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rdx, -768(%rbp)
	movq	%rsi, -776(%rbp)
	movq	-768(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -760(%rbp)
	movq	%r9, -752(%rbp)
	movq	-752(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	-856(%rbp), %rdx
	movq	%rdx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -744(%rbp)
	movq	-744(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rdx, -720(%rbp)
	movq	%rsi, -728(%rbp)
	movq	-728(%rbp), %rdx
	movq	%r8, -696(%rbp)
	movq	%rdx, -704(%rbp)
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	movq	%rdx, -680(%rbp)
	movq	%rsi, -688(%rbp)
	movq	-680(%rbp), %rdx
	movq	-688(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	-712(%rbp), %rdx
	movq	%rdx, -736(%rbp)
	movq	-736(%rbp), %rdx
	movq	%rdx, -904(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -384(%rbp)
	movq	-384(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	%rdx, -360(%rbp)
	movq	%rsi, -368(%rbp)
	movq	-368(%rbp), %rdx
	movq	%rcx, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	-336(%rbp), %rcx
	movq	-344(%rbp), %rdx
	movq	%rcx, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	-320(%rbp), %rcx
	movq	-328(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-352(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -912(%rbp)
	movq	%rax, -952(%rbp)        ## 8-byte Spill
	movq	%rdi, -960(%rbp)        ## 8-byte Spill
LBB1_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-912(%rbp), %rax
	leaq	-904(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	%rax, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB1_2
	jmp	LBB1_14
LBB1_2:                                 ##   in Loop: Header=BB1_1 Depth=1
	leaq	-904(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-904(%rbp), %rax
	movq	%rax, -920(%rbp)
	movq	-864(%rbp), %rdi
Ltmp18:
	movq	%rax, %rsi
	callq	__ZZ4mainENK3$_0clERKi
Ltmp19:
	movb	%al, -961(%rbp)         ## 1-byte Spill
	jmp	LBB1_3
LBB1_3:                                 ##   in Loop: Header=BB1_1 Depth=1
	movb	-961(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB1_4
	jmp	LBB1_12
LBB1_4:                                 ##   in Loop: Header=BB1_1 Depth=1
	leaq	-944(%rbp), %rax
	leaq	-888(%rbp), %rcx
	movq	-920(%rbp), %rdx
	movq	%rax, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rdx
	movq	-40(%rbp), %rsi
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rdx
	movq	-24(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, -296(%rbp)
	movq	%rax, -304(%rbp)
	movq	-296(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, %rdx
	movq	%rdx, -288(%rbp)
	movq	-288(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rdx, -272(%rbp)
	movq	-272(%rbp), %rdx
	cmpq	(%rdx), %rcx
	movq	%rax, -976(%rbp)        ## 8-byte Spill
	jae	LBB1_7
## BB#5:                                ##   in Loop: Header=BB1_1 Depth=1
Ltmp22:
	movl	$1, %eax
	movl	%eax, %edx
	leaq	-312(%rbp), %rdi
	movq	-976(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
Ltmp23:
	jmp	LBB1_6
LBB1_6:                                 ## %.noexc
                                        ##   in Loop: Header=BB1_1 Depth=1
	leaq	-312(%rbp), %rdi
	movq	-976(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -264(%rbp)
	movq	-264(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rax
	movq	-976(%rbp), %rcx        ## 8-byte Reload
	movq	8(%rcx), %rdx
	movq	%rdx, -232(%rbp)
	movq	-232(%rbp), %rdx
	movq	-304(%rbp), %rsi
	movq	%rsi, -104(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rax, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rax, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rsi
	movq	%rax, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	movq	-976(%rbp), %rax        ## 8-byte Reload
	movq	8(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, 8(%rax)
	jmp	LBB1_9
LBB1_7:                                 ##   in Loop: Header=BB1_1 Depth=1
	movq	-304(%rbp), %rax
	movq	%rax, -240(%rbp)
Ltmp20:
	movq	-976(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Ltmp21:
	jmp	LBB1_8
LBB1_8:                                 ## %.noexc1
                                        ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_9
LBB1_9:                                 ## %_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE9push_backEOS2_.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_10
LBB1_10:                                ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_12
LBB1_11:
Ltmp24:
	leaq	-888(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -928(%rbp)
	movl	%ecx, -932(%rbp)
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	jmp	LBB1_15
LBB1_12:                                ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_13
LBB1_13:                                ##   in Loop: Header=BB1_1 Depth=1
	leaq	-904(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	-392(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB1_1
LBB1_14:
	leaq	-888(%rbp), %rax
	leaq	-440(%rbp), %rcx
	leaq	-424(%rbp), %rdx
	leaq	-456(%rbp), %rsi
	leaq	-472(%rbp), %rdi
	movq	-960(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -664(%rbp)
	movq	%rax, -672(%rbp)
	movq	-664(%rbp), %r9
	movq	-672(%rbp), %r10
	movq	%r9, -648(%rbp)
	movq	%r10, -656(%rbp)
	movq	-648(%rbp), %r9
	movq	%r9, %r10
	movq	-656(%rbp), %r11
	movq	%r11, -640(%rbp)
	movq	-640(%rbp), %r11
	addq	$16, %r11
	movq	%r11, -632(%rbp)
	movq	-632(%rbp), %r11
	movq	%r11, -624(%rbp)
	movq	-624(%rbp), %r11
	movq	%r11, -616(%rbp)
	movq	-616(%rbp), %r11
	movq	%r10, -520(%rbp)
	movq	%r11, -528(%rbp)
	movq	-520(%rbp), %r10
	movq	%r10, %r11
	movq	%r11, -512(%rbp)
	movq	$0, (%r10)
	movq	$0, 8(%r10)
	addq	$16, %r10
	movq	%r10, -496(%rbp)
	movq	$0, -504(%rbp)
	movq	-496(%rbp), %r10
	movq	-504(%rbp), %r11
	movq	%r10, -464(%rbp)
	movq	%r11, -472(%rbp)
	movq	-464(%rbp), %r10
	movq	%rdi, -448(%rbp)
	movq	-448(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rsi, -400(%rbp)
	movq	%r10, -432(%rbp)
	movq	%rdi, -440(%rbp)
	movq	-432(%rbp), %rsi
	movq	%rdx, -416(%rbp)
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	-656(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%r9)
	movq	-656(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%r9)
	movq	-656(%rbp), %rcx
	movq	%rcx, -560(%rbp)
	movq	-560(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -552(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%r9, -584(%rbp)
	movq	-584(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	-656(%rbp), %rcx
	movq	%rcx, -608(%rbp)
	movq	-608(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -600(%rbp)
	movq	-600(%rbp), %rcx
	movq	%rcx, -592(%rbp)
	movq	-592(%rbp), %rcx
	movq	$0, (%rcx)
	movq	-656(%rbp), %rcx
	movq	$0, 8(%rcx)
	movq	-656(%rbp), %rcx
	movq	$0, (%rcx)
	movq	%rax, %rdi
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	movq	-952(%rbp), %rax        ## 8-byte Reload
	addq	$976, %rsp              ## imm = 0x3D0
	popq	%rbp
	retq
LBB1_15:
	movq	-928(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset11 = Ltmp18-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset11
Lset12 = Ltmp21-Ltmp18                  ##   Call between Ltmp18 and Ltmp21
	.long	Lset12
Lset13 = Ltmp24-Lfunc_begin1            ##     jumps to Ltmp24
	.long	Lset13
	.byte	0                       ##   On action: cleanup
Lset14 = Ltmp21-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset14
Lset15 = Lfunc_end1-Ltmp21              ##   Call between Ltmp21 and Lfunc_end1
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp33:
	.cfi_def_cfa_offset 16
Ltmp34:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp35:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp28:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp29:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB2_1
LBB2_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp30:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp31:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB2_3
LBB2_2:
Ltmp32:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB2_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset16 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp28-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp28
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp28-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset18
Lset19 = Ltmp31-Ltmp28                  ##   Call between Ltmp28 and Ltmp31
	.long	Lset19
Lset20 = Ltmp32-Lfunc_begin2            ##     jumps to Ltmp32
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp31-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset21
Lset22 = Lfunc_end2-Ltmp31              ##   Call between Ltmp31 and Lfunc_end2
	.long	Lset22
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp39:
	.cfi_def_cfa_offset 16
Ltmp40:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp41:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZZ4mainENK3$_0clERKi:                 ## @"_ZZ4mainENK3$_0clERKi"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp42:
	.cfi_def_cfa_offset 16
Ltmp43:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp44:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	cmpl	$5, (%rsi)
	setg	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp45:
	.cfi_def_cfa_offset 16
Ltmp46:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp47:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp48:
	.cfi_def_cfa_offset 16
Ltmp49:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp50:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp54:
	.cfi_def_cfa_offset 16
Ltmp55:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp56:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	movq	%rdi, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -336(%rbp)
	movq	-336(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -360(%rbp)
	movq	%rsi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	subq	%rdi, %rax
	sarq	$3, %rax
	addq	$1, %rax
	movq	%rsi, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -424(%rbp)        ## 8-byte Spill
	movq	%rax, -432(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE8max_sizeEv
	movq	%rax, -288(%rbp)
	movq	-280(%rbp), %rax
	cmpq	-288(%rbp), %rax
	jbe	LBB9_2
## BB#1:
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB9_2:
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -232(%rbp)
	movq	-232(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-224(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rdx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	-288(%rbp), %rdx
	shrq	$1, %rdx
	cmpq	%rdx, %rcx
	jb	LBB9_4
## BB#3:
	movq	-288(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	LBB9_8
LBB9_4:
	leaq	-176(%rbp), %rax
	leaq	-280(%rbp), %rcx
	leaq	-304(%rbp), %rdx
	movq	-296(%rbp), %rsi
	shlq	$1, %rsi
	movq	%rsi, -304(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	%rcx, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-168(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB9_6
## BB#5:
	movq	-192(%rbp), %rax
	movq	%rax, -440(%rbp)        ## 8-byte Spill
	jmp	LBB9_7
LBB9_6:
	movq	-184(%rbp), %rax
	movq	%rax, -440(%rbp)        ## 8-byte Spill
LBB9_7:                                 ## %_ZNSt3__13maxImEERKT_S3_S3_.exit.i
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -264(%rbp)
LBB9_8:                                 ## %_ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE11__recommendEm.exit
	leaq	-400(%rbp), %rdi
	movq	-264(%rbp), %rsi
	movq	-424(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	-360(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC1EmmS5_
	movq	-360(%rbp), %rax
	movq	-384(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
## BB#9:
	movq	-384(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -384(%rbp)
Ltmp51:
	leaq	-400(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
Ltmp52:
	jmp	LBB9_10
LBB9_10:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
LBB9_11:
Ltmp53:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev
## BB#12:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table9:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset23 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset23
Lset24 = Ltmp51-Lfunc_begin3            ##   Call between Lfunc_begin3 and Ltmp51
	.long	Lset24
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp51-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset25
Lset26 = Ltmp52-Ltmp51                  ##   Call between Ltmp51 and Ltmp52
	.long	Lset26
Lset27 = Ltmp53-Lfunc_begin3            ##     jumps to Ltmp53
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp52-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset28
Lset29 = Lfunc_end3-Ltmp52              ##   Call between Ltmp52 and Lfunc_end3
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp57:
	.cfi_def_cfa_offset 16
Ltmp58:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp59:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC1EmmS5_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC1EmmS5_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC1EmmS5_: ## @_ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC1EmmS5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp60:
	.cfi_def_cfa_offset 16
Ltmp61:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp62:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC2EmmS5_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp63:
	.cfi_def_cfa_offset 16
Ltmp64:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp65:
	.cfi_def_cfa_register %rbp
	subq	$304, %rsp              ## imm = 0x130
	movq	%rdi, -280(%rbp)
	movq	%rsi, -288(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -296(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE17__annotate_deleteEv
	xorl	%eax, %eax
	movl	%eax, %esi
	movq	-296(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -264(%rbp)
	movq	-264(%rbp), %rdi
	movq	%rdi, -256(%rbp)
	movq	-256(%rbp), %rdi
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	(%rcx), %rdx
	movq	8(%rcx), %r8
	movq	-288(%rbp), %r9
	addq	$8, %r9
	movq	%rdi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%r8, -224(%rbp)
	movq	%r9, -232(%rbp)
	movq	-224(%rbp), %rdx
	movq	-216(%rbp), %rdi
	subq	%rdi, %rdx
	sarq	$3, %rdx
	movq	%rdx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	-232(%rbp), %rdi
	movq	(%rdi), %r8
	subq	%rdx, %rsi
	shlq	$3, %rsi
	addq	%rsi, %r8
	movq	%r8, (%rdi)
	cmpq	$0, -240(%rbp)
	jle	LBB12_2
## BB#1:
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movq	-216(%rbp), %rcx
	movq	-240(%rbp), %rdx
	shlq	$3, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
LBB12_2:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_17reference_wrapperIiEEEEE20__construct_backwardIS3_EENS_9enable_ifIXaaoosr7is_sameIS4_NS1_IT_EEEE5valuentsr15__has_constructIS4_PS8_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS4_SA_SA_RSA_.exit
	leaq	-192(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	-296(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdi
	addq	$8, %rdi
	movq	%rsi, -32(%rbp)
	movq	%rdi, -40(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-32(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-40(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-296(%rbp), %rdx        ## 8-byte Reload
	addq	$8, %rdx
	movq	-288(%rbp), %rsi
	addq	$16, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-80(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	-288(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rcx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-176(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-288(%rbp), %rax
	movq	8(%rax), %rax
	movq	-288(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	callq	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE14__annotate_newEm
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -248(%rbp)
	addq	$304, %rsp              ## imm = 0x130
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev: ## @_ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp66:
	.cfi_def_cfa_offset 16
Ltmp67:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp68:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE8max_sizeEv: ## @_ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp69:
	.cfi_def_cfa_offset 16
Ltmp70:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp71:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$2305843009213693951, %r8 ## imm = 0x1FFFFFFFFFFFFFFF
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB14_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB14_3
LBB14_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB14_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC2EmmS5_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC2EmmS5_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC2EmmS5_: ## @_ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEEC2EmmS5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	leaq	-136(%rbp), %rax
	leaq	-168(%rbp), %r8
	movq	%rdi, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	-232(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	-160(%rbp), %rdx
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-176(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-144(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, 8(%rdx)
	cmpq	$0, -216(%rbp)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	je	LBB15_2
## BB#1:
	movq	-240(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	-216(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rcx
	shlq	$3, %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	jmp	LBB15_3
LBB15_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	jmp	LBB15_3
LBB15_3:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	-240(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-224(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	-216(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE17__annotate_deleteEv: ## @_ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp75:
	.cfi_def_cfa_offset 16
Ltmp76:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp77:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	subq	%rdx, %rsi
	sarq	$3, %rsi
	shlq	$3, %rsi
	addq	%rsi, %rcx
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	8(%rsi), %r8
	movq	(%rsi), %rsi
	subq	%rsi, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, -104(%rbp)
	movq	-104(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -96(%rbp)
	movq	-96(%rbp), %r9
	movq	%r9, -88(%rbp)
	movq	-88(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$3, %r9
	shlq	$3, %r9
	addq	%r9, %rsi
	movq	%rsi, -168(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	-168(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE14__annotate_newEm: ## @_ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp78:
	.cfi_def_cfa_offset 16
Ltmp79:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp80:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdx), %rdx
	subq	%rdx, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	-160(%rbp), %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-168(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-176(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.align	4, 0x90
__ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_: ## @_ZNKSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp81:
	.cfi_def_cfa_offset 16
Ltmp82:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp83:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp84:
	.cfi_def_cfa_offset 16
Ltmp85:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp86:
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp              ## imm = 0x140
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	%rdi, -288(%rbp)        ## 8-byte Spill
	movq	%rax, -296(%rbp)        ## 8-byte Spill
LBB19_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-240(%rbp), %rax
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	cmpq	16(%rcx), %rax
	je	LBB19_3
## BB#2:                                ##   in Loop: Header=BB19_1 Depth=1
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB19_1
LBB19_3:                                ## %_ZNSt3__114__split_bufferINS_17reference_wrapperIiEERNS_9allocatorIS2_EEE5clearEv.exit
	movq	-288(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, (%rax)
	je	LBB19_6
## BB#4:
	movq	-288(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -304(%rbp)        ## 8-byte Spill
	movq	%rdx, -312(%rbp)        ## 8-byte Spill
	movq	%rdi, -320(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-304(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -80(%rbp)
	movq	-320(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -88(%rbp)
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %r8
	movq	%rsi, -48(%rbp)
	movq	%rdi, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rdi
	callq	__ZdlPv
LBB19_6:
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__16vectorINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp87:
	.cfi_def_cfa_offset 16
Ltmp88:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp89:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp90:
	.cfi_def_cfa_offset 16
Ltmp91:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp92:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB21_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB21_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB21_4
## BB#3:                                ##   in Loop: Header=BB21_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB21_2
LBB21_4:                                ## %_ZNSt3__113__vector_baseINS_17reference_wrapperIiEENS_9allocatorIS2_EEE5clearEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__ZdlPv
LBB21_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp93:
	.cfi_def_cfa_offset 16
Ltmp94:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp95:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp96:
	.cfi_def_cfa_offset 16
Ltmp97:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp98:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB23_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB23_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB23_4
## BB#3:                                ##   in Loop: Header=BB23_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-4, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB23_2
LBB23_4:                                ## %_ZNSt3__113__vector_baseIiNS_9allocatorIiEEE5clearEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$2, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__ZdlPv
LBB23_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp99:
	.cfi_def_cfa_offset 16
Ltmp100:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp101:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rsi, -128(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	cmpq	%rax, %rsi
	jbe	LBB24_2
## BB#1:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB24_2:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%rax, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rax
	shlq	$2, %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	xorl	%edx, %edx
	movl	%edx, %esi
	movq	-128(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-112(%rbp), %rdi
	shlq	$2, %rdi
	addq	%rdi, %rax
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-128(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp102:
	.cfi_def_cfa_offset 16
Ltmp103:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp104:
	.cfi_def_cfa_register %rbp
	subq	$240, %rsp
	leaq	-232(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	movq	-224(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	addq	$8, %rsi
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
LBB25_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-144(%rbp), %rax
	cmpq	-152(%rbp), %rax
	je	LBB25_3
## BB#2:                                ##   in Loop: Header=BB25_1 Depth=1
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	movq	-144(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -144(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB25_1
LBB25_3:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorIiEEE25__construct_range_forwardIPKiPiEEvRS2_T_S9_RT0_.exit
	leaq	-232(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	addq	$240, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp105:
	.cfi_def_cfa_offset 16
Ltmp106:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp107:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$4611686018427387903, %r8 ## imm = 0x3FFFFFFFFFFFFFFF
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB26_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB26_3
LBB26_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB26_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp108:
	.cfi_def_cfa_offset 16
Ltmp109:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp110:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	shlq	$2, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdx), %rdx
	subq	%rdx, %r8
	sarq	$2, %r8
	shlq	$2, %r8
	addq	%r8, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	-160(%rbp), %r8
	shlq	$2, %r8
	addq	%r8, %rdx
	movq	%rdi, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-168(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-176(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp111:
	.cfi_def_cfa_offset 16
Ltmp112:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp113:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp114:
	.cfi_def_cfa_offset 16
Ltmp115:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp116:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp117:
	.cfi_def_cfa_offset 16
Ltmp118:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp119:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZZ4mainENK3$_1clERi:                  ## @"_ZZ4mainENK3$_1clERi"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp123:
	.cfi_def_cfa_offset 16
Ltmp124:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp125:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rax
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rsi
	movl	(%rsi), %ecx
	movl	%ecx, %edx
	addl	$1, %edx
	movl	%edx, (%rsi)
	movq	%rax, %rdi
	movl	%ecx, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rdi
	movq	%rax, -8(%rbp)
	movq	%rdi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	*-16(%rbp)
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.align	4, 0x90
__ZZ4mainENK3$_2clERKi:                 ## @"_ZZ4mainENK3$_2clERKi"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp126:
	.cfi_def_cfa_offset 16
Ltmp127:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp128:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rax
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rsi
	movl	(%rsi), %esi
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rdi
	movq	%rax, -8(%rbp)
	movq	%rdi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	*-16(%rbp)
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__const
	.align	2                       ## @.ref.tmp
l_.ref.tmp:
	.long	0                       ## 0x0
	.long	5                       ## 0x5
	.long	2                       ## 0x2
	.long	1                       ## 0x1
	.long	7                       ## 0x7
	.long	9                       ## 0x9


.subsections_via_symbols
