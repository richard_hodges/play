	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z8demanglePKc
	.align	4, 0x90
__Z8demanglePKc:                        ## @_Z8demanglePKc
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp41:
	.cfi_def_cfa_offset 16
Ltmp42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp43:
	.cfi_def_cfa_register %rbp
	subq	$1472, %rsp             ## imm = 0x5C0
	movq	%rdi, %rax
	xorl	%ecx, %ecx
	movl	%ecx, %edx
	leaq	-1112(%rbp), %r8
	leaq	-1116(%rbp), %rcx
	movq	%rsi, -1104(%rbp)
	movq	$0, -1112(%rbp)
	movl	$0, -1116(%rbp)
	movq	-1104(%rbp), %rsi
	movq	%rdi, -1296(%rbp)       ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%r8, %rdx
	movq	%rax, -1304(%rbp)       ## 8-byte Spill
	callq	___cxa_demangle
	leaq	-968(%rbp), %rcx
	leaq	-952(%rbp), %rdx
	leaq	-984(%rbp), %rsi
	leaq	-1000(%rbp), %rdi
	leaq	-1136(%rbp), %r8
	leaq	-1128(%rbp), %r9
	movq	%r9, -1080(%rbp)
	movq	%rax, -1088(%rbp)
	movq	%r8, -1096(%rbp)
	movq	-1080(%rbp), %rax
	movq	-1088(%rbp), %r8
	movq	-1096(%rbp), %r9
	movq	%rax, -1048(%rbp)
	movq	%r8, -1056(%rbp)
	movq	%r9, -1064(%rbp)
	movq	-1048(%rbp), %rax
	movq	-1056(%rbp), %r8
	movq	-1064(%rbp), %r9
	movq	%r9, -1040(%rbp)
	movq	%rax, -1024(%rbp)
	movq	%r8, -1032(%rbp)
	movq	-1024(%rbp), %rax
	movq	-1032(%rbp), %r8
	movq	%rax, -992(%rbp)
	movq	%r8, -1000(%rbp)
	movq	-992(%rbp), %rax
	movq	%rdi, -976(%rbp)
	movq	-976(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rsi, -928(%rbp)
	movq	%rax, -960(%rbp)
	movq	%rdi, -968(%rbp)
	movq	-960(%rbp), %rax
	movq	%rdx, -944(%rbp)
	movq	%rcx, -936(%rbp)
	movq	-936(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	cmpl	$0, -1116(%rbp)
	je	LBB0_49
## BB#1:
	movl	-1116(%rbp), %eax
	movl	%eax, %ecx
	subl	$-3, %ecx
	movl	%eax, -1308(%rbp)       ## 4-byte Spill
	movl	%ecx, -1312(%rbp)       ## 4-byte Spill
	je	LBB0_20
	jmp	LBB0_63
LBB0_63:
	movl	-1308(%rbp), %eax       ## 4-byte Reload
	subl	$-2, %eax
	movl	%eax, -1316(%rbp)       ## 4-byte Spill
	je	LBB0_4
	jmp	LBB0_64
LBB0_64:
	movl	-1308(%rbp), %eax       ## 4-byte Reload
	subl	$-1, %eax
	movl	%eax, -1320(%rbp)       ## 4-byte Spill
	jne	LBB0_24
	jmp	LBB0_2
LBB0_2:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -1328(%rbp)       ## 8-byte Spill
	callq	__ZNSt9bad_allocC1Ev
Ltmp21:
	movq	__ZTISt9bad_alloc@GOTPCREL(%rip), %rsi
	movq	__ZNSt9bad_allocD1Ev@GOTPCREL(%rip), %rdx
	movq	-1328(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_throw
Ltmp22:
	jmp	LBB0_62
LBB0_3:
Ltmp23:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_56
LBB0_4:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movb	$1, -1201(%rbp)
	leaq	L_.str(%rip), %rcx
	movq	%rcx, -824(%rbp)
	movq	$14, -832(%rbp)
	movq	-824(%rbp), %rcx
	leaq	-1200(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	%rcx, -808(%rbp)
	movq	$14, -816(%rbp)
	movq	-800(%rbp), %rcx
	movq	-808(%rbp), %rdx
	movq	%rcx, -776(%rbp)
	movq	%rdx, -784(%rbp)
	movq	$14, -792(%rbp)
	movq	-776(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	%rcx, -760(%rbp)
	movq	%rcx, -752(%rbp)
	movq	%rcx, -744(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-784(%rbp), %rsi
	movq	-792(%rbp), %rdx
Ltmp10:
	movq	%rdi, -1336(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -1344(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp11:
	jmp	LBB0_5
LBB0_5:                                 ## %_ZNSt3__18literals15string_literalsli1sEPKcm.exit
	jmp	LBB0_6
LBB0_6:
	movq	-1104(%rbp), %rax
	leaq	-1200(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	%rax, -736(%rbp)
	movq	-728(%rbp), %rdi
Ltmp13:
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKc
Ltmp14:
	movq	%rax, -1352(%rbp)       ## 8-byte Spill
	jmp	LBB0_7
LBB0_7:                                 ## %.noexc
	leaq	-1176(%rbp), %rax
	movq	-1352(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -720(%rbp)
	movq	-720(%rbp), %rdx
	movq	%rax, -704(%rbp)
	movq	%rdx, -712(%rbp)
	movq	-704(%rbp), %rax
	movq	-712(%rbp), %rdx
	movq	%rax, -688(%rbp)
	movq	%rdx, -696(%rbp)
	movq	-688(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	%rdx, -680(%rbp)
	movq	-680(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rdx), %rdx
	movq	%rdx, 16(%rax)
	movq	-696(%rbp), %rax
	movq	%rax, -656(%rbp)
	movq	-656(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	-648(%rbp), %rax
	movq	%rax, -640(%rbp)
	movq	-640(%rbp), %rax
	movq	%rax, -664(%rbp)
	movl	$0, -668(%rbp)
LBB0_8:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -668(%rbp)
	jae	LBB0_10
## BB#9:                                ##   in Loop: Header=BB0_8 Depth=1
	movl	-668(%rbp), %eax
	movl	%eax, %ecx
	movq	-664(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-668(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -668(%rbp)
	jmp	LBB0_8
LBB0_10:                                ## %_ZNSt3__1plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEOS9_PKS6_.exit
	jmp	LBB0_11
LBB0_11:
Ltmp16:
	leaq	-1176(%rbp), %rsi
	movq	-1344(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt11logic_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp17:
	jmp	LBB0_12
LBB0_12:
	movb	$0, -1201(%rbp)
Ltmp18:
	movq	__ZTISt11logic_error@GOTPCREL(%rip), %rsi
	movq	__ZNSt11logic_errorD1Ev@GOTPCREL(%rip), %rdx
	movq	-1336(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_throw
Ltmp19:
	jmp	LBB0_62
LBB0_13:
Ltmp12:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_17
LBB0_14:
Ltmp15:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_16
LBB0_15:
Ltmp20:
	leaq	-1176(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_16:
	leaq	-1200(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_17:
	testb	$1, -1201(%rbp)
	jne	LBB0_18
	jmp	LBB0_19
LBB0_18:
	movq	-1336(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_free_exception
LBB0_19:
	jmp	LBB0_56
LBB0_20:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -624(%rbp)
	leaq	L_.str.1(%rip), %rax
	movq	%rax, -632(%rbp)
	movq	-624(%rbp), %rcx
	movq	%rcx, -608(%rbp)
	movq	%rax, -616(%rbp)
	movq	-608(%rbp), %rcx
Ltmp5:
	movq	%rdi, -1360(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -1368(%rbp)       ## 8-byte Spill
	callq	__ZNSt11logic_errorC2EPKc
Ltmp6:
	jmp	LBB0_21
LBB0_21:                                ## %_ZNSt16invalid_argumentC1EPKc.exit
	movq	__ZTVSt16invalid_argument@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-1368(%rbp), %rcx       ## 8-byte Reload
	movq	%rax, (%rcx)
## BB#22:
Ltmp8:
	movq	__ZTISt16invalid_argument@GOTPCREL(%rip), %rsi
	movq	__ZNSt16invalid_argumentD1Ev@GOTPCREL(%rip), %rdx
	movq	-1360(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_throw
Ltmp9:
	jmp	LBB0_62
LBB0_23:
Ltmp7:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	movq	-1360(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_free_exception
	jmp	LBB0_56
LBB0_24:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movb	$1, -1281(%rbp)
	leaq	L_.str.2(%rip), %rcx
	movq	%rcx, -592(%rbp)
	movq	$19, -600(%rbp)
	movq	-592(%rbp), %rcx
	leaq	-1256(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	%rcx, -576(%rbp)
	movq	$19, -584(%rbp)
	movq	-568(%rbp), %rcx
	movq	-576(%rbp), %rdx
	movq	%rcx, -544(%rbp)
	movq	%rdx, -552(%rbp)
	movq	$19, -560(%rbp)
	movq	-544(%rbp), %rcx
	movq	%rcx, -536(%rbp)
	movq	%rcx, -528(%rbp)
	movq	%rcx, -520(%rbp)
	movq	%rcx, -512(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-552(%rbp), %rsi
	movq	-560(%rbp), %rdx
Ltmp24:
	movq	%rdi, -1376(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -1384(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp25:
	jmp	LBB0_25
LBB0_25:                                ## %_ZNSt3__18literals15string_literalsli1sEPKcm.exit3
	jmp	LBB0_26
LBB0_26:
	movl	-1116(%rbp), %esi
Ltmp27:
	leaq	-1280(%rbp), %rdi
	callq	__ZNSt3__19to_stringEi
Ltmp28:
	jmp	LBB0_27
LBB0_27:
	leaq	-1280(%rbp), %rax
	leaq	-1256(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	%rax, -504(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rax, -480(%rbp)
	movq	%rcx, -488(%rbp)
	movq	-480(%rbp), %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rax
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -440(%rbp)
	movq	-440(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rdi, -1392(%rbp)       ## 8-byte Spill
	movq	%rax, -1400(%rbp)       ## 8-byte Spill
	je	LBB0_29
## BB#28:
	movq	-1400(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -392(%rbp)
	movq	-392(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1408(%rbp)       ## 8-byte Spill
	jmp	LBB0_30
LBB0_29:
	movq	-1400(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rcx, -1408(%rbp)       ## 8-byte Spill
LBB0_30:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit.i.i
	movq	-1408(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rsi
	movq	-488(%rbp), %rax
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -1416(%rbp)       ## 8-byte Spill
	movq	%rax, -1424(%rbp)       ## 8-byte Spill
	je	LBB0_32
## BB#31:
	movq	-1424(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1432(%rbp)       ## 8-byte Spill
	jmp	LBB0_33
LBB0_32:
	movq	-1424(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1432(%rbp)       ## 8-byte Spill
LBB0_33:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendERKS5_.exit.i
Ltmp30:
	movq	-1432(%rbp), %rax       ## 8-byte Reload
	movq	-1392(%rbp), %rdi       ## 8-byte Reload
	movq	-1416(%rbp), %rsi       ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKcm
Ltmp31:
	movq	%rax, -1440(%rbp)       ## 8-byte Spill
	jmp	LBB0_34
LBB0_34:                                ## %.noexc6
	leaq	-1232(%rbp), %rax
	movq	-1440(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%rdx), %rdx
	movq	%rdx, 16(%rax)
	movq	-256(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -224(%rbp)
	movl	$0, -228(%rbp)
LBB0_35:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -228(%rbp)
	jae	LBB0_37
## BB#36:                               ##   in Loop: Header=BB0_35 Depth=1
	movl	-228(%rbp), %eax
	movl	%eax, %ecx
	movq	-224(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-228(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -228(%rbp)
	jmp	LBB0_35
LBB0_37:                                ## %_ZNSt3__1plIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_12basic_stringIT_T0_T1_EEOS9_SA_.exit
	jmp	LBB0_38
LBB0_38:
Ltmp33:
	leaq	-1232(%rbp), %rsi
	movq	-1384(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt11logic_errorC1ERKNSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp34:
	jmp	LBB0_39
LBB0_39:
	movb	$0, -1281(%rbp)
Ltmp35:
	movq	__ZTISt11logic_error@GOTPCREL(%rip), %rsi
	movq	__ZNSt11logic_errorD1Ev@GOTPCREL(%rip), %rdx
	movq	-1376(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_throw
Ltmp36:
	jmp	LBB0_62
LBB0_40:
Ltmp26:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_46
LBB0_41:
Ltmp29:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_45
LBB0_42:
Ltmp32:
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	jmp	LBB0_44
LBB0_43:
Ltmp37:
	leaq	-1232(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -1144(%rbp)
	movl	%ecx, -1148(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_44:
	leaq	-1280(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_45:
	leaq	-1256(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_46:
	testb	$1, -1281(%rbp)
	jne	LBB0_47
	jmp	LBB0_48
LBB0_47:
	movq	-1376(%rbp), %rdi       ## 8-byte Reload
	callq	___cxa_free_exception
LBB0_48:
	jmp	LBB0_56
LBB0_49:
	leaq	-1128(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	movq	-1128(%rbp), %rax
	movq	-1112(%rbp), %rcx
	movq	-1296(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rsi
	movq	%rax, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdx
Ltmp0:
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp1:
	jmp	LBB0_50
LBB0_50:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcm.exit
	jmp	LBB0_51
LBB0_51:
	leaq	-1128(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -72(%rbp)
	movq	%rax, -1448(%rbp)       ## 8-byte Spill
	je	LBB0_55
## BB#52:
	movq	-1448(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	-72(%rbp), %rsi
Ltmp2:
	movq	%rax, %rdi
	callq	__ZZ8demanglePKcENK7deleterclES0_
Ltmp3:
	jmp	LBB0_53
LBB0_53:
	jmp	LBB0_55
LBB0_54:
Ltmp4:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -1452(%rbp)       ## 4-byte Spill
	callq	___clang_call_terminate
LBB0_55:                                ## %_ZNSt3__110unique_ptrIKcZ8demanglePS1_E7deleterED1Ev.exit9
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	addq	$1472, %rsp             ## imm = 0x5C0
	popq	%rbp
	retq
LBB0_56:
	leaq	-1128(%rbp), %rax
	movq	%rax, -920(%rbp)
	movq	-920(%rbp), %rax
	movq	%rax, -912(%rbp)
	movq	-912(%rbp), %rax
	movq	%rax, -888(%rbp)
	movq	$0, -896(%rbp)
	movq	-888(%rbp), %rax
	movq	%rax, -880(%rbp)
	movq	-880(%rbp), %rcx
	movq	%rcx, -872(%rbp)
	movq	-872(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -904(%rbp)
	movq	-896(%rbp), %rcx
	movq	%rax, -864(%rbp)
	movq	-864(%rbp), %rdx
	movq	%rdx, -856(%rbp)
	movq	-856(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -904(%rbp)
	movq	%rax, -1464(%rbp)       ## 8-byte Spill
	je	LBB0_60
## BB#57:
	movq	-1464(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movq	%rax, -840(%rbp)
	movq	-904(%rbp), %rsi
Ltmp38:
	movq	%rax, %rdi
	callq	__ZZ8demanglePKcENK7deleterclES0_
Ltmp39:
	jmp	LBB0_58
LBB0_58:
	jmp	LBB0_60
LBB0_59:
Ltmp40:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -1468(%rbp)       ## 4-byte Spill
	callq	___clang_call_terminate
LBB0_60:                                ## %_ZNSt3__110unique_ptrIKcZ8demanglePS1_E7deleterED1Ev.exit
	jmp	LBB0_61
LBB0_61:
	movq	-1144(%rbp), %rdi
	callq	__Unwind_Resume
LBB0_62:
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.ascii	"\200\002"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\367\001"              ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp21-Lfunc_begin0             ##   Call between Lfunc_begin0 and Ltmp21
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp21-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp22-Ltmp21                   ##   Call between Ltmp21 and Ltmp22
	.long	Lset3
Lset4 = Ltmp23-Lfunc_begin0             ##     jumps to Ltmp23
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp22-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp10-Ltmp22                   ##   Call between Ltmp22 and Ltmp10
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset7 = Ltmp10-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset7
Lset8 = Ltmp11-Ltmp10                   ##   Call between Ltmp10 and Ltmp11
	.long	Lset8
Lset9 = Ltmp12-Lfunc_begin0             ##     jumps to Ltmp12
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp13-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset10
Lset11 = Ltmp14-Ltmp13                  ##   Call between Ltmp13 and Ltmp14
	.long	Lset11
Lset12 = Ltmp15-Lfunc_begin0            ##     jumps to Ltmp15
	.long	Lset12
	.byte	0                       ##   On action: cleanup
Lset13 = Ltmp16-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset13
Lset14 = Ltmp19-Ltmp16                  ##   Call between Ltmp16 and Ltmp19
	.long	Lset14
Lset15 = Ltmp20-Lfunc_begin0            ##     jumps to Ltmp20
	.long	Lset15
	.byte	0                       ##   On action: cleanup
Lset16 = Ltmp19-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset16
Lset17 = Ltmp5-Ltmp19                   ##   Call between Ltmp19 and Ltmp5
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp5-Lfunc_begin0             ## >> Call Site 8 <<
	.long	Lset18
Lset19 = Ltmp6-Ltmp5                    ##   Call between Ltmp5 and Ltmp6
	.long	Lset19
Lset20 = Ltmp7-Lfunc_begin0             ##     jumps to Ltmp7
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp8-Lfunc_begin0             ## >> Call Site 9 <<
	.long	Lset21
Lset22 = Ltmp9-Ltmp8                    ##   Call between Ltmp8 and Ltmp9
	.long	Lset22
Lset23 = Ltmp23-Lfunc_begin0            ##     jumps to Ltmp23
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp9-Lfunc_begin0             ## >> Call Site 10 <<
	.long	Lset24
Lset25 = Ltmp24-Ltmp9                   ##   Call between Ltmp9 and Ltmp24
	.long	Lset25
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset26 = Ltmp24-Lfunc_begin0            ## >> Call Site 11 <<
	.long	Lset26
Lset27 = Ltmp25-Ltmp24                  ##   Call between Ltmp24 and Ltmp25
	.long	Lset27
Lset28 = Ltmp26-Lfunc_begin0            ##     jumps to Ltmp26
	.long	Lset28
	.byte	0                       ##   On action: cleanup
Lset29 = Ltmp27-Lfunc_begin0            ## >> Call Site 12 <<
	.long	Lset29
Lset30 = Ltmp28-Ltmp27                  ##   Call between Ltmp27 and Ltmp28
	.long	Lset30
Lset31 = Ltmp29-Lfunc_begin0            ##     jumps to Ltmp29
	.long	Lset31
	.byte	0                       ##   On action: cleanup
Lset32 = Ltmp30-Lfunc_begin0            ## >> Call Site 13 <<
	.long	Lset32
Lset33 = Ltmp31-Ltmp30                  ##   Call between Ltmp30 and Ltmp31
	.long	Lset33
Lset34 = Ltmp32-Lfunc_begin0            ##     jumps to Ltmp32
	.long	Lset34
	.byte	0                       ##   On action: cleanup
Lset35 = Ltmp33-Lfunc_begin0            ## >> Call Site 14 <<
	.long	Lset35
Lset36 = Ltmp36-Ltmp33                  ##   Call between Ltmp33 and Ltmp36
	.long	Lset36
Lset37 = Ltmp37-Lfunc_begin0            ##     jumps to Ltmp37
	.long	Lset37
	.byte	0                       ##   On action: cleanup
Lset38 = Ltmp36-Lfunc_begin0            ## >> Call Site 15 <<
	.long	Lset38
Lset39 = Ltmp0-Ltmp36                   ##   Call between Ltmp36 and Ltmp0
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp0-Lfunc_begin0             ## >> Call Site 16 <<
	.long	Lset40
Lset41 = Ltmp1-Ltmp0                    ##   Call between Ltmp0 and Ltmp1
	.long	Lset41
Lset42 = Ltmp23-Lfunc_begin0            ##     jumps to Ltmp23
	.long	Lset42
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp2-Lfunc_begin0             ## >> Call Site 17 <<
	.long	Lset43
Lset44 = Ltmp3-Ltmp2                    ##   Call between Ltmp2 and Ltmp3
	.long	Lset44
Lset45 = Ltmp4-Lfunc_begin0             ##     jumps to Ltmp4
	.long	Lset45
	.byte	1                       ##   On action: 1
Lset46 = Ltmp38-Lfunc_begin0            ## >> Call Site 18 <<
	.long	Lset46
Lset47 = Ltmp39-Ltmp38                  ##   Call between Ltmp38 and Ltmp39
	.long	Lset47
Lset48 = Ltmp40-Lfunc_begin0            ##     jumps to Ltmp40
	.long	Lset48
	.byte	1                       ##   On action: 1
Lset49 = Ltmp39-Lfunc_begin0            ## >> Call Site 19 <<
	.long	Lset49
Lset50 = Lfunc_end0-Ltmp39              ##   Call between Ltmp39 and Lfunc_end0
	.long	Lset50
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp49:
	.cfi_def_cfa_offset 16
Ltmp50:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp51:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movl	$0, -28(%rbp)
	movq	__ZTI1BI1DI1ES1_EE@GOTPCREL(%rip), %rax
	movq	%rax, -24(%rbp)
	movq	8(%rax), %rsi
	leaq	-56(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__Z8demanglePKc
Ltmp44:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-80(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp45:
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	jmp	LBB1_1
LBB1_1:
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -8(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp46:
	callq	*%rcx
Ltmp47:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB1_2
LBB1_2:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB1_3
LBB1_3:
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	addq	$96, %rsp
	popq	%rbp
	retq
LBB1_4:
Ltmp48:
	leaq	-56(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#5:
	movq	-64(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset51 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset51
Lset52 = Ltmp44-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp44
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset53 = Ltmp44-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset53
Lset54 = Ltmp47-Ltmp44                  ##   Call between Ltmp44 and Ltmp47
	.long	Lset54
Lset55 = Ltmp48-Lfunc_begin1            ##     jumps to Ltmp48
	.long	Lset55
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp47-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset56
Lset57 = Lfunc_end1-Ltmp47              ##   Call between Ltmp47 and Lfunc_end1
	.long	Lset57
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp52:
	.cfi_def_cfa_offset 16
Ltmp53:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp54:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB2_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB2_3
LBB2_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB2_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB2_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB2_6
LBB2_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB2_6:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp60:
	.cfi_def_cfa_offset 16
Ltmp61:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp62:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp55:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp56:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB3_1
LBB3_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp57:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp58:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB3_3
LBB3_2:
Ltmp59:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB3_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset58 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset58
Lset59 = Ltmp55-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp55
	.long	Lset59
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset60 = Ltmp55-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset60
Lset61 = Ltmp58-Ltmp55                  ##   Call between Ltmp55 and Ltmp58
	.long	Lset61
Lset62 = Ltmp59-Lfunc_begin2            ##     jumps to Ltmp59
	.long	Lset62
	.byte	0                       ##   On action: cleanup
Lset63 = Ltmp58-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset63
Lset64 = Lfunc_end2-Ltmp58              ##   Call between Ltmp58 and Lfunc_end2
	.long	Lset64
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZZ8demanglePKcENK7deleterclES0_:      ## @_ZZ8demanglePKcENK7deleterclES0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp63:
	.cfi_def_cfa_offset 16
Ltmp64:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp65:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	cmpq	$0, -16(%rbp)
	je	LBB5_2
## BB#1:
	movq	-16(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rdi
	callq	_free
LBB5_2:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp87:
	.cfi_def_cfa_offset 16
Ltmp88:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp89:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp66:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp67:
	jmp	LBB6_1
LBB6_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB6_3
	jmp	LBB6_26
LBB6_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB6_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB6_7
LBB6_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB6_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB6_8
	jmp	LBB6_13
LBB6_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp69:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp70:
	jmp	LBB6_9
LBB6_9:                                 ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp71:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp72:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB6_10
LBB6_10:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp73:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp74:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB6_12
LBB6_11:
Ltmp75:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB6_21
LBB6_12:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB6_13:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp76:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp77:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB6_15
LBB6_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB6_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp78:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp79:
	jmp	LBB6_17
LBB6_17:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB6_18
LBB6_18:
	jmp	LBB6_25
LBB6_19:
Ltmp68:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB6_22
LBB6_20:
Ltmp80:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB6_21
LBB6_21:                                ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB6_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp81:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp82:
	jmp	LBB6_23
LBB6_23:
	callq	___cxa_end_catch
LBB6_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB6_25:
	jmp	LBB6_26
LBB6_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB6_24
LBB6_27:
Ltmp83:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp84:
	callq	___cxa_end_catch
Ltmp85:
	jmp	LBB6_28
LBB6_28:
	jmp	LBB6_29
LBB6_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB6_30:
Ltmp86:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table6:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset65 = Ltmp66-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset65
Lset66 = Ltmp67-Ltmp66                  ##   Call between Ltmp66 and Ltmp67
	.long	Lset66
Lset67 = Ltmp68-Lfunc_begin3            ##     jumps to Ltmp68
	.long	Lset67
	.byte	5                       ##   On action: 3
Lset68 = Ltmp69-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset68
Lset69 = Ltmp70-Ltmp69                  ##   Call between Ltmp69 and Ltmp70
	.long	Lset69
Lset70 = Ltmp80-Lfunc_begin3            ##     jumps to Ltmp80
	.long	Lset70
	.byte	5                       ##   On action: 3
Lset71 = Ltmp71-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset71
Lset72 = Ltmp74-Ltmp71                  ##   Call between Ltmp71 and Ltmp74
	.long	Lset72
Lset73 = Ltmp75-Lfunc_begin3            ##     jumps to Ltmp75
	.long	Lset73
	.byte	3                       ##   On action: 2
Lset74 = Ltmp76-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset74
Lset75 = Ltmp79-Ltmp76                  ##   Call between Ltmp76 and Ltmp79
	.long	Lset75
Lset76 = Ltmp80-Lfunc_begin3            ##     jumps to Ltmp80
	.long	Lset76
	.byte	5                       ##   On action: 3
Lset77 = Ltmp79-Lfunc_begin3            ## >> Call Site 5 <<
	.long	Lset77
Lset78 = Ltmp81-Ltmp79                  ##   Call between Ltmp79 and Ltmp81
	.long	Lset78
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset79 = Ltmp81-Lfunc_begin3            ## >> Call Site 6 <<
	.long	Lset79
Lset80 = Ltmp82-Ltmp81                  ##   Call between Ltmp81 and Ltmp82
	.long	Lset80
Lset81 = Ltmp83-Lfunc_begin3            ##     jumps to Ltmp83
	.long	Lset81
	.byte	0                       ##   On action: cleanup
Lset82 = Ltmp82-Lfunc_begin3            ## >> Call Site 7 <<
	.long	Lset82
Lset83 = Ltmp84-Ltmp82                  ##   Call between Ltmp82 and Ltmp84
	.long	Lset83
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset84 = Ltmp84-Lfunc_begin3            ## >> Call Site 8 <<
	.long	Lset84
Lset85 = Ltmp85-Ltmp84                  ##   Call between Ltmp84 and Ltmp85
	.long	Lset85
Lset86 = Ltmp86-Lfunc_begin3            ##     jumps to Ltmp86
	.long	Lset86
	.byte	5                       ##   On action: 3
Lset87 = Ltmp85-Lfunc_begin3            ## >> Call Site 9 <<
	.long	Lset87
Lset88 = Lfunc_end3-Ltmp85              ##   Call between Ltmp85 and Lfunc_end3
	.long	Lset88
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp93:
	.cfi_def_cfa_offset 16
Ltmp94:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp95:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB7_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB7_26
LBB7_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB7_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB7_5
LBB7_4:
	movq	$0, -368(%rbp)
LBB7_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB7_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB7_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB7_26
LBB7_8:
	jmp	LBB7_9
LBB7_9:
	cmpq	$0, -368(%rbp)
	jle	LBB7_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB7_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB7_13
LBB7_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB7_13:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp90:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp91:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB7_14
LBB7_14:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB7_15
LBB7_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB7_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB7_19
LBB7_17:
Ltmp92:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB7_27
LBB7_18:
	movl	$0, -416(%rbp)
LBB7_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB7_20
	jmp	LBB7_29
LBB7_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB7_26
	jmp	LBB7_28
LBB7_20:
	jmp	LBB7_21
LBB7_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB7_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB7_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB7_26
LBB7_24:
	jmp	LBB7_25
LBB7_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB7_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB7_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB7_28:
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table7:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset89 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset89
Lset90 = Ltmp90-Lfunc_begin4            ##   Call between Lfunc_begin4 and Ltmp90
	.long	Lset90
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset91 = Ltmp90-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset91
Lset92 = Ltmp91-Ltmp90                  ##   Call between Ltmp90 and Ltmp91
	.long	Lset92
Lset93 = Ltmp92-Lfunc_begin4            ##     jumps to Ltmp92
	.long	Lset93
	.byte	0                       ##   On action: cleanup
Lset94 = Ltmp91-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset94
Lset95 = Lfunc_end4-Ltmp91              ##   Call between Ltmp91 and Lfunc_end4
	.long	Lset95
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp96:
	.cfi_def_cfa_offset 16
Ltmp97:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp98:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp99:
	.cfi_def_cfa_offset 16
Ltmp100:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp101:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"invalid name: "

L_.str.1:                               ## @.str.1
	.asciz	"demangle"

L_.str.2:                               ## @.str.2
	.asciz	"unknown error code "

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS1BI1DI1ES1_EE      ## @_ZTS1BI1DI1ES1_EE
	.weak_definition	__ZTS1BI1DI1ES1_EE
__ZTS1BI1DI1ES1_EE:
	.asciz	"1BI1DI1ES1_EE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI1BI1DI1ES1_EE      ## @_ZTI1BI1DI1ES1_EE
	.weak_definition	__ZTI1BI1DI1ES1_EE
	.align	3
__ZTI1BI1DI1ES1_EE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS1BI1DI1ES1_EE


.subsections_via_symbols
