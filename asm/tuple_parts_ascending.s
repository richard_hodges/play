	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp33:
	.cfi_def_cfa_offset 16
Ltmp34:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp35:
	.cfi_def_cfa_register %rbp
	subq	$2768, %rsp             ## imm = 0xAD0
	leaq	-2192(%rbp), %rax
	leaq	-2120(%rbp), %rcx
	leaq	-2144(%rbp), %rdx
	leaq	-2272(%rbp), %rsi
	leaq	l_.ref.tmp(%rip), %rdi
	movl	$0, -2244(%rbp)
	movq	%rdi, -2288(%rbp)
	movq	$11, -2280(%rbp)
	movq	-2288(%rbp), %rdi
	movq	-2280(%rbp), %r8
	movq	%rdi, -2232(%rbp)
	movq	%r8, -2224(%rbp)
	movq	%rsi, -2240(%rbp)
	movq	-2240(%rbp), %rsi
	movq	-2232(%rbp), %rdi
	movq	-2224(%rbp), %r8
	movq	%rdi, -2192(%rbp)
	movq	%r8, -2184(%rbp)
	movq	%rsi, -2200(%rbp)
	movq	-2200(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -2176(%rbp)
	movq	-2176(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -2168(%rbp)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	addq	$16, %rdi
	movq	%rdi, -2152(%rbp)
	movq	$0, -2160(%rbp)
	movq	-2152(%rbp), %rdi
	movq	-2160(%rbp), %r8
	movq	%rdi, -2136(%rbp)
	movq	%r8, -2144(%rbp)
	movq	-2136(%rbp), %rdi
	movq	%rdx, -2128(%rbp)
	movq	-2128(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdi, -2112(%rbp)
	movq	%rdx, -2120(%rbp)
	movq	-2112(%rbp), %rdx
	movq	%rdx, %rdi
	movq	%rdi, -2104(%rbp)
	movq	%rcx, -2096(%rbp)
	movq	-2096(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	%rax, -2088(%rbp)
	movq	-2088(%rbp), %rax
	cmpq	$0, 8(%rax)
	movq	%rsi, -2568(%rbp)       ## 8-byte Spill
	jbe	LBB0_5
## BB#1:
	leaq	-2192(%rbp), %rax
	movq	%rax, -2080(%rbp)
	movq	-2184(%rbp), %rsi
Ltmp0:
	movq	-2568(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
Ltmp1:
	jmp	LBB0_2
LBB0_2:
	leaq	-2192(%rbp), %rax
	movq	%rax, -2056(%rbp)
	movq	-2192(%rbp), %rsi
	movq	%rax, -2064(%rbp)
	movq	-2192(%rbp), %rcx
	movq	-2184(%rbp), %rdx
	leaq	(%rcx,%rdx,4), %rdx
	movq	%rax, -2072(%rbp)
	movq	-2184(%rbp), %rcx
Ltmp2:
	movq	-2568(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
Ltmp3:
	jmp	LBB0_3
LBB0_3:
	jmp	LBB0_5
LBB0_4:
Ltmp4:
	movl	%edx, %ecx
	movq	%rax, -2208(%rbp)
	movl	%ecx, -2212(%rbp)
	movq	-2568(%rbp), %rax       ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	movq	-2208(%rbp), %rax
	movq	%rax, -2576(%rbp)       ## 8-byte Spill
	jmp	LBB0_58
LBB0_5:                                 ## %_ZNSt3__16vectorIiNS_9allocatorIiEEEC1ESt16initializer_listIiE.exit
Ltmp5:
	leaq	-2312(%rbp), %rdi
	leaq	-2272(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
Ltmp6:
	jmp	LBB0_6
LBB0_6:
	leaq	-2272(%rbp), %rax
	leaq	-2376(%rbp), %rcx
	leaq	-2368(%rbp), %rdx
	leaq	-1976(%rbp), %rsi
	leaq	-2000(%rbp), %rdi
	leaq	-2352(%rbp), %r8
	movq	%r8, -2048(%rbp)
	movq	-2048(%rbp), %r8
	movq	%r8, -2040(%rbp)
	movq	-2040(%rbp), %r8
	movq	%r8, -2032(%rbp)
	movq	-2032(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -2024(%rbp)
	movq	$0, (%r8)
	movq	$0, 8(%r8)
	addq	$16, %r8
	movq	%r8, -2008(%rbp)
	movq	$0, -2016(%rbp)
	movq	-2008(%rbp), %r8
	movq	-2016(%rbp), %r9
	movq	%r8, -1992(%rbp)
	movq	%r9, -2000(%rbp)
	movq	-1992(%rbp), %r8
	movq	%rdi, -1984(%rbp)
	movq	-1984(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%r8, -1968(%rbp)
	movq	%rdi, -1976(%rbp)
	movq	-1968(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -1960(%rbp)
	movq	%rsi, -1952(%rbp)
	movq	-1952(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, (%rdi)
	movq	%rdx, -1944(%rbp)
	movq	-1944(%rbp), %rdx
	movq	%rdx, -1936(%rbp)
	movq	-1936(%rbp), %rdx
	movq	%rdx, -1928(%rbp)
	movq	-1928(%rbp), %rdx
	movq	%rdx, -1920(%rbp)
	movq	-1920(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -1912(%rbp)
	movq	-1912(%rbp), %rsi
	movl	$0, (%rsi)
	movq	%rdx, %rsi
	addq	$4, %rsi
	movq	%rsi, -1896(%rbp)
	movq	-1896(%rbp), %rsi
	movl	$0, (%rsi)
	addq	$8, %rdx
	movq	%rdx, -1904(%rbp)
	movq	-1904(%rbp), %rdx
	movl	$0, (%rdx)
	movq	%rcx, -1888(%rbp)
	movq	-1888(%rbp), %rcx
	movq	%rcx, -1880(%rbp)
	movq	-1880(%rbp), %rcx
	movq	$0, (%rcx)
	movq	%rax, -1872(%rbp)
	movq	-1872(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	subq	%rax, %rcx
	sarq	$2, %rcx
	movl	%ecx, %r10d
	movl	%r10d, -2380(%rbp)
	movl	$0, -2384(%rbp)
LBB0_7:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_12 Depth 2
	movl	-2384(%rbp), %eax
	cmpl	-2380(%rbp), %eax
	jge	LBB0_37
## BB#8:                                ##   in Loop: Header=BB0_7 Depth=1
	leaq	-1816(%rbp), %rax
	leaq	-2312(%rbp), %rcx
	movq	%rcx, -1864(%rbp)
	movq	-1864(%rbp), %rcx
	movq	%rcx, -1848(%rbp)
	movq	-1848(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -1824(%rbp)
	movq	%rdx, -1832(%rbp)
	movq	-1832(%rbp), %rcx
	movq	%rax, -1800(%rbp)
	movq	%rcx, -1808(%rbp)
	movq	-1800(%rbp), %rax
	movq	-1808(%rbp), %rcx
	movq	%rax, -1784(%rbp)
	movq	%rcx, -1792(%rbp)
	movq	-1784(%rbp), %rax
	movq	-1792(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1816(%rbp), %rax
	movq	%rax, -1840(%rbp)
	movq	-1840(%rbp), %rax
	movq	%rax, -1856(%rbp)
	movq	-1856(%rbp), %rax
	movq	%rax, -2584(%rbp)       ## 8-byte Spill
## BB#9:                                ##   in Loop: Header=BB0_7 Depth=1
	leaq	-1728(%rbp), %rax
	leaq	-2312(%rbp), %rcx
	movq	-2584(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -2400(%rbp)
	movq	%rcx, -1776(%rbp)
	movq	-1776(%rbp), %rcx
	movq	%rcx, -1760(%rbp)
	movq	-1760(%rbp), %rcx
	movq	8(%rcx), %rsi
	movq	%rcx, -1736(%rbp)
	movq	%rsi, -1744(%rbp)
	movq	-1744(%rbp), %rcx
	movq	%rax, -1712(%rbp)
	movq	%rcx, -1720(%rbp)
	movq	-1712(%rbp), %rax
	movq	-1720(%rbp), %rcx
	movq	%rax, -1696(%rbp)
	movq	%rcx, -1704(%rbp)
	movq	-1696(%rbp), %rax
	movq	-1704(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1728(%rbp), %rax
	movq	%rax, -1752(%rbp)
	movq	-1752(%rbp), %rax
	movq	%rax, -1768(%rbp)
	movq	-1768(%rbp), %rax
	movq	%rax, -2592(%rbp)       ## 8-byte Spill
## BB#10:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-2272(%rbp), %rax
	movq	-2592(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -2408(%rbp)
	movslq	-2384(%rbp), %rdx
	movq	%rax, -1680(%rbp)
	movq	%rdx, -1688(%rbp)
	movq	-1680(%rbp), %rax
	movq	-1688(%rbp), %rdx
	shlq	$2, %rdx
	addq	(%rax), %rdx
	movq	%rdx, -2600(%rbp)       ## 8-byte Spill
## BB#11:                               ##   in Loop: Header=BB0_7 Depth=1
	movq	-2400(%rbp), %rax
	movq	-2408(%rbp), %rcx
	movq	%rax, -1648(%rbp)
	movq	%rcx, -1656(%rbp)
	movq	-2600(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1664(%rbp)
	movq	$0, -1672(%rbp)
LBB0_12:                                ##   Parent Loop BB0_7 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	leaq	-1656(%rbp), %rax
	leaq	-1648(%rbp), %rcx
	movq	%rcx, -1632(%rbp)
	movq	%rax, -1640(%rbp)
	movq	-1632(%rbp), %rax
	movq	-1640(%rbp), %rcx
	movq	%rax, -1616(%rbp)
	movq	%rcx, -1624(%rbp)
	movq	-1616(%rbp), %rax
	movq	%rax, -1608(%rbp)
	movq	-1608(%rbp), %rax
	movq	(%rax), %rax
	movq	-1624(%rbp), %rcx
	movq	%rcx, -1600(%rbp)
	movq	-1600(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB0_13
	jmp	LBB0_16
LBB0_13:                                ##   in Loop: Header=BB0_12 Depth=2
	leaq	-1648(%rbp), %rax
	movq	%rax, -1584(%rbp)
	movq	-1584(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	-1664(%rbp), %rax
	cmpl	(%rax), %ecx
	jne	LBB0_15
## BB#14:                               ##   in Loop: Header=BB0_12 Depth=2
	movq	-1672(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -1672(%rbp)
LBB0_15:                                ##   in Loop: Header=BB0_12 Depth=2
	leaq	-1648(%rbp), %rax
	movq	%rax, -1592(%rbp)
	movq	-1592(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB0_12
LBB0_16:                                ## %_ZNSt3__15countINS_11__wrap_iterIPiEEiEENS_15iterator_traitsIT_E15difference_typeES5_S5_RKT0_.exit
                                        ##   in Loop: Header=BB0_7 Depth=1
	movq	-1672(%rbp), %rax
	movq	%rax, -2608(%rbp)       ## 8-byte Spill
## BB#17:                               ##   in Loop: Header=BB0_7 Depth=1
	movq	-2608(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -2392(%rbp)
	cmpq	$0, -2392(%rbp)
	jle	LBB0_35
## BB#18:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-1496(%rbp), %rax
	leaq	-2312(%rbp), %rcx
	movq	%rcx, -1544(%rbp)
	movq	-1544(%rbp), %rcx
	movq	%rcx, -1528(%rbp)
	movq	-1528(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -1504(%rbp)
	movq	%rdx, -1512(%rbp)
	movq	-1512(%rbp), %rcx
	movq	%rax, -1480(%rbp)
	movq	%rcx, -1488(%rbp)
	movq	-1480(%rbp), %rax
	movq	-1488(%rbp), %rcx
	movq	%rax, -1464(%rbp)
	movq	%rcx, -1472(%rbp)
	movq	-1464(%rbp), %rax
	movq	-1472(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1496(%rbp), %rax
	movq	%rax, -1520(%rbp)
	movq	-1520(%rbp), %rax
	movq	%rax, -1536(%rbp)
	movq	-1536(%rbp), %rax
	movq	%rax, -2616(%rbp)       ## 8-byte Spill
## BB#19:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-1376(%rbp), %rax
	leaq	-2312(%rbp), %rcx
	movq	-2616(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -2432(%rbp)
	movq	%rcx, -1424(%rbp)
	movq	-1424(%rbp), %rcx
	movq	%rcx, -1408(%rbp)
	movq	-1408(%rbp), %rcx
	movq	8(%rcx), %rsi
	movq	%rcx, -1384(%rbp)
	movq	%rsi, -1392(%rbp)
	movq	-1392(%rbp), %rcx
	movq	%rax, -1360(%rbp)
	movq	%rcx, -1368(%rbp)
	movq	-1360(%rbp), %rax
	movq	-1368(%rbp), %rcx
	movq	%rax, -1344(%rbp)
	movq	%rcx, -1352(%rbp)
	movq	-1344(%rbp), %rax
	movq	-1352(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1376(%rbp), %rax
	movq	%rax, -1400(%rbp)
	movq	-1400(%rbp), %rax
	movq	%rax, -1416(%rbp)
	movq	-1416(%rbp), %rax
	movq	%rax, -2624(%rbp)       ## 8-byte Spill
## BB#20:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-2272(%rbp), %rax
	movq	-2624(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -2440(%rbp)
	movslq	-2384(%rbp), %rdx
	movq	%rax, -1224(%rbp)
	movq	%rdx, -1232(%rbp)
	movq	-1224(%rbp), %rax
	movq	-1232(%rbp), %rdx
	shlq	$2, %rdx
	addq	(%rax), %rdx
	movq	%rdx, -2632(%rbp)       ## 8-byte Spill
## BB#21:                               ##   in Loop: Header=BB0_7 Depth=1
	movq	-2432(%rbp), %rdi
	movq	-2440(%rbp), %rsi
Ltmp24:
	movq	-2632(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__16removeINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_
Ltmp25:
	movq	%rax, -2640(%rbp)       ## 8-byte Spill
	jmp	LBB0_22
LBB0_22:                                ##   in Loop: Header=BB0_7 Depth=1
	leaq	-936(%rbp), %rax
	leaq	-2312(%rbp), %rcx
	leaq	-2424(%rbp), %rdx
	leaq	-2416(%rbp), %rsi
	movq	-2640(%rbp), %rdi       ## 8-byte Reload
	movq	%rdi, -2424(%rbp)
	movq	%rsi, -1200(%rbp)
	movq	%rdx, -1208(%rbp)
	movq	$0, -1216(%rbp)
	movq	-1200(%rbp), %rdx
	movq	-1216(%rbp), %rsi
	movq	-1208(%rbp), %r8
	movq	%rdx, -1176(%rbp)
	movq	%r8, -1184(%rbp)
	movq	%rsi, -1192(%rbp)
	movq	-1176(%rbp), %rdx
	movq	-1184(%rbp), %rsi
	movq	%rsi, -1168(%rbp)
	movq	-1168(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	%rcx, -984(%rbp)
	movq	-984(%rbp), %rcx
	movq	%rcx, -968(%rbp)
	movq	-968(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -944(%rbp)
	movq	%rdx, -952(%rbp)
	movq	-952(%rbp), %rcx
	movq	%rax, -920(%rbp)
	movq	%rcx, -928(%rbp)
	movq	-920(%rbp), %rax
	movq	-928(%rbp), %rcx
	movq	%rax, -904(%rbp)
	movq	%rcx, -912(%rbp)
	movq	-904(%rbp), %rax
	movq	-912(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-936(%rbp), %rax
	movq	%rax, -960(%rbp)
	movq	-960(%rbp), %rax
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rax
	movq	%rax, -2648(%rbp)       ## 8-byte Spill
## BB#23:                               ##   in Loop: Header=BB0_7 Depth=1
	movq	-2648(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -2456(%rbp)
	leaq	-2448(%rbp), %rcx
	movq	%rcx, -880(%rbp)
	leaq	-2456(%rbp), %rcx
	movq	%rcx, -888(%rbp)
	movq	$0, -896(%rbp)
	movq	-880(%rbp), %rcx
	movq	-888(%rbp), %rdx
	movq	%rcx, -856(%rbp)
	movq	%rdx, -864(%rbp)
	movq	$0, -872(%rbp)
	movq	-856(%rbp), %rcx
	movq	-864(%rbp), %rdx
	movq	%rdx, -848(%rbp)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	-2416(%rbp), %rsi
	movq	-2448(%rbp), %rdx
Ltmp26:
	leaq	-2312(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEES7_
Ltmp27:
	movq	%rax, -2656(%rbp)       ## 8-byte Spill
	jmp	LBB0_24
LBB0_24:                                ##   in Loop: Header=BB0_7 Depth=1
	leaq	-2272(%rbp), %rax
	movq	-2656(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -2464(%rbp)
	movslq	-2384(%rbp), %rdx
	movq	%rax, -616(%rbp)
	movq	%rdx, -624(%rbp)
	movq	-616(%rbp), %rax
	movq	-624(%rbp), %rdx
	shlq	$2, %rdx
	addq	(%rax), %rdx
	movq	%rdx, -2664(%rbp)       ## 8-byte Spill
## BB#25:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-2488(%rbp), %rax
	leaq	-2384(%rbp), %rcx
	leaq	-2392(%rbp), %rdx
	movq	-2664(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -592(%rbp)
	movq	%rdx, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	-592(%rbp), %rcx
	movq	%rcx, -584(%rbp)
	movq	-584(%rbp), %rcx
	movq	-600(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-224(%rbp), %rdx
	movq	-608(%rbp), %rdi
	movq	%rdi, -232(%rbp)
	movq	-232(%rbp), %rdi
	movq	%rax, -552(%rbp)
	movq	%rcx, -560(%rbp)
	movq	%rdx, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-552(%rbp), %rax
	movq	-560(%rbp), %rcx
	movq	-568(%rbp), %rdx
	movq	-576(%rbp), %rdi
	movq	%rax, -488(%rbp)
	movq	%rcx, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rdi, -512(%rbp)
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %rcx
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	-504(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	-512(%rbp), %rdi
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	movq	%rax, -448(%rbp)
	movq	%rcx, -456(%rbp)
	movq	%rdx, -464(%rbp)
	movq	%rdi, -472(%rbp)
	movq	-448(%rbp), %rax
	movq	-456(%rbp), %rcx
	movq	-464(%rbp), %rdx
	movq	-472(%rbp), %rdi
	movq	%rax, -384(%rbp)
	movq	%rcx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	%rdi, -408(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, %rcx
	movq	-392(%rbp), %rdx
	movq	%rdx, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-264(%rbp), %rcx
	movq	-272(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	-256(%rbp), %rdx
	movl	(%rdx), %r8d
	movl	%r8d, (%rcx)
	movq	%rax, %rcx
	addq	$8, %rcx
	movq	-400(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rcx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	addq	$16, %rax
	movq	-408(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rax, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	-328(%rbp), %rax
	movq	-336(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movl	(%rcx), %r8d
	movl	%r8d, (%rax)
## BB#26:                               ##   in Loop: Header=BB0_7 Depth=1
	leaq	-2504(%rbp), %rax
	leaq	-2352(%rbp), %rcx
	leaq	-2488(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, %rsi
	movq	-160(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rsi, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movl	(%rdi), %r8d
	movl	%r8d, (%rsi)
	movq	%rdx, %rsi
	addq	$4, %rsi
	movq	-160(%rbp), %rdi
	movq	%rdi, -48(%rbp)
	movq	-48(%rbp), %rdi
	addq	$8, %rdi
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rsi, -72(%rbp)
	movq	%rdi, -80(%rbp)
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rdi
	movl	%edi, %r8d
	movl	%r8d, (%rsi)
	addq	$8, %rdx
	movq	-160(%rbp), %rsi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	%rsi, -104(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rdx, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rsi
	movl	(%rsi), %r8d
	movl	%r8d, (%rdx)
	movq	%rcx, -824(%rbp)
	movq	%rax, -832(%rbp)
	movq	-824(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, %rdx
	movq	%rdx, -816(%rbp)
	movq	-816(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -808(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	-800(%rbp), %rdx
	cmpq	(%rdx), %rcx
	movq	%rax, -2672(%rbp)       ## 8-byte Spill
	jae	LBB0_29
## BB#27:                               ##   in Loop: Header=BB0_7 Depth=1
Ltmp30:
	movl	$1, %eax
	movl	%eax, %edx
	leaq	-840(%rbp), %rdi
	movq	-2672(%rbp), %rsi       ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
Ltmp31:
	jmp	LBB0_28
LBB0_28:                                ## %.noexc
                                        ##   in Loop: Header=BB0_7 Depth=1
	leaq	-840(%rbp), %rdi
	movq	-2672(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -792(%rbp)
	movq	-792(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rax
	movq	%rax, -776(%rbp)
	movq	-776(%rbp), %rax
	movq	-2672(%rbp), %rcx       ## 8-byte Reload
	movq	8(%rcx), %rdx
	movq	%rdx, -760(%rbp)
	movq	-760(%rbp), %rdx
	movq	-832(%rbp), %rsi
	movq	%rsi, -632(%rbp)
	movq	-632(%rbp), %rsi
	movq	%rax, -720(%rbp)
	movq	%rdx, -728(%rbp)
	movq	%rsi, -736(%rbp)
	movq	-720(%rbp), %rax
	movq	-728(%rbp), %rdx
	movq	-736(%rbp), %rsi
	movq	%rsi, -712(%rbp)
	movq	-712(%rbp), %rsi
	movq	%rax, -688(%rbp)
	movq	%rdx, -696(%rbp)
	movq	%rsi, -704(%rbp)
	movq	-688(%rbp), %rax
	movq	-696(%rbp), %rdx
	movq	-704(%rbp), %rsi
	movq	%rsi, -672(%rbp)
	movq	-672(%rbp), %rsi
	movq	%rax, -648(%rbp)
	movq	%rdx, -656(%rbp)
	movq	%rsi, -664(%rbp)
	movq	-656(%rbp), %rax
	movq	-664(%rbp), %rdx
	movq	%rdx, -640(%rbp)
	movq	-640(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rax)
	movl	8(%rdx), %r8d
	movl	%r8d, 8(%rax)
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	movq	-2672(%rbp), %rax       ## 8-byte Reload
	movq	8(%rax), %rcx
	addq	$12, %rcx
	movq	%rcx, 8(%rax)
	jmp	LBB0_31
LBB0_29:                                ##   in Loop: Header=BB0_7 Depth=1
	movq	-832(%rbp), %rax
	movq	%rax, -768(%rbp)
Ltmp28:
	movq	-2672(%rbp), %rdi       ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Ltmp29:
	jmp	LBB0_30
LBB0_30:                                ## %.noexc4
                                        ##   in Loop: Header=BB0_7 Depth=1
	jmp	LBB0_31
LBB0_31:                                ## %_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE9push_backEOS2_.exit
                                        ##   in Loop: Header=BB0_7 Depth=1
	jmp	LBB0_32
LBB0_32:                                ##   in Loop: Header=BB0_7 Depth=1
	jmp	LBB0_35
LBB0_33:
Ltmp7:
	movl	%edx, %ecx
	movq	%rax, -2320(%rbp)
	movl	%ecx, -2324(%rbp)
	jmp	LBB0_56
LBB0_34:
Ltmp32:
	leaq	-2352(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -2320(%rbp)
	movl	%ecx, -2324(%rbp)
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev
	leaq	-2312(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	jmp	LBB0_56
LBB0_35:                                ##   in Loop: Header=BB0_7 Depth=1
	jmp	LBB0_36
LBB0_36:                                ##   in Loop: Header=BB0_7 Depth=1
	movl	-2384(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -2384(%rbp)
	jmp	LBB0_7
LBB0_37:
	leaq	-1024(%rbp), %rax
	leaq	-2352(%rbp), %rcx
	movq	%rcx, -1072(%rbp)
	movq	-1072(%rbp), %rcx
	movq	%rcx, -1056(%rbp)
	movq	-1056(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -1032(%rbp)
	movq	%rdx, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rax, -1008(%rbp)
	movq	%rcx, -1016(%rbp)
	movq	-1008(%rbp), %rax
	movq	-1016(%rbp), %rcx
	movq	%rax, -992(%rbp)
	movq	%rcx, -1000(%rbp)
	movq	-992(%rbp), %rax
	movq	-1000(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1024(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rax
	movq	%rax, -1064(%rbp)
	movq	-1064(%rbp), %rax
	movq	%rax, -2680(%rbp)       ## 8-byte Spill
## BB#38:
	leaq	-1112(%rbp), %rax
	leaq	-2352(%rbp), %rcx
	movq	-2680(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -2512(%rbp)
	movq	%rcx, -1160(%rbp)
	movq	-1160(%rbp), %rcx
	movq	%rcx, -1144(%rbp)
	movq	-1144(%rbp), %rcx
	movq	8(%rcx), %rsi
	movq	%rcx, -1120(%rbp)
	movq	%rsi, -1128(%rbp)
	movq	-1128(%rbp), %rcx
	movq	%rax, -1096(%rbp)
	movq	%rcx, -1104(%rbp)
	movq	-1096(%rbp), %rax
	movq	-1104(%rbp), %rcx
	movq	%rax, -1080(%rbp)
	movq	%rcx, -1088(%rbp)
	movq	-1080(%rbp), %rax
	movq	-1088(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-1112(%rbp), %rax
	movq	%rax, -1136(%rbp)
	movq	-1136(%rbp), %rax
	movq	%rax, -1152(%rbp)
	movq	-1152(%rbp), %rax
	movq	%rax, -2688(%rbp)       ## 8-byte Spill
## BB#39:
	movq	-2688(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -2520(%rbp)
Ltmp8:
	leaq	-2536(%rbp), %rdi
	callq	__Z14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEDaOT_NS0_16integer_sequenceImJXspT0_EEEE
Ltmp9:
	jmp	LBB0_40
LBB0_40:
	movq	-2512(%rbp), %rax
	movq	-2520(%rbp), %rcx
	movq	%rax, -1280(%rbp)
	movq	%rcx, -1288(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -1272(%rbp)
	movq	-1280(%rbp), %rax
	leaq	-1288(%rbp), %rcx
	movq	%rcx, -1240(%rbp)
	movq	-1288(%rbp), %rcx
	movq	%rax, -1248(%rbp)
	movq	%rcx, -1256(%rbp)
	leaq	-1296(%rbp), %rax
	movq	%rax, -1264(%rbp)
	movq	-1248(%rbp), %rdi
	movq	-1256(%rbp), %rsi
Ltmp10:
	movq	%rax, %rdx
	callq	__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
Ltmp11:
	jmp	LBB0_41
LBB0_41:                                ## %_ZNSt3__14sortINS_5tupleIJiiiEEEN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEEEvNS_11__wrap_iterIPT_EESB_T0_.exit
	jmp	LBB0_42
LBB0_42:
	movl	$0, -2556(%rbp)
LBB0_43:                                ## =>This Inner Loop Header: Depth=1
	movl	$12, %eax
	movl	%eax, %ecx
	leaq	-2352(%rbp), %rdx
	movslq	-2556(%rbp), %rsi
	movq	%rdx, -1304(%rbp)
	movq	-1304(%rbp), %rdx
	movq	8(%rdx), %rdi
	movq	(%rdx), %rdx
	subq	%rdx, %rdi
	movq	%rdi, %rax
	cqto
	idivq	%rcx
	cmpq	%rax, %rsi
	jae	LBB0_55
## BB#44:                               ##   in Loop: Header=BB0_43 Depth=1
	leaq	-2352(%rbp), %rax
	movslq	-2556(%rbp), %rcx
	movq	%rax, -1312(%rbp)
	movq	%rcx, -1320(%rbp)
	movq	-1312(%rbp), %rax
	movq	-1320(%rbp), %rcx
	imulq	$12, %rcx, %rcx
	addq	(%rax), %rcx
	movq	%rcx, -2696(%rbp)       ## 8-byte Spill
## BB#45:                               ##   in Loop: Header=BB0_43 Depth=1
	movq	-2696(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1336(%rbp)
	movq	%rax, -1328(%rbp)
	movl	(%rax), %esi
Ltmp12:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp13:
	movq	%rax, -2704(%rbp)       ## 8-byte Spill
	jmp	LBB0_46
LBB0_46:                                ##   in Loop: Header=BB0_43 Depth=1
Ltmp14:
	leaq	L_.str(%rip), %rsi
	movq	-2704(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp15:
	movq	%rax, -2712(%rbp)       ## 8-byte Spill
	jmp	LBB0_47
LBB0_47:                                ##   in Loop: Header=BB0_43 Depth=1
	leaq	-2352(%rbp), %rax
	movslq	-2556(%rbp), %rcx
	movq	%rax, -1432(%rbp)
	movq	%rcx, -1440(%rbp)
	movq	-1432(%rbp), %rax
	movq	-1440(%rbp), %rcx
	imulq	$12, %rcx, %rcx
	addq	(%rax), %rcx
	movq	%rcx, -2720(%rbp)       ## 8-byte Spill
## BB#48:                               ##   in Loop: Header=BB0_43 Depth=1
	movq	-2720(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1456(%rbp)
	addq	$4, %rax
	movq	%rax, -1448(%rbp)
	movq	-2720(%rbp), %rax       ## 8-byte Reload
	movl	4(%rax), %esi
Ltmp16:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp17:
	movq	%rax, -2728(%rbp)       ## 8-byte Spill
	jmp	LBB0_49
LBB0_49:                                ##   in Loop: Header=BB0_43 Depth=1
Ltmp18:
	leaq	L_.str(%rip), %rsi
	movq	-2728(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp19:
	movq	%rax, -2736(%rbp)       ## 8-byte Spill
	jmp	LBB0_50
LBB0_50:                                ##   in Loop: Header=BB0_43 Depth=1
	leaq	-2352(%rbp), %rax
	movslq	-2556(%rbp), %rcx
	movq	%rax, -1552(%rbp)
	movq	%rcx, -1560(%rbp)
	movq	-1552(%rbp), %rax
	movq	-1560(%rbp), %rcx
	imulq	$12, %rcx, %rcx
	addq	(%rax), %rcx
	movq	%rcx, -2744(%rbp)       ## 8-byte Spill
## BB#51:                               ##   in Loop: Header=BB0_43 Depth=1
	movq	-2744(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1576(%rbp)
	addq	$8, %rax
	movq	%rax, -1568(%rbp)
	movq	-2744(%rbp), %rax       ## 8-byte Reload
	movl	8(%rax), %esi
Ltmp20:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp21:
	movq	%rax, -2752(%rbp)       ## 8-byte Spill
	jmp	LBB0_52
LBB0_52:                                ##   in Loop: Header=BB0_43 Depth=1
Ltmp22:
	leaq	L_.str.1(%rip), %rsi
	movq	-2752(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp23:
	movq	%rax, -2760(%rbp)       ## 8-byte Spill
	jmp	LBB0_53
LBB0_53:                                ##   in Loop: Header=BB0_43 Depth=1
	jmp	LBB0_54
LBB0_54:                                ##   in Loop: Header=BB0_43 Depth=1
	movl	-2556(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -2556(%rbp)
	jmp	LBB0_43
LBB0_55:
	leaq	-2352(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev
	leaq	-2312(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	leaq	-2272(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	movl	-2244(%rbp), %eax
	addq	$2768, %rsp             ## imm = 0xAD0
	popq	%rbp
	retq
LBB0_56:
	leaq	-2272(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
## BB#57:
	movq	-2320(%rbp), %rax
	movq	%rax, -2576(%rbp)       ## 8-byte Spill
LBB0_58:                                ## %unwind_resume
	movq	-2576(%rbp), %rax       ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp3-Ltmp0                     ##   Call between Ltmp0 and Ltmp3
	.long	Lset1
Lset2 = Ltmp4-Lfunc_begin0              ##     jumps to Ltmp4
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp5-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp6-Ltmp5                     ##   Call between Ltmp5 and Ltmp6
	.long	Lset4
Lset5 = Ltmp7-Lfunc_begin0              ##     jumps to Ltmp7
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp24-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp23-Ltmp24                   ##   Call between Ltmp24 and Ltmp23
	.long	Lset7
Lset8 = Ltmp32-Lfunc_begin0             ##     jumps to Ltmp32
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp23-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Lfunc_end0-Ltmp23              ##   Call between Ltmp23 and Lfunc_end0
	.long	Lset10
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEEC1ERKS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEES7_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEES7_
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEES7_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE5eraseENS_11__wrap_iterIPKiEES7_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp42:
	.cfi_def_cfa_offset 16
Ltmp43:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp44:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$552, %rsp              ## imm = 0x228
Ltmp45:
	.cfi_offset %rbx, -24
	leaq	-504(%rbp), %rax
	leaq	-496(%rbp), %rcx
	leaq	-208(%rbp), %r8
	leaq	-528(%rbp), %r9
	leaq	-448(%rbp), %r10
	movq	%rsi, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rdi, -512(%rbp)
	movq	-512(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdi
	movq	(%rdi), %r11
	movq	%rdi, -456(%rbp)
	movq	%r11, -464(%rbp)
	movq	-464(%rbp), %rdi
	movq	%r10, -432(%rbp)
	movq	%rdi, -440(%rbp)
	movq	-432(%rbp), %rdi
	movq	-440(%rbp), %r10
	movq	%rdi, -416(%rbp)
	movq	%r10, -424(%rbp)
	movq	-416(%rbp), %rdi
	movq	-424(%rbp), %r10
	movq	%r10, (%rdi)
	movq	-448(%rbp), %rdi
	movq	%rdi, -472(%rbp)
	movq	-472(%rbp), %rdi
	movq	%rdi, -528(%rbp)
	movq	%rcx, -248(%rbp)
	movq	%r9, -256(%rbp)
	movq	-248(%rbp), %rdi
	movq	%rdi, -240(%rbp)
	movq	-240(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	-256(%rbp), %r9
	movq	%r9, -232(%rbp)
	movq	-232(%rbp), %r9
	movq	(%r9), %r9
	subq	%r9, %rdi
	sarq	$2, %rdi
	shlq	$2, %rdi
	addq	%rdi, %rsi
	movq	%rsi, -520(%rbp)
	movq	-520(%rbp), %rsi
	movq	%rdx, -216(%rbp)
	movq	%rsi, -224(%rbp)
	movq	-224(%rbp), %rsi
	movq	%r8, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movq	-176(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	%rdi, (%rsi)
	movq	-208(%rbp), %rsi
	movq	%rsi, -488(%rbp)
	movq	%rcx, -48(%rbp)
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rax
	movq	-56(%rbp), %rcx
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	-40(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%bl
	xorb	$-1, %bl
	testb	$1, %bl
	movq	%rdx, -536(%rbp)        ## 8-byte Spill
	jne	LBB2_1
	jmp	LBB2_9
LBB2_1:
	leaq	-496(%rbp), %rax
	leaq	-504(%rbp), %rcx
	movq	-520(%rbp), %rdx
	movq	%rcx, -80(%rbp)
	movq	%rax, -88(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	-88(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	(%rcx), %rcx
	subq	%rcx, %rax
	sarq	$2, %rax
	shlq	$2, %rax
	addq	%rax, %rdx
	movq	-536(%rbp), %rax        ## 8-byte Reload
	movq	8(%rax), %rcx
	movq	-520(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	-160(%rbp), %rdx
	movq	%rdx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rsi, -104(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %rdx
	subq	%rdx, %rcx
	sarq	$2, %rcx
	movq	%rcx, -136(%rbp)
	cmpq	$0, -136(%rbp)
	jbe	LBB2_3
## BB#2:
	movq	-128(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	-136(%rbp), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memmove
LBB2_3:                                 ## %_ZNSt3__14moveIPiS1_EET0_T_S3_S2_.exit
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rcx
	shlq	$2, %rcx
	addq	%rcx, %rax
	movq	-536(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -392(%rbp)
	movq	%rax, -400(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	-384(%rbp), %rdx
	movq	8(%rdx), %rsi
	movq	(%rdx), %rdx
	subq	%rdx, %rsi
	sarq	$2, %rsi
	movq	%rsi, -408(%rbp)
	movq	%rax, %rdx
	movq	-400(%rbp), %rsi
	movq	%rdx, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	-368(%rbp), %rdx
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	movq	%rdx, -552(%rbp)        ## 8-byte Spill
LBB2_4:                                 ## =>This Inner Loop Header: Depth=1
	movq	-376(%rbp), %rax
	movq	-552(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB2_6
## BB#5:                                ##   in Loop: Header=BB2_4 Depth=1
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	-344(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-4, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rcx, -304(%rbp)
	movq	%rdx, -312(%rbp)
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rdx
	movq	%rcx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	-288(%rbp), %rcx
	movq	-296(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rdx, -272(%rbp)
	jmp	LBB2_4
LBB2_6:                                 ## %_ZNSt3__113__vector_baseIiNS_9allocatorIiEEE17__destruct_at_endEPi.exit.i
	movq	-408(%rbp), %rsi
Ltmp39:
	movq	-544(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm
Ltmp40:
	jmp	LBB2_8
LBB2_7:
Ltmp41:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -556(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB2_8:                                 ## %_ZNSt3__16vectorIiNS_9allocatorIiEEE17__destruct_at_endEPi.exit
	jmp	LBB2_9
LBB2_9:
	movq	-488(%rbp), %rax
	addq	$552, %rsp              ## imm = 0x228
	popq	%rbx
	popq	%rbp
	retq
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset11 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset11
Lset12 = Ltmp39-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp39
	.long	Lset12
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset13 = Ltmp39-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset13
Lset14 = Ltmp40-Ltmp39                  ##   Call between Ltmp39 and Ltmp40
	.long	Lset14
Lset15 = Ltmp41-Lfunc_begin1            ##     jumps to Ltmp41
	.long	Lset15
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16removeINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_
	.weak_def_can_be_hidden	__ZNSt3__16removeINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_
	.align	4, 0x90
__ZNSt3__16removeINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_: ## @_ZNSt3__16removeINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp46:
	.cfi_def_cfa_offset 16
Ltmp47:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp48:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -256(%rbp)
	movq	%rsi, -264(%rbp)
	movq	%rdx, -272(%rbp)
	movq	-256(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rdx, -296(%rbp)
	movq	-272(%rbp), %rdx
	movq	-288(%rbp), %rsi
	movq	-296(%rbp), %rdi
	movq	%rsi, -224(%rbp)
	movq	%rdi, -232(%rbp)
	movq	%rdx, -240(%rbp)
LBB3_1:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-232(%rbp), %rax
	leaq	-224(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	%rax, -208(%rbp)
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rcx
	movq	%rax, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	(%rax), %rax
	movq	-192(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB3_2
	jmp	LBB3_5
LBB3_2:                                 ##   in Loop: Header=BB3_1 Depth=1
	leaq	-224(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	-240(%rbp), %rax
	cmpl	(%rax), %ecx
	jne	LBB3_4
## BB#3:
	jmp	LBB3_5
LBB3_4:                                 ##   in Loop: Header=BB3_1 Depth=1
	leaq	-224(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB3_1
LBB3_5:                                 ## %_ZNSt3__14findINS_11__wrap_iterIPiEEiEET_S4_S4_RKT0_.exit
	leaq	-264(%rbp), %rax
	leaq	-256(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%sil
	xorb	$-1, %sil
	testb	$1, %sil
	jne	LBB3_6
	jmp	LBB3_12
LBB3_6:
	movq	-256(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB3_7:                                 ## =>This Inner Loop Header: Depth=1
	leaq	-264(%rbp), %rax
	leaq	-304(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rdx
	addq	$4, %rdx
	movq	%rdx, (%rcx)
	movq	%rcx, -96(%rbp)
	movq	%rax, -104(%rbp)
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	(%rax), %rax
	movq	-88(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%sil
	xorb	$-1, %sil
	testb	$1, %sil
	jne	LBB3_8
	jmp	LBB3_11
LBB3_8:                                 ##   in Loop: Header=BB3_7 Depth=1
	leaq	-304(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	-272(%rbp), %rax
	cmpl	(%rax), %ecx
	je	LBB3_10
## BB#9:                                ##   in Loop: Header=BB3_7 Depth=1
	leaq	-256(%rbp), %rax
	leaq	-304(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movl	(%rcx), %edx
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	movl	%edx, (%rcx)
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
LBB3_10:                                ##   in Loop: Header=BB3_7 Depth=1
	jmp	LBB3_7
LBB3_11:
	jmp	LBB3_12
LBB3_12:
	movq	-256(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rax
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEDaOT_NS0_16integer_sequenceImJXspT0_EEEE
	.weak_def_can_be_hidden	__Z14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEDaOT_NS0_16integer_sequenceImJXspT0_EEEE
	.align	4, 0x90
__Z14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEDaOT_NS0_16integer_sequenceImJXspT0_EEEE: ## @_Z14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEDaOT_NS0_16integer_sequenceImJXspT0_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp49:
	.cfi_def_cfa_offset 16
Ltmp50:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp51:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	leaq	-16(%rbp), %rax
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC1EOS3_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp52:
	.cfi_def_cfa_offset 16
Ltmp53:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp54:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp55:
	.cfi_def_cfa_offset 16
Ltmp56:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp57:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp58:
	.cfi_def_cfa_offset 16
Ltmp59:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp60:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC1EOS3_
	.weak_def_can_be_hidden	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC1EOS3_
	.align	4, 0x90
__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC1EOS3_: ## @_ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC1EOS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC2EOS3_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC2EOS3_
	.weak_def_can_be_hidden	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC2EOS3_
	.align	4, 0x90
__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC2EOS3_: ## @_ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEC2EOS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp64:
	.cfi_def_cfa_offset 16
Ltmp65:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp66:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp67:
	.cfi_def_cfa_offset 16
Ltmp68:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp69:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp70:
	.cfi_def_cfa_offset 16
Ltmp71:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp72:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB11_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB11_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB11_4
## BB#3:                                ##   in Loop: Header=BB11_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-12, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB11_2
LBB11_4:                                ## %_ZNSt3__113__vector_baseINS_5tupleIJiiiEEENS_9allocatorIS2_EEE5clearEv.exit
	movl	$12, %eax
	movl	%eax, %ecx
	movq	-256(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	(%rdx), %rdi
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	%r9, -8(%rbp)
	movq	-8(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%r9, %rax
	cqto
	idivq	%rcx
	movq	%rsi, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	%rax, -112(%rbp)
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	-112(%rbp), %rsi
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__ZdlPv
LBB11_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp73:
	.cfi_def_cfa_offset 16
Ltmp74:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp75:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev: ## @_ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp76:
	.cfi_def_cfa_offset 16
Ltmp77:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp78:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB14_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB14_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB14_4
## BB#3:                                ##   in Loop: Header=BB14_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-4, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB14_2
LBB14_4:                                ## %_ZNSt3__113__vector_baseIiNS_9allocatorIiEEE5clearEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$2, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rdi
	callq	__ZdlPv
LBB14_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp79:
	.cfi_def_cfa_offset 16
Ltmp80:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp81:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rsi, -128(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	cmpq	%rax, %rsi
	jbe	LBB15_2
## BB#1:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB15_2:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %rcx
	movq	%rax, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rax
	shlq	$2, %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	xorl	%edx, %edx
	movl	%edx, %esi
	movq	-128(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 8(%rcx)
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-112(%rbp), %rdi
	shlq	$2, %rdi
	addq	%rdi, %rax
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-128(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPKiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES8_S8_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp82:
	.cfi_def_cfa_offset 16
Ltmp83:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp84:
	.cfi_def_cfa_register %rbp
	subq	$240, %rsp
	leaq	-232(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	movq	-224(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	addq	$8, %rsi
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
LBB16_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-144(%rbp), %rax
	cmpq	-152(%rbp), %rax
	je	LBB16_3
## BB#2:                                ##   in Loop: Header=BB16_1 Depth=1
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movl	(%rcx), %esi
	movl	%esi, (%rax)
	movq	-144(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -144(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	addq	$4, %rcx
	movq	%rcx, (%rax)
	jmp	LBB16_1
LBB16_3:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorIiEEE25__construct_range_forwardIPKiPiEEvRS2_T_S9_RT0_.exit
	leaq	-232(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	addq	$240, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp85:
	.cfi_def_cfa_offset 16
Ltmp86:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp87:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$4611686018427387903, %r8 ## imm = 0x3FFFFFFFFFFFFFFF
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB17_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB17_3
LBB17_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB17_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp88:
	.cfi_def_cfa_offset 16
Ltmp89:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp90:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	shlq	$2, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdx), %rdx
	subq	%rdx, %r8
	sarq	$2, %r8
	shlq	$2, %r8
	addq	%r8, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	-160(%rbp), %r8
	shlq	$2, %r8
	addq	%r8, %rdx
	movq	%rdi, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-168(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-176(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp91:
	.cfi_def_cfa_offset 16
Ltmp92:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp93:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp94:
	.cfi_def_cfa_offset 16
Ltmp95:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp96:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp97:
	.cfi_def_cfa_offset 16
Ltmp98:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp99:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC2ERKS3_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEEC2ERKS3_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp108:
	.cfi_def_cfa_offset 16
Ltmp109:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp110:
	.cfi_def_cfa_register %rbp
	subq	$288, %rsp              ## imm = 0x120
	leaq	-56(%rbp), %rax
	leaq	-40(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	-88(%rbp), %r8
	leaq	-248(%rbp), %r9
	movq	%rdi, -232(%rbp)
	movq	%rsi, -240(%rbp)
	movq	-232(%rbp), %rsi
	movq	%rsi, %rdi
	movq	-240(%rbp), %r10
	movq	%r10, -224(%rbp)
	movq	-224(%rbp), %r10
	addq	$16, %r10
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	%r10, -208(%rbp)
	movq	-208(%rbp), %r10
	movq	%r10, -176(%rbp)
	movq	-176(%rbp), %r10
	movq	%r10, -160(%rbp)
	movq	%rdi, -136(%rbp)
	movq	%r9, -144(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -128(%rbp)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	addq	$16, %rdi
	movq	%rdi, -112(%rbp)
	movq	$0, -120(%rbp)
	movq	-112(%rbp), %rdi
	movq	-120(%rbp), %r9
	movq	%rdi, -80(%rbp)
	movq	%r9, -88(%rbp)
	movq	-80(%rbp), %rdi
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	%rdx, -16(%rbp)
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-240(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rcx
	movq	(%rax), %rax
	subq	%rax, %rcx
	sarq	$2, %rcx
	movq	%rcx, -264(%rbp)
	cmpq	$0, -264(%rbp)
	movq	%rsi, -288(%rbp)        ## 8-byte Spill
	jbe	LBB23_5
## BB#1:
	movq	-264(%rbp), %rsi
Ltmp103:
	movq	-288(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE8allocateEm
Ltmp104:
	jmp	LBB23_2
LBB23_2:
	movq	-240(%rbp), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rdx
	movq	-264(%rbp), %rcx
Ltmp105:
	movq	-288(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
Ltmp106:
	jmp	LBB23_3
LBB23_3:
	jmp	LBB23_5
LBB23_4:
Ltmp107:
	movl	%edx, %ecx
	movq	%rax, -272(%rbp)
	movl	%ecx, -276(%rbp)
	movq	-288(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__113__vector_baseIiNS_9allocatorIiEEED2Ev
	jmp	LBB23_6
LBB23_5:
	addq	$288, %rsp              ## imm = 0x120
	popq	%rbp
	retq
LBB23_6:
	movq	-272(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table23:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset16 = Ltmp103-Lfunc_begin2           ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp106-Ltmp103                ##   Call between Ltmp103 and Ltmp106
	.long	Lset17
Lset18 = Ltmp107-Lfunc_begin2           ##     jumps to Ltmp107
	.long	Lset18
	.byte	0                       ##   On action: cleanup
Lset19 = Ltmp106-Lfunc_begin2           ## >> Call Site 2 <<
	.long	Lset19
Lset20 = Lfunc_end2-Ltmp106             ##   Call between Ltmp106 and Lfunc_end2
	.long	Lset20
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.align	4, 0x90
__ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m: ## @_ZNSt3__16vectorIiNS_9allocatorIiEEE18__construct_at_endIPiEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES7_S7_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp111:
	.cfi_def_cfa_offset 16
Ltmp112:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp113:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	leaq	-112(%rbp), %rax
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	movq	%rcx, -120(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotatorC1ERKS3_m
	movq	-104(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	addq	$8, %rsi
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, -40(%rbp)
	cmpq	$0, -40(%rbp)
	jle	LBB24_2
## BB#1:
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movq	-16(%rbp), %rcx
	movq	-40(%rbp), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
	movq	-40(%rbp), %rax
	movq	-32(%rbp), %rcx
	shlq	$2, %rax
	addq	(%rcx), %rax
	movq	%rax, (%rcx)
LBB24_2:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorIiEEE25__construct_range_forwardIiEENS_9enable_ifIXaaoosr7is_sameIS2_NS1_IT_EEEE5valuentsr15__has_constructIS2_PS6_S6_EE5valuesr31is_trivially_move_constructibleIS6_EE5valueEvE4typeERS2_S8_S8_RS8_.exit
	leaq	-112(%rbp), %rdi
	callq	__ZNSt3__16vectorIiNS_9allocatorIiEEE24__RAII_IncreaseAnnotator6__doneEv
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm
	.align	4, 0x90
__ZNKSt3__16vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm: ## @_ZNKSt3__16vectorIiNS_9allocatorIiEEE17__annotate_shrinkEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp114:
	.cfi_def_cfa_offset 16
Ltmp115:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp116:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	-120(%rbp), %rsi
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$2, %rdx
	shlq	$2, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-128(%rbp), %rdx
	shlq	$2, %rdx
	addq	%rdx, %rcx
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %r8
	movq	8(%r8), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$2, %r9
	shlq	$2, %r9
	addq	%r9, %rdx
	movq	%rdi, -136(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-136(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -144(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-144(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorIiNS_9allocatorIiEEE31__annotate_contiguous_containerEPKvS5_S5_S5_
	addq	$144, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC1ERKS5_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp117:
	.cfi_def_cfa_offset 16
Ltmp118:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp119:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp126:
	.cfi_def_cfa_offset 16
Ltmp127:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp128:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	movl	$12, %eax
	movl	%eax, %ecx
	movq	%rdi, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -336(%rbp)
	movq	-336(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -360(%rbp)
	movq	%rsi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	8(%rdi), %rdx
	movq	(%rdi), %rdi
	subq	%rdi, %rdx
	movq	%rdx, %rax
	cqto
	idivq	%rcx
	addq	$1, %rax
	movq	%rsi, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -424(%rbp)        ## 8-byte Spill
	movq	%rax, -432(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE8max_sizeEv
	movq	%rax, -288(%rbp)
	movq	-280(%rbp), %rax
	cmpq	-288(%rbp), %rax
	jbe	LBB28_2
## BB#1:
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB28_2:
	movl	$12, %eax
	movl	%eax, %ecx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -256(%rbp)
	movq	-256(%rbp), %rsi
	movq	%rsi, -248(%rbp)
	movq	-248(%rbp), %rsi
	movq	%rsi, -240(%rbp)
	movq	-240(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -232(%rbp)
	movq	-232(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	movq	-224(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	movq	%rdi, %rax
	cqto
	idivq	%rcx
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	-288(%rbp), %rcx
	shrq	$1, %rcx
	cmpq	%rcx, %rax
	jb	LBB28_4
## BB#3:
	movq	-288(%rbp), %rax
	movq	%rax, -264(%rbp)
	jmp	LBB28_8
LBB28_4:
	leaq	-176(%rbp), %rax
	leaq	-280(%rbp), %rcx
	leaq	-304(%rbp), %rdx
	movq	-296(%rbp), %rsi
	shlq	$1, %rsi
	movq	%rsi, -304(%rbp)
	movq	%rdx, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdx
	movq	%rcx, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rdx
	movq	%rax, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-168(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB28_6
## BB#5:
	movq	-192(%rbp), %rax
	movq	%rax, -440(%rbp)        ## 8-byte Spill
	jmp	LBB28_7
LBB28_6:
	movq	-184(%rbp), %rax
	movq	%rax, -440(%rbp)        ## 8-byte Spill
LBB28_7:                                ## %_ZNSt3__13maxImEERKT_S3_S3_.exit.i
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -264(%rbp)
LBB28_8:                                ## %_ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE11__recommendEm.exit
	leaq	-400(%rbp), %rdi
	movl	$12, %eax
	movl	%eax, %ecx
	movq	-264(%rbp), %rsi
	movq	-424(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %r8
	movq	8(%r8), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%r9, %rax
	cqto
	idivq	%rcx
	movq	-360(%rbp), %rcx
	movq	%rax, %rdx
	callq	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC1EmmS5_
	movq	-360(%rbp), %rax
	movq	-384(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	%rdx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movl	8(%rcx), %r10d
	movl	%r10d, 8(%rax)
## BB#9:
	movq	-384(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -384(%rbp)
Ltmp123:
	leaq	-400(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
Ltmp124:
	jmp	LBB28_10
LBB28_10:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
LBB28_11:
Ltmp125:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev
## BB#12:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table28:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset21 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset21
Lset22 = Ltmp123-Lfunc_begin3           ##   Call between Lfunc_begin3 and Ltmp123
	.long	Lset22
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset23 = Ltmp123-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset23
Lset24 = Ltmp124-Ltmp123                ##   Call between Ltmp123 and Ltmp124
	.long	Lset24
Lset25 = Ltmp125-Lfunc_begin3           ##     jumps to Ltmp125
	.long	Lset25
	.byte	0                       ##   On action: cleanup
Lset26 = Ltmp124-Lfunc_begin3           ## >> Call Site 3 <<
	.long	Lset26
Lset27 = Lfunc_end3-Ltmp124             ##   Call between Ltmp124 and Lfunc_end3
	.long	Lset27
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE24__RAII_IncreaseAnnotatorC2ERKS5_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp129:
	.cfi_def_cfa_offset 16
Ltmp130:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp131:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC1EmmS5_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC1EmmS5_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC1EmmS5_: ## @_ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC1EmmS5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp132:
	.cfi_def_cfa_offset 16
Ltmp133:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp134:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC2EmmS5_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.align	4, 0x90
__ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE: ## @_ZNSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS2_RS4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp135:
	.cfi_def_cfa_offset 16
Ltmp136:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp137:
	.cfi_def_cfa_register %rbp
	subq	$304, %rsp              ## imm = 0x130
	movq	%rdi, -280(%rbp)
	movq	%rsi, -288(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -296(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE17__annotate_deleteEv
	xorl	%eax, %eax
	movl	%eax, %esi
	movl	$12, %eax
	movl	%eax, %edi
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rcx
	movq	-296(%rbp), %rdx        ## 8-byte Reload
	movq	(%rdx), %r8
	movq	8(%rdx), %r9
	movq	-288(%rbp), %r10
	addq	$8, %r10
	movq	%rcx, -208(%rbp)
	movq	%r8, -216(%rbp)
	movq	%r9, -224(%rbp)
	movq	%r10, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	-216(%rbp), %r8
	subq	%r8, %rcx
	movq	%rcx, %rax
	cqto
	idivq	%rdi
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rdi
	subq	%rax, %rsi
	imulq	$12, %rsi, %rax
	addq	%rax, %rdi
	movq	%rdi, (%rcx)
	cmpq	$0, -240(%rbp)
	jle	LBB31_2
## BB#1:
	movq	-232(%rbp), %rax
	movq	(%rax), %rax
	movq	-216(%rbp), %rcx
	imulq	$12, -240(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
LBB31_2:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_5tupleIJiiiEEEEEE20__construct_backwardIS3_EENS_9enable_ifIXaaoosr7is_sameIS4_NS1_IT_EEEE5valuentsr15__has_constructIS4_PS8_S8_EE5valuesr31is_trivially_move_constructibleIS8_EE5valueEvE4typeERS4_SA_SA_RSA_.exit
	movl	$12, %eax
	movl	%eax, %ecx
	leaq	-192(%rbp), %rdx
	leaq	-96(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	movq	-296(%rbp), %r8         ## 8-byte Reload
	movq	-288(%rbp), %r9
	addq	$8, %r9
	movq	%r8, -32(%rbp)
	movq	%r9, -40(%rbp)
	movq	-32(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r8
	movq	(%r8), %r8
	movq	%r8, -48(%rbp)
	movq	-40(%rbp), %r8
	movq	%r8, -8(%rbp)
	movq	-8(%rbp), %r8
	movq	(%r8), %r8
	movq	-32(%rbp), %r9
	movq	%r8, (%r9)
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	-40(%rbp), %r8
	movq	%rdi, (%r8)
	movq	-296(%rbp), %rdi        ## 8-byte Reload
	addq	$8, %rdi
	movq	-288(%rbp), %r8
	addq	$16, %r8
	movq	%rdi, -80(%rbp)
	movq	%r8, -88(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -96(%rbp)
	movq	-88(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	-80(%rbp), %r8
	movq	%rdi, (%r8)
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-88(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	-296(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -120(%rbp)
	movq	-120(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rsi
	movq	%rsi, -104(%rbp)
	movq	-104(%rbp), %rsi
	movq	-288(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rsi, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movq	-176(%rbp), %rsi
	movq	%rsi, -168(%rbp)
	movq	-168(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -192(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -152(%rbp)
	movq	-152(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-176(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-184(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-288(%rbp), %rdx
	movq	8(%rdx), %rdx
	movq	-288(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-296(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rsi
	movq	8(%rsi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	movq	%rdi, %rax
	cqto
	idivq	%rcx
	movq	-296(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE14__annotate_newEm
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -248(%rbp)
	addq	$304, %rsp              ## imm = 0x130
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev: ## @_ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp138:
	.cfi_def_cfa_offset 16
Ltmp139:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp140:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE8max_sizeEv: ## @_ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp141:
	.cfi_def_cfa_offset 16
Ltmp142:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp143:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$1537228672809129301, %r8 ## imm = 0x1555555555555555
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB33_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB33_3
LBB33_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB33_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC2EmmS5_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC2EmmS5_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC2EmmS5_: ## @_ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEEC2EmmS5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp144:
	.cfi_def_cfa_offset 16
Ltmp145:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp146:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	leaq	-136(%rbp), %rax
	leaq	-168(%rbp), %r8
	movq	%rdi, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	-232(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	-160(%rbp), %rdx
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-176(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-144(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, 8(%rdx)
	cmpq	$0, -216(%rbp)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	je	LBB34_2
## BB#1:
	movq	-240(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	-216(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	$0, -56(%rbp)
	imulq	$12, -48(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	jmp	LBB34_3
LBB34_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	jmp	LBB34_3
LBB34_3:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	-240(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	imulq	$12, -224(%rbp), %rdx
	addq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	imulq	$12, -216(%rbp), %rdx
	addq	%rdx, %rax
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE17__annotate_deleteEv: ## @_ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE17__annotate_deleteEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp147:
	.cfi_def_cfa_offset 16
Ltmp148:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp149:
	.cfi_def_cfa_register %rbp
	subq	$208, %rsp
	movl	$12, %eax
	movl	%eax, %ecx
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %r8
	movq	%r8, -32(%rbp)
	movq	-32(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	%r9, -8(%rbp)
	movq	-8(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%r9, %rax
	movq	%rdx, -168(%rbp)        ## 8-byte Spill
	cqto
	idivq	%rcx
	imulq	$12, %rax, %rax
	addq	%rax, %rsi
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	8(%r8), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	movq	%r9, %rax
	cqto
	idivq	%rcx
	imulq	$12, %rax, %rax
	movq	-176(%rbp), %r8         ## 8-byte Reload
	addq	%rax, %r8
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %r9
	movq	%r9, -112(%rbp)
	movq	-112(%rbp), %r9
	movq	%r9, -104(%rbp)
	movq	-104(%rbp), %r10
	addq	$16, %r10
	movq	%r10, -96(%rbp)
	movq	-96(%rbp), %r10
	movq	%r10, -88(%rbp)
	movq	-88(%rbp), %r10
	movq	(%r10), %r10
	movq	(%r9), %r9
	subq	%r9, %r10
	movq	%rax, -184(%rbp)        ## 8-byte Spill
	movq	%r10, %rax
	cqto
	idivq	%rcx
	imulq	$12, %rax, %rax
	movq	-184(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -192(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	-192(%rbp), %r9         ## 8-byte Reload
	movq	%r9, %rdx
	movq	%rcx, -200(%rbp)        ## 8-byte Spill
	movq	%r8, %rcx
	movq	-200(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	addq	$208, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE14__annotate_newEm: ## @_ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp150:
	.cfi_def_cfa_offset 16
Ltmp151:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp152:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movl	$12, %eax
	movl	%eax, %ecx
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %r8
	movq	%r8, -32(%rbp)
	movq	-32(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	%r9, -8(%rbp)
	movq	-8(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%r9, %rax
	movq	%rdx, -168(%rbp)        ## 8-byte Spill
	cqto
	idivq	%rcx
	imulq	$12, %rax, %rax
	movq	-168(%rbp), %r8         ## 8-byte Reload
	addq	%rax, %r8
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %r9
	movq	%r9, -88(%rbp)
	movq	-88(%rbp), %r9
	movq	%r9, -80(%rbp)
	movq	-80(%rbp), %r10
	addq	$16, %r10
	movq	%r10, -72(%rbp)
	movq	-72(%rbp), %r10
	movq	%r10, -64(%rbp)
	movq	-64(%rbp), %r10
	movq	(%r10), %r10
	movq	(%r9), %r9
	subq	%r9, %r10
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	movq	%r10, %rax
	cqto
	idivq	%rcx
	imulq	$12, %rax, %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	imulq	$12, -160(%rbp), %r9
	addq	%r9, %rax
	movq	%rdi, -184(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-184(%rbp), %rsi        ## 8-byte Reload
	movq	%r8, %rdx
	movq	%rax, %r8
	callq	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.align	4, 0x90
__ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_: ## @_ZNKSt3__16vectorINS_5tupleIJiiiEEENS_9allocatorIS2_EEE31__annotate_contiguous_containerEPKvS7_S7_S7_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp153:
	.cfi_def_cfa_offset 16
Ltmp154:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp155:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED2Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED2Ev: ## @_ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp156:
	.cfi_def_cfa_offset 16
Ltmp157:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp158:
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp              ## imm = 0x140
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	%rdi, -288(%rbp)        ## 8-byte Spill
	movq	%rax, -296(%rbp)        ## 8-byte Spill
LBB38_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-240(%rbp), %rax
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	cmpq	16(%rcx), %rax
	je	LBB38_3
## BB#2:                                ##   in Loop: Header=BB38_1 Depth=1
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rdx
	addq	$-12, %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	jmp	LBB38_1
LBB38_3:                                ## %_ZNSt3__114__split_bufferINS_5tupleIJiiiEEERNS_9allocatorIS2_EEE5clearEv.exit
	movq	-288(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, (%rax)
	je	LBB38_6
## BB#4:
	movl	$12, %eax
	movl	%eax, %ecx
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rsi
	addq	$24, %rsi
	movq	%rsi, -104(%rbp)
	movq	-104(%rbp), %rsi
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	(%rdx), %rdi
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r9
	addq	$24, %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	%r9, -8(%rbp)
	movq	-8(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	movq	%r9, %rax
	cqto
	idivq	%rcx
	movq	%rsi, -304(%rbp)        ## 8-byte Spill
	movq	%rdi, -312(%rbp)        ## 8-byte Spill
	movq	%rax, -320(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-304(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -80(%rbp)
	movq	-320(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -88(%rbp)
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %r8
	movq	%rsi, -48(%rbp)
	movq	%rdi, -56(%rbp)
	movq	%r8, -64(%rbp)
	movq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rdi
	callq	__ZdlPv
LBB38_6:
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.align	4, 0x90
__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_: ## @_ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp159:
	.cfi_def_cfa_offset 16
Ltmp160:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp161:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$2456, %rsp             ## imm = 0x998
Ltmp162:
	.cfi_offset %rbx, -24
	movq	%rdi, -2272(%rbp)
	movq	%rsi, -2280(%rbp)
	movq	%rdx, -2288(%rbp)
	movq	$6, -2296(%rbp)
LBB39_1:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB39_2 Depth 2
                                        ##       Child Loop BB39_17 Depth 3
                                        ##       Child Loop BB39_20 Depth 3
                                        ##       Child Loop BB39_29 Depth 3
                                        ##         Child Loop BB39_30 Depth 4
                                        ##         Child Loop BB39_33 Depth 4
                                        ##     Child Loop BB39_45 Depth 2
                                        ##       Child Loop BB39_46 Depth 3
                                        ##       Child Loop BB39_49 Depth 3
	jmp	LBB39_2
LBB39_2:                                ##   Parent Loop BB39_1 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB39_17 Depth 3
                                        ##       Child Loop BB39_20 Depth 3
                                        ##       Child Loop BB39_29 Depth 3
                                        ##         Child Loop BB39_30 Depth 4
                                        ##         Child Loop BB39_33 Depth 4
	movq	-2280(%rbp), %rax
	movq	-2272(%rbp), %rcx
	subq	%rcx, %rax
	sarq	$2, %rax
	movabsq	$-6148914691236517205, %rcx ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rcx, %rax
	movq	%rax, -2304(%rbp)
	movq	%rax, %rcx
	subq	$5, %rcx
	movq	%rax, -2368(%rbp)       ## 8-byte Spill
	movq	%rcx, -2376(%rbp)       ## 8-byte Spill
	ja	LBB39_10
## BB#74:
	leaq	LJTI39_0(%rip), %rax
	movq	-2368(%rbp), %rcx       ## 8-byte Reload
	movslq	(%rax,%rcx,4), %rdx
	addq	%rax, %rdx
	jmpq	*%rdx
LBB39_3:
	jmp	LBB39_73
LBB39_4:
	movq	-2288(%rbp), %rdi
	movq	-2280(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -2280(%rbp)
	movq	-2272(%rbp), %rdx
	movq	%rax, %rsi
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_5
	jmp	LBB39_6
LBB39_5:
	leaq	-2228(%rbp), %rax
	leaq	-2224(%rbp), %rcx
	leaq	-2220(%rbp), %rdx
	leaq	-2036(%rbp), %rsi
	leaq	-1940(%rbp), %rdi
	leaq	-2156(%rbp), %r8
	movq	-2272(%rbp), %r9
	movq	-2280(%rbp), %r10
	movq	%r9, -2256(%rbp)
	movq	%r10, -2264(%rbp)
	movq	-2256(%rbp), %r9
	movq	-2264(%rbp), %r10
	movq	%r9, -2240(%rbp)
	movq	%r10, -2248(%rbp)
	movq	-2240(%rbp), %r9
	movq	-2248(%rbp), %r10
	movq	%r9, -2208(%rbp)
	movq	%r10, -2216(%rbp)
	movq	-2208(%rbp), %r9
	movq	%r9, %r10
	movq	-2216(%rbp), %r11
	movq	%r10, -2192(%rbp)
	movq	%r11, -2200(%rbp)
	movq	-2192(%rbp), %r10
	movq	-2200(%rbp), %r11
	movq	%r10, -2176(%rbp)
	movq	%r11, -2184(%rbp)
	movq	-2176(%rbp), %r10
	movq	%r10, -2168(%rbp)
	movq	-2168(%rbp), %r10
	movq	-2184(%rbp), %r11
	movq	%r11, -2112(%rbp)
	movq	-2112(%rbp), %r11
	movq	%r10, -2144(%rbp)
	movq	%r11, -2152(%rbp)
	movq	-2144(%rbp), %r10
	movq	%r10, -2136(%rbp)
	movq	-2136(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -2156(%rbp)
	movq	-2152(%rbp), %r10
	movq	%r10, -2120(%rbp)
	movq	-2120(%rbp), %r10
	movl	(%r10), %ebx
	movq	-2144(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -2128(%rbp)
	movq	-2128(%rbp), %r8
	movl	(%r8), %ebx
	movq	-2152(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -2220(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-2216(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1976(%rbp)
	movq	%r10, -1984(%rbp)
	movq	-1976(%rbp), %r8
	movq	-1984(%rbp), %r10
	movq	%r8, -1960(%rbp)
	movq	%r10, -1968(%rbp)
	movq	-1960(%rbp), %r8
	movq	%r8, -1952(%rbp)
	movq	-1952(%rbp), %r8
	movq	-1968(%rbp), %r10
	movq	%r10, -1896(%rbp)
	movq	-1896(%rbp), %r10
	movq	%r8, -1928(%rbp)
	movq	%r10, -1936(%rbp)
	movq	-1928(%rbp), %r8
	movq	%r8, -1920(%rbp)
	movq	-1920(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1940(%rbp)
	movq	-1936(%rbp), %r8
	movq	%r8, -1904(%rbp)
	movq	-1904(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1928(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1912(%rbp)
	movq	-1912(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1936(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -2224(%rbp)
	addq	$8, %r9
	movq	-2216(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -2072(%rbp)
	movq	%rdi, -2080(%rbp)
	movq	-2072(%rbp), %rdi
	movq	-2080(%rbp), %r8
	movq	%rdi, -2056(%rbp)
	movq	%r8, -2064(%rbp)
	movq	-2056(%rbp), %rdi
	movq	%rdi, -2048(%rbp)
	movq	-2048(%rbp), %rdi
	movq	-2064(%rbp), %r8
	movq	%r8, -1992(%rbp)
	movq	-1992(%rbp), %r8
	movq	%rdi, -2024(%rbp)
	movq	%r8, -2032(%rbp)
	movq	-2024(%rbp), %rdi
	movq	%rdi, -2016(%rbp)
	movq	-2016(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -2036(%rbp)
	movq	-2032(%rbp), %rdi
	movq	%rdi, -2000(%rbp)
	movq	-2000(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-2024(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -2008(%rbp)
	movq	-2008(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-2032(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -2228(%rbp)
	movq	%rdx, -2088(%rbp)
	movq	%rcx, -2096(%rbp)
	movq	%rax, -2104(%rbp)
LBB39_6:
	jmp	LBB39_73
LBB39_7:
	movq	-2272(%rbp), %rdi
	movq	-2272(%rbp), %rax
	addq	$12, %rax
	movq	-2280(%rbp), %rcx
	addq	$-12, %rcx
	movq	%rcx, -2280(%rbp)
	movq	-2288(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, -2384(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-2384(%rbp), %rcx       ## 8-byte Reload
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movl	%eax, -2388(%rbp)       ## 4-byte Spill
	jmp	LBB39_73
LBB39_8:
	movq	-2272(%rbp), %rdi
	movq	-2272(%rbp), %rax
	addq	$12, %rax
	movq	-2272(%rbp), %rcx
	addq	$24, %rcx
	movq	-2280(%rbp), %rdx
	addq	$-12, %rdx
	movq	%rdx, -2280(%rbp)
	movq	-2288(%rbp), %r8
	movq	%rax, %rsi
	movq	%rdx, -2400(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-2400(%rbp), %rcx       ## 8-byte Reload
	callq	__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	movl	%eax, -2404(%rbp)       ## 4-byte Spill
	jmp	LBB39_73
LBB39_9:
	movq	-2272(%rbp), %rdi
	movq	-2272(%rbp), %rax
	addq	$12, %rax
	movq	-2272(%rbp), %rcx
	addq	$24, %rcx
	movq	-2272(%rbp), %rdx
	addq	$36, %rdx
	movq	-2280(%rbp), %rsi
	addq	$-12, %rsi
	movq	%rsi, -2280(%rbp)
	movq	-2288(%rbp), %r9
	movq	%rsi, -2416(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rdx, -2424(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-2424(%rbp), %rcx       ## 8-byte Reload
	movq	-2416(%rbp), %r8        ## 8-byte Reload
	callq	__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	movl	%eax, -2428(%rbp)       ## 4-byte Spill
	jmp	LBB39_73
LBB39_10:                               ##   in Loop: Header=BB39_2 Depth=2
	cmpq	$6, -2304(%rbp)
	jg	LBB39_12
## BB#11:
	movq	-2272(%rbp), %rdi
	movq	-2280(%rbp), %rsi
	movq	-2288(%rbp), %rdx
	callq	__ZNSt3__118__insertion_sort_3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	jmp	LBB39_73
LBB39_12:                               ##   in Loop: Header=BB39_2 Depth=2
	movq	-2272(%rbp), %rax
	movq	%rax, -2312(%rbp)
	movq	-2280(%rbp), %rax
	movq	%rax, -2320(%rbp)
	movq	-2320(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -2320(%rbp)
	cmpq	$1000, -2304(%rbp)      ## imm = 0x3E8
	jl	LBB39_14
## BB#13:                               ##   in Loop: Header=BB39_2 Depth=2
	movl	$2, %eax
	movl	%eax, %ecx
	movq	-2304(%rbp), %rax
	cqto
	idivq	%rcx
	movq	%rax, -2336(%rbp)
	movq	-2336(%rbp), %rax
	imulq	$12, %rax, %rax
	addq	-2312(%rbp), %rax
	movq	%rax, -2312(%rbp)
	movq	-2336(%rbp), %rax
	cqto
	idivq	%rcx
	movq	%rax, -2336(%rbp)
	movq	-2272(%rbp), %rdi
	movq	-2272(%rbp), %rax
	imulq	$12, -2336(%rbp), %rcx
	addq	%rcx, %rax
	movq	-2312(%rbp), %rcx
	movq	-2312(%rbp), %rsi
	imulq	$12, -2336(%rbp), %r8
	addq	%r8, %rsi
	movq	-2320(%rbp), %r8
	movq	-2288(%rbp), %r9
	movq	%rsi, -2440(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-2440(%rbp), %rcx       ## 8-byte Reload
	callq	__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	movl	%eax, -2324(%rbp)
	jmp	LBB39_15
LBB39_14:                               ##   in Loop: Header=BB39_2 Depth=2
	movl	$2, %eax
	movl	%eax, %ecx
	movq	-2304(%rbp), %rax
	cqto
	idivq	%rcx
	movq	%rax, -2336(%rbp)
	movq	-2336(%rbp), %rax
	imulq	$12, %rax, %rax
	addq	-2312(%rbp), %rax
	movq	%rax, -2312(%rbp)
	movq	-2272(%rbp), %rdi
	movq	-2312(%rbp), %rsi
	movq	-2320(%rbp), %rax
	movq	-2288(%rbp), %rcx
	movq	%rax, %rdx
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movl	%eax, -2324(%rbp)
LBB39_15:                               ##   in Loop: Header=BB39_2 Depth=2
	movq	-2272(%rbp), %rax
	movq	%rax, -2344(%rbp)
	movq	-2320(%rbp), %rax
	movq	%rax, -2352(%rbp)
	movq	-2288(%rbp), %rdi
	movq	-2344(%rbp), %rsi
	movq	-2312(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_43
## BB#16:                               ##   in Loop: Header=BB39_2 Depth=2
	jmp	LBB39_17
LBB39_17:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	-2344(%rbp), %rax
	movq	-2352(%rbp), %rcx
	addq	$-12, %rcx
	movq	%rcx, -2352(%rbp)
	cmpq	%rcx, %rax
	jne	LBB39_39
## BB#18:                               ##   in Loop: Header=BB39_2 Depth=2
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	movq	-2280(%rbp), %rax
	movq	%rax, -2352(%rbp)
	movq	-2288(%rbp), %rdi
	movq	-2272(%rbp), %rsi
	movq	-2352(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -2352(%rbp)
	movq	%rax, %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_26
## BB#19:                               ##   in Loop: Header=BB39_2 Depth=2
	jmp	LBB39_20
LBB39_20:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_2 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	-2344(%rbp), %rax
	cmpq	-2352(%rbp), %rax
	jne	LBB39_22
## BB#21:
	jmp	LBB39_73
LBB39_22:                               ##   in Loop: Header=BB39_20 Depth=3
	movq	-2288(%rbp), %rdi
	movq	-2272(%rbp), %rsi
	movq	-2344(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_23
	jmp	LBB39_24
LBB39_23:                               ##   in Loop: Header=BB39_2 Depth=2
	leaq	-1852(%rbp), %rax
	leaq	-1848(%rbp), %rcx
	leaq	-1844(%rbp), %rdx
	leaq	-1660(%rbp), %rsi
	leaq	-1564(%rbp), %rdi
	leaq	-1780(%rbp), %r8
	movq	-2344(%rbp), %r9
	movq	-2352(%rbp), %r10
	movq	%r9, -1880(%rbp)
	movq	%r10, -1888(%rbp)
	movq	-1880(%rbp), %r9
	movq	-1888(%rbp), %r10
	movq	%r9, -1864(%rbp)
	movq	%r10, -1872(%rbp)
	movq	-1864(%rbp), %r9
	movq	-1872(%rbp), %r10
	movq	%r9, -1832(%rbp)
	movq	%r10, -1840(%rbp)
	movq	-1832(%rbp), %r9
	movq	%r9, %r10
	movq	-1840(%rbp), %r11
	movq	%r10, -1816(%rbp)
	movq	%r11, -1824(%rbp)
	movq	-1816(%rbp), %r10
	movq	-1824(%rbp), %r11
	movq	%r10, -1800(%rbp)
	movq	%r11, -1808(%rbp)
	movq	-1800(%rbp), %r10
	movq	%r10, -1792(%rbp)
	movq	-1792(%rbp), %r10
	movq	-1808(%rbp), %r11
	movq	%r11, -1736(%rbp)
	movq	-1736(%rbp), %r11
	movq	%r10, -1768(%rbp)
	movq	%r11, -1776(%rbp)
	movq	-1768(%rbp), %r10
	movq	%r10, -1760(%rbp)
	movq	-1760(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1780(%rbp)
	movq	-1776(%rbp), %r10
	movq	%r10, -1744(%rbp)
	movq	-1744(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1768(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1752(%rbp)
	movq	-1752(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1776(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1844(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1840(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1600(%rbp)
	movq	%r10, -1608(%rbp)
	movq	-1600(%rbp), %r8
	movq	-1608(%rbp), %r10
	movq	%r8, -1584(%rbp)
	movq	%r10, -1592(%rbp)
	movq	-1584(%rbp), %r8
	movq	%r8, -1576(%rbp)
	movq	-1576(%rbp), %r8
	movq	-1592(%rbp), %r10
	movq	%r10, -1520(%rbp)
	movq	-1520(%rbp), %r10
	movq	%r8, -1552(%rbp)
	movq	%r10, -1560(%rbp)
	movq	-1552(%rbp), %r8
	movq	%r8, -1544(%rbp)
	movq	-1544(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1564(%rbp)
	movq	-1560(%rbp), %r8
	movq	%r8, -1528(%rbp)
	movq	-1528(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1552(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1536(%rbp)
	movq	-1536(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1560(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1848(%rbp)
	addq	$8, %r9
	movq	-1840(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -1696(%rbp)
	movq	%rdi, -1704(%rbp)
	movq	-1696(%rbp), %rdi
	movq	-1704(%rbp), %r8
	movq	%rdi, -1680(%rbp)
	movq	%r8, -1688(%rbp)
	movq	-1680(%rbp), %rdi
	movq	%rdi, -1672(%rbp)
	movq	-1672(%rbp), %rdi
	movq	-1688(%rbp), %r8
	movq	%r8, -1616(%rbp)
	movq	-1616(%rbp), %r8
	movq	%rdi, -1648(%rbp)
	movq	%r8, -1656(%rbp)
	movq	-1648(%rbp), %rdi
	movq	%rdi, -1640(%rbp)
	movq	-1640(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -1660(%rbp)
	movq	-1656(%rbp), %rdi
	movq	%rdi, -1624(%rbp)
	movq	-1624(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1648(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -1632(%rbp)
	movq	-1632(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-1656(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1852(%rbp)
	movq	%rdx, -1712(%rbp)
	movq	%rcx, -1720(%rbp)
	movq	%rax, -1728(%rbp)
	movl	-2324(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -2324(%rbp)
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_25
LBB39_24:                               ##   in Loop: Header=BB39_20 Depth=3
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_20
LBB39_25:                               ##   in Loop: Header=BB39_2 Depth=2
	jmp	LBB39_26
LBB39_26:                               ##   in Loop: Header=BB39_2 Depth=2
	movq	-2344(%rbp), %rax
	cmpq	-2352(%rbp), %rax
	jne	LBB39_28
## BB#27:
	jmp	LBB39_73
LBB39_28:                               ##   in Loop: Header=BB39_2 Depth=2
	jmp	LBB39_29
LBB39_29:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_2 Depth=2
                                        ## =>    This Loop Header: Depth=3
                                        ##         Child Loop BB39_30 Depth 4
                                        ##         Child Loop BB39_33 Depth 4
	jmp	LBB39_30
LBB39_30:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_2 Depth=2
                                        ##       Parent Loop BB39_29 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	movq	-2288(%rbp), %rdi
	movq	-2272(%rbp), %rsi
	movq	-2344(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	xorb	$-1, %al
	testb	$1, %al
	jne	LBB39_31
	jmp	LBB39_32
LBB39_31:                               ##   in Loop: Header=BB39_30 Depth=4
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_30
LBB39_32:                               ##   in Loop: Header=BB39_29 Depth=3
	jmp	LBB39_33
LBB39_33:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_2 Depth=2
                                        ##       Parent Loop BB39_29 Depth=3
                                        ## =>      This Inner Loop Header: Depth=4
	movq	-2288(%rbp), %rdi
	movq	-2272(%rbp), %rsi
	movq	-2352(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -2352(%rbp)
	movq	%rax, %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_34
	jmp	LBB39_35
LBB39_34:                               ##   in Loop: Header=BB39_33 Depth=4
	jmp	LBB39_33
LBB39_35:                               ##   in Loop: Header=BB39_29 Depth=3
	movq	-2344(%rbp), %rax
	cmpq	-2352(%rbp), %rax
	jb	LBB39_37
## BB#36:                               ##   in Loop: Header=BB39_2 Depth=2
	jmp	LBB39_38
LBB39_37:                               ##   in Loop: Header=BB39_29 Depth=3
	leaq	-1476(%rbp), %rax
	leaq	-1472(%rbp), %rcx
	leaq	-1468(%rbp), %rdx
	leaq	-1284(%rbp), %rsi
	leaq	-1188(%rbp), %rdi
	leaq	-1404(%rbp), %r8
	movq	-2344(%rbp), %r9
	movq	-2352(%rbp), %r10
	movq	%r9, -1504(%rbp)
	movq	%r10, -1512(%rbp)
	movq	-1504(%rbp), %r9
	movq	-1512(%rbp), %r10
	movq	%r9, -1488(%rbp)
	movq	%r10, -1496(%rbp)
	movq	-1488(%rbp), %r9
	movq	-1496(%rbp), %r10
	movq	%r9, -1456(%rbp)
	movq	%r10, -1464(%rbp)
	movq	-1456(%rbp), %r9
	movq	%r9, %r10
	movq	-1464(%rbp), %r11
	movq	%r10, -1440(%rbp)
	movq	%r11, -1448(%rbp)
	movq	-1440(%rbp), %r10
	movq	-1448(%rbp), %r11
	movq	%r10, -1424(%rbp)
	movq	%r11, -1432(%rbp)
	movq	-1424(%rbp), %r10
	movq	%r10, -1416(%rbp)
	movq	-1416(%rbp), %r10
	movq	-1432(%rbp), %r11
	movq	%r11, -1360(%rbp)
	movq	-1360(%rbp), %r11
	movq	%r10, -1392(%rbp)
	movq	%r11, -1400(%rbp)
	movq	-1392(%rbp), %r10
	movq	%r10, -1384(%rbp)
	movq	-1384(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1404(%rbp)
	movq	-1400(%rbp), %r10
	movq	%r10, -1368(%rbp)
	movq	-1368(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1392(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1376(%rbp)
	movq	-1376(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1400(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1468(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1464(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1224(%rbp)
	movq	%r10, -1232(%rbp)
	movq	-1224(%rbp), %r8
	movq	-1232(%rbp), %r10
	movq	%r8, -1208(%rbp)
	movq	%r10, -1216(%rbp)
	movq	-1208(%rbp), %r8
	movq	%r8, -1200(%rbp)
	movq	-1200(%rbp), %r8
	movq	-1216(%rbp), %r10
	movq	%r10, -1144(%rbp)
	movq	-1144(%rbp), %r10
	movq	%r8, -1176(%rbp)
	movq	%r10, -1184(%rbp)
	movq	-1176(%rbp), %r8
	movq	%r8, -1168(%rbp)
	movq	-1168(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1188(%rbp)
	movq	-1184(%rbp), %r8
	movq	%r8, -1152(%rbp)
	movq	-1152(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1176(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1160(%rbp)
	movq	-1160(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1184(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1472(%rbp)
	addq	$8, %r9
	movq	-1464(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -1320(%rbp)
	movq	%rdi, -1328(%rbp)
	movq	-1320(%rbp), %rdi
	movq	-1328(%rbp), %r8
	movq	%rdi, -1304(%rbp)
	movq	%r8, -1312(%rbp)
	movq	-1304(%rbp), %rdi
	movq	%rdi, -1296(%rbp)
	movq	-1296(%rbp), %rdi
	movq	-1312(%rbp), %r8
	movq	%r8, -1240(%rbp)
	movq	-1240(%rbp), %r8
	movq	%rdi, -1272(%rbp)
	movq	%r8, -1280(%rbp)
	movq	-1272(%rbp), %rdi
	movq	%rdi, -1264(%rbp)
	movq	-1264(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -1284(%rbp)
	movq	-1280(%rbp), %rdi
	movq	%rdi, -1248(%rbp)
	movq	-1248(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1272(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -1256(%rbp)
	movq	-1256(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-1280(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1476(%rbp)
	movq	%rdx, -1336(%rbp)
	movq	%rcx, -1344(%rbp)
	movq	%rax, -1352(%rbp)
	movl	-2324(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -2324(%rbp)
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_29
LBB39_38:                               ##   in Loop: Header=BB39_2 Depth=2
	movq	-2344(%rbp), %rax
	movq	%rax, -2272(%rbp)
	jmp	LBB39_2
LBB39_39:                               ##   in Loop: Header=BB39_17 Depth=3
	movq	-2288(%rbp), %rdi
	movq	-2352(%rbp), %rsi
	movq	-2312(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_40
	jmp	LBB39_41
LBB39_40:                               ##   in Loop: Header=BB39_1 Depth=1
	leaq	-1100(%rbp), %rax
	leaq	-1096(%rbp), %rcx
	leaq	-1092(%rbp), %rdx
	leaq	-908(%rbp), %rsi
	leaq	-812(%rbp), %rdi
	leaq	-1028(%rbp), %r8
	movq	-2344(%rbp), %r9
	movq	-2352(%rbp), %r10
	movq	%r9, -1128(%rbp)
	movq	%r10, -1136(%rbp)
	movq	-1128(%rbp), %r9
	movq	-1136(%rbp), %r10
	movq	%r9, -1112(%rbp)
	movq	%r10, -1120(%rbp)
	movq	-1112(%rbp), %r9
	movq	-1120(%rbp), %r10
	movq	%r9, -1080(%rbp)
	movq	%r10, -1088(%rbp)
	movq	-1080(%rbp), %r9
	movq	%r9, %r10
	movq	-1088(%rbp), %r11
	movq	%r10, -1064(%rbp)
	movq	%r11, -1072(%rbp)
	movq	-1064(%rbp), %r10
	movq	-1072(%rbp), %r11
	movq	%r10, -1048(%rbp)
	movq	%r11, -1056(%rbp)
	movq	-1048(%rbp), %r10
	movq	%r10, -1040(%rbp)
	movq	-1040(%rbp), %r10
	movq	-1056(%rbp), %r11
	movq	%r11, -984(%rbp)
	movq	-984(%rbp), %r11
	movq	%r10, -1016(%rbp)
	movq	%r11, -1024(%rbp)
	movq	-1016(%rbp), %r10
	movq	%r10, -1008(%rbp)
	movq	-1008(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1028(%rbp)
	movq	-1024(%rbp), %r10
	movq	%r10, -992(%rbp)
	movq	-992(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1016(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1000(%rbp)
	movq	-1000(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1024(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1092(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1088(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -848(%rbp)
	movq	%r10, -856(%rbp)
	movq	-848(%rbp), %r8
	movq	-856(%rbp), %r10
	movq	%r8, -832(%rbp)
	movq	%r10, -840(%rbp)
	movq	-832(%rbp), %r8
	movq	%r8, -824(%rbp)
	movq	-824(%rbp), %r8
	movq	-840(%rbp), %r10
	movq	%r10, -768(%rbp)
	movq	-768(%rbp), %r10
	movq	%r8, -800(%rbp)
	movq	%r10, -808(%rbp)
	movq	-800(%rbp), %r8
	movq	%r8, -792(%rbp)
	movq	-792(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -812(%rbp)
	movq	-808(%rbp), %r8
	movq	%r8, -776(%rbp)
	movq	-776(%rbp), %r8
	movl	(%r8), %ebx
	movq	-800(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -784(%rbp)
	movq	-784(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-808(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1096(%rbp)
	addq	$8, %r9
	movq	-1088(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -944(%rbp)
	movq	%rdi, -952(%rbp)
	movq	-944(%rbp), %rdi
	movq	-952(%rbp), %r8
	movq	%rdi, -928(%rbp)
	movq	%r8, -936(%rbp)
	movq	-928(%rbp), %rdi
	movq	%rdi, -920(%rbp)
	movq	-920(%rbp), %rdi
	movq	-936(%rbp), %r8
	movq	%r8, -864(%rbp)
	movq	-864(%rbp), %r8
	movq	%rdi, -896(%rbp)
	movq	%r8, -904(%rbp)
	movq	-896(%rbp), %rdi
	movq	%rdi, -888(%rbp)
	movq	-888(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -908(%rbp)
	movq	-904(%rbp), %rdi
	movq	%rdi, -872(%rbp)
	movq	-872(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-896(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -880(%rbp)
	movq	-880(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-904(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1100(%rbp)
	movq	%rdx, -960(%rbp)
	movq	%rcx, -968(%rbp)
	movq	%rax, -976(%rbp)
	movl	-2324(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -2324(%rbp)
	jmp	LBB39_42
LBB39_41:                               ##   in Loop: Header=BB39_17 Depth=3
	jmp	LBB39_17
LBB39_42:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_43
LBB39_43:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	movq	-2344(%rbp), %rax
	cmpq	-2352(%rbp), %rax
	jae	LBB39_57
## BB#44:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_45
LBB39_45:                               ##   Parent Loop BB39_1 Depth=1
                                        ## =>  This Loop Header: Depth=2
                                        ##       Child Loop BB39_46 Depth 3
                                        ##       Child Loop BB39_49 Depth 3
	jmp	LBB39_46
LBB39_46:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_45 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	-2288(%rbp), %rdi
	movq	-2344(%rbp), %rsi
	movq	-2312(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_47
	jmp	LBB39_48
LBB39_47:                               ##   in Loop: Header=BB39_46 Depth=3
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_46
LBB39_48:                               ##   in Loop: Header=BB39_45 Depth=2
	jmp	LBB39_49
LBB39_49:                               ##   Parent Loop BB39_1 Depth=1
                                        ##     Parent Loop BB39_45 Depth=2
                                        ## =>    This Inner Loop Header: Depth=3
	movq	-2288(%rbp), %rdi
	movq	-2352(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -2352(%rbp)
	movq	-2312(%rbp), %rdx
	movq	%rax, %rsi
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	xorb	$-1, %al
	testb	$1, %al
	jne	LBB39_50
	jmp	LBB39_51
LBB39_50:                               ##   in Loop: Header=BB39_49 Depth=3
	jmp	LBB39_49
LBB39_51:                               ##   in Loop: Header=BB39_45 Depth=2
	movq	-2344(%rbp), %rax
	cmpq	-2352(%rbp), %rax
	jbe	LBB39_53
## BB#52:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_56
LBB39_53:                               ##   in Loop: Header=BB39_45 Depth=2
	leaq	-724(%rbp), %rax
	leaq	-720(%rbp), %rcx
	leaq	-716(%rbp), %rdx
	leaq	-532(%rbp), %rsi
	leaq	-436(%rbp), %rdi
	leaq	-652(%rbp), %r8
	movq	-2344(%rbp), %r9
	movq	-2352(%rbp), %r10
	movq	%r9, -752(%rbp)
	movq	%r10, -760(%rbp)
	movq	-752(%rbp), %r9
	movq	-760(%rbp), %r10
	movq	%r9, -736(%rbp)
	movq	%r10, -744(%rbp)
	movq	-736(%rbp), %r9
	movq	-744(%rbp), %r10
	movq	%r9, -704(%rbp)
	movq	%r10, -712(%rbp)
	movq	-704(%rbp), %r9
	movq	%r9, %r10
	movq	-712(%rbp), %r11
	movq	%r10, -688(%rbp)
	movq	%r11, -696(%rbp)
	movq	-688(%rbp), %r10
	movq	-696(%rbp), %r11
	movq	%r10, -672(%rbp)
	movq	%r11, -680(%rbp)
	movq	-672(%rbp), %r10
	movq	%r10, -664(%rbp)
	movq	-664(%rbp), %r10
	movq	-680(%rbp), %r11
	movq	%r11, -608(%rbp)
	movq	-608(%rbp), %r11
	movq	%r10, -640(%rbp)
	movq	%r11, -648(%rbp)
	movq	-640(%rbp), %r10
	movq	%r10, -632(%rbp)
	movq	-632(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -652(%rbp)
	movq	-648(%rbp), %r10
	movq	%r10, -616(%rbp)
	movq	-616(%rbp), %r10
	movl	(%r10), %ebx
	movq	-640(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -624(%rbp)
	movq	-624(%rbp), %r8
	movl	(%r8), %ebx
	movq	-648(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -716(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-712(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %r8
	movq	-480(%rbp), %r10
	movq	%r8, -456(%rbp)
	movq	%r10, -464(%rbp)
	movq	-456(%rbp), %r8
	movq	%r8, -448(%rbp)
	movq	-448(%rbp), %r8
	movq	-464(%rbp), %r10
	movq	%r10, -392(%rbp)
	movq	-392(%rbp), %r10
	movq	%r8, -424(%rbp)
	movq	%r10, -432(%rbp)
	movq	-424(%rbp), %r8
	movq	%r8, -416(%rbp)
	movq	-416(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -436(%rbp)
	movq	-432(%rbp), %r8
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movl	(%r8), %ebx
	movq	-424(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -408(%rbp)
	movq	-408(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-432(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -720(%rbp)
	addq	$8, %r9
	movq	-712(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-568(%rbp), %rdi
	movq	-576(%rbp), %r8
	movq	%rdi, -552(%rbp)
	movq	%r8, -560(%rbp)
	movq	-552(%rbp), %rdi
	movq	%rdi, -544(%rbp)
	movq	-544(%rbp), %rdi
	movq	-560(%rbp), %r8
	movq	%r8, -488(%rbp)
	movq	-488(%rbp), %r8
	movq	%rdi, -520(%rbp)
	movq	%r8, -528(%rbp)
	movq	-520(%rbp), %rdi
	movq	%rdi, -512(%rbp)
	movq	-512(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -532(%rbp)
	movq	-528(%rbp), %rdi
	movq	%rdi, -496(%rbp)
	movq	-496(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-520(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -504(%rbp)
	movq	-504(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-528(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -724(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movl	-2324(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -2324(%rbp)
	movq	-2312(%rbp), %rax
	cmpq	-2344(%rbp), %rax
	jne	LBB39_55
## BB#54:                               ##   in Loop: Header=BB39_45 Depth=2
	movq	-2352(%rbp), %rax
	movq	%rax, -2312(%rbp)
LBB39_55:                               ##   in Loop: Header=BB39_45 Depth=2
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	jmp	LBB39_45
LBB39_56:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_57
LBB39_57:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2344(%rbp), %rax
	cmpq	-2312(%rbp), %rax
	je	LBB39_60
## BB#58:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2288(%rbp), %rdi
	movq	-2312(%rbp), %rsi
	movq	-2344(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB39_59
	jmp	LBB39_60
LBB39_59:                               ##   in Loop: Header=BB39_1 Depth=1
	leaq	-348(%rbp), %rax
	leaq	-344(%rbp), %rcx
	leaq	-340(%rbp), %rdx
	leaq	-156(%rbp), %rsi
	leaq	-60(%rbp), %rdi
	leaq	-276(%rbp), %r8
	movq	-2344(%rbp), %r9
	movq	-2312(%rbp), %r10
	movq	%r9, -376(%rbp)
	movq	%r10, -384(%rbp)
	movq	-376(%rbp), %r9
	movq	-384(%rbp), %r10
	movq	%r9, -360(%rbp)
	movq	%r10, -368(%rbp)
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%r9, -328(%rbp)
	movq	%r10, -336(%rbp)
	movq	-328(%rbp), %r9
	movq	%r9, %r10
	movq	-336(%rbp), %r11
	movq	%r10, -312(%rbp)
	movq	%r11, -320(%rbp)
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	movq	%r10, -296(%rbp)
	movq	%r11, -304(%rbp)
	movq	-296(%rbp), %r10
	movq	%r10, -288(%rbp)
	movq	-288(%rbp), %r10
	movq	-304(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	movq	-264(%rbp), %r10
	movq	%r10, -256(%rbp)
	movq	-256(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -276(%rbp)
	movq	-272(%rbp), %r10
	movq	%r10, -240(%rbp)
	movq	-240(%rbp), %r10
	movl	(%r10), %ebx
	movq	-264(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movl	(%r8), %ebx
	movq	-272(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -340(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-336(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	movq	%r8, -80(%rbp)
	movq	%r10, -88(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-16(%rbp), %r10
	movq	%r8, -48(%rbp)
	movq	%r10, -56(%rbp)
	movq	-48(%rbp), %r8
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -60(%rbp)
	movq	-56(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r8
	movl	(%r8), %ebx
	movq	-48(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-56(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -344(%rbp)
	addq	$8, %r9
	movq	-336(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	-200(%rbp), %r8
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%rdi, -144(%rbp)
	movq	%r8, -152(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -156(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-144(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-152(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -348(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movl	-2324(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -2324(%rbp)
LBB39_60:                               ##   in Loop: Header=BB39_1 Depth=1
	cmpl	$0, -2324(%rbp)
	jne	LBB39_69
## BB#61:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2272(%rbp), %rdi
	movq	-2344(%rbp), %rsi
	movq	-2288(%rbp), %rdx
	callq	__ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_
	andb	$1, %al
	movb	%al, -2353(%rbp)
	movq	-2344(%rbp), %rdx
	addq	$12, %rdx
	movq	-2280(%rbp), %rsi
	movq	-2288(%rbp), %rdi
	movq	%rdi, -2448(%rbp)       ## 8-byte Spill
	movq	%rdx, %rdi
	movq	-2448(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_
	testb	$1, %al
	jne	LBB39_62
	jmp	LBB39_65
LBB39_62:                               ##   in Loop: Header=BB39_1 Depth=1
	testb	$1, -2353(%rbp)
	je	LBB39_64
## BB#63:
	jmp	LBB39_73
LBB39_64:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2344(%rbp), %rax
	movq	%rax, -2280(%rbp)
	jmp	LBB39_1
LBB39_65:                               ##   in Loop: Header=BB39_1 Depth=1
	testb	$1, -2353(%rbp)
	je	LBB39_67
## BB#66:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -2344(%rbp)
	movq	%rax, -2272(%rbp)
	jmp	LBB39_1
LBB39_67:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_68
LBB39_68:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_69
LBB39_69:                               ##   in Loop: Header=BB39_1 Depth=1
	movl	$12, %eax
	movl	%eax, %ecx
	movq	-2344(%rbp), %rdx
	movq	-2272(%rbp), %rsi
	subq	%rsi, %rdx
	movq	%rdx, %rax
	cqto
	idivq	%rcx
	movq	-2280(%rbp), %rsi
	movq	-2344(%rbp), %rdi
	subq	%rdi, %rsi
	movq	%rax, -2456(%rbp)       ## 8-byte Spill
	movq	%rsi, %rax
	cqto
	idivq	%rcx
	movq	-2456(%rbp), %rcx       ## 8-byte Reload
	cmpq	%rax, %rcx
	jge	LBB39_71
## BB#70:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2272(%rbp), %rdi
	movq	-2344(%rbp), %rsi
	movq	-2288(%rbp), %rdx
	callq	__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	movq	-2344(%rbp), %rdx
	addq	$12, %rdx
	movq	%rdx, -2344(%rbp)
	movq	%rdx, -2272(%rbp)
	jmp	LBB39_72
LBB39_71:                               ##   in Loop: Header=BB39_1 Depth=1
	movq	-2344(%rbp), %rax
	addq	$12, %rax
	movq	-2280(%rbp), %rsi
	movq	-2288(%rbp), %rdx
	movq	%rax, %rdi
	callq	__ZNSt3__16__sortIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	movq	-2344(%rbp), %rax
	movq	%rax, -2280(%rbp)
LBB39_72:                               ##   in Loop: Header=BB39_1 Depth=1
	jmp	LBB39_1
LBB39_73:
	addq	$2456, %rsp             ## imm = 0x998
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L39_0_set_3 = LBB39_3-LJTI39_0
L39_0_set_4 = LBB39_4-LJTI39_0
L39_0_set_7 = LBB39_7-LJTI39_0
L39_0_set_8 = LBB39_8-LJTI39_0
L39_0_set_9 = LBB39_9-LJTI39_0
LJTI39_0:
	.long	L39_0_set_3
	.long	L39_0_set_3
	.long	L39_0_set_4
	.long	L39_0_set_7
	.long	L39_0_set_8
	.long	L39_0_set_9

	.globl	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	.weak_def_can_be_hidden	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	.align	4, 0x90
__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_: ## @_ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp163:
	.cfi_def_cfa_offset 16
Ltmp164:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp165:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -320(%rbp)
	movq	%rsi, -328(%rbp)
	movq	%rdx, -336(%rbp)
	movq	-320(%rbp), %rdx
	movq	-328(%rbp), %rdi
	movq	%rdx, -376(%rbp)        ## 8-byte Spill
	callq	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE
	movq	%rax, -352(%rbp)
	movq	%rdx, -344(%rbp)
	movq	-336(%rbp), %rdi
	callq	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE
	leaq	-280(%rbp), %rsi
	leaq	-368(%rbp), %rdi
	leaq	-352(%rbp), %rcx
	movq	%rax, -368(%rbp)
	movq	%rdx, -360(%rbp)
	movq	-376(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%rdi, -312(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-288(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rax, -264(%rbp)
	movq	%rcx, -272(%rbp)
	movq	-264(%rbp), %rax
	movq	-272(%rbp), %rcx
	movq	%rsi, -224(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	$0, -248(%rbp)
	movq	-232(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %r8d
	movq	-240(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	cmpl	(%rax), %r8d
	jge	LBB40_2
## BB#1:
	movb	$1, -209(%rbp)
	jmp	LBB40_10
LBB40_2:
	movq	-240(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	-232(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	(%rax), %rax
	cmpl	(%rax), %ecx
	jge	LBB40_4
## BB#3:
	movb	$0, -209(%rbp)
	jmp	LBB40_10
LBB40_4:
	leaq	-256(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rdx, -176(%rbp)
	movq	$1, -184(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %esi
	movq	-176(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	cmpl	(%rax), %esi
	jge	LBB40_6
## BB#5:
	movb	$1, -145(%rbp)
	jmp	LBB40_9
LBB40_6:
	movq	-176(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	movq	-168(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	cmpl	(%rax), %ecx
	jge	LBB40_8
## BB#7:
	movb	$0, -145(%rbp)
	jmp	LBB40_9
LBB40_8:
	leaq	-192(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	%rax, -112(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movb	$0, -145(%rbp)
LBB40_9:                                ## %_ZNSt3__112__tuple_lessILm1EEclINS_5tupleIJRKiS5_EEES6_EEbRKT_RKT0_.exit.i.i.i
	movb	-145(%rbp), %al
	andb	$1, %al
	movb	%al, -209(%rbp)
LBB40_10:                               ## %_ZNKSt3__14lessIvEclINS_5tupleIJRKiS5_EEES6_EEDTltclsr3std3__1E7forwardIT_Efp_Eclsr3std3__1E7forwardIT0_Efp0_EEOS7_OS8_.exit
	movb	-209(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	.align	4, 0x90
__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_: ## @_ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp166:
	.cfi_def_cfa_offset 16
Ltmp167:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp168:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$1928, %rsp             ## imm = 0x788
Ltmp169:
	.cfi_offset %rbx, -24
	movq	%rdi, -1904(%rbp)
	movq	%rsi, -1912(%rbp)
	movq	%rdx, -1920(%rbp)
	movq	%rcx, -1928(%rbp)
	movl	$0, -1932(%rbp)
	movq	-1928(%rbp), %rdi
	movq	-1912(%rbp), %rsi
	movq	-1904(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB41_6
## BB#1:
	movq	-1928(%rbp), %rdi
	movq	-1920(%rbp), %rsi
	movq	-1912(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB41_3
## BB#2:
	movl	-1932(%rbp), %eax
	movl	%eax, -1892(%rbp)
	jmp	LBB41_11
LBB41_3:
	leaq	-1852(%rbp), %rax
	leaq	-1848(%rbp), %rcx
	leaq	-1844(%rbp), %rdx
	leaq	-1660(%rbp), %rsi
	leaq	-1564(%rbp), %rdi
	leaq	-1780(%rbp), %r8
	movq	-1912(%rbp), %r9
	movq	-1920(%rbp), %r10
	movq	%r9, -1880(%rbp)
	movq	%r10, -1888(%rbp)
	movq	-1880(%rbp), %r9
	movq	-1888(%rbp), %r10
	movq	%r9, -1864(%rbp)
	movq	%r10, -1872(%rbp)
	movq	-1864(%rbp), %r9
	movq	-1872(%rbp), %r10
	movq	%r9, -1832(%rbp)
	movq	%r10, -1840(%rbp)
	movq	-1832(%rbp), %r9
	movq	%r9, %r10
	movq	-1840(%rbp), %r11
	movq	%r10, -1816(%rbp)
	movq	%r11, -1824(%rbp)
	movq	-1816(%rbp), %r10
	movq	-1824(%rbp), %r11
	movq	%r10, -1800(%rbp)
	movq	%r11, -1808(%rbp)
	movq	-1800(%rbp), %r10
	movq	%r10, -1792(%rbp)
	movq	-1792(%rbp), %r10
	movq	-1808(%rbp), %r11
	movq	%r11, -1736(%rbp)
	movq	-1736(%rbp), %r11
	movq	%r10, -1768(%rbp)
	movq	%r11, -1776(%rbp)
	movq	-1768(%rbp), %r10
	movq	%r10, -1760(%rbp)
	movq	-1760(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1780(%rbp)
	movq	-1776(%rbp), %r10
	movq	%r10, -1744(%rbp)
	movq	-1744(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1768(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1752(%rbp)
	movq	-1752(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1776(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1844(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1840(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1600(%rbp)
	movq	%r10, -1608(%rbp)
	movq	-1600(%rbp), %r8
	movq	-1608(%rbp), %r10
	movq	%r8, -1584(%rbp)
	movq	%r10, -1592(%rbp)
	movq	-1584(%rbp), %r8
	movq	%r8, -1576(%rbp)
	movq	-1576(%rbp), %r8
	movq	-1592(%rbp), %r10
	movq	%r10, -1520(%rbp)
	movq	-1520(%rbp), %r10
	movq	%r8, -1552(%rbp)
	movq	%r10, -1560(%rbp)
	movq	-1552(%rbp), %r8
	movq	%r8, -1544(%rbp)
	movq	-1544(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1564(%rbp)
	movq	-1560(%rbp), %r8
	movq	%r8, -1528(%rbp)
	movq	-1528(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1552(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1536(%rbp)
	movq	-1536(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1560(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1848(%rbp)
	addq	$8, %r9
	movq	-1840(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -1696(%rbp)
	movq	%rdi, -1704(%rbp)
	movq	-1696(%rbp), %rdi
	movq	-1704(%rbp), %r8
	movq	%rdi, -1680(%rbp)
	movq	%r8, -1688(%rbp)
	movq	-1680(%rbp), %rdi
	movq	%rdi, -1672(%rbp)
	movq	-1672(%rbp), %rdi
	movq	-1688(%rbp), %r8
	movq	%r8, -1616(%rbp)
	movq	-1616(%rbp), %r8
	movq	%rdi, -1648(%rbp)
	movq	%r8, -1656(%rbp)
	movq	-1648(%rbp), %rdi
	movq	%rdi, -1640(%rbp)
	movq	-1640(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -1660(%rbp)
	movq	-1656(%rbp), %rdi
	movq	%rdi, -1624(%rbp)
	movq	-1624(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1648(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -1632(%rbp)
	movq	-1632(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-1656(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1852(%rbp)
	movq	%rdx, -1712(%rbp)
	movq	%rcx, -1720(%rbp)
	movq	%rax, -1728(%rbp)
	movl	$1, -1932(%rbp)
	movq	-1928(%rbp), %rdi
	movq	-1912(%rbp), %rsi
	movq	-1904(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB41_4
	jmp	LBB41_5
LBB41_4:
	leaq	-1100(%rbp), %rax
	leaq	-1096(%rbp), %rcx
	leaq	-1092(%rbp), %rdx
	leaq	-908(%rbp), %rsi
	leaq	-812(%rbp), %rdi
	leaq	-1028(%rbp), %r8
	movq	-1904(%rbp), %r9
	movq	-1912(%rbp), %r10
	movq	%r9, -1128(%rbp)
	movq	%r10, -1136(%rbp)
	movq	-1128(%rbp), %r9
	movq	-1136(%rbp), %r10
	movq	%r9, -1112(%rbp)
	movq	%r10, -1120(%rbp)
	movq	-1112(%rbp), %r9
	movq	-1120(%rbp), %r10
	movq	%r9, -1080(%rbp)
	movq	%r10, -1088(%rbp)
	movq	-1080(%rbp), %r9
	movq	%r9, %r10
	movq	-1088(%rbp), %r11
	movq	%r10, -1064(%rbp)
	movq	%r11, -1072(%rbp)
	movq	-1064(%rbp), %r10
	movq	-1072(%rbp), %r11
	movq	%r10, -1048(%rbp)
	movq	%r11, -1056(%rbp)
	movq	-1048(%rbp), %r10
	movq	%r10, -1040(%rbp)
	movq	-1040(%rbp), %r10
	movq	-1056(%rbp), %r11
	movq	%r11, -984(%rbp)
	movq	-984(%rbp), %r11
	movq	%r10, -1016(%rbp)
	movq	%r11, -1024(%rbp)
	movq	-1016(%rbp), %r10
	movq	%r10, -1008(%rbp)
	movq	-1008(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1028(%rbp)
	movq	-1024(%rbp), %r10
	movq	%r10, -992(%rbp)
	movq	-992(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1016(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1000(%rbp)
	movq	-1000(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1024(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1092(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1088(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -848(%rbp)
	movq	%r10, -856(%rbp)
	movq	-848(%rbp), %r8
	movq	-856(%rbp), %r10
	movq	%r8, -832(%rbp)
	movq	%r10, -840(%rbp)
	movq	-832(%rbp), %r8
	movq	%r8, -824(%rbp)
	movq	-824(%rbp), %r8
	movq	-840(%rbp), %r10
	movq	%r10, -768(%rbp)
	movq	-768(%rbp), %r10
	movq	%r8, -800(%rbp)
	movq	%r10, -808(%rbp)
	movq	-800(%rbp), %r8
	movq	%r8, -792(%rbp)
	movq	-792(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -812(%rbp)
	movq	-808(%rbp), %r8
	movq	%r8, -776(%rbp)
	movq	-776(%rbp), %r8
	movl	(%r8), %ebx
	movq	-800(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -784(%rbp)
	movq	-784(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-808(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1096(%rbp)
	addq	$8, %r9
	movq	-1088(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -944(%rbp)
	movq	%rdi, -952(%rbp)
	movq	-944(%rbp), %rdi
	movq	-952(%rbp), %r8
	movq	%rdi, -928(%rbp)
	movq	%r8, -936(%rbp)
	movq	-928(%rbp), %rdi
	movq	%rdi, -920(%rbp)
	movq	-920(%rbp), %rdi
	movq	-936(%rbp), %r8
	movq	%r8, -864(%rbp)
	movq	-864(%rbp), %r8
	movq	%rdi, -896(%rbp)
	movq	%r8, -904(%rbp)
	movq	-896(%rbp), %rdi
	movq	%rdi, -888(%rbp)
	movq	-888(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -908(%rbp)
	movq	-904(%rbp), %rdi
	movq	%rdi, -872(%rbp)
	movq	-872(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-896(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -880(%rbp)
	movq	-880(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-904(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1100(%rbp)
	movq	%rdx, -960(%rbp)
	movq	%rcx, -968(%rbp)
	movq	%rax, -976(%rbp)
	movl	$2, -1932(%rbp)
LBB41_5:
	movl	-1932(%rbp), %eax
	movl	%eax, -1892(%rbp)
	jmp	LBB41_11
LBB41_6:
	movq	-1928(%rbp), %rdi
	movq	-1920(%rbp), %rsi
	movq	-1912(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB41_7
	jmp	LBB41_8
LBB41_7:
	leaq	-348(%rbp), %rax
	leaq	-344(%rbp), %rcx
	leaq	-340(%rbp), %rdx
	leaq	-156(%rbp), %rsi
	leaq	-60(%rbp), %rdi
	leaq	-276(%rbp), %r8
	movq	-1904(%rbp), %r9
	movq	-1920(%rbp), %r10
	movq	%r9, -376(%rbp)
	movq	%r10, -384(%rbp)
	movq	-376(%rbp), %r9
	movq	-384(%rbp), %r10
	movq	%r9, -360(%rbp)
	movq	%r10, -368(%rbp)
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%r9, -328(%rbp)
	movq	%r10, -336(%rbp)
	movq	-328(%rbp), %r9
	movq	%r9, %r10
	movq	-336(%rbp), %r11
	movq	%r10, -312(%rbp)
	movq	%r11, -320(%rbp)
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	movq	%r10, -296(%rbp)
	movq	%r11, -304(%rbp)
	movq	-296(%rbp), %r10
	movq	%r10, -288(%rbp)
	movq	-288(%rbp), %r10
	movq	-304(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	movq	-264(%rbp), %r10
	movq	%r10, -256(%rbp)
	movq	-256(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -276(%rbp)
	movq	-272(%rbp), %r10
	movq	%r10, -240(%rbp)
	movq	-240(%rbp), %r10
	movl	(%r10), %ebx
	movq	-264(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movl	(%r8), %ebx
	movq	-272(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -340(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-336(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	movq	%r8, -80(%rbp)
	movq	%r10, -88(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-16(%rbp), %r10
	movq	%r8, -48(%rbp)
	movq	%r10, -56(%rbp)
	movq	-48(%rbp), %r8
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -60(%rbp)
	movq	-56(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r8
	movl	(%r8), %ebx
	movq	-48(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-56(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -344(%rbp)
	addq	$8, %r9
	movq	-336(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	-200(%rbp), %r8
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%rdi, -144(%rbp)
	movq	%r8, -152(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -156(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-144(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-152(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -348(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movl	$1, -1932(%rbp)
	movl	-1932(%rbp), %ebx
	movl	%ebx, -1892(%rbp)
	jmp	LBB41_11
LBB41_8:
	leaq	-724(%rbp), %rax
	leaq	-720(%rbp), %rcx
	leaq	-716(%rbp), %rdx
	leaq	-532(%rbp), %rsi
	leaq	-436(%rbp), %rdi
	leaq	-652(%rbp), %r8
	movq	-1904(%rbp), %r9
	movq	-1912(%rbp), %r10
	movq	%r9, -752(%rbp)
	movq	%r10, -760(%rbp)
	movq	-752(%rbp), %r9
	movq	-760(%rbp), %r10
	movq	%r9, -736(%rbp)
	movq	%r10, -744(%rbp)
	movq	-736(%rbp), %r9
	movq	-744(%rbp), %r10
	movq	%r9, -704(%rbp)
	movq	%r10, -712(%rbp)
	movq	-704(%rbp), %r9
	movq	%r9, %r10
	movq	-712(%rbp), %r11
	movq	%r10, -688(%rbp)
	movq	%r11, -696(%rbp)
	movq	-688(%rbp), %r10
	movq	-696(%rbp), %r11
	movq	%r10, -672(%rbp)
	movq	%r11, -680(%rbp)
	movq	-672(%rbp), %r10
	movq	%r10, -664(%rbp)
	movq	-664(%rbp), %r10
	movq	-680(%rbp), %r11
	movq	%r11, -608(%rbp)
	movq	-608(%rbp), %r11
	movq	%r10, -640(%rbp)
	movq	%r11, -648(%rbp)
	movq	-640(%rbp), %r10
	movq	%r10, -632(%rbp)
	movq	-632(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -652(%rbp)
	movq	-648(%rbp), %r10
	movq	%r10, -616(%rbp)
	movq	-616(%rbp), %r10
	movl	(%r10), %ebx
	movq	-640(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -624(%rbp)
	movq	-624(%rbp), %r8
	movl	(%r8), %ebx
	movq	-648(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -716(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-712(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %r8
	movq	-480(%rbp), %r10
	movq	%r8, -456(%rbp)
	movq	%r10, -464(%rbp)
	movq	-456(%rbp), %r8
	movq	%r8, -448(%rbp)
	movq	-448(%rbp), %r8
	movq	-464(%rbp), %r10
	movq	%r10, -392(%rbp)
	movq	-392(%rbp), %r10
	movq	%r8, -424(%rbp)
	movq	%r10, -432(%rbp)
	movq	-424(%rbp), %r8
	movq	%r8, -416(%rbp)
	movq	-416(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -436(%rbp)
	movq	-432(%rbp), %r8
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movl	(%r8), %ebx
	movq	-424(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -408(%rbp)
	movq	-408(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-432(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -720(%rbp)
	addq	$8, %r9
	movq	-712(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-568(%rbp), %rdi
	movq	-576(%rbp), %r8
	movq	%rdi, -552(%rbp)
	movq	%r8, -560(%rbp)
	movq	-552(%rbp), %rdi
	movq	%rdi, -544(%rbp)
	movq	-544(%rbp), %rdi
	movq	-560(%rbp), %r8
	movq	%r8, -488(%rbp)
	movq	-488(%rbp), %r8
	movq	%rdi, -520(%rbp)
	movq	%r8, -528(%rbp)
	movq	-520(%rbp), %rdi
	movq	%rdi, -512(%rbp)
	movq	-512(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -532(%rbp)
	movq	-528(%rbp), %rdi
	movq	%rdi, -496(%rbp)
	movq	-496(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-520(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -504(%rbp)
	movq	-504(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-528(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -724(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movl	$1, -1932(%rbp)
	movq	-1928(%rbp), %rdi
	movq	-1920(%rbp), %rsi
	movq	-1912(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB41_9
	jmp	LBB41_10
LBB41_9:
	leaq	-1476(%rbp), %rax
	leaq	-1472(%rbp), %rcx
	leaq	-1468(%rbp), %rdx
	leaq	-1284(%rbp), %rsi
	leaq	-1188(%rbp), %rdi
	leaq	-1404(%rbp), %r8
	movq	-1912(%rbp), %r9
	movq	-1920(%rbp), %r10
	movq	%r9, -1504(%rbp)
	movq	%r10, -1512(%rbp)
	movq	-1504(%rbp), %r9
	movq	-1512(%rbp), %r10
	movq	%r9, -1488(%rbp)
	movq	%r10, -1496(%rbp)
	movq	-1488(%rbp), %r9
	movq	-1496(%rbp), %r10
	movq	%r9, -1456(%rbp)
	movq	%r10, -1464(%rbp)
	movq	-1456(%rbp), %r9
	movq	%r9, %r10
	movq	-1464(%rbp), %r11
	movq	%r10, -1440(%rbp)
	movq	%r11, -1448(%rbp)
	movq	-1440(%rbp), %r10
	movq	-1448(%rbp), %r11
	movq	%r10, -1424(%rbp)
	movq	%r11, -1432(%rbp)
	movq	-1424(%rbp), %r10
	movq	%r10, -1416(%rbp)
	movq	-1416(%rbp), %r10
	movq	-1432(%rbp), %r11
	movq	%r11, -1360(%rbp)
	movq	-1360(%rbp), %r11
	movq	%r10, -1392(%rbp)
	movq	%r11, -1400(%rbp)
	movq	-1392(%rbp), %r10
	movq	%r10, -1384(%rbp)
	movq	-1384(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1404(%rbp)
	movq	-1400(%rbp), %r10
	movq	%r10, -1368(%rbp)
	movq	-1368(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1392(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1376(%rbp)
	movq	-1376(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1400(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1468(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1464(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1224(%rbp)
	movq	%r10, -1232(%rbp)
	movq	-1224(%rbp), %r8
	movq	-1232(%rbp), %r10
	movq	%r8, -1208(%rbp)
	movq	%r10, -1216(%rbp)
	movq	-1208(%rbp), %r8
	movq	%r8, -1200(%rbp)
	movq	-1200(%rbp), %r8
	movq	-1216(%rbp), %r10
	movq	%r10, -1144(%rbp)
	movq	-1144(%rbp), %r10
	movq	%r8, -1176(%rbp)
	movq	%r10, -1184(%rbp)
	movq	-1176(%rbp), %r8
	movq	%r8, -1168(%rbp)
	movq	-1168(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1188(%rbp)
	movq	-1184(%rbp), %r8
	movq	%r8, -1152(%rbp)
	movq	-1152(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1176(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1160(%rbp)
	movq	-1160(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1184(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1472(%rbp)
	addq	$8, %r9
	movq	-1464(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -1320(%rbp)
	movq	%rdi, -1328(%rbp)
	movq	-1320(%rbp), %rdi
	movq	-1328(%rbp), %r8
	movq	%rdi, -1304(%rbp)
	movq	%r8, -1312(%rbp)
	movq	-1304(%rbp), %rdi
	movq	%rdi, -1296(%rbp)
	movq	-1296(%rbp), %rdi
	movq	-1312(%rbp), %r8
	movq	%r8, -1240(%rbp)
	movq	-1240(%rbp), %r8
	movq	%rdi, -1272(%rbp)
	movq	%r8, -1280(%rbp)
	movq	-1272(%rbp), %rdi
	movq	%rdi, -1264(%rbp)
	movq	-1264(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -1284(%rbp)
	movq	-1280(%rbp), %rdi
	movq	%rdi, -1248(%rbp)
	movq	-1248(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1272(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -1256(%rbp)
	movq	-1256(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-1280(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1476(%rbp)
	movq	%rdx, -1336(%rbp)
	movq	%rcx, -1344(%rbp)
	movq	%rax, -1352(%rbp)
	movl	$2, -1932(%rbp)
LBB41_10:
	movl	-1932(%rbp), %eax
	movl	%eax, -1892(%rbp)
LBB41_11:
	movl	-1892(%rbp), %eax
	addq	$1928, %rsp             ## imm = 0x788
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	.align	4, 0x90
__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_: ## @_ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp170:
	.cfi_def_cfa_offset 16
Ltmp171:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp172:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$1176, %rsp             ## imm = 0x498
Ltmp173:
	.cfi_offset %rbx, -24
	movq	%rdi, -1144(%rbp)
	movq	%rsi, -1152(%rbp)
	movq	%rdx, -1160(%rbp)
	movq	%rcx, -1168(%rbp)
	movq	%r8, -1176(%rbp)
	movq	-1144(%rbp), %rdi
	movq	-1152(%rbp), %rsi
	movq	-1160(%rbp), %rdx
	movq	-1176(%rbp), %rcx
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movl	%eax, -1180(%rbp)
	movq	-1176(%rbp), %rdi
	movq	-1168(%rbp), %rsi
	movq	-1160(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB42_1
	jmp	LBB42_6
LBB42_1:
	leaq	-1100(%rbp), %rax
	leaq	-1096(%rbp), %rcx
	leaq	-1092(%rbp), %rdx
	leaq	-908(%rbp), %rsi
	leaq	-812(%rbp), %rdi
	leaq	-1028(%rbp), %r8
	movq	-1160(%rbp), %r9
	movq	-1168(%rbp), %r10
	movq	%r9, -1128(%rbp)
	movq	%r10, -1136(%rbp)
	movq	-1128(%rbp), %r9
	movq	-1136(%rbp), %r10
	movq	%r9, -1112(%rbp)
	movq	%r10, -1120(%rbp)
	movq	-1112(%rbp), %r9
	movq	-1120(%rbp), %r10
	movq	%r9, -1080(%rbp)
	movq	%r10, -1088(%rbp)
	movq	-1080(%rbp), %r9
	movq	%r9, %r10
	movq	-1088(%rbp), %r11
	movq	%r10, -1064(%rbp)
	movq	%r11, -1072(%rbp)
	movq	-1064(%rbp), %r10
	movq	-1072(%rbp), %r11
	movq	%r10, -1048(%rbp)
	movq	%r11, -1056(%rbp)
	movq	-1048(%rbp), %r10
	movq	%r10, -1040(%rbp)
	movq	-1040(%rbp), %r10
	movq	-1056(%rbp), %r11
	movq	%r11, -984(%rbp)
	movq	-984(%rbp), %r11
	movq	%r10, -1016(%rbp)
	movq	%r11, -1024(%rbp)
	movq	-1016(%rbp), %r10
	movq	%r10, -1008(%rbp)
	movq	-1008(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1028(%rbp)
	movq	-1024(%rbp), %r10
	movq	%r10, -992(%rbp)
	movq	-992(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1016(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1000(%rbp)
	movq	-1000(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1024(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1092(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1088(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -848(%rbp)
	movq	%r10, -856(%rbp)
	movq	-848(%rbp), %r8
	movq	-856(%rbp), %r10
	movq	%r8, -832(%rbp)
	movq	%r10, -840(%rbp)
	movq	-832(%rbp), %r8
	movq	%r8, -824(%rbp)
	movq	-824(%rbp), %r8
	movq	-840(%rbp), %r10
	movq	%r10, -768(%rbp)
	movq	-768(%rbp), %r10
	movq	%r8, -800(%rbp)
	movq	%r10, -808(%rbp)
	movq	-800(%rbp), %r8
	movq	%r8, -792(%rbp)
	movq	-792(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -812(%rbp)
	movq	-808(%rbp), %r8
	movq	%r8, -776(%rbp)
	movq	-776(%rbp), %r8
	movl	(%r8), %ebx
	movq	-800(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -784(%rbp)
	movq	-784(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-808(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1096(%rbp)
	addq	$8, %r9
	movq	-1088(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -944(%rbp)
	movq	%rdi, -952(%rbp)
	movq	-944(%rbp), %rdi
	movq	-952(%rbp), %r8
	movq	%rdi, -928(%rbp)
	movq	%r8, -936(%rbp)
	movq	-928(%rbp), %rdi
	movq	%rdi, -920(%rbp)
	movq	-920(%rbp), %rdi
	movq	-936(%rbp), %r8
	movq	%r8, -864(%rbp)
	movq	-864(%rbp), %r8
	movq	%rdi, -896(%rbp)
	movq	%r8, -904(%rbp)
	movq	-896(%rbp), %rdi
	movq	%rdi, -888(%rbp)
	movq	-888(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -908(%rbp)
	movq	-904(%rbp), %rdi
	movq	%rdi, -872(%rbp)
	movq	-872(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-896(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -880(%rbp)
	movq	-880(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-904(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1100(%rbp)
	movq	%rdx, -960(%rbp)
	movq	%rcx, -968(%rbp)
	movq	%rax, -976(%rbp)
	movl	-1180(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1180(%rbp)
	movq	-1176(%rbp), %rdi
	movq	-1160(%rbp), %rsi
	movq	-1152(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB42_2
	jmp	LBB42_5
LBB42_2:
	leaq	-348(%rbp), %rax
	leaq	-344(%rbp), %rcx
	leaq	-340(%rbp), %rdx
	leaq	-156(%rbp), %rsi
	leaq	-60(%rbp), %rdi
	leaq	-276(%rbp), %r8
	movq	-1152(%rbp), %r9
	movq	-1160(%rbp), %r10
	movq	%r9, -376(%rbp)
	movq	%r10, -384(%rbp)
	movq	-376(%rbp), %r9
	movq	-384(%rbp), %r10
	movq	%r9, -360(%rbp)
	movq	%r10, -368(%rbp)
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%r9, -328(%rbp)
	movq	%r10, -336(%rbp)
	movq	-328(%rbp), %r9
	movq	%r9, %r10
	movq	-336(%rbp), %r11
	movq	%r10, -312(%rbp)
	movq	%r11, -320(%rbp)
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	movq	%r10, -296(%rbp)
	movq	%r11, -304(%rbp)
	movq	-296(%rbp), %r10
	movq	%r10, -288(%rbp)
	movq	-288(%rbp), %r10
	movq	-304(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	movq	-264(%rbp), %r10
	movq	%r10, -256(%rbp)
	movq	-256(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -276(%rbp)
	movq	-272(%rbp), %r10
	movq	%r10, -240(%rbp)
	movq	-240(%rbp), %r10
	movl	(%r10), %ebx
	movq	-264(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movl	(%r8), %ebx
	movq	-272(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -340(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-336(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	movq	%r8, -80(%rbp)
	movq	%r10, -88(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-16(%rbp), %r10
	movq	%r8, -48(%rbp)
	movq	%r10, -56(%rbp)
	movq	-48(%rbp), %r8
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -60(%rbp)
	movq	-56(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r8
	movl	(%r8), %ebx
	movq	-48(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-56(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -344(%rbp)
	addq	$8, %r9
	movq	-336(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	-200(%rbp), %r8
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%rdi, -144(%rbp)
	movq	%r8, -152(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -156(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-144(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-152(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -348(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movl	-1180(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1180(%rbp)
	movq	-1176(%rbp), %rdi
	movq	-1152(%rbp), %rsi
	movq	-1144(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB42_3
	jmp	LBB42_4
LBB42_3:
	leaq	-724(%rbp), %rax
	leaq	-720(%rbp), %rcx
	leaq	-716(%rbp), %rdx
	leaq	-532(%rbp), %rsi
	leaq	-436(%rbp), %rdi
	leaq	-652(%rbp), %r8
	movq	-1144(%rbp), %r9
	movq	-1152(%rbp), %r10
	movq	%r9, -752(%rbp)
	movq	%r10, -760(%rbp)
	movq	-752(%rbp), %r9
	movq	-760(%rbp), %r10
	movq	%r9, -736(%rbp)
	movq	%r10, -744(%rbp)
	movq	-736(%rbp), %r9
	movq	-744(%rbp), %r10
	movq	%r9, -704(%rbp)
	movq	%r10, -712(%rbp)
	movq	-704(%rbp), %r9
	movq	%r9, %r10
	movq	-712(%rbp), %r11
	movq	%r10, -688(%rbp)
	movq	%r11, -696(%rbp)
	movq	-688(%rbp), %r10
	movq	-696(%rbp), %r11
	movq	%r10, -672(%rbp)
	movq	%r11, -680(%rbp)
	movq	-672(%rbp), %r10
	movq	%r10, -664(%rbp)
	movq	-664(%rbp), %r10
	movq	-680(%rbp), %r11
	movq	%r11, -608(%rbp)
	movq	-608(%rbp), %r11
	movq	%r10, -640(%rbp)
	movq	%r11, -648(%rbp)
	movq	-640(%rbp), %r10
	movq	%r10, -632(%rbp)
	movq	-632(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -652(%rbp)
	movq	-648(%rbp), %r10
	movq	%r10, -616(%rbp)
	movq	-616(%rbp), %r10
	movl	(%r10), %ebx
	movq	-640(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -624(%rbp)
	movq	-624(%rbp), %r8
	movl	(%r8), %ebx
	movq	-648(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -716(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-712(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %r8
	movq	-480(%rbp), %r10
	movq	%r8, -456(%rbp)
	movq	%r10, -464(%rbp)
	movq	-456(%rbp), %r8
	movq	%r8, -448(%rbp)
	movq	-448(%rbp), %r8
	movq	-464(%rbp), %r10
	movq	%r10, -392(%rbp)
	movq	-392(%rbp), %r10
	movq	%r8, -424(%rbp)
	movq	%r10, -432(%rbp)
	movq	-424(%rbp), %r8
	movq	%r8, -416(%rbp)
	movq	-416(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -436(%rbp)
	movq	-432(%rbp), %r8
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movl	(%r8), %ebx
	movq	-424(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -408(%rbp)
	movq	-408(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-432(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -720(%rbp)
	addq	$8, %r9
	movq	-712(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-568(%rbp), %rdi
	movq	-576(%rbp), %r8
	movq	%rdi, -552(%rbp)
	movq	%r8, -560(%rbp)
	movq	-552(%rbp), %rdi
	movq	%rdi, -544(%rbp)
	movq	-544(%rbp), %rdi
	movq	-560(%rbp), %r8
	movq	%r8, -488(%rbp)
	movq	-488(%rbp), %r8
	movq	%rdi, -520(%rbp)
	movq	%r8, -528(%rbp)
	movq	-520(%rbp), %rdi
	movq	%rdi, -512(%rbp)
	movq	-512(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -532(%rbp)
	movq	-528(%rbp), %rdi
	movq	%rdi, -496(%rbp)
	movq	-496(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-520(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -504(%rbp)
	movq	-504(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-528(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -724(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movl	-1180(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1180(%rbp)
LBB42_4:
	jmp	LBB42_5
LBB42_5:
	jmp	LBB42_6
LBB42_6:
	movl	-1180(%rbp), %eax
	addq	$1176, %rsp             ## imm = 0x498
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	.align	4, 0x90
__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_: ## @_ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp174:
	.cfi_def_cfa_offset 16
Ltmp175:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp176:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$1560, %rsp             ## imm = 0x618
Ltmp177:
	.cfi_offset %rbx, -24
	movq	%rdi, -1520(%rbp)
	movq	%rsi, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rcx, -1544(%rbp)
	movq	%r8, -1552(%rbp)
	movq	%r9, -1560(%rbp)
	movq	-1520(%rbp), %rdi
	movq	-1528(%rbp), %rsi
	movq	-1536(%rbp), %rdx
	movq	-1544(%rbp), %rcx
	movq	-1560(%rbp), %r8
	callq	__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	movl	%eax, -1564(%rbp)
	movq	-1560(%rbp), %rdi
	movq	-1552(%rbp), %rsi
	movq	-1544(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB43_1
	jmp	LBB43_8
LBB43_1:
	leaq	-1476(%rbp), %rax
	leaq	-1472(%rbp), %rcx
	leaq	-1468(%rbp), %rdx
	leaq	-1284(%rbp), %rsi
	leaq	-1188(%rbp), %rdi
	leaq	-1404(%rbp), %r8
	movq	-1544(%rbp), %r9
	movq	-1552(%rbp), %r10
	movq	%r9, -1504(%rbp)
	movq	%r10, -1512(%rbp)
	movq	-1504(%rbp), %r9
	movq	-1512(%rbp), %r10
	movq	%r9, -1488(%rbp)
	movq	%r10, -1496(%rbp)
	movq	-1488(%rbp), %r9
	movq	-1496(%rbp), %r10
	movq	%r9, -1456(%rbp)
	movq	%r10, -1464(%rbp)
	movq	-1456(%rbp), %r9
	movq	%r9, %r10
	movq	-1464(%rbp), %r11
	movq	%r10, -1440(%rbp)
	movq	%r11, -1448(%rbp)
	movq	-1440(%rbp), %r10
	movq	-1448(%rbp), %r11
	movq	%r10, -1424(%rbp)
	movq	%r11, -1432(%rbp)
	movq	-1424(%rbp), %r10
	movq	%r10, -1416(%rbp)
	movq	-1416(%rbp), %r10
	movq	-1432(%rbp), %r11
	movq	%r11, -1360(%rbp)
	movq	-1360(%rbp), %r11
	movq	%r10, -1392(%rbp)
	movq	%r11, -1400(%rbp)
	movq	-1392(%rbp), %r10
	movq	%r10, -1384(%rbp)
	movq	-1384(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1404(%rbp)
	movq	-1400(%rbp), %r10
	movq	%r10, -1368(%rbp)
	movq	-1368(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1392(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1376(%rbp)
	movq	-1376(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1400(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1468(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1464(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -1224(%rbp)
	movq	%r10, -1232(%rbp)
	movq	-1224(%rbp), %r8
	movq	-1232(%rbp), %r10
	movq	%r8, -1208(%rbp)
	movq	%r10, -1216(%rbp)
	movq	-1208(%rbp), %r8
	movq	%r8, -1200(%rbp)
	movq	-1200(%rbp), %r8
	movq	-1216(%rbp), %r10
	movq	%r10, -1144(%rbp)
	movq	-1144(%rbp), %r10
	movq	%r8, -1176(%rbp)
	movq	%r10, -1184(%rbp)
	movq	-1176(%rbp), %r8
	movq	%r8, -1168(%rbp)
	movq	-1168(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -1188(%rbp)
	movq	-1184(%rbp), %r8
	movq	%r8, -1152(%rbp)
	movq	-1152(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1176(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -1160(%rbp)
	movq	-1160(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1184(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1472(%rbp)
	addq	$8, %r9
	movq	-1464(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -1320(%rbp)
	movq	%rdi, -1328(%rbp)
	movq	-1320(%rbp), %rdi
	movq	-1328(%rbp), %r8
	movq	%rdi, -1304(%rbp)
	movq	%r8, -1312(%rbp)
	movq	-1304(%rbp), %rdi
	movq	%rdi, -1296(%rbp)
	movq	-1296(%rbp), %rdi
	movq	-1312(%rbp), %r8
	movq	%r8, -1240(%rbp)
	movq	-1240(%rbp), %r8
	movq	%rdi, -1272(%rbp)
	movq	%r8, -1280(%rbp)
	movq	-1272(%rbp), %rdi
	movq	%rdi, -1264(%rbp)
	movq	-1264(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -1284(%rbp)
	movq	-1280(%rbp), %rdi
	movq	%rdi, -1248(%rbp)
	movq	-1248(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-1272(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -1256(%rbp)
	movq	-1256(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-1280(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1476(%rbp)
	movq	%rdx, -1336(%rbp)
	movq	%rcx, -1344(%rbp)
	movq	%rax, -1352(%rbp)
	movl	-1564(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1564(%rbp)
	movq	-1560(%rbp), %rdi
	movq	-1544(%rbp), %rsi
	movq	-1536(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB43_2
	jmp	LBB43_7
LBB43_2:
	leaq	-724(%rbp), %rax
	leaq	-720(%rbp), %rcx
	leaq	-716(%rbp), %rdx
	leaq	-532(%rbp), %rsi
	leaq	-436(%rbp), %rdi
	leaq	-652(%rbp), %r8
	movq	-1536(%rbp), %r9
	movq	-1544(%rbp), %r10
	movq	%r9, -752(%rbp)
	movq	%r10, -760(%rbp)
	movq	-752(%rbp), %r9
	movq	-760(%rbp), %r10
	movq	%r9, -736(%rbp)
	movq	%r10, -744(%rbp)
	movq	-736(%rbp), %r9
	movq	-744(%rbp), %r10
	movq	%r9, -704(%rbp)
	movq	%r10, -712(%rbp)
	movq	-704(%rbp), %r9
	movq	%r9, %r10
	movq	-712(%rbp), %r11
	movq	%r10, -688(%rbp)
	movq	%r11, -696(%rbp)
	movq	-688(%rbp), %r10
	movq	-696(%rbp), %r11
	movq	%r10, -672(%rbp)
	movq	%r11, -680(%rbp)
	movq	-672(%rbp), %r10
	movq	%r10, -664(%rbp)
	movq	-664(%rbp), %r10
	movq	-680(%rbp), %r11
	movq	%r11, -608(%rbp)
	movq	-608(%rbp), %r11
	movq	%r10, -640(%rbp)
	movq	%r11, -648(%rbp)
	movq	-640(%rbp), %r10
	movq	%r10, -632(%rbp)
	movq	-632(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -652(%rbp)
	movq	-648(%rbp), %r10
	movq	%r10, -616(%rbp)
	movq	-616(%rbp), %r10
	movl	(%r10), %ebx
	movq	-640(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -624(%rbp)
	movq	-624(%rbp), %r8
	movl	(%r8), %ebx
	movq	-648(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -716(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-712(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %r8
	movq	-480(%rbp), %r10
	movq	%r8, -456(%rbp)
	movq	%r10, -464(%rbp)
	movq	-456(%rbp), %r8
	movq	%r8, -448(%rbp)
	movq	-448(%rbp), %r8
	movq	-464(%rbp), %r10
	movq	%r10, -392(%rbp)
	movq	-392(%rbp), %r10
	movq	%r8, -424(%rbp)
	movq	%r10, -432(%rbp)
	movq	-424(%rbp), %r8
	movq	%r8, -416(%rbp)
	movq	-416(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -436(%rbp)
	movq	-432(%rbp), %r8
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movl	(%r8), %ebx
	movq	-424(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -408(%rbp)
	movq	-408(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-432(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -720(%rbp)
	addq	$8, %r9
	movq	-712(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-568(%rbp), %rdi
	movq	-576(%rbp), %r8
	movq	%rdi, -552(%rbp)
	movq	%r8, -560(%rbp)
	movq	-552(%rbp), %rdi
	movq	%rdi, -544(%rbp)
	movq	-544(%rbp), %rdi
	movq	-560(%rbp), %r8
	movq	%r8, -488(%rbp)
	movq	-488(%rbp), %r8
	movq	%rdi, -520(%rbp)
	movq	%r8, -528(%rbp)
	movq	-520(%rbp), %rdi
	movq	%rdi, -512(%rbp)
	movq	-512(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -532(%rbp)
	movq	-528(%rbp), %rdi
	movq	%rdi, -496(%rbp)
	movq	-496(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-520(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -504(%rbp)
	movq	-504(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-528(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -724(%rbp)
	movq	%rdx, -584(%rbp)
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movl	-1564(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1564(%rbp)
	movq	-1560(%rbp), %rdi
	movq	-1536(%rbp), %rsi
	movq	-1528(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB43_3
	jmp	LBB43_6
LBB43_3:
	leaq	-348(%rbp), %rax
	leaq	-344(%rbp), %rcx
	leaq	-340(%rbp), %rdx
	leaq	-156(%rbp), %rsi
	leaq	-60(%rbp), %rdi
	leaq	-276(%rbp), %r8
	movq	-1528(%rbp), %r9
	movq	-1536(%rbp), %r10
	movq	%r9, -376(%rbp)
	movq	%r10, -384(%rbp)
	movq	-376(%rbp), %r9
	movq	-384(%rbp), %r10
	movq	%r9, -360(%rbp)
	movq	%r10, -368(%rbp)
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r10
	movq	%r9, -328(%rbp)
	movq	%r10, -336(%rbp)
	movq	-328(%rbp), %r9
	movq	%r9, %r10
	movq	-336(%rbp), %r11
	movq	%r10, -312(%rbp)
	movq	%r11, -320(%rbp)
	movq	-312(%rbp), %r10
	movq	-320(%rbp), %r11
	movq	%r10, -296(%rbp)
	movq	%r11, -304(%rbp)
	movq	-296(%rbp), %r10
	movq	%r10, -288(%rbp)
	movq	-288(%rbp), %r10
	movq	-304(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r10, -264(%rbp)
	movq	%r11, -272(%rbp)
	movq	-264(%rbp), %r10
	movq	%r10, -256(%rbp)
	movq	-256(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -276(%rbp)
	movq	-272(%rbp), %r10
	movq	%r10, -240(%rbp)
	movq	-240(%rbp), %r10
	movl	(%r10), %ebx
	movq	-264(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movl	(%r8), %ebx
	movq	-272(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -340(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-336(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	movq	%r8, -80(%rbp)
	movq	%r10, -88(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-16(%rbp), %r10
	movq	%r8, -48(%rbp)
	movq	%r10, -56(%rbp)
	movq	-48(%rbp), %r8
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -60(%rbp)
	movq	-56(%rbp), %r8
	movq	%r8, -24(%rbp)
	movq	-24(%rbp), %r8
	movl	(%r8), %ebx
	movq	-48(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-56(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -344(%rbp)
	addq	$8, %r9
	movq	-336(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	-200(%rbp), %r8
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	-184(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%rdi, -144(%rbp)
	movq	%r8, -152(%rbp)
	movq	-144(%rbp), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -156(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-144(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-152(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -348(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movl	-1564(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1564(%rbp)
	movq	-1560(%rbp), %rdi
	movq	-1528(%rbp), %rsi
	movq	-1520(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB43_4
	jmp	LBB43_5
LBB43_4:
	leaq	-1100(%rbp), %rax
	leaq	-1096(%rbp), %rcx
	leaq	-1092(%rbp), %rdx
	leaq	-908(%rbp), %rsi
	leaq	-812(%rbp), %rdi
	leaq	-1028(%rbp), %r8
	movq	-1520(%rbp), %r9
	movq	-1528(%rbp), %r10
	movq	%r9, -1128(%rbp)
	movq	%r10, -1136(%rbp)
	movq	-1128(%rbp), %r9
	movq	-1136(%rbp), %r10
	movq	%r9, -1112(%rbp)
	movq	%r10, -1120(%rbp)
	movq	-1112(%rbp), %r9
	movq	-1120(%rbp), %r10
	movq	%r9, -1080(%rbp)
	movq	%r10, -1088(%rbp)
	movq	-1080(%rbp), %r9
	movq	%r9, %r10
	movq	-1088(%rbp), %r11
	movq	%r10, -1064(%rbp)
	movq	%r11, -1072(%rbp)
	movq	-1064(%rbp), %r10
	movq	-1072(%rbp), %r11
	movq	%r10, -1048(%rbp)
	movq	%r11, -1056(%rbp)
	movq	-1048(%rbp), %r10
	movq	%r10, -1040(%rbp)
	movq	-1040(%rbp), %r10
	movq	-1056(%rbp), %r11
	movq	%r11, -984(%rbp)
	movq	-984(%rbp), %r11
	movq	%r10, -1016(%rbp)
	movq	%r11, -1024(%rbp)
	movq	-1016(%rbp), %r10
	movq	%r10, -1008(%rbp)
	movq	-1008(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -1028(%rbp)
	movq	-1024(%rbp), %r10
	movq	%r10, -992(%rbp)
	movq	-992(%rbp), %r10
	movl	(%r10), %ebx
	movq	-1016(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -1000(%rbp)
	movq	-1000(%rbp), %r8
	movl	(%r8), %ebx
	movq	-1024(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -1092(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-1088(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -848(%rbp)
	movq	%r10, -856(%rbp)
	movq	-848(%rbp), %r8
	movq	-856(%rbp), %r10
	movq	%r8, -832(%rbp)
	movq	%r10, -840(%rbp)
	movq	-832(%rbp), %r8
	movq	%r8, -824(%rbp)
	movq	-824(%rbp), %r8
	movq	-840(%rbp), %r10
	movq	%r10, -768(%rbp)
	movq	-768(%rbp), %r10
	movq	%r8, -800(%rbp)
	movq	%r10, -808(%rbp)
	movq	-800(%rbp), %r8
	movq	%r8, -792(%rbp)
	movq	-792(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -812(%rbp)
	movq	-808(%rbp), %r8
	movq	%r8, -776(%rbp)
	movq	-776(%rbp), %r8
	movl	(%r8), %ebx
	movq	-800(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -784(%rbp)
	movq	-784(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-808(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -1096(%rbp)
	addq	$8, %r9
	movq	-1088(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -944(%rbp)
	movq	%rdi, -952(%rbp)
	movq	-944(%rbp), %rdi
	movq	-952(%rbp), %r8
	movq	%rdi, -928(%rbp)
	movq	%r8, -936(%rbp)
	movq	-928(%rbp), %rdi
	movq	%rdi, -920(%rbp)
	movq	-920(%rbp), %rdi
	movq	-936(%rbp), %r8
	movq	%r8, -864(%rbp)
	movq	-864(%rbp), %r8
	movq	%rdi, -896(%rbp)
	movq	%r8, -904(%rbp)
	movq	-896(%rbp), %rdi
	movq	%rdi, -888(%rbp)
	movq	-888(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -908(%rbp)
	movq	-904(%rbp), %rdi
	movq	%rdi, -872(%rbp)
	movq	-872(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-896(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -880(%rbp)
	movq	-880(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-904(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -1100(%rbp)
	movq	%rdx, -960(%rbp)
	movq	%rcx, -968(%rbp)
	movq	%rax, -976(%rbp)
	movl	-1564(%rbp), %ebx
	addl	$1, %ebx
	movl	%ebx, -1564(%rbp)
LBB43_5:
	jmp	LBB43_6
LBB43_6:
	jmp	LBB43_7
LBB43_7:
	jmp	LBB43_8
LBB43_8:
	movl	-1564(%rbp), %eax
	addq	$1560, %rsp             ## imm = 0x618
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__118__insertion_sort_3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__118__insertion_sort_3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.align	4, 0x90
__ZNSt3__118__insertion_sort_3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_: ## @_ZNSt3__118__insertion_sort_3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEvT0_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp178:
	.cfi_def_cfa_offset 16
Ltmp179:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp180:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-32(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -56(%rbp)
	movq	-32(%rbp), %rdi
	movq	-32(%rbp), %rdx
	addq	$12, %rdx
	movq	-56(%rbp), %rsi
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)         ## 8-byte Spill
	movq	%rdx, %rsi
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movq	-56(%rbp), %rcx
	addq	$12, %rcx
	movq	%rcx, -64(%rbp)
	movl	%eax, -100(%rbp)        ## 4-byte Spill
LBB44_1:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB44_4 Depth 2
	movq	-64(%rbp), %rax
	cmpq	-40(%rbp), %rax
	je	LBB44_11
## BB#2:                                ##   in Loop: Header=BB44_1 Depth=1
	movq	-48(%rbp), %rdi
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB44_3
	jmp	LBB44_9
LBB44_3:                                ##   in Loop: Header=BB44_1 Depth=1
	movq	-64(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -80(%rbp)
	movl	8(%rax), %edx
	movl	%edx, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
LBB44_4:                                ##   Parent Loop BB44_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	-56(%rbp), %rdi
	movq	-88(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__15tupleIJiiiEEaSEOS1_
	movq	-88(%rbp), %rsi
	movq	%rsi, -56(%rbp)
	movq	%rax, -112(%rbp)        ## 8-byte Spill
## BB#5:                                ##   in Loop: Header=BB44_4 Depth=2
	xorl	%eax, %eax
	movb	%al, %cl
	movq	-56(%rbp), %rdx
	cmpq	-32(%rbp), %rdx
	movb	%cl, -113(%rbp)         ## 1-byte Spill
	je	LBB44_7
## BB#6:                                ##   in Loop: Header=BB44_4 Depth=2
	leaq	-80(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	-88(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -88(%rbp)
	movq	%rax, %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	movb	%al, -113(%rbp)         ## 1-byte Spill
LBB44_7:                                ##   in Loop: Header=BB44_4 Depth=2
	movb	-113(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB44_4
## BB#8:                                ##   in Loop: Header=BB44_1 Depth=1
	leaq	-80(%rbp), %rax
	movq	-56(%rbp), %rdi
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rsi
	callq	__ZNSt3__15tupleIJiiiEEaSEOS1_
	movq	%rax, -128(%rbp)        ## 8-byte Spill
LBB44_9:                                ##   in Loop: Header=BB44_1 Depth=1
	movq	-64(%rbp), %rax
	movq	%rax, -56(%rbp)
## BB#10:                               ##   in Loop: Header=BB44_1 Depth=1
	movq	-64(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -64(%rbp)
	jmp	LBB44_1
LBB44_11:
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_
	.weak_def_can_be_hidden	__ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_
	.align	4, 0x90
__ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_: ## @_ZNSt3__127__insertion_sort_incompleteIRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEbT0_SA_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp181:
	.cfi_def_cfa_offset 16
Ltmp182:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp183:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$584, %rsp              ## imm = 0x248
Ltmp184:
	.cfi_offset %rbx, -24
	movq	%rdi, -424(%rbp)
	movq	%rsi, -432(%rbp)
	movq	%rdx, -440(%rbp)
	movq	-432(%rbp), %rdx
	movq	-424(%rbp), %rsi
	subq	%rsi, %rdx
	sarq	$2, %rdx
	movabsq	$-6148914691236517205, %rsi ## imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rsi, %rdx
	movq	%rdx, %rsi
	subq	$5, %rsi
	movq	%rdx, -496(%rbp)        ## 8-byte Spill
	movq	%rsi, -504(%rbp)        ## 8-byte Spill
	ja	LBB45_8
## BB#23:
	leaq	LJTI45_0(%rip), %rax
	movq	-496(%rbp), %rcx        ## 8-byte Reload
	movslq	(%rax,%rcx,4), %rdx
	addq	%rax, %rdx
	jmpq	*%rdx
LBB45_1:
	movb	$1, -409(%rbp)
	jmp	LBB45_22
LBB45_2:
	movq	-440(%rbp), %rdi
	movq	-432(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -432(%rbp)
	movq	-424(%rbp), %rdx
	movq	%rax, %rsi
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB45_3
	jmp	LBB45_4
LBB45_3:
	leaq	-372(%rbp), %rax
	leaq	-368(%rbp), %rcx
	leaq	-364(%rbp), %rdx
	leaq	-180(%rbp), %rsi
	leaq	-84(%rbp), %rdi
	leaq	-300(%rbp), %r8
	movq	-424(%rbp), %r9
	movq	-432(%rbp), %r10
	movq	%r9, -400(%rbp)
	movq	%r10, -408(%rbp)
	movq	-400(%rbp), %r9
	movq	-408(%rbp), %r10
	movq	%r9, -384(%rbp)
	movq	%r10, -392(%rbp)
	movq	-384(%rbp), %r9
	movq	-392(%rbp), %r10
	movq	%r9, -352(%rbp)
	movq	%r10, -360(%rbp)
	movq	-352(%rbp), %r9
	movq	%r9, %r10
	movq	-360(%rbp), %r11
	movq	%r10, -336(%rbp)
	movq	%r11, -344(%rbp)
	movq	-336(%rbp), %r10
	movq	-344(%rbp), %r11
	movq	%r10, -320(%rbp)
	movq	%r11, -328(%rbp)
	movq	-320(%rbp), %r10
	movq	%r10, -312(%rbp)
	movq	-312(%rbp), %r10
	movq	-328(%rbp), %r11
	movq	%r11, -256(%rbp)
	movq	-256(%rbp), %r11
	movq	%r10, -288(%rbp)
	movq	%r11, -296(%rbp)
	movq	-288(%rbp), %r10
	movq	%r10, -280(%rbp)
	movq	-280(%rbp), %r10
	movl	(%r10), %ebx
	movl	%ebx, -300(%rbp)
	movq	-296(%rbp), %r10
	movq	%r10, -264(%rbp)
	movq	-264(%rbp), %r10
	movl	(%r10), %ebx
	movq	-288(%rbp), %r10
	movl	%ebx, (%r10)
	movq	%r8, -272(%rbp)
	movq	-272(%rbp), %r8
	movl	(%r8), %ebx
	movq	-296(%rbp), %r8
	movl	%ebx, (%r8)
	movl	$0, -364(%rbp)
	movq	%r9, %r8
	addq	$4, %r8
	movq	-360(%rbp), %r10
	addq	$4, %r10
	movq	%r8, -120(%rbp)
	movq	%r10, -128(%rbp)
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r10
	movq	%r8, -104(%rbp)
	movq	%r10, -112(%rbp)
	movq	-104(%rbp), %r8
	movq	%r8, -96(%rbp)
	movq	-96(%rbp), %r8
	movq	-112(%rbp), %r10
	movq	%r10, -40(%rbp)
	movq	-40(%rbp), %r10
	movq	%r8, -72(%rbp)
	movq	%r10, -80(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movl	(%r8), %ebx
	movl	%ebx, -84(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -48(%rbp)
	movq	-48(%rbp), %r8
	movl	(%r8), %ebx
	movq	-72(%rbp), %r8
	movl	%ebx, (%r8)
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-80(%rbp), %rdi
	movl	%ebx, (%rdi)
	movl	$0, -368(%rbp)
	addq	$8, %r9
	movq	-360(%rbp), %rdi
	addq	$8, %rdi
	movq	%r9, -216(%rbp)
	movq	%rdi, -224(%rbp)
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %r8
	movq	%rdi, -200(%rbp)
	movq	%r8, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	%rdi, -192(%rbp)
	movq	-192(%rbp), %rdi
	movq	-208(%rbp), %r8
	movq	%r8, -136(%rbp)
	movq	-136(%rbp), %r8
	movq	%rdi, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movl	(%rdi), %ebx
	movl	%ebx, -180(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movl	(%rdi), %ebx
	movq	-168(%rbp), %rdi
	movl	%ebx, (%rdi)
	movq	%rsi, -152(%rbp)
	movq	-152(%rbp), %rsi
	movl	(%rsi), %ebx
	movq	-176(%rbp), %rsi
	movl	%ebx, (%rsi)
	movl	$0, -372(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%rax, -248(%rbp)
LBB45_4:
	movb	$1, -409(%rbp)
	jmp	LBB45_22
LBB45_5:
	movq	-424(%rbp), %rdi
	movq	-424(%rbp), %rax
	addq	$12, %rax
	movq	-432(%rbp), %rcx
	addq	$-12, %rcx
	movq	%rcx, -432(%rbp)
	movq	-440(%rbp), %rdx
	movq	%rax, %rsi
	movq	%rdx, -512(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-512(%rbp), %rcx        ## 8-byte Reload
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movb	$1, -409(%rbp)
	movl	%eax, -516(%rbp)        ## 4-byte Spill
	jmp	LBB45_22
LBB45_6:
	movq	-424(%rbp), %rdi
	movq	-424(%rbp), %rax
	addq	$12, %rax
	movq	-424(%rbp), %rcx
	addq	$24, %rcx
	movq	-432(%rbp), %rdx
	addq	$-12, %rdx
	movq	%rdx, -432(%rbp)
	movq	-440(%rbp), %r8
	movq	%rax, %rsi
	movq	%rdx, -528(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-528(%rbp), %rcx        ## 8-byte Reload
	callq	__ZNSt3__17__sort4IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_T_
	movb	$1, -409(%rbp)
	movl	%eax, -532(%rbp)        ## 4-byte Spill
	jmp	LBB45_22
LBB45_7:
	movq	-424(%rbp), %rdi
	movq	-424(%rbp), %rax
	addq	$12, %rax
	movq	-424(%rbp), %rcx
	addq	$24, %rcx
	movq	-424(%rbp), %rdx
	addq	$36, %rdx
	movq	-432(%rbp), %rsi
	addq	$-12, %rsi
	movq	%rsi, -432(%rbp)
	movq	-440(%rbp), %r9
	movq	%rsi, -544(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rdx, -552(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-552(%rbp), %rcx        ## 8-byte Reload
	movq	-544(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__17__sort5IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_SA_SA_T_
	movb	$1, -409(%rbp)
	movl	%eax, -556(%rbp)        ## 4-byte Spill
	jmp	LBB45_22
LBB45_8:
	movq	-424(%rbp), %rax
	addq	$24, %rax
	movq	%rax, -448(%rbp)
	movq	-424(%rbp), %rdi
	movq	-424(%rbp), %rax
	addq	$12, %rax
	movq	-448(%rbp), %rdx
	movq	-440(%rbp), %rcx
	movq	%rax, %rsi
	callq	__ZNSt3__17__sort3IRN6detail14order_by_partsINS_4lessIvEEJLm1ELm2EEEEPNS_5tupleIJiiiEEEEEjT0_SA_SA_T_
	movl	$8, -452(%rbp)
	movl	$0, -456(%rbp)
	movq	-448(%rbp), %rcx
	addq	$12, %rcx
	movq	%rcx, -464(%rbp)
	movl	%eax, -560(%rbp)        ## 4-byte Spill
LBB45_9:                                ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB45_12 Depth 2
	movq	-464(%rbp), %rax
	cmpq	-432(%rbp), %rax
	je	LBB45_21
## BB#10:                               ##   in Loop: Header=BB45_9 Depth=1
	movq	-440(%rbp), %rdi
	movq	-464(%rbp), %rsi
	movq	-448(%rbp), %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	testb	$1, %al
	jne	LBB45_11
	jmp	LBB45_19
LBB45_11:                               ##   in Loop: Header=BB45_9 Depth=1
	movq	-464(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	(%rax), %rcx
	movq	%rcx, -480(%rbp)
	movl	8(%rax), %edx
	movl	%edx, -472(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	-464(%rbp), %rax
	movq	%rax, -448(%rbp)
LBB45_12:                               ##   Parent Loop BB45_9 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	-448(%rbp), %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__15tupleIJiiiEEaSEOS1_
	movq	-488(%rbp), %rsi
	movq	%rsi, -448(%rbp)
	movq	%rax, -568(%rbp)        ## 8-byte Spill
## BB#13:                               ##   in Loop: Header=BB45_12 Depth=2
	xorl	%eax, %eax
	movb	%al, %cl
	movq	-448(%rbp), %rdx
	cmpq	-424(%rbp), %rdx
	movb	%cl, -569(%rbp)         ## 1-byte Spill
	je	LBB45_15
## BB#14:                               ##   in Loop: Header=BB45_12 Depth=2
	leaq	-480(%rbp), %rsi
	movq	-440(%rbp), %rdi
	movq	-488(%rbp), %rax
	addq	$-12, %rax
	movq	%rax, -488(%rbp)
	movq	%rax, %rdx
	callq	__ZNK6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEEclIJiiiEEEbRKNS1_5tupleIJDpT_EEESB_
	movb	%al, -569(%rbp)         ## 1-byte Spill
LBB45_15:                               ##   in Loop: Header=BB45_12 Depth=2
	movb	-569(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB45_12
## BB#16:                               ##   in Loop: Header=BB45_9 Depth=1
	leaq	-480(%rbp), %rax
	movq	-448(%rbp), %rdi
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rsi
	callq	__ZNSt3__15tupleIJiiiEEaSEOS1_
	movl	-456(%rbp), %ecx
	addl	$1, %ecx
	movl	%ecx, -456(%rbp)
	cmpl	$8, %ecx
	movq	%rax, -584(%rbp)        ## 8-byte Spill
	jne	LBB45_18
## BB#17:
	movq	-464(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -464(%rbp)
	cmpq	-432(%rbp), %rax
	sete	%cl
	andb	$1, %cl
	movb	%cl, -409(%rbp)
	jmp	LBB45_22
LBB45_18:                               ##   in Loop: Header=BB45_9 Depth=1
	jmp	LBB45_19
LBB45_19:                               ##   in Loop: Header=BB45_9 Depth=1
	movq	-464(%rbp), %rax
	movq	%rax, -448(%rbp)
## BB#20:                               ##   in Loop: Header=BB45_9 Depth=1
	movq	-464(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -464(%rbp)
	jmp	LBB45_9
LBB45_21:
	movb	$1, -409(%rbp)
LBB45_22:
	movb	-409(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	addq	$584, %rsp              ## imm = 0x248
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc
	.align	2, 0x90
L45_0_set_1 = LBB45_1-LJTI45_0
L45_0_set_2 = LBB45_2-LJTI45_0
L45_0_set_5 = LBB45_5-LJTI45_0
L45_0_set_6 = LBB45_6-LJTI45_0
L45_0_set_7 = LBB45_7-LJTI45_0
LJTI45_0:
	.long	L45_0_set_1
	.long	L45_0_set_1
	.long	L45_0_set_2
	.long	L45_0_set_5
	.long	L45_0_set_6
	.long	L45_0_set_7

	.globl	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE
	.weak_def_can_be_hidden	__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE
	.align	4, 0x90
__ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE: ## @_ZN6detail14order_by_partsINSt3__14lessIvEEJLm1ELm2EEE10sort_orderIJiiiEEEDaRKNS1_5tupleIJDpT_EEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp185:
	.cfi_def_cfa_offset 16
Ltmp186:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp187:
	.cfi_def_cfa_register %rbp
	subq	$216, %rsp
	movq	%rdi, -344(%rbp)
	movq	%rdi, -320(%rbp)
	addq	$4, %rdi
	movq	%rdi, -312(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -16(%rbp)
	addq	$8, %rax
	movq	%rax, -8(%rbp)
	movq	%rdi, -296(%rbp)
	movq	%rax, -304(%rbp)
	movq	-296(%rbp), %rdi
	leaq	-288(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	%rdi, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-256(%rbp), %rcx
	movq	-264(%rbp), %rdi
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movq	%rax, -216(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-176(%rbp), %rcx
	movq	-184(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movq	%rax, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rcx, -24(%rbp)
	movq	%rcx, (%rdi)
	addq	$8, %rax
	movq	-136(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rax
	movq	%rcx, -56(%rbp)
	movq	%rcx, (%rax)
	movq	-288(%rbp), %rax
	movq	-280(%rbp), %rcx
	movq	%rax, -336(%rbp)
	movq	%rcx, -328(%rbp)
	movq	-336(%rbp), %rax
	movq	%rcx, %rdx
	addq	$216, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__15tupleIJiiiEEaSEOS1_
	.weak_def_can_be_hidden	__ZNSt3__15tupleIJiiiEEaSEOS1_
	.align	4, 0x90
__ZNSt3__15tupleIJiiiEEaSEOS1_:         ## @_ZNSt3__15tupleIJiiiEEaSEOS1_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp188:
	.cfi_def_cfa_offset 16
Ltmp189:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp190:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %rax
	movq	-160(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movl	(%rcx), %edx
	movl	%edx, (%rax)
	movq	%rdi, %rcx
	addq	$4, %rcx
	movq	-160(%rbp), %r8
	addq	$4, %r8
	movq	%r8, -40(%rbp)
	movq	-40(%rbp), %r8
	movq	%r8, -48(%rbp)
	movq	-48(%rbp), %r8
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	movq	%r8, -56(%rbp)
	movq	-56(%rbp), %r8
	movl	(%r8), %edx
	movl	%edx, (%rcx)
	addq	$8, %rdi
	movq	-160(%rbp), %r8
	addq	$8, %r8
	movq	%r8, -80(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -88(%rbp)
	movq	-88(%rbp), %r8
	movq	%rdi, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	-104(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	%r8, -96(%rbp)
	movq	-96(%rbp), %r8
	movl	(%r8), %edx
	movl	%edx, (%rdi)
	movq	%rax, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdi, -136(%rbp)
	movq	%rsi, %rax
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp212:
	.cfi_def_cfa_offset 16
Ltmp213:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp214:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp191:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp192:
	jmp	LBB48_1
LBB48_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB48_3
	jmp	LBB48_26
LBB48_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB48_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB48_7
LBB48_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB48_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB48_8
	jmp	LBB48_13
LBB48_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp194:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp195:
	jmp	LBB48_9
LBB48_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp196:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp197:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB48_10
LBB48_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp198:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp199:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB48_12
LBB48_11:
Ltmp200:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB48_21
LBB48_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB48_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp201:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp202:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB48_15
LBB48_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB48_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp203:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp204:
	jmp	LBB48_17
LBB48_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB48_18
LBB48_18:
	jmp	LBB48_25
LBB48_19:
Ltmp193:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB48_22
LBB48_20:
Ltmp205:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB48_21
LBB48_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB48_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp206:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp207:
	jmp	LBB48_23
LBB48_23:
	callq	___cxa_end_catch
LBB48_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB48_25:
	jmp	LBB48_26
LBB48_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB48_24
LBB48_27:
Ltmp208:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp209:
	callq	___cxa_end_catch
Ltmp210:
	jmp	LBB48_28
LBB48_28:
	jmp	LBB48_29
LBB48_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB48_30:
Ltmp211:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table48:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset28 = Ltmp191-Lfunc_begin4           ## >> Call Site 1 <<
	.long	Lset28
Lset29 = Ltmp192-Ltmp191                ##   Call between Ltmp191 and Ltmp192
	.long	Lset29
Lset30 = Ltmp193-Lfunc_begin4           ##     jumps to Ltmp193
	.long	Lset30
	.byte	5                       ##   On action: 3
Lset31 = Ltmp194-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset31
Lset32 = Ltmp195-Ltmp194                ##   Call between Ltmp194 and Ltmp195
	.long	Lset32
Lset33 = Ltmp205-Lfunc_begin4           ##     jumps to Ltmp205
	.long	Lset33
	.byte	5                       ##   On action: 3
Lset34 = Ltmp196-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset34
Lset35 = Ltmp199-Ltmp196                ##   Call between Ltmp196 and Ltmp199
	.long	Lset35
Lset36 = Ltmp200-Lfunc_begin4           ##     jumps to Ltmp200
	.long	Lset36
	.byte	3                       ##   On action: 2
Lset37 = Ltmp201-Lfunc_begin4           ## >> Call Site 4 <<
	.long	Lset37
Lset38 = Ltmp204-Ltmp201                ##   Call between Ltmp201 and Ltmp204
	.long	Lset38
Lset39 = Ltmp205-Lfunc_begin4           ##     jumps to Ltmp205
	.long	Lset39
	.byte	5                       ##   On action: 3
Lset40 = Ltmp204-Lfunc_begin4           ## >> Call Site 5 <<
	.long	Lset40
Lset41 = Ltmp206-Ltmp204                ##   Call between Ltmp204 and Ltmp206
	.long	Lset41
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp206-Lfunc_begin4           ## >> Call Site 6 <<
	.long	Lset42
Lset43 = Ltmp207-Ltmp206                ##   Call between Ltmp206 and Ltmp207
	.long	Lset43
Lset44 = Ltmp208-Lfunc_begin4           ##     jumps to Ltmp208
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp207-Lfunc_begin4           ## >> Call Site 7 <<
	.long	Lset45
Lset46 = Ltmp209-Ltmp207                ##   Call between Ltmp207 and Ltmp209
	.long	Lset46
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset47 = Ltmp209-Lfunc_begin4           ## >> Call Site 8 <<
	.long	Lset47
Lset48 = Ltmp210-Ltmp209                ##   Call between Ltmp209 and Ltmp210
	.long	Lset48
Lset49 = Ltmp211-Lfunc_begin4           ##     jumps to Ltmp211
	.long	Lset49
	.byte	5                       ##   On action: 3
Lset50 = Ltmp210-Lfunc_begin4           ## >> Call Site 9 <<
	.long	Lset50
Lset51 = Lfunc_end4-Ltmp210             ##   Call between Ltmp210 and Lfunc_end4
	.long	Lset51
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp215:
	.cfi_def_cfa_offset 16
Ltmp216:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp217:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp221:
	.cfi_def_cfa_offset 16
Ltmp222:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp223:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB50_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB50_26
LBB50_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB50_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB50_5
LBB50_4:
	movq	$0, -368(%rbp)
LBB50_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB50_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB50_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB50_26
LBB50_8:
	jmp	LBB50_9
LBB50_9:
	cmpq	$0, -368(%rbp)
	jle	LBB50_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB50_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB50_13
LBB50_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB50_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp218:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp219:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB50_14
LBB50_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB50_15
LBB50_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB50_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB50_19
LBB50_17:
Ltmp220:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB50_27
LBB50_18:
	movl	$0, -416(%rbp)
LBB50_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB50_20
	jmp	LBB50_29
LBB50_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB50_26
	jmp	LBB50_28
LBB50_20:
	jmp	LBB50_21
LBB50_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB50_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB50_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB50_26
LBB50_24:
	jmp	LBB50_25
LBB50_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB50_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB50_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB50_28:
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table50:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset52 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset52
Lset53 = Ltmp218-Lfunc_begin5           ##   Call between Lfunc_begin5 and Ltmp218
	.long	Lset53
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset54 = Ltmp218-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset54
Lset55 = Ltmp219-Ltmp218                ##   Call between Ltmp218 and Ltmp219
	.long	Lset55
Lset56 = Ltmp220-Lfunc_begin5           ##     jumps to Ltmp220
	.long	Lset56
	.byte	0                       ##   On action: cleanup
Lset57 = Ltmp219-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset57
Lset58 = Lfunc_end5-Ltmp219             ##   Call between Ltmp219 and Lfunc_end5
	.long	Lset58
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp224:
	.cfi_def_cfa_offset 16
Ltmp225:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp226:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp227:
	.cfi_def_cfa_offset 16
Ltmp228:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp229:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__const
	.align	2                       ## @.ref.tmp
l_.ref.tmp:
	.long	1                       ## 0x1
	.long	1                       ## 0x1
	.long	1                       ## 0x1
	.long	6                       ## 0x6
	.long	6                       ## 0x6
	.long	5                       ## 0x5
	.long	4                       ## 0x4
	.long	4                       ## 0x4
	.long	5                       ## 0x5
	.long	5                       ## 0x5
	.long	5                       ## 0x5

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	" "

L_.str.1:                               ## @.str.1
	.asciz	" \n"


.subsections_via_symbols
