	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__ZlsRNSt3__113basic_ostreamIcNS_11char_traitsIcEEEERK19exception_swallower
	.align	4, 0x90
__ZlsRNSt3__113basic_ostreamIcNS_11char_traitsIcEEEERK19exception_swallower: ## @_ZlsRNSt3__113basic_ostreamIcNS_11char_traitsIcEEEERK19exception_swallower
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	testb	$1, (%rsi)
	je	LBB0_2
## BB#1:
	leaq	L_.str(%rip), %rsi
	movq	-16(%rbp), %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	%rax, -8(%rbp)
	jmp	LBB0_3
LBB0_2:
	leaq	L_.str.1(%rip), %rsi
	movq	-16(%rbp), %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-24(%rbp), %rsi
	addq	$8, %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	movq	%rax, -8(%rbp)
LBB0_3:
	movq	-8(%rbp), %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB2_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB2_3
LBB2_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB2_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB2_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB2_6
LBB2_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB2_6:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	subq	$1664, %rsp             ## imm = 0x680
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	movl	%edi, -1448(%rbp)
	movq	%rsi, -1456(%rbp)
	movq	(%rsi), %rax
	leaq	-584(%rbp), %rsi
	movq	%rsi, -1416(%rbp)
	movq	%rax, -1424(%rbp)
	movl	$8, -1428(%rbp)
	movq	-1416(%rbp), %rax
	movq	%rax, %rsi
	addq	$424, %rsi              ## imm = 0x1A8
	movq	%rsi, -1408(%rbp)
	movq	%rsi, -1400(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rsi
	addq	$16, %rsi
	movq	%rsi, 424(%rax)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	addq	$16, %rsi
	movq	%rsi, 424(%rax)
	movq	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rsi, %rcx
	addq	$24, %rcx
	movq	%rcx, (%rax)
	addq	$64, %rsi
	movq	%rsi, 424(%rax)
	movq	%rax, %rcx
	addq	$16, %rcx
	movq	%rax, -1376(%rbp)
	movq	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%rsi, -1384(%rbp)
	movq	%rcx, -1392(%rbp)
	movq	-1376(%rbp), %rcx
	movq	-1384(%rbp), %rsi
	movq	(%rsi), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rsi), %rsi
	movq	-24(%rdx), %rdx
	movq	%rsi, (%rcx,%rdx)
	movq	$0, 8(%rcx)
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	-1392(%rbp), %rdx
	movq	%rcx, -1360(%rbp)
	movq	%rdx, -1368(%rbp)
	movq	-1360(%rbp), %rcx
Ltmp9:
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	%rax, -1560(%rbp)       ## 8-byte Spill
	movq	%rcx, -1568(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp10:
	jmp	LBB3_1
LBB3_1:                                 ## %_ZNSt3__113basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-1568(%rbp), %rax       ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-1568(%rbp), %rcx       ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-1560(%rbp), %rdi       ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 424(%rdi)
	addq	$16, %rdi
Ltmp12:
	movq	%rdi, -1576(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
Ltmp13:
	jmp	LBB3_2
LBB3_2:
	movq	-1560(%rbp), %rax       ## 8-byte Reload
	addq	$16, %rax
	movq	-1424(%rbp), %rsi
	movl	-1428(%rbp), %ecx
	orl	$8, %ecx
Ltmp15:
	movq	%rax, %rdi
	movl	%ecx, %edx
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
Ltmp16:
	movq	%rax, -1584(%rbp)       ## 8-byte Spill
	jmp	LBB3_3
LBB3_3:
	movq	-1584(%rbp), %rax       ## 8-byte Reload
	cmpq	$0, %rax
	jne	LBB3_11
## BB#4:
	movq	-1560(%rbp), %rax       ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -1344(%rbp)
	movl	$4, -1348(%rbp)
	movq	-1344(%rbp), %rax
	movq	%rax, -1328(%rbp)
	movl	$4, -1332(%rbp)
	movq	-1328(%rbp), %rax
	movl	32(%rax), %edx
	orl	$4, %edx
Ltmp17:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp18:
	jmp	LBB3_5
LBB3_5:                                 ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit.i
	jmp	LBB3_11
LBB3_6:
Ltmp11:
	movl	%edx, %ecx
	movq	%rax, -1440(%rbp)
	movl	%ecx, -1444(%rbp)
	jmp	LBB3_10
LBB3_7:
Ltmp14:
	movl	%edx, %ecx
	movq	%rax, -1440(%rbp)
	movl	%ecx, -1444(%rbp)
	jmp	LBB3_9
LBB3_8:
Ltmp19:
	movl	%edx, %ecx
	movq	%rax, -1440(%rbp)
	movl	%ecx, -1444(%rbp)
	movq	-1576(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
LBB3_9:
	movq	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rax
	addq	$8, %rax
	movq	-1560(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
LBB3_10:
	movq	-1560(%rbp), %rax       ## 8-byte Reload
	addq	$424, %rax              ## imm = 0x1A8
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-1440(%rbp), %rax
	movq	%rax, -1592(%rbp)       ## 8-byte Spill
	jmp	LBB3_39
LBB3_11:                                ## %_ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEC1EPKcj.exit
Ltmp20:
	leaq	-1488(%rbp), %rdi
	leaq	-584(%rbp), %rsi
	callq	__Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_0E19exception_swallowerT_T0_
Ltmp21:
	jmp	LBB3_12
LBB3_12:
Ltmp23:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	-1488(%rbp), %rsi
	callq	__ZlsRNSt3__113basic_ostreamIcNS_11char_traitsIcEEEERK19exception_swallower
Ltmp24:
	movq	%rax, -1600(%rbp)       ## 8-byte Spill
	jmp	LBB3_13
LBB3_13:
	movq	-1600(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1312(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -1320(%rbp)
	movq	-1312(%rbp), %rdi
Ltmp25:
	callq	*%rcx
Ltmp26:
	movq	%rax, -1608(%rbp)       ## 8-byte Spill
	jmp	LBB3_14
LBB3_14:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB3_15
LBB3_15:
	leaq	-1488(%rbp), %rdi
	callq	__ZN19exception_swallowerD1Ev
	leaq	-584(%rbp), %rdi
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	movq	-1456(%rbp), %rdi
	movq	(%rdi), %rdi
	leaq	-1160(%rbp), %rax
	movq	%rax, -1272(%rbp)
	movq	%rdi, -1280(%rbp)
	movl	$8, -1284(%rbp)
	movq	-1272(%rbp), %rax
	movq	%rax, %rdi
	addq	$424, %rdi              ## imm = 0x1A8
	movq	%rdi, -1264(%rbp)
	movq	%rdi, -1256(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, 424(%rax)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, 424(%rax)
	movq	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdi
	movq	%rdi, %rcx
	addq	$24, %rcx
	movq	%rcx, (%rax)
	addq	$64, %rdi
	movq	%rdi, 424(%rax)
	movq	%rax, %rcx
	addq	$16, %rcx
	movq	%rax, -1232(%rbp)
	movq	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdi
	addq	$8, %rdi
	movq	%rdi, -1240(%rbp)
	movq	%rcx, -1248(%rbp)
	movq	-1232(%rbp), %rcx
	movq	-1240(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	8(%rdi), %rdi
	movq	-24(%rdx), %rdx
	movq	%rdi, (%rcx,%rdx)
	movq	$0, 8(%rcx)
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	-1248(%rbp), %rdx
	movq	%rcx, -1216(%rbp)
	movq	%rdx, -1224(%rbp)
	movq	-1216(%rbp), %rcx
Ltmp28:
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	%rax, -1616(%rbp)       ## 8-byte Spill
	movq	%rcx, -1624(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp29:
	jmp	LBB3_16
LBB3_16:                                ## %_ZNSt3__113basic_istreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i.1
	movq	-1624(%rbp), %rax       ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-1624(%rbp), %rcx       ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-1616(%rbp), %rdi       ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 424(%rdi)
	addq	$16, %rdi
Ltmp31:
	movq	%rdi, -1632(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
Ltmp32:
	jmp	LBB3_17
LBB3_17:
	movq	-1616(%rbp), %rax       ## 8-byte Reload
	addq	$16, %rax
	movq	-1280(%rbp), %rsi
	movl	-1284(%rbp), %ecx
	orl	$8, %ecx
Ltmp34:
	movq	%rax, %rdi
	movl	%ecx, %edx
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
Ltmp35:
	movq	%rax, -1640(%rbp)       ## 8-byte Spill
	jmp	LBB3_18
LBB3_18:
	movq	-1640(%rbp), %rax       ## 8-byte Reload
	cmpq	$0, %rax
	jne	LBB3_26
## BB#19:
	movq	-1616(%rbp), %rax       ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -1200(%rbp)
	movl	$4, -1204(%rbp)
	movq	-1200(%rbp), %rax
	movq	%rax, -1184(%rbp)
	movl	$4, -1188(%rbp)
	movq	-1184(%rbp), %rax
	movl	32(%rax), %edx
	orl	$4, %edx
Ltmp36:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp37:
	jmp	LBB3_20
LBB3_20:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit.i.2
	jmp	LBB3_26
LBB3_21:
Ltmp30:
	movl	%edx, %ecx
	movq	%rax, -1296(%rbp)
	movl	%ecx, -1300(%rbp)
	jmp	LBB3_25
LBB3_22:
Ltmp33:
	movl	%edx, %ecx
	movq	%rax, -1296(%rbp)
	movl	%ecx, -1300(%rbp)
	jmp	LBB3_24
LBB3_23:
Ltmp38:
	movl	%edx, %ecx
	movq	%rax, -1296(%rbp)
	movl	%ecx, -1300(%rbp)
	movq	-1632(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
LBB3_24:
	movq	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rax
	addq	$8, %rax
	movq	-1616(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
LBB3_25:
	movq	-1616(%rbp), %rax       ## 8-byte Reload
	addq	$424, %rax              ## imm = 0x1A8
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-1296(%rbp), %rax
	movq	%rax, -1592(%rbp)       ## 8-byte Spill
	jmp	LBB3_39
LBB3_26:                                ## %_ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEC1EPKcj.exit3
Ltmp39:
	leaq	-1544(%rbp), %rdi
	leaq	-1160(%rbp), %rsi
	callq	__Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_1E19exception_swallowerT_T0_
Ltmp40:
	jmp	LBB3_27
LBB3_27:
Ltmp42:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	-1544(%rbp), %rsi
	callq	__ZlsRNSt3__113basic_ostreamIcNS_11char_traitsIcEEEERK19exception_swallower
Ltmp43:
	movq	%rax, -1648(%rbp)       ## 8-byte Spill
	jmp	LBB3_28
LBB3_28:
	movq	-1648(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1168(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -1176(%rbp)
	movq	-1168(%rbp), %rdi
Ltmp44:
	callq	*%rcx
Ltmp45:
	movq	%rax, -1656(%rbp)       ## 8-byte Spill
	jmp	LBB3_29
LBB3_29:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit4
	jmp	LBB3_30
LBB3_30:
	leaq	-1544(%rbp), %rdi
	callq	__ZN19exception_swallowerD1Ev
	leaq	-1160(%rbp), %rdi
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	movq	___stack_chk_guard@GOTPCREL(%rip), %rdi
	movq	(%rdi), %rdi
	cmpq	-8(%rbp), %rdi
	jne	LBB3_40
## BB#31:                               ## %SP_return
	xorl	%eax, %eax
	addq	$1664, %rsp             ## imm = 0x680
	popq	%rbp
	retq
LBB3_32:
Ltmp22:
	movl	%edx, %ecx
	movq	%rax, -1504(%rbp)
	movl	%ecx, -1508(%rbp)
	jmp	LBB3_34
LBB3_33:
Ltmp27:
	leaq	-1488(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -1504(%rbp)
	movl	%ecx, -1508(%rbp)
	callq	__ZN19exception_swallowerD1Ev
LBB3_34:
	leaq	-584(%rbp), %rdi
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	jmp	LBB3_38
LBB3_35:
Ltmp41:
	movl	%edx, %ecx
	movq	%rax, -1504(%rbp)
	movl	%ecx, -1508(%rbp)
	jmp	LBB3_37
LBB3_36:
Ltmp46:
	leaq	-1544(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -1504(%rbp)
	movl	%ecx, -1508(%rbp)
	callq	__ZN19exception_swallowerD1Ev
LBB3_37:
	leaq	-1160(%rbp), %rdi
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
LBB3_38:
	movq	-1504(%rbp), %rax
	movq	%rax, -1592(%rbp)       ## 8-byte Spill
LBB3_39:                                ## %unwind_resume
	movq	-1592(%rbp), %rax       ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
LBB3_40:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\222\201\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\217\001"              ## Call site table length
Lset0 = Ltmp9-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp10-Ltmp9                    ##   Call between Ltmp9 and Ltmp10
	.long	Lset1
Lset2 = Ltmp11-Lfunc_begin0             ##     jumps to Ltmp11
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp12-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp13-Ltmp12                   ##   Call between Ltmp12 and Ltmp13
	.long	Lset4
Lset5 = Ltmp14-Lfunc_begin0             ##     jumps to Ltmp14
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp15-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp18-Ltmp15                   ##   Call between Ltmp15 and Ltmp18
	.long	Lset7
Lset8 = Ltmp19-Lfunc_begin0             ##     jumps to Ltmp19
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp20-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp21-Ltmp20                  ##   Call between Ltmp20 and Ltmp21
	.long	Lset10
Lset11 = Ltmp22-Lfunc_begin0            ##     jumps to Ltmp22
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp23-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset12
Lset13 = Ltmp26-Ltmp23                  ##   Call between Ltmp23 and Ltmp26
	.long	Lset13
Lset14 = Ltmp27-Lfunc_begin0            ##     jumps to Ltmp27
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp28-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset15
Lset16 = Ltmp29-Ltmp28                  ##   Call between Ltmp28 and Ltmp29
	.long	Lset16
Lset17 = Ltmp30-Lfunc_begin0            ##     jumps to Ltmp30
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp31-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset18
Lset19 = Ltmp32-Ltmp31                  ##   Call between Ltmp31 and Ltmp32
	.long	Lset19
Lset20 = Ltmp33-Lfunc_begin0            ##     jumps to Ltmp33
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp34-Lfunc_begin0            ## >> Call Site 8 <<
	.long	Lset21
Lset22 = Ltmp37-Ltmp34                  ##   Call between Ltmp34 and Ltmp37
	.long	Lset22
Lset23 = Ltmp38-Lfunc_begin0            ##     jumps to Ltmp38
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp39-Lfunc_begin0            ## >> Call Site 9 <<
	.long	Lset24
Lset25 = Ltmp40-Ltmp39                  ##   Call between Ltmp39 and Ltmp40
	.long	Lset25
Lset26 = Ltmp41-Lfunc_begin0            ##     jumps to Ltmp41
	.long	Lset26
	.byte	0                       ##   On action: cleanup
Lset27 = Ltmp42-Lfunc_begin0            ## >> Call Site 10 <<
	.long	Lset27
Lset28 = Ltmp45-Ltmp42                  ##   Call between Ltmp42 and Ltmp45
	.long	Lset28
Lset29 = Ltmp46-Lfunc_begin0            ##     jumps to Ltmp46
	.long	Lset29
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp45-Lfunc_begin0            ## >> Call Site 11 <<
	.long	Lset30
Lset31 = Lfunc_end0-Ltmp45              ##   Call between Ltmp45 and Lfunc_end0
	.long	Lset31
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_0E19exception_swallowerT_T0_: ## @"_Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_0E19exception_swallowerT_T0_"
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	subq	$240, %rsp
	movq	%rdi, %rax
	movb	$0, -97(%rbp)
	movq	%rdi, -160(%rbp)        ## 8-byte Spill
	movq	%rax, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, -176(%rbp)        ## 8-byte Spill
	callq	__ZN19exception_swallowerC1Ev
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movl	32(%rax), %ecx
	andl	$5, %ecx
	cmpl	$0, %ecx
	setne	%dl
	movb	%dl, -177(%rbp)         ## 1-byte Spill
## BB#1:
	movb	-177(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB4_2
	jmp	LBB4_14
LBB4_2:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
Ltmp52:
	leaq	L_.str.15(%rip), %rsi
	movq	%rdi, -192(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt13runtime_errorC1EPKc
Ltmp53:
	jmp	LBB4_3
LBB4_3:
Ltmp55:
	movq	__ZTISt13runtime_error@GOTPCREL(%rip), %rsi
	movq	__ZNSt13runtime_errorD1Ev@GOTPCREL(%rip), %rdx
	movq	-192(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_throw
Ltmp56:
	jmp	LBB4_26
LBB4_4:
Ltmp57:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB4_6
LBB4_5:
Ltmp54:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	movq	-192(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
LBB4_6:
	movl	-116(%rbp), %eax
	movl	$1, %ecx
	cmpl	%ecx, %eax
	jne	LBB4_23
## BB#7:
	movq	-112(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -128(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %rdi
	movq	%rdi, -200(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-200(%rbp), %rax        ## 8-byte Reload
	callq	*%rax
	leaq	-152(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdi, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-48(%rbp), %rdi
Ltmp58:
	movq	%rdi, -208(%rbp)        ## 8-byte Spill
	movq	%rax, -216(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp59:
	movq	%rax, -224(%rbp)        ## 8-byte Spill
	jmp	LBB4_8
LBB4_8:                                 ## %.noexc
Ltmp60:
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-208(%rbp), %rsi        ## 8-byte Reload
	movq	-224(%rbp), %rdx        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp61:
	jmp	LBB4_9
LBB4_9:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
	jmp	LBB4_10
LBB4_10:
Ltmp63:
	leaq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp64:
	jmp	LBB4_11
LBB4_11:
	leaq	-152(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp69:
	callq	___cxa_end_catch
Ltmp70:
	jmp	LBB4_12
LBB4_12:
	jmp	LBB4_13
LBB4_13:
	movb	$1, -97(%rbp)
	testb	$1, -97(%rbp)
	jne	LBB4_22
	jmp	LBB4_21
LBB4_14:
Ltmp50:
	leaq	-96(%rbp), %rdi
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNK3$_0clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_
Ltmp51:
	jmp	LBB4_15
LBB4_15:
	jmp	LBB4_13
LBB4_16:
Ltmp62:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB4_19
LBB4_17:
Ltmp65:
	leaq	-152(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB4_19
LBB4_18:
Ltmp71:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB4_23
LBB4_19:
Ltmp66:
	callq	___cxa_end_catch
Ltmp67:
	jmp	LBB4_20
LBB4_20:
	jmp	LBB4_23
LBB4_21:
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallowerD1Ev
LBB4_22:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$240, %rsp
	popq	%rbp
	retq
LBB4_23:
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallowerD1Ev
## BB#24:
	movq	-112(%rbp), %rdi
	callq	__Unwind_Resume
LBB4_25:
Ltmp68:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -228(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB4_26:
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\223\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\202\001"              ## Call site table length
Lset32 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset32
Lset33 = Ltmp52-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp52
	.long	Lset33
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp52-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset34
Lset35 = Ltmp53-Ltmp52                  ##   Call between Ltmp52 and Ltmp53
	.long	Lset35
Lset36 = Ltmp54-Lfunc_begin1            ##     jumps to Ltmp54
	.long	Lset36
	.byte	3                       ##   On action: 2
Lset37 = Ltmp55-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset37
Lset38 = Ltmp56-Ltmp55                  ##   Call between Ltmp55 and Ltmp56
	.long	Lset38
Lset39 = Ltmp57-Lfunc_begin1            ##     jumps to Ltmp57
	.long	Lset39
	.byte	3                       ##   On action: 2
Lset40 = Ltmp56-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset40
Lset41 = Ltmp58-Ltmp56                  ##   Call between Ltmp56 and Ltmp58
	.long	Lset41
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp58-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset42
Lset43 = Ltmp61-Ltmp58                  ##   Call between Ltmp58 and Ltmp61
	.long	Lset43
Lset44 = Ltmp62-Lfunc_begin1            ##     jumps to Ltmp62
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp63-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset45
Lset46 = Ltmp64-Ltmp63                  ##   Call between Ltmp63 and Ltmp64
	.long	Lset46
Lset47 = Ltmp65-Lfunc_begin1            ##     jumps to Ltmp65
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp69-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset48
Lset49 = Ltmp70-Ltmp69                  ##   Call between Ltmp69 and Ltmp70
	.long	Lset49
Lset50 = Ltmp71-Lfunc_begin1            ##     jumps to Ltmp71
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp50-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset51
Lset52 = Ltmp51-Ltmp50                  ##   Call between Ltmp50 and Ltmp51
	.long	Lset52
Lset53 = Ltmp57-Lfunc_begin1            ##     jumps to Ltmp57
	.long	Lset53
	.byte	3                       ##   On action: 2
Lset54 = Ltmp66-Lfunc_begin1            ## >> Call Site 9 <<
	.long	Lset54
Lset55 = Ltmp67-Ltmp66                  ##   Call between Ltmp66 and Ltmp67
	.long	Lset55
Lset56 = Ltmp68-Lfunc_begin1            ##     jumps to Ltmp68
	.long	Lset56
	.byte	5                       ##   On action: 3
Lset57 = Ltmp67-Lfunc_begin1            ## >> Call Site 10 <<
	.long	Lset57
Lset58 = Lfunc_end1-Ltmp67              ##   Call between Ltmp67 and Lfunc_end1
	.long	Lset58
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	2                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 2
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 2
	.long	__ZTISt9exception@GOTPCREL+4 ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp80:
	.cfi_def_cfa_offset 16
Ltmp81:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp82:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp75:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp76:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB5_1
LBB5_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp77:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp78:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB5_3
LBB5_2:
Ltmp79:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB5_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset59 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset59
Lset60 = Ltmp75-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp75
	.long	Lset60
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset61 = Ltmp75-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset61
Lset62 = Ltmp78-Ltmp75                  ##   Call between Ltmp75 and Ltmp78
	.long	Lset62
Lset63 = Ltmp79-Lfunc_begin2            ##     jumps to Ltmp79
	.long	Lset63
	.byte	0                       ##   On action: cleanup
Lset64 = Ltmp78-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset64
Lset65 = Lfunc_end2-Ltmp78              ##   Call between Ltmp78 and Lfunc_end2
	.long	Lset65
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN19exception_swallowerD1Ev
	.weak_def_can_be_hidden	__ZN19exception_swallowerD1Ev
	.align	4, 0x90
__ZN19exception_swallowerD1Ev:          ## @_ZN19exception_swallowerD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp83:
	.cfi_def_cfa_offset 16
Ltmp84:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp85:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN19exception_swallowerD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev: ## @_ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp86:
	.cfi_def_cfa_offset 16
Ltmp87:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp88:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$424, %rsi              ## imm = 0x1A8
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_1E19exception_swallowerT_T0_: ## @"_Z20perform_op_on_streamINSt3__114basic_ifstreamIcNS0_11char_traitsIcEEEE3$_1E19exception_swallowerT_T0_"
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp111:
	.cfi_def_cfa_offset 16
Ltmp112:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp113:
	.cfi_def_cfa_register %rbp
	subq	$240, %rsp
	movq	%rdi, %rax
	movb	$0, -97(%rbp)
	movq	%rdi, -160(%rbp)        ## 8-byte Spill
	movq	%rax, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, -176(%rbp)        ## 8-byte Spill
	callq	__ZN19exception_swallowerC1Ev
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movl	32(%rax), %ecx
	andl	$5, %ecx
	cmpl	$0, %ecx
	setne	%dl
	movb	%dl, -177(%rbp)         ## 1-byte Spill
## BB#1:
	movb	-177(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB8_2
	jmp	LBB8_14
LBB8_2:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
Ltmp91:
	leaq	L_.str.15(%rip), %rsi
	movq	%rdi, -192(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt13runtime_errorC1EPKc
Ltmp92:
	jmp	LBB8_3
LBB8_3:
Ltmp94:
	movq	__ZTISt13runtime_error@GOTPCREL(%rip), %rsi
	movq	__ZNSt13runtime_errorD1Ev@GOTPCREL(%rip), %rdx
	movq	-192(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_throw
Ltmp95:
	jmp	LBB8_26
LBB8_4:
Ltmp96:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB8_6
LBB8_5:
Ltmp93:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	movq	-192(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
LBB8_6:
	movl	-116(%rbp), %eax
	movl	$1, %ecx
	cmpl	%ecx, %eax
	jne	LBB8_23
## BB#7:
	movq	-112(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -128(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %rdi
	movq	%rdi, -200(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-200(%rbp), %rax        ## 8-byte Reload
	callq	*%rax
	leaq	-152(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdi, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, 16(%rax)
	movq	$0, 8(%rax)
	movq	$0, (%rax)
	movq	-48(%rbp), %rdi
Ltmp97:
	movq	%rdi, -208(%rbp)        ## 8-byte Spill
	movq	%rax, -216(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp98:
	movq	%rax, -224(%rbp)        ## 8-byte Spill
	jmp	LBB8_8
LBB8_8:                                 ## %.noexc
Ltmp99:
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-208(%rbp), %rsi        ## 8-byte Reload
	movq	-224(%rbp), %rdx        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp100:
	jmp	LBB8_9
LBB8_9:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
	jmp	LBB8_10
LBB8_10:
Ltmp102:
	leaq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Ltmp103:
	jmp	LBB8_11
LBB8_11:
	leaq	-152(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp108:
	callq	___cxa_end_catch
Ltmp109:
	jmp	LBB8_12
LBB8_12:
	jmp	LBB8_13
LBB8_13:
	movb	$1, -97(%rbp)
	testb	$1, -97(%rbp)
	jne	LBB8_22
	jmp	LBB8_21
LBB8_14:
Ltmp89:
	leaq	-96(%rbp), %rdi
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNK3$_1clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_
Ltmp90:
	jmp	LBB8_15
LBB8_15:
	jmp	LBB8_13
LBB8_16:
Ltmp101:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB8_19
LBB8_17:
Ltmp104:
	leaq	-152(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB8_19
LBB8_18:
Ltmp110:
	movl	%edx, %ecx
	movq	%rax, -112(%rbp)
	movl	%ecx, -116(%rbp)
	jmp	LBB8_23
LBB8_19:
Ltmp105:
	callq	___cxa_end_catch
Ltmp106:
	jmp	LBB8_20
LBB8_20:
	jmp	LBB8_23
LBB8_21:
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallowerD1Ev
LBB8_22:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$240, %rsp
	popq	%rbp
	retq
LBB8_23:
	movq	-160(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN19exception_swallowerD1Ev
## BB#24:
	movq	-112(%rbp), %rdi
	callq	__Unwind_Resume
LBB8_25:
Ltmp107:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -228(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB8_26:
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table8:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\223\201"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\202\001"              ## Call site table length
Lset66 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset66
Lset67 = Ltmp91-Lfunc_begin3            ##   Call between Lfunc_begin3 and Ltmp91
	.long	Lset67
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset68 = Ltmp91-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset68
Lset69 = Ltmp92-Ltmp91                  ##   Call between Ltmp91 and Ltmp92
	.long	Lset69
Lset70 = Ltmp93-Lfunc_begin3            ##     jumps to Ltmp93
	.long	Lset70
	.byte	3                       ##   On action: 2
Lset71 = Ltmp94-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset71
Lset72 = Ltmp95-Ltmp94                  ##   Call between Ltmp94 and Ltmp95
	.long	Lset72
Lset73 = Ltmp96-Lfunc_begin3            ##     jumps to Ltmp96
	.long	Lset73
	.byte	3                       ##   On action: 2
Lset74 = Ltmp95-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset74
Lset75 = Ltmp97-Ltmp95                  ##   Call between Ltmp95 and Ltmp97
	.long	Lset75
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset76 = Ltmp97-Lfunc_begin3            ## >> Call Site 5 <<
	.long	Lset76
Lset77 = Ltmp100-Ltmp97                 ##   Call between Ltmp97 and Ltmp100
	.long	Lset77
Lset78 = Ltmp101-Lfunc_begin3           ##     jumps to Ltmp101
	.long	Lset78
	.byte	0                       ##   On action: cleanup
Lset79 = Ltmp102-Lfunc_begin3           ## >> Call Site 6 <<
	.long	Lset79
Lset80 = Ltmp103-Ltmp102                ##   Call between Ltmp102 and Ltmp103
	.long	Lset80
Lset81 = Ltmp104-Lfunc_begin3           ##     jumps to Ltmp104
	.long	Lset81
	.byte	0                       ##   On action: cleanup
Lset82 = Ltmp108-Lfunc_begin3           ## >> Call Site 7 <<
	.long	Lset82
Lset83 = Ltmp109-Ltmp108                ##   Call between Ltmp108 and Ltmp109
	.long	Lset83
Lset84 = Ltmp110-Lfunc_begin3           ##     jumps to Ltmp110
	.long	Lset84
	.byte	0                       ##   On action: cleanup
Lset85 = Ltmp89-Lfunc_begin3            ## >> Call Site 8 <<
	.long	Lset85
Lset86 = Ltmp90-Ltmp89                  ##   Call between Ltmp89 and Ltmp90
	.long	Lset86
Lset87 = Ltmp96-Lfunc_begin3            ##     jumps to Ltmp96
	.long	Lset87
	.byte	3                       ##   On action: 2
Lset88 = Ltmp105-Lfunc_begin3           ## >> Call Site 9 <<
	.long	Lset88
Lset89 = Ltmp106-Ltmp105                ##   Call between Ltmp105 and Ltmp106
	.long	Lset89
Lset90 = Ltmp107-Lfunc_begin3           ##     jumps to Ltmp107
	.long	Lset90
	.byte	5                       ##   On action: 3
Lset91 = Ltmp106-Lfunc_begin3           ## >> Call Site 10 <<
	.long	Lset91
Lset92 = Lfunc_end3-Ltmp106             ##   Call between Ltmp106 and Lfunc_end3
	.long	Lset92
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	2                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 2
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 2
	.long	__ZTISt9exception@GOTPCREL+4 ## TypeInfo 1
	.align	2

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init:                 ## @__cxx_global_var_init
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp114:
	.cfi_def_cfa_offset 16
Ltmp115:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp116:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN19exception_swallowerD2Ev
	.weak_def_can_be_hidden	__ZN19exception_swallowerD2Ev
	.align	4, 0x90
__ZN19exception_swallowerD2Ev:          ## @_ZN19exception_swallowerD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp117:
	.cfi_def_cfa_offset 16
Ltmp118:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp119:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	addq	$8, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED2Ev
	.align	4, 0x90
__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED2Ev: ## @_ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$16, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev: ## @_ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp123:
	.cfi_def_cfa_offset 16
Ltmp124:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp125:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev: ## @_ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp126:
	.cfi_def_cfa_offset 16
Ltmp127:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp128:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev: ## @_ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp129:
	.cfi_def_cfa_offset 16
Ltmp130:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp131:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp132:
	.cfi_def_cfa_offset 16
Ltmp133:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp134:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp141:
	.cfi_def_cfa_offset 16
Ltmp142:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp143:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rdi, %rax
	movq	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rdi)
Ltmp135:
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
Ltmp136:
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	jmp	LBB16_1
LBB16_1:
	jmp	LBB16_5
LBB16_2:
Ltmp137:
	movl	%edx, %ecx
	movq	%rax, -16(%rbp)
	movl	%ecx, -20(%rbp)
## BB#3:
	movq	-16(%rbp), %rdi
	callq	___cxa_begin_catch
Ltmp138:
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	___cxa_end_catch
Ltmp139:
	jmp	LBB16_4
LBB16_4:
	jmp	LBB16_5
LBB16_5:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	testb	$1, 400(%rax)
	je	LBB16_10
## BB#6:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -56(%rbp)         ## 8-byte Spill
	je	LBB16_8
## BB#7:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdaPv
LBB16_8:
	jmp	LBB16_10
LBB16_9:
Ltmp140:
	movl	%edx, %ecx
	movq	%rax, -16(%rbp)
	movl	%ecx, -20(%rbp)
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	jmp	LBB16_15
LBB16_10:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	testb	$1, 401(%rax)
	je	LBB16_14
## BB#11:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	104(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -64(%rbp)         ## 8-byte Spill
	je	LBB16_13
## BB#12:
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdaPv
LBB16_13:
	jmp	LBB16_14
LBB16_14:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$64, %rsp
	popq	%rbp
	retq
LBB16_15:
	movq	-16(%rbp), %rdi
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table16:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\257\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset93 = Ltmp135-Lfunc_begin4           ## >> Call Site 1 <<
	.long	Lset93
Lset94 = Ltmp136-Ltmp135                ##   Call between Ltmp135 and Ltmp136
	.long	Lset94
Lset95 = Ltmp137-Lfunc_begin4           ##     jumps to Ltmp137
	.long	Lset95
	.byte	1                       ##   On action: 1
Lset96 = Ltmp136-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset96
Lset97 = Ltmp138-Ltmp136                ##   Call between Ltmp136 and Ltmp138
	.long	Lset97
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset98 = Ltmp138-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset98
Lset99 = Ltmp139-Ltmp138                ##   Call between Ltmp138 and Ltmp139
	.long	Lset99
Lset100 = Ltmp140-Lfunc_begin4          ##     jumps to Ltmp140
	.long	Lset100
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp155:
	.cfi_def_cfa_offset 16
Ltmp156:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp157:
	.cfi_def_cfa_register %rbp
	subq	$496, %rsp              ## imm = 0x1F0
	movq	%rdi, -392(%rbp)
	movq	-392(%rbp), %rdi
	movq	$0, -400(%rbp)
	cmpq	$0, 120(%rdi)
	movq	%rdi, -448(%rbp)        ## 8-byte Spill
	je	LBB17_18
## BB#1:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	120(%rax), %rcx
	movq	_fclose@GOTPCREL(%rip), %rdx
	movq	%rdx, -424(%rbp)
	leaq	-416(%rbp), %rdx
	movq	%rdx, -368(%rbp)
	movq	%rcx, -376(%rbp)
	leaq	-424(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-368(%rbp), %rdx
	movq	-376(%rbp), %rsi
	movq	%rdx, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	movq	%rcx, -336(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rdx, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	%rdx, -288(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	-288(%rbp), %rcx
	leaq	-296(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-296(%rbp), %rdx
	leaq	-304(%rbp), %rsi
	movq	%rsi, -232(%rbp)
	movq	-304(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	-256(%rbp), %rcx
	leaq	-264(%rbp), %rdx
	movq	%rdx, -248(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rdx, (%rcx)
	leaq	-272(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rax), %rcx
	movq	48(%rcx), %rcx
Ltmp144:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp145:
	movl	%eax, -452(%rbp)        ## 4-byte Spill
	jmp	LBB17_2
LBB17_2:
	movl	-452(%rbp), %eax        ## 4-byte Reload
	cmpl	$0, %eax
	je	LBB17_9
## BB#3:
	movq	$0, -400(%rbp)
	jmp	LBB17_9
LBB17_4:
Ltmp148:
	leaq	-416(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -432(%rbp)
	movl	%esi, -436(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -120(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -120(%rbp)
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	je	LBB17_8
## BB#5:
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	8(%rax), %rcx
	movq	-120(%rbp), %rdi
Ltmp149:
	callq	*%rcx
Ltmp150:
	movl	%eax, -468(%rbp)        ## 4-byte Spill
	jmp	LBB17_6
LBB17_6:
	jmp	LBB17_8
LBB17_7:
Ltmp151:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -472(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB17_8:                                ## %_ZNSt3__110unique_ptrI7__sFILEPFiPS1_EED1Ev.exit2
	jmp	LBB17_19
LBB17_9:
	leaq	-416(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	-416(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, -416(%rbp)
	movq	-48(%rbp), %rdi
Ltmp146:
	callq	_fclose
Ltmp147:
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	jmp	LBB17_10
LBB17_10:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	cmpl	$0, %eax
	jne	LBB17_12
## BB#11:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	$0, 120(%rax)
	jmp	LBB17_13
LBB17_12:
	movq	$0, -400(%rbp)
LBB17_13:
	leaq	-416(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	$0, -200(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -208(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -208(%rbp)
	movq	%rax, -488(%rbp)        ## 8-byte Spill
	je	LBB17_17
## BB#14:
	movq	-488(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	8(%rax), %rcx
	movq	-208(%rbp), %rdi
Ltmp152:
	callq	*%rcx
Ltmp153:
	movl	%eax, -492(%rbp)        ## 4-byte Spill
	jmp	LBB17_15
LBB17_15:
	jmp	LBB17_17
LBB17_16:
Ltmp154:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -496(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB17_17:                               ## %_ZNSt3__110unique_ptrI7__sFILEPFiPS1_EED1Ev.exit
	jmp	LBB17_18
LBB17_18:
	movq	-400(%rbp), %rax
	addq	$496, %rsp              ## imm = 0x1F0
	popq	%rbp
	retq
LBB17_19:
	movq	-432(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table17:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset101 = Ltmp144-Lfunc_begin5          ## >> Call Site 1 <<
	.long	Lset101
Lset102 = Ltmp145-Ltmp144               ##   Call between Ltmp144 and Ltmp145
	.long	Lset102
Lset103 = Ltmp148-Lfunc_begin5          ##     jumps to Ltmp148
	.long	Lset103
	.byte	0                       ##   On action: cleanup
Lset104 = Ltmp149-Lfunc_begin5          ## >> Call Site 2 <<
	.long	Lset104
Lset105 = Ltmp150-Ltmp149               ##   Call between Ltmp149 and Ltmp150
	.long	Lset105
Lset106 = Ltmp151-Lfunc_begin5          ##     jumps to Ltmp151
	.long	Lset106
	.byte	1                       ##   On action: 1
Lset107 = Ltmp146-Lfunc_begin5          ## >> Call Site 3 <<
	.long	Lset107
Lset108 = Ltmp147-Ltmp146               ##   Call between Ltmp146 and Ltmp147
	.long	Lset108
Lset109 = Ltmp148-Lfunc_begin5          ##     jumps to Ltmp148
	.long	Lset109
	.byte	0                       ##   On action: cleanup
Lset110 = Ltmp152-Lfunc_begin5          ## >> Call Site 4 <<
	.long	Lset110
Lset111 = Ltmp153-Ltmp152               ##   Call between Ltmp152 and Ltmp153
	.long	Lset111
Lset112 = Ltmp154-Lfunc_begin5          ##     jumps to Ltmp154
	.long	Lset112
	.byte	1                       ##   On action: 1
Lset113 = Ltmp153-Lfunc_begin5          ## >> Call Site 5 <<
	.long	Lset113
Lset114 = Lfunc_end5-Ltmp153            ##   Call between Ltmp153 and Lfunc_end5
	.long	Lset114
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp158:
	.cfi_def_cfa_offset 16
Ltmp159:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp160:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp161:
	.cfi_def_cfa_offset 16
Ltmp162:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp163:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, -104(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-104(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	callq	*48(%rax)
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movl	%eax, -116(%rbp)        ## 4-byte Spill
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
	movq	-112(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, 128(%rsi)
	movb	402(%rsi), %cl
	andb	$1, %cl
	movb	%cl, -89(%rbp)
	movq	128(%rsi), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rdi
	movq	%rdi, -128(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-128(%rbp), %rax        ## 8-byte Reload
	callq	*56(%rax)
	andb	$1, %al
	movq	-112(%rbp), %rsi        ## 8-byte Reload
	movb	%al, 402(%rsi)
	movb	-89(%rbp), %al
	andb	$1, %al
	movzbl	%al, %edx
	movb	402(%rsi), %al
	andb	$1, %al
	movzbl	%al, %r8d
	cmpl	%r8d, %edx
	je	LBB20_13
## BB#1:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-56(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB20_7
## BB#2:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 400(%rax)
	je	LBB20_6
## BB#3:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -136(%rbp)        ## 8-byte Spill
	je	LBB20_5
## BB#4:
	movq	-136(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB20_5:
	jmp	LBB20_6
LBB20_6:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movb	401(%rax), %cl
	andb	$1, %cl
	movb	%cl, 400(%rax)
	movq	112(%rax), %rdx
	movq	%rdx, 96(%rax)
	movq	104(%rax), %rdx
	movq	%rdx, 64(%rax)
	movq	$0, 112(%rax)
	movq	$0, 104(%rax)
	movb	$0, 401(%rax)
	jmp	LBB20_12
LBB20_7:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 400(%rax)
	jne	LBB20_10
## BB#8:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	addq	$88, %rax
	cmpq	%rax, %rcx
	je	LBB20_10
## BB#9:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, 112(%rax)
	movq	64(%rax), %rcx
	movq	%rcx, 104(%rax)
	movb	$0, 401(%rax)
	movq	96(%rax), %rdi
	callq	__Znam
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movb	$1, 400(%rcx)
	jmp	LBB20_11
LBB20_10:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, 112(%rax)
	movq	112(%rax), %rdi
	callq	__Znam
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 104(%rcx)
	movb	$1, 401(%rcx)
LBB20_11:
	jmp	LBB20_12
LBB20_12:
	jmp	LBB20_13
LBB20_13:
	addq	$144, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp164:
	.cfi_def_cfa_offset 16
Ltmp165:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp166:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -136(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -128(%rbp)
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%rdi, 16(%rsi)
	movq	-120(%rbp), %rdi
	movq	%rdi, 24(%rsi)
	movq	-128(%rbp), %rdi
	movq	%rdi, 32(%rsi)
	movq	%rdx, %rsi
	movq	%rsi, -8(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -24(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, 48(%rsi)
	movq	%rdi, 40(%rsi)
	movq	-24(%rbp), %rdi
	movq	%rdi, 56(%rsi)
	testb	$1, 400(%rdx)
	movq	%rdx, -168(%rbp)        ## 8-byte Spill
	je	LBB21_4
## BB#1:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -176(%rbp)        ## 8-byte Spill
	je	LBB21_3
## BB#2:
	movq	-176(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB21_3:
	jmp	LBB21_4
LBB21_4:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 401(%rax)
	je	LBB21_8
## BB#5:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	104(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -184(%rbp)        ## 8-byte Spill
	je	LBB21_7
## BB#6:
	movq	-184(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB21_7:
	jmp	LBB21_8
LBB21_8:
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 96(%rcx)
	cmpq	$8, 96(%rcx)
	jbe	LBB21_14
## BB#9:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB21_12
## BB#10:
	cmpq	$0, -144(%rbp)
	je	LBB21_12
## BB#11:
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movb	$0, 400(%rcx)
	jmp	LBB21_13
LBB21_12:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rdi
	callq	__Znam
	movq	-168(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, 64(%rdi)
	movb	$1, 400(%rdi)
LBB21_13:
	jmp	LBB21_15
LBB21_14:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$88, %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movq	$8, 96(%rcx)
	movb	$0, 400(%rcx)
LBB21_15:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	jne	LBB21_24
## BB#16:
	leaq	-56(%rbp), %rax
	leaq	-160(%rbp), %rcx
	leaq	-152(%rbp), %rdx
	movq	$8, -160(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	-48(%rbp), %rcx
	cmpq	(%rcx), %rax
	jge	LBB21_18
## BB#17:
	movq	-72(%rbp), %rax
	movq	%rax, -192(%rbp)        ## 8-byte Spill
	jmp	LBB21_19
LBB21_18:
	movq	-64(%rbp), %rax
	movq	%rax, -192(%rbp)        ## 8-byte Spill
LBB21_19:                               ## %_ZNSt3__13maxIlEERKT_S3_S3_.exit
	movq	-192(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 112(%rcx)
	cmpq	$0, -144(%rbp)
	je	LBB21_22
## BB#20:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	cmpq	$8, 112(%rax)
	jb	LBB21_22
## BB#21:
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 104(%rcx)
	movb	$0, 401(%rcx)
	jmp	LBB21_23
LBB21_22:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	112(%rax), %rdi
	callq	__Znam
	movq	-168(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, 104(%rdi)
	movb	$1, 401(%rdi)
LBB21_23:
	jmp	LBB21_25
LBB21_24:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	$0, 112(%rax)
	movq	$0, 104(%rax)
	movb	$0, 401(%rax)
LBB21_25:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp167:
	.cfi_def_cfa_offset 16
Ltmp168:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp169:
	.cfi_def_cfa_register %rbp
	subq	$720, %rsp              ## imm = 0x2D0
	movq	%rdi, %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %r9
	movq	(%r9), %r9
	movq	%r9, -8(%rbp)
	movq	%rsi, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movl	%ecx, -564(%rbp)
	movl	%r8d, -568(%rbp)
	movq	-552(%rbp), %rdx
	cmpq	$0, 128(%rdx)
	movq	%rax, -584(%rbp)        ## 8-byte Spill
	movq	%rdi, -592(%rbp)        ## 8-byte Spill
	movq	%rdx, -600(%rbp)        ## 8-byte Spill
	jne	LBB22_2
## BB#1:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -608(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-608(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -616(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-616(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB22_2:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, %rdi
	callq	*48(%rdx)
	movl	%eax, -572(%rbp)
	movq	-600(%rbp), %rcx        ## 8-byte Reload
	cmpq	$0, 120(%rcx)
	je	LBB22_6
## BB#3:
	cmpl	$0, -572(%rbp)
	jg	LBB22_5
## BB#4:
	cmpq	$0, -560(%rbp)
	jne	LBB22_6
LBB22_5:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	cmpl	$0, %eax
	je	LBB22_7
LBB22_6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	$-1, -536(%rbp)
	movq	-528(%rbp), %rdi
	movq	-536(%rbp), %r8
	movq	%rdi, -512(%rbp)
	movq	%r8, -520(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -624(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-520(%rbp), %rcx
	movq	-624(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB22_18
LBB22_7:
	movl	-564(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -628(%rbp)        ## 4-byte Spill
	je	LBB22_8
	jmp	LBB22_21
LBB22_21:
	movl	-628(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -632(%rbp)        ## 4-byte Spill
	je	LBB22_9
	jmp	LBB22_22
LBB22_22:
	movl	-628(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -636(%rbp)        ## 4-byte Spill
	je	LBB22_10
	jmp	LBB22_11
LBB22_8:
	movl	$0, -576(%rbp)
	jmp	LBB22_12
LBB22_9:
	movl	$1, -576(%rbp)
	jmp	LBB22_12
LBB22_10:
	movl	$2, -576(%rbp)
	jmp	LBB22_12
LBB22_11:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -648(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-648(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB22_18
LBB22_12:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	cmpl	$0, -572(%rbp)
	movq	%rdi, -656(%rbp)        ## 8-byte Spill
	jle	LBB22_14
## BB#13:
	movslq	-572(%rbp), %rax
	imulq	-560(%rbp), %rax
	movq	%rax, -664(%rbp)        ## 8-byte Spill
	jmp	LBB22_15
LBB22_14:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -664(%rbp)        ## 8-byte Spill
	jmp	LBB22_15
LBB22_15:
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movl	-576(%rbp), %edx
	movq	-656(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB22_17
## BB#16:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -456(%rbp)
	movq	$-1, -464(%rbp)
	movq	-456(%rbp), %rdi
	movq	-464(%rbp), %r8
	movq	%rdi, -440(%rbp)
	movq	%r8, -448(%rbp)
	movq	-440(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -672(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-448(%rbp), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB22_18
LBB22_17:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_ftello
	movl	$136, %ecx
	movl	%ecx, %edx
	leaq	-272(%rbp), %rdi
	movl	$128, %ecx
	movl	%ecx, %esi
	leaq	-136(%rbp), %r8
	leaq	-400(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%rdi, -488(%rbp)
	movq	%rax, -496(%rbp)
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %r10
	movq	%rax, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %rax
	movq	%rax, %r10
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, -688(%rbp)        ## 8-byte Spill
	movl	%ecx, %esi
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	%rdx, -696(%rbp)        ## 8-byte Spill
	movq	%r10, %rdx
	movq	%r9, -704(%rbp)         ## 8-byte Spill
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movq	%r8, -720(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-480(%rbp), %rax
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, 128(%rdx)
	movq	-600(%rbp), %rax        ## 8-byte Reload
	addq	$136, %rax
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rdx
	movq	-720(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, %rdi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	-696(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
LBB22_18:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	jne	LBB22_20
## BB#19:                               ## %SP_return
	movq	-584(%rbp), %rax        ## 8-byte Reload
	addq	$720, %rsp              ## imm = 0x2D0
	popq	%rbp
	retq
LBB22_20:                               ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp170:
	.cfi_def_cfa_offset 16
Ltmp171:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp172:
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp              ## imm = 0x140
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	movq	___stack_chk_guard@GOTPCREL(%rip), %r8
	movq	(%r8), %r8
	movq	%r8, -8(%rbp)
	movq	%rsi, -224(%rbp)
	movl	%edx, -228(%rbp)
	movq	-224(%rbp), %rsi
	cmpq	$0, 120(%rsi)
	movq	%rax, -240(%rbp)        ## 8-byte Spill
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	je	LBB23_2
## BB#1:
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	cmpl	$0, %eax
	je	LBB23_3
LBB23_2:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	$-1, -216(%rbp)
	movq	-208(%rbp), %rdi
	movq	-216(%rbp), %r8
	movq	%rdi, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -272(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-200(%rbp), %rcx
	movq	-272(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB23_6
LBB23_3:
	xorl	%edx, %edx
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	movq	-248(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rsi
	movq	128(%rsi), %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB23_5
## BB#4:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -168(%rbp)
	movq	$-1, -176(%rbp)
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r8
	movq	%rdi, -152(%rbp)
	movq	%r8, -160(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -280(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-160(%rbp), %rcx
	movq	-280(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB23_6
LBB23_5:
	movl	$136, %eax
	movl	%eax, %edx
	movl	$128, %eax
	movl	%eax, %ecx
	leaq	-136(%rbp), %rsi
	movq	-264(%rbp), %rdi        ## 8-byte Reload
	addq	$136, %rdi
	movq	-248(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -184(%rbp)
	movq	-184(%rbp), %r9
	movq	%rsi, %r10
	movq	%rdi, -288(%rbp)        ## 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, -296(%rbp)        ## 8-byte Spill
	movq	%r9, %rsi
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	%rcx, -312(%rbp)        ## 8-byte Spill
	callq	_memcpy
	movq	-288(%rbp), %rcx        ## 8-byte Reload
	movq	-296(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	-312(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	-248(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	-304(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
LBB23_6:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	jne	LBB23_8
## BB#7:                                ## %SP_return
	movq	-240(%rbp), %rax        ## 8-byte Reload
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbp
	retq
LBB23_8:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp173:
	.cfi_def_cfa_offset 16
Ltmp174:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp175:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rax
	cmpq	$0, 120(%rax)
	movq	%rax, -408(%rbp)        ## 8-byte Spill
	jne	LBB24_2
## BB#1:
	movl	$0, -340(%rbp)
	jmp	LBB24_35
LBB24_2:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB24_4
## BB#3:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-416(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -424(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-424(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB24_4:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	396(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB24_19
## BB#5:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	48(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rcx
	cmpq	40(%rcx), %rax
	je	LBB24_9
## BB#6:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movq	%rcx, -432(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-432(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -436(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-436(%rbp), %esi        ## 4-byte Reload
	cmpl	%eax, %esi
	jne	LBB24_8
## BB#7:
	movl	$-1, -340(%rbp)
	jmp	LBB24_35
LBB24_8:
	jmp	LBB24_9
LBB24_9:
	jmp	LBB24_10
LBB24_10:                               ## =>This Inner Loop Header: Depth=1
	leaq	-368(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	128(%rcx), %rdx
	addq	$136, %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	movq	64(%rsi), %rdi
	movq	64(%rsi), %r8
	addq	96(%rsi), %r8
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rdi
	movq	-192(%rbp), %r8
	movq	%rdi, -448(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	-456(%rbp), %r9         ## 8-byte Reload
	callq	*%r9
	movl	$1, %r10d
	movl	%r10d, %esi
	movl	%eax, -356(%rbp)
	movq	-368(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	64(%rdx), %rdi
	subq	%rdi, %rcx
	movq	%rcx, -376(%rbp)
	movq	64(%rdx), %rdi
	movq	-376(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-376(%rbp), %rax
	je	LBB24_12
## BB#11:
	movl	$-1, -340(%rbp)
	jmp	LBB24_35
LBB24_12:                               ##   in Loop: Header=BB24_10 Depth=1
	jmp	LBB24_13
LBB24_13:                               ##   in Loop: Header=BB24_10 Depth=1
	cmpl	$1, -356(%rbp)
	je	LBB24_10
## BB#14:
	cmpl	$2, -356(%rbp)
	jne	LBB24_16
## BB#15:
	movl	$-1, -340(%rbp)
	jmp	LBB24_35
LBB24_16:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_fflush
	cmpl	$0, %eax
	je	LBB24_18
## BB#17:
	movl	$-1, -340(%rbp)
	jmp	LBB24_35
LBB24_18:
	jmp	LBB24_34
LBB24_19:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	396(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB24_33
## BB#20:
	movl	$128, %eax
	movl	%eax, %edx
	leaq	-136(%rbp), %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	addq	$264, %rsi              ## imm = 0x108
	movq	%rcx, %rdi
	callq	_memcpy
	movb	$0, -385(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	testb	$1, 402(%rcx)
	je	LBB24_22
## BB#21:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	32(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	24(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -384(%rbp)
	jmp	LBB24_28
LBB24_22:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, %rdi
	callq	*48(%rdx)
	movl	%eax, -392(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	80(%rcx), %rdx
	movq	72(%rcx), %rdi
	subq	%rdi, %rdx
	movq	%rdx, -384(%rbp)
	cmpl	$0, -392(%rbp)
	jle	LBB24_24
## BB#23:
	movslq	-392(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	32(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -216(%rbp)
	movq	-216(%rbp), %rdx
	movq	24(%rdx), %rdx
	subq	%rdx, %rcx
	imulq	%rcx, %rax
	addq	-384(%rbp), %rax
	movq	%rax, -384(%rbp)
	jmp	LBB24_27
LBB24_24:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	24(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	cmpq	32(%rcx), %rax
	je	LBB24_26
## BB#25:
	leaq	-136(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	128(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	72(%rcx), %rdi
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	24(%rcx), %rcx
	movq	-408(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	16(%r8), %r8
	subq	%r8, %rcx
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-264(%rbp), %rax
	movq	(%rax), %rcx
	movq	64(%rcx), %rcx
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %rdi
	movq	-296(%rbp), %r8
	movq	%rdi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -472(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	-472(%rbp), %r9         ## 8-byte Reload
	callq	*%r9
	movl	%eax, -396(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	72(%rcx), %rdx
	movq	64(%rcx), %rsi
	subq	%rsi, %rdx
	movslq	-396(%rbp), %rsi
	subq	%rsi, %rdx
	addq	-384(%rbp), %rdx
	movq	%rdx, -384(%rbp)
	movb	$1, -385(%rbp)
LBB24_26:
	jmp	LBB24_27
LBB24_27:
	jmp	LBB24_28
LBB24_28:
	movl	$1, %edx
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	movq	120(%rsi), %rdi
	subq	-384(%rbp), %rcx
	movq	%rcx, %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB24_30
## BB#29:
	movl	$-1, -340(%rbp)
	jmp	LBB24_35
LBB24_30:
	testb	$1, -385(%rbp)
	je	LBB24_32
## BB#31:
	movl	$128, %eax
	movl	%eax, %edx
	leaq	-136(%rbp), %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	addq	$136, %rsi
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
LBB24_32:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	%rcx, 80(%rax)
	movq	%rcx, 72(%rax)
	movq	%rax, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -328(%rbp)
	movq	-304(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-320(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-328(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	$0, 396(%rax)
LBB24_33:
	jmp	LBB24_34
LBB24_34:
	movl	$0, -340(%rbp)
LBB24_35:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movl	-340(%rbp), %ecx
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	movl	%ecx, -476(%rbp)        ## 4-byte Spill
	jne	LBB24_37
## BB#36:                               ## %SP_return
	movl	-476(%rbp), %eax        ## 4-byte Reload
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB24_37:                               ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp176:
	.cfi_def_cfa_offset 16
Ltmp177:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp178:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$792, %rsp              ## imm = 0x318
Ltmp179:
	.cfi_offset %rbx, -24
	movq	%rdi, -576(%rbp)
	movq	-576(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	jne	LBB25_2
## BB#1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -564(%rbp)
	jmp	LBB25_36
LBB25_2:
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	andb	$1, %al
	movb	%al, -577(%rbp)
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -560(%rbp)
	movq	-560(%rbp), %rdi
	cmpq	$0, 24(%rdi)
	jne	LBB25_4
## BB#3:
	leaq	-578(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, %rdx
	addq	$1, %rdx
	movq	%rax, %rsi
	addq	$1, %rsi
	movq	%rcx, -488(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rsi, -512(%rbp)
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-504(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB25_4:
	testb	$1, -577(%rbp)
	je	LBB25_6
## BB#5:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -688(%rbp)        ## 8-byte Spill
	jmp	LBB25_10
LBB25_6:
	leaq	-248(%rbp), %rax
	leaq	-608(%rbp), %rcx
	leaq	-600(%rbp), %rdx
	movl	$2, %esi
	movl	%esi, %edi
	movq	-680(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -472(%rbp)
	movq	-472(%rbp), %r8
	movq	32(%r8), %r8
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -400(%rbp)
	movq	-400(%rbp), %r9
	movq	16(%r9), %r9
	subq	%r9, %r8
	movq	%rax, -696(%rbp)        ## 8-byte Spill
	movq	%r8, %rax
	movq	%rdx, -704(%rbp)        ## 8-byte Spill
	cqto
	idivq	%rdi
	movq	%rax, -600(%rbp)
	movq	$4, -608(%rbp)
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	%rcx, -280(%rbp)
	movq	-272(%rbp), %rcx
	movq	-280(%rbp), %rdi
	movq	%rcx, -256(%rbp)
	movq	%rdi, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rdi
	movq	-696(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdi, -240(%rbp)
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-240(%rbp), %rdi
	cmpq	(%rdi), %rcx
	jae	LBB25_8
## BB#7:
	movq	-264(%rbp), %rax
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	jmp	LBB25_9
LBB25_8:
	movq	-256(%rbp), %rax
	movq	%rax, -712(%rbp)        ## 8-byte Spill
LBB25_9:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -688(%rbp)        ## 8-byte Spill
LBB25_10:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -592(%rbp)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -612(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	cmpq	32(%rdx), %rcx
	jne	LBB25_32
## BB#11:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	16(%rdx), %rdi
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	32(%rdx), %rdx
	subq	-592(%rbp), %rcx
	addq	%rcx, %rdx
	movq	-592(%rbp), %rcx
	shlq	$0, %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	_memmove
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	testb	$1, 402(%rcx)
	je	LBB25_15
## BB#12:
	movl	$1, %eax
	movl	%eax, %esi
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	32(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	16(%rdx), %rdx
	subq	%rdx, %rcx
	subq	-592(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	16(%rcx), %rcx
	addq	-592(%rbp), %rcx
	movq	-624(%rbp), %rdx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	120(%rdi), %r8
	movq	%rcx, %rdi
	movq	%r8, %rcx
	callq	_fread
	movq	%rax, -624(%rbp)
	cmpq	$0, -624(%rbp)
	je	LBB25_14
## BB#13:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-592(%rbp), %rdx
	movq	-680(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	16(%rsi), %rsi
	addq	-592(%rbp), %rsi
	addq	-624(%rbp), %rsi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB25_14:
	jmp	LBB25_31
LBB25_15:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rdi
	movq	72(%rax), %rsi
	movq	80(%rax), %rcx
	movq	72(%rax), %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	callq	_memmove
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	80(%rax), %rdx
	movq	72(%rax), %rsi
	subq	%rsi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 72(%rax)
	movq	64(%rax), %rcx
	movq	64(%rax), %rdx
	addq	$88, %rax
	cmpq	%rax, %rdx
	movq	%rcx, -720(%rbp)        ## 8-byte Spill
	jne	LBB25_17
## BB#16:
	movl	$8, %eax
	movl	%eax, %ecx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB25_18
LBB25_17:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB25_18:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	leaq	-160(%rbp), %rcx
	leaq	-648(%rbp), %rdx
	leaq	-640(%rbp), %rsi
	movq	-720(%rbp), %rdi        ## 8-byte Reload
	addq	%rax, %rdi
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rdi, 80(%rax)
	movq	112(%rax), %rdi
	subq	-592(%rbp), %rdi
	movq	%rdi, -640(%rbp)
	movq	80(%rax), %rdi
	movq	72(%rax), %r8
	subq	%r8, %rdi
	movq	%rdi, -648(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-152(%rbp), %rdx
	cmpq	(%rdx), %rcx
	jae	LBB25_20
## BB#19:
	movq	-176(%rbp), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB25_21
LBB25_20:
	movq	-168(%rbp), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
LBB25_21:                               ## %_ZNSt3__13minImEERKT_S3_S3_.exit3
	movq	-736(%rbp), %rax        ## 8-byte Reload
	movl	$1, %ecx
	movl	%ecx, %esi
	movl	$128, %ecx
	movl	%ecx, %edx
	movq	(%rax), %rax
	movq	%rax, -632(%rbp)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	addq	$264, %rax              ## imm = 0x108
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	addq	$136, %rdi
	movq	%rdi, -744(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -752(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	callq	_memcpy
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	72(%rax), %rdi
	movq	-632(%rbp), %rdx
	movq	120(%rax), %rcx
	movq	-752(%rbp), %rsi        ## 8-byte Reload
	callq	_fread
	movq	%rax, -664(%rbp)
	cmpq	$0, -664(%rbp)
	je	LBB25_30
## BB#22:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB25_24
## BB#23:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB25_24:
	leaq	-672(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	72(%rcx), %rdx
	addq	-664(%rbp), %rdx
	movq	%rdx, 80(%rcx)
	movq	128(%rcx), %rdx
	addq	$136, %rcx
	movq	-680(%rbp), %rsi        ## 8-byte Reload
	movq	64(%rsi), %rdi
	movq	80(%rsi), %r8
	addq	$72, %rsi
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	16(%r9), %r9
	addq	-592(%rbp), %r9
	movq	-680(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	16(%r10), %r10
	movq	-680(%rbp), %r11        ## 8-byte Reload
	addq	112(%r11), %r10
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%rdi, -312(%rbp)
	movq	%r8, -320(%rbp)
	movq	%rsi, -328(%rbp)
	movq	%r9, -336(%rbp)
	movq	%r10, -344(%rbp)
	movq	%rax, -352(%rbp)
	movq	-296(%rbp), %rax
	movq	(%rax), %rcx
	movq	32(%rcx), %rcx
	movq	-304(%rbp), %rsi
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rdi
	movq	-328(%rbp), %r8
	movq	-336(%rbp), %r9
	movq	-344(%rbp), %r10
	movq	-352(%rbp), %rbx
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-776(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -784(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	%r10, (%rsp)
	movq	%rbx, 8(%rsp)
	movq	-784(%rbp), %r10        ## 8-byte Reload
	callq	*%r10
	movl	%eax, -652(%rbp)
	cmpl	$3, -652(%rbp)
	jne	LBB25_26
## BB#25:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	80(%rcx), %rdi
	movq	%rax, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rdi, -384(%rbp)
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-376(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-384(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
	jmp	LBB25_29
LBB25_26:
	movq	-672(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	16(%rcx), %rcx
	addq	-592(%rbp), %rcx
	cmpq	%rcx, %rax
	je	LBB25_28
## BB#27:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -424(%rbp)
	movq	-424(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-592(%rbp), %rdx
	movq	-672(%rbp), %rsi
	movq	%rax, -432(%rbp)
	movq	%rcx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	movq	%rsi, -456(%rbp)
	movq	-432(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-448(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-456(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB25_28:
	jmp	LBB25_29
LBB25_29:
	jmp	LBB25_30
LBB25_30:
	jmp	LBB25_31
LBB25_31:
	jmp	LBB25_33
LBB25_32:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB25_33:
	leaq	-578(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	cmpq	%rax, 16(%rcx)
	jne	LBB25_35
## BB#34:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB25_35:
	movl	-612(%rbp), %eax
	movl	%eax, -564(%rbp)
LBB25_36:
	movl	-564(%rbp), %eax
	addq	$792, %rsp              ## imm = 0x318
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp180:
	.cfi_def_cfa_offset 16
Ltmp181:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp182:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -80(%rbp)
	movl	%esi, -84(%rbp)
	movq	-80(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -96(%rbp)         ## 8-byte Spill
	je	LBB26_8
## BB#1:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	16(%rax), %rax
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB26_8
## BB#2:
	movl	-84(%rbp), %edi
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-100(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB26_3
	jmp	LBB26_4
LBB26_3:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movl	$-1, -36(%rbp)
	movq	-32(%rbp), %rax
	movl	-36(%rbp), %ecx
	movq	24(%rax), %rdx
	movslq	%ecx, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 24(%rax)
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -68(%rbp)
	jmp	LBB26_9
LBB26_4:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movl	392(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB26_6
## BB#5:
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB26_6
	jmp	LBB26_7
LBB26_6:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movl	$-1, -20(%rbp)
	movq	-16(%rbp), %rax
	movl	-20(%rbp), %ecx
	movq	24(%rax), %rdx
	movslq	%ecx, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 24(%rax)
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	24(%rdx), %rdx
	movb	%al, (%rdx)
	movl	-84(%rbp), %ecx
	movl	%ecx, -68(%rbp)
	jmp	LBB26_9
LBB26_7:
	jmp	LBB26_8
LBB26_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -68(%rbp)
LBB26_9:
	movl	-68(%rbp), %eax
	addq	$112, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp183:
	.cfi_def_cfa_offset 16
Ltmp184:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp185:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -328(%rbp)
	movl	%esi, -332(%rbp)
	movq	-328(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -408(%rbp)        ## 8-byte Spill
	jne	LBB27_2
## BB#1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_2:
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	40(%rdi), %rdi
	movq	%rdi, -344(%rbp)
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -304(%rbp)
	movq	-304(%rbp), %rdi
	movq	56(%rdi), %rdi
	movq	%rdi, -352(%rbp)
	movl	-332(%rbp), %edi
	movl	%edi, -412(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-412(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB27_6
## BB#3:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB27_5
## BB#4:
	leaq	-333(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, %rdx
	addq	$1, %rdx
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-200(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB27_5:
	movl	-332(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	48(%rcx), %rcx
	movb	%al, (%rcx)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -136(%rbp)
	movl	$1, -140(%rbp)
	movq	-136(%rbp), %rcx
	movl	-140(%rbp), %edi
	movq	48(%rcx), %rdx
	movslq	%edi, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 48(%rcx)
LBB27_6:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	48(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	cmpq	40(%rcx), %rax
	je	LBB27_33
## BB#7:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB27_11
## BB#8:
	movl	$1, %eax
	movl	%eax, %esi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	48(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movq	%rcx, -360(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	-360(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-360(%rbp), %rax
	je	LBB27_10
## BB#9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_10:
	jmp	LBB27_32
LBB27_11:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	%rcx, -368(%rbp)
LBB27_12:                               ## =>This Inner Loop Header: Depth=1
	movq	-408(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB27_14
## BB#13:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-424(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB27_14:                               ##   in Loop: Header=BB27_12 Depth=1
	leaq	-368(%rbp), %rax
	leaq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	128(%rdx), %rsi
	addq	$136, %rdx
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	40(%rdi), %rdi
	movq	-408(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -48(%rbp)
	movq	-48(%rbp), %r8
	movq	48(%r8), %r8
	movq	-408(%rbp), %r9         ## 8-byte Reload
	movq	64(%r9), %r10
	movq	64(%r9), %r11
	addq	96(%r9), %r11
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -80(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r11, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	%rdi, -440(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	%r10, (%rsp)
	movq	%r11, 8(%rsp)
	movq	-448(%rbp), %r10        ## 8-byte Reload
	callq	*%r10
	movl	%eax, -372(%rbp)
	movq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	cmpq	40(%rdx), %rcx
	jne	LBB27_16
## BB#15:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_16:                               ##   in Loop: Header=BB27_12 Depth=1
	cmpl	$3, -372(%rbp)
	jne	LBB27_20
## BB#17:                               ##   in Loop: Header=BB27_12 Depth=1
	movl	$1, %eax
	movl	%eax, %esi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	48(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movq	%rcx, -392(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	-392(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-392(%rbp), %rax
	je	LBB27_19
## BB#18:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_19:                               ##   in Loop: Header=BB27_12 Depth=1
	jmp	LBB27_29
LBB27_20:                               ##   in Loop: Header=BB27_12 Depth=1
	cmpl	$0, -372(%rbp)
	je	LBB27_22
## BB#21:                               ##   in Loop: Header=BB27_12 Depth=1
	cmpl	$1, -372(%rbp)
	jne	LBB27_27
LBB27_22:                               ##   in Loop: Header=BB27_12 Depth=1
	movl	$1, %eax
	movl	%eax, %esi
	movq	-368(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	64(%rdx), %rdi
	subq	%rdi, %rcx
	movq	%rcx, -400(%rbp)
	movq	64(%rdx), %rdi
	movq	-400(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-400(%rbp), %rax
	je	LBB27_24
## BB#23:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_24:                               ##   in Loop: Header=BB27_12 Depth=1
	cmpl	$1, -372(%rbp)
	jne	LBB27_26
## BB#25:                               ##   in Loop: Header=BB27_12 Depth=1
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	48(%rdx), %rdx
	movq	%rax, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-232(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	56(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -248(%rbp)
	movq	-248(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movl	%ecx, %esi
	movq	%rax, -256(%rbp)
	movl	%esi, -260(%rbp)
	movq	-256(%rbp), %rax
	movl	-260(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB27_26:                               ##   in Loop: Header=BB27_12 Depth=1
	jmp	LBB27_28
LBB27_27:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB27_34
LBB27_28:                               ##   in Loop: Header=BB27_12 Depth=1
	jmp	LBB27_29
LBB27_29:                               ##   in Loop: Header=BB27_12 Depth=1
	jmp	LBB27_30
LBB27_30:                               ##   in Loop: Header=BB27_12 Depth=1
	cmpl	$1, -372(%rbp)
	je	LBB27_12
## BB#31:
	jmp	LBB27_32
LBB27_32:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-344(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	%rax, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-296(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB27_33:
	movl	-332(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -316(%rbp)
LBB27_34:
	movl	-316(%rbp), %eax
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp186:
	.cfi_def_cfa_offset 16
Ltmp187:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp188:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp189:
	.cfi_def_cfa_offset 16
Ltmp190:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp191:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movl	396(%rdi), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	movq	%rdi, -112(%rbp)        ## 8-byte Spill
	jne	LBB29_5
## BB#1:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-88(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB29_3
## BB#2:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	addq	96(%rcx), %rsi
	movq	64(%rcx), %rdi
	addq	96(%rcx), %rdi
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-24(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-32(%rbp), %rdx
	movq	%rdx, 32(%rax)
	jmp	LBB29_4
LBB29_3:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	104(%rcx), %rdx
	movq	104(%rcx), %rsi
	addq	112(%rcx), %rsi
	movq	104(%rcx), %rdi
	addq	112(%rcx), %rdi
	movq	%rax, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rdi, -64(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB29_4:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movl	$8, 396(%rax)
	movb	$1, -89(%rbp)
	jmp	LBB29_6
LBB29_5:
	movb	$0, -89(%rbp)
LBB29_6:
	movb	-89(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp192:
	.cfi_def_cfa_offset 16
Ltmp193:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp194:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp195:
	.cfi_def_cfa_offset 16
Ltmp196:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp197:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp198:
	.cfi_def_cfa_offset 16
Ltmp199:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp200:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB32_1
	jmp	LBB32_2
LBB32_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB32_3
LBB32_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB32_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp201:
	.cfi_def_cfa_offset 16
Ltmp202:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp203:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp204:
	.cfi_def_cfa_offset 16
Ltmp205:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp206:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp207:
	.cfi_def_cfa_offset 16
Ltmp208:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp209:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movl	396(%rdi), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jne	LBB35_8
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-96(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-120(%rbp), %rax        ## 8-byte Reload
	cmpq	$8, 96(%rax)
	jbe	LBB35_6
## BB#2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB35_4
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	96(%rcx), %rdi
	subq	$1, %rdi
	addq	%rdi, %rsi
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	%rdx, 40(%rax)
	movq	-24(%rbp), %rdx
	movq	%rdx, 56(%rax)
	jmp	LBB35_5
LBB35_4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	104(%rcx), %rdx
	movq	104(%rcx), %rsi
	movq	112(%rcx), %rdi
	subq	$1, %rdi
	addq	%rdi, %rsi
	movq	%rax, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	%rdx, 40(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 56(%rax)
LBB35_5:
	jmp	LBB35_7
LBB35_6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-72(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB35_7:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	$16, 396(%rax)
LBB35_8:
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init.2:               ## @__cxx_global_var_init.2
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp210:
	.cfi_def_cfa_offset 16
Ltmp211:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp212:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp234:
	.cfi_def_cfa_offset 16
Ltmp235:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp236:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp213:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp214:
	jmp	LBB37_1
LBB37_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB37_3
	jmp	LBB37_26
LBB37_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB37_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB37_7
LBB37_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB37_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB37_8
	jmp	LBB37_13
LBB37_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp216:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp217:
	jmp	LBB37_9
LBB37_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp218:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp219:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB37_10
LBB37_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp220:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp221:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB37_12
LBB37_11:
Ltmp222:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB37_21
LBB37_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB37_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp223:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp224:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB37_15
LBB37_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB37_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp225:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp226:
	jmp	LBB37_17
LBB37_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB37_18
LBB37_18:
	jmp	LBB37_25
LBB37_19:
Ltmp215:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB37_22
LBB37_20:
Ltmp227:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB37_21
LBB37_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB37_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp228:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp229:
	jmp	LBB37_23
LBB37_23:
	callq	___cxa_end_catch
LBB37_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB37_25:
	jmp	LBB37_26
LBB37_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB37_24
LBB37_27:
Ltmp230:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp231:
	callq	___cxa_end_catch
Ltmp232:
	jmp	LBB37_28
LBB37_28:
	jmp	LBB37_29
LBB37_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB37_30:
Ltmp233:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table37:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset115 = Ltmp213-Lfunc_begin6          ## >> Call Site 1 <<
	.long	Lset115
Lset116 = Ltmp214-Ltmp213               ##   Call between Ltmp213 and Ltmp214
	.long	Lset116
Lset117 = Ltmp215-Lfunc_begin6          ##     jumps to Ltmp215
	.long	Lset117
	.byte	5                       ##   On action: 3
Lset118 = Ltmp216-Lfunc_begin6          ## >> Call Site 2 <<
	.long	Lset118
Lset119 = Ltmp217-Ltmp216               ##   Call between Ltmp216 and Ltmp217
	.long	Lset119
Lset120 = Ltmp227-Lfunc_begin6          ##     jumps to Ltmp227
	.long	Lset120
	.byte	5                       ##   On action: 3
Lset121 = Ltmp218-Lfunc_begin6          ## >> Call Site 3 <<
	.long	Lset121
Lset122 = Ltmp221-Ltmp218               ##   Call between Ltmp218 and Ltmp221
	.long	Lset122
Lset123 = Ltmp222-Lfunc_begin6          ##     jumps to Ltmp222
	.long	Lset123
	.byte	3                       ##   On action: 2
Lset124 = Ltmp223-Lfunc_begin6          ## >> Call Site 4 <<
	.long	Lset124
Lset125 = Ltmp226-Ltmp223               ##   Call between Ltmp223 and Ltmp226
	.long	Lset125
Lset126 = Ltmp227-Lfunc_begin6          ##     jumps to Ltmp227
	.long	Lset126
	.byte	5                       ##   On action: 3
Lset127 = Ltmp226-Lfunc_begin6          ## >> Call Site 5 <<
	.long	Lset127
Lset128 = Ltmp228-Ltmp226               ##   Call between Ltmp226 and Ltmp228
	.long	Lset128
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset129 = Ltmp228-Lfunc_begin6          ## >> Call Site 6 <<
	.long	Lset129
Lset130 = Ltmp229-Ltmp228               ##   Call between Ltmp228 and Ltmp229
	.long	Lset130
Lset131 = Ltmp230-Lfunc_begin6          ##     jumps to Ltmp230
	.long	Lset131
	.byte	0                       ##   On action: cleanup
Lset132 = Ltmp229-Lfunc_begin6          ## >> Call Site 7 <<
	.long	Lset132
Lset133 = Ltmp231-Ltmp229               ##   Call between Ltmp229 and Ltmp231
	.long	Lset133
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset134 = Ltmp231-Lfunc_begin6          ## >> Call Site 8 <<
	.long	Lset134
Lset135 = Ltmp232-Ltmp231               ##   Call between Ltmp231 and Ltmp232
	.long	Lset135
Lset136 = Ltmp233-Lfunc_begin6          ##     jumps to Ltmp233
	.long	Lset136
	.byte	5                       ##   On action: 3
Lset137 = Ltmp232-Lfunc_begin6          ## >> Call Site 9 <<
	.long	Lset137
Lset138 = Lfunc_end6-Ltmp232            ##   Call between Ltmp232 and Lfunc_end6
	.long	Lset138
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp237:
	.cfi_def_cfa_offset 16
Ltmp238:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp239:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp243:
	.cfi_def_cfa_offset 16
Ltmp244:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp245:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB39_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB39_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB39_5
LBB39_4:
	movq	$0, -368(%rbp)
LBB39_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB39_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB39_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_8:
	jmp	LBB39_9
LBB39_9:
	cmpq	$0, -368(%rbp)
	jle	LBB39_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB39_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB39_13
LBB39_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB39_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp240:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp241:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB39_14
LBB39_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB39_15
LBB39_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB39_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB39_19
LBB39_17:
Ltmp242:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB39_27
LBB39_18:
	movl	$0, -416(%rbp)
LBB39_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB39_20
	jmp	LBB39_29
LBB39_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB39_26
	jmp	LBB39_28
LBB39_20:
	jmp	LBB39_21
LBB39_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB39_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB39_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB39_26
LBB39_24:
	jmp	LBB39_25
LBB39_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB39_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB39_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB39_28:
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table39:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset139 = Lfunc_begin7-Lfunc_begin7     ## >> Call Site 1 <<
	.long	Lset139
Lset140 = Ltmp240-Lfunc_begin7          ##   Call between Lfunc_begin7 and Ltmp240
	.long	Lset140
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset141 = Ltmp240-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset141
Lset142 = Ltmp241-Ltmp240               ##   Call between Ltmp240 and Ltmp241
	.long	Lset142
Lset143 = Ltmp242-Lfunc_begin7          ##     jumps to Ltmp242
	.long	Lset143
	.byte	0                       ##   On action: cleanup
Lset144 = Ltmp241-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset144
Lset145 = Lfunc_end7-Ltmp241            ##   Call between Ltmp241 and Lfunc_end7
	.long	Lset145
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp246:
	.cfi_def_cfa_offset 16
Ltmp247:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp248:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp249:
	.cfi_def_cfa_offset 16
Ltmp250:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp251:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-8(%rbp), %rsi
	movq	$0, -32(%rbp)
	cmpq	$0, 120(%rsi)
	movq	%rsi, -48(%rbp)         ## 8-byte Spill
	jne	LBB41_25
## BB#1:
	movq	-48(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movl	-20(%rbp), %ecx
	andl	$-3, %ecx
	movl	%ecx, %edx
	subl	$1, %edx
	movl	%ecx, -52(%rbp)         ## 4-byte Spill
	movl	%edx, -56(%rbp)         ## 4-byte Spill
	je	LBB41_3
	jmp	LBB41_26
LBB41_26:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$5, %eax
	movl	%eax, -60(%rbp)         ## 4-byte Spill
	je	LBB41_9
	jmp	LBB41_27
LBB41_27:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$8, %eax
	movl	%eax, -64(%rbp)         ## 4-byte Spill
	je	LBB41_4
	jmp	LBB41_28
LBB41_28:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$9, %eax
	movl	%eax, -68(%rbp)         ## 4-byte Spill
	je	LBB41_7
	jmp	LBB41_29
LBB41_29:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$12, %eax
	movl	%eax, -72(%rbp)         ## 4-byte Spill
	je	LBB41_10
	jmp	LBB41_30
LBB41_30:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$13, %eax
	movl	%eax, -76(%rbp)         ## 4-byte Spill
	je	LBB41_13
	jmp	LBB41_31
LBB41_31:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$16, %eax
	movl	%eax, -80(%rbp)         ## 4-byte Spill
	je	LBB41_2
	jmp	LBB41_32
LBB41_32:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$17, %eax
	movl	%eax, -84(%rbp)         ## 4-byte Spill
	je	LBB41_3
	jmp	LBB41_33
LBB41_33:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$20, %eax
	movl	%eax, -88(%rbp)         ## 4-byte Spill
	je	LBB41_8
	jmp	LBB41_34
LBB41_34:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$21, %eax
	movl	%eax, -92(%rbp)         ## 4-byte Spill
	je	LBB41_9
	jmp	LBB41_35
LBB41_35:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$24, %eax
	movl	%eax, -96(%rbp)         ## 4-byte Spill
	je	LBB41_5
	jmp	LBB41_36
LBB41_36:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$25, %eax
	movl	%eax, -100(%rbp)        ## 4-byte Spill
	je	LBB41_7
	jmp	LBB41_37
LBB41_37:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$28, %eax
	movl	%eax, -104(%rbp)        ## 4-byte Spill
	je	LBB41_11
	jmp	LBB41_38
LBB41_38:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$29, %eax
	movl	%eax, -108(%rbp)        ## 4-byte Spill
	je	LBB41_13
	jmp	LBB41_39
LBB41_39:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$48, %eax
	movl	%eax, -112(%rbp)        ## 4-byte Spill
	je	LBB41_2
	jmp	LBB41_40
LBB41_40:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$52, %eax
	movl	%eax, -116(%rbp)        ## 4-byte Spill
	je	LBB41_8
	jmp	LBB41_41
LBB41_41:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$56, %eax
	movl	%eax, -120(%rbp)        ## 4-byte Spill
	je	LBB41_6
	jmp	LBB41_42
LBB41_42:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$60, %eax
	movl	%eax, -124(%rbp)        ## 4-byte Spill
	je	LBB41_12
	jmp	LBB41_14
LBB41_2:
	leaq	L_.str.3(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_3:
	leaq	L_.str.4(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_4:
	leaq	L_.str.5(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_5:
	leaq	L_.str.6(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_6:
	leaq	L_.str.7(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_7:
	leaq	L_.str.8(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_8:
	leaq	L_.str.9(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_9:
	leaq	L_.str.10(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_10:
	leaq	L_.str.11(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_11:
	leaq	L_.str.12(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_12:
	leaq	L_.str.13(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_13:
	leaq	L_.str.14(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB41_15
LBB41_14:
	movq	$0, -32(%rbp)
LBB41_15:
	cmpq	$0, -32(%rbp)
	je	LBB41_24
## BB#16:
	movq	-16(%rbp), %rdi
	movq	-40(%rbp), %rsi
	callq	_fopen
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, 120(%rsi)
	cmpq	$0, 120(%rsi)
	je	LBB41_22
## BB#17:
	movl	-20(%rbp), %eax
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	movl	%eax, 392(%rcx)
	movl	-20(%rbp), %eax
	andl	$2, %eax
	cmpl	$0, %eax
	je	LBB41_21
## BB#18:
	xorl	%eax, %eax
	movl	%eax, %esi
	movl	$2, %edx
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	movq	120(%rcx), %rdi
	callq	_fseek
	cmpl	$0, %eax
	je	LBB41_20
## BB#19:
	movq	-48(%rbp), %rax         ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_fclose
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	movq	$0, 120(%rdi)
	movq	$0, -32(%rbp)
	movl	%eax, -128(%rbp)        ## 4-byte Spill
LBB41_20:
	jmp	LBB41_21
LBB41_21:
	jmp	LBB41_23
LBB41_22:
	movq	$0, -32(%rbp)
LBB41_23:
	jmp	LBB41_24
LBB41_24:
	jmp	LBB41_25
LBB41_25:
	movq	-32(%rbp), %rax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp261:
	.cfi_def_cfa_offset 16
Ltmp262:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp263:
	.cfi_def_cfa_register %rbp
	subq	$160, %rsp
	movq	%rdi, -48(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -88(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
	leaq	-56(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$128, %edx
	movl	%edx, %eax
	movq	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	addq	$16, %rsi
	movq	-88(%rbp), %r8          ## 8-byte Reload
	movq	%rsi, (%r8)
	movq	$0, 64(%r8)
	movq	$0, 72(%r8)
	movq	$0, 80(%r8)
	movq	$0, 96(%r8)
	movq	$0, 104(%r8)
	movq	$0, 112(%r8)
	movq	$0, 120(%r8)
	movq	$0, 128(%r8)
	addq	$136, %r8
	movq	%rdi, -96(%rbp)         ## 8-byte Spill
	movq	%r8, %rdi
	movl	%ecx, %esi
	movq	%rax, %rdx
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	movl	%ecx, -108(%rbp)        ## 4-byte Spill
	callq	_memset
	movq	-88(%rbp), %rax         ## 8-byte Reload
	addq	$264, %rax              ## imm = 0x108
	movq	%rax, %rdi
	movl	-108(%rbp), %esi        ## 4-byte Reload
	movq	-104(%rbp), %rdx        ## 8-byte Reload
	callq	_memset
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movl	$0, 392(%rax)
	movl	$0, 396(%rax)
	movb	$0, 400(%rax)
	movb	$0, 401(%rax)
	movb	$0, 402(%rax)
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16localeC1ERKS0_
## BB#1:
	leaq	-56(%rbp), %rax
	movq	%rax, -32(%rbp)
Ltmp252:
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9has_facetERNS0_2idE
Ltmp253:
	movb	%al, -109(%rbp)         ## 1-byte Spill
	jmp	LBB42_3
LBB42_2:
Ltmp254:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -116(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB42_3:                                ## %_ZNSt3__19has_facetINS_7codecvtIcc11__mbstate_tEEEEbRKNS_6localeE.exit
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-109(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB42_4
	jmp	LBB42_10
LBB42_4:
	leaq	-80(%rbp), %rdi
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rsi
	callq	__ZNSt3__16localeC1ERKS0_
## BB#5:
	leaq	-80(%rbp), %rax
	movq	%rax, -16(%rbp)
Ltmp255:
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp256:
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	jmp	LBB42_6
LBB42_6:                                ## %_ZNSt3__19use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE.exit
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)        ## 8-byte Spill
## BB#7:
	leaq	-80(%rbp), %rdi
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	-136(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, 128(%rax)
	callq	__ZNSt3__16localeD1Ev
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, -144(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	-144(%rbp), %rcx        ## 8-byte Reload
	callq	*56(%rcx)
	andb	$1, %al
	movq	-88(%rbp), %rcx         ## 8-byte Reload
	movb	%al, 402(%rcx)
	jmp	LBB42_10
LBB42_8:
Ltmp260:
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	jmp	LBB42_12
LBB42_9:
Ltmp257:
	leaq	-80(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB42_12
LBB42_10:
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
Ltmp258:
	xorl	%edx, %edx
	movl	%edx, %esi
	movl	$4096, %edx             ## imm = 0x1000
                                        ## 
	movq	%rax, %rdi
	callq	*%rcx
Ltmp259:
	movq	%rax, -152(%rbp)        ## 8-byte Spill
	jmp	LBB42_11
LBB42_11:
	addq	$160, %rsp
	popq	%rbp
	retq
LBB42_12:
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
## BB#13:
	movq	-64(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table42:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset146 = Lfunc_begin8-Lfunc_begin8     ## >> Call Site 1 <<
	.long	Lset146
Lset147 = Ltmp252-Lfunc_begin8          ##   Call between Lfunc_begin8 and Ltmp252
	.long	Lset147
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset148 = Ltmp252-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset148
Lset149 = Ltmp253-Ltmp252               ##   Call between Ltmp252 and Ltmp253
	.long	Lset149
Lset150 = Ltmp254-Lfunc_begin8          ##     jumps to Ltmp254
	.long	Lset150
	.byte	1                       ##   On action: 1
Lset151 = Ltmp255-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset151
Lset152 = Ltmp256-Ltmp255               ##   Call between Ltmp255 and Ltmp256
	.long	Lset152
Lset153 = Ltmp257-Lfunc_begin8          ##     jumps to Ltmp257
	.long	Lset153
	.byte	0                       ##   On action: cleanup
Lset154 = Ltmp256-Lfunc_begin8          ## >> Call Site 4 <<
	.long	Lset154
Lset155 = Ltmp258-Ltmp256               ##   Call between Ltmp256 and Ltmp258
	.long	Lset155
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset156 = Ltmp258-Lfunc_begin8          ## >> Call Site 5 <<
	.long	Lset156
Lset157 = Ltmp259-Ltmp258               ##   Call between Ltmp258 and Ltmp259
	.long	Lset157
Lset158 = Ltmp260-Lfunc_begin8          ##     jumps to Ltmp260
	.long	Lset158
	.byte	0                       ##   On action: cleanup
Lset159 = Ltmp259-Lfunc_begin8          ## >> Call Site 6 <<
	.long	Lset159
Lset160 = Lfunc_end8-Ltmp259            ##   Call between Ltmp259 and Lfunc_end8
	.long	Lset160
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN19exception_swallowerC1Ev
	.weak_def_can_be_hidden	__ZN19exception_swallowerC1Ev
	.align	4, 0x90
__ZN19exception_swallowerC1Ev:          ## @_ZN19exception_swallowerC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp264:
	.cfi_def_cfa_offset 16
Ltmp265:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp266:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN19exception_swallowerC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZNK3$_0clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_: ## @"_ZNK3$_0clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_"
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp267:
	.cfi_def_cfa_offset 16
Ltmp268:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp269:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.align	4, 0x90
__ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE: ## @_ZN19exception_swallower9set_errorENSt3__112basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp273:
	.cfi_def_cfa_offset 16
Ltmp274:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp275:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	movq	%rdi, -416(%rbp)
	movq	-416(%rbp), %rdi
	movb	$0, (%rdi)
	addq	$8, %rdi
	movq	%rsi, -408(%rbp)
	movq	-408(%rbp), %rsi
	movq	%rdi, -384(%rbp)
	movq	%rsi, -392(%rbp)
	movq	-384(%rbp), %rsi
	movq	-392(%rbp), %rdi
	movq	%rsi, -368(%rbp)
	movq	%rdi, -376(%rbp)
	movq	-368(%rbp), %rsi
	movq	%rsi, -352(%rbp)
	movq	-352(%rbp), %rdi
	movq	%rdi, -344(%rbp)
	movq	%rdi, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rsi, -424(%rbp)        ## 8-byte Spill
	movq	%rdi, -432(%rbp)        ## 8-byte Spill
	je	LBB45_2
## BB#1:
	leaq	-353(%rbp), %rsi
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rdi
	movb	$0, -353(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -208(%rbp)
	movq	$0, -216(%rbp)
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %rsi
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rsi, 8(%rcx)
	jmp	LBB45_3
LBB45_2:
	leaq	-354(%rbp), %rsi
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rdi
	movb	$0, -354(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-432(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rsi
	shlq	$1, %rsi
	movb	%sil, %dl
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movb	%dl, (%rcx)
LBB45_3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i
	movq	-424(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)
Ltmp270:
	xorl	%ecx, %ecx
	movl	%ecx, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp271:
	jmp	LBB45_5
LBB45_4:
Ltmp272:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -436(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB45_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13shrink_to_fitEv.exit.i.i
	movq	-424(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rcx)
	movq	16(%rdx), %rdx
	movq	%rdx, 16(%rcx)
	movq	-376(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB45_6:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB45_8
## BB#7:                                ##   in Loop: Header=BB45_6 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB45_6
LBB45_8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table45:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset161 = Ltmp270-Lfunc_begin9          ## >> Call Site 1 <<
	.long	Lset161
Lset162 = Ltmp271-Ltmp270               ##   Call between Ltmp270 and Ltmp271
	.long	Lset162
Lset163 = Ltmp272-Lfunc_begin9          ##     jumps to Ltmp272
	.long	Lset163
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN19exception_swallowerC2Ev
	.weak_def_can_be_hidden	__ZN19exception_swallowerC2Ev
	.align	4, 0x90
__ZN19exception_swallowerC2Ev:          ## @_ZN19exception_swallowerC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp276:
	.cfi_def_cfa_offset 16
Ltmp277:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp278:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movb	$1, (%rdi)
	addq	$8, %rdi
	movq	%rdi, -88(%rbp)
	movq	-88(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, -48(%rbp)
	movq	%rdi, -104(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	callq	_memset
	movq	-104(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB46_1:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB46_3
## BB#2:                                ##   in Loop: Header=BB46_1 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB46_1
LBB46_3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit
	addq	$112, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6assignERcRKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6assignERcRKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6assignERcRKc: ## @_ZNSt3__111char_traitsIcE6assignERcRKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movb	(%rsi), %al
	movq	-8(%rbp), %rsi
	movb	%al, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.align	4, 0x90
__ZNK3$_1clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_: ## @"_ZNK3$_1clINSt3__114basic_ifstreamIcNS1_11char_traitsIcEEEEEEDaRT_"
Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception10
## BB#0:
	pushq	%rbp
Ltmp285:
	.cfi_def_cfa_offset 16
Ltmp286:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp287:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rsi
Ltmp282:
	leaq	L_.str.16(%rip), %rdi
	movq	%rdi, -40(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	-40(%rbp), %rax         ## 8-byte Reload
	movq	%rsi, -48(%rbp)         ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt13runtime_errorC1EPKc
Ltmp283:
	jmp	LBB48_1
LBB48_1:
	movq	__ZTISt13runtime_error@GOTPCREL(%rip), %rax
	movq	__ZNSt13runtime_errorD1Ev@GOTPCREL(%rip), %rcx
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
LBB48_2:
Ltmp284:
	movl	%edx, %ecx
	movq	%rax, -24(%rbp)
	movl	%ecx, -28(%rbp)
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	callq	___cxa_free_exception
## BB#3:
	movq	-24(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end10:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table48:
Lexception10:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset164 = Lfunc_begin10-Lfunc_begin10   ## >> Call Site 1 <<
	.long	Lset164
Lset165 = Ltmp282-Lfunc_begin10         ##   Call between Lfunc_begin10 and Ltmp282
	.long	Lset165
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset166 = Ltmp282-Lfunc_begin10         ## >> Call Site 2 <<
	.long	Lset166
Lset167 = Ltmp283-Ltmp282               ##   Call between Ltmp282 and Ltmp283
	.long	Lset167
Lset168 = Ltmp284-Lfunc_begin10         ##     jumps to Ltmp284
	.long	Lset168
	.byte	0                       ##   On action: cleanup
Lset169 = Ltmp283-Lfunc_begin10         ## >> Call Site 3 <<
	.long	Lset169
Lset170 = Lfunc_end10-Ltmp283           ##   Call between Ltmp283 and Lfunc_end10
	.long	Lset170
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
__GLOBAL__sub_I_file_pattern.cpp:       ## @_GLOBAL__sub_I_file_pattern.cpp
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp288:
	.cfi_def_cfa_offset 16
Ltmp289:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp290:
	.cfi_def_cfa_register %rbp
	callq	___cxx_global_var_init
	callq	___cxx_global_var_init.2
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"success"

L_.str.1:                               ## @.str.1
	.asciz	"failure: "

.zerofill __DATA,__bss,_null_op,1,0     ## @null_op
.zerofill __DATA,__bss,_error_op,1,0    ## @error_op
	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE ## @_ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.align	3
__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE:
	.quad	424
	.quad	0
	.quad	__ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev
	.quad	-424
	.quad	-424
	.quad	__ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__114basic_ifstreamIcNS_11char_traitsIcEEED0Ev

	.globl	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE ## @_ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTTNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE:
	.quad	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE+24
	.quad	__ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE+24
	.quad	__ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE+64
	.quad	__ZTVNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE+64

	.globl	__ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE ## @_ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE
	.align	4
__ZTCNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE:
	.quad	424
	.quad	0
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev
	.quad	-424
	.quad	-424
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE ## @_ZTSNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTSNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTSNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE:
	.asciz	"NSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE ## @_ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTINSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__114basic_ifstreamIcNS_11char_traitsIcEEEE
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	3
__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.quad	0
	.quad	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	4
__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.asciz	"NSt3__113basic_filebufIcNS_11char_traitsIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	4
__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE

	.section	__TEXT,__cstring,cstring_literals
L_.str.3:                               ## @.str.3
	.asciz	"w"

L_.str.4:                               ## @.str.4
	.asciz	"a"

L_.str.5:                               ## @.str.5
	.asciz	"r"

L_.str.6:                               ## @.str.6
	.asciz	"r+"

L_.str.7:                               ## @.str.7
	.asciz	"w+"

L_.str.8:                               ## @.str.8
	.asciz	"a+"

L_.str.9:                               ## @.str.9
	.asciz	"wb"

L_.str.10:                              ## @.str.10
	.asciz	"ab"

L_.str.11:                              ## @.str.11
	.asciz	"rb"

L_.str.12:                              ## @.str.12
	.asciz	"r+b"

L_.str.13:                              ## @.str.13
	.asciz	"w+b"

L_.str.14:                              ## @.str.14
	.asciz	"a+b"

L_.str.15:                              ## @.str.15
	.asciz	"stream didn't open"

L_.str.16:                              ## @.str.16
	.asciz	"error in stream"

	.section	__DATA,__mod_init_func,mod_init_funcs
	.align	3
	.quad	__GLOBAL__sub_I_file_pattern.cpp

.subsections_via_symbols
