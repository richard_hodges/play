	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp17:
	.cfi_def_cfa_offset 16
Ltmp18:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp19:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
Ltmp20:
	.cfi_offset %rbx, -48
Ltmp21:
	.cfi_offset %r12, -40
Ltmp22:
	.cfi_offset %r14, -32
Ltmp23:
	.cfi_offset %r15, -24
	movslq	%edi, %rax
	leaq	(%rsi,%rax,8), %rdx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IPPKcEET_NS_9enable_ifIXaasr21__is_forward_iteratorISD_EE5valuesr16is_constructibleIS6_NS_15iterator_traitsISD_E9referenceEEE5valueESD_E4typeE
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r12
	cmpq	%r12, %rax
	je	LBB0_5
## BB#1:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %r15
	leaq	L_.str(%rip), %r14
	.align	4, 0x90
LBB0_2:                                 ## %.lr.ph.split.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %edx
	leaq	1(%rax), %rbx
	movb	%dl, %cl
	andb	$1, %cl
	movq	16(%rax), %rsi
	cmoveq	%rbx, %rsi
	shrq	%rdx
	testb	%cl, %cl
	cmovneq	8(%rax), %rdx
Ltmp0:
	movq	%r15, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp1:
## BB#3:                                ## %.noexc9
                                        ##   in Loop: Header=BB0_2 Depth=1
Ltmp2:
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp3:
## BB#4:                                ## %.noexc10
                                        ##   in Loop: Header=BB0_2 Depth=1
	addq	$23, %rbx
	cmpq	%r12, %rbx
	movq	%rbx, %rax
	jne	LBB0_2
LBB0_5:                                 ## %_ZNSt3__14copyINS_11__wrap_iterIPNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEENS_16ostream_iteratorIS7_cS4_EEEET0_T_SD_SC_.exit
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rsi
	movq	(%rsi), %rax
	addq	-24(%rax), %rsi
Ltmp5:
	leaq	-40(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp6:
## BB#6:                                ## %.noexc15
Ltmp7:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-40(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp8:
## BB#7:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp9:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %bl
Ltmp10:
## BB#8:                                ## %.noexc12
	leaq	-40(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp12:
	movsbl	%bl, %esi
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp13:
## BB#9:                                ## %.noexc13
Ltmp14:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp15:
## BB#10:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	movq	-64(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB0_15
## BB#11:
	movq	-56(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB0_14
	.align	4, 0x90
LBB0_12:                                ## %.lr.ph.i.i.i.i.i.3
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, -56(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-56(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB0_12
## BB#13:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i.6
	movq	-64(%rbp), %rbx
LBB0_14:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i.7
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB0_15:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit8
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_16:                                ## %.loopexit
Ltmp4:
LBB0_18:                                ## %.body
	movq	%rax, %r14
LBB0_19:                                ## %.body
	movq	-64(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB0_24
## BB#20:
	movq	-56(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	LBB0_23
	.align	4, 0x90
LBB0_21:                                ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, -56(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-56(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	LBB0_21
## BB#22:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i.i.i
	movq	-64(%rbp), %rbx
LBB0_23:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB0_24:                                ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB0_17:                                ## %.loopexit.split-lp
Ltmp16:
	jmp	LBB0_18
LBB0_25:
Ltmp11:
	movq	%rax, %r14
	leaq	-40(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB0_19
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp3-Ltmp0                     ##   Call between Ltmp0 and Ltmp3
	.long	Lset3
Lset4 = Ltmp4-Lfunc_begin0              ##     jumps to Ltmp4
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp5-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp6-Ltmp5                     ##   Call between Ltmp5 and Ltmp6
	.long	Lset6
Lset7 = Ltmp16-Lfunc_begin0             ##     jumps to Ltmp16
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp7-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp10-Ltmp7                    ##   Call between Ltmp7 and Ltmp10
	.long	Lset9
Lset10 = Ltmp11-Lfunc_begin0            ##     jumps to Ltmp11
	.long	Lset10
	.byte	0                       ##   On action: cleanup
Lset11 = Ltmp12-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Ltmp15-Ltmp12                  ##   Call between Ltmp12 and Ltmp15
	.long	Lset12
Lset13 = Ltmp16-Lfunc_begin0            ##     jumps to Ltmp16
	.long	Lset13
	.byte	0                       ##   On action: cleanup
Lset14 = Ltmp15-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset14
Lset15 = Lfunc_end0-Ltmp15              ##   Call between Ltmp15 and Lfunc_end0
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.globl	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IPPKcEET_NS_9enable_ifIXaasr21__is_forward_iteratorISD_EE5valuesr16is_constructibleIS6_NS_15iterator_traitsISD_E9referenceEEE5valueESD_E4typeE
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IPPKcEET_NS_9enable_ifIXaasr21__is_forward_iteratorISD_EE5valuesr16is_constructibleIS6_NS_15iterator_traitsISD_E9referenceEEE5valueESD_E4typeE
	.align	4, 0x90
__ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IPPKcEET_NS_9enable_ifIXaasr21__is_forward_iteratorISD_EE5valuesr16is_constructibleIS6_NS_15iterator_traitsISD_E9referenceEEE5valueESD_E4typeE: ## @_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC2IPPKcEET_NS_9enable_ifIXaasr21__is_forward_iteratorISD_EE5valuesr16is_constructibleIS6_NS_15iterator_traitsISD_E9referenceEEE5valueESD_E4typeE
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp32:
	.cfi_def_cfa_offset 16
Ltmp33:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp34:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp35:
	.cfi_offset %rbx, -56
Ltmp36:
	.cfi_offset %r12, -48
Ltmp37:
	.cfi_offset %r13, -40
Ltmp38:
	.cfi_offset %r14, -32
Ltmp39:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	$0, 16(%r13)
	movq	$0, 8(%r13)
	movq	$0, (%r13)
	movq	%r14, %rbx
	subq	%r15, %rbx
	sarq	$3, %rbx
	je	LBB2_7
## BB#1:
	movabsq	$768614336404564651, %rax ## imm = 0xAAAAAAAAAAAAAAB
	cmpq	%rax, %rbx
	jb	LBB2_3
## BB#2:
Ltmp24:
	movq	%r13, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
Ltmp25:
LBB2_3:                                 ## %.noexc
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %rdi
Ltmp26:
	callq	__Znwm
	movq	%rax, %r12
Ltmp27:
## BB#4:
	movq	%r12, 8(%r13)
	movq	%r12, (%r13)
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r13)
	cmpq	%r14, %r15
	je	LBB2_7
	.align	4, 0x90
LBB2_5:                                 ## %.lr.ph.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movq	(%r15), %rbx
	movq	$0, 16(%r12)
	movq	$0, 8(%r12)
	movq	$0, (%r12)
	movq	%rbx, %rdi
	callq	_strlen
Ltmp29:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp30:
## BB#6:                                ## %.noexc2
                                        ##   in Loop: Header=BB2_5 Depth=1
	addq	$8, %r15
	movq	8(%r13), %r12
	addq	$24, %r12
	movq	%r12, 8(%r13)
	cmpq	%r15, %r14
	jne	LBB2_5
LBB2_7:                                 ## %_ZNSt3__16vectorINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE18__construct_at_endIPPKcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeESE_SE_m.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB2_8:                                 ## %.loopexit
Ltmp31:
LBB2_10:
	movq	%rax, %r14
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	LBB2_15
## BB#11:
	movq	8(%r13), %rdi
	cmpq	%rbx, %rdi
	je	LBB2_14
	.align	4, 0x90
LBB2_12:                                ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	addq	$-24, %rdi
	movq	%rdi, 8(%r13)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	8(%r13), %rdi
	cmpq	%rbx, %rdi
	jne	LBB2_12
## BB#13:                               ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.loopexit.i
	movq	(%r13), %rbx
LBB2_14:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE5clearEv.exit.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB2_15:                                ## %_ZNSt3__113__vector_baseINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB2_9:                                 ## %.loopexit.split-lp
Ltmp28:
	jmp	LBB2_10
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset16 = Ltmp24-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp27-Ltmp24                  ##   Call between Ltmp24 and Ltmp27
	.long	Lset17
Lset18 = Ltmp28-Lfunc_begin1            ##     jumps to Ltmp28
	.long	Lset18
	.byte	0                       ##   On action: cleanup
Lset19 = Ltmp29-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset19
Lset20 = Ltmp30-Ltmp29                  ##   Call between Ltmp29 and Ltmp30
	.long	Lset20
Lset21 = Ltmp31-Lfunc_begin1            ##     jumps to Ltmp31
	.long	Lset21
	.byte	0                       ##   On action: cleanup
Lset22 = Ltmp30-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset22
Lset23 = Lfunc_end1-Ltmp30              ##   Call between Ltmp30 and Lfunc_end1
	.long	Lset23
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp64:
	.cfi_offset %rbx, -56
Ltmp65:
	.cfi_offset %r12, -48
Ltmp66:
	.cfi_offset %r13, -40
Ltmp67:
	.cfi_offset %r14, -32
Ltmp68:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp40:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp41:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB3_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB3_7
## BB#3:
Ltmp43:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp44:
## BB#4:                                ## %.noexc
Ltmp45:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp46:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp47:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp48:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB3_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp50:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp51:
## BB#8:
	testq	%rax, %rax
	jne	LBB3_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp52:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp53:
LBB3_10:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB3_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB3_11:
Ltmp54:
	movq	%rax, %r14
	jmp	LBB3_12
LBB3_20:
Ltmp42:
	movq	%rax, %r14
	jmp	LBB3_13
LBB3_19:
Ltmp49:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB3_12:                                ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB3_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp55:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp56:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB3_15
LBB3_16:
Ltmp57:
	movq	%rax, %rbx
Ltmp58:
	callq	___cxa_end_catch
Ltmp59:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB3_18:
Ltmp60:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset24 = Ltmp40-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset24
Lset25 = Ltmp41-Ltmp40                  ##   Call between Ltmp40 and Ltmp41
	.long	Lset25
Lset26 = Ltmp42-Lfunc_begin2            ##     jumps to Ltmp42
	.long	Lset26
	.byte	1                       ##   On action: 1
Lset27 = Ltmp43-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset27
Lset28 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset28
Lset29 = Ltmp54-Lfunc_begin2            ##     jumps to Ltmp54
	.long	Lset29
	.byte	1                       ##   On action: 1
Lset30 = Ltmp45-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset30
Lset31 = Ltmp48-Ltmp45                  ##   Call between Ltmp45 and Ltmp48
	.long	Lset31
Lset32 = Ltmp49-Lfunc_begin2            ##     jumps to Ltmp49
	.long	Lset32
	.byte	1                       ##   On action: 1
Lset33 = Ltmp50-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset33
Lset34 = Ltmp53-Ltmp50                  ##   Call between Ltmp50 and Ltmp53
	.long	Lset34
Lset35 = Ltmp54-Lfunc_begin2            ##     jumps to Ltmp54
	.long	Lset35
	.byte	1                       ##   On action: 1
Lset36 = Ltmp53-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset36
Lset37 = Ltmp55-Ltmp53                  ##   Call between Ltmp53 and Ltmp55
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset38 = Ltmp55-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset38
Lset39 = Ltmp56-Ltmp55                  ##   Call between Ltmp55 and Ltmp56
	.long	Lset39
Lset40 = Ltmp57-Lfunc_begin2            ##     jumps to Ltmp57
	.long	Lset40
	.byte	0                       ##   On action: cleanup
Lset41 = Ltmp56-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset41
Lset42 = Ltmp58-Ltmp56                  ##   Call between Ltmp56 and Ltmp58
	.long	Lset42
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp58-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset43
Lset44 = Ltmp59-Ltmp58                  ##   Call between Ltmp58 and Ltmp59
	.long	Lset44
Lset45 = Ltmp60-Lfunc_begin2            ##     jumps to Ltmp60
	.long	Lset45
	.byte	1                       ##   On action: 1
Lset46 = Ltmp59-Lfunc_begin2            ## >> Call Site 9 <<
	.long	Lset46
Lset47 = Lfunc_end2-Ltmp59              ##   Call between Ltmp59 and Lfunc_end2
	.long	Lset47
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp75:
	.cfi_offset %rbx, -56
Ltmp76:
	.cfi_offset %r12, -48
Ltmp77:
	.cfi_offset %r13, -40
Ltmp78:
	.cfi_offset %r14, -32
Ltmp79:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB4_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB4_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB4_9
LBB4_3:
	testq	%rbx, %rbx
	jle	LBB4_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp69:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp70:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB4_9
LBB4_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB4_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB4_9
LBB4_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB4_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB4_10:
Ltmp71:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset48 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset48
Lset49 = Ltmp69-Lfunc_begin3            ##   Call between Lfunc_begin3 and Ltmp69
	.long	Lset49
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset50 = Ltmp69-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset50
Lset51 = Ltmp70-Ltmp69                  ##   Call between Ltmp69 and Ltmp70
	.long	Lset51
Lset52 = Ltmp71-Lfunc_begin3            ##     jumps to Ltmp71
	.long	Lset52
	.byte	0                       ##   On action: cleanup
Lset53 = Ltmp70-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset53
Lset54 = Lfunc_end3-Ltmp70              ##   Call between Ltmp70 and Lfunc_end3
	.long	Lset54
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	", "


.subsections_via_symbols
