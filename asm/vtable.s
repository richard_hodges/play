	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	-8(%rbp), %rdi
	callq	__ZN6DriverI14SpecificDriverEC2Ev
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN6DriverI14SpecificDriverEC2Ev
	.weak_def_can_be_hidden	__ZN6DriverI14SpecificDriverEC2Ev
	.align	4, 0x90
__ZN6DriverI14SpecificDriverEC2Ev:      ## @_ZN6DriverI14SpecificDriverEC2Ev
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp32:
	.cfi_def_cfa_offset 16
Ltmp33:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp34:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$16, %rsp
Ltmp35:
	.cfi_offset %rbx, -32
Ltmp36:
	.cfi_offset %r14, -24
	movq	__ZTV6DriverI14SpecificDriverE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rdi)
	testq	%rdi, %rdi
	je	LBB1_1
LBB1_2:
Ltmp3:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	__ZTS6DriverI14SpecificDriverE@GOTPCREL(%rip), %rsi
	movl	$25, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp4:
## BB#3:                                ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp5:
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp6:
## BB#4:                                ## %.noexc13
Ltmp7:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp8:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp9:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp10:
## BB#6:                                ## %.noexc6
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp12:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp13:
## BB#7:                                ## %.noexc7
Ltmp14:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp15:
## BB#8:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit5
	movq	__ZTI14SpecificDriver@GOTPCREL(%rip), %rax
	movq	8(%rax), %rbx
	movq	%rbx, %rdi
	callq	_strlen
Ltmp16:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp17:
## BB#9:                                ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit11
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp18:
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp19:
## BB#10:                               ## %.noexc12
Ltmp20:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp21:
## BB#11:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp22:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp23:
## BB#12:                               ## %.noexc
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp25:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp26:
## BB#13:                               ## %.noexc1
Ltmp27:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp28:
## BB#14:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB1_17:
Ltmp11:
	movq	%rax, %rbx
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB1_18:
Ltmp24:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB1_1:
Ltmp29:
	callq	___cxa_bad_typeid
Ltmp30:
	jmp	LBB1_2
LBB1_15:
Ltmp31:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\352\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	104                     ## Call site table length
Lset0 = Ltmp3-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp6-Ltmp3                     ##   Call between Ltmp3 and Ltmp6
	.long	Lset1
Lset2 = Ltmp31-Lfunc_begin0             ##     jumps to Ltmp31
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp7-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp10-Ltmp7                    ##   Call between Ltmp7 and Ltmp10
	.long	Lset4
Lset5 = Ltmp11-Lfunc_begin0             ##     jumps to Ltmp11
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp12-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp19-Ltmp12                   ##   Call between Ltmp12 and Ltmp19
	.long	Lset7
Lset8 = Ltmp31-Lfunc_begin0             ##     jumps to Ltmp31
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp20-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp23-Ltmp20                  ##   Call between Ltmp20 and Ltmp23
	.long	Lset10
Lset11 = Ltmp24-Lfunc_begin0            ##     jumps to Ltmp24
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp25-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset12
Lset13 = Ltmp28-Ltmp25                  ##   Call between Ltmp25 and Ltmp28
	.long	Lset13
Lset14 = Ltmp31-Lfunc_begin0            ##     jumps to Ltmp31
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp28-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset15
Lset16 = Ltmp29-Ltmp28                  ##   Call between Ltmp28 and Ltmp29
	.long	Lset16
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset17 = Ltmp29-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset17
Lset18 = Ltmp30-Ltmp29                  ##   Call between Ltmp29 and Ltmp30
	.long	Lset18
Lset19 = Ltmp31-Lfunc_begin0            ##     jumps to Ltmp31
	.long	Lset19
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp30-Lfunc_begin0            ## >> Call Site 8 <<
	.long	Lset20
Lset21 = Lfunc_end0-Ltmp30              ##   Call between Ltmp30 and Lfunc_end0
	.long	Lset21
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN6DriverI14SpecificDriverED1Ev
	.weak_def_can_be_hidden	__ZN6DriverI14SpecificDriverED1Ev
	.align	4, 0x90
__ZN6DriverI14SpecificDriverED1Ev:      ## @_ZN6DriverI14SpecificDriverED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp37:
	.cfi_def_cfa_offset 16
Ltmp38:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp39:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6DriverI14SpecificDriverED0Ev
	.weak_def_can_be_hidden	__ZN6DriverI14SpecificDriverED0Ev
	.align	4, 0x90
__ZN6DriverI14SpecificDriverED0Ev:      ## @_ZN6DriverI14SpecificDriverED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp40:
	.cfi_def_cfa_offset 16
Ltmp41:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp42:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp64:
	.cfi_def_cfa_offset 16
Ltmp65:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp66:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp67:
	.cfi_offset %rbx, -56
Ltmp68:
	.cfi_offset %r12, -48
Ltmp69:
	.cfi_offset %r13, -40
Ltmp70:
	.cfi_offset %r14, -32
Ltmp71:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp43:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp44:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB4_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB4_7
## BB#3:
Ltmp46:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp47:
## BB#4:                                ## %.noexc
Ltmp48:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp49:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp50:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp51:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB4_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp53:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp54:
## BB#8:
	testq	%rax, %rax
	jne	LBB4_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp55:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp56:
LBB4_10:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB4_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB4_11:
Ltmp57:
	movq	%rax, %r14
	jmp	LBB4_12
LBB4_20:
Ltmp45:
	movq	%rax, %r14
	jmp	LBB4_13
LBB4_19:
Ltmp52:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB4_12:                                ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB4_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp58:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp59:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB4_15
LBB4_16:
Ltmp60:
	movq	%rax, %rbx
Ltmp61:
	callq	___cxa_end_catch
Ltmp62:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB4_18:
Ltmp63:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset22 = Ltmp43-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset22
Lset23 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset23
Lset24 = Ltmp45-Lfunc_begin1            ##     jumps to Ltmp45
	.long	Lset24
	.byte	1                       ##   On action: 1
Lset25 = Ltmp46-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset25
Lset26 = Ltmp47-Ltmp46                  ##   Call between Ltmp46 and Ltmp47
	.long	Lset26
Lset27 = Ltmp57-Lfunc_begin1            ##     jumps to Ltmp57
	.long	Lset27
	.byte	1                       ##   On action: 1
Lset28 = Ltmp48-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset28
Lset29 = Ltmp51-Ltmp48                  ##   Call between Ltmp48 and Ltmp51
	.long	Lset29
Lset30 = Ltmp52-Lfunc_begin1            ##     jumps to Ltmp52
	.long	Lset30
	.byte	1                       ##   On action: 1
Lset31 = Ltmp53-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset31
Lset32 = Ltmp56-Ltmp53                  ##   Call between Ltmp53 and Ltmp56
	.long	Lset32
Lset33 = Ltmp57-Lfunc_begin1            ##     jumps to Ltmp57
	.long	Lset33
	.byte	1                       ##   On action: 1
Lset34 = Ltmp56-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset34
Lset35 = Ltmp58-Ltmp56                  ##   Call between Ltmp56 and Ltmp58
	.long	Lset35
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp58-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset36
Lset37 = Ltmp59-Ltmp58                  ##   Call between Ltmp58 and Ltmp59
	.long	Lset37
Lset38 = Ltmp60-Lfunc_begin1            ##     jumps to Ltmp60
	.long	Lset38
	.byte	0                       ##   On action: cleanup
Lset39 = Ltmp59-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset39
Lset40 = Ltmp61-Ltmp59                  ##   Call between Ltmp59 and Ltmp61
	.long	Lset40
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset41 = Ltmp61-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset41
Lset42 = Ltmp62-Ltmp61                  ##   Call between Ltmp61 and Ltmp62
	.long	Lset42
Lset43 = Ltmp63-Lfunc_begin1            ##     jumps to Ltmp63
	.long	Lset43
	.byte	1                       ##   On action: 1
Lset44 = Ltmp62-Lfunc_begin1            ## >> Call Site 9 <<
	.long	Lset44
Lset45 = Lfunc_end1-Ltmp62              ##   Call between Ltmp62 and Lfunc_end1
	.long	Lset45
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp75:
	.cfi_def_cfa_offset 16
Ltmp76:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp77:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp78:
	.cfi_offset %rbx, -56
Ltmp79:
	.cfi_offset %r12, -48
Ltmp80:
	.cfi_offset %r13, -40
Ltmp81:
	.cfi_offset %r14, -32
Ltmp82:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB5_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB5_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB5_9
LBB5_3:
	testq	%rbx, %rbx
	jle	LBB5_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp72:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp73:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB5_9
LBB5_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB5_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB5_9
LBB5_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB5_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB5_10:
Ltmp74:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset46 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset46
Lset47 = Ltmp72-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp72
	.long	Lset47
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp72-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset48
Lset49 = Ltmp73-Ltmp72                  ##   Call between Ltmp72 and Ltmp73
	.long	Lset49
Lset50 = Ltmp74-Lfunc_begin2            ##     jumps to Ltmp74
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp73-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset51
Lset52 = Lfunc_end2-Ltmp73              ##   Call between Ltmp73 and Lfunc_end2
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTV6DriverI14SpecificDriverE ## @_ZTV6DriverI14SpecificDriverE
	.weak_def_can_be_hidden	__ZTV6DriverI14SpecificDriverE
	.align	3
__ZTV6DriverI14SpecificDriverE:
	.quad	0
	.quad	__ZTI6DriverI14SpecificDriverE
	.quad	___cxa_pure_virtual
	.quad	__ZN6DriverI14SpecificDriverED1Ev
	.quad	__ZN6DriverI14SpecificDriverED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS6DriverI14SpecificDriverE ## @_ZTS6DriverI14SpecificDriverE
	.weak_definition	__ZTS6DriverI14SpecificDriverE
	.align	4
__ZTS6DriverI14SpecificDriverE:
	.asciz	"6DriverI14SpecificDriverE"

	.globl	__ZTS7IDriver           ## @_ZTS7IDriver
	.weak_definition	__ZTS7IDriver
__ZTS7IDriver:
	.asciz	"7IDriver"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI7IDriver           ## @_ZTI7IDriver
	.weak_definition	__ZTI7IDriver
	.align	3
__ZTI7IDriver:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS7IDriver

	.globl	__ZTI6DriverI14SpecificDriverE ## @_ZTI6DriverI14SpecificDriverE
	.weak_definition	__ZTI6DriverI14SpecificDriverE
	.align	4
__ZTI6DriverI14SpecificDriverE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS6DriverI14SpecificDriverE
	.quad	__ZTI7IDriver


.subsections_via_symbols
