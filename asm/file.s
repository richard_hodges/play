	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$896, %rsp              ## imm = 0x380
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	movl	$0, -776(%rbp)
	leaq	-576(%rbp), %rax
	movq	%rax, -744(%rbp)
	leaq	L_.str(%rip), %rax
	movq	%rax, -752(%rbp)
	movl	$16, -756(%rbp)
	movq	-744(%rbp), %rax
	movq	%rax, %rcx
	addq	$416, %rcx              ## imm = 0x1A0
	movq	%rcx, -736(%rbp)
	movq	%rcx, -728(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 416(%rax)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 416(%rax)
	movq	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	%rdx, (%rax)
	addq	$64, %rcx
	movq	%rcx, 416(%rax)
	movq	%rax, %rcx
	addq	$8, %rcx
	movq	%rax, -704(%rbp)
	movq	__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rdx, -712(%rbp)
	movq	%rcx, -720(%rbp)
	movq	-704(%rbp), %rcx
	movq	-712(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rdx
	movq	-24(%rsi), %rsi
	movq	%rdx, (%rcx,%rsi)
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	-720(%rbp), %rdx
	movq	%rcx, -688(%rbp)
	movq	%rdx, -696(%rbp)
	movq	-688(%rbp), %rcx
Ltmp0:
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rcx, -808(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp1:
	jmp	LBB0_1
LBB0_1:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-808(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-808(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-800(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 416(%rdi)
	addq	$8, %rdi
Ltmp3:
	movq	%rdi, -816(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
Ltmp4:
	jmp	LBB0_2
LBB0_2:
	movq	-800(%rbp), %rax        ## 8-byte Reload
	addq	$8, %rax
	movq	-752(%rbp), %rsi
	movl	-756(%rbp), %ecx
	orl	$16, %ecx
Ltmp6:
	movq	%rax, %rdi
	movl	%ecx, %edx
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
Ltmp7:
	movq	%rax, -824(%rbp)        ## 8-byte Spill
	jmp	LBB0_3
LBB0_3:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, %rax
	jne	LBB0_11
## BB#4:
	movq	-800(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -672(%rbp)
	movl	$4, -676(%rbp)
	movq	-672(%rbp), %rax
	movq	%rax, -656(%rbp)
	movl	$4, -660(%rbp)
	movq	-656(%rbp), %rax
	movl	32(%rax), %edx
	orl	$4, %edx
Ltmp8:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp9:
	jmp	LBB0_5
LBB0_5:                                 ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit.i
	jmp	LBB0_11
LBB0_6:
Ltmp2:
	movl	%edx, %ecx
	movq	%rax, -768(%rbp)
	movl	%ecx, -772(%rbp)
	jmp	LBB0_10
LBB0_7:
Ltmp5:
	movl	%edx, %ecx
	movq	%rax, -768(%rbp)
	movl	%ecx, -772(%rbp)
	jmp	LBB0_9
LBB0_8:
Ltmp10:
	movl	%edx, %ecx
	movq	%rax, -768(%rbp)
	movl	%ecx, -772(%rbp)
	movq	-816(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
LBB0_9:
	movq	__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rax
	addq	$8, %rax
	movq	-800(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB0_10:
	movq	-800(%rbp), %rax        ## 8-byte Reload
	addq	$416, %rax              ## imm = 0x1A0
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-768(%rbp), %rax
	movq	%rax, -832(%rbp)        ## 8-byte Spill
	jmp	LBB0_29
LBB0_11:                                ## %_ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEC1EPKcj.exit
	leaq	-576(%rbp), %rax
	movq	%rax, -648(%rbp)
	movq	-648(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -640(%rbp)
	movq	-640(%rbp), %rax
	cmpq	$0, 120(%rax)
	setne	%cl
	movb	%cl, -833(%rbp)         ## 1-byte Spill
## BB#12:
	movb	-833(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB0_13
	jmp	LBB0_22
LBB0_13:
Ltmp15:
	leaq	L_.str.1(%rip), %rsi
	leaq	-576(%rbp), %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp16:
	movq	%rax, -848(%rbp)        ## 8-byte Spill
	jmp	LBB0_14
LBB0_14:
Ltmp17:
	leaq	-576(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp18:
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	jmp	LBB0_15
LBB0_15:
	leaq	-576(%rbp), %rax
	movq	%rax, -632(%rbp)
	leaq	-568(%rbp), %rdi
Ltmp19:
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
Ltmp20:
	movq	%rax, -872(%rbp)        ## 8-byte Spill
	jmp	LBB0_16
LBB0_16:                                ## %.noexc
	movq	-872(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, %rax
	jne	LBB0_19
## BB#17:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -616(%rbp)
	movl	$4, -620(%rbp)
	movq	-616(%rbp), %rax
	movq	%rax, -600(%rbp)
	movl	$4, -604(%rbp)
	movq	-600(%rbp), %rax
	movl	32(%rax), %edx
	orl	$4, %edx
Ltmp21:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp22:
	jmp	LBB0_18
LBB0_18:                                ## %.noexc1
	jmp	LBB0_19
LBB0_19:                                ## %_ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEE5closeEv.exit
	jmp	LBB0_20
LBB0_20:
	jmp	LBB0_26
LBB0_21:
Ltmp23:
	leaq	-576(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -784(%rbp)
	movl	%ecx, -788(%rbp)
	callq	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	jmp	LBB0_28
LBB0_22:
Ltmp11:
	movq	__ZNSt3__14cerrE@GOTPCREL(%rip), %rdi
	leaq	L_.str.2(%rip), %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp12:
	movq	%rax, -880(%rbp)        ## 8-byte Spill
	jmp	LBB0_23
LBB0_23:
	movq	-880(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -584(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -592(%rbp)
	movq	-584(%rbp), %rdi
Ltmp13:
	callq	*%rcx
Ltmp14:
	movq	%rax, -888(%rbp)        ## 8-byte Spill
	jmp	LBB0_24
LBB0_24:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB0_25
LBB0_25:
	jmp	LBB0_26
LBB0_26:
	leaq	-576(%rbp), %rdi
	callq	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	movq	___stack_chk_guard@GOTPCREL(%rip), %rdi
	movl	-776(%rbp), %eax
	movq	(%rdi), %rdi
	cmpq	-8(%rbp), %rdi
	movl	%eax, -892(%rbp)        ## 4-byte Spill
	jne	LBB0_30
## BB#27:                               ## %SP_return
	movl	-892(%rbp), %eax        ## 4-byte Reload
	addq	$896, %rsp              ## imm = 0x380
	popq	%rbp
	retq
LBB0_28:
	movq	-784(%rbp), %rax
	movq	%rax, -832(%rbp)        ## 8-byte Spill
LBB0_29:                                ## %unwind_resume
	movq	-832(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
LBB0_30:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset1
Lset2 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp3-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset4
Lset5 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp6-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp9-Ltmp6                     ##   Call between Ltmp6 and Ltmp9
	.long	Lset7
Lset8 = Ltmp10-Lfunc_begin0             ##     jumps to Ltmp10
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp15-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp14-Ltmp15                  ##   Call between Ltmp15 and Ltmp14
	.long	Lset10
Lset11 = Ltmp23-Lfunc_begin0            ##     jumps to Ltmp23
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp14-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset12
Lset13 = Lfunc_end0-Ltmp14              ##   Call between Ltmp14 and Lfunc_end0
	.long	Lset13
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp27:
	.cfi_def_cfa_offset 16
Ltmp28:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp29:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp35:
	.cfi_def_cfa_offset 16
Ltmp36:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp37:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp30:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp31:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB2_1
LBB2_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp32:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp33:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB2_3
LBB2_2:
Ltmp34:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB2_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset14 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset14
Lset15 = Ltmp30-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp30
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset16 = Ltmp30-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset16
Lset17 = Ltmp33-Ltmp30                  ##   Call between Ltmp30 and Ltmp33
	.long	Lset17
Lset18 = Ltmp34-Lfunc_begin1            ##     jumps to Ltmp34
	.long	Lset18
	.byte	0                       ##   On action: cleanup
Lset19 = Ltmp33-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset19
Lset20 = Lfunc_end1-Ltmp33              ##   Call between Ltmp33 and Lfunc_end1
	.long	Lset20
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev: ## @_ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp38:
	.cfi_def_cfa_offset 16
Ltmp39:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp40:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$416, %rsi              ## imm = 0x1A0
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED2Ev
	.align	4, 0x90
__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED2Ev: ## @_ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp41:
	.cfi_def_cfa_offset 16
Ltmp42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp43:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$8, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev: ## @_ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev: ## @_ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev: ## @_ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp50:
	.cfi_def_cfa_offset 16
Ltmp51:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp52:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED2Ev
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp62:
	.cfi_def_cfa_offset 16
Ltmp63:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp64:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rdi, %rax
	movq	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rdi)
Ltmp56:
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
Ltmp57:
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	jmp	LBB9_1
LBB9_1:
	jmp	LBB9_5
LBB9_2:
Ltmp58:
	movl	%edx, %ecx
	movq	%rax, -16(%rbp)
	movl	%ecx, -20(%rbp)
## BB#3:
	movq	-16(%rbp), %rdi
	callq	___cxa_begin_catch
Ltmp59:
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	___cxa_end_catch
Ltmp60:
	jmp	LBB9_4
LBB9_4:
	jmp	LBB9_5
LBB9_5:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	testb	$1, 400(%rax)
	je	LBB9_10
## BB#6:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -56(%rbp)         ## 8-byte Spill
	je	LBB9_8
## BB#7:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdaPv
LBB9_8:
	jmp	LBB9_10
LBB9_9:
Ltmp61:
	movl	%edx, %ecx
	movq	%rax, -16(%rbp)
	movl	%ecx, -20(%rbp)
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	jmp	LBB9_15
LBB9_10:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	testb	$1, 401(%rax)
	je	LBB9_14
## BB#11:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	104(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -64(%rbp)         ## 8-byte Spill
	je	LBB9_13
## BB#12:
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdaPv
LBB9_13:
	jmp	LBB9_14
LBB9_14:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$64, %rsp
	popq	%rbp
	retq
LBB9_15:
	movq	-16(%rbp), %rdi
	callq	___clang_call_terminate
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table9:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\257\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset21 = Ltmp56-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset21
Lset22 = Ltmp57-Ltmp56                  ##   Call between Ltmp56 and Ltmp57
	.long	Lset22
Lset23 = Ltmp58-Lfunc_begin2            ##     jumps to Ltmp58
	.long	Lset23
	.byte	1                       ##   On action: 1
Lset24 = Ltmp57-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset24
Lset25 = Ltmp59-Ltmp57                  ##   Call between Ltmp57 and Ltmp59
	.long	Lset25
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset26 = Ltmp59-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset26
Lset27 = Ltmp60-Ltmp59                  ##   Call between Ltmp59 and Ltmp60
	.long	Lset27
Lset28 = Ltmp61-Lfunc_begin2            ##     jumps to Ltmp61
	.long	Lset28
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5closeEv
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp76:
	.cfi_def_cfa_offset 16
Ltmp77:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp78:
	.cfi_def_cfa_register %rbp
	subq	$496, %rsp              ## imm = 0x1F0
	movq	%rdi, -392(%rbp)
	movq	-392(%rbp), %rdi
	movq	$0, -400(%rbp)
	cmpq	$0, 120(%rdi)
	movq	%rdi, -448(%rbp)        ## 8-byte Spill
	je	LBB10_18
## BB#1:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	120(%rax), %rcx
	movq	_fclose@GOTPCREL(%rip), %rdx
	movq	%rdx, -424(%rbp)
	leaq	-416(%rbp), %rdx
	movq	%rdx, -368(%rbp)
	movq	%rcx, -376(%rbp)
	leaq	-424(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-368(%rbp), %rdx
	movq	-376(%rbp), %rsi
	movq	%rdx, -344(%rbp)
	movq	%rsi, -352(%rbp)
	movq	%rcx, -360(%rbp)
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	movq	%rcx, -336(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rdx, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rsi
	movq	%rdx, -288(%rbp)
	movq	%rsi, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	-288(%rbp), %rcx
	leaq	-296(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-296(%rbp), %rdx
	leaq	-304(%rbp), %rsi
	movq	%rsi, -232(%rbp)
	movq	-304(%rbp), %rsi
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	-256(%rbp), %rcx
	leaq	-264(%rbp), %rdx
	movq	%rdx, -248(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rdx, (%rcx)
	leaq	-272(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	(%rax), %rcx
	movq	48(%rcx), %rcx
Ltmp65:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp66:
	movl	%eax, -452(%rbp)        ## 4-byte Spill
	jmp	LBB10_2
LBB10_2:
	movl	-452(%rbp), %eax        ## 4-byte Reload
	cmpl	$0, %eax
	je	LBB10_9
## BB#3:
	movq	$0, -400(%rbp)
	jmp	LBB10_9
LBB10_4:
Ltmp69:
	leaq	-416(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -432(%rbp)
	movl	%esi, -436(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -120(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -120(%rbp)
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	je	LBB10_8
## BB#5:
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	8(%rax), %rcx
	movq	-120(%rbp), %rdi
Ltmp70:
	callq	*%rcx
Ltmp71:
	movl	%eax, -468(%rbp)        ## 4-byte Spill
	jmp	LBB10_6
LBB10_6:
	jmp	LBB10_8
LBB10_7:
Ltmp72:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -472(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB10_8:                                ## %_ZNSt3__110unique_ptrI7__sFILEPFiPS1_EED1Ev.exit2
	jmp	LBB10_19
LBB10_9:
	leaq	-416(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	%rax, -32(%rbp)
	movq	%rax, -24(%rbp)
	movq	-416(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	$0, -416(%rbp)
	movq	-48(%rbp), %rdi
Ltmp67:
	callq	_fclose
Ltmp68:
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	jmp	LBB10_10
LBB10_10:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	cmpl	$0, %eax
	jne	LBB10_12
## BB#11:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	$0, 120(%rax)
	jmp	LBB10_13
LBB10_12:
	movq	$0, -400(%rbp)
LBB10_13:
	leaq	-416(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	$0, -200(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -208(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -208(%rbp)
	movq	%rax, -488(%rbp)        ## 8-byte Spill
	je	LBB10_17
## BB#14:
	movq	-488(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	%rax, -144(%rbp)
	movq	8(%rax), %rcx
	movq	-208(%rbp), %rdi
Ltmp73:
	callq	*%rcx
Ltmp74:
	movl	%eax, -492(%rbp)        ## 4-byte Spill
	jmp	LBB10_15
LBB10_15:
	jmp	LBB10_17
LBB10_16:
Ltmp75:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -496(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB10_17:                               ## %_ZNSt3__110unique_ptrI7__sFILEPFiPS1_EED1Ev.exit
	jmp	LBB10_18
LBB10_18:
	movq	-400(%rbp), %rax
	addq	$496, %rsp              ## imm = 0x1F0
	popq	%rbp
	retq
LBB10_19:
	movq	-432(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table10:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset29 = Ltmp65-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset29
Lset30 = Ltmp66-Ltmp65                  ##   Call between Ltmp65 and Ltmp66
	.long	Lset30
Lset31 = Ltmp69-Lfunc_begin3            ##     jumps to Ltmp69
	.long	Lset31
	.byte	0                       ##   On action: cleanup
Lset32 = Ltmp70-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset32
Lset33 = Ltmp71-Ltmp70                  ##   Call between Ltmp70 and Ltmp71
	.long	Lset33
Lset34 = Ltmp72-Lfunc_begin3            ##     jumps to Ltmp72
	.long	Lset34
	.byte	1                       ##   On action: 1
Lset35 = Ltmp67-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset35
Lset36 = Ltmp68-Ltmp67                  ##   Call between Ltmp67 and Ltmp68
	.long	Lset36
Lset37 = Ltmp69-Lfunc_begin3            ##     jumps to Ltmp69
	.long	Lset37
	.byte	0                       ##   On action: cleanup
Lset38 = Ltmp73-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset38
Lset39 = Ltmp74-Ltmp73                  ##   Call between Ltmp73 and Ltmp74
	.long	Lset39
Lset40 = Ltmp75-Lfunc_begin3            ##     jumps to Ltmp75
	.long	Lset40
	.byte	1                       ##   On action: 1
Lset41 = Ltmp74-Lfunc_begin3            ## >> Call Site 5 <<
	.long	Lset41
Lset42 = Lfunc_end3-Ltmp74              ##   Call between Ltmp74 and Lfunc_end3
	.long	Lset42
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp79:
	.cfi_def_cfa_offset 16
Ltmp80:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp81:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp82:
	.cfi_def_cfa_offset 16
Ltmp83:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp84:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	%rdi, -104(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-104(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	callq	*48(%rax)
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	-88(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movl	%eax, -116(%rbp)        ## 4-byte Spill
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
	movq	-112(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, 128(%rsi)
	movb	402(%rsi), %cl
	andb	$1, %cl
	movb	%cl, -89(%rbp)
	movq	128(%rsi), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rdi
	movq	%rdi, -128(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-128(%rbp), %rax        ## 8-byte Reload
	callq	*56(%rax)
	andb	$1, %al
	movq	-112(%rbp), %rsi        ## 8-byte Reload
	movb	%al, 402(%rsi)
	movb	-89(%rbp), %al
	andb	$1, %al
	movzbl	%al, %edx
	movb	402(%rsi), %al
	andb	$1, %al
	movzbl	%al, %r8d
	cmpl	%r8d, %edx
	je	LBB13_13
## BB#1:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	$0, -40(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-56(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB13_7
## BB#2:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 400(%rax)
	je	LBB13_6
## BB#3:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -136(%rbp)        ## 8-byte Spill
	je	LBB13_5
## BB#4:
	movq	-136(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB13_5:
	jmp	LBB13_6
LBB13_6:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movb	401(%rax), %cl
	andb	$1, %cl
	movb	%cl, 400(%rax)
	movq	112(%rax), %rdx
	movq	%rdx, 96(%rax)
	movq	104(%rax), %rdx
	movq	%rdx, 64(%rax)
	movq	$0, 112(%rax)
	movq	$0, 104(%rax)
	movb	$0, 401(%rax)
	jmp	LBB13_12
LBB13_7:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 400(%rax)
	jne	LBB13_10
## BB#8:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	addq	$88, %rax
	cmpq	%rax, %rcx
	je	LBB13_10
## BB#9:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, 112(%rax)
	movq	64(%rax), %rcx
	movq	%rcx, 104(%rax)
	movb	$0, 401(%rax)
	movq	96(%rax), %rdi
	callq	__Znam
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movb	$1, 400(%rcx)
	jmp	LBB13_11
LBB13_10:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, 112(%rax)
	movq	112(%rax), %rdi
	callq	__Znam
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 104(%rcx)
	movb	$1, 401(%rcx)
LBB13_11:
	jmp	LBB13_12
LBB13_12:
	jmp	LBB13_13
LBB13_13:
	addq	$144, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp85:
	.cfi_def_cfa_offset 16
Ltmp86:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp87:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -136(%rbp)
	movq	%rsi, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	$0, -120(%rbp)
	movq	$0, -128(%rbp)
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%rdi, 16(%rsi)
	movq	-120(%rbp), %rdi
	movq	%rdi, 24(%rsi)
	movq	-128(%rbp), %rdi
	movq	%rdi, 32(%rsi)
	movq	%rdx, %rsi
	movq	%rsi, -8(%rbp)
	movq	$0, -16(%rbp)
	movq	$0, -24(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	%rdi, 48(%rsi)
	movq	%rdi, 40(%rsi)
	movq	-24(%rbp), %rdi
	movq	%rdi, 56(%rsi)
	testb	$1, 400(%rdx)
	movq	%rdx, -168(%rbp)        ## 8-byte Spill
	je	LBB14_4
## BB#1:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -176(%rbp)        ## 8-byte Spill
	je	LBB14_3
## BB#2:
	movq	-176(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB14_3:
	jmp	LBB14_4
LBB14_4:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 401(%rax)
	je	LBB14_8
## BB#5:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	104(%rax), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -184(%rbp)        ## 8-byte Spill
	je	LBB14_7
## BB#6:
	movq	-184(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdaPv
LBB14_7:
	jmp	LBB14_8
LBB14_8:
	movq	-152(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 96(%rcx)
	cmpq	$8, 96(%rcx)
	jbe	LBB14_14
## BB#9:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB14_12
## BB#10:
	cmpq	$0, -144(%rbp)
	je	LBB14_12
## BB#11:
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movb	$0, 400(%rcx)
	jmp	LBB14_13
LBB14_12:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rdi
	callq	__Znam
	movq	-168(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, 64(%rdi)
	movb	$1, 400(%rdi)
LBB14_13:
	jmp	LBB14_15
LBB14_14:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$88, %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 64(%rcx)
	movq	$8, 96(%rcx)
	movb	$0, 400(%rcx)
LBB14_15:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	jne	LBB14_24
## BB#16:
	leaq	-56(%rbp), %rax
	leaq	-160(%rbp), %rcx
	leaq	-152(%rbp), %rdx
	movq	$8, -160(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rax, -32(%rbp)
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	-48(%rbp), %rcx
	cmpq	(%rcx), %rax
	jge	LBB14_18
## BB#17:
	movq	-72(%rbp), %rax
	movq	%rax, -192(%rbp)        ## 8-byte Spill
	jmp	LBB14_19
LBB14_18:
	movq	-64(%rbp), %rax
	movq	%rax, -192(%rbp)        ## 8-byte Spill
LBB14_19:                               ## %_ZNSt3__13maxIlEERKT_S3_S3_.exit
	movq	-192(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 112(%rcx)
	cmpq	$0, -144(%rbp)
	je	LBB14_22
## BB#20:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	cmpq	$8, 112(%rax)
	jb	LBB14_22
## BB#21:
	movq	-144(%rbp), %rax
	movq	-168(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 104(%rcx)
	movb	$0, 401(%rcx)
	jmp	LBB14_23
LBB14_22:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	112(%rax), %rdi
	callq	__Znam
	movq	-168(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, 104(%rdi)
	movb	$1, 401(%rdi)
LBB14_23:
	jmp	LBB14_25
LBB14_24:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	movq	$0, 112(%rax)
	movq	$0, 104(%rax)
	movb	$0, 401(%rax)
LBB14_25:
	movq	-168(%rbp), %rax        ## 8-byte Reload
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp88:
	.cfi_def_cfa_offset 16
Ltmp89:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp90:
	.cfi_def_cfa_register %rbp
	subq	$720, %rsp              ## imm = 0x2D0
	movq	%rdi, %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %r9
	movq	(%r9), %r9
	movq	%r9, -8(%rbp)
	movq	%rsi, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movl	%ecx, -564(%rbp)
	movl	%r8d, -568(%rbp)
	movq	-552(%rbp), %rdx
	cmpq	$0, 128(%rdx)
	movq	%rax, -584(%rbp)        ## 8-byte Spill
	movq	%rdi, -592(%rbp)        ## 8-byte Spill
	movq	%rdx, -600(%rbp)        ## 8-byte Spill
	jne	LBB15_2
## BB#1:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -608(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-608(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -616(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-616(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB15_2:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, %rdi
	callq	*48(%rdx)
	movl	%eax, -572(%rbp)
	movq	-600(%rbp), %rcx        ## 8-byte Reload
	cmpq	$0, 120(%rcx)
	je	LBB15_6
## BB#3:
	cmpl	$0, -572(%rbp)
	jg	LBB15_5
## BB#4:
	cmpq	$0, -560(%rbp)
	jne	LBB15_6
LBB15_5:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	cmpl	$0, %eax
	je	LBB15_7
LBB15_6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	$-1, -536(%rbp)
	movq	-528(%rbp), %rdi
	movq	-536(%rbp), %r8
	movq	%rdi, -512(%rbp)
	movq	%r8, -520(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -624(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-520(%rbp), %rcx
	movq	-624(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB15_18
LBB15_7:
	movl	-564(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -628(%rbp)        ## 4-byte Spill
	je	LBB15_8
	jmp	LBB15_21
LBB15_21:
	movl	-628(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -632(%rbp)        ## 4-byte Spill
	je	LBB15_9
	jmp	LBB15_22
LBB15_22:
	movl	-628(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -636(%rbp)        ## 4-byte Spill
	je	LBB15_10
	jmp	LBB15_11
LBB15_8:
	movl	$0, -576(%rbp)
	jmp	LBB15_12
LBB15_9:
	movl	$1, -576(%rbp)
	jmp	LBB15_12
LBB15_10:
	movl	$2, -576(%rbp)
	jmp	LBB15_12
LBB15_11:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -648(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-648(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB15_18
LBB15_12:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	cmpl	$0, -572(%rbp)
	movq	%rdi, -656(%rbp)        ## 8-byte Spill
	jle	LBB15_14
## BB#13:
	movslq	-572(%rbp), %rax
	imulq	-560(%rbp), %rax
	movq	%rax, -664(%rbp)        ## 8-byte Spill
	jmp	LBB15_15
LBB15_14:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -664(%rbp)        ## 8-byte Spill
	jmp	LBB15_15
LBB15_15:
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movl	-576(%rbp), %edx
	movq	-656(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB15_17
## BB#16:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-592(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -456(%rbp)
	movq	$-1, -464(%rbp)
	movq	-456(%rbp), %rdi
	movq	-464(%rbp), %r8
	movq	%rdi, -440(%rbp)
	movq	%r8, -448(%rbp)
	movq	-440(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -672(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-448(%rbp), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB15_18
LBB15_17:
	movq	-600(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_ftello
	movl	$136, %ecx
	movl	%ecx, %edx
	leaq	-272(%rbp), %rdi
	movl	$128, %ecx
	movl	%ecx, %esi
	leaq	-136(%rbp), %r8
	leaq	-400(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%rdi, -488(%rbp)
	movq	%rax, -496(%rbp)
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %r10
	movq	%rax, -472(%rbp)
	movq	%r10, -480(%rbp)
	movq	-472(%rbp), %rax
	movq	%rax, %r10
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, -688(%rbp)        ## 8-byte Spill
	movl	%ecx, %esi
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	%rdx, -696(%rbp)        ## 8-byte Spill
	movq	%r10, %rdx
	movq	%r9, -704(%rbp)         ## 8-byte Spill
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movq	%r8, -720(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-480(%rbp), %rax
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, 128(%rdx)
	movq	-600(%rbp), %rax        ## 8-byte Reload
	addq	$136, %rax
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rdx
	movq	-720(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, %rdi
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-592(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	-696(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
LBB15_18:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	jne	LBB15_20
## BB#19:                               ## %SP_return
	movq	-584(%rbp), %rax        ## 8-byte Reload
	addq	$720, %rsp              ## imm = 0x2D0
	popq	%rbp
	retq
LBB15_20:                               ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp91:
	.cfi_def_cfa_offset 16
Ltmp92:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp93:
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp              ## imm = 0x140
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	movq	___stack_chk_guard@GOTPCREL(%rip), %r8
	movq	(%r8), %r8
	movq	%r8, -8(%rbp)
	movq	%rsi, -224(%rbp)
	movl	%edx, -228(%rbp)
	movq	-224(%rbp), %rsi
	cmpq	$0, 120(%rsi)
	movq	%rax, -240(%rbp)        ## 8-byte Spill
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	je	LBB16_2
## BB#1:
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*48(%rcx)
	cmpl	$0, %eax
	je	LBB16_3
LBB16_2:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	$-1, -216(%rbp)
	movq	-208(%rbp), %rdi
	movq	-216(%rbp), %r8
	movq	%rdi, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	-192(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -272(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-200(%rbp), %rcx
	movq	-272(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB16_6
LBB16_3:
	xorl	%edx, %edx
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	movq	-248(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rsi
	movq	128(%rsi), %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB16_5
## BB#4:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -168(%rbp)
	movq	$-1, -176(%rbp)
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r8
	movq	%rdi, -152(%rbp)
	movq	%r8, -160(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -280(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-160(%rbp), %rcx
	movq	-280(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB16_6
LBB16_5:
	movl	$136, %eax
	movl	%eax, %edx
	movl	$128, %eax
	movl	%eax, %ecx
	leaq	-136(%rbp), %rsi
	movq	-264(%rbp), %rdi        ## 8-byte Reload
	addq	$136, %rdi
	movq	-248(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -184(%rbp)
	movq	-184(%rbp), %r9
	movq	%rsi, %r10
	movq	%rdi, -288(%rbp)        ## 8-byte Spill
	movq	%r10, %rdi
	movq	%rsi, -296(%rbp)        ## 8-byte Spill
	movq	%r9, %rsi
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	%rcx, -312(%rbp)        ## 8-byte Spill
	callq	_memcpy
	movq	-288(%rbp), %rcx        ## 8-byte Reload
	movq	-296(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	-312(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	movq	-248(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	-304(%rbp), %rdx        ## 8-byte Reload
	callq	_memcpy
LBB16_6:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	jne	LBB16_8
## BB#7:                                ## %SP_return
	movq	-240(%rbp), %rax        ## 8-byte Reload
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbp
	retq
LBB16_8:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp94:
	.cfi_def_cfa_offset 16
Ltmp95:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp96:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rax
	cmpq	$0, 120(%rax)
	movq	%rax, -408(%rbp)        ## 8-byte Spill
	jne	LBB17_2
## BB#1:
	movl	$0, -340(%rbp)
	jmp	LBB17_35
LBB17_2:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB17_4
## BB#3:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -416(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-416(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -424(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-424(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB17_4:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	396(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB17_19
## BB#5:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	48(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rcx
	cmpq	40(%rcx), %rax
	je	LBB17_9
## BB#6:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movq	%rcx, -432(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-432(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -436(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-436(%rbp), %esi        ## 4-byte Reload
	cmpl	%eax, %esi
	jne	LBB17_8
## BB#7:
	movl	$-1, -340(%rbp)
	jmp	LBB17_35
LBB17_8:
	jmp	LBB17_9
LBB17_9:
	jmp	LBB17_10
LBB17_10:                               ## =>This Inner Loop Header: Depth=1
	leaq	-368(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	128(%rcx), %rdx
	addq	$136, %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	movq	64(%rsi), %rdi
	movq	64(%rsi), %r8
	addq	96(%rsi), %r8
	movq	%rdx, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rcx
	movq	40(%rcx), %rcx
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %rdi
	movq	-192(%rbp), %r8
	movq	%rdi, -448(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	-456(%rbp), %r9         ## 8-byte Reload
	callq	*%r9
	movl	$1, %r10d
	movl	%r10d, %esi
	movl	%eax, -356(%rbp)
	movq	-368(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	64(%rdx), %rdi
	subq	%rdi, %rcx
	movq	%rcx, -376(%rbp)
	movq	64(%rdx), %rdi
	movq	-376(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-376(%rbp), %rax
	je	LBB17_12
## BB#11:
	movl	$-1, -340(%rbp)
	jmp	LBB17_35
LBB17_12:                               ##   in Loop: Header=BB17_10 Depth=1
	jmp	LBB17_13
LBB17_13:                               ##   in Loop: Header=BB17_10 Depth=1
	cmpl	$1, -356(%rbp)
	je	LBB17_10
## BB#14:
	cmpl	$2, -356(%rbp)
	jne	LBB17_16
## BB#15:
	movl	$-1, -340(%rbp)
	jmp	LBB17_35
LBB17_16:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_fflush
	cmpl	$0, %eax
	je	LBB17_18
## BB#17:
	movl	$-1, -340(%rbp)
	jmp	LBB17_35
LBB17_18:
	jmp	LBB17_34
LBB17_19:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	396(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB17_33
## BB#20:
	movl	$128, %eax
	movl	%eax, %edx
	leaq	-136(%rbp), %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	addq	$264, %rsi              ## imm = 0x108
	movq	%rcx, %rdi
	callq	_memcpy
	movb	$0, -385(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	testb	$1, 402(%rcx)
	je	LBB17_22
## BB#21:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	32(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	24(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -384(%rbp)
	jmp	LBB17_28
LBB17_22:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, %rdi
	callq	*48(%rdx)
	movl	%eax, -392(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	80(%rcx), %rdx
	movq	72(%rcx), %rdi
	subq	%rdi, %rdx
	movq	%rdx, -384(%rbp)
	cmpl	$0, -392(%rbp)
	jle	LBB17_24
## BB#23:
	movslq	-392(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	32(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -216(%rbp)
	movq	-216(%rbp), %rdx
	movq	24(%rdx), %rdx
	subq	%rdx, %rcx
	imulq	%rcx, %rax
	addq	-384(%rbp), %rax
	movq	%rax, -384(%rbp)
	jmp	LBB17_27
LBB17_24:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	24(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	cmpq	32(%rcx), %rax
	je	LBB17_26
## BB#25:
	leaq	-136(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	128(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	72(%rcx), %rdi
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	24(%rcx), %rcx
	movq	-408(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -248(%rbp)
	movq	-248(%rbp), %r8
	movq	16(%r8), %r8
	subq	%r8, %rcx
	movq	%rdx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rsi, -280(%rbp)
	movq	%rdi, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-264(%rbp), %rax
	movq	(%rax), %rcx
	movq	64(%rcx), %rcx
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %rdx
	movq	-288(%rbp), %rdi
	movq	-296(%rbp), %r8
	movq	%rdi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -472(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	-472(%rbp), %r9         ## 8-byte Reload
	callq	*%r9
	movl	%eax, -396(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	72(%rcx), %rdx
	movq	64(%rcx), %rsi
	subq	%rsi, %rdx
	movslq	-396(%rbp), %rsi
	subq	%rsi, %rdx
	addq	-384(%rbp), %rdx
	movq	%rdx, -384(%rbp)
	movb	$1, -385(%rbp)
LBB17_26:
	jmp	LBB17_27
LBB17_27:
	jmp	LBB17_28
LBB17_28:
	movl	$1, %edx
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	movq	120(%rsi), %rdi
	subq	-384(%rbp), %rcx
	movq	%rcx, %rsi
	callq	_fseeko
	cmpl	$0, %eax
	je	LBB17_30
## BB#29:
	movl	$-1, -340(%rbp)
	jmp	LBB17_35
LBB17_30:
	testb	$1, -385(%rbp)
	je	LBB17_32
## BB#31:
	movl	$128, %eax
	movl	%eax, %edx
	leaq	-136(%rbp), %rcx
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	addq	$136, %rsi
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	callq	_memcpy
LBB17_32:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	%rcx, 80(%rax)
	movq	%rcx, 72(%rax)
	movq	%rax, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -328(%rbp)
	movq	-304(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-320(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-328(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movl	$0, 396(%rax)
LBB17_33:
	jmp	LBB17_34
LBB17_34:
	movl	$0, -340(%rbp)
LBB17_35:
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movl	-340(%rbp), %ecx
	movq	(%rax), %rax
	cmpq	-8(%rbp), %rax
	movl	%ecx, -476(%rbp)        ## 4-byte Spill
	jne	LBB17_37
## BB#36:                               ## %SP_return
	movl	-476(%rbp), %eax        ## 4-byte Reload
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB17_37:                               ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp97:
	.cfi_def_cfa_offset 16
Ltmp98:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp99:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$792, %rsp              ## imm = 0x318
Ltmp100:
	.cfi_offset %rbx, -24
	movq	%rdi, -576(%rbp)
	movq	-576(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	jne	LBB18_2
## BB#1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -564(%rbp)
	jmp	LBB18_36
LBB18_2:
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	andb	$1, %al
	movb	%al, -577(%rbp)
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -560(%rbp)
	movq	-560(%rbp), %rdi
	cmpq	$0, 24(%rdi)
	jne	LBB18_4
## BB#3:
	leaq	-578(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, %rdx
	addq	$1, %rdx
	movq	%rax, %rsi
	addq	$1, %rsi
	movq	%rcx, -488(%rbp)
	movq	%rax, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	%rsi, -512(%rbp)
	movq	-488(%rbp), %rax
	movq	-496(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-504(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB18_4:
	testb	$1, -577(%rbp)
	je	LBB18_6
## BB#5:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -688(%rbp)        ## 8-byte Spill
	jmp	LBB18_10
LBB18_6:
	leaq	-248(%rbp), %rax
	leaq	-608(%rbp), %rcx
	leaq	-600(%rbp), %rdx
	movl	$2, %esi
	movl	%esi, %edi
	movq	-680(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -472(%rbp)
	movq	-472(%rbp), %r8
	movq	32(%r8), %r8
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -400(%rbp)
	movq	-400(%rbp), %r9
	movq	16(%r9), %r9
	subq	%r9, %r8
	movq	%rax, -696(%rbp)        ## 8-byte Spill
	movq	%r8, %rax
	movq	%rdx, -704(%rbp)        ## 8-byte Spill
	cqto
	idivq	%rdi
	movq	%rax, -600(%rbp)
	movq	$4, -608(%rbp)
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	%rcx, -280(%rbp)
	movq	-272(%rbp), %rcx
	movq	-280(%rbp), %rdi
	movq	%rcx, -256(%rbp)
	movq	%rdi, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rdi
	movq	-696(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdi, -240(%rbp)
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-240(%rbp), %rdi
	cmpq	(%rdi), %rcx
	jae	LBB18_8
## BB#7:
	movq	-264(%rbp), %rax
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	jmp	LBB18_9
LBB18_8:
	movq	-256(%rbp), %rax
	movq	%rax, -712(%rbp)        ## 8-byte Spill
LBB18_9:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -688(%rbp)        ## 8-byte Spill
LBB18_10:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -592(%rbp)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -612(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	cmpq	32(%rdx), %rcx
	jne	LBB18_32
## BB#11:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	16(%rdx), %rdi
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	32(%rdx), %rdx
	subq	-592(%rbp), %rcx
	addq	%rcx, %rdx
	movq	-592(%rbp), %rcx
	shlq	$0, %rcx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	callq	_memmove
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	testb	$1, 402(%rcx)
	je	LBB18_15
## BB#12:
	movl	$1, %eax
	movl	%eax, %esi
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	32(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	16(%rdx), %rdx
	subq	%rdx, %rcx
	subq	-592(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	16(%rcx), %rcx
	addq	-592(%rbp), %rcx
	movq	-624(%rbp), %rdx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	120(%rdi), %r8
	movq	%rcx, %rdi
	movq	%r8, %rcx
	callq	_fread
	movq	%rax, -624(%rbp)
	cmpq	$0, -624(%rbp)
	je	LBB18_14
## BB#13:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-592(%rbp), %rdx
	movq	-680(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	16(%rsi), %rsi
	addq	-592(%rbp), %rsi
	addq	-624(%rbp), %rsi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB18_14:
	jmp	LBB18_31
LBB18_15:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rdi
	movq	72(%rax), %rsi
	movq	80(%rax), %rcx
	movq	72(%rax), %rdx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	callq	_memmove
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	80(%rax), %rdx
	movq	72(%rax), %rsi
	subq	%rsi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 72(%rax)
	movq	64(%rax), %rcx
	movq	64(%rax), %rdx
	addq	$88, %rax
	cmpq	%rax, %rdx
	movq	%rcx, -720(%rbp)        ## 8-byte Spill
	jne	LBB18_17
## BB#16:
	movl	$8, %eax
	movl	%eax, %ecx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB18_18
LBB18_17:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	96(%rax), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB18_18:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	leaq	-160(%rbp), %rcx
	leaq	-648(%rbp), %rdx
	leaq	-640(%rbp), %rsi
	movq	-720(%rbp), %rdi        ## 8-byte Reload
	addq	%rax, %rdi
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rdi, 80(%rax)
	movq	112(%rax), %rdi
	subq	-592(%rbp), %rdi
	movq	%rdi, -640(%rbp)
	movq	80(%rax), %rdi
	movq	72(%rax), %r8
	subq	%r8, %rdi
	movq	%rdi, -648(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdx, -192(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	-168(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rsi, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-152(%rbp), %rdx
	cmpq	(%rdx), %rcx
	jae	LBB18_20
## BB#19:
	movq	-176(%rbp), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB18_21
LBB18_20:
	movq	-168(%rbp), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
LBB18_21:                               ## %_ZNSt3__13minImEERKT_S3_S3_.exit3
	movq	-736(%rbp), %rax        ## 8-byte Reload
	movl	$1, %ecx
	movl	%ecx, %esi
	movl	$128, %ecx
	movl	%ecx, %edx
	movq	(%rax), %rax
	movq	%rax, -632(%rbp)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	addq	$264, %rax              ## imm = 0x108
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	addq	$136, %rdi
	movq	%rdi, -744(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -752(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	callq	_memcpy
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	72(%rax), %rdi
	movq	-632(%rbp), %rdx
	movq	120(%rax), %rcx
	movq	-752(%rbp), %rsi        ## 8-byte Reload
	callq	_fread
	movq	%rax, -664(%rbp)
	cmpq	$0, -664(%rbp)
	je	LBB18_30
## BB#22:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB18_24
## BB#23:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB18_24:
	leaq	-672(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	72(%rcx), %rdx
	addq	-664(%rbp), %rdx
	movq	%rdx, 80(%rcx)
	movq	128(%rcx), %rdx
	addq	$136, %rcx
	movq	-680(%rbp), %rsi        ## 8-byte Reload
	movq	64(%rsi), %rdi
	movq	80(%rsi), %r8
	addq	$72, %rsi
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	16(%r9), %r9
	addq	-592(%rbp), %r9
	movq	-680(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	16(%r10), %r10
	movq	-680(%rbp), %r11        ## 8-byte Reload
	addq	112(%r11), %r10
	movq	%rdx, -296(%rbp)
	movq	%rcx, -304(%rbp)
	movq	%rdi, -312(%rbp)
	movq	%r8, -320(%rbp)
	movq	%rsi, -328(%rbp)
	movq	%r9, -336(%rbp)
	movq	%r10, -344(%rbp)
	movq	%rax, -352(%rbp)
	movq	-296(%rbp), %rax
	movq	(%rax), %rcx
	movq	32(%rcx), %rcx
	movq	-304(%rbp), %rsi
	movq	-312(%rbp), %rdx
	movq	-320(%rbp), %rdi
	movq	-328(%rbp), %r8
	movq	-336(%rbp), %r9
	movq	-344(%rbp), %r10
	movq	-352(%rbp), %rbx
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-776(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -784(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	%r10, (%rsp)
	movq	%rbx, 8(%rsp)
	movq	-784(%rbp), %r10        ## 8-byte Reload
	callq	*%r10
	movl	%eax, -652(%rbp)
	cmpl	$3, -652(%rbp)
	jne	LBB18_26
## BB#25:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	80(%rcx), %rdi
	movq	%rax, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rdi, -384(%rbp)
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-376(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-384(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
	jmp	LBB18_29
LBB18_26:
	movq	-672(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	16(%rcx), %rcx
	addq	-592(%rbp), %rcx
	cmpq	%rcx, %rax
	je	LBB18_28
## BB#27:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -424(%rbp)
	movq	-424(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-592(%rbp), %rdx
	movq	-672(%rbp), %rsi
	movq	%rax, -432(%rbp)
	movq	%rcx, -440(%rbp)
	movq	%rdx, -448(%rbp)
	movq	%rsi, -456(%rbp)
	movq	-432(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-448(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-456(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB18_28:
	jmp	LBB18_29
LBB18_29:
	jmp	LBB18_30
LBB18_30:
	jmp	LBB18_31
LBB18_31:
	jmp	LBB18_33
LBB18_32:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -612(%rbp)
LBB18_33:
	leaq	-578(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	cmpq	%rax, 16(%rcx)
	jne	LBB18_35
## BB#34:
	movq	-680(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	$0, -536(%rbp)
	movq	$0, -544(%rbp)
	movq	$0, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB18_35:
	movl	-612(%rbp), %eax
	movl	%eax, -564(%rbp)
LBB18_36:
	movl	-564(%rbp), %eax
	addq	$792, %rsp              ## imm = 0x318
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp101:
	.cfi_def_cfa_offset 16
Ltmp102:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp103:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -80(%rbp)
	movl	%esi, -84(%rbp)
	movq	-80(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -96(%rbp)         ## 8-byte Spill
	je	LBB19_8
## BB#1:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	16(%rax), %rax
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB19_8
## BB#2:
	movl	-84(%rbp), %edi
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-100(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB19_3
	jmp	LBB19_4
LBB19_3:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movl	$-1, -36(%rbp)
	movq	-32(%rbp), %rax
	movl	-36(%rbp), %ecx
	movq	24(%rax), %rdx
	movslq	%ecx, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 24(%rax)
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -68(%rbp)
	jmp	LBB19_9
LBB19_4:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movl	392(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB19_6
## BB#5:
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB19_6
	jmp	LBB19_7
LBB19_6:
	movq	-96(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movl	$-1, -20(%rbp)
	movq	-16(%rbp), %rax
	movl	-20(%rbp), %ecx
	movq	24(%rax), %rdx
	movslq	%ecx, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 24(%rax)
	movl	-84(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	24(%rdx), %rdx
	movb	%al, (%rdx)
	movl	-84(%rbp), %ecx
	movl	%ecx, -68(%rbp)
	jmp	LBB19_9
LBB19_7:
	jmp	LBB19_8
LBB19_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -68(%rbp)
LBB19_9:
	movl	-68(%rbp), %eax
	addq	$112, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp104:
	.cfi_def_cfa_offset 16
Ltmp105:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp106:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -328(%rbp)
	movl	%esi, -332(%rbp)
	movq	-328(%rbp), %rdi
	cmpq	$0, 120(%rdi)
	movq	%rdi, -408(%rbp)        ## 8-byte Spill
	jne	LBB20_2
## BB#1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_2:
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	40(%rdi), %rdi
	movq	%rdi, -344(%rbp)
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -304(%rbp)
	movq	-304(%rbp), %rdi
	movq	56(%rdi), %rdi
	movq	%rdi, -352(%rbp)
	movl	-332(%rbp), %edi
	movl	%edi, -412(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-412(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB20_6
## BB#3:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB20_5
## BB#4:
	leaq	-333(%rbp), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, %rdx
	addq	$1, %rdx
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-200(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB20_5:
	movl	-332(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	48(%rcx), %rcx
	movb	%al, (%rcx)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -136(%rbp)
	movl	$1, -140(%rbp)
	movq	-136(%rbp), %rcx
	movl	-140(%rbp), %edi
	movq	48(%rcx), %rdx
	movslq	%edi, %rsi
	addq	%rsi, %rdx
	movq	%rdx, 48(%rcx)
LBB20_6:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	48(%rax), %rax
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	cmpq	40(%rcx), %rax
	je	LBB20_33
## BB#7:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB20_11
## BB#8:
	movl	$1, %eax
	movl	%eax, %esi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	48(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movq	%rcx, -360(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	-360(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-360(%rbp), %rax
	je	LBB20_10
## BB#9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_10:
	jmp	LBB20_32
LBB20_11:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	64(%rax), %rcx
	movq	%rcx, -368(%rbp)
LBB20_12:                               ## =>This Inner Loop Header: Depth=1
	movq	-408(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, 128(%rax)
	jne	LBB20_14
## BB#13:
	movl	$8, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movq	%rax, -424(%rbp)        ## 8-byte Spill
	callq	__ZNSt8bad_castC1Ev
	movq	__ZTISt8bad_cast@GOTPCREL(%rip), %rax
	movq	__ZNSt8bad_castD1Ev@GOTPCREL(%rip), %rdi
	movq	-424(%rbp), %rcx        ## 8-byte Reload
	movq	%rdi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	callq	___cxa_throw
LBB20_14:                               ##   in Loop: Header=BB20_12 Depth=1
	leaq	-368(%rbp), %rax
	leaq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	128(%rdx), %rsi
	addq	$136, %rdx
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdi
	movq	40(%rdi), %rdi
	movq	-408(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -48(%rbp)
	movq	-48(%rbp), %r8
	movq	48(%r8), %r8
	movq	-408(%rbp), %r9         ## 8-byte Reload
	movq	64(%r9), %r10
	movq	64(%r9), %r11
	addq	96(%r9), %r11
	movq	%rsi, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rdi, -80(%rbp)
	movq	%r8, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r11, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rdi
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	%rdi, -440(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	movq	%rax, %rcx
	movq	%r10, (%rsp)
	movq	%r11, 8(%rsp)
	movq	-448(%rbp), %r10        ## 8-byte Reload
	callq	*%r10
	movl	%eax, -372(%rbp)
	movq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	cmpq	40(%rdx), %rcx
	jne	LBB20_16
## BB#15:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_16:                               ##   in Loop: Header=BB20_12 Depth=1
	cmpl	$3, -372(%rbp)
	jne	LBB20_20
## BB#17:                               ##   in Loop: Header=BB20_12 Depth=1
	movl	$1, %eax
	movl	%eax, %esi
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	48(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movq	%rcx, -392(%rbp)
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	40(%rcx), %rdi
	movq	-392(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-392(%rbp), %rax
	je	LBB20_19
## BB#18:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_19:                               ##   in Loop: Header=BB20_12 Depth=1
	jmp	LBB20_29
LBB20_20:                               ##   in Loop: Header=BB20_12 Depth=1
	cmpl	$0, -372(%rbp)
	je	LBB20_22
## BB#21:                               ##   in Loop: Header=BB20_12 Depth=1
	cmpl	$1, -372(%rbp)
	jne	LBB20_27
LBB20_22:                               ##   in Loop: Header=BB20_12 Depth=1
	movl	$1, %eax
	movl	%eax, %esi
	movq	-368(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	64(%rdx), %rdi
	subq	%rdi, %rcx
	movq	%rcx, -400(%rbp)
	movq	64(%rdx), %rdi
	movq	-400(%rbp), %rdx
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	120(%rcx), %rcx
	callq	_fwrite
	cmpq	-400(%rbp), %rax
	je	LBB20_24
## BB#23:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_24:                               ##   in Loop: Header=BB20_12 Depth=1
	cmpl	$1, -372(%rbp)
	jne	LBB20_26
## BB#25:                               ##   in Loop: Header=BB20_12 Depth=1
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-384(%rbp), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	48(%rdx), %rdx
	movq	%rax, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-232(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-408(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	56(%rcx), %rcx
	movq	-408(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -248(%rbp)
	movq	-248(%rbp), %rdx
	movq	40(%rdx), %rdx
	subq	%rdx, %rcx
	movl	%ecx, %esi
	movq	%rax, -256(%rbp)
	movl	%esi, -260(%rbp)
	movq	-256(%rbp), %rax
	movl	-260(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB20_26:                               ##   in Loop: Header=BB20_12 Depth=1
	jmp	LBB20_28
LBB20_27:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -316(%rbp)
	jmp	LBB20_34
LBB20_28:                               ##   in Loop: Header=BB20_12 Depth=1
	jmp	LBB20_29
LBB20_29:                               ##   in Loop: Header=BB20_12 Depth=1
	jmp	LBB20_30
LBB20_30:                               ##   in Loop: Header=BB20_12 Depth=1
	cmpl	$1, -372(%rbp)
	je	LBB20_12
## BB#31:
	jmp	LBB20_32
LBB20_32:
	movq	-408(%rbp), %rax        ## 8-byte Reload
	movq	-344(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	%rax, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-296(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB20_33:
	movl	-332(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -316(%rbp)
LBB20_34:
	movl	-316(%rbp), %eax
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp107:
	.cfi_def_cfa_offset 16
Ltmp108:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp109:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE11__read_modeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp110:
	.cfi_def_cfa_offset 16
Ltmp111:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp112:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movl	396(%rdi), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	movq	%rdi, -112(%rbp)        ## 8-byte Spill
	jne	LBB22_5
## BB#1:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-88(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-112(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB22_3
## BB#2:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	addq	96(%rcx), %rsi
	movq	64(%rcx), %rdi
	addq	96(%rcx), %rdi
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-24(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-32(%rbp), %rdx
	movq	%rdx, 32(%rax)
	jmp	LBB22_4
LBB22_3:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	-112(%rbp), %rcx        ## 8-byte Reload
	movq	104(%rcx), %rdx
	movq	104(%rcx), %rsi
	addq	112(%rcx), %rsi
	movq	104(%rcx), %rdi
	addq	112(%rcx), %rdi
	movq	%rax, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rsi, -56(%rbp)
	movq	%rdi, -64(%rbp)
	movq	-40(%rbp), %rax
	movq	-48(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB22_4:
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movl	$8, 396(%rax)
	movb	$1, -89(%rbp)
	jmp	LBB22_6
LBB22_5:
	movb	$0, -89(%rbp)
LBB22_6:
	movb	-89(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp113:
	.cfi_def_cfa_offset 16
Ltmp114:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp115:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp116:
	.cfi_def_cfa_offset 16
Ltmp117:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp118:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp119:
	.cfi_def_cfa_offset 16
Ltmp120:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp121:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB25_1
	jmp	LBB25_2
LBB25_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB25_3
LBB25_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB25_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp122:
	.cfi_def_cfa_offset 16
Ltmp123:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp124:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp125:
	.cfi_def_cfa_offset 16
Ltmp126:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp127:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE12__write_modeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp128:
	.cfi_def_cfa_offset 16
Ltmp129:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp130:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movl	396(%rdi), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jne	LBB28_8
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -104(%rbp)
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-96(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-120(%rbp), %rax        ## 8-byte Reload
	cmpq	$8, 96(%rax)
	jbe	LBB28_6
## BB#2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	testb	$1, 402(%rax)
	je	LBB28_4
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	64(%rcx), %rdx
	movq	64(%rcx), %rsi
	movq	96(%rcx), %rdi
	subq	$1, %rdi
	addq	%rdi, %rsi
	movq	%rax, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	%rdx, 40(%rax)
	movq	-24(%rbp), %rdx
	movq	%rdx, 56(%rax)
	jmp	LBB28_5
LBB28_4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	104(%rcx), %rdx
	movq	104(%rcx), %rsi
	movq	112(%rcx), %rdi
	subq	$1, %rdi
	addq	%rdi, %rsi
	movq	%rax, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	%rdx, 40(%rax)
	movq	-48(%rbp), %rdx
	movq	%rdx, 56(%rax)
LBB28_5:
	jmp	LBB28_7
LBB28_6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-72(%rbp), %rcx
	movq	%rcx, 56(%rax)
LBB28_7:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	$16, 396(%rax)
LBB28_8:
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp131:
	.cfi_def_cfa_offset 16
Ltmp132:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp133:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4openEPKcj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp134:
	.cfi_def_cfa_offset 16
Ltmp135:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp136:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-8(%rbp), %rsi
	movq	$0, -32(%rbp)
	cmpq	$0, 120(%rsi)
	movq	%rsi, -48(%rbp)         ## 8-byte Spill
	jne	LBB30_25
## BB#1:
	movq	-48(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movl	-20(%rbp), %ecx
	andl	$-3, %ecx
	movl	%ecx, %edx
	subl	$1, %edx
	movl	%ecx, -52(%rbp)         ## 4-byte Spill
	movl	%edx, -56(%rbp)         ## 4-byte Spill
	je	LBB30_3
	jmp	LBB30_26
LBB30_26:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$5, %eax
	movl	%eax, -60(%rbp)         ## 4-byte Spill
	je	LBB30_9
	jmp	LBB30_27
LBB30_27:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$8, %eax
	movl	%eax, -64(%rbp)         ## 4-byte Spill
	je	LBB30_4
	jmp	LBB30_28
LBB30_28:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$9, %eax
	movl	%eax, -68(%rbp)         ## 4-byte Spill
	je	LBB30_7
	jmp	LBB30_29
LBB30_29:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$12, %eax
	movl	%eax, -72(%rbp)         ## 4-byte Spill
	je	LBB30_10
	jmp	LBB30_30
LBB30_30:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$13, %eax
	movl	%eax, -76(%rbp)         ## 4-byte Spill
	je	LBB30_13
	jmp	LBB30_31
LBB30_31:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$16, %eax
	movl	%eax, -80(%rbp)         ## 4-byte Spill
	je	LBB30_2
	jmp	LBB30_32
LBB30_32:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$17, %eax
	movl	%eax, -84(%rbp)         ## 4-byte Spill
	je	LBB30_3
	jmp	LBB30_33
LBB30_33:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$20, %eax
	movl	%eax, -88(%rbp)         ## 4-byte Spill
	je	LBB30_8
	jmp	LBB30_34
LBB30_34:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$21, %eax
	movl	%eax, -92(%rbp)         ## 4-byte Spill
	je	LBB30_9
	jmp	LBB30_35
LBB30_35:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$24, %eax
	movl	%eax, -96(%rbp)         ## 4-byte Spill
	je	LBB30_5
	jmp	LBB30_36
LBB30_36:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$25, %eax
	movl	%eax, -100(%rbp)        ## 4-byte Spill
	je	LBB30_7
	jmp	LBB30_37
LBB30_37:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$28, %eax
	movl	%eax, -104(%rbp)        ## 4-byte Spill
	je	LBB30_11
	jmp	LBB30_38
LBB30_38:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$29, %eax
	movl	%eax, -108(%rbp)        ## 4-byte Spill
	je	LBB30_13
	jmp	LBB30_39
LBB30_39:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$48, %eax
	movl	%eax, -112(%rbp)        ## 4-byte Spill
	je	LBB30_2
	jmp	LBB30_40
LBB30_40:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$52, %eax
	movl	%eax, -116(%rbp)        ## 4-byte Spill
	je	LBB30_8
	jmp	LBB30_41
LBB30_41:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$56, %eax
	movl	%eax, -120(%rbp)        ## 4-byte Spill
	je	LBB30_6
	jmp	LBB30_42
LBB30_42:
	movl	-52(%rbp), %eax         ## 4-byte Reload
	subl	$60, %eax
	movl	%eax, -124(%rbp)        ## 4-byte Spill
	je	LBB30_12
	jmp	LBB30_14
LBB30_2:
	leaq	L_.str.3(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_3:
	leaq	L_.str.4(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_4:
	leaq	L_.str.5(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_5:
	leaq	L_.str.6(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_6:
	leaq	L_.str.7(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_7:
	leaq	L_.str.8(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_8:
	leaq	L_.str.9(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_9:
	leaq	L_.str.10(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_10:
	leaq	L_.str.11(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_11:
	leaq	L_.str.12(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_12:
	leaq	L_.str.13(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_13:
	leaq	L_.str.14(%rip), %rax
	movq	%rax, -40(%rbp)
	jmp	LBB30_15
LBB30_14:
	movq	$0, -32(%rbp)
LBB30_15:
	cmpq	$0, -32(%rbp)
	je	LBB30_24
## BB#16:
	movq	-16(%rbp), %rdi
	movq	-40(%rbp), %rsi
	callq	_fopen
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, 120(%rsi)
	cmpq	$0, 120(%rsi)
	je	LBB30_22
## BB#17:
	movl	-20(%rbp), %eax
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	movl	%eax, 392(%rcx)
	movl	-20(%rbp), %eax
	andl	$2, %eax
	cmpl	$0, %eax
	je	LBB30_21
## BB#18:
	xorl	%eax, %eax
	movl	%eax, %esi
	movl	$2, %edx
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	movq	120(%rcx), %rdi
	callq	_fseek
	cmpl	$0, %eax
	je	LBB30_20
## BB#19:
	movq	-48(%rbp), %rax         ## 8-byte Reload
	movq	120(%rax), %rdi
	callq	_fclose
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	movq	$0, 120(%rdi)
	movq	$0, -32(%rbp)
	movl	%eax, -128(%rbp)        ## 4-byte Spill
LBB30_20:
	jmp	LBB30_21
LBB30_21:
	jmp	LBB30_23
LBB30_22:
	movq	$0, -32(%rbp)
LBB30_23:
	jmp	LBB30_24
LBB30_24:
	jmp	LBB30_25
LBB30_25:
	movq	-32(%rbp), %rax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	.weak_def_can_be_hidden	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
	.align	4, 0x90
__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev: ## @_ZNSt3__113basic_filebufIcNS_11char_traitsIcEEEC2Ev
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp146:
	.cfi_def_cfa_offset 16
Ltmp147:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp148:
	.cfi_def_cfa_register %rbp
	subq	$160, %rsp
	movq	%rdi, -48(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -88(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
	leaq	-56(%rbp), %rdi
	xorl	%ecx, %ecx
	movl	$128, %edx
	movl	%edx, %eax
	movq	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rsi
	addq	$16, %rsi
	movq	-88(%rbp), %r8          ## 8-byte Reload
	movq	%rsi, (%r8)
	movq	$0, 64(%r8)
	movq	$0, 72(%r8)
	movq	$0, 80(%r8)
	movq	$0, 96(%r8)
	movq	$0, 104(%r8)
	movq	$0, 112(%r8)
	movq	$0, 120(%r8)
	movq	$0, 128(%r8)
	addq	$136, %r8
	movq	%rdi, -96(%rbp)         ## 8-byte Spill
	movq	%r8, %rdi
	movl	%ecx, %esi
	movq	%rax, %rdx
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	movl	%ecx, -108(%rbp)        ## 4-byte Spill
	callq	_memset
	movq	-88(%rbp), %rax         ## 8-byte Reload
	addq	$264, %rax              ## imm = 0x108
	movq	%rax, %rdi
	movl	-108(%rbp), %esi        ## 4-byte Reload
	movq	-104(%rbp), %rdx        ## 8-byte Reload
	callq	_memset
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movl	$0, 392(%rax)
	movl	$0, 396(%rax)
	movb	$0, 400(%rax)
	movb	$0, 401(%rax)
	movb	$0, 402(%rax)
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	addq	$8, %rax
	movq	-96(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16localeC1ERKS0_
## BB#1:
	leaq	-56(%rbp), %rax
	movq	%rax, -32(%rbp)
Ltmp137:
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9has_facetERNS0_2idE
Ltmp138:
	movb	%al, -109(%rbp)         ## 1-byte Spill
	jmp	LBB31_3
LBB31_2:
Ltmp139:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -116(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB31_3:                                ## %_ZNSt3__19has_facetINS_7codecvtIcc11__mbstate_tEEEEbRKNS_6localeE.exit
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-109(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB31_4
	jmp	LBB31_10
LBB31_4:
	leaq	-80(%rbp), %rdi
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$8, %rax
	movq	%rax, %rsi
	callq	__ZNSt3__16localeC1ERKS0_
## BB#5:
	leaq	-80(%rbp), %rax
	movq	%rax, -16(%rbp)
Ltmp140:
	movq	__ZNSt3__17codecvtIcc11__mbstate_tE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp141:
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	jmp	LBB31_6
LBB31_6:                                ## %_ZNSt3__19use_facetINS_7codecvtIcc11__mbstate_tEEEERKT_RKNS_6localeE.exit
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)        ## 8-byte Spill
## BB#7:
	leaq	-80(%rbp), %rdi
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	-136(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, 128(%rax)
	callq	__ZNSt3__16localeD1Ev
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	128(%rax), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rdi
	movq	%rdi, -144(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	-144(%rbp), %rcx        ## 8-byte Reload
	callq	*56(%rcx)
	andb	$1, %al
	movq	-88(%rbp), %rcx         ## 8-byte Reload
	movb	%al, 402(%rcx)
	jmp	LBB31_10
LBB31_8:
Ltmp145:
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	jmp	LBB31_12
LBB31_9:
Ltmp142:
	leaq	-80(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -64(%rbp)
	movl	%ecx, -68(%rbp)
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB31_12
LBB31_10:
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	24(%rcx), %rcx
Ltmp143:
	xorl	%edx, %edx
	movl	%edx, %esi
	movl	$4096, %edx             ## imm = 0x1000
                                        ## 
	movq	%rax, %rdi
	callq	*%rcx
Ltmp144:
	movq	%rax, -152(%rbp)        ## 8-byte Spill
	jmp	LBB31_11
LBB31_11:
	addq	$160, %rsp
	popq	%rbp
	retq
LBB31_12:
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
## BB#13:
	movq	-64(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table31:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset43 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset43
Lset44 = Ltmp137-Lfunc_begin4           ##   Call between Lfunc_begin4 and Ltmp137
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp137-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset45
Lset46 = Ltmp138-Ltmp137                ##   Call between Ltmp137 and Ltmp138
	.long	Lset46
Lset47 = Ltmp139-Lfunc_begin4           ##     jumps to Ltmp139
	.long	Lset47
	.byte	1                       ##   On action: 1
Lset48 = Ltmp140-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset48
Lset49 = Ltmp141-Ltmp140                ##   Call between Ltmp140 and Ltmp141
	.long	Lset49
Lset50 = Ltmp142-Lfunc_begin4           ##     jumps to Ltmp142
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp141-Lfunc_begin4           ## >> Call Site 4 <<
	.long	Lset51
Lset52 = Ltmp143-Ltmp141                ##   Call between Ltmp141 and Ltmp143
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset53 = Ltmp143-Lfunc_begin4           ## >> Call Site 5 <<
	.long	Lset53
Lset54 = Ltmp144-Ltmp143                ##   Call between Ltmp143 and Ltmp144
	.long	Lset54
Lset55 = Ltmp145-Lfunc_begin4           ##     jumps to Ltmp145
	.long	Lset55
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp144-Lfunc_begin4           ## >> Call Site 6 <<
	.long	Lset56
Lset57 = Lfunc_end4-Ltmp144             ##   Call between Ltmp144 and Lfunc_end4
	.long	Lset57
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp170:
	.cfi_def_cfa_offset 16
Ltmp171:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp172:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp149:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp150:
	jmp	LBB32_1
LBB32_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB32_3
	jmp	LBB32_26
LBB32_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB32_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB32_7
LBB32_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB32_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB32_8
	jmp	LBB32_13
LBB32_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp152:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp153:
	jmp	LBB32_9
LBB32_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp154:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp155:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB32_10
LBB32_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp156:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp157:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB32_12
LBB32_11:
Ltmp158:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB32_21
LBB32_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB32_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp159:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp160:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB32_15
LBB32_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB32_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp161:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp162:
	jmp	LBB32_17
LBB32_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB32_18
LBB32_18:
	jmp	LBB32_25
LBB32_19:
Ltmp151:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB32_22
LBB32_20:
Ltmp163:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB32_21
LBB32_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB32_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp164:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp165:
	jmp	LBB32_23
LBB32_23:
	callq	___cxa_end_catch
LBB32_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB32_25:
	jmp	LBB32_26
LBB32_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB32_24
LBB32_27:
Ltmp166:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp167:
	callq	___cxa_end_catch
Ltmp168:
	jmp	LBB32_28
LBB32_28:
	jmp	LBB32_29
LBB32_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB32_30:
Ltmp169:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table32:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset58 = Ltmp149-Lfunc_begin5           ## >> Call Site 1 <<
	.long	Lset58
Lset59 = Ltmp150-Ltmp149                ##   Call between Ltmp149 and Ltmp150
	.long	Lset59
Lset60 = Ltmp151-Lfunc_begin5           ##     jumps to Ltmp151
	.long	Lset60
	.byte	5                       ##   On action: 3
Lset61 = Ltmp152-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset61
Lset62 = Ltmp153-Ltmp152                ##   Call between Ltmp152 and Ltmp153
	.long	Lset62
Lset63 = Ltmp163-Lfunc_begin5           ##     jumps to Ltmp163
	.long	Lset63
	.byte	5                       ##   On action: 3
Lset64 = Ltmp154-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset64
Lset65 = Ltmp157-Ltmp154                ##   Call between Ltmp154 and Ltmp157
	.long	Lset65
Lset66 = Ltmp158-Lfunc_begin5           ##     jumps to Ltmp158
	.long	Lset66
	.byte	3                       ##   On action: 2
Lset67 = Ltmp159-Lfunc_begin5           ## >> Call Site 4 <<
	.long	Lset67
Lset68 = Ltmp162-Ltmp159                ##   Call between Ltmp159 and Ltmp162
	.long	Lset68
Lset69 = Ltmp163-Lfunc_begin5           ##     jumps to Ltmp163
	.long	Lset69
	.byte	5                       ##   On action: 3
Lset70 = Ltmp162-Lfunc_begin5           ## >> Call Site 5 <<
	.long	Lset70
Lset71 = Ltmp164-Ltmp162                ##   Call between Ltmp162 and Ltmp164
	.long	Lset71
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset72 = Ltmp164-Lfunc_begin5           ## >> Call Site 6 <<
	.long	Lset72
Lset73 = Ltmp165-Ltmp164                ##   Call between Ltmp164 and Ltmp165
	.long	Lset73
Lset74 = Ltmp166-Lfunc_begin5           ##     jumps to Ltmp166
	.long	Lset74
	.byte	0                       ##   On action: cleanup
Lset75 = Ltmp165-Lfunc_begin5           ## >> Call Site 7 <<
	.long	Lset75
Lset76 = Ltmp167-Ltmp165                ##   Call between Ltmp165 and Ltmp167
	.long	Lset76
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset77 = Ltmp167-Lfunc_begin5           ## >> Call Site 8 <<
	.long	Lset77
Lset78 = Ltmp168-Ltmp167                ##   Call between Ltmp167 and Ltmp168
	.long	Lset78
Lset79 = Ltmp169-Lfunc_begin5           ##     jumps to Ltmp169
	.long	Lset79
	.byte	5                       ##   On action: 3
Lset80 = Ltmp168-Lfunc_begin5           ## >> Call Site 9 <<
	.long	Lset80
Lset81 = Lfunc_end5-Ltmp168             ##   Call between Ltmp168 and Lfunc_end5
	.long	Lset81
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp173:
	.cfi_def_cfa_offset 16
Ltmp174:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp175:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp179:
	.cfi_def_cfa_offset 16
Ltmp180:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp181:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB34_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB34_26
LBB34_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB34_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB34_5
LBB34_4:
	movq	$0, -368(%rbp)
LBB34_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB34_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB34_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB34_26
LBB34_8:
	jmp	LBB34_9
LBB34_9:
	cmpq	$0, -368(%rbp)
	jle	LBB34_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB34_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB34_13
LBB34_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB34_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp176:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp177:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB34_14
LBB34_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB34_15
LBB34_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB34_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB34_19
LBB34_17:
Ltmp178:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB34_27
LBB34_18:
	movl	$0, -416(%rbp)
LBB34_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB34_20
	jmp	LBB34_29
LBB34_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB34_26
	jmp	LBB34_28
LBB34_20:
	jmp	LBB34_21
LBB34_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB34_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB34_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB34_26
LBB34_24:
	jmp	LBB34_25
LBB34_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB34_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB34_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB34_28:
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table34:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset82 = Lfunc_begin6-Lfunc_begin6      ## >> Call Site 1 <<
	.long	Lset82
Lset83 = Ltmp176-Lfunc_begin6           ##   Call between Lfunc_begin6 and Ltmp176
	.long	Lset83
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset84 = Ltmp176-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset84
Lset85 = Ltmp177-Ltmp176                ##   Call between Ltmp176 and Ltmp177
	.long	Lset85
Lset86 = Ltmp178-Lfunc_begin6           ##     jumps to Ltmp178
	.long	Lset86
	.byte	0                       ##   On action: cleanup
Lset87 = Ltmp177-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset87
Lset88 = Lfunc_end6-Ltmp177             ##   Call between Ltmp177 and Lfunc_end6
	.long	Lset88
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"TodayTime.txt"

L_.str.1:                               ## @.str.1
	.asciz	"The average call time is "

L_.str.2:                               ## @.str.2
	.asciz	"didn't write"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE ## @_ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.align	3
__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE:
	.quad	416
	.quad	0
	.quad	__ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev
	.quad	-416
	.quad	-416
	.quad	__ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__114basic_ofstreamIcNS_11char_traitsIcEEED0Ev

	.globl	__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE ## @_ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTTNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE:
	.quad	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE+24
	.quad	__ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE+24
	.quad	__ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE+64
	.quad	__ZTVNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE+64

	.globl	__ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE ## @_ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE
	.align	4
__ZTCNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE0_NS_13basic_ostreamIcS2_EE:
	.quad	416
	.quad	0
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.quad	-416
	.quad	-416
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE ## @_ZTSNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTSNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTSNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE:
	.asciz	"NSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE ## @_ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.align	4
__ZTINSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__114basic_ofstreamIcNS_11char_traitsIcEEEE
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	3
__ZTVNSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.quad	0
	.quad	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEED0Ev
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__113basic_filebufIcNS_11char_traitsIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	4
__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.asciz	"NSt3__113basic_filebufIcNS_11char_traitsIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE ## @_ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.weak_definition	__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.align	4
__ZTINSt3__113basic_filebufIcNS_11char_traitsIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__113basic_filebufIcNS_11char_traitsIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE

	.section	__TEXT,__cstring,cstring_literals
L_.str.3:                               ## @.str.3
	.asciz	"w"

L_.str.4:                               ## @.str.4
	.asciz	"a"

L_.str.5:                               ## @.str.5
	.asciz	"r"

L_.str.6:                               ## @.str.6
	.asciz	"r+"

L_.str.7:                               ## @.str.7
	.asciz	"w+"

L_.str.8:                               ## @.str.8
	.asciz	"a+"

L_.str.9:                               ## @.str.9
	.asciz	"wb"

L_.str.10:                              ## @.str.10
	.asciz	"ab"

L_.str.11:                              ## @.str.11
	.asciz	"rb"

L_.str.12:                              ## @.str.12
	.asciz	"r+b"

L_.str.13:                              ## @.str.13
	.asciz	"w+b"

L_.str.14:                              ## @.str.14
	.asciz	"a+b"


.subsections_via_symbols
