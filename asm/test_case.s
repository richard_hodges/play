	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init:                 ## @__cxx_global_var_init
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEv
	movq	%rax, __ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEv
	.align	4, 0x90
__ZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEv: ## @_ZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEv
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rax
	cmpb	$0, (%rax)
	jne	LBB1_4
## BB#1:
	movq	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	callq	___cxa_guard_acquire
	cmpl	$0, %eax
	je	LBB1_4
## BB#2:
Ltmp3:
	movq	__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	callq	__ZN5boost9unit_test15unit_test_log_tC1Ev
Ltmp4:
	jmp	LBB1_3
LBB1_3:
	movq	__ZN5boost9unit_test15unit_test_log_tD1Ev@GOTPCREL(%rip), %rax
	movq	__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rcx
	movq	___dso_handle@GOTPCREL(%rip), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	___cxa_atexit
	movq	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	movl	%eax, -16(%rbp)         ## 4-byte Spill
	callq	___cxa_guard_release
LBB1_4:
	movq	__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rax
	addq	$16, %rsp
	popq	%rbp
	retq
LBB1_5:
Ltmp5:
	movq	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	movl	%edx, %ecx
	movq	%rax, -8(%rbp)
	movl	%ecx, -12(%rbp)
	callq	___cxa_guard_abort
## BB#6:
	movq	-8(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset0 = Ltmp3-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset1
Lset2 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp4-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Lfunc_end0-Ltmp4                ##   Call between Ltmp4 and Lfunc_end0
	.long	Lset4
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init.1:               ## @__cxx_global_var_init.1
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp14:
	.cfi_def_cfa_offset 16
Ltmp15:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp16:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	leaq	L_.str(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1EPS2_
	leaq	L_.str.2(%rip), %rsi
	leaq	-64(%rbp), %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1EPS2_
	movq	-48(%rbp), %rsi
	movq	-40(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	movl	$46, %eax
	movl	%eax, %r9d
	leaq	-32(%rbp), %rdi
	movq	%rdi, -88(%rbp)         ## 8-byte Spill
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC1ENS0_13basic_cstringIKcEESG_m
Ltmp9:
	callq	__ZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEv
Ltmp10:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB2_1
LBB2_1:
Ltmp11:
	leaq	__ZL22MyTestCase_registrar46(%rip), %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	movq	-96(%rbp), %rdx         ## 8-byte Reload
	callq	__ZN5boost9unit_test9ut_detail24auto_test_unit_registrarC1ERKNS0_19test_unit_generatorERNS0_9decorator9collectorE
Ltmp12:
	jmp	LBB2_2
LBB2_2:
	leaq	-32(%rbp), %rdi
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	addq	$96, %rsp
	popq	%rbp
	retq
LBB2_3:
Ltmp13:
	leaq	-32(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -72(%rbp)
	movl	%ecx, -76(%rbp)
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
## BB#4:
	movq	-72(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset5 = Lfunc_begin1-Lfunc_begin1       ## >> Call Site 1 <<
	.long	Lset5
Lset6 = Ltmp9-Lfunc_begin1              ##   Call between Lfunc_begin1 and Ltmp9
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset7 = Ltmp9-Lfunc_begin1              ## >> Call Site 2 <<
	.long	Lset7
Lset8 = Ltmp12-Ltmp9                    ##   Call between Ltmp9 and Ltmp12
	.long	Lset8
Lset9 = Ltmp13-Lfunc_begin1             ##     jumps to Ltmp13
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp12-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset10
Lset11 = Lfunc_end1-Ltmp12              ##   Call between Ltmp12 and Lfunc_end1
	.long	Lset11
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test13basic_cstringIKcEC1EPS2_
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC1EPS2_
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC1EPS2_: ## @_ZN5boost9unit_test13basic_cstringIKcEC1EPS2_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp17:
	.cfi_def_cfa_offset 16
Ltmp18:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp19:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC2EPS2_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC1ENS0_13basic_cstringIKcEESG_m
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC1ENS0_13basic_cstringIKcEESG_m
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC1ENS0_13basic_cstringIKcEESG_m: ## @_ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC1ENS0_13basic_cstringIKcEESG_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp20:
	.cfi_def_cfa_offset 16
Ltmp21:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp22:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -24(%rbp)
	movq	%rdi, -40(%rbp)
	movq	%r9, -48(%rbp)
	movq	-40(%rbp), %rdi
	movq	-48(%rbp), %r9
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %r8
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC2ENS0_13basic_cstringIKcEESG_m
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEv
	.align	4, 0x90
__ZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEv: ## @_ZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEv
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp26:
	.cfi_def_cfa_offset 16
Ltmp27:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp28:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rax
	cmpb	$0, (%rax)
	jne	LBB5_4
## BB#1:
	movq	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	callq	___cxa_guard_acquire
	cmpl	$0, %eax
	je	LBB5_4
## BB#2:
Ltmp23:
	movq	__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	callq	__ZN5boost9unit_test9decorator9collectorC1Ev
Ltmp24:
	jmp	LBB5_3
LBB5_3:
	movq	__ZN5boost9unit_test9decorator9collectorD1Ev@GOTPCREL(%rip), %rax
	movq	__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rcx
	movq	___dso_handle@GOTPCREL(%rip), %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	___cxa_atexit
	movq	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	movl	%eax, -16(%rbp)         ## 4-byte Spill
	callq	___cxa_guard_release
LBB5_4:
	movq	__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rax
	addq	$16, %rsp
	popq	%rbp
	retq
LBB5_5:
Ltmp25:
	movq	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst@GOTPCREL(%rip), %rdi
	movl	%edx, %ecx
	movq	%rax, -8(%rbp)
	movl	%ecx, -12(%rbp)
	callq	___cxa_guard_abort
## BB#6:
	movq	-8(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset12 = Ltmp23-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset12
Lset13 = Ltmp24-Ltmp23                  ##   Call between Ltmp23 and Ltmp24
	.long	Lset13
Lset14 = Ltmp25-Lfunc_begin2            ##     jumps to Ltmp25
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp24-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset15
Lset16 = Lfunc_end2-Ltmp24              ##   Call between Ltmp24 and Lfunc_end2
	.long	Lset16
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev: ## @_ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp29:
	.cfi_def_cfa_offset 16
Ltmp30:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp31:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED2Ev
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED2Ev: ## @_ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp32:
	.cfi_def_cfa_offset 16
Ltmp33:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp34:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$8, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZN5boost9unit_test19test_unit_generatorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev
	.align	4, 0x90
__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev: ## @_ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp35:
	.cfi_def_cfa_offset 16
Ltmp36:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp37:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test19test_unit_generatorD2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test19test_unit_generatorD2Ev
	.align	4, 0x90
__ZN5boost9unit_test19test_unit_generatorD2Ev: ## @_ZN5boost9unit_test19test_unit_generatorD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp38:
	.cfi_def_cfa_offset 16
Ltmp39:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp40:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEE4nextEv
	.weak_def_can_be_hidden	__ZNK5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEE4nextEv
	.align	4, 0x90
__ZNK5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEE4nextEv: ## @_ZNK5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEE4nextEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp41:
	.cfi_def_cfa_offset 16
Ltmp42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp43:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	%rdi, %rax
	addq	$8, %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rdi, -80(%rbp)         ## 8-byte Spill
	jne	LBB10_2
## BB#1:
	movq	$0, -56(%rbp)
	jmp	LBB10_3
LBB10_2:
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	%rax, -72(%rbp)
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9pop_frontEv
	movq	-72(%rbp), %rax
	movq	%rax, -56(%rbp)
LBB10_3:
	movq	-56(%rbp), %rax
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED0Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED0Ev
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED0Ev: ## @_ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.align	4, 0x90
__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev: ## @_ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.align	4, 0x90
__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev: ## @_ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp50:
	.cfi_def_cfa_offset 16
Ltmp51:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp52:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE5clearEv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE5clearEv
	.weak_def_can_be_hidden	__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE5clearEv
	.align	4, 0x90
__ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE5clearEv: ## @_ZNSt3__110__list_impIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE5clearEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -232(%rbp)
	movq	-232(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	cmpq	$0, (%rax)
	movq	%rdi, -272(%rbp)        ## 8-byte Spill
	je	LBB14_6
## BB#1:
	movq	-272(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	8(%rax), %rcx
	movq	%rcx, -248(%rbp)
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-248(%rbp), %rcx
	movq	-256(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rcx, -8(%rbp)
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rcx, 8(%rdx)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-16(%rbp), %rdx
	movq	8(%rdx), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	$0, (%rcx)
LBB14_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-248(%rbp), %rax
	cmpq	-256(%rbp), %rax
	je	LBB14_5
## BB#3:                                ##   in Loop: Header=BB14_2 Depth=1
	movq	-248(%rbp), %rax
	movq	%rax, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, -248(%rbp)
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rax, -72(%rbp)
	movq	%rcx, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
## BB#4:                                ##   in Loop: Header=BB14_2 Depth=1
	movq	-240(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rax, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	$1, -168(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rdx, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rdi
	callq	__ZdlPv
	jmp	LBB14_2
LBB14_5:
	jmp	LBB14_6
LBB14_6:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9pop_frontEv
	.weak_def_can_be_hidden	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9pop_frontEv
	.align	4, 0x90
__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9pop_frontEv: ## @_ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9pop_frontEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp56:
	.cfi_def_cfa_offset 16
Ltmp57:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp58:
	.cfi_def_cfa_register %rbp
	subq	$208, %rsp
	movq	%rdi, -184(%rbp)
	movq	-184(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	8(%rdi), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	8(%rax), %rax
	movq	-88(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 8(%rcx)
	movq	-88(%rbp), %rax
	movq	(%rax), %rax
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rax, (%rcx)
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rcx
	addq	$-1, %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	%rax, -40(%rbp)
	movq	%rcx, -48(%rbp)
	movq	-192(%rbp), %rax
	movq	-200(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	$1, -152(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdi
	movq	%rax, -112(%rbp)
	movq	%rcx, -120(%rbp)
	movq	%rdi, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rdi
	callq	__ZdlPv
	addq	$208, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test15unit_test_log_tC1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test15unit_test_log_tC1Ev
	.align	4, 0x90
__ZN5boost9unit_test15unit_test_log_tC1Ev: ## @_ZN5boost9unit_test15unit_test_log_tC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp59:
	.cfi_def_cfa_offset 16
Ltmp60:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp61:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test15unit_test_log_tC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test15unit_test_log_tD1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test15unit_test_log_tD1Ev
	.align	4, 0x90
__ZN5boost9unit_test15unit_test_log_tD1Ev: ## @_ZN5boost9unit_test15unit_test_log_tD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp62:
	.cfi_def_cfa_offset 16
Ltmp63:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp64:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test15unit_test_log_tD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test15unit_test_log_tC2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test15unit_test_log_tC2Ev
	.align	4, 0x90
__ZN5boost9unit_test15unit_test_log_tC2Ev: ## @_ZN5boost9unit_test15unit_test_log_tC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp65:
	.cfi_def_cfa_offset 16
Ltmp66:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp67:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN5boost9unit_test13test_observerC2Ev
	movq	__ZTVN5boost9unit_test15unit_test_log_tE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, (%rdi)
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observerC2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observerC2Ev
	.align	4, 0x90
__ZN5boost9unit_test13test_observerC2Ev: ## @_ZN5boost9unit_test13test_observerC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp68:
	.cfi_def_cfa_offset 16
Ltmp69:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp70:
	.cfi_def_cfa_register %rbp
	movq	__ZTVN5boost9unit_test13test_observerE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer10test_startEm
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer10test_startEm
	.align	4, 0x90
__ZN5boost9unit_test13test_observer10test_startEm: ## @_ZN5boost9unit_test13test_observer10test_startEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp71:
	.cfi_def_cfa_offset 16
Ltmp72:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp73:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer11test_finishEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer11test_finishEv
	.align	4, 0x90
__ZN5boost9unit_test13test_observer11test_finishEv: ## @_ZN5boost9unit_test13test_observer11test_finishEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp74:
	.cfi_def_cfa_offset 16
Ltmp75:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp76:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer12test_abortedEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer12test_abortedEv
	.align	4, 0x90
__ZN5boost9unit_test13test_observer12test_abortedEv: ## @_ZN5boost9unit_test13test_observer12test_abortedEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp77:
	.cfi_def_cfa_offset 16
Ltmp78:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp79:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer15test_unit_startERKNS0_9test_unitE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer15test_unit_startERKNS0_9test_unitE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer15test_unit_startERKNS0_9test_unitE: ## @_ZN5boost9unit_test13test_observer15test_unit_startERKNS0_9test_unitE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp80:
	.cfi_def_cfa_offset 16
Ltmp81:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp82:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer16test_unit_finishERKNS0_9test_unitEm
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer16test_unit_finishERKNS0_9test_unitEm
	.align	4, 0x90
__ZN5boost9unit_test13test_observer16test_unit_finishERKNS0_9test_unitEm: ## @_ZN5boost9unit_test13test_observer16test_unit_finishERKNS0_9test_unitEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp83:
	.cfi_def_cfa_offset 16
Ltmp84:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp85:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitENS0_13basic_cstringIKcEE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitENS0_13basic_cstringIKcEE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitENS0_13basic_cstringIKcEE: ## @_ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitENS0_13basic_cstringIKcEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp86:
	.cfi_def_cfa_offset 16
Ltmp87:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp88:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdx, -16(%rbp)
	movq	%rcx, -8(%rbp)
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	48(%rdx), %rdx
	movq	-32(%rbp), %rsi
	movq	%rcx, %rdi
	callq	*%rdx
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitE: ## @_ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp89:
	.cfi_def_cfa_offset 16
Ltmp90:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp91:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer17test_unit_abortedERKNS0_9test_unitE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer17test_unit_abortedERKNS0_9test_unitE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer17test_unit_abortedERKNS0_9test_unitE: ## @_ZN5boost9unit_test13test_observer17test_unit_abortedERKNS0_9test_unitE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp92:
	.cfi_def_cfa_offset 16
Ltmp93:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp94:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer16assertion_resultENS0_16assertion_resultE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer16assertion_resultENS0_16assertion_resultE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer16assertion_resultENS0_16assertion_resultE: ## @_ZN5boost9unit_test13test_observer16assertion_resultENS0_16assertion_resultE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp95:
	.cfi_def_cfa_offset 16
Ltmp96:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp97:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movq	-8(%rbp), %rdi
	movl	%esi, %eax
	testl	%esi, %esi
	movl	%eax, -16(%rbp)         ## 4-byte Spill
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	je	LBB29_2
	jmp	LBB29_6
LBB29_6:
	movl	-16(%rbp), %eax         ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -28(%rbp)         ## 4-byte Spill
	je	LBB29_1
	jmp	LBB29_7
LBB29_7:
	movl	-16(%rbp), %eax         ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -32(%rbp)         ## 4-byte Spill
	je	LBB29_3
	jmp	LBB29_4
LBB29_1:
	movl	$1, %esi
	movq	-24(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*88(%rcx)
	jmp	LBB29_5
LBB29_2:
	xorl	%esi, %esi
	movq	-24(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*88(%rcx)
	jmp	LBB29_5
LBB29_3:
	jmp	LBB29_5
LBB29_4:
	jmp	LBB29_5
LBB29_5:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer16exception_caughtERKNS_19execution_exceptionE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer16exception_caughtERKNS_19execution_exceptionE
	.align	4, 0x90
__ZN5boost9unit_test13test_observer16exception_caughtERKNS_19execution_exceptionE: ## @_ZN5boost9unit_test13test_observer16exception_caughtERKNS_19execution_exceptionE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp98:
	.cfi_def_cfa_offset 16
Ltmp99:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp100:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer8priorityEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer8priorityEv
	.align	4, 0x90
__ZN5boost9unit_test13test_observer8priorityEv: ## @_ZN5boost9unit_test13test_observer8priorityEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp101:
	.cfi_def_cfa_offset 16
Ltmp102:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp103:
	.cfi_def_cfa_register %rbp
	xorl	%eax, %eax
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observer16assertion_resultEb
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observer16assertion_resultEb
	.align	4, 0x90
__ZN5boost9unit_test13test_observer16assertion_resultEb: ## @_ZN5boost9unit_test13test_observer16assertion_resultEb
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp104:
	.cfi_def_cfa_offset 16
Ltmp105:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp106:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movq	%rdi, -8(%rbp)
	andb	$1, %al
	movb	%al, -9(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observerD1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observerD1Ev
	.align	4, 0x90
__ZN5boost9unit_test13test_observerD1Ev: ## @_ZN5boost9unit_test13test_observerD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp107:
	.cfi_def_cfa_offset 16
Ltmp108:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp109:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test13test_observerD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observerD0Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observerD0Ev
	.align	4, 0x90
__ZN5boost9unit_test13test_observerD0Ev: ## @_ZN5boost9unit_test13test_observerD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp110:
	.cfi_def_cfa_offset 16
Ltmp111:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp112:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN5boost9unit_test13test_observerD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13test_observerD2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test13test_observerD2Ev
	.align	4, 0x90
__ZN5boost9unit_test13test_observerD2Ev: ## @_ZN5boost9unit_test13test_observerD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp113:
	.cfi_def_cfa_offset 16
Ltmp114:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp115:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test15unit_test_log_tD2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test15unit_test_log_tD2Ev
	.align	4, 0x90
__ZN5boost9unit_test15unit_test_log_tD2Ev: ## @_ZN5boost9unit_test15unit_test_log_tD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp116:
	.cfi_def_cfa_offset 16
Ltmp117:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp118:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test13test_observerD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcEC2EPS2_
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC2EPS2_
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC2EPS2_: ## @_ZN5boost9unit_test13basic_cstringIKcEC2EPS2_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp119:
	.cfi_def_cfa_offset 16
Ltmp120:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp121:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	cmpq	$0, -16(%rbp)
	movq	%rsi, -24(%rbp)         ## 8-byte Spill
	je	LBB37_2
## BB#1:
	movq	-16(%rbp), %rax
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	jmp	LBB37_3
LBB37_2:
	callq	__ZN5boost9unit_test13basic_cstringIKcE8null_strEv
	movq	%rax, -32(%rbp)         ## 8-byte Spill
LBB37_3:
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	-24(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rdx
	cmpq	$0, -16(%rbp)
	movq	%rcx, -40(%rbp)         ## 8-byte Spill
	movq	%rdx, -48(%rbp)         ## 8-byte Spill
	je	LBB37_5
## BB#4:
	movq	-16(%rbp), %rdi
	callq	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE6lengthEPS3_
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	jmp	LBB37_6
LBB37_5:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -56(%rbp)         ## 8-byte Spill
	jmp	LBB37_6
LBB37_6:
	movq	-56(%rbp), %rax         ## 8-byte Reload
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	addq	%rax, %rcx
	movq	-40(%rbp), %rax         ## 8-byte Reload
	movq	%rcx, (%rax)
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcE8null_strEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcE8null_strEv
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcE8null_strEv: ## @_ZN5boost9unit_test13basic_cstringIKcE8null_strEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp122:
	.cfi_def_cfa_offset 16
Ltmp123:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp124:
	.cfi_def_cfa_register %rbp
	movq	__ZZN5boost9unit_test13basic_cstringIKcE8null_strEvE4null@GOTPCREL(%rip), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE6lengthEPS3_
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE6lengthEPS3_
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE6lengthEPS3_: ## @_ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE6lengthEPS3_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp125:
	.cfi_def_cfa_offset 16
Ltmp126:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp127:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movb	$0, -9(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -24(%rbp)
LBB39_1:                                ## =>This Inner Loop Header: Depth=1
	xorl	%esi, %esi
	movq	-24(%rbp), %rax
	movsbl	(%rax), %edi
	callq	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE2eqEcc
	xorb	$-1, %al
	testb	$1, %al
	jne	LBB39_2
	jmp	LBB39_3
LBB39_2:                                ##   in Loop: Header=BB39_1 Depth=1
	movq	-24(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -24(%rbp)
	jmp	LBB39_1
LBB39_3:
	movq	-24(%rbp), %rax
	movq	-8(%rbp), %rcx
	subq	%rcx, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE2eqEcc
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE2eqEcc
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE2eqEcc: ## @_ZN5boost9unit_test9ut_detail20bcs_char_traits_implIKcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp128:
	.cfi_def_cfa_offset 16
Ltmp129:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp130:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC2ENS0_13basic_cstringIKcEESG_m
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC2ENS0_13basic_cstringIKcEESG_m
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC2ENS0_13basic_cstringIKcEESG_m: ## @_ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEC2ENS0_13basic_cstringIKcEESG_m
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp136:
	.cfi_def_cfa_offset 16
Ltmp137:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp138:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rsi, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -184(%rbp)
	movq	%rdi, -200(%rbp)
	movq	%r9, -208(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -312(%rbp)        ## 8-byte Spill
	callq	__ZN5boost9unit_test19test_unit_generatorC2Ev
	movq	__ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-312(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, (%rdx)
	addq	$8, %rdx
	movq	%rdx, -160(%rbp)
	movq	%rdx, -152(%rbp)
	movq	%rdx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	%rdx, 8(%rcx)
	movq	%rdx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdx, 16(%rcx)
	addq	$24, %rcx
	movq	%rcx, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rcx
	leaq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-56(%rbp), %rsi
	movq	%rcx, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	leaq	-32(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, (%rcx)
	movq	-176(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movq	%rcx, -272(%rbp)
	movq	-192(%rbp), %rcx
	movq	-184(%rbp), %rsi
	movq	%rsi, -280(%rbp)
	movq	%rcx, -288(%rbp)
	movq	-208(%rbp), %r9
	movq	-272(%rbp), %rsi
	movq	-264(%rbp), %rcx
	movq	-288(%rbp), %rdi
	movq	-280(%rbp), %r8
Ltmp131:
	movq	%rsp, %rax
	movq	-312(%rbp), %r10        ## 8-byte Reload
	movq	%r10, (%rax)
	leaq	-256(%rbp), %rax
	movq	%rdi, -320(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rdx, -328(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	callq	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC1ENS0_13basic_cstringIKcEESI_mRSE_
Ltmp132:
	jmp	LBB41_1
LBB41_1:
Ltmp133:
	movq	-216(%rbp), %rax
	movq	%rsp, %rcx
	movq	%rax, 40(%rcx)
	movq	-224(%rbp), %rax
	movq	%rax, 32(%rcx)
	movq	-232(%rbp), %rax
	movq	%rax, 24(%rcx)
	movq	-240(%rbp), %rax
	movq	%rax, 16(%rcx)
	movq	-256(%rbp), %rax
	movq	-248(%rbp), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rax, (%rcx)
	xorl	%esi, %esi
	movl	%esi, %eax
	movq	%rax, %rdi
	movq	%rax, %rsi
	callq	__ZN5boost3mpl8for_eachINS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naES8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_EENS0_13make_identityINS7_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSF_22template_test_case_genI18MyTestCase_invokerS9_EESI_EEEEvT1_PT_PT0_
Ltmp134:
	jmp	LBB41_2
LBB41_2:
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB41_3:
Ltmp135:
	movl	%edx, %ecx
	movq	%rax, -296(%rbp)
	movl	%ecx, -300(%rbp)
	movq	-328(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEED1Ev
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZN5boost9unit_test19test_unit_generatorD2Ev
## BB#4:
	movq	-296(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table41:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset17 = Ltmp131-Lfunc_begin3           ## >> Call Site 1 <<
	.long	Lset17
Lset18 = Ltmp134-Ltmp131                ##   Call between Ltmp131 and Ltmp134
	.long	Lset18
Lset19 = Ltmp135-Lfunc_begin3           ##     jumps to Ltmp135
	.long	Lset19
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp134-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset20
Lset21 = Lfunc_end3-Ltmp134             ##   Call between Ltmp134 and Lfunc_end3
	.long	Lset21
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test19test_unit_generatorC2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test19test_unit_generatorC2Ev
	.align	4, 0x90
__ZN5boost9unit_test19test_unit_generatorC2Ev: ## @_ZN5boost9unit_test19test_unit_generatorC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp139:
	.cfi_def_cfa_offset 16
Ltmp140:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp141:
	.cfi_def_cfa_register %rbp
	movq	__ZTVN5boost9unit_test19test_unit_generatorE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost3mpl8for_eachINS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naES8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_EENS0_13make_identityINS7_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSF_22template_test_case_genI18MyTestCase_invokerS9_EESI_EEEEvT1_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost3mpl8for_eachINS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naES8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_EENS0_13make_identityINS7_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSF_22template_test_case_genI18MyTestCase_invokerS9_EESI_EEEEvT1_PT_PT0_
	.align	4, 0x90
__ZN5boost3mpl8for_eachINS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naES8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_EENS0_13make_identityINS7_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSF_22template_test_case_genI18MyTestCase_invokerS9_EESI_EEEEvT1_PT_PT0_: ## @_ZN5boost3mpl8for_eachINS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naES8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_S8_EENS0_13make_identityINS7_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSF_22template_test_case_genI18MyTestCase_invokerS9_EESI_EEEEvT1_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp142:
	.cfi_def_cfa_offset 16
Ltmp143:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp144:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	leaq	16(%rbp), %rax
	xorl	%ecx, %ecx
	movl	%ecx, %edx
	leaq	-64(%rbp), %r8
	movl	$48, %ecx
	movl	%ecx, %r9d
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%rdx, -72(%rbp)         ## 8-byte Spill
	movq	%r9, %rdx
	callq	_memcpy
	movq	-24(%rbp), %rax
	movq	%rsp, %rdx
	movq	%rax, 40(%rdx)
	movq	-32(%rbp), %rax
	movq	%rax, 32(%rdx)
	movq	-40(%rbp), %rax
	movq	%rax, 24(%rdx)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rdx)
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rsi
	movq	%rsi, 8(%rdx)
	movq	%rax, (%rdx)
	xorl	%ecx, %ecx
	movl	%ecx, %eax
	movq	%rax, %rdi
	movq	%rax, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost3mpl3aux13for_each_implILb0EE7executeINS0_6l_iterINS0_5list1I9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEENS5_INS0_5l_endEEENS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSL_22template_test_case_genI18MyTestCase_invokerNS0_4listISA_NSG_2naESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_EEEESO_EEEEvPT_PT0_PT1_T2_
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC1ENS0_13basic_cstringIKcEESI_mRSE_
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC1ENS0_13basic_cstringIKcEESI_mRSE_
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC1ENS0_13basic_cstringIKcEESI_mRSE_: ## @_ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC1ENS0_13basic_cstringIKcEESI_mRSE_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp145:
	.cfi_def_cfa_offset 16
Ltmp146:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp147:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	16(%rbp), %rax
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -24(%rbp)
	movq	%rdi, -40(%rbp)
	movq	%r9, -48(%rbp)
	movq	%rax, -56(%rbp)
	movq	-40(%rbp), %rdi
	movq	-48(%rbp), %r9
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdx
	movq	-32(%rbp), %rcx
	movq	-24(%rbp), %r8
	movq	-56(%rbp), %rax
	movq	%rax, (%rsp)
	callq	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC2ENS0_13basic_cstringIKcEESI_mRSE_
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test19test_unit_generatorD1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test19test_unit_generatorD1Ev
	.align	4, 0x90
__ZN5boost9unit_test19test_unit_generatorD1Ev: ## @_ZN5boost9unit_test19test_unit_generatorD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp148:
	.cfi_def_cfa_offset 16
Ltmp149:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp150:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test19test_unit_generatorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test19test_unit_generatorD0Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test19test_unit_generatorD0Ev
	.align	4, 0x90
__ZN5boost9unit_test19test_unit_generatorD0Ev: ## @_ZN5boost9unit_test19test_unit_generatorD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp151:
	.cfi_def_cfa_offset 16
Ltmp152:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp153:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN5boost9unit_test19test_unit_generatorD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost3mpl3aux13for_each_implILb0EE7executeINS0_6l_iterINS0_5list1I9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEENS5_INS0_5l_endEEENS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSL_22template_test_case_genI18MyTestCase_invokerNS0_4listISA_NSG_2naESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_EEEESO_EEEEvPT_PT0_PT1_T2_
	.weak_def_can_be_hidden	__ZN5boost3mpl3aux13for_each_implILb0EE7executeINS0_6l_iterINS0_5list1I9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEENS5_INS0_5l_endEEENS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSL_22template_test_case_genI18MyTestCase_invokerNS0_4listISA_NSG_2naESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_EEEESO_EEEEvPT_PT0_PT1_T2_
	.align	4, 0x90
__ZN5boost3mpl3aux13for_each_implILb0EE7executeINS0_6l_iterINS0_5list1I9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEENS5_INS0_5l_endEEENS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSL_22template_test_case_genI18MyTestCase_invokerNS0_4listISA_NSG_2naESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_EEEESO_EEEEvPT_PT0_PT1_T2_: ## @_ZN5boost3mpl3aux13for_each_implILb0EE7executeINS0_6l_iterINS0_5list1I9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEENS5_INS0_5l_endEEENS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSL_22template_test_case_genI18MyTestCase_invokerNS0_4listISA_NSG_2naESQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_SQ_EEEESO_EEEEvPT_PT0_PT1_T2_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp163:
	.cfi_def_cfa_offset 16
Ltmp164:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp165:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	leaq	16(%rbp), %rax
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	leaq	-32(%rbp), %rdi
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
Ltmp154:
	xorl	%ecx, %ecx
	movl	%ecx, %esi
	movq	-104(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN5boost3mpl3aux6unwrapINS_9unit_test9ut_detail25generate_test_case_4_typeINS4_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_EEEES7_EEEERT_SJ_l
Ltmp155:
	movq	%rax, -112(%rbp)        ## 8-byte Spill
	jmp	LBB47_1
LBB47_1:
Ltmp156:
	leaq	-32(%rbp), %rdi
	callq	__ZN5boost3getINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEERT_RNS_17value_initializedIS8_EE
Ltmp157:
	movq	%rax, -120(%rbp)        ## 8-byte Spill
	jmp	LBB47_2
LBB47_2:
Ltmp158:
	movq	-112(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EclISA_EEvNS5_8identityIT_EE
Ltmp159:
	jmp	LBB47_3
LBB47_3:
	movq	-104(%rbp), %rax        ## 8-byte Reload
	movq	40(%rax), %rcx
	movq	%rcx, -56(%rbp)
	movq	32(%rax), %rcx
	movq	%rcx, -64(%rbp)
	movq	24(%rax), %rcx
	movq	%rcx, -72(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, -88(%rbp)
	movq	%rcx, -96(%rbp)
Ltmp160:
	movq	-56(%rbp), %rcx
	movq	%rsp, %rdx
	movq	%rcx, 40(%rdx)
	movq	-64(%rbp), %rcx
	movq	%rcx, 32(%rdx)
	movq	-72(%rbp), %rcx
	movq	%rcx, 24(%rdx)
	movq	-80(%rbp), %rcx
	movq	%rcx, 16(%rdx)
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rsi
	movq	%rsi, 8(%rdx)
	movq	%rcx, (%rdx)
	xorl	%edi, %edi
	movl	%edi, %ecx
	movq	%rcx, %rdi
	movq	%rcx, %rsi
	movq	%rcx, %rdx
	callq	__ZN5boost3mpl3aux13for_each_implILb1EE7executeINS0_6l_iterINS0_5l_endEEES7_NS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSE_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEENS9_2naESN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_EEEESH_EEEEvPT_PT0_PT1_T2_
Ltmp161:
	jmp	LBB47_4
LBB47_4:
	leaq	-32(%rbp), %rdi
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	addq	$176, %rsp
	popq	%rbp
	retq
LBB47_5:
Ltmp162:
	leaq	-32(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
## BB#6:
	movq	-40(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table47:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset22 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset22
Lset23 = Ltmp154-Lfunc_begin4           ##   Call between Lfunc_begin4 and Ltmp154
	.long	Lset23
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp154-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset24
Lset25 = Ltmp161-Ltmp154                ##   Call between Ltmp154 and Ltmp161
	.long	Lset25
Lset26 = Ltmp162-Lfunc_begin4           ##     jumps to Ltmp162
	.long	Lset26
	.byte	0                       ##   On action: cleanup
Lset27 = Ltmp161-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset27
Lset28 = Lfunc_end4-Ltmp161             ##   Call between Ltmp161 and Lfunc_end4
	.long	Lset28
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.weak_def_can_be_hidden	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.align	4, 0x90
__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev: ## @_ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp166:
	.cfi_def_cfa_offset 16
Ltmp167:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp168:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost3mpl3aux6unwrapINS_9unit_test9ut_detail25generate_test_case_4_typeINS4_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_EEEES7_EEEERT_SJ_l
	.weak_def_can_be_hidden	__ZN5boost3mpl3aux6unwrapINS_9unit_test9ut_detail25generate_test_case_4_typeINS4_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_EEEES7_EEEERT_SJ_l
	.align	4, 0x90
__ZN5boost3mpl3aux6unwrapINS_9unit_test9ut_detail25generate_test_case_4_typeINS4_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_EEEES7_EEEERT_SJ_l: ## @_ZN5boost3mpl3aux6unwrapINS_9unit_test9ut_detail25generate_test_case_4_typeINS4_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_SE_EEEES7_EEEERT_SJ_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp169:
	.cfi_def_cfa_offset 16
Ltmp170:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp171:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EclISA_EEvNS5_8identityIT_EE
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EclISA_EEvNS5_8identityIT_EE
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EclISA_EEvNS5_8identityIT_EE: ## @_ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EclISA_EEvNS5_8identityIT_EE
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp198:
	.cfi_def_cfa_offset 16
Ltmp199:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp200:
	.cfi_def_cfa_register %rbp
	subq	$416, %rsp              ## imm = 0x1A0
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-184(%rbp), %rcx
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %r8
	movq	%r8, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -104(%rbp)
	movq	%rdi, -352(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -360(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-360(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movl	$0, -92(%rbp)
LBB50_1:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -92(%rbp)
	jae	LBB50_3
## BB#2:                                ##   in Loop: Header=BB50_1 Depth=1
	movl	-92(%rbp), %eax
	movl	%eax, %ecx
	movq	-88(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-92(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -92(%rbp)
	jmp	LBB50_1
LBB50_3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit
	movq	-352(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, -192(%rbp)
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rsi
	movq	-192(%rbp), %rdx
Ltmp172:
	leaq	-184(%rbp), %rdi
	xorl	%ecx, %ecx
	callq	__ZN5boost9unit_test9assign_opIcKcEEvRNSt3__112basic_stringIT_NS3_11char_traitsIS5_EENS3_9allocatorIS5_EEEENS0_13basic_cstringIT0_EEi
Ltmp173:
	jmp	LBB50_4
LBB50_4:
	leaq	-184(%rbp), %rax
	movq	%rax, -48(%rbp)
	movb	$60, -49(%rbp)
	movq	-48(%rbp), %rdi
	movsbl	-49(%rbp), %esi
Ltmp174:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp175:
	jmp	LBB50_5
LBB50_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLEc.exit
	jmp	LBB50_6
LBB50_6:
	movq	__ZTI9MyMessageIN18MyMessageNamespace17ParticularMessageEE@GOTPCREL(%rip), %rax
	movq	%rax, -40(%rbp)
	movq	8(%rax), %rax
	leaq	-184(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	%rax, -32(%rbp)
	movq	-24(%rbp), %rdi
Ltmp176:
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKc
Ltmp177:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB50_7
LBB50_7:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLEPKc.exit
	jmp	LBB50_8
LBB50_8:
	leaq	-184(%rbp), %rax
	movq	%rax, -8(%rbp)
	movb	$62, -9(%rbp)
	movq	-8(%rbp), %rdi
	movsbl	-9(%rbp), %esi
Ltmp178:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp179:
	jmp	LBB50_9
LBB50_9:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLEc.exit1
	jmp	LBB50_10
LBB50_10:
	movq	-352(%rbp), %rax        ## 8-byte Reload
	movq	40(%rax), %rcx
	addq	$8, %rcx
Ltmp180:
	movl	$296, %edx              ## imm = 0x128
	movl	%edx, %edi
	movq	%rcx, -376(%rbp)        ## 8-byte Spill
	callq	__Znwm
Ltmp181:
	movq	%rax, -384(%rbp)        ## 8-byte Spill
	jmp	LBB50_11
LBB50_11:
	movb	$1, -337(%rbp)
	movq	-384(%rbp), %rax        ## 8-byte Reload
Ltmp183:
	leaq	-280(%rbp), %rdi
	leaq	-184(%rbp), %rsi
	movq	%rax, -392(%rbp)        ## 8-byte Spill
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
Ltmp184:
	jmp	LBB50_12
LBB50_12:
	movq	-280(%rbp), %rsi
	movq	-272(%rbp), %rdx
Ltmp185:
	leaq	-264(%rbp), %rdi
	callq	__ZN5boost9unit_test9ut_detail24normalize_test_case_nameENS0_13basic_cstringIKcEE
Ltmp186:
	jmp	LBB50_13
LBB50_13:
Ltmp188:
	leaq	-240(%rbp), %rdi
	leaq	-264(%rbp), %rsi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
Ltmp189:
	jmp	LBB50_14
LBB50_14:
	movq	-352(%rbp), %rax        ## 8-byte Reload
	movq	16(%rax), %rcx
	movq	24(%rax), %rdx
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	32(%rax), %r9
Ltmp190:
	leaq	-328(%rbp), %rdi
	xorl	%esi, %esi
	movq	%r9, -400(%rbp)         ## 8-byte Spill
	callq	__ZN5boost8functionIFvvEEC1INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
Ltmp191:
	jmp	LBB50_15
LBB50_15:
	movq	-240(%rbp), %rsi
	movq	-232(%rbp), %rdx
	movq	-296(%rbp), %rcx
	movq	-288(%rbp), %r8
Ltmp193:
	movq	%rsp, %rax
	leaq	-328(%rbp), %rdi
	movq	%rdi, (%rax)
	movq	-392(%rbp), %rdi        ## 8-byte Reload
	movq	-400(%rbp), %r9         ## 8-byte Reload
	callq	__ZN5boost9unit_test9test_caseC1ENS0_13basic_cstringIKcEES4_mRKNS_8functionIFvvEEE
Ltmp194:
	jmp	LBB50_16
LBB50_16:
	movb	$0, -337(%rbp)
	movq	-392(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -224(%rbp)
Ltmp195:
	leaq	-224(%rbp), %rsi
	movq	-376(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9push_backEOS4_
Ltmp196:
	jmp	LBB50_17
LBB50_17:
	leaq	-328(%rbp), %rdi
	callq	__ZN5boost8functionIFvvEED1Ev
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-184(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$416, %rsp              ## imm = 0x1A0
	popq	%rbp
	retq
LBB50_18:
Ltmp182:
	movl	%edx, %ecx
	movq	%rax, -208(%rbp)
	movl	%ecx, -212(%rbp)
	jmp	LBB50_26
LBB50_19:
Ltmp187:
	movl	%edx, %ecx
	movq	%rax, -208(%rbp)
	movl	%ecx, -212(%rbp)
	jmp	LBB50_23
LBB50_20:
Ltmp192:
	movl	%edx, %ecx
	movq	%rax, -208(%rbp)
	movl	%ecx, -212(%rbp)
	jmp	LBB50_22
LBB50_21:
Ltmp197:
	leaq	-328(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -208(%rbp)
	movl	%ecx, -212(%rbp)
	callq	__ZN5boost8functionIFvvEED1Ev
LBB50_22:
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB50_23:
	testb	$1, -337(%rbp)
	jne	LBB50_24
	jmp	LBB50_25
LBB50_24:
	movq	-384(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdlPv
LBB50_25:
	jmp	LBB50_26
LBB50_26:
	leaq	-184(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#27:
	movq	-208(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table50:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset29 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset29
Lset30 = Ltmp172-Lfunc_begin5           ##   Call between Lfunc_begin5 and Ltmp172
	.long	Lset30
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset31 = Ltmp172-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset31
Lset32 = Ltmp181-Ltmp172                ##   Call between Ltmp172 and Ltmp181
	.long	Lset32
Lset33 = Ltmp182-Lfunc_begin5           ##     jumps to Ltmp182
	.long	Lset33
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp183-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset34
Lset35 = Ltmp186-Ltmp183                ##   Call between Ltmp183 and Ltmp186
	.long	Lset35
Lset36 = Ltmp187-Lfunc_begin5           ##     jumps to Ltmp187
	.long	Lset36
	.byte	0                       ##   On action: cleanup
Lset37 = Ltmp188-Lfunc_begin5           ## >> Call Site 4 <<
	.long	Lset37
Lset38 = Ltmp191-Ltmp188                ##   Call between Ltmp188 and Ltmp191
	.long	Lset38
Lset39 = Ltmp192-Lfunc_begin5           ##     jumps to Ltmp192
	.long	Lset39
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp193-Lfunc_begin5           ## >> Call Site 5 <<
	.long	Lset40
Lset41 = Ltmp196-Ltmp193                ##   Call between Ltmp193 and Ltmp196
	.long	Lset41
Lset42 = Ltmp197-Lfunc_begin5           ##     jumps to Ltmp197
	.long	Lset42
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp196-Lfunc_begin5           ## >> Call Site 6 <<
	.long	Lset43
Lset44 = Lfunc_end5-Ltmp196             ##   Call between Ltmp196 and Lfunc_end5
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost3getINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEERT_RNS_17value_initializedIS8_EE
	.weak_def_can_be_hidden	__ZN5boost3getINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEERT_RNS_17value_initializedIS8_EE
	.align	4, 0x90
__ZN5boost3getINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEERT_RNS_17value_initializedIS8_EE: ## @_ZN5boost3getINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEERT_RNS_17value_initializedIS8_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp201:
	.cfi_def_cfa_offset 16
Ltmp202:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp203:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost3mpl3aux13for_each_implILb1EE7executeINS0_6l_iterINS0_5l_endEEES7_NS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSE_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEENS9_2naESN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_EEEESH_EEEEvPT_PT0_PT1_T2_
	.weak_def_can_be_hidden	__ZN5boost3mpl3aux13for_each_implILb1EE7executeINS0_6l_iterINS0_5l_endEEES7_NS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSE_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEENS9_2naESN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_EEEESH_EEEEvPT_PT0_PT1_T2_
	.align	4, 0x90
__ZN5boost3mpl3aux13for_each_implILb1EE7executeINS0_6l_iterINS0_5l_endEEES7_NS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSE_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEENS9_2naESN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_EEEESH_EEEEvPT_PT0_PT1_T2_: ## @_ZN5boost3mpl3aux13for_each_implILb1EE7executeINS0_6l_iterINS0_5l_endEEES7_NS0_13make_identityIN4mpl_3argILin1EEEEENS_9unit_test9ut_detail25generate_test_case_4_typeINSE_22template_test_case_genI18MyTestCase_invokerNS0_4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEENS9_2naESN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_SN_EEEESH_EEEEvPT_PT0_PT1_T2_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp204:
	.cfi_def_cfa_offset 16
Ltmp205:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp206:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.weak_def_can_be_hidden	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.align	4, 0x90
__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev: ## @_ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp207:
	.cfi_def_cfa_offset 16
Ltmp208:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp209:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.weak_def_can_be_hidden	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.align	4, 0x90
__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev: ## @_ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp210:
	.cfi_def_cfa_offset 16
Ltmp211:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp212:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp213:
	.cfi_def_cfa_offset 16
Ltmp214:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp215:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp216:
	.cfi_def_cfa_offset 16
Ltmp217:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp218:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
	movq	%rax, %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
	.weak_def_can_be_hidden	__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
	.align	4, 0x90
__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv: ## @_ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp219:
	.cfi_def_cfa_offset 16
Ltmp220:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp221:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC1Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC1Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC1Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp222:
	.cfi_def_cfa_offset 16
Ltmp223:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp224:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC2Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC2Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC2Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7wrapperC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp225:
	.cfi_def_cfa_offset 16
Ltmp226:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp227:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9assign_opIcKcEEvRNSt3__112basic_stringIT_NS3_11char_traitsIS5_EENS3_9allocatorIS5_EEEENS0_13basic_cstringIT0_EEi
	.weak_def_can_be_hidden	__ZN5boost9unit_test9assign_opIcKcEEvRNSt3__112basic_stringIT_NS3_11char_traitsIS5_EENS3_9allocatorIS5_EEEENS0_13basic_cstringIT0_EEi
	.align	4, 0x90
__ZN5boost9unit_test9assign_opIcKcEEvRNSt3__112basic_stringIT_NS3_11char_traitsIS5_EENS3_9allocatorIS5_EEEENS0_13basic_cstringIT0_EEi: ## @_ZN5boost9unit_test9assign_opIcKcEEvRNSt3__112basic_stringIT_NS3_11char_traitsIS5_EENS3_9allocatorIS5_EEEENS0_13basic_cstringIT0_EEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp228:
	.cfi_def_cfa_offset 16
Ltmp229:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp230:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	leaq	-16(%rbp), %rax
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	%rdi, -24(%rbp)
	movl	%ecx, -28(%rbp)
	movq	-24(%rbp), %rdi
	movq	%rdi, -40(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcE5beginEv
	leaq	-16(%rbp), %rdi
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	__ZNK5boost9unit_test13basic_cstringIKcE4sizeEv
	movq	-40(%rbp), %rdi         ## 8-byte Reload
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6assignEPKcm
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9push_backEOS4_
	.weak_def_can_be_hidden	__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9push_backEOS4_
	.align	4, 0x90
__ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9push_backEOS4_: ## @_ZNSt3__14listIPN5boost9unit_test9test_unitENS_9allocatorIS4_EEE9push_backEOS4_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp231:
	.cfi_def_cfa_offset 16
Ltmp232:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp233:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$1080, %rsp             ## imm = 0x438
Ltmp234:
	.cfi_offset %rbx, -24
	movq	%rdi, -1000(%rbp)
	movq	%rsi, -1008(%rbp)
	movq	-1000(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -992(%rbp)
	movq	-992(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -984(%rbp)
	movq	-984(%rbp), %rdi
	movq	%rdi, -976(%rbp)
	movq	-976(%rbp), %rdi
	movq	%rdi, -1016(%rbp)
	movq	-1016(%rbp), %rdi
	movq	%rdi, -800(%rbp)
	movq	$1, -808(%rbp)
	movq	-800(%rbp), %rdi
	movq	-808(%rbp), %rax
	movq	%rdi, -776(%rbp)
	movq	%rax, -784(%rbp)
	movq	$0, -792(%rbp)
	imulq	$24, -784(%rbp), %rax
	movq	%rax, -768(%rbp)
	movq	-768(%rbp), %rdi
	movq	%rsi, -1080(%rbp)       ## 8-byte Spill
	callq	__Znwm
	leaq	-1040(%rbp), %rsi
	leaq	-376(%rbp), %rdi
	leaq	-392(%rbp), %rcx
	leaq	-416(%rbp), %rdx
	leaq	-432(%rbp), %r8
	leaq	-1056(%rbp), %r9
	movq	-1016(%rbp), %r10
	movq	%r9, -744(%rbp)
	movq	%r10, -752(%rbp)
	movq	$1, -760(%rbp)
	movq	-744(%rbp), %r10
	movq	-760(%rbp), %r11
	movq	-752(%rbp), %rbx
	movq	%r10, -720(%rbp)
	movq	%rbx, -728(%rbp)
	movq	%r11, -736(%rbp)
	movq	-720(%rbp), %r10
	movq	-728(%rbp), %r11
	movq	%r11, (%r10)
	movq	-736(%rbp), %r11
	movq	%r11, 8(%r10)
	movq	%rsi, -536(%rbp)
	movq	%rax, -544(%rbp)
	movq	%r9, -552(%rbp)
	movq	-536(%rbp), %rax
	movq	-544(%rbp), %r9
	movq	-552(%rbp), %r10
	movq	%rax, -496(%rbp)
	movq	%r9, -504(%rbp)
	movq	%r10, -512(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %r9
	movq	-512(%rbp), %r10
	movq	%r10, -488(%rbp)
	movq	-488(%rbp), %r10
	movq	(%r10), %r11
	movq	%r11, -528(%rbp)
	movq	8(%r10), %r10
	movq	%r10, -520(%rbp)
	movq	-528(%rbp), %r10
	movq	-520(%rbp), %r11
	movq	%r10, -464(%rbp)
	movq	%r11, -456(%rbp)
	movq	%rax, -472(%rbp)
	movq	%r9, -480(%rbp)
	movq	-472(%rbp), %rax
	movq	-480(%rbp), %r9
	movq	-464(%rbp), %r10
	movq	-456(%rbp), %r11
	movq	%r10, -416(%rbp)
	movq	%r11, -408(%rbp)
	movq	%rax, -424(%rbp)
	movq	%r9, -432(%rbp)
	movq	-424(%rbp), %rax
	movq	%r8, -400(%rbp)
	movq	-400(%rbp), %r8
	movq	(%r8), %r8
	movq	%rdx, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	(%rdx), %r9
	movq	%r9, -448(%rbp)
	movq	8(%rdx), %rdx
	movq	%rdx, -440(%rbp)
	movq	-448(%rbp), %rdx
	movq	-440(%rbp), %r9
	movq	%rdx, -376(%rbp)
	movq	%r9, -368(%rbp)
	movq	%rax, -384(%rbp)
	movq	%r8, -392(%rbp)
	movq	-384(%rbp), %rax
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, 8(%rax)
	movq	8(%rcx), %rcx
	movq	%rcx, 16(%rax)
	movq	-1016(%rbp), %rax
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	(%rcx), %rcx
	addq	$16, %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	-1008(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
## BB#1:
	leaq	-1040(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	-1080(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rax, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	-264(%rbp), %rsi
	movq	%rcx, 8(%rsi)
	movq	(%rax), %rcx
	movq	-256(%rbp), %rsi
	movq	%rcx, (%rsi)
	movq	-256(%rbp), %rcx
	movq	-256(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rcx, 8(%rsi)
	movq	-264(%rbp), %rcx
	movq	%rcx, (%rax)
## BB#2:
	leaq	-1040(%rbp), %rax
	movq	-1080(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	(%rcx), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rcx)
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rdx
	movq	%rdx, -312(%rbp)
	movq	-312(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -336(%rbp)
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	$0, (%rcx)
	movq	%rax, -712(%rbp)
	movq	-712(%rbp), %rax
	movq	%rax, -704(%rbp)
	movq	-704(%rbp), %rax
	movq	%rax, -680(%rbp)
	movq	$0, -688(%rbp)
	movq	-680(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	-664(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -696(%rbp)
	movq	-688(%rbp), %rcx
	movq	%rax, -584(%rbp)
	movq	-584(%rbp), %rdx
	movq	%rdx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -696(%rbp)
	movq	%rax, -1088(%rbp)       ## 8-byte Spill
	je	LBB61_4
## BB#3:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -568(%rbp)
	movq	-568(%rbp), %rcx
	movq	%rcx, -560(%rbp)
	movq	-560(%rbp), %rcx
	addq	$8, %rcx
	movq	-696(%rbp), %rdx
	movq	%rcx, -648(%rbp)
	movq	%rdx, -656(%rbp)
	movq	-648(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-656(%rbp), %rsi
	movq	8(%rcx), %rcx
	movq	%rdx, -624(%rbp)
	movq	%rsi, -632(%rbp)
	movq	%rcx, -640(%rbp)
	movq	-624(%rbp), %rcx
	movq	-632(%rbp), %rdx
	movq	-640(%rbp), %rsi
	movq	%rcx, -600(%rbp)
	movq	%rdx, -608(%rbp)
	movq	%rsi, -616(%rbp)
	movq	-608(%rbp), %rcx
	movq	%rcx, -592(%rbp)
	movq	-592(%rbp), %rdi
	callq	__ZdlPv
LBB61_4:                                ## %_ZNSt3__110unique_ptrINS_11__list_nodeIPN5boost9unit_test9test_unitEPvEENS_22__allocator_destructorINS_9allocatorIS7_EEEEED1Ev.exit2
	addq	$1080, %rsp             ## imm = 0x438
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE: ## @_ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp235:
	.cfi_def_cfa_offset 16
Ltmp236:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp237:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC2ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost8functionIFvvEEC1INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.weak_def_can_be_hidden	__ZN5boost8functionIFvvEEC1INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.align	4, 0x90
__ZN5boost8functionIFvvEEC1INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE: ## @_ZN5boost8functionIFvvEEC1INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp238:
	.cfi_def_cfa_offset 16
Ltmp239:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp240:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movq	-16(%rbp), %rdi
	movl	-20(%rbp), %esi
	callq	__ZN5boost8functionIFvvEEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost8functionIFvvEED1Ev
	.weak_def_can_be_hidden	__ZN5boost8functionIFvvEED1Ev
	.align	4, 0x90
__ZN5boost8functionIFvvEED1Ev:          ## @_ZN5boost8functionIFvvEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp241:
	.cfi_def_cfa_offset 16
Ltmp242:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp243:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost8functionIFvvEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcE5beginEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcE5beginEv
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcE5beginEv: ## @_ZN5boost9unit_test13basic_cstringIKcE5beginEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp244:
	.cfi_def_cfa_offset 16
Ltmp245:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp246:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost9unit_test13basic_cstringIKcE4sizeEv
	.weak_def_can_be_hidden	__ZNK5boost9unit_test13basic_cstringIKcE4sizeEv
	.align	4, 0x90
__ZNK5boost9unit_test13basic_cstringIKcE4sizeEv: ## @_ZNK5boost9unit_test13basic_cstringIKcE4sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp247:
	.cfi_def_cfa_offset 16
Ltmp248:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp249:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	subq	%rdi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcEC2ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC2ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC2ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE: ## @_ZN5boost9unit_test13basic_cstringIKcEC2ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp250:
	.cfi_def_cfa_offset 16
Ltmp251:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp252:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	-208(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movq	%rdi, -200(%rbp)
	movq	-200(%rbp), %rdi
	movq	%rdi, -192(%rbp)
	movq	-192(%rbp), %rdi
	movq	%rdi, -184(%rbp)
	movq	-184(%rbp), %rdi
	movq	%rdi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	movq	%rdi, -232(%rbp)        ## 8-byte Spill
	je	LBB67_2
## BB#1:
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	jmp	LBB67_3
LBB67_2:
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
LBB67_3:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv.exit
	movq	-240(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	-224(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	addq	$8, %rcx
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdx
	movq	-216(%rbp), %rsi
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	movzbl	(%rdi), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	movq	%rdx, -256(%rbp)        ## 8-byte Spill
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	je	LBB67_5
## BB#4:
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -272(%rbp)        ## 8-byte Spill
	jmp	LBB67_6
LBB67_5:
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -272(%rbp)        ## 8-byte Spill
LBB67_6:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-272(%rbp), %rax        ## 8-byte Reload
	movq	-256(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rcx, (%rax)
	addq	$144, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost8functionIFvvEEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.weak_def_can_be_hidden	__ZN5boost8functionIFvvEEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.align	4, 0x90
__ZN5boost8functionIFvvEEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE: ## @_ZN5boost8functionIFvvEEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISD_EE5valueEiE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp253:
	.cfi_def_cfa_offset 16
Ltmp254:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp255:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	xorl	%eax, %eax
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movq	-16(%rbp), %rdi
	movl	%eax, %esi
	callq	__ZN5boost9function0IvEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISC_EE5valueEiE4typeE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9function0IvEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISC_EE5valueEiE4typeE
	.weak_def_can_be_hidden	__ZN5boost9function0IvEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISC_EE5valueEiE4typeE
	.align	4, 0x90
__ZN5boost9function0IvEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISC_EE5valueEiE4typeE: ## @_ZN5boost9function0IvEC2INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEET_NS_11enable_if_cIXntsr11is_integralISC_EE5valueEiE4typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp256:
	.cfi_def_cfa_offset 16
Ltmp257:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp258:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movl	%esi, -20(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -32(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN5boost13function_baseC2Ev
	movq	-32(%rbp), %rdi         ## 8-byte Reload
	callq	__ZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost13function_baseC2Ev
	.weak_def_can_be_hidden	__ZN5boost13function_baseC2Ev
	.align	4, 0x90
__ZN5boost13function_baseC2Ev:          ## @_ZN5boost13function_baseC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp259:
	.cfi_def_cfa_offset 16
Ltmp260:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp261:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	$0, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_
	.weak_def_can_be_hidden	__ZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_
	.align	4, 0x90
__ZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_: ## @_ZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp262:
	.cfi_def_cfa_offset 16
Ltmp263:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp264:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	__ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable@GOTPCREL(%rip), %rax
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %rcx
	addq	$8, %rcx
	movq	%rdi, -40(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferE
	testb	$1, %al
	jne	LBB71_1
	jmp	LBB71_2
LBB71_1:
	movq	__ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable@GOTPCREL(%rip), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	orq	$1, %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	-40(%rbp), %rcx         ## 8-byte Reload
	movq	%rax, (%rcx)
	jmp	LBB71_3
LBB71_2:
	movq	-40(%rbp), %rax         ## 8-byte Reload
	movq	$0, (%rax)
LBB71_3:
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE6manageERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.weak_definition	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE6manageERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.align	4, 0x90
__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE6manageERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE: ## @_ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE6manageERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp265:
	.cfi_def_cfa_offset 16
Ltmp266:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp267:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	subl	$4, %edx
	movl	%edx, -28(%rbp)         ## 4-byte Spill
	jne	LBB72_2
	jmp	LBB72_1
LBB72_1:
	movq	__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE@GOTPCREL(%rip), %rax
	movq	-16(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-16(%rbp), %rax
	movb	$0, 8(%rax)
	movq	-16(%rbp), %rax
	movb	$0, 9(%rax)
	jmp	LBB72_3
LBB72_2:
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movl	-20(%rbp), %edx
	callq	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE
LBB72_3:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function26void_function_obj_invoker0INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvE6invokeERNS1_15function_bufferE
	.weak_definition	__ZN5boost6detail8function26void_function_obj_invoker0INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvE6invokeERNS1_15function_bufferE
	.align	4, 0x90
__ZN5boost6detail8function26void_function_obj_invoker0INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvE6invokeERNS1_15function_bufferE: ## @_ZN5boost6detail8function26void_function_obj_invoker0INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvE6invokeERNS1_15function_bufferE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp268:
	.cfi_def_cfa_offset 16
Ltmp269:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp270:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	__ZN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEclEv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferE
	.weak_def_can_be_hidden	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferE
	.align	4, 0x90
__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferE: ## @_ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp271:
	.cfi_def_cfa_offset 16
Ltmp272:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp273:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	-24(%rbp), %rsi
	callq	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferENS1_16function_obj_tagE
	andb	$1, %al
	movzbl	%al, %eax
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE
	.weak_def_can_be_hidden	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE
	.align	4, 0x90
__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE: ## @_ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp274:
	.cfi_def_cfa_offset 16
Ltmp275:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp276:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movq	-16(%rbp), %rdi
	movq	-24(%rbp), %rsi
	movl	-28(%rbp), %edx
	callq	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeEN4mpl_5bool_ILb1EEE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeEN4mpl_5bool_ILb1EEE
	.weak_def_can_be_hidden	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeEN4mpl_5bool_ILb1EEE
	.align	4, 0x90
__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeEN4mpl_5bool_ILb1EEE: ## @_ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE7managerERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeEN4mpl_5bool_ILb1EEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp277:
	.cfi_def_cfa_offset 16
Ltmp278:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp279:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movl	%edx, -28(%rbp)
	movq	-16(%rbp), %rdi
	movq	-24(%rbp), %rsi
	movl	-28(%rbp), %edx
	callq	__ZN5boost6detail8function22functor_manager_commonINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE12manage_smallERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function22functor_manager_commonINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE12manage_smallERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.weak_def_can_be_hidden	__ZN5boost6detail8function22functor_manager_commonINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE12manage_smallERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.align	4, 0x90
__ZN5boost6detail8function22functor_manager_commonINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE12manage_smallERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE: ## @_ZN5boost6detail8function22functor_manager_commonINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE12manage_smallERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp280:
	.cfi_def_cfa_offset 16
Ltmp281:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp282:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movl	%edx, -36(%rbp)
	cmpl	$0, -36(%rbp)
	je	LBB77_2
## BB#1:
	cmpl	$1, -36(%rbp)
	jne	LBB77_5
LBB77_2:
	movq	-24(%rbp), %rax
	movq	%rax, -48(%rbp)
	cmpl	$1, -36(%rbp)
	jne	LBB77_4
## BB#3:
	movq	-24(%rbp), %rax
	movq	%rax, -56(%rbp)
LBB77_4:
	jmp	LBB77_15
LBB77_5:
	cmpl	$2, -36(%rbp)
	jne	LBB77_7
## BB#6:
	movq	-32(%rbp), %rax
	movq	%rax, -64(%rbp)
	jmp	LBB77_14
LBB77_7:
	cmpl	$3, -36(%rbp)
	jne	LBB77_12
## BB#8:
	movq	__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE@GOTPCREL(%rip), %rax
	movq	-32(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	8(%rcx), %rdi
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	8(%rax), %rsi
	callq	_strcmp
	cmpl	$0, %eax
	jne	LBB77_10
## BB#9:
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rax, (%rcx)
	jmp	LBB77_11
LBB77_10:
	movq	-32(%rbp), %rax
	movq	$0, (%rax)
LBB77_11:
	jmp	LBB77_13
LBB77_12:
	movq	__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE@GOTPCREL(%rip), %rax
	movq	-32(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-32(%rbp), %rax
	movb	$0, 8(%rax)
	movq	-32(%rbp), %rax
	movb	$0, 9(%rax)
LBB77_13:
	jmp	LBB77_14
LBB77_14:
	jmp	LBB77_15
LBB77_15:
	addq	$80, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEclEv
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEclEv
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEclEv: ## @_ZN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEclEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp283:
	.cfi_def_cfa_offset 16
Ltmp284:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp285:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rdi, -8(%rbp)
	movq	%rcx, %rdi
	callq	__ZN18MyTestCase_invoker3runI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvPN5boost4typeIT_EE
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN18MyTestCase_invoker3runI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvPN5boost4typeIT_EE
	.weak_def_can_be_hidden	__ZN18MyTestCase_invoker3runI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvPN5boost4typeIT_EE
	.align	4, 0x90
__ZN18MyTestCase_invoker3runI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvPN5boost4typeIT_EE: ## @_ZN18MyTestCase_invoker3runI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvPN5boost4typeIT_EE
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp331:
	.cfi_def_cfa_offset 16
Ltmp332:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp333:
	.cfi_def_cfa_register %rbp
	subq	$1168, %rsp             ## imm = 0x490
	movq	%rdi, -8(%rbp)
	movq	__ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE(%rip), %rdi
	leaq	L_.str.2(%rip), %rsi
	movl	$15, %eax
	movl	%eax, %edx
	leaq	-24(%rbp), %rcx
	movq	%rdi, -1008(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	movl	$288, %eax              ## imm = 0x120
	movl	%eax, %esi
	leaq	-328(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1016(%rbp)       ## 8-byte Spill
	callq	___bzero
	movq	-1016(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcEC1Ev
Ltmp286:
	movq	-1016(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3refEv
Ltmp287:
	movq	%rax, -1024(%rbp)       ## 8-byte Spill
	jmp	LBB79_1
LBB79_1:
	movb	$34, -341(%rbp)
Ltmp288:
	leaq	-341(%rbp), %rsi
	movq	-1024(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
Ltmp289:
	movq	%rax, -1032(%rbp)       ## 8-byte Spill
	jmp	LBB79_2
LBB79_2:
Ltmp290:
	leaq	L_.str(%rip), %rsi
	movq	-1032(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp291:
	movq	%rax, -1040(%rbp)       ## 8-byte Spill
	jmp	LBB79_3
LBB79_3:
Ltmp292:
	leaq	L_.str.3(%rip), %rsi
	movq	-1040(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA17_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp293:
	movq	%rax, -1048(%rbp)       ## 8-byte Spill
	jmp	LBB79_4
LBB79_4:
Ltmp294:
	movq	-1048(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3strEv
Ltmp295:
	movq	%rax, -1056(%rbp)       ## 8-byte Spill
	jmp	LBB79_5
LBB79_5:
Ltmp296:
	leaq	-40(%rbp), %rdi
	movq	-1056(%rbp), %rsi       ## 8-byte Reload
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
Ltmp297:
	jmp	LBB79_6
LBB79_6:
	movq	-24(%rbp), %rsi
	movq	-16(%rbp), %rdx
	movq	-40(%rbp), %r8
	movq	-32(%rbp), %r9
Ltmp298:
	movl	$46, %eax
	movl	%eax, %ecx
	movq	-1008(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost9unit_test15unit_test_log_t14set_checkpointENS0_13basic_cstringIKcEEmS4_
Ltmp299:
	jmp	LBB79_7
LBB79_7:
	leaq	-328(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	movq	__ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE(%rip), %rdi
	leaq	L_.str.2(%rip), %rsi
	movl	$15, %eax
	movl	%eax, %edx
	leaq	-360(%rbp), %rcx
	movq	%rdi, -1064(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	movl	$288, %eax              ## imm = 0x120
	movl	%eax, %esi
	leaq	-664(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1072(%rbp)       ## 8-byte Spill
	callq	___bzero
	movq	-1072(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcEC1Ev
Ltmp301:
	movq	-1072(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3refEv
Ltmp302:
	movq	%rax, -1080(%rbp)       ## 8-byte Spill
	jmp	LBB79_8
LBB79_8:
	movb	$34, -665(%rbp)
Ltmp303:
	leaq	-665(%rbp), %rsi
	movq	-1080(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
Ltmp304:
	movq	%rax, -1088(%rbp)       ## 8-byte Spill
	jmp	LBB79_9
LBB79_9:
Ltmp305:
	leaq	L_.str(%rip), %rsi
	movq	-1088(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp306:
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	jmp	LBB79_10
LBB79_10:
Ltmp307:
	leaq	L_.str.4(%rip), %rsi
	movq	-1096(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA9_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp308:
	movq	%rax, -1104(%rbp)       ## 8-byte Spill
	jmp	LBB79_11
LBB79_11:
Ltmp309:
	movq	-1104(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3strEv
Ltmp310:
	movq	%rax, -1112(%rbp)       ## 8-byte Spill
	jmp	LBB79_12
LBB79_12:
Ltmp311:
	leaq	-376(%rbp), %rdi
	movq	-1112(%rbp), %rsi       ## 8-byte Reload
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
Ltmp312:
	jmp	LBB79_13
LBB79_13:
	movq	-360(%rbp), %rsi
	movq	-352(%rbp), %rdx
	movq	-376(%rbp), %r8
	movq	-368(%rbp), %r9
Ltmp313:
	movl	$46, %eax
	movl	%eax, %ecx
	movq	-1064(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost9unit_test15unit_test_log_t14set_checkpointENS0_13basic_cstringIKcEEmS4_
Ltmp314:
	jmp	LBB79_14
LBB79_14:
	leaq	-664(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	leaq	-344(%rbp), %rdi
	callq	__ZN10MyTestCaseI9MyMessageIN18MyMessageNamespace17ParticularMessageEEE11test_methodEv
	movq	__ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE(%rip), %rdi
	leaq	L_.str.2(%rip), %rsi
	movl	$15, %eax
	movl	%eax, %edx
	leaq	-688(%rbp), %rcx
	movq	%rdi, -1120(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	movl	$288, %eax              ## imm = 0x120
	movl	%eax, %esi
	leaq	-992(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rcx, -1128(%rbp)       ## 8-byte Spill
	callq	___bzero
	movq	-1128(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcEC1Ev
Ltmp316:
	movq	-1128(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3refEv
Ltmp317:
	movq	%rax, -1136(%rbp)       ## 8-byte Spill
	jmp	LBB79_15
LBB79_15:
	movb	$34, -993(%rbp)
Ltmp318:
	leaq	-993(%rbp), %rsi
	movq	-1136(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
Ltmp319:
	movq	%rax, -1144(%rbp)       ## 8-byte Spill
	jmp	LBB79_16
LBB79_16:
Ltmp320:
	leaq	L_.str(%rip), %rsi
	movq	-1144(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp321:
	movq	%rax, -1152(%rbp)       ## 8-byte Spill
	jmp	LBB79_17
LBB79_17:
Ltmp322:
	leaq	L_.str.5(%rip), %rsi
	movq	-1152(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boostlsIcA8_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
Ltmp323:
	movq	%rax, -1160(%rbp)       ## 8-byte Spill
	jmp	LBB79_18
LBB79_18:
Ltmp324:
	movq	-1160(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost23basic_wrap_stringstreamIcE3strEv
Ltmp325:
	movq	%rax, -1168(%rbp)       ## 8-byte Spill
	jmp	LBB79_19
LBB79_19:
Ltmp326:
	leaq	-704(%rbp), %rdi
	movq	-1168(%rbp), %rsi       ## 8-byte Reload
	callq	__ZN5boost9unit_test13basic_cstringIKcEC1ERKNSt3__112basic_stringIcNS4_11char_traitsIcEENS4_9allocatorIcEEEE
Ltmp327:
	jmp	LBB79_20
LBB79_20:
	movq	-688(%rbp), %rsi
	movq	-680(%rbp), %rdx
	movq	-704(%rbp), %r8
	movq	-696(%rbp), %r9
Ltmp328:
	movl	$46, %eax
	movl	%eax, %ecx
	movq	-1120(%rbp), %rdi       ## 8-byte Reload
	callq	__ZN5boost9unit_test15unit_test_log_t14set_checkpointENS0_13basic_cstringIKcEEmS4_
Ltmp329:
	jmp	LBB79_21
LBB79_21:
	leaq	-992(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	addq	$1168, %rsp             ## imm = 0x490
	popq	%rbp
	retq
LBB79_22:
Ltmp300:
	leaq	-328(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -336(%rbp)
	movl	%ecx, -340(%rbp)
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	jmp	LBB79_25
LBB79_23:
Ltmp315:
	leaq	-664(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -336(%rbp)
	movl	%ecx, -340(%rbp)
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	jmp	LBB79_25
LBB79_24:
Ltmp330:
	leaq	-992(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -336(%rbp)
	movl	%ecx, -340(%rbp)
	callq	__ZN5boost23basic_wrap_stringstreamIcED1Ev
LBB79_25:
	movq	-336(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table79:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	93                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	91                      ## Call site table length
Lset45 = Lfunc_begin6-Lfunc_begin6      ## >> Call Site 1 <<
	.long	Lset45
Lset46 = Ltmp286-Lfunc_begin6           ##   Call between Lfunc_begin6 and Ltmp286
	.long	Lset46
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset47 = Ltmp286-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset47
Lset48 = Ltmp299-Ltmp286                ##   Call between Ltmp286 and Ltmp299
	.long	Lset48
Lset49 = Ltmp300-Lfunc_begin6           ##     jumps to Ltmp300
	.long	Lset49
	.byte	0                       ##   On action: cleanup
Lset50 = Ltmp299-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset50
Lset51 = Ltmp301-Ltmp299                ##   Call between Ltmp299 and Ltmp301
	.long	Lset51
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset52 = Ltmp301-Lfunc_begin6           ## >> Call Site 4 <<
	.long	Lset52
Lset53 = Ltmp314-Ltmp301                ##   Call between Ltmp301 and Ltmp314
	.long	Lset53
Lset54 = Ltmp315-Lfunc_begin6           ##     jumps to Ltmp315
	.long	Lset54
	.byte	0                       ##   On action: cleanup
Lset55 = Ltmp314-Lfunc_begin6           ## >> Call Site 5 <<
	.long	Lset55
Lset56 = Ltmp316-Ltmp314                ##   Call between Ltmp314 and Ltmp316
	.long	Lset56
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset57 = Ltmp316-Lfunc_begin6           ## >> Call Site 6 <<
	.long	Lset57
Lset58 = Ltmp329-Ltmp316                ##   Call between Ltmp316 and Ltmp329
	.long	Lset58
Lset59 = Ltmp330-Lfunc_begin6           ##     jumps to Ltmp330
	.long	Lset59
	.byte	0                       ##   On action: cleanup
Lset60 = Ltmp329-Lfunc_begin6           ## >> Call Site 7 <<
	.long	Lset60
Lset61 = Lfunc_end6-Ltmp329             ##   Call between Ltmp329 and Lfunc_end6
	.long	Lset61
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_: ## @_ZN5boost9unit_test13basic_cstringIKcEC1ImEEPS2_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp334:
	.cfi_def_cfa_offset 16
Ltmp335:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp336:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	callq	__ZN5boost9unit_test13basic_cstringIKcEC2ImEEPS2_T_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boostlsIcA17_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.weak_def_can_be_hidden	__ZN5boostlsIcA17_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.align	4, 0x90
__ZN5boostlsIcA17_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_: ## @_ZN5boostlsIcA17_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp337:
	.cfi_def_cfa_offset 16
Ltmp338:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp339:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rsi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.weak_def_can_be_hidden	__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.align	4, 0x90
__ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_: ## @_ZN5boostlsIcA11_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp340:
	.cfi_def_cfa_offset 16
Ltmp341:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp342:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rsi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
	.weak_def_can_be_hidden	__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
	.align	4, 0x90
__ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_: ## @_ZN5boostlsIccEERNS_23basic_wrap_stringstreamIT_EES4_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp343:
	.cfi_def_cfa_offset 16
Ltmp344:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp345:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	movsbl	(%rsi), %esi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	movq	-8(%rbp), %rdi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rdi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcEC1Ev
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcEC1Ev
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcEC1Ev: ## @_ZN5boost23basic_wrap_stringstreamIcEC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp346:
	.cfi_def_cfa_offset 16
Ltmp347:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp348:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcEC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcE3refEv
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcE3refEv
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcE3refEv: ## @_ZN5boost23basic_wrap_stringstreamIcE3refEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp349:
	.cfi_def_cfa_offset 16
Ltmp350:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp351:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcE3strEv
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcE3strEv
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcE3strEv: ## @_ZN5boost23basic_wrap_stringstreamIcE3strEv
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp355:
	.cfi_def_cfa_offset 16
Ltmp356:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp357:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	leaq	-440(%rbp), %rax
	movq	%rdi, -416(%rbp)
	movq	-416(%rbp), %rdi
	movq	%rdi, %rcx
	addq	$264, %rcx              ## imm = 0x108
	movq	%rdi, -408(%rbp)
	movq	-408(%rbp), %rdx
	addq	$8, %rdx
	movq	%rdi, -448(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	leaq	-440(%rbp), %rax
	movq	-456(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -384(%rbp)
	movq	%rax, -392(%rbp)
	movq	-384(%rbp), %rax
	movq	-392(%rbp), %rcx
	movq	%rax, -368(%rbp)
	movq	%rcx, -376(%rbp)
	movq	-368(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	%rcx, -344(%rbp)
	movq	%rcx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movq	%rdx, -320(%rbp)
	movq	-320(%rbp), %rdx
	movzbl	(%rdx), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	movq	%rcx, -472(%rbp)        ## 8-byte Spill
	je	LBB86_2
## BB#1:
	leaq	-353(%rbp), %rsi
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rdi
	movb	$0, -353(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -208(%rbp)
	movq	$0, -216(%rbp)
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %rsi
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rsi, 8(%rcx)
	jmp	LBB86_3
LBB86_2:
	leaq	-354(%rbp), %rsi
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rdi
	movb	$0, -354(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	$0, -312(%rbp)
	movq	-304(%rbp), %rcx
	movq	-312(%rbp), %rsi
	shlq	$1, %rsi
	movb	%sil, %dl
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movb	%dl, (%rcx)
LBB86_3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit.i.i
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)
Ltmp352:
	xorl	%ecx, %ecx
	movl	%ecx, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEm
Ltmp353:
	jmp	LBB86_5
LBB86_4:
Ltmp354:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -476(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB86_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE13shrink_to_fitEv.exit.i.i
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rsi
	movq	%rsi, 8(%rcx)
	movq	16(%rdx), %rdx
	movq	%rdx, 16(%rcx)
	movq	-376(%rbp), %rcx
	movq	%rax, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB86_6:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB86_8
## BB#7:                                ##   in Loop: Header=BB86_6 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB86_6
LBB86_8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEOS5_.exit
	leaq	-440(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-448(%rbp), %rdi        ## 8-byte Reload
	addq	$264, %rdi              ## imm = 0x108
	movq	%rdi, %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table86:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset62 = Lfunc_begin7-Lfunc_begin7      ## >> Call Site 1 <<
	.long	Lset62
Lset63 = Ltmp352-Lfunc_begin7           ##   Call between Lfunc_begin7 and Ltmp352
	.long	Lset63
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset64 = Ltmp352-Lfunc_begin7           ## >> Call Site 2 <<
	.long	Lset64
Lset65 = Ltmp353-Ltmp352                ##   Call between Ltmp352 and Ltmp353
	.long	Lset65
Lset66 = Ltmp354-Lfunc_begin7           ##     jumps to Ltmp354
	.long	Lset66
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcED1Ev
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcED1Ev: ## @_ZN5boost23basic_wrap_stringstreamIcED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp358:
	.cfi_def_cfa_offset 16
Ltmp359:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp360:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boostlsIcA9_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.weak_def_can_be_hidden	__ZN5boostlsIcA9_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.align	4, 0x90
__ZN5boostlsIcA9_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_: ## @_ZN5boostlsIcA9_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp361:
	.cfi_def_cfa_offset 16
Ltmp362:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp363:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rsi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN10MyTestCaseI9MyMessageIN18MyMessageNamespace17ParticularMessageEEE11test_methodEv
	.weak_def_can_be_hidden	__ZN10MyTestCaseI9MyMessageIN18MyMessageNamespace17ParticularMessageEEE11test_methodEv
	.align	4, 0x90
__ZN10MyTestCaseI9MyMessageIN18MyMessageNamespace17ParticularMessageEEE11test_methodEv: ## @_ZN10MyTestCaseI9MyMessageIN18MyMessageNamespace17ParticularMessageEEE11test_methodEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp364:
	.cfi_def_cfa_offset 16
Ltmp365:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp366:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	leaq	-16(%rbp), %rax
	movl	$20, %esi
	movq	%rdi, -8(%rbp)
	movq	%rax, %rdi
	callq	__ZN18MyMessageNamespace17ParticularMessage12SetSomethingILi1EEEvi
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boostlsIcA8_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.weak_def_can_be_hidden	__ZN5boostlsIcA8_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.align	4, 0x90
__ZN5boostlsIcA8_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_: ## @_ZN5boostlsIcA8_cEERNS_23basic_wrap_stringstreamIT_EES5_RKT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp367:
	.cfi_def_cfa_offset 16
Ltmp368:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp369:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	movq	-16(%rbp), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	movq	-8(%rbp), %rsi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rsi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test13basic_cstringIKcEC2ImEEPS2_T_
	.weak_def_can_be_hidden	__ZN5boost9unit_test13basic_cstringIKcEC2ImEEPS2_T_
	.align	4, 0x90
__ZN5boost9unit_test13basic_cstringIKcEC2ImEEPS2_T_: ## @_ZN5boost9unit_test13basic_cstringIKcEC2ImEEPS2_T_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp370:
	.cfi_def_cfa_offset 16
Ltmp371:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp372:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdx
	movq	-16(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	(%rdx), %rsi
	addq	-24(%rbp), %rsi
	movq	%rsi, 8(%rdx)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp373:
	.cfi_def_cfa_offset 16
Ltmp374:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp375:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcE6streamEv
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcE6streamEv: ## @_ZN5boost23basic_wrap_stringstreamIcE6streamEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp376:
	.cfi_def_cfa_offset 16
Ltmp377:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp378:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp400:
	.cfi_def_cfa_offset 16
Ltmp401:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp402:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp379:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp380:
	jmp	LBB94_1
LBB94_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB94_3
	jmp	LBB94_26
LBB94_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB94_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB94_7
LBB94_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB94_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB94_8
	jmp	LBB94_13
LBB94_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp382:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp383:
	jmp	LBB94_9
LBB94_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp384:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp385:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB94_10
LBB94_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp386:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp387:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB94_12
LBB94_11:
Ltmp388:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB94_21
LBB94_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB94_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp389:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp390:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB94_15
LBB94_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB94_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp391:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp392:
	jmp	LBB94_17
LBB94_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB94_18
LBB94_18:
	jmp	LBB94_25
LBB94_19:
Ltmp381:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB94_22
LBB94_20:
Ltmp393:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB94_21
LBB94_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB94_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp394:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp395:
	jmp	LBB94_23
LBB94_23:
	callq	___cxa_end_catch
LBB94_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB94_25:
	jmp	LBB94_26
LBB94_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB94_24
LBB94_27:
Ltmp396:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp397:
	callq	___cxa_end_catch
Ltmp398:
	jmp	LBB94_28
LBB94_28:
	jmp	LBB94_29
LBB94_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB94_30:
Ltmp399:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table94:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset67 = Ltmp379-Lfunc_begin8           ## >> Call Site 1 <<
	.long	Lset67
Lset68 = Ltmp380-Ltmp379                ##   Call between Ltmp379 and Ltmp380
	.long	Lset68
Lset69 = Ltmp381-Lfunc_begin8           ##     jumps to Ltmp381
	.long	Lset69
	.byte	5                       ##   On action: 3
Lset70 = Ltmp382-Lfunc_begin8           ## >> Call Site 2 <<
	.long	Lset70
Lset71 = Ltmp383-Ltmp382                ##   Call between Ltmp382 and Ltmp383
	.long	Lset71
Lset72 = Ltmp393-Lfunc_begin8           ##     jumps to Ltmp393
	.long	Lset72
	.byte	5                       ##   On action: 3
Lset73 = Ltmp384-Lfunc_begin8           ## >> Call Site 3 <<
	.long	Lset73
Lset74 = Ltmp387-Ltmp384                ##   Call between Ltmp384 and Ltmp387
	.long	Lset74
Lset75 = Ltmp388-Lfunc_begin8           ##     jumps to Ltmp388
	.long	Lset75
	.byte	3                       ##   On action: 2
Lset76 = Ltmp389-Lfunc_begin8           ## >> Call Site 4 <<
	.long	Lset76
Lset77 = Ltmp392-Ltmp389                ##   Call between Ltmp389 and Ltmp392
	.long	Lset77
Lset78 = Ltmp393-Lfunc_begin8           ##     jumps to Ltmp393
	.long	Lset78
	.byte	5                       ##   On action: 3
Lset79 = Ltmp392-Lfunc_begin8           ## >> Call Site 5 <<
	.long	Lset79
Lset80 = Ltmp394-Ltmp392                ##   Call between Ltmp392 and Ltmp394
	.long	Lset80
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset81 = Ltmp394-Lfunc_begin8           ## >> Call Site 6 <<
	.long	Lset81
Lset82 = Ltmp395-Ltmp394                ##   Call between Ltmp394 and Ltmp395
	.long	Lset82
Lset83 = Ltmp396-Lfunc_begin8           ##     jumps to Ltmp396
	.long	Lset83
	.byte	0                       ##   On action: cleanup
Lset84 = Ltmp395-Lfunc_begin8           ## >> Call Site 7 <<
	.long	Lset84
Lset85 = Ltmp397-Ltmp395                ##   Call between Ltmp395 and Ltmp397
	.long	Lset85
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp397-Lfunc_begin8           ## >> Call Site 8 <<
	.long	Lset86
Lset87 = Ltmp398-Ltmp397                ##   Call between Ltmp397 and Ltmp398
	.long	Lset87
Lset88 = Ltmp399-Lfunc_begin8           ##     jumps to Ltmp399
	.long	Lset88
	.byte	5                       ##   On action: 3
Lset89 = Ltmp398-Lfunc_begin8           ## >> Call Site 9 <<
	.long	Lset89
Lset90 = Lfunc_end8-Ltmp398             ##   Call between Ltmp398 and Lfunc_end8
	.long	Lset90
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp403:
	.cfi_def_cfa_offset 16
Ltmp404:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp405:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp409:
	.cfi_def_cfa_offset 16
Ltmp410:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp411:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB96_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB96_26
LBB96_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB96_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB96_5
LBB96_4:
	movq	$0, -368(%rbp)
LBB96_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB96_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB96_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB96_26
LBB96_8:
	jmp	LBB96_9
LBB96_9:
	cmpq	$0, -368(%rbp)
	jle	LBB96_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB96_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB96_13
LBB96_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB96_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp406:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp407:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB96_14
LBB96_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB96_15
LBB96_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB96_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB96_19
LBB96_17:
Ltmp408:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB96_27
LBB96_18:
	movl	$0, -416(%rbp)
LBB96_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB96_20
	jmp	LBB96_29
LBB96_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB96_26
	jmp	LBB96_28
LBB96_20:
	jmp	LBB96_21
LBB96_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB96_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB96_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB96_26
LBB96_24:
	jmp	LBB96_25
LBB96_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB96_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB96_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB96_28:
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table96:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset91 = Lfunc_begin9-Lfunc_begin9      ## >> Call Site 1 <<
	.long	Lset91
Lset92 = Ltmp406-Lfunc_begin9           ##   Call between Lfunc_begin9 and Ltmp406
	.long	Lset92
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset93 = Ltmp406-Lfunc_begin9           ## >> Call Site 2 <<
	.long	Lset93
Lset94 = Ltmp407-Ltmp406                ##   Call between Ltmp406 and Ltmp407
	.long	Lset94
Lset95 = Ltmp408-Lfunc_begin9           ##     jumps to Ltmp408
	.long	Lset95
	.byte	0                       ##   On action: cleanup
Lset96 = Ltmp407-Lfunc_begin9           ## >> Call Site 3 <<
	.long	Lset96
Lset97 = Lfunc_end9-Ltmp407             ##   Call between Ltmp407 and Lfunc_end9
	.long	Lset97
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp412:
	.cfi_def_cfa_offset 16
Ltmp413:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp414:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp415:
	.cfi_def_cfa_offset 16
Ltmp416:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp417:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp418:
	.cfi_def_cfa_offset 16
Ltmp419:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp420:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movb	%sil, %al
	leaq	-9(%rbp), %rsi
	movl	$1, %ecx
	movl	%ecx, %edx
	movq	%rdi, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcEC2Ev
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcEC2Ev
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcEC2Ev: ## @_ZN5boost23basic_wrap_stringstreamIcEC2Ev
Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception10
## BB#0:
	pushq	%rbp
Ltmp430:
	.cfi_def_cfa_offset 16
Ltmp431:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp432:
	.cfi_def_cfa_register %rbp
	subq	$512, %rsp              ## imm = 0x200
	movq	%rdi, -432(%rbp)
	movq	%rdi, %rax
	movq	%rdi, -400(%rbp)
	movl	$16, -404(%rbp)
	movq	-400(%rbp), %rdi
	movq	%rdi, %rcx
	addq	$112, %rcx
	movq	%rcx, -392(%rbp)
	movq	%rcx, -384(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 112(%rdi)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 112(%rdi)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	%rdx, (%rdi)
	addq	$64, %rcx
	movq	%rcx, 112(%rdi)
	movq	%rdi, %rcx
	addq	$8, %rcx
	movq	%rdi, -112(%rbp)
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rdx, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rsi, (%rcx)
	movq	8(%rdx), %rdx
	movq	-24(%rsi), %rsi
	movq	%rdx, (%rcx,%rsi)
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	-128(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-96(%rbp), %rcx
Ltmp421:
	movq	%rdi, -440(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	movq	%rax, -448(%rbp)        ## 8-byte Spill
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp422:
	jmp	LBB100_1
LBB100_1:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-456(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-440(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 112(%rdi)
	addq	$8, %rdi
	movl	-404(%rbp), %eax
	orl	$16, %eax
	movq	%rdi, -368(%rbp)
	movl	%eax, -372(%rbp)
	movq	-368(%rbp), %rdx
	movq	%rdx, -312(%rbp)
	movl	%eax, -316(%rbp)
	movq	-312(%rbp), %rdx
Ltmp424:
	movq	%rdx, %rdi
	movq	%rdx, -464(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp425:
	jmp	LBB100_2
LBB100_2:                               ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-464(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %r8
	movq	%r8, -280(%rbp)
	movq	-280(%rbp), %r8
	movq	%r8, -272(%rbp)
	movq	-272(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -264(%rbp)
	movq	%rdi, -472(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -480(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-480(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	movq	-232(%rbp), %rdx
	movq	%rdx, -224(%rbp)
	movq	-224(%rbp), %rdx
	movq	%rdx, -248(%rbp)
	movl	$0, -252(%rbp)
LBB100_3:                               ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -252(%rbp)
	jae	LBB100_5
## BB#4:                                ##   in Loop: Header=BB100_3 Depth=1
	movl	-252(%rbp), %eax
	movl	%eax, %ecx
	movq	-248(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-252(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -252(%rbp)
	jmp	LBB100_3
LBB100_5:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-344(%rbp), %rcx
	movq	-464(%rbp), %rdi        ## 8-byte Reload
	movq	$0, 88(%rdi)
	movl	-316(%rbp), %eax
	movl	%eax, 96(%rdi)
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %r8
	movq	%r8, -192(%rbp)
	movq	-192(%rbp), %r8
	movq	%r8, -184(%rbp)
	movq	-184(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -176(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -488(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-488(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movl	$0, -164(%rbp)
LBB100_6:                               ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -164(%rbp)
	jae	LBB100_8
## BB#7:                                ##   in Loop: Header=BB100_6 Depth=1
	movl	-164(%rbp), %eax
	movl	%eax, %ecx
	movq	-160(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-164(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -164(%rbp)
	jmp	LBB100_6
LBB100_8:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3.i.i.i
Ltmp427:
	leaq	-344(%rbp), %rsi
	movq	-464(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp428:
	jmp	LBB100_14
LBB100_9:
Ltmp429:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)
	movl	%ecx, -356(%rbp)
	leaq	-344(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-472(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-464(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-352(%rbp), %rax
	movl	-356(%rbp), %ecx
	movq	%rax, -496(%rbp)        ## 8-byte Spill
	movl	%ecx, -500(%rbp)        ## 4-byte Spill
	jmp	LBB100_12
LBB100_10:
Ltmp423:
	movl	%edx, %ecx
	movq	%rax, -416(%rbp)
	movl	%ecx, -420(%rbp)
	jmp	LBB100_13
LBB100_11:
Ltmp426:
	movl	%edx, %ecx
	movq	%rax, -496(%rbp)        ## 8-byte Spill
	movl	%ecx, -500(%rbp)        ## 4-byte Spill
	jmp	LBB100_12
LBB100_12:                              ## %.body.i
	movl	-500(%rbp), %eax        ## 4-byte Reload
	movq	-496(%rbp), %rcx        ## 8-byte Reload
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -416(%rbp)
	movl	%eax, -420(%rbp)
	movq	-440(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB100_13:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-416(%rbp), %rdi
	callq	__Unwind_Resume
LBB100_14:                              ## %_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej.exit
	leaq	-344(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	-448(%rbp), %rdi        ## 8-byte Reload
	addq	$264, %rdi              ## imm = 0x108
	movq	%rdi, -88(%rbp)
	movq	-88(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, -48(%rbp)
	movq	%rdi, -512(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	callq	_memset
	movq	-512(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB100_15:                              ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB100_17
## BB#16:                               ##   in Loop: Header=BB100_15 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB100_15
LBB100_17:                              ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit
	addq	$512, %rsp              ## imm = 0x200
	popq	%rbp
	retq
Lfunc_end10:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table100:
Lexception10:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset98 = Ltmp421-Lfunc_begin10          ## >> Call Site 1 <<
	.long	Lset98
Lset99 = Ltmp422-Ltmp421                ##   Call between Ltmp421 and Ltmp422
	.long	Lset99
Lset100 = Ltmp423-Lfunc_begin10         ##     jumps to Ltmp423
	.long	Lset100
	.byte	0                       ##   On action: cleanup
Lset101 = Ltmp424-Lfunc_begin10         ## >> Call Site 2 <<
	.long	Lset101
Lset102 = Ltmp425-Ltmp424               ##   Call between Ltmp424 and Ltmp425
	.long	Lset102
Lset103 = Ltmp426-Lfunc_begin10         ##     jumps to Ltmp426
	.long	Lset103
	.byte	0                       ##   On action: cleanup
Lset104 = Ltmp425-Lfunc_begin10         ## >> Call Site 3 <<
	.long	Lset104
Lset105 = Ltmp427-Ltmp425               ##   Call between Ltmp425 and Ltmp427
	.long	Lset105
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset106 = Ltmp427-Lfunc_begin10         ## >> Call Site 4 <<
	.long	Lset106
Lset107 = Ltmp428-Ltmp427               ##   Call between Ltmp427 and Ltmp428
	.long	Lset107
Lset108 = Ltmp429-Lfunc_begin10         ##     jumps to Ltmp429
	.long	Lset108
	.byte	0                       ##   On action: cleanup
Lset109 = Ltmp428-Lfunc_begin10         ## >> Call Site 5 <<
	.long	Lset109
Lset110 = Lfunc_end10-Ltmp428           ##   Call between Ltmp428 and Lfunc_end10
	.long	Lset110
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp433:
	.cfi_def_cfa_offset 16
Ltmp434:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp435:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$112, %rsi
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp436:
	.cfi_def_cfa_offset 16
Ltmp437:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp438:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp439:
	.cfi_def_cfa_offset 16
Ltmp440:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp441:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp442:
	.cfi_def_cfa_offset 16
Ltmp443:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp444:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp445:
	.cfi_def_cfa_offset 16
Ltmp446:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp447:
	.cfi_def_cfa_register %rbp
	subq	$1312, %rsp             ## imm = 0x520
	movq	%rdi, -1064(%rbp)
	movq	%rsi, -1072(%rbp)
	movq	-1064(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$64, %rdi
	movq	-1072(%rbp), %rax
	movq	%rsi, -1088(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	-1088(%rbp), %rsi       ## 8-byte Reload
	movq	$0, 88(%rsi)
	movl	96(%rsi), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	je	LBB105_14
## BB#1:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rax
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	movq	%rcx, -1024(%rbp)
	movq	-1024(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1104(%rbp)       ## 8-byte Spill
	je	LBB105_3
## BB#2:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rcx
	movq	%rcx, -968(%rbp)
	movq	-968(%rbp), %rcx
	movq	%rcx, -960(%rbp)
	movq	-960(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
	jmp	LBB105_4
LBB105_3:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1016(%rbp)
	movq	-1016(%rbp), %rcx
	movq	%rcx, -1008(%rbp)
	movq	-1008(%rbp), %rcx
	movq	%rcx, -1000(%rbp)
	movq	-1000(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -992(%rbp)
	movq	-992(%rbp), %rcx
	movq	%rcx, -984(%rbp)
	movq	-984(%rbp), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
LBB105_4:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-1112(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -584(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rdx, -560(%rbp)
	movq	-560(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1120(%rbp)       ## 8-byte Spill
	movq	%rcx, -1128(%rbp)       ## 8-byte Spill
	je	LBB105_6
## BB#5:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	movq	%rcx, -512(%rbp)
	movq	-512(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
	jmp	LBB105_7
LBB105_6:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -552(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	%rcx, -536(%rbp)
	movq	-536(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
LBB105_7:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit3
	movq	-1136(%rbp), %rax       ## 8-byte Reload
	movq	-1120(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1144(%rbp)       ## 8-byte Spill
	movq	%rcx, -1152(%rbp)       ## 8-byte Spill
	je	LBB105_9
## BB#8:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
	jmp	LBB105_10
LBB105_9:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
LBB105_10:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit7
	movq	-1160(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1168(%rbp)       ## 8-byte Spill
	movq	%rcx, -1176(%rbp)       ## 8-byte Spill
	je	LBB105_12
## BB#11:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
	jmp	LBB105_13
LBB105_12:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
LBB105_13:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	-1184(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	88(%rcx), %rdx
	movq	-1144(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -232(%rbp)
	movq	-1168(%rbp), %rdi       ## 8-byte Reload
	movq	%rdi, -240(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-256(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB105_14:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB105_36
## BB#15:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1192(%rbp)       ## 8-byte Spill
	je	LBB105_17
## BB#16:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
	jmp	LBB105_18
LBB105_17:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
LBB105_18:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit5
	movq	-1200(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1080(%rbp)
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1208(%rbp)       ## 8-byte Spill
	je	LBB105_20
## BB#19:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
	jmp	LBB105_21
LBB105_20:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
LBB105_21:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4
	movq	-1216(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rax
	addq	-1080(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	%rax, 88(%rcx)
	addq	$64, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	-496(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -1224(%rbp)       ## 8-byte Spill
	movq	%rax, -1232(%rbp)       ## 8-byte Spill
	je	LBB105_23
## BB#22:
	movq	-1232(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB105_24
LBB105_23:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB105_24
LBB105_24:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-1240(%rbp), %rax       ## 8-byte Reload
	xorl	%edx, %edx
	subq	$1, %rax
	movq	-1224(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movq	-592(%rbp), %rdi
	movq	-600(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -712(%rbp)
	movq	-712(%rbp), %rcx
	movq	%rcx, -704(%rbp)
	movq	-704(%rbp), %rcx
	movq	%rcx, -696(%rbp)
	movq	-696(%rbp), %rsi
	movq	%rsi, -688(%rbp)
	movq	-688(%rbp), %rsi
	movq	%rsi, -680(%rbp)
	movq	-680(%rbp), %rsi
	movzbl	(%rsi), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1248(%rbp)       ## 8-byte Spill
	movq	%rcx, -1256(%rbp)       ## 8-byte Spill
	je	LBB105_26
## BB#25:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -632(%rbp)
	movq	-632(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-624(%rbp), %rcx
	movq	%rcx, -616(%rbp)
	movq	-616(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
	jmp	LBB105_27
LBB105_26:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -672(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	-664(%rbp), %rcx
	movq	%rcx, -656(%rbp)
	movq	-656(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -648(%rbp)
	movq	-648(%rbp), %rcx
	movq	%rcx, -640(%rbp)
	movq	-640(%rbp), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
LBB105_27:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-1264(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -824(%rbp)
	movq	-824(%rbp), %rcx
	movq	%rcx, -816(%rbp)
	movq	-816(%rbp), %rcx
	movq	%rcx, -808(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	-800(%rbp), %rdx
	movq	%rdx, -792(%rbp)
	movq	-792(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1272(%rbp)       ## 8-byte Spill
	movq	%rcx, -1280(%rbp)       ## 8-byte Spill
	je	LBB105_29
## BB#28:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -744(%rbp)
	movq	-744(%rbp), %rcx
	movq	%rcx, -736(%rbp)
	movq	-736(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	-728(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
	jmp	LBB105_30
LBB105_29:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -776(%rbp)
	movq	-776(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	-768(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -760(%rbp)
	movq	-760(%rbp), %rcx
	movq	%rcx, -752(%rbp)
	movq	-752(%rbp), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
LBB105_30:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-1288(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -720(%rbp)
	movq	-720(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -904(%rbp)
	movq	-904(%rbp), %rcx
	movq	%rcx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1296(%rbp)       ## 8-byte Spill
	movq	%rcx, -1304(%rbp)       ## 8-byte Spill
	je	LBB105_32
## BB#31:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movq	-848(%rbp), %rcx
	movq	%rcx, -840(%rbp)
	movq	-840(%rbp), %rcx
	movq	%rcx, -832(%rbp)
	movq	-832(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
	jmp	LBB105_33
LBB105_32:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -872(%rbp)
	movq	-872(%rbp), %rcx
	movq	%rcx, -864(%rbp)
	movq	-864(%rbp), %rcx
	movq	%rcx, -856(%rbp)
	movq	-856(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
LBB105_33:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-1312(%rbp), %rax       ## 8-byte Reload
	movq	-1296(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1248(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -912(%rbp)
	movq	-1272(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -920(%rbp)
	movq	%rcx, -928(%rbp)
	movq	-912(%rbp), %rcx
	movq	-920(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-928(%rbp), %rsi
	movq	%rsi, 56(%rcx)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movl	96(%rcx), %edi
	andl	$3, %edi
	cmpl	$0, %edi
	je	LBB105_35
## BB#34:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1080(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -936(%rbp)
	movl	%edx, -940(%rbp)
	movq	-936(%rbp), %rax
	movl	-940(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
LBB105_35:
	jmp	LBB105_36
LBB105_36:
	addq	$1312, %rsp             ## imm = 0x520
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp448:
	.cfi_def_cfa_offset 16
Ltmp449:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp450:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp451:
	.cfi_def_cfa_offset 16
Ltmp452:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp453:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp454:
	.cfi_def_cfa_offset 16
Ltmp455:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp456:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movl	%ecx, -636(%rbp)
	movl	%r8d, -640(%rbp)
	movq	-624(%rbp), %rdx
	movq	88(%rdx), %rsi
	movq	%rdx, %r9
	movq	%r9, -616(%rbp)
	movq	-616(%rbp), %r9
	cmpq	48(%r9), %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdi, -664(%rbp)        ## 8-byte Spill
	movq	%rdx, -672(%rbp)        ## 8-byte Spill
	jae	LBB108_2
## BB#1:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB108_2:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$0, %eax
	jne	LBB108_4
## BB#3:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	$-1, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %r8
	movq	%rdi, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-24(%rbp), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_4:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$24, %eax
	jne	LBB108_7
## BB#5:
	cmpl	$1, -636(%rbp)
	jne	LBB108_7
## BB#6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	$-1, -72(%rbp)
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r8
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-56(%rbp), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_7:
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -692(%rbp)        ## 4-byte Spill
	je	LBB108_8
	jmp	LBB108_38
LBB108_38:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -696(%rbp)        ## 4-byte Spill
	je	LBB108_9
	jmp	LBB108_39
LBB108_39:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -700(%rbp)        ## 4-byte Spill
	je	LBB108_13
	jmp	LBB108_17
LBB108_8:
	movq	$0, -648(%rbp)
	jmp	LBB108_18
LBB108_9:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB108_11
## BB#10:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	24(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	jmp	LBB108_12
LBB108_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	40(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
LBB108_12:
	jmp	LBB108_18
LBB108_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -712(%rbp)        ## 8-byte Spill
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	je	LBB108_15
## BB#14:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB108_16
LBB108_15:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB108_16:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, -648(%rbp)
	jmp	LBB108_18
LBB108_17:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	$-1, -248(%rbp)
	movq	-240(%rbp), %rdi
	movq	-248(%rbp), %r8
	movq	%rdi, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -736(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-232(%rbp), %rcx
	movq	-736(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_18:
	movq	-632(%rbp), %rax
	addq	-648(%rbp), %rax
	movq	%rax, -648(%rbp)
	cmpq	$0, -648(%rbp)
	jl	LBB108_23
## BB#19:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	je	LBB108_21
## BB#20:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
	jmp	LBB108_22
LBB108_21:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
LBB108_22:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	-744(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	cmpq	-648(%rbp), %rcx
	jge	LBB108_24
LBB108_23:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -384(%rbp)
	movq	$-1, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	-392(%rbp), %r8
	movq	%rdi, -368(%rbp)
	movq	%r8, -376(%rbp)
	movq	-368(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-376(%rbp), %rcx
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_24:
	cmpq	$0, -648(%rbp)
	je	LBB108_32
## BB#25:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB108_28
## BB#26:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	-400(%rbp), %rax
	cmpq	$0, 24(%rax)
	jne	LBB108_28
## BB#27:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-776(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_28:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB108_31
## BB#29:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB108_31
## BB#30:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -464(%rbp)
	movq	$-1, -472(%rbp)
	movq	-464(%rbp), %rdi
	movq	-472(%rbp), %r8
	movq	%rdi, -448(%rbp)
	movq	%r8, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -784(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-456(%rbp), %rcx
	movq	-784(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB108_37
LBB108_31:
	jmp	LBB108_32
LBB108_32:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB108_34
## BB#33:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-648(%rbp), %rdx
	movq	-672(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -496(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdi, -520(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-520(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB108_34:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB108_36
## BB#35:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -536(%rbp)
	movq	-536(%rbp), %rdx
	movq	56(%rdx), %rdx
	movq	%rax, -544(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-560(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-648(%rbp), %rcx
	movl	%ecx, %esi
	movq	%rax, -568(%rbp)
	movl	%esi, -572(%rbp)
	movq	-568(%rbp), %rax
	movl	-572(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB108_36:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-648(%rbp), %rcx
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	-600(%rbp), %rcx
	movq	-608(%rbp), %r8
	movq	%rcx, -584(%rbp)
	movq	%r8, -592(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, %rdi
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-592(%rbp), %rcx
	movq	-792(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
LBB108_37:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp457:
	.cfi_def_cfa_offset 16
Ltmp458:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp459:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-16(%rbp), %rsi
	movq	(%rsi), %r9
	movq	32(%r9), %r9
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	128(%rcx), %rdx
	movl	-20(%rbp), %r10d
	movl	%r8d, %ecx
	movl	%r10d, %r8d
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	*%r9
	movq	-32(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp460:
	.cfi_def_cfa_offset 16
Ltmp461:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp462:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jae	LBB110_2
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	48(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB110_2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB110_8
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	cmpq	88(%rcx), %rax
	jae	LBB110_5
## BB#4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-120(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	24(%rdx), %rdx
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-40(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB110_5:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	24(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	32(%rcx), %rax
	jae	LBB110_7
## BB#6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -100(%rbp)
	jmp	LBB110_9
LBB110_7:
	jmp	LBB110_8
LBB110_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -100(%rbp)
LBB110_9:
	movl	-100(%rbp), %eax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp463:
	.cfi_def_cfa_offset 16
Ltmp464:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp465:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -160(%rbp)
	movl	%esi, -164(%rbp)
	movq	-160(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -176(%rbp)        ## 8-byte Spill
	jae	LBB111_2
## BB#1:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	48(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB111_2:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB111_9
## BB#3:
	movl	-164(%rbp), %edi
	movl	%edi, -180(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-180(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB111_4
	jmp	LBB111_5
LBB111_4:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-32(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -148(%rbp)
	jmp	LBB111_10
LBB111_5:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB111_7
## BB#6:
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB111_7
	jmp	LBB111_8
LBB111_7:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movb	%al, (%rcx)
	movl	-164(%rbp), %edi
	movl	%edi, -148(%rbp)
	jmp	LBB111_10
LBB111_8:
	jmp	LBB111_9
LBB111_9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -148(%rbp)
LBB111_10:
	movl	-148(%rbp), %eax
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception11
## BB#0:
	pushq	%rbp
Ltmp471:
	.cfi_def_cfa_offset 16
Ltmp472:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp473:
	.cfi_def_cfa_register %rbp
	subq	$896, %rsp              ## imm = 0x380
	movq	%rdi, -632(%rbp)
	movl	%esi, -636(%rbp)
	movq	-632(%rbp), %rdi
	movl	-636(%rbp), %esi
	movq	%rdi, -712(%rbp)        ## 8-byte Spill
	movl	%esi, -716(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-716(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB112_38
## BB#1:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -616(%rbp)
	movq	-616(%rbp), %rax
	movq	24(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -608(%rbp)
	movq	-608(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -576(%rbp)
	movq	-576(%rbp), %rax
	movq	48(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -568(%rbp)
	movq	-568(%rbp), %rcx
	cmpq	56(%rcx), %rax
	jne	LBB112_26
## BB#2:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB112_4
## BB#3:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	jmp	LBB112_39
LBB112_4:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
## BB#6:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	-736(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -656(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rdx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
## BB#7:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	-752(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -680(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
Ltmp466:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp467:
	jmp	LBB112_8
LBB112_8:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	movq	%rcx, -768(%rbp)        ## 8-byte Spill
	je	LBB112_10
## BB#9:
	movq	-768(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB112_11
LBB112_10:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB112_11
LBB112_11:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-776(%rbp), %rax        ## 8-byte Reload
	decq	%rax
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rdi
Ltmp468:
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp469:
	jmp	LBB112_12
LBB112_12:                              ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	jmp	LBB112_13
LBB112_13:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	je	LBB112_15
## BB#14:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	jmp	LBB112_16
LBB112_15:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
LBB112_16:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-688(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	addq	$64, %rsi
	movq	%rsi, -272(%rbp)
	movq	-272(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movq	-264(%rbp), %rdi
	movq	%rdi, -256(%rbp)
	movq	-256(%rbp), %rdi
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	movzbl	(%rdi), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rcx, -808(%rbp)        ## 8-byte Spill
	movq	%rdx, -816(%rbp)        ## 8-byte Spill
	movq	%rsi, -824(%rbp)        ## 8-byte Spill
	je	LBB112_18
## BB#17:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
	jmp	LBB112_19
LBB112_18:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
LBB112_19:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-832(%rbp), %rax        ## 8-byte Reload
	movq	-816(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-800(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-808(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-296(%rbp), %rsi
	movq	%rsi, 56(%rcx)
## BB#20:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-656(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -304(%rbp)
	movl	%edx, -308(%rbp)
	movq	-304(%rbp), %rax
	movl	-308(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
## BB#21:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -840(%rbp)        ## 8-byte Spill
## BB#22:
	movq	-840(%rbp), %rax        ## 8-byte Reload
	addq	-680(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	jmp	LBB112_25
LBB112_23:
Ltmp470:
	movl	%edx, %ecx
	movq	%rax, -664(%rbp)
	movl	%ecx, -668(%rbp)
## BB#24:
	movq	-664(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -848(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	callq	___cxa_end_catch
	jmp	LBB112_39
LBB112_25:
	jmp	LBB112_26
LBB112_26:
	leaq	-368(%rbp), %rax
	leaq	-696(%rbp), %rcx
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	48(%rdx), %rdx
	addq	$1, %rdx
	movq	%rdx, -696(%rbp)
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	addq	$88, %rdx
	movq	%rcx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rax, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	-360(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB112_28
## BB#27:
	movq	-384(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	jmp	LBB112_29
LBB112_28:
	movq	-376(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
LBB112_29:                              ## %_ZNSt3__13maxIPcEERKT_S4_S4_.exit
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	movl	96(%rcx), %edx
	andl	$8, %edx
	cmpl	$0, %edx
	je	LBB112_34
## BB#30:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -520(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	-496(%rbp), %rcx
	movq	%rcx, -488(%rbp)
	movq	-488(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	je	LBB112_32
## BB#31:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
	jmp	LBB112_33
LBB112_32:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
LBB112_33:                              ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-872(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -704(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rcx
	movq	-704(%rbp), %rdx
	addq	-648(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -528(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	movq	%rdi, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB112_34:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	-636(%rbp), %ecx
	movb	%cl, %dl
	movq	%rax, -592(%rbp)
	movb	%dl, -593(%rbp)
	movq	-592(%rbp), %rax
	movq	48(%rax), %rsi
	cmpq	56(%rax), %rsi
	movq	%rax, -880(%rbp)        ## 8-byte Spill
	jne	LBB112_36
## BB#35:
	movq	-880(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movsbl	-593(%rbp), %edi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movq	-880(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -580(%rbp)
	jmp	LBB112_37
LBB112_36:
	movb	-593(%rbp), %al
	movq	-880(%rbp), %rcx        ## 8-byte Reload
	movq	48(%rcx), %rdx
	movq	%rdx, %rsi
	addq	$1, %rsi
	movq	%rsi, 48(%rcx)
	movb	%al, (%rdx)
	movsbl	-593(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -580(%rbp)
LBB112_37:                              ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	-580(%rbp), %eax
	movl	%eax, -620(%rbp)
	jmp	LBB112_39
LBB112_38:
	movl	-636(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -620(%rbp)
LBB112_39:
	movl	-620(%rbp), %eax
	addq	$896, %rsp              ## imm = 0x380
	popq	%rbp
	retq
Lfunc_end11:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table112:
Lexception11:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset111 = Ltmp466-Lfunc_begin11         ## >> Call Site 1 <<
	.long	Lset111
Lset112 = Ltmp469-Ltmp466               ##   Call between Ltmp466 and Ltmp469
	.long	Lset112
Lset113 = Ltmp470-Lfunc_begin11         ##     jumps to Ltmp470
	.long	Lset113
	.byte	1                       ##   On action: 1
Lset114 = Ltmp469-Lfunc_begin11         ## >> Call Site 2 <<
	.long	Lset114
Lset115 = Lfunc_end11-Ltmp469           ##   Call between Ltmp469 and Lfunc_end11
	.long	Lset115
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp474:
	.cfi_def_cfa_offset 16
Ltmp475:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp476:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$64, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp477:
	.cfi_def_cfa_offset 16
Ltmp478:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp479:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp480:
	.cfi_def_cfa_offset 16
Ltmp481:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp482:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB115_1
	jmp	LBB115_2
LBB115_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB115_3
LBB115_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB115_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp483:
	.cfi_def_cfa_offset 16
Ltmp484:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp485:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp486:
	.cfi_def_cfa_offset 16
Ltmp487:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp488:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp489:
	.cfi_def_cfa_offset 16
Ltmp490:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp491:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$8, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6assignERcRKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6assignERcRKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6assignERcRKc: ## @_ZNSt3__111char_traitsIcE6assignERcRKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp492:
	.cfi_def_cfa_offset 16
Ltmp493:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp494:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movb	(%rsi), %al
	movq	-8(%rbp), %rsi
	movb	%al, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.weak_def_can_be_hidden	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.align	4, 0x90
__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv: ## @_ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp495:
	.cfi_def_cfa_offset 16
Ltmp496:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp497:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$712, %rsp              ## imm = 0x2C8
Ltmp498:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movl	96(%rsi), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	movq	%rax, -672(%rbp)        ## 8-byte Spill
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%rsi, -688(%rbp)        ## 8-byte Spill
	je	LBB120_4
## BB#1:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	cmpq	48(%rax), %rcx
	jae	LBB120_3
## BB#2:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	48(%rax), %rax
	movq	-688(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB120_3:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-72(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	leaq	-624(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -56(%rbp)
	movq	-56(%rbp), %r9
	movq	40(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	88(%r10), %r11
	addq	$64, %r10
	movq	%r10, -48(%rbp)
	movq	-48(%rbp), %r10
	movq	%r10, -32(%rbp)
	movq	-32(%rbp), %r10
	movq	%r10, -24(%rbp)
	movq	-24(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-680(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -176(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	-200(%rbp), %rbx
	movq	%r8, -136(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r11, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movq	-136(%rbp), %r8
	movq	%r8, -128(%rbp)
	movq	-128(%rbp), %r9
	movq	%r9, -104(%rbp)
	movq	-104(%rbp), %r9
	movq	%rdi, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -696(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-144(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	-696(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB120_11
LBB120_4:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB120_6
## BB#5:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-280(%rbp), %rcx
	leaq	-304(%rbp), %rdi
	leaq	-640(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	16(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	32(%r10), %r10
	movq	-688(%rbp), %r11        ## 8-byte Reload
	addq	$64, %r11
	movq	%r11, -256(%rbp)
	movq	-256(%rbp), %r11
	movq	%r11, -240(%rbp)
	movq	-240(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r11, -224(%rbp)
	movq	-680(%rbp), %r11        ## 8-byte Reload
	movq	%r11, -384(%rbp)
	movq	%r9, -392(%rbp)
	movq	%r10, -400(%rbp)
	movq	%r8, -408(%rbp)
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %r9
	movq	-400(%rbp), %r10
	movq	-408(%rbp), %rbx
	movq	%r8, -344(%rbp)
	movq	%r9, -352(%rbp)
	movq	%r10, -360(%rbp)
	movq	%rbx, -368(%rbp)
	movq	-344(%rbp), %r8
	movq	%r8, -336(%rbp)
	movq	-336(%rbp), %r9
	movq	%r9, -312(%rbp)
	movq	-312(%rbp), %r9
	movq	%rdi, -296(%rbp)
	movq	%r9, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rcx, -272(%rbp)
	movq	%r8, -704(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-352(%rbp), %rsi
	movq	-360(%rbp), %rdx
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB120_11
LBB120_6:
	jmp	LBB120_7
LBB120_7:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-504(%rbp), %rcx
	leaq	-528(%rbp), %rdi
	leaq	-656(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	addq	$64, %r9
	movq	%r9, -448(%rbp)
	movq	-448(%rbp), %r9
	movq	%r9, -432(%rbp)
	movq	-432(%rbp), %r9
	movq	%r9, -424(%rbp)
	movq	-424(%rbp), %r9
	movq	%r9, -416(%rbp)
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -592(%rbp)
	movq	%r8, -600(%rbp)
	movq	-592(%rbp), %r8
	movq	-600(%rbp), %r10
	movq	%r8, -568(%rbp)
	movq	%r10, -576(%rbp)
	movq	-568(%rbp), %r8
	movq	%r8, -560(%rbp)
	movq	-560(%rbp), %r10
	movq	%r10, -536(%rbp)
	movq	-536(%rbp), %r10
	movq	%rdi, -520(%rbp)
	movq	%r10, -512(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rcx, -496(%rbp)
	movq	%r8, -712(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -464(%rbp)
	movq	-464(%rbp), %rdx
	movq	%rdx, -456(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movl	$0, -484(%rbp)
LBB120_8:                               ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -484(%rbp)
	jae	LBB120_10
## BB#9:                                ##   in Loop: Header=BB120_8 Depth=1
	movl	-484(%rbp), %eax
	movl	%eax, %ecx
	movq	-480(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-484(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -484(%rbp)
	jmp	LBB120_8
LBB120_10:                              ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS4_.exit
	jmp	LBB120_11
LBB120_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	addq	$712, %rsp              ## imm = 0x2C8
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.weak_def_can_be_hidden	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.align	4, 0x90
__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_: ## @_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp499:
	.cfi_def_cfa_offset 16
Ltmp500:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp501:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -392(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	-392(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movq	-408(%rbp), %rdi
	movq	%rsi, -368(%rbp)
	movq	%rdi, -376(%rbp)
	movq	-368(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%rsi, -352(%rbp)
	movq	%rdi, -360(%rbp)
	movq	-360(%rbp), %rsi
	movq	-352(%rbp), %rdi
	subq	%rdi, %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rdi, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -264(%rbp)
	movq	$-1, -344(%rbp)
	movq	-344(%rbp), %rdi
	subq	$16, %rdi
	cmpq	%rdi, %rsi
	movq	%rdx, -448(%rbp)        ## 8-byte Spill
	jbe	LBB121_2
## BB#1:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__121__basic_string_commonILb1EE20__throw_length_errorEv
LBB121_2:
	cmpq	$23, -416(%rbp)
	jae	LBB121_4
## BB#3:
	movq	-416(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdx
	shlq	$1, %rdx
	movb	%dl, %sil
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rax
	movb	%sil, (%rax)
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -424(%rbp)
	jmp	LBB121_8
LBB121_4:
	movq	-416(%rbp), %rax
	movq	%rax, -16(%rbp)
	cmpq	$23, -16(%rbp)
	jae	LBB121_6
## BB#5:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	jmp	LBB121_7
LBB121_6:
	movq	-16(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$15, %rax
	andq	$-16, %rax
	movq	%rax, -456(%rbp)        ## 8-byte Spill
LBB121_7:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm.exit
	movq	-456(%rbp), %rax        ## 8-byte Reload
	subq	$1, %rax
	movq	%rax, -432(%rbp)
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	-432(%rbp), %rdx
	addq	$1, %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-432(%rbp), %rax
	addq	$1, %rax
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rdx
	orq	$1, %rdx
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-416(%rbp), %rax
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rdx, 8(%rax)
LBB121_8:
	jmp	LBB121_9
LBB121_9:                               ## =>This Inner Loop Header: Depth=1
	movq	-400(%rbp), %rax
	cmpq	-408(%rbp), %rax
	je	LBB121_12
## BB#10:                               ##   in Loop: Header=BB121_9 Depth=1
	movq	-424(%rbp), %rdi
	movq	-400(%rbp), %rsi
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
## BB#11:                               ##   in Loop: Header=BB121_9 Depth=1
	movq	-400(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -400(%rbp)
	movq	-424(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -424(%rbp)
	jmp	LBB121_9
LBB121_12:
	leaq	-433(%rbp), %rsi
	movq	-424(%rbp), %rdi
	movb	$0, -433(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost23basic_wrap_stringstreamIcED2Ev
	.weak_def_can_be_hidden	__ZN5boost23basic_wrap_stringstreamIcED2Ev
	.align	4, 0x90
__ZN5boost23basic_wrap_stringstreamIcED2Ev: ## @_ZN5boost23basic_wrap_stringstreamIcED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp502:
	.cfi_def_cfa_offset 16
Ltmp503:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp504:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$264, %rax              ## imm = 0x108
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN18MyMessageNamespace17ParticularMessage12SetSomethingILi1EEEvi
	.weak_def_can_be_hidden	__ZN18MyMessageNamespace17ParticularMessage12SetSomethingILi1EEEvi
	.align	4, 0x90
__ZN18MyMessageNamespace17ParticularMessage12SetSomethingILi1EEEvi: ## @_ZN18MyMessageNamespace17ParticularMessage12SetSomethingILi1EEEvi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp505:
	.cfi_def_cfa_offset 16
Ltmp506:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp507:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferENS1_16function_obj_tagE
	.weak_def_can_be_hidden	__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferENS1_16function_obj_tagE
	.align	4, 0x90
__ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferENS1_16function_obj_tagE: ## @_ZNK5boost6detail8function13basic_vtable0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEbT_RNS1_15function_bufferENS1_16function_obj_tagE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp508:
	.cfi_def_cfa_offset 16
Ltmp509:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp510:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	leaq	-72(%rbp), %rax
	leaq	-80(%rbp), %rcx
	movq	%rdi, -96(%rbp)
	movq	%rsi, -104(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rax, -48(%rbp)
	movq	%rcx, -56(%rbp)
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %rsi
	movq	%rcx, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	-40(%rbp), %rsi
	movq	%rsi, (%rcx)
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -16(%rbp)
	movq	$0, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	%rdi, -128(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movb	$0, %al
	callq	__ZN5boost6detail8function16has_empty_targetEz
	testb	$1, %al
	jne	LBB124_2
## BB#1:
	movq	-104(%rbp), %rsi
	movq	-128(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK5boost6detail8function13basic_vtable0IvE14assign_functorINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_RNS1_15function_bufferEN4mpl_5bool_ILb1EEE
	movb	$1, -73(%rbp)
	jmp	LBB124_3
LBB124_2:
	movb	$0, -73(%rbp)
LBB124_3:
	movb	-73(%rbp), %al
	andb	$1, %al
	movzbl	%al, %eax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail8function16has_empty_targetEz
	.weak_def_can_be_hidden	__ZN5boost6detail8function16has_empty_targetEz
	.align	4, 0x90
__ZN5boost6detail8function16has_empty_targetEz: ## @_ZN5boost6detail8function16has_empty_targetEz
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp511:
	.cfi_def_cfa_offset 16
Ltmp512:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp513:
	.cfi_def_cfa_register %rbp
	xorl	%eax, %eax
	movb	%al, %cl
	movb	%cl, %al
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost6detail8function13basic_vtable0IvE14assign_functorINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_RNS1_15function_bufferEN4mpl_5bool_ILb1EEE
	.weak_def_can_be_hidden	__ZNK5boost6detail8function13basic_vtable0IvE14assign_functorINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_RNS1_15function_bufferEN4mpl_5bool_ILb1EEE
	.align	4, 0x90
__ZNK5boost6detail8function13basic_vtable0IvE14assign_functorINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_RNS1_15function_bufferEN4mpl_5bool_ILb1EEE: ## @_ZNK5boost6detail8function13basic_vtable0IvE14assign_functorINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_RNS1_15function_bufferEN4mpl_5bool_ILb1EEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp514:
	.cfi_def_cfa_offset 16
Ltmp515:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp516:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost8functionIFvvEED2Ev
	.weak_def_can_be_hidden	__ZN5boost8functionIFvvEED2Ev
	.align	4, 0x90
__ZN5boost8functionIFvvEED2Ev:          ## @_ZN5boost8functionIFvvEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp517:
	.cfi_def_cfa_offset 16
Ltmp518:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp519:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9function0IvED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9function0IvED2Ev
	.weak_def_can_be_hidden	__ZN5boost9function0IvED2Ev
	.align	4, 0x90
__ZN5boost9function0IvED2Ev:            ## @_ZN5boost9function0IvED2Ev
Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception12
## BB#0:
	pushq	%rbp
Ltmp523:
	.cfi_def_cfa_offset 16
Ltmp524:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp525:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
Ltmp520:
	callq	__ZN5boost9function0IvE5clearEv
Ltmp521:
	jmp	LBB128_1
LBB128_1:
	addq	$16, %rsp
	popq	%rbp
	retq
LBB128_2:
Ltmp522:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -12(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end12:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table128:
Lexception12:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset116 = Ltmp520-Lfunc_begin12         ## >> Call Site 1 <<
	.long	Lset116
Lset117 = Ltmp521-Ltmp520               ##   Call between Ltmp520 and Ltmp521
	.long	Lset117
Lset118 = Ltmp522-Lfunc_begin12         ##     jumps to Ltmp522
	.long	Lset118
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9function0IvE5clearEv
	.weak_def_can_be_hidden	__ZN5boost9function0IvE5clearEv
	.align	4, 0x90
__ZN5boost9function0IvE5clearEv:        ## @_ZN5boost9function0IvE5clearEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp526:
	.cfi_def_cfa_offset 16
Ltmp527:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp528:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	je	LBB129_4
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNK5boost13function_base28has_trivial_copy_and_destroyEv
	testb	$1, %al
	jne	LBB129_3
## BB#2:
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNK5boost9function0IvE10get_vtableEv
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	addq	$8, %rdi
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	-24(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNK5boost6detail8function13basic_vtable0IvE5clearERNS1_15function_bufferE
LBB129_3:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	$0, (%rax)
LBB129_4:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost13function_base28has_trivial_copy_and_destroyEv
	.weak_def_can_be_hidden	__ZNK5boost13function_base28has_trivial_copy_and_destroyEv
	.align	4, 0x90
__ZNK5boost13function_base28has_trivial_copy_and_destroyEv: ## @_ZNK5boost13function_base28has_trivial_copy_and_destroyEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp529:
	.cfi_def_cfa_offset 16
Ltmp530:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp531:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	andq	$1, %rdi
	cmpq	$0, %rdi
	setne	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost9function0IvE10get_vtableEv
	.weak_def_can_be_hidden	__ZNK5boost9function0IvE10get_vtableEv
	.align	4, 0x90
__ZNK5boost9function0IvE10get_vtableEv: ## @_ZNK5boost9function0IvE10get_vtableEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp532:
	.cfi_def_cfa_offset 16
Ltmp533:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp534:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	andq	$-2, %rdi
	movq	%rdi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost6detail8function13basic_vtable0IvE5clearERNS1_15function_bufferE
	.weak_def_can_be_hidden	__ZNK5boost6detail8function13basic_vtable0IvE5clearERNS1_15function_bufferE
	.align	4, 0x90
__ZNK5boost6detail8function13basic_vtable0IvE5clearERNS1_15function_bufferE: ## @_ZNK5boost6detail8function13basic_vtable0IvE5clearERNS1_15function_bufferE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp535:
	.cfi_def_cfa_offset 16
Ltmp536:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp537:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	cmpq	$0, (%rsi)
	movq	%rsi, -24(%rbp)         ## 8-byte Spill
	je	LBB132_2
## BB#1:
	movl	$2, %edx
	movq	-24(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-16(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	*%rcx
LBB132_2:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.weak_def_can_be_hidden	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.align	4, 0x90
__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv: ## @_ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp538:
	.cfi_def_cfa_offset 16
Ltmp539:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp540:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE4dataEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp541:
	.cfi_def_cfa_offset 16
Ltmp542:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp543:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	.weak_def_can_be_hidden	__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	.align	4, 0x90
__ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev: ## @_ZN5boost17value_initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp544:
	.cfi_def_cfa_offset 16
Ltmp545:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp546:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp547:
	.cfi_def_cfa_offset 16
Ltmp548:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp549:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	.weak_def_can_be_hidden	__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
	.align	4, 0x90
__ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev: ## @_ZN5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEED2Ev
Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception13
## BB#0:
	pushq	%rbp
Ltmp553:
	.cfi_def_cfa_offset 16
Ltmp554:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp555:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
Ltmp550:
	callq	__ZNK5boost11initializedINS_3mpl8identityI9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE15wrapper_addressEv
Ltmp551:
	movq	%rax, -16(%rbp)         ## 8-byte Spill
	jmp	LBB137_1
LBB137_1:
	addq	$32, %rsp
	popq	%rbp
	retq
LBB137_2:
Ltmp552:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -20(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end13:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table137:
Lexception13:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset119 = Ltmp550-Lfunc_begin13         ## >> Call Site 1 <<
	.long	Lset119
Lset120 = Ltmp551-Ltmp550               ##   Call between Ltmp550 and Ltmp551
	.long	Lset120
Lset121 = Ltmp552-Lfunc_begin13         ##     jumps to Ltmp552
	.long	Lset121
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC2ENS0_13basic_cstringIKcEESI_mRSE_
	.weak_def_can_be_hidden	__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC2ENS0_13basic_cstringIKcEESI_mRSE_
	.align	4, 0x90
__ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC2ENS0_13basic_cstringIKcEESI_mRSE_: ## @_ZN5boost9unit_test9ut_detail25generate_test_case_4_typeINS1_22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_SC_EEEES4_EC2ENS0_13basic_cstringIKcEESI_mRSE_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp556:
	.cfi_def_cfa_offset 16
Ltmp557:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp558:
	.cfi_def_cfa_register %rbp
	movq	16(%rbp), %rax
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -24(%rbp)
	movq	%rdi, -40(%rbp)
	movq	%r9, -48(%rbp)
	movq	%rax, -56(%rbp)
	movq	-40(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-8(%rbp), %rcx
	movq	%rcx, 8(%rax)
	movq	-32(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movq	-56(%rbp), %rcx
	movq	%rcx, 40(%rax)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9decorator9collectorC1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9decorator9collectorC1Ev
	.align	4, 0x90
__ZN5boost9unit_test9decorator9collectorC1Ev: ## @_ZN5boost9unit_test9decorator9collectorC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp559:
	.cfi_def_cfa_offset 16
Ltmp560:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp561:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test9decorator9collectorC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9decorator9collectorD1Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9decorator9collectorD1Ev
	.align	4, 0x90
__ZN5boost9unit_test9decorator9collectorD1Ev: ## @_ZN5boost9unit_test9decorator9collectorD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp562:
	.cfi_def_cfa_offset 16
Ltmp563:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp564:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost9unit_test9decorator9collectorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9decorator9collectorC2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9decorator9collectorC2Ev
	.align	4, 0x90
__ZN5boost9unit_test9decorator9collectorC2Ev: ## @_ZN5boost9unit_test9decorator9collectorC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp565:
	.cfi_def_cfa_offset 16
Ltmp566:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp567:
	.cfi_def_cfa_register %rbp
	leaq	-32(%rbp), %rax
	leaq	-56(%rbp), %rcx
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -88(%rbp)
	movq	-88(%rbp), %rdi
	movq	%rdi, %rdx
	movq	%rdx, -80(%rbp)
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	addq	$16, %rdi
	movq	%rdi, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	movq	%rdi, -56(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, %rdx
	movq	%rdx, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost9unit_test9decorator9collectorD2Ev
	.weak_def_can_be_hidden	__ZN5boost9unit_test9decorator9collectorD2Ev
	.align	4, 0x90
__ZN5boost9unit_test9decorator9collectorD2Ev: ## @_ZN5boost9unit_test9decorator9collectorD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp568:
	.cfi_def_cfa_offset 16
Ltmp569:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp570:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED1Ev: ## @_ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp571:
	.cfi_def_cfa_offset 16
Ltmp572:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp573:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev: ## @_ZNSt3__16vectorIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp574:
	.cfi_def_cfa_offset 16
Ltmp575:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp576:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev: ## @_ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp577:
	.cfi_def_cfa_offset 16
Ltmp578:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp579:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB145_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB145_2:                               ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB145_4
## BB#3:                                ##   in Loop: Header=BB145_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-16, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdi
	callq	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED1Ev
	jmp	LBB145_2
LBB145_4:                               ## %_ZNSt3__113__vector_baseIN5boost10shared_ptrINS1_9unit_test9decorator4baseEEENS_9allocatorIS6_EEE5clearEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$4, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rdi
	callq	__ZdlPv
LBB145_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED1Ev
	.weak_def_can_be_hidden	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED1Ev
	.align	4, 0x90
__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED1Ev: ## @_ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp580:
	.cfi_def_cfa_offset 16
Ltmp581:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp582:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED2Ev
	.weak_def_can_be_hidden	__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED2Ev
	.align	4, 0x90
__ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED2Ev: ## @_ZN5boost10shared_ptrINS_9unit_test9decorator4baseEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp583:
	.cfi_def_cfa_offset 16
Ltmp584:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp585:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	addq	$8, %rdi
	callq	__ZN5boost6detail12shared_countD1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail12shared_countD1Ev
	.weak_def_can_be_hidden	__ZN5boost6detail12shared_countD1Ev
	.align	4, 0x90
__ZN5boost6detail12shared_countD1Ev:    ## @_ZN5boost6detail12shared_countD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp586:
	.cfi_def_cfa_offset 16
Ltmp587:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp588:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost6detail12shared_countD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail12shared_countD2Ev
	.weak_def_can_be_hidden	__ZN5boost6detail12shared_countD2Ev
	.align	4, 0x90
__ZN5boost6detail12shared_countD2Ev:    ## @_ZN5boost6detail12shared_countD2Ev
Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception14
## BB#0:
	pushq	%rbp
Ltmp592:
	.cfi_def_cfa_offset 16
Ltmp593:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp594:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	je	LBB149_3
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rdi
Ltmp589:
	callq	__ZN5boost6detail15sp_counted_base7releaseEv
Ltmp590:
	jmp	LBB149_2
LBB149_2:
	jmp	LBB149_3
LBB149_3:
	addq	$32, %rsp
	popq	%rbp
	retq
LBB149_4:
Ltmp591:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -20(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end14:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table149:
Lexception14:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset122 = Ltmp589-Lfunc_begin14         ## >> Call Site 1 <<
	.long	Lset122
Lset123 = Ltmp590-Ltmp589               ##   Call between Ltmp589 and Ltmp590
	.long	Lset123
Lset124 = Ltmp591-Lfunc_begin14         ##     jumps to Ltmp591
	.long	Lset124
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6detail15sp_counted_base7releaseEv
	.weak_def_can_be_hidden	__ZN5boost6detail15sp_counted_base7releaseEv
	.align	4, 0x90
__ZN5boost6detail15sp_counted_base7releaseEv: ## @_ZN5boost6detail15sp_counted_base7releaseEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp595:
	.cfi_def_cfa_offset 16
Ltmp596:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp597:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$8, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN5boost6detail16atomic_decrementEPU7_Atomici
	cmpl	$1, %eax
	jne	LBB150_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*16(%rcx)
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZN5boost6detail15sp_counted_base12weak_releaseEv
LBB150_2:
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail16atomic_decrementEPU7_Atomici
	.weak_def_can_be_hidden	__ZN5boost6detail16atomic_decrementEPU7_Atomici
	.align	4, 0x90
__ZN5boost6detail16atomic_decrementEPU7_Atomici: ## @_ZN5boost6detail16atomic_decrementEPU7_Atomici
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp598:
	.cfi_def_cfa_offset 16
Ltmp599:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp600:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	$1, -12(%rbp)
	movl	$-1, %eax
	lock		xaddl	%eax, (%rdi)
	movl	%eax, -16(%rbp)
	movl	-16(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6detail15sp_counted_base12weak_releaseEv
	.weak_def_can_be_hidden	__ZN5boost6detail15sp_counted_base12weak_releaseEv
	.align	4, 0x90
__ZN5boost6detail15sp_counted_base12weak_releaseEv: ## @_ZN5boost6detail15sp_counted_base12weak_releaseEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp601:
	.cfi_def_cfa_offset 16
Ltmp602:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp603:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	addq	$12, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN5boost6detail16atomic_decrementEPU7_Atomici
	cmpl	$1, %eax
	jne	LBB152_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*24(%rcx)
LBB152_2:
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
__GLOBAL__sub_I_test_case.cpp:          ## @_GLOBAL__sub_I_test_case.cpp
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp604:
	.cfi_def_cfa_offset 16
Ltmp605:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp606:
	.cfi_def_cfa_register %rbp
	callq	___cxx_global_var_init
	callq	___cxx_global_var_init.1
	popq	%rbp
	retq
	.cfi_endproc

.zerofill __DATA,__bss,__ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE,8,3 ## @_ZN5boost9unit_test12_GLOBAL__N_113unit_test_logE
.zerofill __DATA,__bss,__ZL22MyTestCase_registrar46,1,0 ## @_ZL22MyTestCase_registrar46
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"MyTestCase"

L_.str.2:                               ## @.str.2
	.asciz	"./test_case.cpp"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE ## @_ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.weak_def_can_be_hidden	__ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.align	3
__ZTVN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE:
	.quad	0
	.quad	__ZTIN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.quad	__ZNK5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEE4nextEv
	.quad	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED1Ev
	.quad	__ZN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE ## @_ZTSN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.weak_definition	__ZTSN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.align	4
__ZTSN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE:
	.asciz	"N5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE"

	.globl	__ZTSN5boost9unit_test19test_unit_generatorE ## @_ZTSN5boost9unit_test19test_unit_generatorE
	.weak_definition	__ZTSN5boost9unit_test19test_unit_generatorE
	.align	4
__ZTSN5boost9unit_test19test_unit_generatorE:
	.asciz	"N5boost9unit_test19test_unit_generatorE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost9unit_test19test_unit_generatorE ## @_ZTIN5boost9unit_test19test_unit_generatorE
	.weak_definition	__ZTIN5boost9unit_test19test_unit_generatorE
	.align	3
__ZTIN5boost9unit_test19test_unit_generatorE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost9unit_test19test_unit_generatorE

	.globl	__ZTIN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE ## @_ZTIN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.weak_definition	__ZTIN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.align	4
__ZTIN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSN5boost9unit_test9ut_detail22template_test_case_genI18MyTestCase_invokerNS_3mpl4listI9MyMessageIN18MyMessageNamespace17ParticularMessageEEN4mpl_2naESB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_SB_EEEE
	.quad	__ZTIN5boost9unit_test19test_unit_generatorE

	.globl	__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst ## @_ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst
	.weak_definition	__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst
	.align	3
__ZZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst:
	.space	8

	.globl	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst ## @_ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst
	.weak_definition	__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst
	.align	3
__ZGVZN5boost9unit_test9singletonINS0_15unit_test_log_tEE8instanceEvE8the_inst:
	.quad	0                       ## 0x0

	.globl	__ZTVN5boost9unit_test13test_observerE ## @_ZTVN5boost9unit_test13test_observerE
	.weak_def_can_be_hidden	__ZTVN5boost9unit_test13test_observerE
	.align	3
__ZTVN5boost9unit_test13test_observerE:
	.quad	0
	.quad	__ZTIN5boost9unit_test13test_observerE
	.quad	__ZN5boost9unit_test13test_observer10test_startEm
	.quad	__ZN5boost9unit_test13test_observer11test_finishEv
	.quad	__ZN5boost9unit_test13test_observer12test_abortedEv
	.quad	__ZN5boost9unit_test13test_observer15test_unit_startERKNS0_9test_unitE
	.quad	__ZN5boost9unit_test13test_observer16test_unit_finishERKNS0_9test_unitEm
	.quad	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitENS0_13basic_cstringIKcEE
	.quad	__ZN5boost9unit_test13test_observer17test_unit_skippedERKNS0_9test_unitE
	.quad	__ZN5boost9unit_test13test_observer17test_unit_abortedERKNS0_9test_unitE
	.quad	__ZN5boost9unit_test13test_observer16assertion_resultENS0_16assertion_resultE
	.quad	__ZN5boost9unit_test13test_observer16exception_caughtERKNS_19execution_exceptionE
	.quad	__ZN5boost9unit_test13test_observer8priorityEv
	.quad	__ZN5boost9unit_test13test_observer16assertion_resultEb
	.quad	__ZN5boost9unit_test13test_observerD1Ev
	.quad	__ZN5boost9unit_test13test_observerD0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost9unit_test13test_observerE ## @_ZTSN5boost9unit_test13test_observerE
	.weak_definition	__ZTSN5boost9unit_test13test_observerE
	.align	4
__ZTSN5boost9unit_test13test_observerE:
	.asciz	"N5boost9unit_test13test_observerE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost9unit_test13test_observerE ## @_ZTIN5boost9unit_test13test_observerE
	.weak_definition	__ZTIN5boost9unit_test13test_observerE
	.align	3
__ZTIN5boost9unit_test13test_observerE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost9unit_test13test_observerE

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZZN5boost9unit_test13basic_cstringIKcE8null_strEvE4null ## @_ZZN5boost9unit_test13basic_cstringIKcE8null_strEvE4null
	.weak_definition	__ZZN5boost9unit_test13basic_cstringIKcE8null_strEvE4null
__ZZN5boost9unit_test13basic_cstringIKcE8null_strEvE4null:
	.byte	0                       ## 0x0

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVN5boost9unit_test19test_unit_generatorE ## @_ZTVN5boost9unit_test19test_unit_generatorE
	.weak_def_can_be_hidden	__ZTVN5boost9unit_test19test_unit_generatorE
	.align	3
__ZTVN5boost9unit_test19test_unit_generatorE:
	.quad	0
	.quad	__ZTIN5boost9unit_test19test_unit_generatorE
	.quad	___cxa_pure_virtual
	.quad	__ZN5boost9unit_test19test_unit_generatorD1Ev
	.quad	__ZN5boost9unit_test19test_unit_generatorD0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS9MyMessageIN18MyMessageNamespace17ParticularMessageEE ## @_ZTS9MyMessageIN18MyMessageNamespace17ParticularMessageEE
	.weak_definition	__ZTS9MyMessageIN18MyMessageNamespace17ParticularMessageEE
	.align	4
__ZTS9MyMessageIN18MyMessageNamespace17ParticularMessageEE:
	.asciz	"9MyMessageIN18MyMessageNamespace17ParticularMessageEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI9MyMessageIN18MyMessageNamespace17ParticularMessageEE ## @_ZTI9MyMessageIN18MyMessageNamespace17ParticularMessageEE
	.weak_definition	__ZTI9MyMessageIN18MyMessageNamespace17ParticularMessageEE
	.align	3
__ZTI9MyMessageIN18MyMessageNamespace17ParticularMessageEE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS9MyMessageIN18MyMessageNamespace17ParticularMessageEE

	.globl	__ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable ## @_ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable
	.weak_definition	__ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable
	.align	3
__ZZN5boost9function0IvE9assign_toINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEEEvT_E13stored_vtable:
	.quad	__ZN5boost6detail8function15functor_managerINS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEE6manageERKNS1_15function_bufferERSD_NS1_30functor_manager_operation_typeE
	.quad	__ZN5boost6detail8function26void_function_obj_invoker0INS_9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEEvE6invokeERNS1_15function_bufferE

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE ## @_ZTSN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE
	.weak_definition	__ZTSN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE
	.align	4
__ZTSN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE:
	.asciz	"N5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE ## @_ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE
	.weak_definition	__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE
	.align	3
__ZTIN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost9unit_test9ut_detail26test_case_template_invokerI18MyTestCase_invoker9MyMessageIN18MyMessageNamespace17ParticularMessageEEEE

	.section	__TEXT,__cstring,cstring_literals
L_.str.3:                               ## @.str.3
	.asciz	"\" fixture entry."

L_.str.4:                               ## @.str.4
	.asciz	"\" entry."

L_.str.5:                               ## @.str.5
	.asciz	"\" exit."

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE ## @_ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE

	.globl	__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst ## @_ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst
	.weak_definition	__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst
	.align	3
__ZZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst:
	.space	24

	.globl	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst ## @_ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst
	.weak_definition	__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst
	.align	3
__ZGVZN5boost9unit_test9singletonINS0_9decorator9collectorEE8instanceEvE8the_inst:
	.quad	0                       ## 0x0

	.section	__DATA,__mod_init_func,mod_init_funcs
	.align	3
	.quad	__GLOBAL__sub_I_test_case.cpp

.subsections_via_symbols
