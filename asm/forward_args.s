	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	leaq	-4(%rbp), %rdi
	leaq	-8(%rbp), %rsi
	movl	$1, -4(%rbp)
	movl	$2, -8(%rbp)
	callq	__Z12forward_argsIJiiEEDaDpOT_
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEm
	leaq	L_.str(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	leaq	-12(%rbp), %rdi
	movl	$1, -12(%rbp)
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	callq	__Z12forward_argsIJiEEDaDpOT_
	movq	-40(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEm
	leaq	L_.str(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	leaq	-16(%rbp), %rdi
	movl	$2, -16(%rbp)
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	__Z12forward_argsIJiEEDaDpOT_
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEm
	leaq	L_.str.1(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	leaq	l_.ref.tmp(%rip), %rsi
	movq	%rsi, -32(%rbp)
	movq	$3, -24(%rbp)
	movq	-32(%rbp), %rdi
	movq	-24(%rbp), %rsi
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	callq	__Z12forward_argsIiEDaSt16initializer_listIT_E
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEm
	xorl	%ecx, %ecx
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	movl	%ecx, %eax
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z12forward_argsIJiiEEDaDpOT_
	.weak_def_can_be_hidden	__Z12forward_argsIJiiEEDaDpOT_
	.align	4, 0x90
__Z12forward_argsIJiiEEDaDpOT_:         ## @_Z12forward_argsIJiiEEDaDpOT_
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, -24(%rbp)
	movq	%rsi, -32(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -16(%rbp)
	movslq	(%rsi), %rsi
	movq	-32(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movl	(%rdi), %edx
	leaq	-48(%rbp), %rdi
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	callq	__ZN3fooC1Emi
Ltmp6:
	movq	-72(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNK3foo4sizeEv
Ltmp7:
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	jmp	LBB2_1
LBB2_1:
	leaq	-48(%rbp), %rdi
	callq	__ZN3fooD1Ev
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$80, %rsp
	popq	%rbp
	retq
LBB2_2:
Ltmp8:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZN3fooD1Ev
## BB#3:
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp6-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp6
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp6-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp7-Ltmp6                     ##   Call between Ltmp6 and Ltmp7
	.long	Lset3
Lset4 = Ltmp8-Lfunc_begin0              ##     jumps to Ltmp8
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp7-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Lfunc_end0-Ltmp7                ##   Call between Ltmp7 and Lfunc_end0
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z12forward_argsIJiEEDaDpOT_
	.weak_def_can_be_hidden	__Z12forward_argsIJiEEDaDpOT_
	.align	4, 0x90
__Z12forward_argsIJiEEDaDpOT_:          ## @_Z12forward_argsIJiEEDaDpOT_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	movslq	(%rdi), %rsi
	leaq	-32(%rbp), %rdi
	movq	%rdi, -56(%rbp)         ## 8-byte Spill
	callq	__ZN3fooC1Em
Ltmp12:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNK3foo4sizeEv
Ltmp13:
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	jmp	LBB3_1
LBB3_1:
	leaq	-32(%rbp), %rdi
	callq	__ZN3fooD1Ev
	movq	-64(%rbp), %rax         ## 8-byte Reload
	addq	$64, %rsp
	popq	%rbp
	retq
LBB3_2:
Ltmp14:
	leaq	-32(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	callq	__ZN3fooD1Ev
## BB#3:
	movq	-40(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset7 = Lfunc_begin1-Lfunc_begin1       ## >> Call Site 1 <<
	.long	Lset7
Lset8 = Ltmp12-Lfunc_begin1             ##   Call between Lfunc_begin1 and Ltmp12
	.long	Lset8
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp12-Lfunc_begin1             ## >> Call Site 2 <<
	.long	Lset9
Lset10 = Ltmp13-Ltmp12                  ##   Call between Ltmp12 and Ltmp13
	.long	Lset10
Lset11 = Ltmp14-Lfunc_begin1            ##     jumps to Ltmp14
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp13-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset12
Lset13 = Lfunc_end1-Ltmp13              ##   Call between Ltmp13 and Lfunc_end1
	.long	Lset13
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__Z12forward_argsIiEDaSt16initializer_listIT_E
	.weak_def_can_be_hidden	__Z12forward_argsIiEDaSt16initializer_listIT_E
	.align	4, 0x90
__Z12forward_argsIiEDaSt16initializer_listIT_E: ## @_Z12forward_argsIiEDaSt16initializer_listIT_E
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	subq	$80, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -8(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rsi
	movq	-40(%rbp), %rdx
	leaq	-32(%rbp), %rdi
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	callq	__ZN3fooC1ESt16initializer_listIiE
Ltmp18:
	movq	-72(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNK3foo4sizeEv
Ltmp19:
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	jmp	LBB4_1
LBB4_1:
	leaq	-32(%rbp), %rdi
	callq	__ZN3fooD1Ev
	movq	-80(%rbp), %rax         ## 8-byte Reload
	addq	$80, %rsp
	popq	%rbp
	retq
LBB4_2:
Ltmp20:
	leaq	-32(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZN3fooD1Ev
## BB#3:
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset14 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset14
Lset15 = Ltmp18-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp18
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset16 = Ltmp18-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset16
Lset17 = Ltmp19-Ltmp18                  ##   Call between Ltmp18 and Ltmp19
	.long	Lset17
Lset18 = Ltmp20-Lfunc_begin2            ##     jumps to Ltmp20
	.long	Lset18
	.byte	0                       ##   On action: cleanup
Lset19 = Ltmp19-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset19
Lset20 = Lfunc_end2-Ltmp19              ##   Call between Ltmp19 and Lfunc_end2
	.long	Lset20
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN3fooC1Emi
	.weak_def_can_be_hidden	__ZN3fooC1Emi
	.align	4, 0x90
__ZN3fooC1Emi:                          ## @_ZN3fooC1Emi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movl	-20(%rbp), %edx
	callq	__ZN3fooC2Emi
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK3foo4sizeEv
	.weak_def_can_be_hidden	__ZNK3foo4sizeEv
	.align	4, 0x90
__ZNK3foo4sizeEv:                       ## @_ZNK3foo4sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp27:
	.cfi_def_cfa_offset 16
Ltmp28:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp29:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooD1Ev
	.weak_def_can_be_hidden	__ZN3fooD1Ev
	.align	4, 0x90
__ZN3fooD1Ev:                           ## @_ZN3fooD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp30:
	.cfi_def_cfa_offset 16
Ltmp31:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp32:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN3fooD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooC2Emi
	.weak_def_can_be_hidden	__ZN3fooC2Emi
	.align	4, 0x90
__ZN3fooC2Emi:                          ## @_ZN3fooC2Emi
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	subq	$592, %rsp              ## imm = 0x250
	movq	%rdi, -472(%rbp)
	movq	%rsi, -480(%rbp)
	movl	%edx, -484(%rbp)
	movq	-472(%rbp), %rsi
	movq	-480(%rbp), %rdi
	movq	%rdi, (%rsi)
	movq	%rsi, %rdi
	addq	$8, %rdi
	movq	%rdi, %rax
	movq	%rdi, -464(%rbp)
	movq	%rdi, -456(%rbp)
	movq	%rdi, -440(%rbp)
	movq	$0, -448(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	$0, -432(%rbp)
	movq	-424(%rbp), %rcx
	leaq	-432(%rbp), %r8
	movq	%r8, -416(%rbp)
	movq	-432(%rbp), %r8
	movq	%rcx, -400(%rbp)
	movq	%r8, -408(%rbp)
	movq	-400(%rbp), %rcx
	leaq	-408(%rbp), %r8
	movq	%r8, -392(%rbp)
	movq	-408(%rbp), %r8
	movq	%r8, (%rcx)
	movq	(%rsi), %rcx
	movl	$4, %edx
	movl	%edx, %r8d
	movq	%rax, -512(%rbp)        ## 8-byte Spill
	movq	%rcx, %rax
	mulq	%r8
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
Ltmp33:
	movq	%rdi, -520(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -528(%rbp)        ## 8-byte Spill
	callq	__Znam
Ltmp34:
	movq	%rax, -536(%rbp)        ## 8-byte Spill
	jmp	LBB8_1
LBB8_1:
	movq	-536(%rbp), %rax        ## 8-byte Reload
	movq	-520(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -264(%rbp)
	movq	%rax, -272(%rbp)
	movq	-264(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rdx
	movq	%rdx, -248(%rbp)
	movq	-248(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -280(%rbp)
	movq	-272(%rbp), %rdx
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rsi
	movq	%rsi, -208(%rbp)
	movq	-208(%rbp), %rsi
	movq	%rdx, (%rsi)
	cmpq	$0, -280(%rbp)
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	je	LBB8_5
## BB#2:
	movq	-544(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	-280(%rbp), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	$0, -240(%rbp)
	movq	-232(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -552(%rbp)        ## 8-byte Spill
	je	LBB8_4
## BB#3:
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdaPv
LBB8_4:                                 ## %_ZNKSt3__114default_deleteIA_iEclIiEEvPT_PNS_9enable_ifIXsr27__same_or_less_cv_qualifiedIS5_PiEE5valueEvE4typeE.exit.i
	jmp	LBB8_5
LBB8_5:                                 ## %_ZNSt3__110unique_ptrIA_iNS_14default_deleteIS1_EEE5resetIPiEENS_9enable_ifIXsr27__same_or_less_cv_qualifiedIT_S6_EE5valueEvE4typeES8_.exit
	movq	-528(%rbp), %rax        ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -176(%rbp)
	movq	$0, -184(%rbp)
	movq	-176(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	shlq	$2, %rcx
	addq	(%rax), %rcx
	movq	%rcx, -560(%rbp)        ## 8-byte Spill
## BB#6:
	movq	-528(%rbp), %rax        ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -24(%rbp)
	movq	$0, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	shlq	$2, %rcx
	addq	(%rax), %rcx
	movq	%rcx, -568(%rbp)        ## 8-byte Spill
## BB#7:
	leaq	-484(%rbp), %rax
	movq	-528(%rbp), %rcx        ## 8-byte Reload
	movq	(%rcx), %rdx
	shlq	$2, %rdx
	movq	-568(%rbp), %rsi        ## 8-byte Reload
	addq	%rdx, %rsi
	movq	-560(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%rax, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdi, -120(%rbp)
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdi
	subq	%rdi, %rsi
	sarq	$2, %rsi
	movq	-120(%rbp), %rdi
	movq	%rax, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-72(%rbp), %rax
	movq	-80(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	-88(%rbp), %rdi
	movq	%rax, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	%rdi, -56(%rbp)
LBB8_8:                                 ## =>This Inner Loop Header: Depth=1
	cmpq	$0, -48(%rbp)
	jle	LBB8_10
## BB#9:                                ##   in Loop: Header=BB8_8 Depth=1
	movq	-56(%rbp), %rax
	movl	(%rax), %ecx
	movq	-40(%rbp), %rax
	movl	%ecx, (%rax)
	movq	-40(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -40(%rbp)
	movq	-48(%rbp), %rax
	addq	$-1, %rax
	movq	%rax, -48(%rbp)
	jmp	LBB8_8
LBB8_10:                                ## %_ZNSt3__14fillIPiiEEvT_S2_RKT0_.exit
## BB#11:
	addq	$592, %rsp              ## imm = 0x250
	popq	%rbp
	retq
LBB8_12:
Ltmp35:
	movl	%edx, %ecx
	movq	%rax, -496(%rbp)
	movl	%ecx, -500(%rbp)
	movq	-512(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -384(%rbp)
	movq	-384(%rbp), %rdx
	movq	%rdx, -376(%rbp)
	movq	-376(%rbp), %rdx
	movq	%rdx, -360(%rbp)
	movq	-360(%rbp), %rdx
	movq	%rdx, -352(%rbp)
	movq	-352(%rbp), %rsi
	movq	%rsi, -344(%rbp)
	movq	-344(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -368(%rbp)
	movq	%rdx, -312(%rbp)
	movq	-312(%rbp), %rsi
	movq	%rsi, -304(%rbp)
	movq	-304(%rbp), %rsi
	movq	$0, (%rsi)
	cmpq	$0, -368(%rbp)
	movq	%rdx, -576(%rbp)        ## 8-byte Spill
	je	LBB8_16
## BB#13:
	movq	-576(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	-368(%rbp), %rdx
	movq	%rcx, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	$0, -336(%rbp)
	movq	-328(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -584(%rbp)        ## 8-byte Spill
	je	LBB8_15
## BB#14:
	movq	-584(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdaPv
LBB8_15:                                ## %_ZNKSt3__114default_deleteIA_iEclIiEEvPT_PNS_9enable_ifIXsr27__same_or_less_cv_qualifiedIS5_PiEE5valueEvE4typeE.exit.i.i.i
	jmp	LBB8_16
LBB8_16:                                ## %_ZNSt3__110unique_ptrIA_iNS_14default_deleteIS1_EEED1Ev.exit
	jmp	LBB8_17
LBB8_17:
	movq	-496(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table8:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\234"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset21 = Ltmp33-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset21
Lset22 = Ltmp34-Ltmp33                  ##   Call between Ltmp33 and Ltmp34
	.long	Lset22
Lset23 = Ltmp35-Lfunc_begin3            ##     jumps to Ltmp35
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp34-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset24
Lset25 = Lfunc_end3-Ltmp34              ##   Call between Ltmp34 and Lfunc_end3
	.long	Lset25
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZN3fooD2Ev
	.weak_def_can_be_hidden	__ZN3fooD2Ev
	.align	4, 0x90
__ZN3fooD2Ev:                           ## @_ZN3fooD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp39:
	.cfi_def_cfa_offset 16
Ltmp40:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp41:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	addq	$8, %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -88(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	$0, (%rax)
	cmpq	$0, -88(%rbp)
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	je	LBB10_4
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -128(%rbp)        ## 8-byte Spill
	je	LBB10_3
## BB#2:
	movq	-128(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdaPv
LBB10_3:                                ## %_ZNKSt3__114default_deleteIA_iEclIiEEvPT_PNS_9enable_ifIXsr27__same_or_less_cv_qualifiedIS5_PiEE5valueEvE4typeE.exit.i.i.i
	jmp	LBB10_4
LBB10_4:                                ## %_ZNSt3__110unique_ptrIA_iNS_14default_deleteIS1_EEED1Ev.exit
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooC1Em
	.weak_def_can_be_hidden	__ZN3fooC1Em
	.align	4, 0x90
__ZN3fooC1Em:                           ## @_ZN3fooC1Em
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp42:
	.cfi_def_cfa_offset 16
Ltmp43:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp44:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN3fooC2Em
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooC2Em
	.weak_def_can_be_hidden	__ZN3fooC2Em
	.align	4, 0x90
__ZN3fooC2Em:                           ## @_ZN3fooC2Em
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp45:
	.cfi_def_cfa_offset 16
Ltmp46:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp47:
	.cfi_def_cfa_register %rbp
	leaq	-24(%rbp), %rax
	leaq	-48(%rbp), %rcx
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	-88(%rbp), %rsi
	movq	-96(%rbp), %rdi
	movq	%rdi, (%rsi)
	addq	$8, %rsi
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rsi, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%rsi, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rsi, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooC1ESt16initializer_listIiE
	.weak_def_can_be_hidden	__ZN3fooC1ESt16initializer_listIiE
	.align	4, 0x90
__ZN3fooC1ESt16initializer_listIiE:     ## @_ZN3fooC1ESt16initializer_listIiE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp48:
	.cfi_def_cfa_offset 16
Ltmp49:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp50:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rsi, -16(%rbp)
	movq	%rdx, -8(%rbp)
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-8(%rbp), %rdx
	callq	__ZN3fooC2ESt16initializer_listIiE
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN3fooC2ESt16initializer_listIiE
	.weak_def_can_be_hidden	__ZN3fooC2ESt16initializer_listIiE
	.align	4, 0x90
__ZN3fooC2ESt16initializer_listIiE:     ## @_ZN3fooC2ESt16initializer_listIiE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp51:
	.cfi_def_cfa_offset 16
Ltmp52:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp53:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$488, %rsp              ## imm = 0x1E8
Ltmp54:
	.cfi_offset %rbx, -40
Ltmp55:
	.cfi_offset %r14, -32
Ltmp56:
	.cfi_offset %r15, -24
	movq	$-1, %rax
	movl	$4, %ecx
	movl	%ecx, %r8d
	leaq	-392(%rbp), %r9
	movq	%rsi, -392(%rbp)
	movq	%rdx, -384(%rbp)
	movq	%rdi, -400(%rbp)
	movq	-400(%rbp), %rdx
	movq	%r9, -376(%rbp)
	movq	-376(%rbp), %rsi
	movq	8(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	%rdx, %rsi
	addq	$8, %rsi
	movq	(%rdx), %rdi
	movq	%rax, -432(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	movq	%rdx, -440(%rbp)        ## 8-byte Spill
	mulq	%r8
	seto	%r10b
	movq	-432(%rbp), %rdi        ## 8-byte Reload
	cmovoq	%rdi, %rax
	movq	%rax, %rdi
	movq	%rsi, -448(%rbp)        ## 8-byte Spill
	movb	%r10b, -449(%rbp)       ## 1-byte Spill
	callq	__Znam
	leaq	-392(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	leaq	-200(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$4, %r11d
	movl	%r11d, %r9d
	leaq	-408(%rbp), %rbx
	movq	%rdi, -464(%rbp)        ## 8-byte Spill
	movq	%rbx, %rdi
	movq	%rsi, -472(%rbp)        ## 8-byte Spill
	movl	%ecx, %esi
	movq	%r9, %rdx
	movq	%rax, -480(%rbp)        ## 8-byte Spill
	movq	%r8, -488(%rbp)         ## 8-byte Spill
	callq	_memset
	movl	-408(%rbp), %ecx
	movl	%ecx, -248(%rbp)
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-480(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -264(%rbp)
	movq	-256(%rbp), %rdx
	movq	-264(%rbp), %rdi
	movl	-248(%rbp), %ecx
	movl	%ecx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rdi, -240(%rbp)
	movq	-232(%rbp), %rdx
	movq	-240(%rbp), %rdi
	movq	%rdx, -208(%rbp)
	movq	%rdi, -216(%rbp)
	movq	-208(%rbp), %rdx
	movq	-216(%rbp), %rdi
	movq	%rdx, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-192(%rbp), %rdx
	movq	-488(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -184(%rbp)
	movq	-184(%rbp), %r8
	movq	(%r8), %r8
	movq	%rdx, -168(%rbp)
	movq	%r8, -176(%rbp)
	movq	-168(%rbp), %rdx
	movq	-464(%rbp), %r8         ## 8-byte Reload
	movq	%r8, -160(%rbp)
	movq	-160(%rbp), %r9
	movq	(%r9), %r9
	movq	%r9, (%rdx)
	movq	-472(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %r9
	movq	(%r9), %r9
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rbx
	movq	(%rbx), %r14
	movq	8(%rbx), %rbx
	shlq	$2, %rbx
	addq	%rbx, %r14
	movq	-440(%rbp), %rbx        ## 8-byte Reload
	addq	$8, %rbx
	movq	%rbx, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-64(%rbp), %rbx
	movq	-72(%rbp), %r15
	movq	%rbx, -56(%rbp)
	movq	-56(%rbp), %rbx
	movq	%rbx, -48(%rbp)
	movq	-48(%rbp), %rbx
	shlq	$2, %r15
	addq	(%rbx), %r15
	movq	%r9, -496(%rbp)         ## 8-byte Spill
	movq	%r14, -504(%rbp)        ## 8-byte Spill
	movq	%r15, -512(%rbp)        ## 8-byte Spill
## BB#1:
	movq	-496(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-504(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -144(%rbp)
	movq	-512(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -152(%rbp)
	movq	-136(%rbp), %rsi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	-152(%rbp), %r8
	movq	%r8, -88(%rbp)
	movq	-88(%rbp), %r8
	movq	%rsi, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	%r8, -112(%rbp)
	movq	-104(%rbp), %rsi
	movq	-96(%rbp), %rdi
	subq	%rdi, %rsi
	sarq	$2, %rsi
	movq	%rsi, -120(%rbp)
	cmpq	$0, -120(%rbp)
	jbe	LBB14_3
## BB#2:
	movq	-112(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-120(%rbp), %rdx
	shlq	$2, %rdx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	_memmove
LBB14_3:                                ## %_ZNSt3__14copyIPKiPiEET0_T_S5_S4_.exit
## BB#4:
	addq	$488, %rsp              ## imm = 0x1E8
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp78:
	.cfi_def_cfa_offset 16
Ltmp79:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp80:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp57:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp58:
	jmp	LBB15_1
LBB15_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB15_3
	jmp	LBB15_26
LBB15_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB15_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB15_7
LBB15_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB15_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB15_8
	jmp	LBB15_13
LBB15_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp60:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp61:
	jmp	LBB15_9
LBB15_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp62:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp63:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB15_10
LBB15_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp64:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp65:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB15_12
LBB15_11:
Ltmp66:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB15_21
LBB15_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB15_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp67:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp68:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB15_15
LBB15_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB15_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp69:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp70:
	jmp	LBB15_17
LBB15_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB15_18
LBB15_18:
	jmp	LBB15_25
LBB15_19:
Ltmp59:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB15_22
LBB15_20:
Ltmp71:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB15_21
LBB15_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB15_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp72:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp73:
	jmp	LBB15_23
LBB15_23:
	callq	___cxa_end_catch
LBB15_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB15_25:
	jmp	LBB15_26
LBB15_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB15_24
LBB15_27:
Ltmp74:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp75:
	callq	___cxa_end_catch
Ltmp76:
	jmp	LBB15_28
LBB15_28:
	jmp	LBB15_29
LBB15_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB15_30:
Ltmp77:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table15:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset26 = Ltmp57-Lfunc_begin4            ## >> Call Site 1 <<
	.long	Lset26
Lset27 = Ltmp58-Ltmp57                  ##   Call between Ltmp57 and Ltmp58
	.long	Lset27
Lset28 = Ltmp59-Lfunc_begin4            ##     jumps to Ltmp59
	.long	Lset28
	.byte	5                       ##   On action: 3
Lset29 = Ltmp60-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset29
Lset30 = Ltmp61-Ltmp60                  ##   Call between Ltmp60 and Ltmp61
	.long	Lset30
Lset31 = Ltmp71-Lfunc_begin4            ##     jumps to Ltmp71
	.long	Lset31
	.byte	5                       ##   On action: 3
Lset32 = Ltmp62-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset32
Lset33 = Ltmp65-Ltmp62                  ##   Call between Ltmp62 and Ltmp65
	.long	Lset33
Lset34 = Ltmp66-Lfunc_begin4            ##     jumps to Ltmp66
	.long	Lset34
	.byte	3                       ##   On action: 2
Lset35 = Ltmp67-Lfunc_begin4            ## >> Call Site 4 <<
	.long	Lset35
Lset36 = Ltmp70-Ltmp67                  ##   Call between Ltmp67 and Ltmp70
	.long	Lset36
Lset37 = Ltmp71-Lfunc_begin4            ##     jumps to Ltmp71
	.long	Lset37
	.byte	5                       ##   On action: 3
Lset38 = Ltmp70-Lfunc_begin4            ## >> Call Site 5 <<
	.long	Lset38
Lset39 = Ltmp72-Ltmp70                  ##   Call between Ltmp70 and Ltmp72
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp72-Lfunc_begin4            ## >> Call Site 6 <<
	.long	Lset40
Lset41 = Ltmp73-Ltmp72                  ##   Call between Ltmp72 and Ltmp73
	.long	Lset41
Lset42 = Ltmp74-Lfunc_begin4            ##     jumps to Ltmp74
	.long	Lset42
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp73-Lfunc_begin4            ## >> Call Site 7 <<
	.long	Lset43
Lset44 = Ltmp75-Ltmp73                  ##   Call between Ltmp73 and Ltmp75
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp75-Lfunc_begin4            ## >> Call Site 8 <<
	.long	Lset45
Lset46 = Ltmp76-Ltmp75                  ##   Call between Ltmp75 and Ltmp76
	.long	Lset46
Lset47 = Ltmp77-Lfunc_begin4            ##     jumps to Ltmp77
	.long	Lset47
	.byte	5                       ##   On action: 3
Lset48 = Ltmp76-Lfunc_begin4            ## >> Call Site 9 <<
	.long	Lset48
Lset49 = Lfunc_end4-Ltmp76              ##   Call between Ltmp76 and Lfunc_end4
	.long	Lset49
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp81:
	.cfi_def_cfa_offset 16
Ltmp82:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp83:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp87:
	.cfi_def_cfa_offset 16
Ltmp88:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp89:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB17_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB17_26
LBB17_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB17_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB17_5
LBB17_4:
	movq	$0, -368(%rbp)
LBB17_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB17_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB17_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB17_26
LBB17_8:
	jmp	LBB17_9
LBB17_9:
	cmpq	$0, -368(%rbp)
	jle	LBB17_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB17_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB17_13
LBB17_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB17_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp84:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp85:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB17_14
LBB17_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB17_15
LBB17_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB17_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB17_19
LBB17_17:
Ltmp86:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB17_27
LBB17_18:
	movl	$0, -416(%rbp)
LBB17_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB17_20
	jmp	LBB17_29
LBB17_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB17_26
	jmp	LBB17_28
LBB17_20:
	jmp	LBB17_21
LBB17_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB17_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB17_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB17_26
LBB17_24:
	jmp	LBB17_25
LBB17_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB17_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB17_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB17_28:
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table17:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset50 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset50
Lset51 = Ltmp84-Lfunc_begin5            ##   Call between Lfunc_begin5 and Ltmp84
	.long	Lset51
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset52 = Ltmp84-Lfunc_begin5            ## >> Call Site 2 <<
	.long	Lset52
Lset53 = Ltmp85-Ltmp84                  ##   Call between Ltmp84 and Ltmp85
	.long	Lset53
Lset54 = Ltmp86-Lfunc_begin5            ##     jumps to Ltmp86
	.long	Lset54
	.byte	0                       ##   On action: cleanup
Lset55 = Ltmp85-Lfunc_begin5            ## >> Call Site 3 <<
	.long	Lset55
Lset56 = Lfunc_end5-Ltmp85              ##   Call between Ltmp85 and Lfunc_end5
	.long	Lset56
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp90:
	.cfi_def_cfa_offset 16
Ltmp91:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp92:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp93:
	.cfi_def_cfa_offset 16
Ltmp94:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp95:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	" "

L_.str.1:                               ## @.str.1
	.asciz	"\n"

	.section	__TEXT,__const
	.align	2                       ## @.ref.tmp
l_.ref.tmp:
	.long	1                       ## 0x1
	.long	2                       ## 0x2
	.long	3                       ## 0x3


.subsections_via_symbols
