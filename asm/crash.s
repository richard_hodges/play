	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp5:
	.cfi_def_cfa_offset 16
Ltmp6:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp7:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movl	$40, %eax
	movl	%eax, %ecx
	movl	$0, -364(%rbp)
	movl	%edi, -368(%rbp)
	movq	%rsi, -376(%rbp)
	movq	%rcx, %rdi
	callq	__Znwm
	xorl	%esi, %esi
	movl	$40, %edx
                                        ## 
	movq	%rax, %rcx
	movq	%rcx, %rdi
	movq	%rax, -408(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-408(%rbp), %rdi        ## 8-byte Reload
	callq	__ZN1FC1Ev
	xorl	%esi, %esi
	movl	%esi, %eax
	leaq	-384(%rbp), %rcx
	leaq	-280(%rbp), %rdx
	leaq	-304(%rbp), %rdi
	leaq	-344(%rbp), %r8
	movq	%rcx, -352(%rbp)
	movq	-408(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -360(%rbp)
	movq	-352(%rbp), %r10
	movq	-360(%rbp), %r11
	movq	%r10, -336(%rbp)
	movq	%r11, -344(%rbp)
	movq	-336(%rbp), %r10
	movq	%r8, -328(%rbp)
	movq	-328(%rbp), %r8
	movq	(%r8), %r8
	movq	%r10, -312(%rbp)
	movq	%r8, -320(%rbp)
	movq	-312(%rbp), %r8
	movq	-320(%rbp), %r10
	movq	%r8, -296(%rbp)
	movq	%r10, -304(%rbp)
	movq	-296(%rbp), %r8
	movq	%rdi, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%r8, -272(%rbp)
	movq	%rdi, -280(%rbp)
	movq	-272(%rbp), %rdi
	movq	%rdx, -264(%rbp)
	movq	-264(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -416(%rbp)        ## 8-byte Spill
	movq	%rax, -424(%rbp)        ## 8-byte Spill
	je	LBB0_2
## BB#1:
	movq	-416(%rbp), %rax        ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -424(%rbp)        ## 8-byte Spill
LBB0_2:
	movq	-424(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	16(%rcx), %rcx
Ltmp0:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp1:
	movq	%rax, -432(%rbp)        ## 8-byte Spill
	jmp	LBB0_3
LBB0_3:
	leaq	-384(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	-384(%rbp), %rax
	movq	(%rax), %rcx
	movq	16(%rcx), %rcx
Ltmp2:
	movq	%rax, %rdi
	callq	*%rcx
Ltmp3:
	movq	%rax, -440(%rbp)        ## 8-byte Spill
	jmp	LBB0_4
LBB0_4:
	leaq	-384(%rbp), %rax
	movl	$0, -364(%rbp)
	movq	%rax, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	$0, -104(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -112(%rbp)
	movq	%rax, -448(%rbp)        ## 8-byte Spill
	je	LBB0_8
## BB#5:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	je	LBB0_7
## BB#6:
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*8(%rcx)
LBB0_7:                                 ## %_ZNKSt3__114default_deleteI1FEclEPS1_.exit.i.i.i.2
	jmp	LBB0_8
LBB0_8:                                 ## %_ZNSt3__110unique_ptrI1FNS_14default_deleteIS1_EEED1Ev.exit3
	movl	-364(%rbp), %eax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB0_9:
Ltmp4:
	leaq	-384(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -392(%rbp)
	movl	%esi, -396(%rbp)
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	$0, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	-176(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -240(%rbp)
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	je	LBB0_13
## BB#10:
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rcx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -472(%rbp)        ## 8-byte Spill
	je	LBB0_12
## BB#11:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*8(%rcx)
LBB0_12:                                ## %_ZNKSt3__114default_deleteI1FEclEPS1_.exit.i.i.i
	jmp	LBB0_13
LBB0_13:                                ## %_ZNSt3__110unique_ptrI1FNS_14default_deleteIS1_EEED1Ev.exit
	jmp	LBB0_14
LBB0_14:
	movq	-392(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp3-Ltmp0                     ##   Call between Ltmp0 and Ltmp3
	.long	Lset3
Lset4 = Ltmp4-Lfunc_begin0              ##     jumps to Ltmp4
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp3-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Lfunc_end0-Ltmp3                ##   Call between Ltmp3 and Lfunc_end0
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN1FC1Ev
	.weak_def_can_be_hidden	__ZN1FC1Ev
	.align	4, 0x90
__ZN1FC1Ev:                             ## @_ZN1FC1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp8:
	.cfi_def_cfa_offset 16
Ltmp9:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp10:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1FC2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1FC2Ev
	.weak_def_can_be_hidden	__ZN1FC2Ev
	.align	4, 0x90
__ZN1FC2Ev:                             ## @_ZN1FC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp11:
	.cfi_def_cfa_offset 16
Ltmp12:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp13:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -104(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN1AC2Ev
	movq	-104(%rbp), %rax        ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, %rdi
	callq	__ZN1EC2Ev
	xorl	%esi, %esi
	movl	$24, %ecx
	movl	%ecx, %edx
	movq	__ZTV1F@GOTPCREL(%rip), %rax
	movq	%rax, %rdi
	addq	$56, %rdi
	addq	$16, %rax
	movq	-104(%rbp), %r8         ## 8-byte Reload
	movq	%rax, (%r8)
	movq	%rdi, 8(%r8)
	addq	$16, %r8
	movq	%r8, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -48(%rbp)
	movq	%rax, -112(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-112(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movl	$0, -36(%rbp)
LBB2_1:                                 ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -36(%rbp)
	jae	LBB2_3
## BB#2:                                ##   in Loop: Header=BB2_1 Depth=1
	movl	-36(%rbp), %eax
	movl	%eax, %ecx
	movq	-32(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-36(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -36(%rbp)
	jmp	LBB2_1
LBB2_3:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit
	addq	$112, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1AC2Ev
	.weak_def_can_be_hidden	__ZN1AC2Ev
	.align	4, 0x90
__ZN1AC2Ev:                             ## @_ZN1AC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp14:
	.cfi_def_cfa_offset 16
Ltmp15:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp16:
	.cfi_def_cfa_register %rbp
	movq	__ZTV1A@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1EC2Ev
	.weak_def_can_be_hidden	__ZN1EC2Ev
	.align	4, 0x90
__ZN1EC2Ev:                             ## @_ZN1EC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp17:
	.cfi_def_cfa_offset 16
Ltmp18:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp19:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZN1DC2Ev
	movq	__ZTV1E@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, (%rdi)
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1FD1Ev
	.weak_def_can_be_hidden	__ZN1FD1Ev
	.align	4, 0x90
__ZN1FD1Ev:                             ## @_ZN1FD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp20:
	.cfi_def_cfa_offset 16
Ltmp21:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp22:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1FD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1FD0Ev
	.weak_def_can_be_hidden	__ZN1FD0Ev
	.align	4, 0x90
__ZN1FD0Ev:                             ## @_ZN1FD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp23:
	.cfi_def_cfa_offset 16
Ltmp24:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp25:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN1FD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1F4funcEv
	.weak_def_can_be_hidden	__ZN1F4funcEv
	.align	4, 0x90
__ZN1F4funcEv:                          ## @_ZN1F4funcEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp26:
	.cfi_def_cfa_offset 16
Ltmp27:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp28:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	leaq	L_.str(%rip), %rax
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6assignEPKc
	xorl	%ecx, %ecx
	movl	%ecx, %esi
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	movq	%rsi, %rax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZThn8_N1FD1Ev
	.weak_def_can_be_hidden	__ZThn8_N1FD1Ev
	.align	4, 0x90
__ZThn8_N1FD1Ev:                        ## @_ZThn8_N1FD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp29:
	.cfi_def_cfa_offset 16
Ltmp30:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp31:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-8, %rdi
	popq	%rbp
	jmp	__ZN1FD1Ev              ## TAILCALL
	.cfi_endproc

	.globl	__ZThn8_N1FD0Ev
	.weak_def_can_be_hidden	__ZThn8_N1FD0Ev
	.align	4, 0x90
__ZThn8_N1FD0Ev:                        ## @_ZThn8_N1FD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp32:
	.cfi_def_cfa_offset 16
Ltmp33:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp34:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-8, %rdi
	popq	%rbp
	jmp	__ZN1FD0Ev              ## TAILCALL
	.cfi_endproc

	.globl	__ZTchn8_h8_N1F4funcEv
	.weak_def_can_be_hidden	__ZTchn8_h8_N1F4funcEv
	.align	4, 0x90
__ZTchn8_h8_N1F4funcEv:                 ## @_ZTchn8_h8_N1F4funcEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp35:
	.cfi_def_cfa_offset 16
Ltmp36:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp37:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	addq	$-8, %rdi
	callq	__ZN1F4funcEv
	cmpq	$0, %rax
	movq	%rax, -16(%rbp)         ## 8-byte Spill
	je	LBB10_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	jmp	LBB10_3
LBB10_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -24(%rbp)         ## 8-byte Spill
	jmp	LBB10_3
LBB10_3:
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZThn8_N1F4funcEv
	.weak_def_can_be_hidden	__ZThn8_N1F4funcEv
	.align	4, 0x90
__ZThn8_N1F4funcEv:                     ## @_ZThn8_N1F4funcEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp38:
	.cfi_def_cfa_offset 16
Ltmp39:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp40:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	addq	$-8, %rdi
	popq	%rbp
	jmp	__ZN1F4funcEv           ## TAILCALL
	.cfi_endproc

	.globl	__ZN1AD1Ev
	.weak_def_can_be_hidden	__ZN1AD1Ev
	.align	4, 0x90
__ZN1AD1Ev:                             ## @_ZN1AD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp41:
	.cfi_def_cfa_offset 16
Ltmp42:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp43:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1AD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1AD0Ev
	.weak_def_can_be_hidden	__ZN1AD0Ev
	.align	4, 0x90
__ZN1AD0Ev:                             ## @_ZN1AD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN1AD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1AD2Ev
	.weak_def_can_be_hidden	__ZN1AD2Ev
	.align	4, 0x90
__ZN1AD2Ev:                             ## @_ZN1AD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1DC2Ev
	.weak_def_can_be_hidden	__ZN1DC2Ev
	.align	4, 0x90
__ZN1DC2Ev:                             ## @_ZN1DC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp50:
	.cfi_def_cfa_offset 16
Ltmp51:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp52:
	.cfi_def_cfa_register %rbp
	movq	__ZTV1D@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1ED1Ev
	.weak_def_can_be_hidden	__ZN1ED1Ev
	.align	4, 0x90
__ZN1ED1Ev:                             ## @_ZN1ED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1ED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1ED0Ev
	.weak_def_can_be_hidden	__ZN1ED0Ev
	.align	4, 0x90
__ZN1ED0Ev:                             ## @_ZN1ED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp56:
	.cfi_def_cfa_offset 16
Ltmp57:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp58:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN1ED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTch0_h8_N1E4funcEv
	.weak_def_can_be_hidden	__ZTch0_h8_N1E4funcEv
	.align	4, 0x90
__ZTch0_h8_N1E4funcEv:                  ## @_ZTch0_h8_N1E4funcEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp59:
	.cfi_def_cfa_offset 16
Ltmp60:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp61:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1E4funcEv
	cmpq	$0, %rax
	movq	%rax, -16(%rbp)         ## 8-byte Spill
	je	LBB18_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	jmp	LBB18_3
LBB18_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -24(%rbp)         ## 8-byte Spill
	jmp	LBB18_3
LBB18_3:
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1E4funcEv
	.weak_def_can_be_hidden	__ZN1E4funcEv
	.align	4, 0x90
__ZN1E4funcEv:                          ## @_ZN1E4funcEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp62:
	.cfi_def_cfa_offset 16
Ltmp63:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp64:
	.cfi_def_cfa_register %rbp
	xorl	%eax, %eax
                                        ## 
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1DD1Ev
	.weak_def_can_be_hidden	__ZN1DD1Ev
	.align	4, 0x90
__ZN1DD1Ev:                             ## @_ZN1DD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp65:
	.cfi_def_cfa_offset 16
Ltmp66:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp67:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1DD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1DD0Ev
	.weak_def_can_be_hidden	__ZN1DD0Ev
	.align	4, 0x90
__ZN1DD0Ev:                             ## @_ZN1DD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp68:
	.cfi_def_cfa_offset 16
Ltmp69:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp70:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN1DD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1DD2Ev
	.weak_def_can_be_hidden	__ZN1DD2Ev
	.align	4, 0x90
__ZN1DD2Ev:                             ## @_ZN1DD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp71:
	.cfi_def_cfa_offset 16
Ltmp72:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp73:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN1ED2Ev
	.weak_def_can_be_hidden	__ZN1ED2Ev
	.align	4, 0x90
__ZN1ED2Ev:                             ## @_ZN1ED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp74:
	.cfi_def_cfa_offset 16
Ltmp75:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp76:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN1DD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZN1FD2Ev
	.weak_def_can_be_hidden	__ZN1FD2Ev
	.align	4, 0x90
__ZN1FD2Ev:                             ## @_ZN1FD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp77:
	.cfi_def_cfa_offset 16
Ltmp78:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp79:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTV1F@GOTPCREL(%rip), %rax
	movq	%rax, %rcx
	addq	$56, %rcx
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rcx, 8(%rdi)
	movq	%rdi, %rax
	addq	$16, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	addq	$8, %rax
	movq	%rax, %rdi
	callq	__ZN1ED2Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZN1AD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTV1F                 ## @_ZTV1F
	.weak_def_can_be_hidden	__ZTV1F
	.align	3
__ZTV1F:
	.quad	0
	.quad	__ZTI1F
	.quad	__ZN1FD1Ev
	.quad	__ZN1FD0Ev
	.quad	__ZN1F4funcEv
	.quad	-8
	.quad	__ZTI1F
	.quad	__ZThn8_N1FD1Ev
	.quad	__ZThn8_N1FD0Ev
	.quad	__ZTchn8_h8_N1F4funcEv
	.quad	__ZThn8_N1F4funcEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS1F                 ## @_ZTS1F
	.weak_definition	__ZTS1F
__ZTS1F:
	.asciz	"1F"

	.globl	__ZTS1A                 ## @_ZTS1A
	.weak_definition	__ZTS1A
__ZTS1A:
	.asciz	"1A"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI1A                 ## @_ZTI1A
	.weak_definition	__ZTI1A
	.align	3
__ZTI1A:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS1A

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS1E                 ## @_ZTS1E
	.weak_definition	__ZTS1E
__ZTS1E:
	.asciz	"1E"

	.globl	__ZTS1D                 ## @_ZTS1D
	.weak_definition	__ZTS1D
__ZTS1D:
	.asciz	"1D"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI1D                 ## @_ZTI1D
	.weak_definition	__ZTI1D
	.align	3
__ZTI1D:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS1D

	.globl	__ZTI1E                 ## @_ZTI1E
	.weak_definition	__ZTI1E
	.align	4
__ZTI1E:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS1E
	.quad	__ZTI1D

	.globl	__ZTI1F                 ## @_ZTI1F
	.weak_definition	__ZTI1F
	.align	4
__ZTI1F:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTS1F
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTI1A
	.quad	2                       ## 0x2
	.quad	__ZTI1E
	.quad	2050                    ## 0x802

	.globl	__ZTV1A                 ## @_ZTV1A
	.weak_def_can_be_hidden	__ZTV1A
	.align	3
__ZTV1A:
	.quad	0
	.quad	__ZTI1A
	.quad	__ZN1AD1Ev
	.quad	__ZN1AD0Ev

	.globl	__ZTV1E                 ## @_ZTV1E
	.weak_def_can_be_hidden	__ZTV1E
	.align	3
__ZTV1E:
	.quad	0
	.quad	__ZTI1E
	.quad	__ZN1ED1Ev
	.quad	__ZN1ED0Ev
	.quad	__ZTch0_h8_N1E4funcEv
	.quad	__ZN1E4funcEv

	.globl	__ZTV1D                 ## @_ZTV1D
	.weak_def_can_be_hidden	__ZTV1D
	.align	3
__ZTV1D:
	.quad	0
	.quad	__ZTI1D
	.quad	__ZN1DD1Ev
	.quad	__ZN1DD0Ev
	.quad	___cxa_pure_virtual

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"Why does the act of setting this field cause a crash?"


.subsections_via_symbols
