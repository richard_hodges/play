	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z8clearmemPvm
	.align	4, 0x90
__Z8clearmemPvm:                        ## @_Z8clearmemPvm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	testq	%rsi, %rsi
	je	LBB0_7
## BB#1:                                ## %.lr.ph.preheader
	leaq	-1(%rsi), %rcx
	testb	$7, %sil
	je	LBB0_2
## BB#3:                                ## %.lr.ph.prol.preheader
	movl	%esi, %edx
	andl	$7, %edx
	negq	%rdx
	movq	%rdi, %rax
	.align	4, 0x90
LBB0_4:                                 ## %.lr.ph.prol
                                        ## =>This Inner Loop Header: Depth=1
	decq	%rsi
	movb	$0, (%rax)
	incq	%rax
	incq	%rdx
	jne	LBB0_4
	jmp	LBB0_5
LBB0_2:
	movq	%rdi, %rax
LBB0_5:                                 ## %.lr.ph.preheader.split
	cmpq	$7, %rcx
	jb	LBB0_7
	.align	4, 0x90
LBB0_6:                                 ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	movb	$0, (%rax)
	movb	$0, 1(%rax)
	movb	$0, 2(%rax)
	movb	$0, 3(%rax)
	movb	$0, 4(%rax)
	movb	$0, 5(%rax)
	movb	$0, 6(%rax)
	addq	$-8, %rsi
	movb	$0, 7(%rax)
	leaq	8(%rax), %rax
	jne	LBB0_6
LBB0_7:                                 ## %._crit_edge
	movq	%rdi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z11use_privacy1A
	.align	4, 0x90
__Z11use_privacy1A:                     ## @_Z11use_privacy1A
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$120, %rsp
Ltmp6:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -16(%rbp)
	leaq	-120(%rbp), %rdi
	movl	$100, %edx
	movq	%rax, %rsi
	callq	_memcpy
	xorl	%eax, %eax
	.align	4, 0x90
LBB1_1:                                 ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movb	$0, -120(%rbp,%rax)
	movb	$0, -119(%rbp,%rax)
	movb	$0, -118(%rbp,%rax)
	movb	$0, -117(%rbp,%rax)
	movb	$0, -116(%rbp,%rax)
	movb	$0, -115(%rbp,%rax)
	movb	$0, -114(%rbp,%rax)
	movb	$0, -113(%rbp,%rax)
	movb	$0, -112(%rbp,%rax)
	movb	$0, -111(%rbp,%rax)
	addq	$10, %rax
	cmpq	$100, %rax
	jne	LBB1_1
## BB#2:                                ## %_ZN1AD1Ev.exit
	cmpq	-16(%rbp), %rbx
	jne	LBB1_4
## BB#3:                                ## %_ZN1AD1Ev.exit
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	retq
LBB1_4:                                 ## %_ZN1AD1Ev.exit
	callq	___stack_chk_fail
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp7:
	.cfi_def_cfa_offset 16
Ltmp8:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp9:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$320, %rsp              ## imm = 0x140
Ltmp10:
	.cfi_offset %rbx, -32
Ltmp11:
	.cfi_offset %r14, -24
	movq	___stack_chk_guard@GOTPCREL(%rip), %r14
	movq	(%r14), %r14
	movq	%r14, -24(%rbp)
	movabsq	$28556934595048048, %rax ## imm = 0x65746176697270
	movq	%rax, -227(%rbp)
	movabsq	$7598258806106252662, %rax ## imm = 0x6972702079726576
	movq	%rax, -232(%rbp)
	leaq	-336(%rbp), %rdi
	leaq	-232(%rbp), %rbx
	movl	$100, %edx
	movq	%rbx, %rsi
	callq	_memcpy
	leaq	-128(%rbp), %rdi
	movl	$100, %edx
	movq	%rbx, %rsi
	callq	_memcpy
	xorl	%eax, %eax
	.align	4, 0x90
LBB2_1:                                 ## %.lr.ph.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movb	$0, -128(%rbp,%rax)
	movb	$0, -127(%rbp,%rax)
	movb	$0, -126(%rbp,%rax)
	movb	$0, -125(%rbp,%rax)
	movb	$0, -124(%rbp,%rax)
	movb	$0, -123(%rbp,%rax)
	movb	$0, -122(%rbp,%rax)
	movb	$0, -121(%rbp,%rax)
	movb	$0, -120(%rbp,%rax)
	movb	$0, -119(%rbp,%rax)
	addq	$10, %rax
	cmpq	$100, %rax
	jne	LBB2_1
## BB#2:                                ## %_Z11use_privacy1A.exit
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.align	4, 0x90
LBB2_3:                                 ## %.lr.ph.i.i.i.3
                                        ## =>This Inner Loop Header: Depth=1
	movb	$0, -336(%rbp,%rcx)
	movb	$0, -335(%rbp,%rcx)
	movb	$0, -334(%rbp,%rcx)
	movb	$0, -333(%rbp,%rcx)
	movb	$0, -332(%rbp,%rcx)
	movb	$0, -331(%rbp,%rcx)
	movb	$0, -330(%rbp,%rcx)
	movb	$0, -329(%rbp,%rcx)
	movb	$0, -328(%rbp,%rcx)
	movb	$0, -327(%rbp,%rcx)
	addq	$10, %rcx
	cmpq	$100, %rcx
	jne	LBB2_3
	.align	4, 0x90
LBB2_4:                                 ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movb	$0, -232(%rbp,%rax)
	movb	$0, -231(%rbp,%rax)
	movb	$0, -230(%rbp,%rax)
	movb	$0, -229(%rbp,%rax)
	movb	$0, -228(%rbp,%rax)
	movb	$0, -227(%rbp,%rax)
	movb	$0, -226(%rbp,%rax)
	movb	$0, -225(%rbp,%rax)
	movb	$0, -224(%rbp,%rax)
	movb	$0, -223(%rbp,%rax)
	addq	$10, %rax
	cmpq	$100, %rax
	jne	LBB2_4
## BB#5:                                ## %_ZN1AD1Ev.exit
	cmpq	-24(%rbp), %r14
	jne	LBB2_7
## BB#6:                                ## %_ZN1AD1Ev.exit
	xorl	%eax, %eax
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB2_7:                                 ## %_ZN1AD1Ev.exit
	callq	___stack_chk_fail
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"very private"


.subsections_via_symbols
