	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z6MyFunc16ForThread_struct
	.align	4, 0x90
__Z6MyFunc16ForThread_struct:           ## @_Z6MyFunc16ForThread_struct
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movl	%edi, -8(%rbp)
	cmpl	$0, -8(%rbp)
	jne	LBB0_4
## BB#1:
	movl	$16, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
Ltmp0:
	leaq	L_.str(%rip), %rsi
	movq	%rdi, -32(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt13runtime_errorC1EPKc
Ltmp1:
	jmp	LBB0_2
LBB0_2:
	movq	__ZTISt13runtime_error@GOTPCREL(%rip), %rax
	movq	__ZNSt13runtime_errorD1Ev@GOTPCREL(%rip), %rcx
	movq	-32(%rbp), %rdi         ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
LBB0_3:
Ltmp2:
	movl	%edx, %ecx
	movq	%rax, -16(%rbp)
	movl	%ecx, -20(%rbp)
	movq	-32(%rbp), %rdi         ## 8-byte Reload
	callq	___cxa_free_exception
	jmp	LBB0_5
LBB0_4:
	leaq	L_.str.1(%rip), %rax
	addq	$32, %rsp
	popq	%rbp
	retq
LBB0_5:
	movq	-16(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp1-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Lfunc_end0-Ltmp1                ##   Call between Ltmp1 and Lfunc_end0
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp31:
	.cfi_def_cfa_offset 16
Ltmp32:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp33:
	.cfi_def_cfa_register %rbp
	subq	$832, %rsp              ## imm = 0x340
	leaq	-32(%rbp), %rax
	leaq	-552(%rbp), %rcx
	leaq	-576(%rbp), %rdx
	leaq	-656(%rbp), %rsi
	movq	___stack_chk_guard@GOTPCREL(%rip), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -8(%rbp)
	movl	$0, -628(%rbp)
	movl	$0, -32(%rbp)
	movl	$1, -28(%rbp)
	movl	$0, -24(%rbp)
	movl	$2, -20(%rbp)
	movl	$0, -16(%rbp)
	movq	%rsi, -624(%rbp)
	movq	-624(%rbp), %rsi
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movq	%rsi, -608(%rbp)
	movq	-608(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -600(%rbp)
	movq	$0, (%rsi)
	movq	$0, 8(%rsi)
	addq	$16, %rsi
	movq	%rsi, -584(%rbp)
	movq	$0, -592(%rbp)
	movq	-584(%rbp), %rsi
	movq	-592(%rbp), %rdi
	movq	%rsi, -568(%rbp)
	movq	%rdi, -576(%rbp)
	movq	-568(%rbp), %rsi
	movq	%rdx, -560(%rbp)
	movq	-560(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rsi, -544(%rbp)
	movq	%rdx, -552(%rbp)
	movq	-544(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rsi, -536(%rbp)
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	%rax, -664(%rbp)
	movq	-664(%rbp), %rax
	movq	%rax, -672(%rbp)
	movq	-664(%rbp), %rax
	addq	$20, %rax
	movq	%rax, -680(%rbp)
LBB1_1:                                 ## =>This Inner Loop Header: Depth=1
	movq	-672(%rbp), %rax
	cmpq	-680(%rbp), %rax
	je	LBB1_13
## BB#2:                                ##   in Loop: Header=BB1_1 Depth=1
	movq	-672(%rbp), %rax
	movq	%rax, -688(%rbp)
	leaq	__Z6MyFunc16ForThread_struct(%rip), %rax
	movq	%rax, -704(%rbp)
	movq	-688(%rbp), %rcx
Ltmp23:
	leaq	-696(%rbp), %rdi
	movl	$1, %esi
	leaq	-704(%rbp), %rdx
	callq	__ZNSt3__15asyncIPFPKc16ForThread_structEJRKS3_EEENS_6futureINS_11__invoke_ofINS_5decayIT_E4typeEJDpNSA_IT0_E4typeEEE4typeEEENS_6launchEOSB_DpOSE_
Ltmp24:
	jmp	LBB1_3
LBB1_3:                                 ##   in Loop: Header=BB1_1 Depth=1
	leaq	-696(%rbp), %rax
	leaq	-656(%rbp), %rcx
	movq	%rcx, -504(%rbp)
	movq	%rax, -512(%rbp)
	movq	-504(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, %rdx
	movq	%rdx, -496(%rbp)
	movq	-496(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	cmpq	(%rdx), %rcx
	movq	%rax, -776(%rbp)        ## 8-byte Spill
	jae	LBB1_6
## BB#4:                                ##   in Loop: Header=BB1_1 Depth=1
Ltmp28:
	movl	$1, %eax
	movl	%eax, %edx
	leaq	-520(%rbp), %rdi
	movq	-776(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC1ERKS7_m
Ltmp29:
	jmp	LBB1_5
LBB1_5:                                 ## %.noexc
                                        ##   in Loop: Header=BB1_1 Depth=1
	leaq	-520(%rbp), %rdi
	movq	-776(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rax
	addq	$16, %rax
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	-456(%rbp), %rax
	movq	-776(%rbp), %rcx        ## 8-byte Reload
	movq	8(%rcx), %rdx
	movq	%rdx, -440(%rbp)
	movq	-440(%rbp), %rdx
	movq	-512(%rbp), %rsi
	movq	%rsi, -280(%rbp)
	movq	-280(%rbp), %rsi
	movq	%rax, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%rsi, -416(%rbp)
	movq	-400(%rbp), %rax
	movq	-408(%rbp), %rdx
	movq	-416(%rbp), %rsi
	movq	%rsi, -392(%rbp)
	movq	-392(%rbp), %rsi
	movq	%rax, -368(%rbp)
	movq	%rdx, -376(%rbp)
	movq	%rsi, -384(%rbp)
	movq	-368(%rbp), %rax
	movq	-376(%rbp), %rdx
	movq	-384(%rbp), %rsi
	movq	%rsi, -352(%rbp)
	movq	-352(%rbp), %rsi
	movq	%rax, -328(%rbp)
	movq	%rdx, -336(%rbp)
	movq	%rsi, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rdx
	movq	%rdx, -320(%rbp)
	movq	-320(%rbp), %rdx
	movq	%rax, -304(%rbp)
	movq	%rdx, -312(%rbp)
	movq	-304(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	%rax, -288(%rbp)
	movq	%rdx, -296(%rbp)
	movq	-288(%rbp), %rax
	movq	-296(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	-296(%rbp), %rax
	movq	$0, (%rax)
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotator6__doneEv
	movq	-776(%rbp), %rax        ## 8-byte Reload
	movq	8(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, 8(%rax)
	jmp	LBB1_8
LBB1_6:                                 ##   in Loop: Header=BB1_1 Depth=1
	movq	-512(%rbp), %rax
	movq	%rax, -448(%rbp)
Ltmp26:
	movq	-776(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_
Ltmp27:
	jmp	LBB1_7
LBB1_7:                                 ## %.noexc4
                                        ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_8
LBB1_8:                                 ## %_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE9push_backEOS4_.exit
                                        ##   in Loop: Header=BB1_1 Depth=1
	jmp	LBB1_9
LBB1_9:                                 ##   in Loop: Header=BB1_1 Depth=1
	leaq	-696(%rbp), %rdi
	callq	__ZNSt3__16futureIPKcED1Ev
## BB#10:                               ##   in Loop: Header=BB1_1 Depth=1
	movq	-672(%rbp), %rax
	addq	$4, %rax
	movq	%rax, -672(%rbp)
	jmp	LBB1_1
LBB1_11:
Ltmp25:
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)
	movl	%ecx, -716(%rbp)
	jmp	LBB1_33
LBB1_12:
Ltmp30:
	leaq	-696(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)
	movl	%ecx, -716(%rbp)
	callq	__ZNSt3__16futureIPKcED1Ev
	jmp	LBB1_33
LBB1_13:
	leaq	-168(%rbp), %rax
	leaq	-240(%rbp), %rcx
	leaq	-656(%rbp), %rdx
	movq	%rdx, -728(%rbp)
	movq	-728(%rbp), %rdx
	movq	%rdx, -272(%rbp)
	movq	-272(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	%rdx, -248(%rbp)
	movq	%rsi, -256(%rbp)
	movq	-256(%rbp), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	-232(%rbp), %rdx
	movq	%rcx, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	-240(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -736(%rbp)
	movq	-728(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	%rcx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rax, -152(%rbp)
	movq	%rcx, -160(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	-168(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -744(%rbp)
LBB1_14:                                ## =>This Inner Loop Header: Depth=1
	leaq	-744(%rbp), %rax
	leaq	-736(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	%rax, -104(%rbp)
	movq	%rcx, -112(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	movq	-112(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dl
	xorb	$-1, %dl
	testb	$1, %dl
	jne	LBB1_15
	jmp	LBB1_31
LBB1_15:                                ##   in Loop: Header=BB1_14 Depth=1
	leaq	-736(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-736(%rbp), %rax
	movq	%rax, -752(%rbp)
Ltmp6:
	movq	%rax, %rdi
	callq	__ZNSt3__16futureIPKcE3getEv
Ltmp7:
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	jmp	LBB1_16
LBB1_16:                                ##   in Loop: Header=BB1_14 Depth=1
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -760(%rbp)
Ltmp8:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp9:
	movq	%rax, -792(%rbp)        ## 8-byte Spill
	jmp	LBB1_17
LBB1_17:                                ##   in Loop: Header=BB1_14 Depth=1
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rdi
Ltmp10:
	callq	*%rcx
Ltmp11:
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	jmp	LBB1_18
LBB1_18:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
                                        ##   in Loop: Header=BB1_14 Depth=1
	jmp	LBB1_19
LBB1_19:                                ##   in Loop: Header=BB1_14 Depth=1
	jmp	LBB1_27
LBB1_20:                                ##   in Loop: Header=BB1_14 Depth=1
Ltmp12:
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)
	movl	%ecx, -716(%rbp)
## BB#21:                               ##   in Loop: Header=BB1_14 Depth=1
	movl	-716(%rbp), %eax
	movl	$1, %ecx
	cmpl	%ecx, %eax
	jne	LBB1_33
## BB#22:                               ##   in Loop: Header=BB1_14 Depth=1
	movq	-712(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -768(%rbp)
	movq	(%rax), %rdi
	movq	16(%rdi), %rdi
	movq	%rdi, -808(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-808(%rbp), %rax        ## 8-byte Reload
	callq	*%rax
Ltmp13:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	%rax, %rsi
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
Ltmp14:
	movq	%rax, -816(%rbp)        ## 8-byte Spill
	jmp	LBB1_23
LBB1_23:                                ##   in Loop: Header=BB1_14 Depth=1
	movq	-816(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	-48(%rbp), %rdi
Ltmp15:
	callq	*%rcx
Ltmp16:
	movq	%rax, -824(%rbp)        ## 8-byte Spill
	jmp	LBB1_24
LBB1_24:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit7
                                        ##   in Loop: Header=BB1_14 Depth=1
	jmp	LBB1_25
LBB1_25:                                ##   in Loop: Header=BB1_14 Depth=1
Ltmp21:
	callq	___cxa_end_catch
Ltmp22:
	jmp	LBB1_26
LBB1_26:                                ##   in Loop: Header=BB1_14 Depth=1
	jmp	LBB1_27
LBB1_27:                                ##   in Loop: Header=BB1_14 Depth=1
	jmp	LBB1_28
LBB1_28:                                ##   in Loop: Header=BB1_14 Depth=1
	leaq	-736(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rcx
	addq	$8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB1_14
LBB1_29:
Ltmp17:
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)
	movl	%ecx, -716(%rbp)
Ltmp18:
	callq	___cxa_end_catch
Ltmp19:
	jmp	LBB1_30
LBB1_30:
	jmp	LBB1_33
LBB1_31:
	leaq	-656(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev
	movq	___stack_chk_guard@GOTPCREL(%rip), %rdi
	movl	-628(%rbp), %eax
	movq	(%rdi), %rdi
	cmpq	-8(%rbp), %rdi
	movl	%eax, -828(%rbp)        ## 4-byte Spill
	jne	LBB1_36
## BB#32:                               ## %SP_return
	movl	-828(%rbp), %eax        ## 4-byte Reload
	addq	$832, %rsp              ## imm = 0x340
	popq	%rbp
	retq
LBB1_33:
	leaq	-656(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev
## BB#34:
	movq	-712(%rbp), %rdi
	callq	__Unwind_Resume
LBB1_35:
Ltmp20:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -832(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB1_36:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\370"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	104                     ## Call site table length
Lset7 = Ltmp23-Lfunc_begin1             ## >> Call Site 1 <<
	.long	Lset7
Lset8 = Ltmp24-Ltmp23                   ##   Call between Ltmp23 and Ltmp24
	.long	Lset8
Lset9 = Ltmp25-Lfunc_begin1             ##     jumps to Ltmp25
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp28-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset10
Lset11 = Ltmp27-Ltmp28                  ##   Call between Ltmp28 and Ltmp27
	.long	Lset11
Lset12 = Ltmp30-Lfunc_begin1            ##     jumps to Ltmp30
	.long	Lset12
	.byte	0                       ##   On action: cleanup
Lset13 = Ltmp6-Lfunc_begin1             ## >> Call Site 3 <<
	.long	Lset13
Lset14 = Ltmp11-Ltmp6                   ##   Call between Ltmp6 and Ltmp11
	.long	Lset14
Lset15 = Ltmp12-Lfunc_begin1            ##     jumps to Ltmp12
	.long	Lset15
	.byte	3                       ##   On action: 2
Lset16 = Ltmp11-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset16
Lset17 = Ltmp13-Ltmp11                  ##   Call between Ltmp11 and Ltmp13
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp13-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset18
Lset19 = Ltmp16-Ltmp13                  ##   Call between Ltmp13 and Ltmp16
	.long	Lset19
Lset20 = Ltmp17-Lfunc_begin1            ##     jumps to Ltmp17
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp21-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset21
Lset22 = Ltmp22-Ltmp21                  ##   Call between Ltmp21 and Ltmp22
	.long	Lset22
Lset23 = Ltmp25-Lfunc_begin1            ##     jumps to Ltmp25
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp18-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset24
Lset25 = Ltmp19-Ltmp18                  ##   Call between Ltmp18 and Ltmp19
	.long	Lset25
Lset26 = Ltmp20-Lfunc_begin1            ##     jumps to Ltmp20
	.long	Lset26
	.byte	5                       ##   On action: 3
Lset27 = Ltmp19-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset27
Lset28 = Lfunc_end1-Ltmp19              ##   Call between Ltmp19 and Lfunc_end1
	.long	Lset28
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	2                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 2
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 2
	.long	__ZTISt9exception@GOTPCREL+4 ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__15asyncIPFPKc16ForThread_structEJRKS3_EEENS_6futureINS_11__invoke_ofINS_5decayIT_E4typeEJDpNSA_IT0_E4typeEEE4typeEEENS_6launchEOSB_DpOSE_
	.weak_def_can_be_hidden	__ZNSt3__15asyncIPFPKc16ForThread_structEJRKS3_EEENS_6futureINS_11__invoke_ofINS_5decayIT_E4typeEJDpNSA_IT0_E4typeEEE4typeEEENS_6launchEOSB_DpOSE_
	.align	4, 0x90
__ZNSt3__15asyncIPFPKc16ForThread_structEJRKS3_EEENS_6futureINS_11__invoke_ofINS_5decayIT_E4typeEJDpNSA_IT0_E4typeEEE4typeEEENS_6launchEOSB_DpOSE_: ## @_ZNSt3__15asyncIPFPKc16ForThread_structEJRKS3_EEENS_6futureINS_11__invoke_ofINS_5decayIT_E4typeEJDpNSA_IT0_E4typeEEE4typeEEENS_6launchEOSB_DpOSE_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp43:
	.cfi_def_cfa_offset 16
Ltmp44:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp45:
	.cfi_def_cfa_register %rbp
	subq	$976, %rsp              ## imm = 0x3D0
	movq	%rdi, %rax
	movl	%esi, -820(%rbp)
	movq	%rdx, -832(%rbp)
	movq	%rcx, -840(%rbp)
	movl	-820(%rbp), %esi
	movl	%esi, -812(%rbp)
	movl	$1, -816(%rbp)
	movl	-812(%rbp), %esi
	andl	-816(%rbp), %esi
	cmpl	$0, %esi
	setne	%r8b
	movq	%rax, -928(%rbp)        ## 8-byte Spill
	movq	%rdi, -936(%rbp)        ## 8-byte Spill
	movb	%r8b, -937(%rbp)        ## 1-byte Spill
## BB#1:
	movb	-937(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB2_2
	jmp	LBB2_10
LBB2_2:
	movq	-832(%rbp), %rax
	movq	%rax, -808(%rbp)
	movq	-808(%rbp), %rax
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rax
	movq	%rax, -776(%rbp)
	movq	-776(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -952(%rbp)        ## 8-byte Spill
## BB#3:
	movq	-952(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -880(%rbp)
	movq	-840(%rbp), %rcx
	movq	%rcx, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movl	(%rcx), %edx
	movl	%edx, -16(%rbp)
	movl	-16(%rbp), %edx
	movl	%edx, -956(%rbp)        ## 4-byte Spill
## BB#4:
	leaq	-888(%rbp), %rax
	leaq	-880(%rbp), %rcx
	leaq	-872(%rbp), %rdx
	movl	-956(%rbp), %esi        ## 4-byte Reload
	movl	%esi, -888(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	-352(%rbp), %rcx
	movq	-360(%rbp), %rdx
	movq	%rax, -320(%rbp)
	movq	%rcx, -328(%rbp)
	movq	%rdx, -336(%rbp)
	movq	-320(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	-336(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rax, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	%rdx, -304(%rbp)
	movq	-288(%rbp), %rax
	movq	-296(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	%rdx, -248(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	-248(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movq	%rcx, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-200(%rbp), %rax
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	%rdx, -160(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, %rcx
	movq	-152(%rbp), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	%rcx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	addq	$8, %rax
	movq	-160(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	movl	(%rcx), %edi
	movl	%edi, (%rax)
## BB#5:
Ltmp34:
	leaq	-872(%rbp), %rsi
	movq	-936(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__124__make_async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
Ltmp35:
	jmp	LBB2_6
LBB2_6:
	jmp	LBB2_17
LBB2_7:
Ltmp36:
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
## BB#8:
	movq	-848(%rbp), %rdi
	callq	___cxa_begin_catch
	cmpl	$1, -820(%rbp)
	movq	%rax, -968(%rbp)        ## 8-byte Spill
	jne	LBB2_12
## BB#9:
Ltmp37:
	callq	___cxa_rethrow
Ltmp38:
	jmp	LBB2_20
LBB2_10:
	jmp	LBB2_13
LBB2_11:
Ltmp39:
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
Ltmp40:
	callq	___cxa_end_catch
Ltmp41:
	jmp	LBB2_15
LBB2_12:
	callq	___cxa_end_catch
LBB2_13:
	movl	-820(%rbp), %eax
	movl	%eax, -372(%rbp)
	movl	$2, -376(%rbp)
	movl	-372(%rbp), %eax
	andl	-376(%rbp), %eax
	cmpl	$0, %eax
	je	LBB2_16
## BB#14:
	leaq	-904(%rbp), %rax
	leaq	-920(%rbp), %rcx
	leaq	-912(%rbp), %rdx
	movq	-832(%rbp), %rsi
	movq	%rsi, -384(%rbp)
	movq	-384(%rbp), %rsi
	movq	%rsi, -400(%rbp)
	movq	-400(%rbp), %rsi
	movq	%rsi, -392(%rbp)
	movq	-392(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -912(%rbp)
	movq	-840(%rbp), %rsi
	movq	%rsi, -408(%rbp)
	movq	-408(%rbp), %rsi
	movq	%rsi, -432(%rbp)
	movq	-432(%rbp), %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movl	(%rsi), %edi
	movl	%edi, -424(%rbp)
	movl	-424(%rbp), %edi
	movl	%edi, -920(%rbp)
	movq	%rax, -752(%rbp)
	movq	%rdx, -760(%rbp)
	movq	%rcx, -768(%rbp)
	movq	-752(%rbp), %rcx
	movq	-760(%rbp), %rdx
	movq	-768(%rbp), %rsi
	movq	%rcx, -728(%rbp)
	movq	%rdx, -736(%rbp)
	movq	%rsi, -744(%rbp)
	movq	-728(%rbp), %rcx
	movq	-736(%rbp), %rdx
	movq	%rdx, -720(%rbp)
	movq	-720(%rbp), %rdx
	movq	-744(%rbp), %rsi
	movq	%rsi, -440(%rbp)
	movq	-440(%rbp), %rsi
	movq	%rcx, -696(%rbp)
	movq	%rdx, -704(%rbp)
	movq	%rsi, -712(%rbp)
	movq	-696(%rbp), %rcx
	movq	-704(%rbp), %rdx
	movq	-712(%rbp), %rsi
	movq	%rcx, -640(%rbp)
	movq	%rdx, -648(%rbp)
	movq	%rsi, -656(%rbp)
	movq	-640(%rbp), %rcx
	movq	-648(%rbp), %rdx
	movq	%rdx, -632(%rbp)
	movq	-632(%rbp), %rdx
	movq	-656(%rbp), %rsi
	movq	%rsi, -448(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rcx, -608(%rbp)
	movq	%rdx, -616(%rbp)
	movq	%rsi, -624(%rbp)
	movq	-608(%rbp), %rcx
	movq	-616(%rbp), %rdx
	movq	-624(%rbp), %rsi
	movq	%rcx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movq	%rsi, -568(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, %rdx
	movq	-560(%rbp), %rsi
	movq	%rsi, -512(%rbp)
	movq	-512(%rbp), %rsi
	movq	%rdx, -464(%rbp)
	movq	%rsi, -472(%rbp)
	movq	-464(%rbp), %rdx
	movq	-472(%rbp), %rsi
	movq	%rsi, -456(%rbp)
	movq	-456(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	addq	$8, %rcx
	movq	-568(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	movq	%rcx, -496(%rbp)
	movq	%rdx, -504(%rbp)
	movq	-496(%rbp), %rcx
	movq	-504(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movl	(%rdx), %edi
	movl	%edi, (%rcx)
	movq	-936(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	callq	__ZNSt3__127__make_deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
	jmp	LBB2_17
LBB2_15:
	jmp	LBB2_18
LBB2_16:
	movq	-936(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -800(%rbp)
	movq	-800(%rbp), %rcx
	movq	%rcx, -792(%rbp)
	movq	-792(%rbp), %rcx
	movq	$0, (%rcx)
LBB2_17:
	movq	-928(%rbp), %rax        ## 8-byte Reload
	addq	$976, %rsp              ## imm = 0x3D0
	popq	%rbp
	retq
LBB2_18:
	movq	-848(%rbp), %rdi
	callq	__Unwind_Resume
LBB2_19:
Ltmp42:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -972(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB2_20:
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset29 = Ltmp34-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset29
Lset30 = Ltmp35-Ltmp34                  ##   Call between Ltmp34 and Ltmp35
	.long	Lset30
Lset31 = Ltmp36-Lfunc_begin2            ##     jumps to Ltmp36
	.long	Lset31
	.byte	1                       ##   On action: 1
Lset32 = Ltmp35-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset32
Lset33 = Ltmp37-Ltmp35                  ##   Call between Ltmp35 and Ltmp37
	.long	Lset33
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset34 = Ltmp37-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset34
Lset35 = Ltmp38-Ltmp37                  ##   Call between Ltmp37 and Ltmp38
	.long	Lset35
Lset36 = Ltmp39-Lfunc_begin2            ##     jumps to Ltmp39
	.long	Lset36
	.byte	0                       ##   On action: cleanup
Lset37 = Ltmp40-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset37
Lset38 = Ltmp41-Ltmp40                  ##   Call between Ltmp40 and Ltmp41
	.long	Lset38
Lset39 = Ltmp42-Lfunc_begin2            ##     jumps to Ltmp42
	.long	Lset39
	.byte	1                       ##   On action: 1
Lset40 = Ltmp41-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset40
Lset41 = Lfunc_end2-Ltmp41              ##   Call between Ltmp41 and Lfunc_end2
	.long	Lset41
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16futureIPKcED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16futureIPKcED1Ev
	.align	4, 0x90
__ZNSt3__16futureIPKcED1Ev:             ## @_ZNSt3__16futureIPKcED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp46:
	.cfi_def_cfa_offset 16
Ltmp47:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp48:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16futureIPKcED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16futureIPKcE3getEv
	.weak_def_can_be_hidden	__ZNSt3__16futureIPKcE3getEv
	.align	4, 0x90
__ZNSt3__16futureIPKcE3getEv:           ## @_ZNSt3__16futureIPKcE3getEv
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp58:
	.cfi_def_cfa_offset 16
Ltmp59:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp60:
	.cfi_def_cfa_register %rbp
	subq	$368, %rsp              ## imm = 0x170
	movq	%rdi, -288(%rbp)
	movq	(%rdi), %rax
	leaq	-296(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	%rax, -264(%rbp)
	movq	-256(%rbp), %rax
	leaq	-264(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	-216(%rbp), %rax
	leaq	-224(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rax, -192(%rbp)
	movq	%rcx, -200(%rbp)
	movq	-192(%rbp), %rax
	leaq	-200(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	-200(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	(%rdi), %rax
	movq	%rax, -304(%rbp)
	movq	$0, (%rdi)
	movq	-304(%rbp), %rdi
Ltmp49:
	callq	__ZNSt3__113__assoc_stateIPKcE4moveEv
Ltmp50:
	movq	%rax, -328(%rbp)        ## 8-byte Spill
	jmp	LBB4_1
LBB4_1:
	leaq	-296(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -72(%rbp)
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	je	LBB4_5
## BB#2:
	movq	-336(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	-72(%rbp), %rsi
Ltmp55:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp56:
	jmp	LBB4_3
LBB4_3:
	jmp	LBB4_5
LBB4_4:
Ltmp57:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -340(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB4_5:                                 ## %_ZNSt3__110unique_ptrINS_14__shared_countENS_22__release_shared_countEED1Ev.exit2
	movq	-328(%rbp), %rax        ## 8-byte Reload
	addq	$368, %rsp              ## imm = 0x170
	popq	%rbp
	retq
LBB4_6:
Ltmp51:
	leaq	-296(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -312(%rbp)
	movl	%esi, -316(%rbp)
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	$0, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -160(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -160(%rbp)
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	je	LBB4_10
## BB#7:
	movq	-352(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	-160(%rbp), %rsi
Ltmp52:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp53:
	jmp	LBB4_8
LBB4_8:
	jmp	LBB4_10
LBB4_9:
Ltmp54:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB4_10:                                ## %_ZNSt3__110unique_ptrINS_14__shared_countENS_22__release_shared_countEED1Ev.exit
	jmp	LBB4_11
LBB4_11:
	movq	-312(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset42 = Ltmp49-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset42
Lset43 = Ltmp50-Ltmp49                  ##   Call between Ltmp49 and Ltmp50
	.long	Lset43
Lset44 = Ltmp51-Lfunc_begin3            ##     jumps to Ltmp51
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp55-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset45
Lset46 = Ltmp56-Ltmp55                  ##   Call between Ltmp55 and Ltmp56
	.long	Lset46
Lset47 = Ltmp57-Lfunc_begin3            ##     jumps to Ltmp57
	.long	Lset47
	.byte	1                       ##   On action: 1
Lset48 = Ltmp52-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset48
Lset49 = Ltmp53-Ltmp52                  ##   Call between Ltmp52 and Ltmp53
	.long	Lset49
Lset50 = Ltmp54-Lfunc_begin3            ##     jumps to Ltmp54
	.long	Lset50
	.byte	1                       ##   On action: 1
Lset51 = Ltmp53-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset51
Lset52 = Lfunc_end3-Ltmp53              ##   Call between Ltmp53 and Lfunc_end3
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-16(%rbp), %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-24(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp69:
	.cfi_def_cfa_offset 16
Ltmp70:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp71:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp64:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp65:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB6_1
LBB6_1:                                 ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp66:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp67:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB6_3
LBB6_2:
Ltmp68:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB6_3:                                 ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table6:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset53 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset53
Lset54 = Ltmp64-Lfunc_begin4            ##   Call between Lfunc_begin4 and Ltmp64
	.long	Lset54
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset55 = Ltmp64-Lfunc_begin4            ## >> Call Site 2 <<
	.long	Lset55
Lset56 = Ltmp67-Ltmp64                  ##   Call between Ltmp64 and Ltmp67
	.long	Lset56
Lset57 = Ltmp68-Lfunc_begin4            ##     jumps to Ltmp68
	.long	Lset57
	.byte	0                       ##   On action: cleanup
Lset58 = Ltmp67-Lfunc_begin4            ## >> Call Site 3 <<
	.long	Lset58
Lset59 = Lfunc_end4-Ltmp67              ##   Call between Ltmp67 and Lfunc_end4
	.long	Lset59
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp72:
	.cfi_def_cfa_offset 16
Ltmp73:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp74:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp75:
	.cfi_def_cfa_offset 16
Ltmp76:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp77:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.align	4, 0x90
__ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev: ## @_ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp78:
	.cfi_def_cfa_offset 16
Ltmp79:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp80:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	je	LBB10_5
## BB#1:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rcx, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -264(%rbp)        ## 8-byte Spill
LBB10_2:                                ## =>This Inner Loop Header: Depth=1
	movq	-232(%rbp), %rax
	movq	-264(%rbp), %rcx        ## 8-byte Reload
	cmpq	8(%rcx), %rax
	je	LBB10_4
## BB#3:                                ##   in Loop: Header=BB10_2 Depth=1
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 8(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdi
	callq	__ZNSt3__16futureIPKcED1Ev
	jmp	LBB10_2
LBB10_4:                                ## %_ZNSt3__113__vector_baseINS_6futureIPKcEENS_9allocatorIS4_EEE5clearEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	(%rax), %rdx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%rdi, -88(%rbp)
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %rsi
	movq	%rcx, -48(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rdi
	callq	__ZdlPv
LBB10_5:
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__make_async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
	.weak_def_can_be_hidden	__ZNSt3__124__make_async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
	.align	4, 0x90
__ZNSt3__124__make_async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_: ## @_ZNSt3__124__make_async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp98:
	.cfi_def_cfa_offset 16
Ltmp99:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp100:
	.cfi_def_cfa_register %rbp
	subq	$576, %rsp              ## imm = 0x240
	movq	%rdi, %rax
	movq	%rsi, -440(%rbp)
	movl	$168, %ecx
	movl	%ecx, %esi
	movq	%rdi, -504(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rax, -512(%rbp)        ## 8-byte Spill
	callq	__Znwm
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	-440(%rbp), %rdx
	movq	%rdx, -432(%rbp)
	movq	%rax, -328(%rbp)
	movq	%rdx, -336(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rdx, -304(%rbp)
	movq	-296(%rbp), %rax
Ltmp81:
	movq	%rdi, -520(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rax, -528(%rbp)        ## 8-byte Spill
	movq	%rsi, -536(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113__assoc_stateIPKcEC2Ev
Ltmp82:
	jmp	LBB11_1
LBB11_1:                                ## %_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEC1EOS7_.exit
	movq	__ZTVNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-528(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	addq	$152, %rcx
	movq	-304(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	-288(%rbp), %rax
	movq	%rcx, -272(%rbp)
	movq	%rax, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	-280(%rbp), %rcx
	movq	%rax, -256(%rbp)
	movq	%rcx, -264(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
## BB#2:
	leaq	-448(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-520(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rdx
	movq	%rdx, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	-216(%rbp), %rdx
	leaq	-224(%rbp), %rsi
	movq	%rsi, -208(%rbp)
	movq	-224(%rbp), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -176(%rbp)
	movq	%rsi, -184(%rbp)
	movq	-176(%rbp), %rdx
	leaq	-184(%rbp), %rsi
	movq	%rsi, -168(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rdx
	leaq	-160(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-160(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	$0, -480(%rbp)
	movq	$25, -488(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rax, -120(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -496(%rbp)
Ltmp84:
	leaq	-472(%rbp), %rdi
	leaq	-488(%rbp), %rsi
	leaq	-496(%rbp), %rdx
	callq	__ZNSt3__16threadC1IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
Ltmp85:
	jmp	LBB11_3
LBB11_3:
Ltmp86:
	leaq	-472(%rbp), %rdi
	callq	__ZNSt3__16thread6detachEv
Ltmp87:
	jmp	LBB11_4
LBB11_4:
	leaq	-472(%rbp), %rdi
	callq	__ZNSt3__16threadD1Ev
	leaq	-448(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	%rdi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	movq	-448(%rbp), %rsi
Ltmp89:
	movq	-504(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE
Ltmp90:
	jmp	LBB11_5
LBB11_5:
	leaq	-448(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	$0, -88(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -96(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -96(%rbp)
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	je	LBB11_9
## BB#6:
	movq	-544(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	%rax, -32(%rbp)
	movq	-96(%rbp), %rsi
Ltmp95:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp96:
	jmp	LBB11_7
LBB11_7:
	jmp	LBB11_9
LBB11_8:
Ltmp97:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -548(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB11_9:                                ## %_ZNSt3__110unique_ptrINS_19__async_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEENS_22__release_shared_countEED1Ev.exit2
	movq	-512(%rbp), %rax        ## 8-byte Reload
	addq	$576, %rsp              ## imm = 0x240
	popq	%rbp
	retq
LBB11_10:
Ltmp83:
	movl	%edx, %ecx
	movq	%rax, -456(%rbp)
	movl	%ecx, -460(%rbp)
	movq	-536(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdlPv
	jmp	LBB11_18
LBB11_11:
Ltmp91:
	movl	%edx, %ecx
	movq	%rax, -456(%rbp)
	movl	%ecx, -460(%rbp)
	jmp	LBB11_13
LBB11_12:
Ltmp88:
	leaq	-472(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -456(%rbp)
	movl	%ecx, -460(%rbp)
	callq	__ZNSt3__16threadD1Ev
LBB11_13:
	leaq	-448(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rax
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	$0, -400(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -408(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rdx
	movq	%rdx, -360(%rbp)
	movq	-360(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -408(%rbp)
	movq	%rax, -560(%rbp)        ## 8-byte Spill
	je	LBB11_17
## BB#14:
	movq	-560(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -352(%rbp)
	movq	%rax, -344(%rbp)
	movq	-408(%rbp), %rsi
Ltmp92:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp93:
	jmp	LBB11_15
LBB11_15:
	jmp	LBB11_17
LBB11_16:
Ltmp94:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -564(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB11_17:                               ## %_ZNSt3__110unique_ptrINS_19__async_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEENS_22__release_shared_countEED1Ev.exit
	jmp	LBB11_18
LBB11_18:
	movq	-456(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table11:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\360"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	104                     ## Call site table length
Lset60 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset60
Lset61 = Ltmp81-Lfunc_begin5            ##   Call between Lfunc_begin5 and Ltmp81
	.long	Lset61
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset62 = Ltmp81-Lfunc_begin5            ## >> Call Site 2 <<
	.long	Lset62
Lset63 = Ltmp82-Ltmp81                  ##   Call between Ltmp81 and Ltmp82
	.long	Lset63
Lset64 = Ltmp83-Lfunc_begin5            ##     jumps to Ltmp83
	.long	Lset64
	.byte	0                       ##   On action: cleanup
Lset65 = Ltmp84-Lfunc_begin5            ## >> Call Site 3 <<
	.long	Lset65
Lset66 = Ltmp85-Ltmp84                  ##   Call between Ltmp84 and Ltmp85
	.long	Lset66
Lset67 = Ltmp91-Lfunc_begin5            ##     jumps to Ltmp91
	.long	Lset67
	.byte	0                       ##   On action: cleanup
Lset68 = Ltmp86-Lfunc_begin5            ## >> Call Site 4 <<
	.long	Lset68
Lset69 = Ltmp87-Ltmp86                  ##   Call between Ltmp86 and Ltmp87
	.long	Lset69
Lset70 = Ltmp88-Lfunc_begin5            ##     jumps to Ltmp88
	.long	Lset70
	.byte	0                       ##   On action: cleanup
Lset71 = Ltmp89-Lfunc_begin5            ## >> Call Site 5 <<
	.long	Lset71
Lset72 = Ltmp90-Ltmp89                  ##   Call between Ltmp89 and Ltmp90
	.long	Lset72
Lset73 = Ltmp91-Lfunc_begin5            ##     jumps to Ltmp91
	.long	Lset73
	.byte	0                       ##   On action: cleanup
Lset74 = Ltmp95-Lfunc_begin5            ## >> Call Site 6 <<
	.long	Lset74
Lset75 = Ltmp96-Ltmp95                  ##   Call between Ltmp95 and Ltmp96
	.long	Lset75
Lset76 = Ltmp97-Lfunc_begin5            ##     jumps to Ltmp97
	.long	Lset76
	.byte	1                       ##   On action: 1
Lset77 = Ltmp92-Lfunc_begin5            ## >> Call Site 7 <<
	.long	Lset77
Lset78 = Ltmp93-Ltmp92                  ##   Call between Ltmp92 and Ltmp93
	.long	Lset78
Lset79 = Ltmp94-Lfunc_begin5            ##     jumps to Ltmp94
	.long	Lset79
	.byte	1                       ##   On action: 1
Lset80 = Ltmp93-Lfunc_begin5            ## >> Call Site 8 <<
	.long	Lset80
Lset81 = Lfunc_end5-Ltmp93              ##   Call between Ltmp93 and Lfunc_end5
	.long	Lset81
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__127__make_deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
	.weak_def_can_be_hidden	__ZNSt3__127__make_deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
	.align	4, 0x90
__ZNSt3__127__make_deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_: ## @_ZNSt3__127__make_deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEENS_6futureIT_EEOT0_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp113:
	.cfi_def_cfa_offset 16
Ltmp114:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp115:
	.cfi_def_cfa_register %rbp
	subq	$528, %rsp              ## imm = 0x210
	movq	%rdi, %rax
	movq	%rsi, -424(%rbp)
	movl	$168, %ecx
	movl	%ecx, %esi
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	callq	__Znwm
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	-424(%rbp), %rdx
	movq	%rdx, -416(%rbp)
	movq	%rax, -312(%rbp)
	movq	%rdx, -320(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	%rdx, -288(%rbp)
	movq	-280(%rbp), %rax
Ltmp101:
	movq	%rdi, -472(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rax, -480(%rbp)        ## 8-byte Spill
	movq	%rsi, -488(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113__assoc_stateIPKcEC2Ev
Ltmp102:
	jmp	LBB12_1
LBB12_1:                                ## %_ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEC1EOS7_.exit
	movq	__ZTVNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	-480(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	addq	$152, %rcx
	movq	-288(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rax
	movq	%rcx, -256(%rbp)
	movq	%rax, -264(%rbp)
	movq	-256(%rbp), %rax
	movq	-264(%rbp), %rcx
	movq	%rax, -240(%rbp)
	movq	%rcx, -248(%rbp)
	movq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, (%rax)
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	movq	-480(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -224(%rbp)
	movq	-224(%rbp), %rax
	movl	136(%rax), %esi
	orl	$8, %esi
	movl	%esi, 136(%rax)
## BB#2:
	leaq	-432(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-472(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -216(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	%rcx, -200(%rbp)
	movq	-192(%rbp), %rdx
	leaq	-200(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-200(%rbp), %rsi
	movq	%rdx, -168(%rbp)
	movq	%rsi, -176(%rbp)
	movq	-168(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rdx
	leaq	-160(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-160(%rbp), %rsi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	-128(%rbp), %rdx
	leaq	-136(%rbp), %rsi
	movq	%rsi, -120(%rbp)
	movq	-136(%rbp), %rsi
	movq	%rsi, (%rdx)
	movq	%rax, -112(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rax, -96(%rbp)
	movq	-432(%rbp), %rsi
Ltmp104:
	movq	-456(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE
Ltmp105:
	jmp	LBB12_3
LBB12_3:
	leaq	-432(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	$0, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -72(%rbp)
	movq	%rax, -496(%rbp)        ## 8-byte Spill
	je	LBB12_7
## BB#4:
	movq	-496(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -16(%rbp)
	movq	%rax, -8(%rbp)
	movq	-72(%rbp), %rsi
Ltmp110:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp111:
	jmp	LBB12_5
LBB12_5:
	jmp	LBB12_7
LBB12_6:
Ltmp112:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -500(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB12_7:                                ## %_ZNSt3__110unique_ptrINS_22__deferred_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEENS_22__release_shared_countEED1Ev.exit2
	movq	-464(%rbp), %rax        ## 8-byte Reload
	addq	$528, %rsp              ## imm = 0x210
	popq	%rbp
	retq
LBB12_8:
Ltmp103:
	movl	%edx, %ecx
	movq	%rax, -440(%rbp)
	movl	%ecx, -444(%rbp)
	movq	-488(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdlPv
	jmp	LBB12_14
LBB12_9:
Ltmp106:
	leaq	-432(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -440(%rbp)
	movl	%esi, -444(%rbp)
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rax
	movq	%rax, -400(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -376(%rbp)
	movq	$0, -384(%rbp)
	movq	-376(%rbp), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -392(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rdx
	movq	%rdx, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -392(%rbp)
	movq	%rax, -512(%rbp)        ## 8-byte Spill
	je	LBB12_13
## BB#10:
	movq	-512(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -336(%rbp)
	movq	%rax, -328(%rbp)
	movq	-392(%rbp), %rsi
Ltmp107:
	movq	%rax, %rdi
	callq	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
Ltmp108:
	jmp	LBB12_11
LBB12_11:
	jmp	LBB12_13
LBB12_12:
Ltmp109:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -516(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
LBB12_13:                               ## %_ZNSt3__110unique_ptrINS_22__deferred_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEENS_22__release_shared_countEED1Ev.exit
	jmp	LBB12_14
LBB12_14:
	movq	-440(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table12:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset82 = Lfunc_begin6-Lfunc_begin6      ## >> Call Site 1 <<
	.long	Lset82
Lset83 = Ltmp101-Lfunc_begin6           ##   Call between Lfunc_begin6 and Ltmp101
	.long	Lset83
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset84 = Ltmp101-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset84
Lset85 = Ltmp102-Ltmp101                ##   Call between Ltmp101 and Ltmp102
	.long	Lset85
Lset86 = Ltmp103-Lfunc_begin6           ##     jumps to Ltmp103
	.long	Lset86
	.byte	0                       ##   On action: cleanup
Lset87 = Ltmp104-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset87
Lset88 = Ltmp105-Ltmp104                ##   Call between Ltmp104 and Ltmp105
	.long	Lset88
Lset89 = Ltmp106-Lfunc_begin6           ##     jumps to Ltmp106
	.long	Lset89
	.byte	0                       ##   On action: cleanup
Lset90 = Ltmp110-Lfunc_begin6           ## >> Call Site 4 <<
	.long	Lset90
Lset91 = Ltmp111-Ltmp110                ##   Call between Ltmp110 and Ltmp111
	.long	Lset91
Lset92 = Ltmp112-Lfunc_begin6           ##     jumps to Ltmp112
	.long	Lset92
	.byte	1                       ##   On action: 1
Lset93 = Ltmp107-Lfunc_begin6           ## >> Call Site 5 <<
	.long	Lset93
Lset94 = Ltmp108-Ltmp107                ##   Call between Ltmp107 and Ltmp108
	.long	Lset94
Lset95 = Ltmp109-Lfunc_begin6           ##     jumps to Ltmp109
	.long	Lset95
	.byte	1                       ##   On action: 1
Lset96 = Ltmp108-Lfunc_begin6           ## >> Call Site 6 <<
	.long	Lset96
Lset97 = Lfunc_end6-Ltmp108             ##   Call between Ltmp108 and Lfunc_end6
	.long	Lset97
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16threadC1IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	.weak_def_can_be_hidden	__ZNSt3__16threadC1IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	.align	4, 0x90
__ZNSt3__16threadC1IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_: ## @_ZNSt3__16threadC1IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp116:
	.cfi_def_cfa_offset 16
Ltmp117:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp118:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	callq	__ZNSt3__16threadC2IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE
	.weak_def_can_be_hidden	__ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE
	.align	4, 0x90
__ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE: ## @_ZNSt3__16futureIPKcEC1EPNS_13__assoc_stateIS2_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp119:
	.cfi_def_cfa_offset 16
Ltmp120:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp121:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16futureIPKcEC2EPNS_13__assoc_stateIS2_EE
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcEC2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcEC2Ev
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcEC2Ev:     ## @_ZNSt3__113__assoc_stateIPKcEC2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp122:
	.cfi_def_cfa_offset 16
Ltmp123:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp124:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$152, %rsp
Ltmp125:
	.cfi_offset %rbx, -24
	movq	__ZTVNSt3__113__assoc_stateIPKcEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	xorl	%ecx, %ecx
	movl	$40, %edx
                                        ## 
	movl	$56, %esi
	movl	%esi, %r8d
	movq	__ZTVNSt3__117__assoc_sub_stateE@GOTPCREL(%rip), %r9
	addq	$16, %r9
	movq	__ZTVNSt3__114__shared_countE@GOTPCREL(%rip), %r10
	addq	$16, %r10
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, %r11
	movq	%r11, -80(%rbp)
	movq	-80(%rbp), %r11
	movq	%r11, %rbx
	movq	%rbx, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	-64(%rbp), %rbx
	movq	%r10, (%rbx)
	movq	-72(%rbp), %r10
	movq	%r10, 8(%rbx)
	movq	%r9, (%r11)
	movq	%r11, %r9
	addq	$16, %r9
	movq	%r9, -24(%rbp)
	movq	-24(%rbp), %r9
	movq	%r9, -16(%rbp)
	movq	-16(%rbp), %r9
	movq	$0, (%r9)
	movq	%r11, %r9
	addq	$24, %r9
	movq	%r9, -40(%rbp)
	movq	-40(%rbp), %r9
	movq	%r9, -32(%rbp)
	movq	-32(%rbp), %r9
	movq	$850045863, (%r9)       ## imm = 0x32AAABA7
	movq	%r9, %r10
	addq	$8, %r10
	movq	%rdi, -112(%rbp)        ## 8-byte Spill
	movq	%r10, %rdi
	movl	%ecx, %esi
	movq	%rdx, -120(%rbp)        ## 8-byte Spill
	movq	%r8, %rdx
	movq	%r11, -128(%rbp)        ## 8-byte Spill
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movl	%ecx, -140(%rbp)        ## 4-byte Spill
	movq	%r9, -152(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-152(%rbp), %rax        ## 8-byte Reload
	movb	$0, 8(%rax)
	movq	-128(%rbp), %rdx        ## 8-byte Reload
	addq	$88, %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	$1018212795, (%rdx)     ## imm = 0x3CB0B1BB
	movq	%rdx, %rdi
	addq	$8, %rdi
	movl	-140(%rbp), %esi        ## 4-byte Reload
	movq	-120(%rbp), %r8         ## 8-byte Reload
	movq	%rdx, -160(%rbp)        ## 8-byte Spill
	movq	%r8, %rdx
	callq	_memset
	movq	-160(%rbp), %rax        ## 8-byte Reload
	movb	$0, 8(%rax)
	movq	-128(%rbp), %rdx        ## 8-byte Reload
	movl	$0, 136(%rdx)
	movq	-112(%rbp), %rdi        ## 8-byte Reload
	movq	-136(%rbp), %r8         ## 8-byte Reload
	movq	%r8, (%rdi)
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcED2Ev
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcED2Ev
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcED2Ev:     ## @_ZNSt3__113__assoc_stateIPKcED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp126:
	.cfi_def_cfa_offset 16
Ltmp127:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp128:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__117__assoc_sub_stateD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.align	4, 0x90
__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev: ## @_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp129:
	.cfi_def_cfa_offset 16
Ltmp130:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp131:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.align	4, 0x90
__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev: ## @_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp132:
	.cfi_def_cfa_offset 16
Ltmp133:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp134:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE16__on_zero_sharedEv
	.weak_def_can_be_hidden	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE16__on_zero_sharedEv
	.align	4, 0x90
__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE16__on_zero_sharedEv: ## @_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE16__on_zero_sharedEv
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp138:
	.cfi_def_cfa_offset 16
Ltmp139:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp140:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rdi, %rax
Ltmp135:
	movq	%rax, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__117__assoc_sub_state4waitEv
Ltmp136:
	jmp	LBB19_1
LBB19_1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	addq	$32, %rsp
	popq	%rbp
	retq
LBB19_2:
Ltmp137:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -20(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table19:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset98 = Ltmp135-Lfunc_begin7           ## >> Call Site 1 <<
	.long	Lset98
Lset99 = Ltmp136-Ltmp135                ##   Call between Ltmp135 and Ltmp136
	.long	Lset99
Lset100 = Ltmp137-Lfunc_begin7          ##     jumps to Ltmp137
	.long	Lset100
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
	.weak_def_can_be_hidden	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
	.align	4, 0x90
__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv: ## @_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp152:
	.cfi_def_cfa_offset 16
Ltmp153:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp154:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rdi, %rax
	movq	%rdi, %rcx
	addq	$152, %rdi
Ltmp141:
	movq	%rcx, -48(%rbp)         ## 8-byte Spill
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv
Ltmp142:
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	jmp	LBB20_1
LBB20_1:
	movq	-64(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -16(%rbp)
Ltmp143:
	leaq	-16(%rbp), %rsi
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_
Ltmp144:
	jmp	LBB20_2
LBB20_2:
	jmp	LBB20_6
LBB20_3:
Ltmp145:
	movl	%edx, %ecx
	movq	%rax, -24(%rbp)
	movl	%ecx, -28(%rbp)
## BB#4:
	movq	-24(%rbp), %rdi
	callq	___cxa_begin_catch
	leaq	-40(%rbp), %rdi
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__ZSt17current_exceptionv
Ltmp146:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	movq	-72(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__117__assoc_sub_state13set_exceptionESt13exception_ptr
Ltmp147:
	jmp	LBB20_5
LBB20_5:
	leaq	-40(%rbp), %rdi
	callq	__ZNSt13exception_ptrD1Ev
	callq	___cxa_end_catch
LBB20_6:
	addq	$96, %rsp
	popq	%rbp
	retq
LBB20_7:
Ltmp148:
	movl	%edx, %ecx
	movq	%rax, -24(%rbp)
	movl	%ecx, -28(%rbp)
	leaq	-40(%rbp), %rdi
	callq	__ZNSt13exception_ptrD1Ev
Ltmp149:
	callq	___cxa_end_catch
Ltmp150:
	jmp	LBB20_8
LBB20_8:
	jmp	LBB20_9
LBB20_9:
	movq	-24(%rbp), %rdi
	callq	__Unwind_Resume
LBB20_10:
Ltmp151:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -84(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table20:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset101 = Ltmp141-Lfunc_begin8          ## >> Call Site 1 <<
	.long	Lset101
Lset102 = Ltmp144-Ltmp141               ##   Call between Ltmp141 and Ltmp144
	.long	Lset102
Lset103 = Ltmp145-Lfunc_begin8          ##     jumps to Ltmp145
	.long	Lset103
	.byte	1                       ##   On action: 1
Lset104 = Ltmp144-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset104
Lset105 = Ltmp146-Ltmp144               ##   Call between Ltmp144 and Ltmp146
	.long	Lset105
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset106 = Ltmp146-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset106
Lset107 = Ltmp147-Ltmp146               ##   Call between Ltmp146 and Ltmp147
	.long	Lset107
Lset108 = Ltmp148-Lfunc_begin8          ##     jumps to Ltmp148
	.long	Lset108
	.byte	0                       ##   On action: cleanup
Lset109 = Ltmp147-Lfunc_begin8          ## >> Call Site 4 <<
	.long	Lset109
Lset110 = Ltmp149-Ltmp147               ##   Call between Ltmp147 and Ltmp149
	.long	Lset110
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset111 = Ltmp149-Lfunc_begin8          ## >> Call Site 5 <<
	.long	Lset111
Lset112 = Ltmp150-Ltmp149               ##   Call between Ltmp149 and Ltmp150
	.long	Lset112
Lset113 = Ltmp151-Lfunc_begin8          ##     jumps to Ltmp151
	.long	Lset113
	.byte	1                       ##   On action: 1
Lset114 = Ltmp150-Lfunc_begin8          ## >> Call Site 6 <<
	.long	Lset114
Lset115 = Lfunc_end8-Ltmp150            ##   Call between Ltmp150 and Lfunc_end8
	.long	Lset115
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__113__assoc_stateIPKcED1Ev
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcED1Ev
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcED1Ev:     ## @_ZNSt3__113__assoc_stateIPKcED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp155:
	.cfi_def_cfa_offset 16
Ltmp156:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp157:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__assoc_stateIPKcED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcED0Ev
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcED0Ev
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcED0Ev:     ## @_ZNSt3__113__assoc_stateIPKcED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp158:
	.cfi_def_cfa_offset 16
Ltmp159:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp160:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__113__assoc_stateIPKcED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv: ## @_ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp161:
	.cfi_def_cfa_offset 16
Ltmp162:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp163:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movl	136(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	je	LBB23_2
## BB#1:
LBB23_2:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	cmpq	$0, %rax
	je	LBB23_4
## BB#3:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*8(%rcx)
LBB23_4:
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__117__assoc_sub_stateD2Ev
	.weak_def_can_be_hidden	__ZNSt3__117__assoc_sub_stateD2Ev
	.align	4, 0x90
__ZNSt3__117__assoc_sub_stateD2Ev:      ## @_ZNSt3__117__assoc_sub_stateD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp164:
	.cfi_def_cfa_offset 16
Ltmp165:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp166:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__117__assoc_sub_stateE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$88, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__118condition_variableD1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	addq	$24, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__15mutexD1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	addq	$16, %rax
	movq	%rax, %rdi
	callq	__ZNSt13exception_ptrD1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__114__shared_countD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.align	4, 0x90
__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev: ## @_ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp167:
	.cfi_def_cfa_offset 16
Ltmp168:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp169:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__assoc_stateIPKcED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_: ## @_ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp176:
	.cfi_def_cfa_offset 16
Ltmp177:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp178:
	.cfi_def_cfa_register %rbp
	subq	$368, %rsp              ## imm = 0x170
	leaq	-272(%rbp), %rax
	movq	%rdi, -248(%rbp)
	movq	%rsi, -256(%rbp)
	movq	-248(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$24, %rdi
	movq	%rax, -232(%rbp)
	movq	%rdi, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdi
	movq	%rax, -216(%rbp)
	movq	%rdi, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdi
	movq	%rdi, (%rax)
	movb	$1, 8(%rax)
	movq	(%rax), %rdi
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__15mutex4lockEv
	movb	$1, %cl
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movl	136(%rax), %edx
	andl	$1, %edx
	movb	$0, -201(%rbp)
	cmpl	$0, %edx
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	movb	%cl, -321(%rbp)         ## 1-byte Spill
	jne	LBB26_2
## BB#1:
	leaq	-200(%rbp), %rax
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	addq	$16, %rcx
	movq	%rax, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rsi
	movq	%rdx, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rdx
	movq	$0, (%rdx)
	movb	$1, -201(%rbp)
	movq	%rcx, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	-128(%rbp), %rcx
	cmpq	(%rcx), %rax
	sete	%dil
	xorb	$-1, %dil
	movb	%dil, -321(%rbp)        ## 1-byte Spill
LBB26_2:
	movb	-321(%rbp), %al         ## 1-byte Reload
	andb	$1, %al
	movb	%al, -177(%rbp)
	testb	$1, -201(%rbp)
	jne	LBB26_3
	jmp	LBB26_4
LBB26_3:
	leaq	-200(%rbp), %rdi
	callq	__ZNSt13exception_ptrD1Ev
LBB26_4:                                ## %_ZNKSt3__117__assoc_sub_state11__has_valueEv.exit
	movb	-177(%rbp), %al
	movb	%al, -322(%rbp)         ## 1-byte Spill
## BB#5:
	movb	-322(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB26_6
	jmp	LBB26_10
LBB26_6:
	movl	$32, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movl	$2, -108(%rbp)
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	movq	%rdi, -344(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115future_categoryEv
	leaq	-104(%rbp), %rdi
	movq	%rdi, -72(%rbp)
	movl	$2, -76(%rbp)
	movq	%rax, -88(%rbp)
	movq	-72(%rbp), %rdi
	movl	-76(%rbp), %ecx
	movq	%rdi, -48(%rbp)
	movl	%ecx, -52(%rbp)
	movq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	movl	-52(%rbp), %ecx
	movl	%ecx, (%rax)
	movq	-64(%rbp), %rdi
	movq	%rdi, 8(%rax)
	movq	-96(%rbp), %rax
	movl	-104(%rbp), %ecx
	movl	%ecx, -304(%rbp)
	movq	%rax, -296(%rbp)
	movl	-304(%rbp), %esi
Ltmp170:
	movq	-336(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112future_errorC1ENS_10error_codeE
Ltmp171:
	jmp	LBB26_7
LBB26_7:
Ltmp173:
	movq	__ZTINSt3__112future_errorE@GOTPCREL(%rip), %rsi
	movq	__ZNSt3__112future_errorD1Ev@GOTPCREL(%rip), %rdx
	movq	-344(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_throw
Ltmp174:
	jmp	LBB26_17
LBB26_8:
Ltmp175:
	movl	%edx, %ecx
	movq	%rax, -280(%rbp)
	movl	%ecx, -284(%rbp)
	jmp	LBB26_13
LBB26_9:
Ltmp172:
	movl	%edx, %ecx
	movq	%rax, -280(%rbp)
	movl	%ecx, -284(%rbp)
	movq	-344(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
	jmp	LBB26_13
LBB26_10:
	movq	-256(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rax
	movq	(%rax), %rax
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 144(%rcx)
	movl	136(%rcx), %edx
	orl	$5, %edx
	movl	%edx, 136(%rcx)
	addq	$88, %rcx
	movq	%rcx, %rdi
	callq	__ZNSt3__118condition_variable10notify_allEv
	leaq	-272(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	testb	$1, 8(%rax)
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	je	LBB26_12
## BB#11:
	movq	-352(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	callq	__ZNSt3__15mutex6unlockEv
LBB26_12:                               ## %_ZNSt3__111unique_lockINS_5mutexEED1Ev.exit1
	addq	$368, %rsp              ## imm = 0x170
	popq	%rbp
	retq
LBB26_13:
	leaq	-272(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	testb	$1, 8(%rax)
	movq	%rax, -360(%rbp)        ## 8-byte Spill
	je	LBB26_15
## BB#14:
	movq	-360(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	callq	__ZNSt3__15mutex6unlockEv
LBB26_15:                               ## %_ZNSt3__111unique_lockINS_5mutexEED1Ev.exit
	jmp	LBB26_16
LBB26_16:
	movq	-280(%rbp), %rdi
	callq	__Unwind_Resume
LBB26_17:
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table26:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset116 = Lfunc_begin9-Lfunc_begin9     ## >> Call Site 1 <<
	.long	Lset116
Lset117 = Ltmp170-Lfunc_begin9          ##   Call between Lfunc_begin9 and Ltmp170
	.long	Lset117
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset118 = Ltmp170-Lfunc_begin9          ## >> Call Site 2 <<
	.long	Lset118
Lset119 = Ltmp171-Ltmp170               ##   Call between Ltmp170 and Ltmp171
	.long	Lset119
Lset120 = Ltmp172-Lfunc_begin9          ##     jumps to Ltmp172
	.long	Lset120
	.byte	0                       ##   On action: cleanup
Lset121 = Ltmp173-Lfunc_begin9          ## >> Call Site 3 <<
	.long	Lset121
Lset122 = Ltmp174-Ltmp173               ##   Call between Ltmp173 and Ltmp174
	.long	Lset122
Lset123 = Ltmp175-Lfunc_begin9          ##     jumps to Ltmp175
	.long	Lset123
	.byte	0                       ##   On action: cleanup
Lset124 = Ltmp174-Lfunc_begin9          ## >> Call Site 4 <<
	.long	Lset124
Lset125 = Lfunc_end9-Ltmp174            ##   Call between Ltmp174 and Lfunc_end9
	.long	Lset125
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv
	.weak_def_can_be_hidden	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv
	.align	4, 0x90
__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv: ## @_ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp179:
	.cfi_def_cfa_offset 16
Ltmp180:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp181:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EE9__executeIJLm1EEEES2_NS_15__tuple_indicesIJXspT_EEEE
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EE9__executeIJLm1EEEES2_NS_15__tuple_indicesIJXspT_EEEE
	.weak_def_can_be_hidden	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EE9__executeIJLm1EEEES2_NS_15__tuple_indicesIJXspT_EEEE
	.align	4, 0x90
__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EE9__executeIJLm1EEEES2_NS_15__tuple_indicesIJXspT_EEEE: ## @_ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EE9__executeIJLm1EEEES2_NS_15__tuple_indicesIJXspT_EEEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp182:
	.cfi_def_cfa_offset 16
Ltmp183:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp184:
	.cfi_def_cfa_register %rbp
	subq	$112, %rsp
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	addq	$8, %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	%rax, -56(%rbp)
	movq	%rdi, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	(%rax), %rax
	movq	-64(%rbp), %rdi
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdi
	movl	(%rdi), %ecx
	movl	%ecx, -72(%rbp)
	movl	-72(%rbp), %edi
	callq	*%rax
	addq	$112, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16threadC2IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	.weak_def_can_be_hidden	__ZNSt3__16threadC2IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
	.align	4, 0x90
__ZNSt3__16threadC2IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_: ## @_ZNSt3__16threadC2IMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEJPSA_EvEEOT_DpOT0_
Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception10
## BB#0:
	pushq	%rbp
Ltmp190:
	.cfi_def_cfa_offset 16
Ltmp191:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp192:
	.cfi_def_cfa_register %rbp
	subq	$864, %rsp              ## imm = 0x360
	movq	%rdi, -712(%rbp)
	movq	%rsi, -720(%rbp)
	movq	%rdx, -728(%rbp)
	movq	-712(%rbp), %rdi
	movl	$24, %eax
	movl	%eax, %edx
	movq	%rdi, -792(%rbp)        ## 8-byte Spill
	movq	%rdx, %rdi
	callq	__Znwm
	movq	-720(%rbp), %rdx
	movq	%rdx, -704(%rbp)
	movq	%rdx, -592(%rbp)
	movq	%rdx, -584(%rbp)
	movq	(%rdx), %rsi
	movq	8(%rdx), %rdx
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rsi, -808(%rbp)        ## 8-byte Spill
	movq	%rdx, -816(%rbp)        ## 8-byte Spill
## BB#1:
	movq	-808(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -752(%rbp)
	movq	-816(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -744(%rbp)
	movq	-728(%rbp), %rdx
	movq	%rdx, -472(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -824(%rbp)        ## 8-byte Spill
## BB#2:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -776(%rbp)
	movq	-800(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -272(%rbp)
	leaq	-752(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	leaq	-776(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-272(%rbp), %rsi
	movq	-280(%rbp), %rdi
	movq	%rsi, -216(%rbp)
	movq	%rdi, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%rsi, -208(%rbp)
	movq	-232(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	%rdx, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdi, -200(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-128(%rbp), %rdx
	movq	-136(%rbp), %rsi
	movq	%rsi, -88(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rdi
	movq	%rsi, -32(%rbp)
	movq	(%rsi), %r8
	movq	8(%rsi), %rsi
	movq	%rsi, 8(%rdi)
	movq	%r8, (%rdi)
	addq	$16, %rdx
	movq	-144(%rbp), %rsi
	movq	%rsi, -56(%rbp)
	movq	%rdx, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rsi, -64(%rbp)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	leaq	-736(%rbp), %rdx
	movq	%rdx, -384(%rbp)
	movq	%rcx, -392(%rbp)
	movq	-384(%rbp), %rsi
	movq	%rsi, -368(%rbp)
	movq	%rcx, -376(%rbp)
	movq	-368(%rbp), %rsi
	leaq	-376(%rbp), %rdi
	movq	%rdi, -360(%rbp)
	movq	-376(%rbp), %rdi
	movq	%rsi, -344(%rbp)
	movq	%rdi, -352(%rbp)
	movq	-344(%rbp), %rsi
	movq	%rsi, -328(%rbp)
	movq	%rdi, -336(%rbp)
	movq	-328(%rbp), %rsi
	leaq	-336(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rsi, -304(%rbp)
	movq	%rdi, -312(%rbp)
	movq	-304(%rbp), %rsi
	leaq	-312(%rbp), %rdi
	movq	%rdi, -296(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rdi, (%rsi)
	movq	%rdx, -416(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-736(%rbp), %rcx
Ltmp185:
	movq	__ZNSt3__114__thread_proxyINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEEPvSF_@GOTPCREL(%rip), %rdx
	xorl	%r9d, %r9d
	movl	%r9d, %esi
	movq	-792(%rbp), %rdi        ## 8-byte Reload
	callq	_pthread_create
Ltmp186:
	movl	%eax, -828(%rbp)        ## 4-byte Spill
	jmp	LBB29_3
LBB29_3:
	movl	-828(%rbp), %eax        ## 4-byte Reload
	movl	%eax, -780(%rbp)
	cmpl	$0, -780(%rbp)
	jne	LBB29_10
## BB#4:
	leaq	-736(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	-456(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -464(%rbp)
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rax
	movq	$0, (%rax)
	jmp	LBB29_12
LBB29_5:
Ltmp189:
	leaq	-736(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -760(%rbp)
	movl	%esi, -764(%rbp)
	movq	%rcx, -576(%rbp)
	movq	-576(%rbp), %rax
	movq	%rax, -568(%rbp)
	movq	-568(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	$0, -552(%rbp)
	movq	-544(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	-536(%rbp), %rcx
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -560(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rdx
	movq	%rdx, -496(%rbp)
	movq	-496(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -560(%rbp)
	movq	%rax, -840(%rbp)        ## 8-byte Spill
	je	LBB29_9
## BB#6:
	movq	-840(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -488(%rbp)
	movq	-488(%rbp), %rcx
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	-560(%rbp), %rdx
	movq	%rcx, -512(%rbp)
	movq	%rdx, -520(%rbp)
	movq	-520(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -848(%rbp)        ## 8-byte Spill
	je	LBB29_8
## BB#7:
	movq	-848(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB29_8:                                ## %_ZNKSt3__114default_deleteINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEclEPSE_.exit.i.i.i.2
	jmp	LBB29_9
LBB29_9:                                ## %_ZNSt3__110unique_ptrINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEENS_14default_deleteISE_EEED1Ev.exit3
	jmp	LBB29_17
LBB29_10:
	movl	-780(%rbp), %edi
Ltmp187:
	leaq	L_.str.2(%rip), %rsi
	callq	__ZNSt3__120__throw_system_errorEiPKc
Ltmp188:
	jmp	LBB29_11
LBB29_11:
	jmp	LBB29_12
LBB29_12:
	leaq	-736(%rbp), %rax
	movq	%rax, -696(%rbp)
	movq	-696(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	-688(%rbp), %rax
	movq	%rax, -664(%rbp)
	movq	$0, -672(%rbp)
	movq	-664(%rbp), %rax
	movq	%rax, -656(%rbp)
	movq	-656(%rbp), %rcx
	movq	%rcx, -648(%rbp)
	movq	-648(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -680(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rax, -624(%rbp)
	movq	-624(%rbp), %rdx
	movq	%rdx, -616(%rbp)
	movq	-616(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -680(%rbp)
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	je	LBB29_16
## BB#13:
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rcx
	movq	%rcx, -600(%rbp)
	movq	-600(%rbp), %rcx
	movq	-680(%rbp), %rdx
	movq	%rcx, -632(%rbp)
	movq	%rdx, -640(%rbp)
	movq	-640(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -864(%rbp)        ## 8-byte Spill
	je	LBB29_15
## BB#14:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB29_15:                               ## %_ZNKSt3__114default_deleteINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEclEPSE_.exit.i.i.i
	jmp	LBB29_16
LBB29_16:                               ## %_ZNSt3__110unique_ptrINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEENS_14default_deleteISE_EEED1Ev.exit
	addq	$864, %rsp              ## imm = 0x360
	popq	%rbp
	retq
LBB29_17:
	movq	-760(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end10:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table29:
Lexception10:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset126 = Lfunc_begin10-Lfunc_begin10   ## >> Call Site 1 <<
	.long	Lset126
Lset127 = Ltmp185-Lfunc_begin10         ##   Call between Lfunc_begin10 and Ltmp185
	.long	Lset127
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset128 = Ltmp185-Lfunc_begin10         ## >> Call Site 2 <<
	.long	Lset128
Lset129 = Ltmp188-Ltmp185               ##   Call between Ltmp185 and Ltmp188
	.long	Lset129
Lset130 = Ltmp189-Lfunc_begin10         ##     jumps to Ltmp189
	.long	Lset130
	.byte	0                       ##   On action: cleanup
Lset131 = Ltmp188-Lfunc_begin10         ## >> Call Site 3 <<
	.long	Lset131
Lset132 = Lfunc_end10-Ltmp188           ##   Call between Ltmp188 and Lfunc_end10
	.long	Lset132
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__114__thread_proxyINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEEPvSF_
	.weak_definition	__ZNSt3__114__thread_proxyINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEEPvSF_
	.align	4, 0x90
__ZNSt3__114__thread_proxyINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEEPvSF_: ## @_ZNSt3__114__thread_proxyINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEEPvSF_
Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception11
## BB#0:
	pushq	%rbp
Ltmp199:
	.cfi_def_cfa_offset 16
Ltmp200:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp201:
	.cfi_def_cfa_register %rbp
	subq	$560, %rsp              ## imm = 0x230
	movq	%rdi, -432(%rbp)
	callq	__ZNSt3__119__thread_local_dataEv
	movl	$8, %ecx
	movl	%ecx, %edi
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	callq	__Znwm
	movq	%rax, %rdi
	movq	%rax, %rdx
Ltmp193:
	movq	%rdi, -480(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rdx, -488(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115__thread_structC1Ev
Ltmp194:
	jmp	LBB30_1
LBB30_1:
	movq	-472(%rbp), %rdi        ## 8-byte Reload
	movq	-488(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__121__thread_specific_ptrINS_15__thread_structEE5resetEPS1_
	leaq	-456(%rbp), %rsi
	leaq	-344(%rbp), %rdi
	leaq	-368(%rbp), %rax
	leaq	-408(%rbp), %rcx
	movq	-432(%rbp), %rdx
	movq	%rsi, -416(%rbp)
	movq	%rdx, -424(%rbp)
	movq	-416(%rbp), %rdx
	movq	-424(%rbp), %r8
	movq	%rdx, -400(%rbp)
	movq	%r8, -408(%rbp)
	movq	-400(%rbp), %rdx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rdx, -376(%rbp)
	movq	%rcx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rcx, -360(%rbp)
	movq	%rdx, -368(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	%rcx, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	%rsi, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -496(%rbp)        ## 8-byte Spill
## BB#2:
	movq	-496(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	%rax, -72(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rax, -32(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	addq	$16, %rcx
	movq	%rcx, -8(%rbp)
	movq	%rcx, -24(%rbp)
	movq	%rax, -48(%rbp)
	movq	%rcx, -56(%rbp)
	movq	-48(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	8(%rcx), %rcx
	movq	-56(%rbp), %rsi
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rsi
	movq	(%rsi), %rsi
	addq	%rcx, %rsi
	movq	%rdx, %rcx
	andq	$1, %rcx
	cmpq	$0, %rcx
	movq	%rdx, -504(%rbp)        ## 8-byte Spill
	movq	%rsi, -512(%rbp)        ## 8-byte Spill
	je	LBB30_4
## BB#3:
	movq	-512(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	-504(%rbp), %rdx        ## 8-byte Reload
	subq	$1, %rdx
	movq	(%rcx,%rdx), %rcx
	movq	%rcx, -520(%rbp)        ## 8-byte Spill
	jmp	LBB30_5
LBB30_4:
	movq	-504(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -520(%rbp)        ## 8-byte Spill
LBB30_5:                                ## %_ZNSt3__18__invokeIMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEEFvvEPS9_JEvEEDTcldsdeclsr3std3__1E7forwardIT0_Efp0_Efp_spclsr3std3__1E7forwardIT1_Efp1_EEEOT_OSD_DpOSE_.exit.i
Ltmp196:
	movq	-520(%rbp), %rax        ## 8-byte Reload
	movq	-512(%rbp), %rdi        ## 8-byte Reload
	callq	*%rax
Ltmp197:
	jmp	LBB30_6
LBB30_6:                                ## %_ZNSt3__116__thread_executeIMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS3_16ForThread_structEJS5_EEEEEFvvEJPS9_EJLm1EEEEvRNS_5tupleIJT_DpT0_EEENS_15__tuple_indicesIJXspT1_EEEE.exit
	jmp	LBB30_7
LBB30_7:
	leaq	-456(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	$0, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -176(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -176(%rbp)
	movq	%rax, -528(%rbp)        ## 8-byte Spill
	je	LBB30_11
## BB#8:
	movq	-528(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	-176(%rbp), %rdx
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -536(%rbp)        ## 8-byte Spill
	je	LBB30_10
## BB#9:
	movq	-536(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB30_10:                               ## %_ZNKSt3__114default_deleteINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEclEPSE_.exit.i.i.i.2
	jmp	LBB30_11
LBB30_11:                               ## %_ZNSt3__110unique_ptrINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEENS_14default_deleteISE_EEED1Ev.exit3
	xorl	%eax, %eax
                                        ## 
	addq	$560, %rsp              ## imm = 0x230
	popq	%rbp
	retq
LBB30_12:
Ltmp195:
	movl	%edx, %ecx
	movq	%rax, -440(%rbp)
	movl	%ecx, -444(%rbp)
	movq	-480(%rbp), %rdi        ## 8-byte Reload
	callq	__ZdlPv
	jmp	LBB30_18
LBB30_13:
Ltmp198:
	leaq	-456(%rbp), %rcx
	movl	%edx, %esi
	movq	%rax, -440(%rbp)
	movl	%esi, -444(%rbp)
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -288(%rbp)
	movq	$0, -296(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -304(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rax, -248(%rbp)
	movq	-248(%rbp), %rdx
	movq	%rdx, -240(%rbp)
	movq	-240(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -304(%rbp)
	movq	%rax, -544(%rbp)        ## 8-byte Spill
	je	LBB30_17
## BB#14:
	movq	-544(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	-304(%rbp), %rdx
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-264(%rbp), %rcx
	cmpq	$0, %rcx
	movq	%rcx, -552(%rbp)        ## 8-byte Spill
	je	LBB30_16
## BB#15:
	movq	-552(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZdlPv
LBB30_16:                               ## %_ZNKSt3__114default_deleteINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEEEclEPSE_.exit.i.i.i
	jmp	LBB30_17
LBB30_17:                               ## %_ZNSt3__110unique_ptrINS_5tupleIJMNS_19__async_assoc_stateIPKcNS_12__async_funcIPFS4_16ForThread_structEJS6_EEEEEFvvEPSA_EEENS_14default_deleteISE_EEED1Ev.exit
	jmp	LBB30_18
LBB30_18:
	movq	-440(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end11:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table30:
Lexception11:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\303\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset133 = Lfunc_begin11-Lfunc_begin11   ## >> Call Site 1 <<
	.long	Lset133
Lset134 = Ltmp193-Lfunc_begin11         ##   Call between Lfunc_begin11 and Ltmp193
	.long	Lset134
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset135 = Ltmp193-Lfunc_begin11         ## >> Call Site 2 <<
	.long	Lset135
Lset136 = Ltmp194-Ltmp193               ##   Call between Ltmp193 and Ltmp194
	.long	Lset136
Lset137 = Ltmp195-Lfunc_begin11         ##     jumps to Ltmp195
	.long	Lset137
	.byte	0                       ##   On action: cleanup
Lset138 = Ltmp194-Lfunc_begin11         ## >> Call Site 3 <<
	.long	Lset138
Lset139 = Ltmp196-Ltmp194               ##   Call between Ltmp194 and Ltmp196
	.long	Lset139
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset140 = Ltmp196-Lfunc_begin11         ## >> Call Site 4 <<
	.long	Lset140
Lset141 = Ltmp197-Ltmp196               ##   Call between Ltmp196 and Ltmp197
	.long	Lset141
Lset142 = Ltmp198-Lfunc_begin11         ##     jumps to Ltmp198
	.long	Lset142
	.byte	0                       ##   On action: cleanup
Lset143 = Ltmp197-Lfunc_begin11         ## >> Call Site 5 <<
	.long	Lset143
Lset144 = Lfunc_end11-Ltmp197           ##   Call between Ltmp197 and Lfunc_end11
	.long	Lset144
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__121__thread_specific_ptrINS_15__thread_structEE5resetEPS1_
	.weak_def_can_be_hidden	__ZNSt3__121__thread_specific_ptrINS_15__thread_structEE5resetEPS1_
	.align	4, 0x90
__ZNSt3__121__thread_specific_ptrINS_15__thread_structEE5resetEPS1_: ## @_ZNSt3__121__thread_specific_ptrINS_15__thread_structEE5resetEPS1_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp202:
	.cfi_def_cfa_offset 16
Ltmp203:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp204:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rsi, -40(%rbp)         ## 8-byte Spill
	callq	_pthread_getspecific
	movq	%rax, -32(%rbp)
	movq	-40(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rdi
	movq	-24(%rbp), %rsi
	callq	_pthread_setspecific
	movq	-32(%rbp), %rsi
	cmpq	$0, %rsi
	movl	%eax, -44(%rbp)         ## 4-byte Spill
	movq	%rsi, -56(%rbp)         ## 8-byte Spill
	je	LBB31_2
## BB#1:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__115__thread_structD1Ev
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
LBB31_2:
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16futureIPKcEC2EPNS_13__assoc_stateIS2_EE
	.weak_def_can_be_hidden	__ZNSt3__16futureIPKcEC2EPNS_13__assoc_stateIS2_EE
	.align	4, 0x90
__ZNSt3__16futureIPKcEC2EPNS_13__assoc_stateIS2_EE: ## @_ZNSt3__16futureIPKcEC2EPNS_13__assoc_stateIS2_EE
Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception12
## BB#0:
	pushq	%rbp
Ltmp208:
	.cfi_def_cfa_offset 16
Ltmp209:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp210:
	.cfi_def_cfa_register %rbp
	subq	$224, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %rdi
	movq	%rdi, (%rsi)
	movq	(%rsi), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movl	136(%rdi), %eax
	andl	$2, %eax
	cmpl	$0, %eax
	movq	%rsi, -200(%rbp)        ## 8-byte Spill
	je	LBB32_4
## BB#1:
	movl	$32, %eax
	movl	%eax, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rdi
	movl	$1, -68(%rbp)
	movq	%rax, -208(%rbp)        ## 8-byte Spill
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115future_categoryEv
	leaq	-64(%rbp), %rdi
	movq	%rdi, -32(%rbp)
	movl	$1, -36(%rbp)
	movq	%rax, -48(%rbp)
	movq	-32(%rbp), %rdi
	movl	-36(%rbp), %ecx
	movq	%rdi, -8(%rbp)
	movl	%ecx, -12(%rbp)
	movq	%rax, -24(%rbp)
	movq	-8(%rbp), %rax
	movl	-12(%rbp), %ecx
	movl	%ecx, (%rax)
	movq	-24(%rbp), %rdi
	movq	%rdi, 8(%rax)
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %ecx
	movl	%ecx, -176(%rbp)
	movq	%rax, -168(%rbp)
	movl	-176(%rbp), %esi
Ltmp205:
	movq	-208(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112future_errorC1ENS_10error_codeE
Ltmp206:
	jmp	LBB32_2
LBB32_2:
	movq	__ZTINSt3__112future_errorE@GOTPCREL(%rip), %rax
	movq	__ZNSt3__112future_errorD1Ev@GOTPCREL(%rip), %rcx
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, %rsi
	movq	%rcx, %rdx
	callq	___cxa_throw
LBB32_3:
Ltmp207:
	movl	%edx, %ecx
	movq	%rax, -184(%rbp)
	movl	%ecx, -188(%rbp)
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	callq	___cxa_free_exception
	jmp	LBB32_5
LBB32_4:
	movq	-200(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, %rdi
	callq	__ZNSt3__114__shared_count12__add_sharedEv
	leaq	-136(%rbp), %rax
	movq	-200(%rbp), %rcx        ## 8-byte Reload
	movq	(%rcx), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, %rdx
	addq	$24, %rdx
	movq	%rax, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-96(%rbp), %rax
	movq	-104(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	(%rax), %rax
	movq	%rdi, -224(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__15mutex4lockEv
	leaq	-136(%rbp), %rax
	movq	-224(%rbp), %rcx        ## 8-byte Reload
	movl	136(%rcx), %esi
	orl	$2, %esi
	movl	%esi, 136(%rcx)
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	(%rax), %rdi
	callq	__ZNSt3__15mutex6unlockEv
	addq	$224, %rsp
	popq	%rbp
	retq
LBB32_5:
	movq	-184(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end12:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table32:
Lexception12:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset145 = Lfunc_begin12-Lfunc_begin12   ## >> Call Site 1 <<
	.long	Lset145
Lset146 = Ltmp205-Lfunc_begin12         ##   Call between Lfunc_begin12 and Ltmp205
	.long	Lset146
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset147 = Ltmp205-Lfunc_begin12         ## >> Call Site 2 <<
	.long	Lset147
Lset148 = Ltmp206-Ltmp205               ##   Call between Ltmp205 and Ltmp206
	.long	Lset148
Lset149 = Ltmp207-Lfunc_begin12         ##     jumps to Ltmp207
	.long	Lset149
	.byte	0                       ##   On action: cleanup
Lset150 = Ltmp206-Lfunc_begin12         ## >> Call Site 3 <<
	.long	Lset150
Lset151 = Lfunc_end12-Ltmp206           ##   Call between Ltmp206 and Lfunc_end12
	.long	Lset151
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
	.weak_def_can_be_hidden	__ZNSt3__122__release_shared_countclEPNS_14__shared_countE
	.align	4, 0x90
__ZNSt3__122__release_shared_countclEPNS_14__shared_countE: ## @_ZNSt3__122__release_shared_countclEPNS_14__shared_countE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp211:
	.cfi_def_cfa_offset 16
Ltmp212:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp213:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	__ZNSt3__114__shared_count16__release_sharedEv
	movb	%al, -17(%rbp)          ## 1-byte Spill
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.align	4, 0x90
__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev: ## @_ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp214:
	.cfi_def_cfa_offset 16
Ltmp215:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp216:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.align	4, 0x90
__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev: ## @_ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp217:
	.cfi_def_cfa_offset 16
Ltmp218:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp219:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
	.weak_def_can_be_hidden	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
	.align	4, 0x90
__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv: ## @_ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv
Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception13
## BB#0:
	pushq	%rbp
Ltmp231:
	.cfi_def_cfa_offset 16
Ltmp232:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp233:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rdi, %rax
	movq	%rdi, %rcx
	addq	$152, %rdi
Ltmp220:
	movq	%rcx, -48(%rbp)         ## 8-byte Spill
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__112__async_funcIPFPKc16ForThread_structEJS3_EEclEv
Ltmp221:
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	jmp	LBB36_1
LBB36_1:
	movq	-64(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -16(%rbp)
Ltmp222:
	leaq	-16(%rbp), %rsi
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	callq	__ZNSt3__113__assoc_stateIPKcE9set_valueIS2_EEvOT_
Ltmp223:
	jmp	LBB36_2
LBB36_2:
	jmp	LBB36_6
LBB36_3:
Ltmp224:
	movl	%edx, %ecx
	movq	%rax, -24(%rbp)
	movl	%ecx, -28(%rbp)
## BB#4:
	movq	-24(%rbp), %rdi
	callq	___cxa_begin_catch
	leaq	-40(%rbp), %rdi
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	callq	__ZSt17current_exceptionv
Ltmp225:
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	movq	-72(%rbp), %rsi         ## 8-byte Reload
	callq	__ZNSt3__117__assoc_sub_state13set_exceptionESt13exception_ptr
Ltmp226:
	jmp	LBB36_5
LBB36_5:
	leaq	-40(%rbp), %rdi
	callq	__ZNSt13exception_ptrD1Ev
	callq	___cxa_end_catch
LBB36_6:
	addq	$96, %rsp
	popq	%rbp
	retq
LBB36_7:
Ltmp227:
	movl	%edx, %ecx
	movq	%rax, -24(%rbp)
	movl	%ecx, -28(%rbp)
	leaq	-40(%rbp), %rdi
	callq	__ZNSt13exception_ptrD1Ev
Ltmp228:
	callq	___cxa_end_catch
Ltmp229:
	jmp	LBB36_8
LBB36_8:
	jmp	LBB36_9
LBB36_9:
	movq	-24(%rbp), %rdi
	callq	__Unwind_Resume
LBB36_10:
Ltmp230:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -84(%rbp)         ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end13:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table36:
Lexception13:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\326\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset152 = Ltmp220-Lfunc_begin13         ## >> Call Site 1 <<
	.long	Lset152
Lset153 = Ltmp223-Ltmp220               ##   Call between Ltmp220 and Ltmp223
	.long	Lset153
Lset154 = Ltmp224-Lfunc_begin13         ##     jumps to Ltmp224
	.long	Lset154
	.byte	1                       ##   On action: 1
Lset155 = Ltmp223-Lfunc_begin13         ## >> Call Site 2 <<
	.long	Lset155
Lset156 = Ltmp225-Ltmp223               ##   Call between Ltmp223 and Ltmp225
	.long	Lset156
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset157 = Ltmp225-Lfunc_begin13         ## >> Call Site 3 <<
	.long	Lset157
Lset158 = Ltmp226-Ltmp225               ##   Call between Ltmp225 and Ltmp226
	.long	Lset158
Lset159 = Ltmp227-Lfunc_begin13         ##     jumps to Ltmp227
	.long	Lset159
	.byte	0                       ##   On action: cleanup
Lset160 = Ltmp226-Lfunc_begin13         ## >> Call Site 4 <<
	.long	Lset160
Lset161 = Ltmp228-Ltmp226               ##   Call between Ltmp226 and Ltmp228
	.long	Lset161
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset162 = Ltmp228-Lfunc_begin13         ## >> Call Site 5 <<
	.long	Lset162
Lset163 = Ltmp229-Ltmp228               ##   Call between Ltmp228 and Ltmp229
	.long	Lset163
Lset164 = Ltmp230-Lfunc_begin13         ##     jumps to Ltmp230
	.long	Lset164
	.byte	1                       ##   On action: 1
Lset165 = Ltmp229-Lfunc_begin13         ## >> Call Site 6 <<
	.long	Lset165
Lset166 = Lfunc_end13-Ltmp229           ##   Call between Ltmp229 and Lfunc_end13
	.long	Lset166
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.align	4, 0x90
__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev: ## @_ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp234:
	.cfi_def_cfa_offset 16
Ltmp235:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp236:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__113__assoc_stateIPKcED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16futureIPKcED2Ev
	.weak_def_can_be_hidden	__ZNSt3__16futureIPKcED2Ev
	.align	4, 0x90
__ZNSt3__16futureIPKcED2Ev:             ## @_ZNSt3__16futureIPKcED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp237:
	.cfi_def_cfa_offset 16
Ltmp238:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp239:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	cmpq	$0, (%rdi)
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	je	LBB38_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	(%rax), %rcx
	movq	%rcx, %rdi
	callq	__ZNSt3__114__shared_count16__release_sharedEv
	movb	%al, -17(%rbp)          ## 1-byte Spill
LBB38_2:
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC1ERKS7_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC1ERKS7_m
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC1ERKS7_m: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC1ERKS7_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp240:
	.cfi_def_cfa_offset 16
Ltmp241:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp242:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-8(%rbp), %rdi
	movq	-24(%rbp), %rdx
	movq	-16(%rbp), %rsi
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC2ERKS7_m
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotator6__doneEv
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotator6__doneEv
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotator6__doneEv: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotator6__doneEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp243:
	.cfi_def_cfa_offset 16
Ltmp244:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp245:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE21__push_back_slow_pathIS4_EEvOT_
Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception14
## BB#0:
	pushq	%rbp
Ltmp249:
	.cfi_def_cfa_offset 16
Ltmp250:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp251:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movq	%rdi, -376(%rbp)
	movq	%rsi, -384(%rbp)
	movq	-376(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rdi, -368(%rbp)
	movq	-368(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -360(%rbp)
	movq	-360(%rbp), %rdi
	movq	%rdi, -352(%rbp)
	movq	-352(%rbp), %rdi
	movq	%rdi, -392(%rbp)
	movq	%rsi, -344(%rbp)
	movq	-344(%rbp), %rdi
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	subq	%rdi, %rax
	sarq	$3, %rax
	addq	$1, %rax
	movq	%rsi, -304(%rbp)
	movq	%rax, -312(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, %rdi
	movq	%rsi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, -464(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE8max_sizeEv
	movq	%rax, -320(%rbp)
	movq	-312(%rbp), %rax
	cmpq	-320(%rbp), %rax
	jbe	LBB41_2
## BB#1:
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
LBB41_2:
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -264(%rbp)
	movq	-264(%rbp), %rdx
	movq	%rdx, -256(%rbp)
	movq	-256(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	-320(%rbp), %rdx
	shrq	$1, %rdx
	cmpq	%rdx, %rcx
	jb	LBB41_4
## BB#3:
	movq	-320(%rbp), %rax
	movq	%rax, -296(%rbp)
	jmp	LBB41_8
LBB41_4:
	leaq	-208(%rbp), %rax
	leaq	-312(%rbp), %rcx
	leaq	-336(%rbp), %rdx
	movq	-328(%rbp), %rsi
	shlq	$1, %rsi
	movq	%rsi, -336(%rbp)
	movq	%rdx, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rcx
	movq	-240(%rbp), %rdx
	movq	%rcx, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	-216(%rbp), %rcx
	movq	-224(%rbp), %rdx
	movq	%rax, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-192(%rbp), %rax
	movq	(%rax), %rax
	movq	-200(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB41_6
## BB#5:
	movq	-224(%rbp), %rax
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB41_7
LBB41_6:
	movq	-216(%rbp), %rax
	movq	%rax, -472(%rbp)        ## 8-byte Spill
LBB41_7:                                ## %_ZNSt3__13maxImEERKT_S3_S3_.exit.i
	movq	-472(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	%rax, -296(%rbp)
LBB41_8:                                ## %_ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE11__recommendEm.exit
	leaq	-432(%rbp), %rdi
	movq	-296(%rbp), %rsi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	-392(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC1EmmS7_
	movq	-392(%rbp), %rax
	movq	-416(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rdx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rax, -48(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rax
	movq	-64(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-16(%rbp), %rax
	movq	$0, (%rax)
## BB#9:
	movq	-416(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -416(%rbp)
Ltmp246:
	leaq	-432(%rbp), %rsi
	movq	-456(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE
Ltmp247:
	jmp	LBB41_10
LBB41_10:
	leaq	-432(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB41_11:
Ltmp248:
	leaq	-432(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -440(%rbp)
	movl	%ecx, -444(%rbp)
	callq	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev
## BB#12:
	movq	-440(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end14:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table41:
Lexception14:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset167 = Lfunc_begin14-Lfunc_begin14   ## >> Call Site 1 <<
	.long	Lset167
Lset168 = Ltmp246-Lfunc_begin14         ##   Call between Lfunc_begin14 and Ltmp246
	.long	Lset168
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset169 = Ltmp246-Lfunc_begin14         ## >> Call Site 2 <<
	.long	Lset169
Lset170 = Ltmp247-Ltmp246               ##   Call between Ltmp246 and Ltmp247
	.long	Lset170
Lset171 = Ltmp248-Lfunc_begin14         ##     jumps to Ltmp248
	.long	Lset171
	.byte	0                       ##   On action: cleanup
Lset172 = Ltmp247-Lfunc_begin14         ## >> Call Site 3 <<
	.long	Lset172
Lset173 = Lfunc_end14-Ltmp247           ##   Call between Ltmp247 and Lfunc_end14
	.long	Lset173
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC2ERKS7_m
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC2ERKS7_m
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC2ERKS7_m: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE24__RAII_IncreaseAnnotatorC2ERKS7_m
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp252:
	.cfi_def_cfa_offset 16
Ltmp253:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp254:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC1EmmS7_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC1EmmS7_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC1EmmS7_: ## @_ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC1EmmS7_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp255:
	.cfi_def_cfa_offset 16
Ltmp256:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp257:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	callq	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC2EmmS7_
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE
	.weak_def_can_be_hidden	__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE
	.align	4, 0x90
__ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE: ## @_ZNSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE26__swap_out_circular_bufferERNS_14__split_bufferIS4_RS6_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp258:
	.cfi_def_cfa_offset 16
Ltmp259:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp260:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -448(%rbp)
	movq	%rsi, -456(%rbp)
	movq	-448(%rbp), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	callq	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE17__annotate_deleteEv
	movq	-464(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -440(%rbp)
	movq	-440(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -432(%rbp)
	movq	-432(%rbp), %rsi
	movq	%rsi, -424(%rbp)
	movq	-424(%rbp), %rsi
	movq	-464(%rbp), %rdi        ## 8-byte Reload
	movq	(%rdi), %rax
	movq	8(%rdi), %rcx
	movq	-456(%rbp), %rdx
	addq	$8, %rdx
	movq	%rsi, -384(%rbp)
	movq	%rax, -392(%rbp)
	movq	%rcx, -400(%rbp)
	movq	%rdx, -408(%rbp)
LBB44_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-400(%rbp), %rax
	cmpq	-392(%rbp), %rax
	je	LBB44_3
## BB#2:                                ##   in Loop: Header=BB44_1 Depth=1
	movq	-384(%rbp), %rax
	movq	-408(%rbp), %rcx
	movq	(%rcx), %rcx
	addq	$-8, %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	-400(%rbp), %rdx
	addq	$-8, %rdx
	movq	%rdx, -400(%rbp)
	movq	%rdx, -216(%rbp)
	movq	-216(%rbp), %rdx
	movq	%rdx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rax, -336(%rbp)
	movq	%rcx, -344(%rbp)
	movq	%rdx, -352(%rbp)
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rcx
	movq	-352(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movq	%rax, -304(%rbp)
	movq	%rcx, -312(%rbp)
	movq	%rdx, -320(%rbp)
	movq	-304(%rbp), %rax
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movq	%rcx, -272(%rbp)
	movq	%rdx, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	-280(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rcx
	movq	%rax, -240(%rbp)
	movq	%rcx, -248(%rbp)
	movq	-240(%rbp), %rax
	movq	-248(%rbp), %rcx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-232(%rbp), %rax
	movq	$0, (%rax)
	movq	-408(%rbp), %rax
	movq	(%rax), %rcx
	addq	$-8, %rcx
	movq	%rcx, (%rax)
	jmp	LBB44_1
LBB44_3:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorINS_6futureIPKcEEEEE20__construct_backwardIPS5_EEvRS6_T_SB_RSB_.exit
	leaq	-192(%rbp), %rax
	leaq	-96(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	-464(%rbp), %rsi        ## 8-byte Reload
	movq	-456(%rbp), %rdi
	addq	$8, %rdi
	movq	%rsi, -32(%rbp)
	movq	%rdi, -40(%rbp)
	movq	-32(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-32(%rbp), %rdi
	movq	%rsi, (%rdi)
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-40(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	-464(%rbp), %rdx        ## 8-byte Reload
	addq	$8, %rdx
	movq	-456(%rbp), %rsi
	addq	$16, %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rdx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -96(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-80(%rbp), %rsi
	movq	%rdx, (%rsi)
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	addq	$16, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	-456(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -136(%rbp)
	movq	-136(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rcx, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -192(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-176(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-456(%rbp), %rax
	movq	8(%rax), %rax
	movq	-456(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	movq	%rax, %rdi
	movq	%rdx, %rsi
	callq	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE14__annotate_newEm
	movq	-464(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev: ## @_ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp261:
	.cfi_def_cfa_offset 16
Ltmp262:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp263:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE8max_sizeEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE8max_sizeEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE8max_sizeEv: ## @_ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE8max_sizeEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp264:
	.cfi_def_cfa_offset 16
Ltmp265:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp266:
	.cfi_def_cfa_register %rbp
	subq	$56, %rsp
	leaq	-32(%rbp), %rax
	leaq	-168(%rbp), %rcx
	leaq	-160(%rbp), %rdx
	movq	$-1, %rsi
	movabsq	$2305843009213693951, %r8 ## imm = 0x1FFFFFFFFFFFFFFF
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, -144(%rbp)
	movq	-144(%rbp), %rdi
	addq	$16, %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rdi, -128(%rbp)
	movq	-128(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdi, -96(%rbp)
	movq	-96(%rbp), %rdi
	movq	%rdi, -80(%rbp)
	movq	%r8, -160(%rbp)
	shrq	$1, %rsi
	movq	%rsi, -168(%rbp)
	movq	%rdx, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	-40(%rbp), %rdx
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	-16(%rbp), %rax
	movq	(%rax), %rax
	movq	-24(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB46_2
## BB#1:
	movq	-48(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
	jmp	LBB46_3
LBB46_2:
	movq	-40(%rbp), %rax
	movq	%rax, -176(%rbp)        ## 8-byte Spill
LBB46_3:                                ## %_ZNSt3__13minImEERKT_S3_S3_.exit
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -184(%rbp)        ## 8-byte Spill
## BB#4:
	movq	-184(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	addq	$56, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC2EmmS7_
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC2EmmS7_
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC2EmmS7_: ## @_ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEEC2EmmS7_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp267:
	.cfi_def_cfa_offset 16
Ltmp268:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp269:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	leaq	-136(%rbp), %rax
	leaq	-168(%rbp), %r8
	movq	%rdi, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rdx, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$24, %rdx
	movq	-232(%rbp), %rsi
	movq	%rdx, -184(%rbp)
	movq	$0, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	-184(%rbp), %rdx
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rdx, -160(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rdi, -176(%rbp)
	movq	-160(%rbp), %rdx
	movq	%r8, -152(%rbp)
	movq	-152(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	-176(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movq	%rdx, -128(%rbp)
	movq	%rsi, -136(%rbp)
	movq	%rdi, -144(%rbp)
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	-144(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, 8(%rdx)
	cmpq	$0, -216(%rbp)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	je	LBB47_2
## BB#1:
	movq	-240(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	-216(%rbp), %rdx
	movq	%rcx, -64(%rbp)
	movq	%rdx, -72(%rbp)
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	$0, -56(%rbp)
	movq	-48(%rbp), %rcx
	shlq	$3, %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	jmp	LBB47_3
LBB47_2:
	xorl	%eax, %eax
	movl	%eax, %ecx
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	jmp	LBB47_3
LBB47_3:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	-240(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, (%rcx)
	movq	(%rcx), %rax
	movq	-224(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rax, 16(%rcx)
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	-216(%rbp), %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	%rax, (%rdx)
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE17__annotate_deleteEv
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE17__annotate_deleteEv
	.align	4, 0x90
__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE17__annotate_deleteEv: ## @_ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE17__annotate_deleteEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp270:
	.cfi_def_cfa_offset 16
Ltmp271:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp272:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rdi, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rsi
	addq	$16, %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	subq	%rdx, %rsi
	sarq	$3, %rsi
	shlq	$3, %rsi
	addq	%rsi, %rcx
	movq	%rdi, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	8(%rsi), %r8
	movq	(%rsi), %rsi
	subq	%rsi, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -80(%rbp)
	movq	-80(%rbp), %rsi
	movq	(%rsi), %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, -104(%rbp)
	movq	-104(%rbp), %r9
	addq	$16, %r9
	movq	%r9, -96(%rbp)
	movq	-96(%rbp), %r9
	movq	%r9, -88(%rbp)
	movq	-88(%rbp), %r9
	movq	(%r9), %r9
	movq	(%r8), %r8
	subq	%r8, %r9
	sarq	$3, %r9
	shlq	$3, %r9
	addq	%r9, %rsi
	movq	%rsi, -168(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdx
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	-168(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE14__annotate_newEm
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE14__annotate_newEm
	.align	4, 0x90
__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE14__annotate_newEm: ## @_ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE14__annotate_newEm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp273:
	.cfi_def_cfa_offset 16
Ltmp274:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp275:
	.cfi_def_cfa_register %rbp
	subq	$176, %rsp
	movq	%rdi, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-152(%rbp), %rsi
	movq	%rsi, -144(%rbp)
	movq	-144(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, -136(%rbp)
	movq	-136(%rbp), %rdi
	movq	%rsi, -128(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	%rsi, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rdx
	addq	$16, %rdx
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rcx), %rcx
	subq	%rcx, %rdx
	sarq	$3, %rdx
	shlq	$3, %rdx
	addq	%rdx, %rax
	movq	%rsi, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rsi, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %r8
	addq	$16, %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	%r8, -64(%rbp)
	movq	-64(%rbp), %r8
	movq	(%r8), %r8
	movq	(%rdx), %rdx
	subq	%rdx, %r8
	sarq	$3, %r8
	shlq	$3, %r8
	addq	%r8, %rcx
	movq	%rsi, -112(%rbp)
	movq	-112(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -104(%rbp)
	movq	-104(%rbp), %rdx
	movq	-160(%rbp), %r8
	shlq	$3, %r8
	addq	%r8, %rdx
	movq	%rdi, -168(%rbp)        ## 8-byte Spill
	movq	%rsi, %rdi
	movq	-168(%rbp), %rsi        ## 8-byte Reload
	movq	%rdx, -176(%rbp)        ## 8-byte Spill
	movq	%rax, %rdx
	movq	-176(%rbp), %r8         ## 8-byte Reload
	callq	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_
	addq	$176, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_
	.weak_def_can_be_hidden	__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_
	.align	4, 0x90
__ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_: ## @_ZNKSt3__16vectorINS_6futureIPKcEENS_9allocatorIS4_EEE31__annotate_contiguous_containerEPKvS9_S9_S9_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp276:
	.cfi_def_cfa_offset 16
Ltmp277:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp278:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%r8, -40(%rbp)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED2Ev
	.align	4, 0x90
__ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED2Ev: ## @_ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	subq	$320, %rsp              ## imm = 0x140
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -272(%rbp)
	movq	-272(%rbp), %rax
	movq	8(%rax), %rcx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rcx
	movq	%rax, -232(%rbp)
	movq	%rcx, -240(%rbp)
	movq	-232(%rbp), %rax
	movq	%rdi, -288(%rbp)        ## 8-byte Spill
	movq	%rax, -296(%rbp)        ## 8-byte Spill
LBB51_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-240(%rbp), %rax
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	cmpq	16(%rcx), %rax
	je	LBB51_3
## BB#2:                                ##   in Loop: Header=BB51_1 Depth=1
	movq	-296(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rdx
	addq	$-8, %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rdx
	movq	%rcx, -144(%rbp)
	movq	%rdx, -152(%rbp)
	movq	-144(%rbp), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-128(%rbp), %rdi
	callq	__ZNSt3__16futureIPKcED1Ev
	jmp	LBB51_1
LBB51_3:                                ## %_ZNSt3__114__split_bufferINS_6futureIPKcEERNS_9allocatorIS4_EEE5clearEv.exit
	movq	-288(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, (%rax)
	je	LBB51_6
## BB#4:
	movq	-288(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	addq	$24, %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	(%rax), %rdx
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	%rsi, -80(%rbp)
	movq	-80(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -72(%rbp)
	movq	-72(%rbp), %rdi
	movq	%rdi, -64(%rbp)
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	subq	%rsi, %rdi
	sarq	$3, %rdi
	movq	%rcx, -304(%rbp)        ## 8-byte Spill
	movq	%rdx, -312(%rbp)        ## 8-byte Spill
	movq	%rdi, -320(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-304(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-320(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -56(%rbp)
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	-56(%rbp), %r8
	movq	%rsi, -16(%rbp)
	movq	%rdi, -24(%rbp)
	movq	%r8, -32(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZdlPv
LBB51_6:
	addq	$320, %rsp              ## imm = 0x140
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__113__assoc_stateIPKcE4moveEv
	.weak_def_can_be_hidden	__ZNSt3__113__assoc_stateIPKcE4moveEv
	.align	4, 0x90
__ZNSt3__113__assoc_stateIPKcE4moveEv:  ## @_ZNSt3__113__assoc_stateIPKcE4moveEv
Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception15
## BB#0:
	pushq	%rbp
Ltmp288:
	.cfi_def_cfa_offset 16
Ltmp289:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp290:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -144(%rbp)
	movq	%rdi, %rax
	movq	%rdi, %rcx
	addq	$24, %rcx
	leaq	-160(%rbp), %rdx
	movq	%rdx, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	-128(%rbp), %rsi
	movq	%rsi, -112(%rbp)
	movq	%rcx, -120(%rbp)
	movq	-112(%rbp), %rsi
	movq	%rcx, (%rsi)
	movb	$1, 8(%rsi)
	movq	(%rsi), %rcx
	movq	%rdi, -200(%rbp)        ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rdx, -208(%rbp)        ## 8-byte Spill
	movq	%rax, -216(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__15mutex4lockEv
Ltmp282:
	movq	-200(%rbp), %rdi        ## 8-byte Reload
	movq	-208(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__117__assoc_sub_state10__sub_waitERNS_11unique_lockINS_5mutexEEE
Ltmp283:
	jmp	LBB52_1
LBB52_1:
	leaq	-184(%rbp), %rax
	movq	-216(%rbp), %rcx        ## 8-byte Reload
	addq	$16, %rcx
	movq	%rax, -96(%rbp)
	movq	$0, -104(%rbp)
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %rsi
	movq	%rdx, -80(%rbp)
	movq	%rsi, -88(%rbp)
	movq	-80(%rbp), %rdx
	movq	$0, (%rdx)
	movq	%rcx, -48(%rbp)
	movq	%rax, -56(%rbp)
	movq	-48(%rbp), %rcx
	movq	-56(%rbp), %rdx
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-40(%rbp), %rdx
	cmpq	(%rdx), %rcx
	sete	%dil
	xorb	$-1, %dil
	movb	%dil, -217(%rbp)        ## 1-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt13exception_ptrD1Ev
	movb	-217(%rbp), %r8b        ## 1-byte Reload
	testb	$1, %r8b
	jne	LBB52_2
	jmp	LBB52_6
LBB52_2:
	movq	-216(%rbp), %rax        ## 8-byte Reload
	addq	$16, %rax
	leaq	-192(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, %rsi
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	callq	__ZNSt13exception_ptrC1ERKS_
Ltmp285:
	movq	-232(%rbp), %rdi        ## 8-byte Reload
	callq	__ZSt17rethrow_exceptionSt13exception_ptr
Ltmp286:
	jmp	LBB52_3
LBB52_3:
LBB52_4:
Ltmp284:
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
	jmp	LBB52_9
LBB52_5:
Ltmp287:
	leaq	-192(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -168(%rbp)
	movl	%ecx, -172(%rbp)
	callq	__ZNSt13exception_ptrD1Ev
	jmp	LBB52_9
LBB52_6:
	leaq	-160(%rbp), %rax
	movq	-216(%rbp), %rcx        ## 8-byte Reload
	addq	$144, %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	testb	$1, 8(%rax)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB52_8
## BB#7:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	callq	__ZNSt3__15mutex6unlockEv
LBB52_8:                                ## %_ZNSt3__111unique_lockINS_5mutexEED1Ev.exit1
	movq	-240(%rbp), %rax        ## 8-byte Reload
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
LBB52_9:
	leaq	-160(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	testb	$1, 8(%rax)
	movq	%rax, -256(%rbp)        ## 8-byte Spill
	je	LBB52_11
## BB#10:
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rdi
	callq	__ZNSt3__15mutex6unlockEv
LBB52_11:                               ## %_ZNSt3__111unique_lockINS_5mutexEED1Ev.exit
	jmp	LBB52_12
LBB52_12:
	movq	-168(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end15:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table52:
Lexception15:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset174 = Lfunc_begin15-Lfunc_begin15   ## >> Call Site 1 <<
	.long	Lset174
Lset175 = Ltmp282-Lfunc_begin15         ##   Call between Lfunc_begin15 and Ltmp282
	.long	Lset175
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset176 = Ltmp282-Lfunc_begin15         ## >> Call Site 2 <<
	.long	Lset176
Lset177 = Ltmp283-Ltmp282               ##   Call between Ltmp282 and Ltmp283
	.long	Lset177
Lset178 = Ltmp284-Lfunc_begin15         ##     jumps to Ltmp284
	.long	Lset178
	.byte	0                       ##   On action: cleanup
Lset179 = Ltmp285-Lfunc_begin15         ## >> Call Site 3 <<
	.long	Lset179
Lset180 = Ltmp286-Ltmp285               ##   Call between Ltmp285 and Ltmp286
	.long	Lset180
Lset181 = Ltmp287-Lfunc_begin15         ##     jumps to Ltmp287
	.long	Lset181
	.byte	0                       ##   On action: cleanup
Lset182 = Ltmp286-Lfunc_begin15         ## >> Call Site 4 <<
	.long	Lset182
Lset183 = Lfunc_end15-Ltmp286           ##   Call between Ltmp286 and Lfunc_end15
	.long	Lset183
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception16
## BB#0:
	pushq	%rbp
Ltmp312:
	.cfi_def_cfa_offset 16
Ltmp313:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp314:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp291:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp292:
	jmp	LBB53_1
LBB53_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB53_3
	jmp	LBB53_26
LBB53_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB53_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB53_7
LBB53_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB53_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB53_8
	jmp	LBB53_13
LBB53_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp294:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp295:
	jmp	LBB53_9
LBB53_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp296:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp297:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB53_10
LBB53_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp298:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp299:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB53_12
LBB53_11:
Ltmp300:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB53_21
LBB53_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB53_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp301:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp302:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB53_15
LBB53_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB53_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp303:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp304:
	jmp	LBB53_17
LBB53_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB53_18
LBB53_18:
	jmp	LBB53_25
LBB53_19:
Ltmp293:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB53_22
LBB53_20:
Ltmp305:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB53_21
LBB53_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB53_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp306:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp307:
	jmp	LBB53_23
LBB53_23:
	callq	___cxa_end_catch
LBB53_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB53_25:
	jmp	LBB53_26
LBB53_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB53_24
LBB53_27:
Ltmp308:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp309:
	callq	___cxa_end_catch
Ltmp310:
	jmp	LBB53_28
LBB53_28:
	jmp	LBB53_29
LBB53_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB53_30:
Ltmp311:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end16:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table53:
Lexception16:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset184 = Ltmp291-Lfunc_begin16         ## >> Call Site 1 <<
	.long	Lset184
Lset185 = Ltmp292-Ltmp291               ##   Call between Ltmp291 and Ltmp292
	.long	Lset185
Lset186 = Ltmp293-Lfunc_begin16         ##     jumps to Ltmp293
	.long	Lset186
	.byte	5                       ##   On action: 3
Lset187 = Ltmp294-Lfunc_begin16         ## >> Call Site 2 <<
	.long	Lset187
Lset188 = Ltmp295-Ltmp294               ##   Call between Ltmp294 and Ltmp295
	.long	Lset188
Lset189 = Ltmp305-Lfunc_begin16         ##     jumps to Ltmp305
	.long	Lset189
	.byte	5                       ##   On action: 3
Lset190 = Ltmp296-Lfunc_begin16         ## >> Call Site 3 <<
	.long	Lset190
Lset191 = Ltmp299-Ltmp296               ##   Call between Ltmp296 and Ltmp299
	.long	Lset191
Lset192 = Ltmp300-Lfunc_begin16         ##     jumps to Ltmp300
	.long	Lset192
	.byte	3                       ##   On action: 2
Lset193 = Ltmp301-Lfunc_begin16         ## >> Call Site 4 <<
	.long	Lset193
Lset194 = Ltmp304-Ltmp301               ##   Call between Ltmp301 and Ltmp304
	.long	Lset194
Lset195 = Ltmp305-Lfunc_begin16         ##     jumps to Ltmp305
	.long	Lset195
	.byte	5                       ##   On action: 3
Lset196 = Ltmp304-Lfunc_begin16         ## >> Call Site 5 <<
	.long	Lset196
Lset197 = Ltmp306-Ltmp304               ##   Call between Ltmp304 and Ltmp306
	.long	Lset197
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset198 = Ltmp306-Lfunc_begin16         ## >> Call Site 6 <<
	.long	Lset198
Lset199 = Ltmp307-Ltmp306               ##   Call between Ltmp306 and Ltmp307
	.long	Lset199
Lset200 = Ltmp308-Lfunc_begin16         ##     jumps to Ltmp308
	.long	Lset200
	.byte	0                       ##   On action: cleanup
Lset201 = Ltmp307-Lfunc_begin16         ## >> Call Site 7 <<
	.long	Lset201
Lset202 = Ltmp309-Ltmp307               ##   Call between Ltmp307 and Ltmp309
	.long	Lset202
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset203 = Ltmp309-Lfunc_begin16         ## >> Call Site 8 <<
	.long	Lset203
Lset204 = Ltmp310-Ltmp309               ##   Call between Ltmp309 and Ltmp310
	.long	Lset204
Lset205 = Ltmp311-Lfunc_begin16         ##     jumps to Ltmp311
	.long	Lset205
	.byte	5                       ##   On action: 3
Lset206 = Ltmp310-Lfunc_begin16         ## >> Call Site 9 <<
	.long	Lset206
Lset207 = Lfunc_end16-Ltmp310           ##   Call between Ltmp310 and Lfunc_end16
	.long	Lset207
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp315:
	.cfi_def_cfa_offset 16
Ltmp316:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp317:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception17
## BB#0:
	pushq	%rbp
Ltmp321:
	.cfi_def_cfa_offset 16
Ltmp322:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp323:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB55_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB55_26
LBB55_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB55_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB55_5
LBB55_4:
	movq	$0, -368(%rbp)
LBB55_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB55_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB55_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB55_26
LBB55_8:
	jmp	LBB55_9
LBB55_9:
	cmpq	$0, -368(%rbp)
	jle	LBB55_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB55_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB55_13
LBB55_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB55_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp318:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp319:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB55_14
LBB55_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB55_15
LBB55_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB55_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB55_19
LBB55_17:
Ltmp320:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB55_27
LBB55_18:
	movl	$0, -416(%rbp)
LBB55_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB55_20
	jmp	LBB55_29
LBB55_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB55_26
	jmp	LBB55_28
LBB55_20:
	jmp	LBB55_21
LBB55_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB55_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB55_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB55_26
LBB55_24:
	jmp	LBB55_25
LBB55_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB55_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB55_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB55_28:
Lfunc_end17:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table55:
Lexception17:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset208 = Lfunc_begin17-Lfunc_begin17   ## >> Call Site 1 <<
	.long	Lset208
Lset209 = Ltmp318-Lfunc_begin17         ##   Call between Lfunc_begin17 and Ltmp318
	.long	Lset209
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset210 = Ltmp318-Lfunc_begin17         ## >> Call Site 2 <<
	.long	Lset210
Lset211 = Ltmp319-Ltmp318               ##   Call between Ltmp318 and Ltmp319
	.long	Lset211
Lset212 = Ltmp320-Lfunc_begin17         ##     jumps to Ltmp320
	.long	Lset212
	.byte	0                       ##   On action: cleanup
Lset213 = Ltmp319-Lfunc_begin17         ## >> Call Site 3 <<
	.long	Lset213
Lset214 = Lfunc_end17-Ltmp319           ##   Call between Ltmp319 and Lfunc_end17
	.long	Lset214
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp324:
	.cfi_def_cfa_offset 16
Ltmp325:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp326:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp327:
	.cfi_def_cfa_offset 16
Ltmp328:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp329:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"ERROR"

L_.str.1:                               ## @.str.1
	.asciz	"FINE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTVNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	3
__ZTVNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.quad	0
	.quad	__ZTINSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.quad	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.quad	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.quad	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE16__on_zero_sharedEv
	.quad	__ZNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTSNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_definition	__ZTSNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	4
__ZTSNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.asciz	"NSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE"

	.globl	__ZTSNSt3__113__assoc_stateIPKcEE ## @_ZTSNSt3__113__assoc_stateIPKcEE
	.weak_definition	__ZTSNSt3__113__assoc_stateIPKcEE
	.align	4
__ZTSNSt3__113__assoc_stateIPKcEE:
	.asciz	"NSt3__113__assoc_stateIPKcEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__113__assoc_stateIPKcEE ## @_ZTINSt3__113__assoc_stateIPKcEE
	.weak_definition	__ZTINSt3__113__assoc_stateIPKcEE
	.align	4
__ZTINSt3__113__assoc_stateIPKcEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__113__assoc_stateIPKcEE
	.quad	__ZTINSt3__117__assoc_sub_stateE

	.globl	__ZTINSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTINSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_definition	__ZTINSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	4
__ZTINSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119__async_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.quad	__ZTINSt3__113__assoc_stateIPKcEE

	.globl	__ZTVNSt3__113__assoc_stateIPKcEE ## @_ZTVNSt3__113__assoc_stateIPKcEE
	.weak_def_can_be_hidden	__ZTVNSt3__113__assoc_stateIPKcEE
	.align	3
__ZTVNSt3__113__assoc_stateIPKcEE:
	.quad	0
	.quad	__ZTINSt3__113__assoc_stateIPKcEE
	.quad	__ZNSt3__113__assoc_stateIPKcED1Ev
	.quad	__ZNSt3__113__assoc_stateIPKcED0Ev
	.quad	__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	.quad	__ZNSt3__117__assoc_sub_state9__executeEv

	.section	__TEXT,__cstring,cstring_literals
L_.str.2:                               ## @.str.2
	.asciz	"thread constructor failed"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTVNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	3
__ZTVNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.quad	0
	.quad	__ZTINSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.quad	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED1Ev
	.quad	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEED0Ev
	.quad	__ZNSt3__113__assoc_stateIPKcE16__on_zero_sharedEv
	.quad	__ZNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEE9__executeEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTSNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_definition	__ZTSNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	4
__ZTSNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.asciz	"NSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE ## @_ZTINSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.weak_definition	__ZTINSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.align	4
__ZTINSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__122__deferred_assoc_stateIPKcNS_12__async_funcIPFS2_16ForThread_structEJS4_EEEEE
	.quad	__ZTINSt3__113__assoc_stateIPKcEE


.subsections_via_symbols
