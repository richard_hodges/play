	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost7variantI3ADDJ3DELEED1Ev
	.weak_def_can_be_hidden	__ZN5boost7variantI3ADDJ3DELEED1Ev
	.align	4, 0x90
__ZN5boost7variantI3ADDJ3DELEED1Ev:     ## @_ZN5boost7variantI3ADDJ3DELEED1Ev
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	(%rdi), %eax
	movl	%eax, %esi
	sarl	$31, %esi
	xorl	%eax, %esi
	leaq	4(%rdi), %rcx
Ltmp0:
	leaq	-8(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	%eax, %edi
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp1:
## BB#1:                                ## %_ZN5boost7variantI3ADDJ3DELEED2Ev.exit
	addq	$16, %rsp
	popq	%rbp
	retq
LBB0_2:
Ltmp2:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset1
Lset2 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset2
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI1_0:
	.quad	4517329193108106637     ## double 9.9999999999999995E-7
	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z19make_virtual_shapesi
	.align	4, 0x90
__Z19make_virtual_shapesi:              ## @_Z19make_virtual_shapesi
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp26:
	.cfi_def_cfa_offset 16
Ltmp27:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp28:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
Ltmp29:
	.cfi_offset %rbx, -56
Ltmp30:
	.cfi_offset %r12, -48
Ltmp31:
	.cfi_offset %r13, -40
Ltmp32:
	.cfi_offset %r14, -32
Ltmp33:
	.cfi_offset %r15, -24
	movl	%edi, %r15d
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
Ltmp6:
	movl	$8, %edi
	callq	__Znwm
Ltmp7:
## BB#1:
	movq	__ZTV6Square@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -72(%rbp)
Ltmp8:
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rsi
	callq	__ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Ltmp9:
## BB#2:                                ## %_ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE9push_backEOS2_.exit
Ltmp10:
	movl	$8, %edi
	callq	__Znwm
Ltmp11:
## BB#3:
	movq	__ZTV6Circle@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	%rax, -80(%rbp)
	movq	-56(%rbp), %rcx
	cmpq	-48(%rbp), %rcx
	jae	LBB1_5
## BB#4:
	movq	%rax, (%rcx)
	addq	$8, -56(%rbp)
	jmp	LBB1_6
LBB1_5:
Ltmp12:
	leaq	-64(%rbp), %rdi
	leaq	-80(%rbp), %rsi
	callq	__ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
Ltmp13:
LBB1_6:                                 ## %_ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE9push_backEOS2_.exit2
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	testl	%r15d, %r15d
	jle	LBB1_10
## BB#7:                                ## %.lr.ph12
	xorl	%r12d, %r12d
	.align	4, 0x90
LBB1_8:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB1_25 Depth 2
	movq	-64(%rbp), %r13
	movq	-56(%rbp), %r14
	cmpq	%r14, %r13
	je	LBB1_9
	.align	4, 0x90
LBB1_25:                                ## %.lr.ph
                                        ##   Parent Loop BB1_8 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rbx
	movq	(%rbx), %rax
	movq	(%rax), %rax
Ltmp14:
	movq	%rbx, %rdi
	callq	*%rax
Ltmp15:
## BB#26:                               ##   in Loop: Header=BB1_25 Depth=2
	movq	(%rbx), %rax
	movq	8(%rax), %rax
Ltmp16:
	movq	%rbx, %rdi
	callq	*%rax
Ltmp17:
## BB#27:                               ##   in Loop: Header=BB1_25 Depth=2
	addq	$8, %r13
	cmpq	%r13, %r14
	jne	LBB1_25
LBB1_9:                                 ## %._crit_edge
                                        ##   in Loop: Header=BB1_8 Depth=1
	incl	%r12d
	cmpl	%r15d, %r12d
	jl	LBB1_8
LBB1_10:                                ## %._crit_edge.13
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, %rbx
Ltmp19:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str(%rip), %rsi
	movl	$25, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp20:
## BB#11:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit4
	subq	-88(%rbp), %rbx         ## 8-byte Folded Reload
	cvtsi2sdq	%rbx, %xmm0
	mulsd	LCPI1_0(%rip), %xmm0
Ltmp21:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEd
Ltmp22:
## BB#12:
Ltmp23:
	leaq	L_.str.5(%rip), %rsi
	movl	$8, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp24:
## BB#13:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB1_17
## BB#14:
	movq	-56(%rbp), %rax
	cmpq	%rdi, %rax
	je	LBB1_16
## BB#15:                               ## %.lr.ph.preheader.i.i.i.i.i
	leaq	-8(%rax), %rcx
	subq	%rdi, %rcx
	notq	%rcx
	andq	$-8, %rcx
	addq	%rax, %rcx
	movq	%rcx, -56(%rbp)
LBB1_16:                                ## %_ZNSt3__113__vector_baseIP6IShapeNS_9allocatorIS2_EEE5clearEv.exit.i.i.i
	callq	__ZdlPv
LBB1_17:                                ## %_ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEED1Ev.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB1_18:                                ## %.loopexit
Ltmp18:
LBB1_20:
	movq	%rax, %rbx
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB1_24
## BB#21:
	movq	-56(%rbp), %rax
	cmpq	%rdi, %rax
	je	LBB1_23
## BB#22:                               ## %.lr.ph.preheader.i.i.i.i.i.7
	leaq	-8(%rax), %rcx
	subq	%rdi, %rcx
	notq	%rcx
	andq	$-8, %rcx
	addq	%rax, %rcx
	movq	%rcx, -56(%rbp)
LBB1_23:                                ## %_ZNSt3__113__vector_baseIP6IShapeNS_9allocatorIS2_EEE5clearEv.exit.i.i.i.8
	callq	__ZdlPv
LBB1_24:                                ## %_ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEED1Ev.exit9
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB1_19:                                ## %.loopexit.split-lp
Ltmp25:
	jmp	LBB1_20
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\266\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset3 = Ltmp6-Lfunc_begin1              ## >> Call Site 1 <<
	.long	Lset3
Lset4 = Ltmp13-Ltmp6                    ##   Call between Ltmp6 and Ltmp13
	.long	Lset4
Lset5 = Ltmp25-Lfunc_begin1             ##     jumps to Ltmp25
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp14-Lfunc_begin1             ## >> Call Site 2 <<
	.long	Lset6
Lset7 = Ltmp17-Ltmp14                   ##   Call between Ltmp14 and Ltmp17
	.long	Lset7
Lset8 = Ltmp18-Lfunc_begin1             ##     jumps to Ltmp18
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp19-Lfunc_begin1             ## >> Call Site 3 <<
	.long	Lset9
Lset10 = Ltmp24-Ltmp19                  ##   Call between Ltmp19 and Ltmp24
	.long	Lset10
Lset11 = Ltmp25-Lfunc_begin1            ##     jumps to Ltmp25
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp24-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset12
Lset13 = Lfunc_end1-Ltmp24              ##   Call between Ltmp24 and Lfunc_end1
	.long	Lset13
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI2_0:
	.quad	4517329193108106637     ## double 9.9999999999999995E-7
	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z20make_template_shapesi
	.align	4, 0x90
__Z20make_template_shapesi:             ## @_Z20make_template_shapesi
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp73:
	.cfi_def_cfa_offset 16
Ltmp74:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp75:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
Ltmp76:
	.cfi_offset %rbx, -56
Ltmp77:
	.cfi_offset %r12, -48
Ltmp78:
	.cfi_offset %r13, -40
Ltmp79:
	.cfi_offset %r14, -32
Ltmp80:
	.cfi_offset %r15, -24
	movl	%edi, %r15d
	movl	%r15d, -156(%rbp)       ## 4-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, -128(%rbp)
	movq	$0, -112(%rbp)
	leaq	-132(%rbp), %rbx
	movq	$0, -136(%rbp)
Ltmp34:
	leaq	-128(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	callq	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
Ltmp35:
## BB#1:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE9push_backEOS5_.exit
	movl	-136(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp40:
	leaq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp41:
## BB#2:
	leaq	-140(%rbp), %r14
	movq	$1, -144(%rbp)
	movq	-120(%rbp), %rbx
	cmpq	-112(%rbp), %rbx
	jae	LBB2_5
## BB#3:
	leaq	4(%rbx), %rax
	movq	%rax, -96(%rbp)
Ltmp49:
	leaq	-96(%rbp), %rdx
	movl	$1, %edi
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp50:
## BB#4:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE9constructIS6_JS6_EEEvRS7_PT_DpOT0_.exit.i.7
	movl	-144(%rbp), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%eax, %ecx
	movl	%ecx, (%rbx)
	addq	$8, -120(%rbp)
	jmp	LBB2_6
LBB2_5:
Ltmp43:
	leaq	-128(%rbp), %rdi
	leaq	-144(%rbp), %rsi
	callq	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
Ltmp44:
LBB2_6:                                 ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE9push_backEOS5_.exit9
	movl	-144(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp52:
	leaq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp53:
## BB#7:                                ## %_ZN5boost7variantI7TSquareJ7TCircleEED1Ev.exit13
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, -168(%rbp)        ## 8-byte Spill
	testl	%r15d, %r15d
	jle	LBB2_12
## BB#8:                                ## %.lr.ph48
	xorl	%r14d, %r14d
	leaq	-152(%rbp), %r15
	leaq	-48(%rbp), %r13
LBB2_9:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB2_10 Depth 2
	movq	-128(%rbp), %rbx
	movq	-120(%rbp), %r12
	jmp	LBB2_10
	.align	4, 0x90
LBB2_40:                                ##   in Loop: Header=BB2_10 Depth=2
	addq	$4, %rbx
LBB2_10:                                ##   Parent Loop BB2_9 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpq	%r12, %rbx
	je	LBB2_11
## BB#38:                               ## %.lr.ph
                                        ##   in Loop: Header=BB2_10 Depth=2
	movq	%r15, -88(%rbp)
	leaq	__ZL3add(%rip), %rax
	movq	%rax, -80(%rbp)
	leaq	-88(%rbp), %rax
	movq	%rax, -72(%rbp)
	movl	(%rbx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$4, %rbx
Ltmp55:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	leaq	-72(%rbp), %rdx
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp56:
## BB#39:                               ##   in Loop: Header=BB2_10 Depth=2
	movq	%r15, -64(%rbp)
	leaq	__ZL3del(%rip), %rax
	movq	%rax, -56(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -48(%rbp)
	movl	-4(%rbx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp57:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp58:
	jmp	LBB2_40
	.align	4, 0x90
LBB2_11:                                ## %._crit_edge
                                        ##   in Loop: Header=BB2_9 Depth=1
	incl	%r14d
	movl	-156(%rbp), %eax        ## 4-byte Reload
	cmpl	%eax, %r14d
	jl	LBB2_9
LBB2_12:                                ## %._crit_edge.49
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, %rbx
Ltmp60:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.6(%rip), %rsi
	movl	$26, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp61:
## BB#13:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
	subq	-168(%rbp), %rbx        ## 8-byte Folded Reload
	cvtsi2sdq	%rbx, %xmm0
	mulsd	LCPI2_0(%rip), %xmm0
Ltmp62:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEd
Ltmp63:
## BB#14:
Ltmp64:
	leaq	L_.str.5(%rip), %rsi
	movl	$8, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp65:
## BB#15:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit33
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB2_22
## BB#16:
	movq	-120(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	LBB2_21
## BB#17:                               ## %.lr.ph.i.i.i.i.i.35
	leaq	-96(%rbp), %r14
	.align	4, 0x90
LBB2_18:                                ## =>This Inner Loop Header: Depth=1
	leaq	-8(%rcx), %rax
	movq	%rax, -120(%rbp)
	movl	-8(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$-4, %rcx
Ltmp70:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp71:
## BB#19:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i.i.i.38
                                        ##   in Loop: Header=BB2_18 Depth=1
	movq	-120(%rbp), %rcx
	cmpq	%rbx, %rcx
	jne	LBB2_18
## BB#20:                               ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.loopexit.i.i.i.41
	movq	-128(%rbp), %rbx
LBB2_21:                                ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.i.i.i.42
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB2_22:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEED1Ev.exit43
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB2_23:                                ## %.loopexit
Ltmp59:
LBB2_25:
	movq	%rax, %r15
LBB2_26:
	movq	-128(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB2_33
## BB#27:
	movq	-120(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	LBB2_32
## BB#28:                               ## %.lr.ph.i.i.i.i.i
	leaq	-96(%rbp), %r14
	.align	4, 0x90
LBB2_29:                                ## =>This Inner Loop Header: Depth=1
	leaq	-8(%rcx), %rax
	movq	%rax, -120(%rbp)
	movl	-8(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$-4, %rcx
Ltmp67:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp68:
## BB#30:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i.i.i
                                        ##   in Loop: Header=BB2_29 Depth=1
	movq	-120(%rbp), %rcx
	cmpq	%rbx, %rcx
	jne	LBB2_29
## BB#31:                               ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.loopexit.i.i.i
	movq	-128(%rbp), %rbx
LBB2_32:                                ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB2_33:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEED1Ev.exit
	movq	%r15, %rdi
	callq	__Unwind_Resume
LBB2_42:
Ltmp69:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_41:
Ltmp72:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_24:                                ## %.loopexit.split-lp
Ltmp66:
	jmp	LBB2_25
LBB2_35:
Ltmp36:
	movq	%rax, %r15
	movl	-136(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp37:
	leaq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp38:
	jmp	LBB2_26
LBB2_34:
Ltmp39:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_43:
Ltmp42:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_45:
Ltmp54:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_44:
Ltmp51:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_37:
Ltmp45:
	movq	%rax, %r15
	movl	-144(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp46:
	leaq	-96(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp47:
	jmp	LBB2_26
LBB2_36:
Ltmp48:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\245\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\234\001"              ## Call site table length
Lset14 = Ltmp34-Lfunc_begin2            ## >> Call Site 1 <<
	.long	Lset14
Lset15 = Ltmp35-Ltmp34                  ##   Call between Ltmp34 and Ltmp35
	.long	Lset15
Lset16 = Ltmp36-Lfunc_begin2            ##     jumps to Ltmp36
	.long	Lset16
	.byte	0                       ##   On action: cleanup
Lset17 = Ltmp40-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset17
Lset18 = Ltmp41-Ltmp40                  ##   Call between Ltmp40 and Ltmp41
	.long	Lset18
Lset19 = Ltmp42-Lfunc_begin2            ##     jumps to Ltmp42
	.long	Lset19
	.byte	1                       ##   On action: 1
Lset20 = Ltmp49-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset20
Lset21 = Ltmp50-Ltmp49                  ##   Call between Ltmp49 and Ltmp50
	.long	Lset21
Lset22 = Ltmp51-Lfunc_begin2            ##     jumps to Ltmp51
	.long	Lset22
	.byte	1                       ##   On action: 1
Lset23 = Ltmp43-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset23
Lset24 = Ltmp44-Ltmp43                  ##   Call between Ltmp43 and Ltmp44
	.long	Lset24
Lset25 = Ltmp45-Lfunc_begin2            ##     jumps to Ltmp45
	.long	Lset25
	.byte	0                       ##   On action: cleanup
Lset26 = Ltmp52-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset26
Lset27 = Ltmp53-Ltmp52                  ##   Call between Ltmp52 and Ltmp53
	.long	Lset27
Lset28 = Ltmp54-Lfunc_begin2            ##     jumps to Ltmp54
	.long	Lset28
	.byte	1                       ##   On action: 1
Lset29 = Ltmp55-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset29
Lset30 = Ltmp58-Ltmp55                  ##   Call between Ltmp55 and Ltmp58
	.long	Lset30
Lset31 = Ltmp59-Lfunc_begin2            ##     jumps to Ltmp59
	.long	Lset31
	.byte	0                       ##   On action: cleanup
Lset32 = Ltmp60-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset32
Lset33 = Ltmp65-Ltmp60                  ##   Call between Ltmp60 and Ltmp65
	.long	Lset33
Lset34 = Ltmp66-Lfunc_begin2            ##     jumps to Ltmp66
	.long	Lset34
	.byte	0                       ##   On action: cleanup
Lset35 = Ltmp70-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset35
Lset36 = Ltmp71-Ltmp70                  ##   Call between Ltmp70 and Ltmp71
	.long	Lset36
Lset37 = Ltmp72-Lfunc_begin2            ##     jumps to Ltmp72
	.long	Lset37
	.byte	1                       ##   On action: 1
Lset38 = Ltmp67-Lfunc_begin2            ## >> Call Site 9 <<
	.long	Lset38
Lset39 = Ltmp68-Ltmp67                  ##   Call between Ltmp67 and Ltmp68
	.long	Lset39
Lset40 = Ltmp69-Lfunc_begin2            ##     jumps to Ltmp69
	.long	Lset40
	.byte	1                       ##   On action: 1
Lset41 = Ltmp68-Lfunc_begin2            ## >> Call Site 10 <<
	.long	Lset41
Lset42 = Ltmp37-Ltmp68                  ##   Call between Ltmp68 and Ltmp37
	.long	Lset42
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp37-Lfunc_begin2            ## >> Call Site 11 <<
	.long	Lset43
Lset44 = Ltmp38-Ltmp37                  ##   Call between Ltmp37 and Ltmp38
	.long	Lset44
Lset45 = Ltmp39-Lfunc_begin2            ##     jumps to Ltmp39
	.long	Lset45
	.byte	1                       ##   On action: 1
Lset46 = Ltmp46-Lfunc_begin2            ## >> Call Site 12 <<
	.long	Lset46
Lset47 = Ltmp47-Ltmp46                  ##   Call between Ltmp46 and Ltmp47
	.long	Lset47
Lset48 = Ltmp48-Lfunc_begin2            ##     jumps to Ltmp48
	.long	Lset48
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI3_0:
	.quad	4517329193108106637     ## double 9.9999999999999995E-7
	.section	__TEXT,__text,regular,pure_instructions
	.globl	__Z27make_template_shapes_singlei
	.align	4, 0x90
__Z27make_template_shapes_singlei:      ## @_Z27make_template_shapes_singlei
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
Ltmp123:
	.cfi_offset %rbx, -56
Ltmp124:
	.cfi_offset %r12, -48
Ltmp125:
	.cfi_offset %r13, -40
Ltmp126:
	.cfi_offset %r14, -32
Ltmp127:
	.cfi_offset %r15, -24
	movl	%edi, %r15d
	movl	%r15d, -116(%rbp)       ## 4-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, -96(%rbp)
	movq	$0, -80(%rbp)
	leaq	-100(%rbp), %rbx
	movq	$0, -104(%rbp)
Ltmp81:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	callq	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
Ltmp82:
## BB#1:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE9push_backEOS5_.exit
	movl	-104(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp87:
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp88:
## BB#2:
	leaq	-108(%rbp), %r14
	movq	$1, -112(%rbp)
	movq	-88(%rbp), %rbx
	cmpq	-80(%rbp), %rbx
	jae	LBB3_5
## BB#3:
	leaq	4(%rbx), %rax
	movq	%rax, -64(%rbp)
Ltmp96:
	leaq	-64(%rbp), %rdx
	movl	$1, %edi
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp97:
## BB#4:                                ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE9constructIS6_JS6_EEEvRS7_PT_DpOT0_.exit.i.7
	movl	-112(%rbp), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%eax, %ecx
	movl	%ecx, (%rbx)
	addq	$8, -88(%rbp)
	jmp	LBB3_6
LBB3_5:
Ltmp90:
	leaq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	callq	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
Ltmp91:
LBB3_6:                                 ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE9push_backEOS5_.exit9
	movl	-112(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp99:
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp100:
## BB#7:                                ## %_ZN5boost7variantI7TSquareJ7TCircleEED1Ev.exit13
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	testl	%r15d, %r15d
	jle	LBB3_12
## BB#8:                                ## %.lr.ph44
	xorl	%r14d, %r14d
	leaq	-56(%rbp), %r13
	leaq	-48(%rbp), %r12
LBB3_9:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB3_10 Depth 2
	movq	-96(%rbp), %rbx
	movq	-88(%rbp), %r15
	jmp	LBB3_10
	.align	4, 0x90
LBB3_40:                                ##   in Loop: Header=BB3_10 Depth=2
	addq	$4, %rbx
LBB3_10:                                ##   Parent Loop BB3_9 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpq	%r15, %rbx
	je	LBB3_11
## BB#38:                               ## %.lr.ph
                                        ##   in Loop: Header=BB3_10 Depth=2
	movl	(%rbx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$4, %rbx
Ltmp102:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r13, %rdx
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp103:
## BB#39:                               ##   in Loop: Header=BB3_10 Depth=2
	movl	-4(%rbx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp104:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp105:
	jmp	LBB3_40
	.align	4, 0x90
LBB3_11:                                ## %._crit_edge
                                        ##   in Loop: Header=BB3_9 Depth=1
	incl	%r14d
	movl	-116(%rbp), %eax        ## 4-byte Reload
	cmpl	%eax, %r14d
	jl	LBB3_9
LBB3_12:                                ## %._crit_edge.45
	callq	__ZN5boost6chrono12steady_clock3nowEv
	movq	%rax, %rbx
Ltmp107:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.7(%rip), %rsi
	movl	$33, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp108:
## BB#13:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
	subq	-128(%rbp), %rbx        ## 8-byte Folded Reload
	cvtsi2sdq	%rbx, %xmm0
	mulsd	LCPI3_0(%rip), %xmm0
Ltmp109:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEd
Ltmp110:
## BB#14:
Ltmp111:
	leaq	L_.str.5(%rip), %rsi
	movl	$8, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp112:
## BB#15:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit29
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB3_22
## BB#16:
	movq	-88(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	LBB3_21
## BB#17:                               ## %.lr.ph.i.i.i.i.i.31
	leaq	-64(%rbp), %r14
	.align	4, 0x90
LBB3_18:                                ## =>This Inner Loop Header: Depth=1
	leaq	-8(%rcx), %rax
	movq	%rax, -88(%rbp)
	movl	-8(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$-4, %rcx
Ltmp117:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp118:
## BB#19:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i.i.i.34
                                        ##   in Loop: Header=BB3_18 Depth=1
	movq	-88(%rbp), %rcx
	cmpq	%rbx, %rcx
	jne	LBB3_18
## BB#20:                               ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.loopexit.i.i.i.37
	movq	-96(%rbp), %rbx
LBB3_21:                                ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.i.i.i.38
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB3_22:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEED1Ev.exit39
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB3_23:                                ## %.loopexit
Ltmp106:
LBB3_25:
	movq	%rax, %r15
LBB3_26:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	LBB3_33
## BB#27:
	movq	-88(%rbp), %rcx
	cmpq	%rbx, %rcx
	je	LBB3_32
## BB#28:                               ## %.lr.ph.i.i.i.i.i
	leaq	-64(%rbp), %r14
	.align	4, 0x90
LBB3_29:                                ## =>This Inner Loop Header: Depth=1
	leaq	-8(%rcx), %rax
	movq	%rax, -88(%rbp)
	movl	-8(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$-4, %rcx
Ltmp114:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp115:
## BB#30:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i.i.i
                                        ##   in Loop: Header=BB3_29 Depth=1
	movq	-88(%rbp), %rcx
	cmpq	%rbx, %rcx
	jne	LBB3_29
## BB#31:                               ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.loopexit.i.i.i
	movq	-96(%rbp), %rbx
LBB3_32:                                ## %_ZNSt3__113__vector_baseIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE5clearEv.exit.i.i.i
	movq	%rbx, %rdi
	callq	__ZdlPv
LBB3_33:                                ## %_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEED1Ev.exit
	movq	%r15, %rdi
	callq	__Unwind_Resume
LBB3_42:
Ltmp116:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_41:
Ltmp119:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_24:                                ## %.loopexit.split-lp
Ltmp113:
	jmp	LBB3_25
LBB3_35:
Ltmp83:
	movq	%rax, %r15
	movl	-104(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp84:
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp85:
	jmp	LBB3_26
LBB3_34:
Ltmp86:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_43:
Ltmp89:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_45:
Ltmp101:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_44:
Ltmp98:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB3_37:
Ltmp92:
	movq	%rax, %r15
	movl	-112(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp93:
	leaq	-64(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp94:
	jmp	LBB3_26
LBB3_36:
Ltmp95:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\245\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\234\001"              ## Call site table length
Lset49 = Ltmp81-Lfunc_begin3            ## >> Call Site 1 <<
	.long	Lset49
Lset50 = Ltmp82-Ltmp81                  ##   Call between Ltmp81 and Ltmp82
	.long	Lset50
Lset51 = Ltmp83-Lfunc_begin3            ##     jumps to Ltmp83
	.long	Lset51
	.byte	0                       ##   On action: cleanup
Lset52 = Ltmp87-Lfunc_begin3            ## >> Call Site 2 <<
	.long	Lset52
Lset53 = Ltmp88-Ltmp87                  ##   Call between Ltmp87 and Ltmp88
	.long	Lset53
Lset54 = Ltmp89-Lfunc_begin3            ##     jumps to Ltmp89
	.long	Lset54
	.byte	1                       ##   On action: 1
Lset55 = Ltmp96-Lfunc_begin3            ## >> Call Site 3 <<
	.long	Lset55
Lset56 = Ltmp97-Ltmp96                  ##   Call between Ltmp96 and Ltmp97
	.long	Lset56
Lset57 = Ltmp98-Lfunc_begin3            ##     jumps to Ltmp98
	.long	Lset57
	.byte	1                       ##   On action: 1
Lset58 = Ltmp90-Lfunc_begin3            ## >> Call Site 4 <<
	.long	Lset58
Lset59 = Ltmp91-Ltmp90                  ##   Call between Ltmp90 and Ltmp91
	.long	Lset59
Lset60 = Ltmp92-Lfunc_begin3            ##     jumps to Ltmp92
	.long	Lset60
	.byte	0                       ##   On action: cleanup
Lset61 = Ltmp99-Lfunc_begin3            ## >> Call Site 5 <<
	.long	Lset61
Lset62 = Ltmp100-Ltmp99                 ##   Call between Ltmp99 and Ltmp100
	.long	Lset62
Lset63 = Ltmp101-Lfunc_begin3           ##     jumps to Ltmp101
	.long	Lset63
	.byte	1                       ##   On action: 1
Lset64 = Ltmp102-Lfunc_begin3           ## >> Call Site 6 <<
	.long	Lset64
Lset65 = Ltmp105-Ltmp102                ##   Call between Ltmp102 and Ltmp105
	.long	Lset65
Lset66 = Ltmp106-Lfunc_begin3           ##     jumps to Ltmp106
	.long	Lset66
	.byte	0                       ##   On action: cleanup
Lset67 = Ltmp107-Lfunc_begin3           ## >> Call Site 7 <<
	.long	Lset67
Lset68 = Ltmp112-Ltmp107                ##   Call between Ltmp107 and Ltmp112
	.long	Lset68
Lset69 = Ltmp113-Lfunc_begin3           ##     jumps to Ltmp113
	.long	Lset69
	.byte	0                       ##   On action: cleanup
Lset70 = Ltmp117-Lfunc_begin3           ## >> Call Site 8 <<
	.long	Lset70
Lset71 = Ltmp118-Ltmp117                ##   Call between Ltmp117 and Ltmp118
	.long	Lset71
Lset72 = Ltmp119-Lfunc_begin3           ##     jumps to Ltmp119
	.long	Lset72
	.byte	1                       ##   On action: 1
Lset73 = Ltmp114-Lfunc_begin3           ## >> Call Site 9 <<
	.long	Lset73
Lset74 = Ltmp115-Ltmp114                ##   Call between Ltmp114 and Ltmp115
	.long	Lset74
Lset75 = Ltmp116-Lfunc_begin3           ##     jumps to Ltmp116
	.long	Lset75
	.byte	1                       ##   On action: 1
Lset76 = Ltmp115-Lfunc_begin3           ## >> Call Site 10 <<
	.long	Lset76
Lset77 = Ltmp84-Ltmp115                 ##   Call between Ltmp115 and Ltmp84
	.long	Lset77
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset78 = Ltmp84-Lfunc_begin3            ## >> Call Site 11 <<
	.long	Lset78
Lset79 = Ltmp85-Ltmp84                  ##   Call between Ltmp84 and Ltmp85
	.long	Lset79
Lset80 = Ltmp86-Lfunc_begin3            ##     jumps to Ltmp86
	.long	Lset80
	.byte	1                       ##   On action: 1
Lset81 = Ltmp93-Lfunc_begin3            ## >> Call Site 12 <<
	.long	Lset81
Lset82 = Ltmp94-Ltmp93                  ##   Call between Ltmp93 and Ltmp94
	.long	Lset82
Lset83 = Ltmp95-Lfunc_begin3            ##     jumps to Ltmp95
	.long	Lset83
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp133:
	.cfi_def_cfa_offset 16
Ltmp134:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp135:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp136:
	.cfi_offset %rbx, -40
Ltmp137:
	.cfi_offset %r14, -32
Ltmp138:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.8(%rip), %rsi
	movl	$12, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
	leaq	-32(%rbp), %r15
	movq	%r15, %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp128:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp129:
## BB#1:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp130:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r15b
Ltmp131:
## BB#2:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	%r15b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	8(%r14), %rdi
	callq	_atoi
	movl	%eax, %ebx
	movl	%ebx, %edi
	callq	__Z19make_virtual_shapesi
	movl	%ebx, %edi
	callq	__Z20make_template_shapesi
	movl	%ebx, %edi
	callq	__Z27make_template_shapes_singlei
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB4_3:
Ltmp132:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset84 = Lfunc_begin4-Lfunc_begin4      ## >> Call Site 1 <<
	.long	Lset84
Lset85 = Ltmp128-Lfunc_begin4           ##   Call between Lfunc_begin4 and Ltmp128
	.long	Lset85
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp128-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset86
Lset87 = Ltmp131-Ltmp128                ##   Call between Ltmp128 and Ltmp131
	.long	Lset87
Lset88 = Ltmp132-Lfunc_begin4           ##     jumps to Ltmp132
	.long	Lset88
	.byte	0                       ##   On action: cleanup
Lset89 = Ltmp131-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset89
Lset90 = Lfunc_end4-Ltmp131             ##   Call between Ltmp131 and Lfunc_end4
	.long	Lset90
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN6Square6rotateEv
	.weak_def_can_be_hidden	__ZN6Square6rotateEv
	.align	4, 0x90
__ZN6Square6rotateEv:                   ## @_ZN6Square6rotateEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp139:
	.cfi_def_cfa_offset 16
Ltmp140:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp141:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Square4spinEv
	.weak_def_can_be_hidden	__ZN6Square4spinEv
	.align	4, 0x90
__ZN6Square4spinEv:                     ## @_ZN6Square4spinEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp142:
	.cfi_def_cfa_offset 16
Ltmp143:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp144:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Circle6rotateEv
	.weak_def_can_be_hidden	__ZN6Circle6rotateEv
	.align	4, 0x90
__ZN6Circle6rotateEv:                   ## @_ZN6Circle6rotateEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp145:
	.cfi_def_cfa_offset 16
Ltmp146:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp147:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN6Circle4spinEv
	.weak_def_can_be_hidden	__ZN6Circle4spinEv
	.align	4, 0x90
__ZN6Circle4spinEv:                     ## @_ZN6Circle4spinEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp148:
	.cfi_def_cfa_offset 16
Ltmp149:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp150:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp151:
	.cfi_def_cfa_offset 16
Ltmp152:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp153:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB10_3
## BB#1:
	leaq	LJTI10_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB10_4:
	popq	%rbp
	retq
LBB10_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
LBB10_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L10_0_set_4 = LBB10_4-LJTI10_0
L10_0_set_2 = LBB10_2-LJTI10_0
LJTI10_0:
	.long	L10_0_set_4
	.long	L10_0_set_4
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2
	.long	L10_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp154:
	.cfi_def_cfa_offset 16
Ltmp155:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp156:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp157:
	.cfi_def_cfa_offset 16
Ltmp158:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp159:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant13forced_returnIvEET_v
	.weak_def_can_be_hidden	__ZN5boost6detail7variant13forced_returnIvEET_v
	.align	4, 0x90
__ZN5boost6detail7variant13forced_returnIvEET_v: ## @_ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp160:
	.cfi_def_cfa_offset 16
Ltmp161:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp162:
	.cfi_def_cfa_register %rbp
	leaq	L___func__._ZN5boost6detail7variant13forced_returnIvEET_v(%rip), %rdi
	leaq	L_.str.9(%rip), %rsi
	leaq	L_.str.10(%rip), %rcx
	movl	$49, %edx
	callq	___assert_rtn
	.cfi_endproc

	.globl	__ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_: ## @_ZNSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE21__push_back_slow_pathIS2_EEvOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp163:
	.cfi_def_cfa_offset 16
Ltmp164:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp165:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp166:
	.cfi_offset %rbx, -56
Ltmp167:
	.cfi_offset %r12, -48
Ltmp168:
	.cfi_offset %r13, -40
Ltmp169:
	.cfi_offset %r14, -32
Ltmp170:
	.cfi_offset %r15, -24
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	(%r12), %rdx
	movq	8(%r12), %rbx
	subq	%rdx, %rbx
	sarq	$3, %rbx
	incq	%rbx
	movq	%rbx, %rax
	shrq	$61, %rax
	je	LBB14_2
## BB#1:
	movq	%r12, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
	movq	(%r12), %rdx
LBB14_2:
	movq	16(%r12), %r14
	subq	%rdx, %r14
	movq	%r14, %rax
	sarq	$3, %rax
	movabsq	$1152921504606846975, %rcx ## imm = 0xFFFFFFFFFFFFFFF
	cmpq	%rcx, %rax
	jae	LBB14_3
## BB#4:                                ## %_ZNKSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE11__recommendEm.exit
	movq	%r15, -48(%rbp)         ## 8-byte Spill
	sarq	$2, %r14
	cmpq	%rbx, %r14
	cmovbq	%rbx, %r14
	movq	8(%r12), %r15
	movq	%r15, %r13
	subq	%rdx, %r13
	sarq	$3, %r13
	xorl	%ecx, %ecx
	testq	%r14, %r14
	movl	$0, %eax
	jne	LBB14_5
	jmp	LBB14_6
LBB14_3:                                ## %_ZNKSt3__16vectorIP6IShapeNS_9allocatorIS2_EEE11__recommendEm.exit.thread
	movq	%r15, -48(%rbp)         ## 8-byte Spill
	movabsq	$2305843009213693951, %r14 ## imm = 0x1FFFFFFFFFFFFFFF
	movq	8(%r12), %r15
	movq	%r15, %r13
	subq	%rdx, %r13
	sarq	$3, %r13
LBB14_5:
	leaq	(,%r14,8), %rdi
	movq	%rdx, %rbx
	callq	__Znwm
	movq	%rbx, %rdx
	movq	%r14, %rcx
LBB14_6:
	leaq	(%rax,%r13,8), %rbx
	leaq	(%rax,%rcx,8), %rsi
	movq	-48(%rbp), %rcx         ## 8-byte Reload
	movq	(%rcx), %rcx
	movq	%rcx, (%rax,%r13,8)
	leaq	8(%rax,%r13,8), %r13
	subq	%rdx, %r15
	subq	%r15, %rbx
	testq	%r15, %r15
	jle	LBB14_8
## BB#7:
	movq	%rbx, %rdi
	movq	%rdx, %r14
	movq	%rsi, -48(%rbp)         ## 8-byte Spill
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_memcpy
	movq	-48(%rbp), %rsi         ## 8-byte Reload
	movq	%r14, %rdx
LBB14_8:                                ## %_ZNSt3__114__split_bufferIP6IShapeRNS_9allocatorIS2_EEE5clearEv.exit.i.i
	movq	%rbx, (%r12)
	movq	%r13, 8(%r12)
	movq	%rsi, 16(%r12)
	testq	%rdx, %rdx
	je	LBB14_9
## BB#10:
	movq	%rdx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB14_9:                                ## %_ZNSt3__114__split_bufferIP6IShapeRNS_9allocatorIS2_EEED1Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp192:
	.cfi_def_cfa_offset 16
Ltmp193:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp194:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp195:
	.cfi_offset %rbx, -56
Ltmp196:
	.cfi_offset %r12, -48
Ltmp197:
	.cfi_offset %r13, -40
Ltmp198:
	.cfi_offset %r14, -32
Ltmp199:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp171:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp172:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB15_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB15_7
## BB#3:
Ltmp174:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp175:
## BB#4:                                ## %.noexc
Ltmp176:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp177:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp178:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp179:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB15_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp181:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp182:
## BB#8:
	testq	%rax, %rax
	jne	LBB15_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp183:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp184:
LBB15_10:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB15_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB15_11:
Ltmp185:
	movq	%rax, %r14
	jmp	LBB15_12
LBB15_20:
Ltmp173:
	movq	%rax, %r14
	jmp	LBB15_13
LBB15_19:
Ltmp180:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB15_12:                               ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB15_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp186:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp187:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB15_15
LBB15_16:
Ltmp188:
	movq	%rax, %rbx
Ltmp189:
	callq	___cxa_end_catch
Ltmp190:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB15_18:
Ltmp191:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table15:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset91 = Ltmp171-Lfunc_begin5           ## >> Call Site 1 <<
	.long	Lset91
Lset92 = Ltmp172-Ltmp171                ##   Call between Ltmp171 and Ltmp172
	.long	Lset92
Lset93 = Ltmp173-Lfunc_begin5           ##     jumps to Ltmp173
	.long	Lset93
	.byte	1                       ##   On action: 1
Lset94 = Ltmp174-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset94
Lset95 = Ltmp175-Ltmp174                ##   Call between Ltmp174 and Ltmp175
	.long	Lset95
Lset96 = Ltmp185-Lfunc_begin5           ##     jumps to Ltmp185
	.long	Lset96
	.byte	1                       ##   On action: 1
Lset97 = Ltmp176-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset97
Lset98 = Ltmp179-Ltmp176                ##   Call between Ltmp176 and Ltmp179
	.long	Lset98
Lset99 = Ltmp180-Lfunc_begin5           ##     jumps to Ltmp180
	.long	Lset99
	.byte	1                       ##   On action: 1
Lset100 = Ltmp181-Lfunc_begin5          ## >> Call Site 4 <<
	.long	Lset100
Lset101 = Ltmp184-Ltmp181               ##   Call between Ltmp181 and Ltmp184
	.long	Lset101
Lset102 = Ltmp185-Lfunc_begin5          ##     jumps to Ltmp185
	.long	Lset102
	.byte	1                       ##   On action: 1
Lset103 = Ltmp184-Lfunc_begin5          ## >> Call Site 5 <<
	.long	Lset103
Lset104 = Ltmp186-Ltmp184               ##   Call between Ltmp184 and Ltmp186
	.long	Lset104
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset105 = Ltmp186-Lfunc_begin5          ## >> Call Site 6 <<
	.long	Lset105
Lset106 = Ltmp187-Ltmp186               ##   Call between Ltmp186 and Ltmp187
	.long	Lset106
Lset107 = Ltmp188-Lfunc_begin5          ##     jumps to Ltmp188
	.long	Lset107
	.byte	0                       ##   On action: cleanup
Lset108 = Ltmp187-Lfunc_begin5          ## >> Call Site 7 <<
	.long	Lset108
Lset109 = Ltmp189-Ltmp187               ##   Call between Ltmp187 and Ltmp189
	.long	Lset109
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset110 = Ltmp189-Lfunc_begin5          ## >> Call Site 8 <<
	.long	Lset110
Lset111 = Ltmp190-Ltmp189               ##   Call between Ltmp189 and Ltmp190
	.long	Lset111
Lset112 = Ltmp191-Lfunc_begin5          ##     jumps to Ltmp191
	.long	Lset112
	.byte	1                       ##   On action: 1
Lset113 = Ltmp190-Lfunc_begin5          ## >> Call Site 9 <<
	.long	Lset113
Lset114 = Lfunc_end5-Ltmp190            ##   Call between Ltmp190 and Lfunc_end5
	.long	Lset114
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp203:
	.cfi_def_cfa_offset 16
Ltmp204:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp205:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp206:
	.cfi_offset %rbx, -56
Ltmp207:
	.cfi_offset %r12, -48
Ltmp208:
	.cfi_offset %r13, -40
Ltmp209:
	.cfi_offset %r14, -32
Ltmp210:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB16_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB16_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB16_9
LBB16_3:
	testq	%rbx, %rbx
	jle	LBB16_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp200:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp201:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB16_9
LBB16_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB16_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB16_9
LBB16_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB16_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB16_10:
Ltmp202:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table16:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset115 = Lfunc_begin6-Lfunc_begin6     ## >> Call Site 1 <<
	.long	Lset115
Lset116 = Ltmp200-Lfunc_begin6          ##   Call between Lfunc_begin6 and Ltmp200
	.long	Lset116
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset117 = Ltmp200-Lfunc_begin6          ## >> Call Site 2 <<
	.long	Lset117
Lset118 = Ltmp201-Ltmp200               ##   Call between Ltmp200 and Ltmp201
	.long	Lset118
Lset119 = Ltmp202-Lfunc_begin6          ##     jumps to Ltmp202
	.long	Lset119
	.byte	0                       ##   On action: cleanup
Lset120 = Ltmp201-Lfunc_begin6          ## >> Call Site 3 <<
	.long	Lset120
Lset121 = Lfunc_end6-Ltmp201            ##   Call between Ltmp201 and Lfunc_end6
	.long	Lset121
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
	.weak_def_can_be_hidden	__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
	.align	4, 0x90
__ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_: ## @_ZNSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE21__push_back_slow_pathIS5_EEvOT_
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp220:
	.cfi_def_cfa_offset 16
Ltmp221:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp222:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp223:
	.cfi_offset %rbx, -56
Ltmp224:
	.cfi_offset %r12, -48
Ltmp225:
	.cfi_offset %r13, -40
Ltmp226:
	.cfi_offset %r14, -32
Ltmp227:
	.cfi_offset %r15, -24
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	(%r12), %rax
	movq	8(%r12), %rbx
	subq	%rax, %rbx
	sarq	$3, %rbx
	incq	%rbx
	movq	%rbx, %rcx
	shrq	$61, %rcx
	je	LBB17_2
## BB#1:
	movq	%r12, %rdi
	callq	__ZNKSt3__120__vector_base_commonILb1EE20__throw_length_errorEv
	movq	(%r12), %rax
LBB17_2:
	movq	16(%r12), %r13
	subq	%rax, %r13
	movq	%r13, %rcx
	sarq	$3, %rcx
	movabsq	$1152921504606846975, %rdx ## imm = 0xFFFFFFFFFFFFFFF
	cmpq	%rdx, %rcx
	jae	LBB17_3
## BB#4:                                ## %_ZNKSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE11__recommendEm.exit
	sarq	$2, %r13
	cmpq	%rbx, %r13
	cmovbq	%rbx, %r13
	movq	8(%r12), %r14
	subq	%rax, %r14
	sarq	$3, %r14
	xorl	%eax, %eax
	testq	%r13, %r13
	movl	$0, %ebx
	jne	LBB17_5
	jmp	LBB17_6
LBB17_3:                                ## %_ZNKSt3__16vectorIN5boost7variantI7TSquareJ7TCircleEEENS_9allocatorIS5_EEE11__recommendEm.exit.thread
	movabsq	$2305843009213693951, %r13 ## imm = 0x1FFFFFFFFFFFFFFF
	movq	8(%r12), %r14
	subq	%rax, %r14
	sarq	$3, %r14
LBB17_5:
	leaq	(,%r13,8), %rdi
	callq	__Znwm
	movq	%rax, %rbx
	movq	%r13, %rax
LBB17_6:                                ## %_ZNSt3__114__split_bufferIN5boost7variantI7TSquareJ7TCircleEEERNS_9allocatorIS5_EEEC1EmmS8_.exit
	movq	%rax, -56(%rbp)         ## 8-byte Spill
	leaq	4(%rbx,%r14,8), %rax
	movq	%rax, -48(%rbp)
	movl	(%r15), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	4(%r15), %rcx
Ltmp211:
	leaq	-48(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp212:
## BB#7:
	leaq	(%rbx,%r14,8), %rdx
	movq	%rbx, -64(%rbp)         ## 8-byte Spill
	movl	(%r15), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%eax, %ecx
	movl	%ecx, (%rdx)
	movq	(%r12), %rbx
	movq	8(%r12), %r14
	cmpq	%rbx, %r14
	je	LBB17_8
## BB#9:                                ## %.lr.ph.i.i
	leaq	-48(%rbp), %r15
	movq	%rdx, %r13
	movq	%rdx, -72(%rbp)         ## 8-byte Spill
	.align	4, 0x90
LBB17_10:                               ## =>This Inner Loop Header: Depth=1
	leaq	-4(%r13), %rax
	movq	%rax, -48(%rbp)
	movl	-8(%r14), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-4(%r14), %rcx
Ltmp214:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp215:
## BB#11:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE9constructIS6_JS6_EEEvRS7_PT_DpOT0_.exit.i.i
                                        ##   in Loop: Header=BB17_10 Depth=1
	movl	-8(%r14), %eax
	addq	$-8, %r14
	movl	%eax, %ecx
	sarl	$31, %ecx
	xorl	%eax, %ecx
	movl	%ecx, -8(%r13)
	addq	$-8, %r13
	cmpq	%r14, %rbx
	jne	LBB17_10
## BB#12:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE20__construct_backwardIPS6_EEvRS7_T_SC_RSC_.exit.loopexit.i
	movq	(%r12), %r15
	movq	8(%r12), %rbx
	movq	-56(%rbp), %rax         ## 8-byte Reload
	movq	-72(%rbp), %rdx         ## 8-byte Reload
	jmp	LBB17_13
LBB17_8:                                ## %._ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE20__construct_backwardIPS6_EEvRS7_T_SC_RSC_.exit_crit_edge.i
	movq	%rdx, %r13
	movq	%rbx, %r15
	movq	-56(%rbp), %rax         ## 8-byte Reload
LBB17_13:
	movq	-64(%rbp), %rcx         ## 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	addq	$8, %rdx
	movq	%r13, (%r12)
	movq	%rdx, 8(%r12)
	movq	%rax, 16(%r12)
	cmpq	%r15, %rbx
	je	LBB17_17
## BB#14:                               ## %.lr.ph.i.i.i.i.i
	leaq	-48(%rbp), %r14
	.align	4, 0x90
LBB17_15:                               ## =>This Inner Loop Header: Depth=1
	movl	-8(%rbx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-4(%rbx), %rcx
Ltmp217:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp218:
## BB#16:                               ## %_ZNSt3__116allocator_traitsINS_9allocatorIN5boost7variantI7TSquareJ7TCircleEEEEEE7destroyIS6_EEvRS7_PT_.exit.i.i.i.i.i
                                        ##   in Loop: Header=BB17_15 Depth=1
	addq	$-8, %rbx
	cmpq	%rbx, %r15
	jne	LBB17_15
LBB17_17:                               ## %_ZNSt3__114__split_bufferIN5boost7variantI7TSquareJ7TCircleEEERNS_9allocatorIS5_EEE5clearEv.exit.i.i
	testq	%r15, %r15
	je	LBB17_19
## BB#18:
	movq	%r15, %rdi
	callq	__ZdlPv
LBB17_19:                               ## %_ZNSt3__114__split_bufferIN5boost7variantI7TSquareJ7TCircleEEERNS_9allocatorIS5_EEED1Ev.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB17_21:
Ltmp216:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB17_22:
Ltmp219:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB17_20:
Ltmp213:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table17:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset122 = Lfunc_begin7-Lfunc_begin7     ## >> Call Site 1 <<
	.long	Lset122
Lset123 = Ltmp211-Lfunc_begin7          ##   Call between Lfunc_begin7 and Ltmp211
	.long	Lset123
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset124 = Ltmp211-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset124
Lset125 = Ltmp212-Ltmp211               ##   Call between Ltmp211 and Ltmp212
	.long	Lset125
Lset126 = Ltmp213-Lfunc_begin7          ##     jumps to Ltmp213
	.long	Lset126
	.byte	1                       ##   On action: 1
Lset127 = Ltmp214-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset127
Lset128 = Ltmp215-Ltmp214               ##   Call between Ltmp214 and Ltmp215
	.long	Lset128
Lset129 = Ltmp216-Lfunc_begin7          ##     jumps to Ltmp216
	.long	Lset129
	.byte	1                       ##   On action: 1
Lset130 = Ltmp217-Lfunc_begin7          ## >> Call Site 4 <<
	.long	Lset130
Lset131 = Ltmp218-Ltmp217               ##   Call between Ltmp217 and Ltmp218
	.long	Lset131
Lset132 = Ltmp219-Lfunc_begin7          ##     jumps to Ltmp219
	.long	Lset132
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9move_intoEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp228:
	.cfi_def_cfa_offset 16
Ltmp229:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp230:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB18_3
## BB#1:
	leaq	LJTI18_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB18_4:
	movq	(%rdx), %rcx
	movl	(%rax), %eax
	movl	%eax, (%rcx)
	popq	%rbp
	retq
LBB18_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
LBB18_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L18_0_set_4 = LBB18_4-LJTI18_0
L18_0_set_2 = LBB18_2-LJTI18_0
LJTI18_0:
	.long	L18_0_set_4
	.long	L18_0_set_4
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2
	.long	L18_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp231:
	.cfi_def_cfa_offset 16
Ltmp232:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp233:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9move_intoEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp234:
	.cfi_def_cfa_offset 16
Ltmp235:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp236:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_9destroyerEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSQ_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp237:
	.cfi_def_cfa_offset 16
Ltmp238:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp239:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB21_3
## BB#1:
	leaq	LJTI21_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB21_4:
	popq	%rbp
	retq
LBB21_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
LBB21_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L21_0_set_4 = LBB21_4-LJTI21_0
L21_0_set_2 = LBB21_2-LJTI21_0
LJTI21_0:
	.long	L21_0_set_4
	.long	L21_0_set_4
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2
	.long	L21_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSA_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp240:
	.cfi_def_cfa_offset 16
Ltmp241:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp242:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSJ_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp243:
	.cfi_def_cfa_offset 16
Ltmp244:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp245:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSO_ISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSY_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp246:
	.cfi_def_cfa_offset 16
Ltmp247:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp248:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB24_4
## BB#1:
	leaq	LJTI24_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB24_5:
	movq	(%rdx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	8(%rcx), %rcx
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movl	(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$4, %rcx
	leaq	-8(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	jmp	LBB24_6
LBB24_2:
	movq	(%rdx), %rcx
	movq	(%rcx), %rdx
	movq	%rdx, -24(%rbp)
	movq	%rax, -16(%rbp)
	movq	8(%rcx), %rcx
	leaq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movl	(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$4, %rcx
	leaq	-8(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
LBB24_6:
	addq	$32, %rsp
	popq	%rbp
	retq
LBB24_3:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNS6_I7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSI_T0_PNS1_22apply_visitor_unrolledET1_l
LBB24_4:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSF_I7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSR_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L24_0_set_5 = LBB24_5-LJTI24_0
L24_0_set_2 = LBB24_2-LJTI24_0
L24_0_set_3 = LBB24_3-LJTI24_0
LJTI24_0:
	.long	L24_0_set_5
	.long	L24_0_set_2
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3
	.long	L24_0_set_3

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNS6_I7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSI_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNS6_I7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSI_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNS6_I7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSI_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNS6_I7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSI_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp249:
	.cfi_def_cfa_offset 16
Ltmp250:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp251:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSF_I7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSR_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSF_I7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSR_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSF_I7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSR_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_unwrapI12MultiVisitorKNS_7variantI3ADDJ3DELEEEEEEEPvNSF_I7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSR_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp252:
	.cfi_def_cfa_offset 16
Ltmp253:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp254:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp255:
	.cfi_def_cfa_offset 16
Ltmp256:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp257:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB27_3
## BB#1:
	leaq	LJTI27_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB27_4:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	incl	(%rax)
	popq	%rbp
	retq
LBB27_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
LBB27_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L27_0_set_4 = LBB27_4-LJTI27_0
L27_0_set_2 = LBB27_2-LJTI27_0
LJTI27_0:
	.long	L27_0_set_4
	.long	L27_0_set_4
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2
	.long	L27_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp258:
	.cfi_def_cfa_offset 16
Ltmp259:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp260:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TSquareEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp261:
	.cfi_def_cfa_offset 16
Ltmp262:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp263:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE3ADDNS9_INSA_ILl1EEE3DELNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSW_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp264:
	.cfi_def_cfa_offset 16
Ltmp265:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp266:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB30_3
## BB#1:
	leaq	LJTI30_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB30_4:
	movq	(%rdx), %rax
	movq	8(%rax), %rax
	incl	(%rax)
	popq	%rbp
	retq
LBB30_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
LBB30_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L30_0_set_4 = LBB30_4-LJTI30_0
L30_0_set_2 = LBB30_2-LJTI30_0
LJTI30_0:
	.long	L30_0_set_4
	.long	L30_0_set_4
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2
	.long	L30_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT_11result_typeEiRSG_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp267:
	.cfi_def_cfa_offset 16
Ltmp268:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp269:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorINS1_27apply_visitor_binary_invokeI12MultiVisitor7TCircleEEEEPKvNS_7variantI3ADDJ3DELEE18has_fallback_type_EEENT1_11result_typeEiiRSP_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp270:
	.cfi_def_cfa_offset 16
Ltmp271:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp272:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp273:
	.cfi_def_cfa_offset 16
Ltmp274:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp275:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB33_3
## BB#1:
	leaq	LJTI33_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB33_4:
	incl	(%rax)
	popq	%rbp
	retq
LBB33_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
LBB33_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L33_0_set_4 = LBB33_4-LJTI33_0
L33_0_set_2 = LBB33_2-LJTI33_0
LJTI33_0:
	.long	L33_0_set_4
	.long	L33_0_set_4
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2
	.long	L33_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp276:
	.cfi_def_cfa_offset 16
Ltmp277:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp278:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI13RotateVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp279:
	.cfi_def_cfa_offset 16
Ltmp280:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp281:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEE7TSquareNS9_INSA_ILl1EEE7TCircleNS7_5l_endEEEEEEENS8_ISF_EEEENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantISC_JSE_EE18has_fallback_type_EEENT1_11result_typeEiiRSS_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp282:
	.cfi_def_cfa_offset 16
Ltmp283:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp284:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB36_3
## BB#1:
	leaq	LJTI36_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB36_4:
	incl	(%rax)
	popq	%rbp
	retq
LBB36_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
LBB36_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L36_0_set_4 = LBB36_4-LJTI36_0
L36_0_set_2 = LBB36_2-LJTI36_0
LJTI36_0:
	.long	L36_0_set_4
	.long	L36_0_set_4
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2
	.long	L36_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT_11result_typeEiRSC_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp285:
	.cfi_def_cfa_offset 16
Ltmp286:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp287:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_14invoke_visitorI11SpinVisitorEEPvNS_7variantI7TSquareJ7TCircleEE18has_fallback_type_EEENT1_11result_typeEiiRSL_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp288:
	.cfi_def_cfa_offset 16
Ltmp289:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp290:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
__GLOBAL__sub_I_perftest.cpp:           ## @_GLOBAL__sub_I_perftest.cpp
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp291:
	.cfi_def_cfa_offset 16
Ltmp292:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp293:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp294:
	.cfi_offset %rbx, -32
Ltmp295:
	.cfi_offset %r14, -24
	callq	__ZN5boost6system16generic_categoryEv
	movq	%rax, __ZN5boost6systemL14posix_categoryE(%rip)
	callq	__ZN5boost6system16generic_categoryEv
	movq	%rax, __ZN5boost6systemL10errno_ecatE(%rip)
	callq	__ZN5boost6system15system_categoryEv
	movq	%rax, __ZN5boost6systemL11native_ecatE(%rip)
	movl	$0, __ZL3add(%rip)
	leaq	__ZL3add(%rip), %rsi
	movq	__ZN5boost7variantI3ADDJ3DELEED1Ev@GOTPCREL(%rip), %rbx
	movq	___dso_handle@GOTPCREL(%rip), %r14
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	___cxa_atexit
	movl	$1, __ZL3del(%rip)
	leaq	__ZL3del(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	___cxa_atexit           ## TAILCALL
	.cfi_endproc

.zerofill __DATA,__bss,__ZN5boost6systemL14posix_categoryE,8,3 ## @_ZN5boost6systemL14posix_categoryE
.zerofill __DATA,__bss,__ZN5boost6systemL10errno_ecatE,8,3 ## @_ZN5boost6systemL10errno_ecatE
.zerofill __DATA,__bss,__ZN5boost6systemL11native_ecatE,8,3 ## @_ZN5boost6systemL11native_ecatE
.zerofill __DATA,__bss,__ZL3add,8,2     ## @_ZL3add
.zerofill __DATA,__bss,__ZL3del,8,2     ## @_ZL3del
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"make_virtual_shapes took "

L_.str.5:                               ## @.str.5
	.asciz	" millis\n"

L_.str.6:                               ## @.str.6
	.asciz	"make_template_shapes took "

L_.str.7:                               ## @.str.7
	.asciz	"make_template_shapes_single took "

L_.str.8:                               ## @.str.8
	.asciz	"Hello, cmake"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTV6Square            ## @_ZTV6Square
	.weak_def_can_be_hidden	__ZTV6Square
	.align	3
__ZTV6Square:
	.quad	0
	.quad	__ZTI6Square
	.quad	__ZN6Square6rotateEv
	.quad	__ZN6Square4spinEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS6Square            ## @_ZTS6Square
	.weak_definition	__ZTS6Square
__ZTS6Square:
	.asciz	"6Square"

	.globl	__ZTS6IShape            ## @_ZTS6IShape
	.weak_definition	__ZTS6IShape
__ZTS6IShape:
	.asciz	"6IShape"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI6IShape            ## @_ZTI6IShape
	.weak_definition	__ZTI6IShape
	.align	3
__ZTI6IShape:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTS6IShape

	.globl	__ZTI6Square            ## @_ZTI6Square
	.weak_definition	__ZTI6Square
	.align	4
__ZTI6Square:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS6Square
	.quad	__ZTI6IShape

	.globl	__ZTV6Circle            ## @_ZTV6Circle
	.weak_def_can_be_hidden	__ZTV6Circle
	.align	3
__ZTV6Circle:
	.quad	0
	.quad	__ZTI6Circle
	.quad	__ZN6Circle6rotateEv
	.quad	__ZN6Circle4spinEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTS6Circle            ## @_ZTS6Circle
	.weak_definition	__ZTS6Circle
__ZTS6Circle:
	.asciz	"6Circle"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTI6Circle            ## @_ZTI6Circle
	.weak_definition	__ZTI6Circle
	.align	4
__ZTI6Circle:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTS6Circle
	.quad	__ZTI6IShape

	.section	__TEXT,__cstring,cstring_literals
L___func__._ZN5boost6detail7variant13forced_returnIvEET_v: ## @__func__._ZN5boost6detail7variant13forced_returnIvEET_v
	.asciz	"forced_return"

L_.str.9:                               ## @.str.9
	.asciz	"/Users/rhodges/local/include/boost/variant/detail/forced_return.hpp"

L_.str.10:                              ## @.str.10
	.asciz	"false"

	.section	__DATA,__mod_init_func,mod_init_funcs
	.align	3
	.quad	__GLOBAL__sub_I_perftest.cpp

.subsections_via_symbols
