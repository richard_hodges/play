	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z10get_resultv
	.align	4, 0x90
__Z10get_resultv:                       ## @_Z10get_resultv
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp14:
	.cfi_def_cfa_offset 16
Ltmp15:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp16:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$328, %rsp              ## imm = 0x148
Ltmp17:
	.cfi_offset %rbx, -56
Ltmp18:
	.cfi_offset %r12, -48
Ltmp19:
	.cfi_offset %r13, -40
Ltmp20:
	.cfi_offset %r14, -32
Ltmp21:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -352(%rbp)
	movq	$0, -336(%rbp)
	leaq	L_.str(%rip), %rsi
	leaq	-352(%rbp), %rdi
	movl	$5, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
	leaq	-200(%rbp), %rdi
	leaq	-304(%rbp), %r15
	movq	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE@GOTPCREL(%rip), %rax
	leaq	64(%rax), %rcx
	movq	%rcx, -200(%rbp)
	addq	$24, %rax
	movd	%rax, %xmm0
	movaps	%xmm0, -320(%rbp)
Ltmp0:
	movq	%r15, %rsi
	callq	__ZNSt3__18ios_base4initEPv
Ltmp1:
## BB#1:
	movq	$0, -64(%rbp)
	movl	$-1, -56(%rbp)
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r12
	leaq	24(%r12), %r14
	movq	%r14, -320(%rbp)
	addq	$64, %r12
	movq	%r12, -200(%rbp)
Ltmp3:
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp4:
## BB#2:                                ## %.noexc.i
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r13
	addq	$16, %r13
	movq	%r13, -304(%rbp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	movaps	%xmm0, -240(%rbp)
	movl	$8, -208(%rbp)
Ltmp6:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp7:
## BB#3:                                ## %_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKNS_12basic_stringIcS2_S4_EEj.exit
	leaq	-352(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	$0, 24(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
Ltmp9:
	leaq	-320(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
Ltmp10:
## BB#4:
	leaq	24(%rbx), %rsi
Ltmp11:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEErsERi
Ltmp12:
## BB#5:
	movq	%r14, -320(%rbp)
	movq	%r12, -200(%rbp)
	movq	%r13, -304(%rbp)
	leaq	-240(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	-320(%rbp), %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	leaq	-200(%rbp), %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rax
	addq	$328, %rsp              ## imm = 0x148
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_11:
Ltmp13:
	movq	%rax, -360(%rbp)        ## 8-byte Spill
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r14, -320(%rbp)
	movq	%r12, -200(%rbp)
	movq	%r13, -304(%rbp)
	leaq	-240(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	-320(%rbp), %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	leaq	-200(%rbp), %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-360(%rbp), %rdi        ## 8-byte Reload
	callq	__Unwind_Resume
LBB0_9:
Ltmp2:
	movq	%rax, -360(%rbp)        ## 8-byte Spill
	jmp	LBB0_10
LBB0_7:
Ltmp5:
	movq	%rax, -360(%rbp)        ## 8-byte Spill
	jmp	LBB0_8
LBB0_6:
Ltmp8:
	movq	%rax, -360(%rbp)        ## 8-byte Spill
	leaq	-240(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
LBB0_8:                                 ## %.body.i
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	-320(%rbp), %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
LBB0_10:                                ## %.body
	leaq	-200(%rbp), %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	leaq	-352(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-360(%rbp), %rdi        ## 8-byte Reload
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp3-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset6
Lset7 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp6-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp7-Ltmp6                     ##   Call between Ltmp6 and Ltmp7
	.long	Lset9
Lset10 = Ltmp8-Lfunc_begin0             ##     jumps to Ltmp8
	.long	Lset10
	.byte	0                       ##   On action: cleanup
Lset11 = Ltmp9-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Ltmp12-Ltmp9                   ##   Call between Ltmp9 and Ltmp12
	.long	Lset12
Lset13 = Ltmp13-Lfunc_begin0            ##     jumps to Ltmp13
	.long	Lset13
	.byte	0                       ##   On action: cleanup
Lset14 = Ltmp12-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset14
Lset15 = Lfunc_end0-Ltmp12              ##   Call between Ltmp12 and Lfunc_end0
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1rsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_istreamIT_T0_EES9_RNS_12basic_stringIS6_S7_T1_EE
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
Ltmp50:
	.cfi_offset %rbx, -56
Ltmp51:
	.cfi_offset %r12, -48
Ltmp52:
	.cfi_offset %r13, -40
Ltmp53:
	.cfi_offset %r14, -32
Ltmp54:
	.cfi_offset %r15, -24
	movq	%rsi, %r14
	movq	%rdi, %r13
Ltmp22:
	leaq	-48(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEE6sentryC1ERS3_b
Ltmp23:
## BB#1:
	cmpb	$0, -48(%rbp)
	je	LBB1_24
## BB#2:
	testb	$1, (%r14)
	jne	LBB1_3
## BB#4:
	movw	$0, (%r14)
	jmp	LBB1_5
LBB1_24:
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	leaq	(%r13,%rax), %rdi
	movl	32(%r13,%rax), %esi
	orl	$4, %esi
Ltmp38:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp39:
	jmp	LBB1_15
LBB1_3:
	movq	16(%r14), %rax
	movb	$0, (%rax)
	movq	$0, 8(%r14)
LBB1_5:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5clearEv.exit
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	leaq	(%r13,%rax), %rsi
	movq	24(%r13,%rax), %rax
	testq	%rax, %rax
	movabsq	$9223372036854775807, %r12 ## imm = 0x7FFFFFFFFFFFFFFF
	cmovgq	%rax, %r12
Ltmp24:
	leaq	-56(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp25:
## BB#6:
Ltmp26:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-56(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
	movq	%rax, %r15
Ltmp27:
## BB#7:
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	xorl	%ebx, %ebx
	testq	%r12, %r12
	movl	$0, %ecx
	jle	LBB1_23
## BB#8:                                ## %.lr.ph
	xorl	%ebx, %ebx
	.align	4, 0x90
LBB1_9:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	movq	40(%r13,%rax), %rdi
	movq	24(%rdi), %rax
	cmpq	32(%rdi), %rax
	je	LBB1_10
## BB#16:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sgetcEv.exit.thread
                                        ##   in Loop: Header=BB1_9 Depth=1
	movzbl	(%rax), %eax
	jmp	LBB1_17
	.align	4, 0x90
LBB1_10:                                ##   in Loop: Header=BB1_9 Depth=1
	movq	(%rdi), %rax
	movq	72(%rax), %rax
Ltmp29:
	callq	*%rax
Ltmp30:
## BB#11:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sgetcEv.exit
                                        ##   in Loop: Header=BB1_9 Depth=1
	movl	$2, %ecx
	cmpl	$-1, %eax
	je	LBB1_23
LBB1_17:                                ##   in Loop: Header=BB1_9 Depth=1
	testb	%al, %al
	js	LBB1_19
## BB#18:                               ## %_ZNKSt3__15ctypeIcE2isEjc.exit
                                        ##   in Loop: Header=BB1_9 Depth=1
	movsbq	%al, %rdx
	movq	16(%r15), %rsi
	xorl	%ecx, %ecx
	testb	$64, 1(%rsi,%rdx,4)
	jne	LBB1_23
LBB1_19:                                ## %_ZNKSt3__15ctypeIcE2isEjc.exit.thread
                                        ##   in Loop: Header=BB1_9 Depth=1
Ltmp31:
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp32:
## BB#20:                               ##   in Loop: Header=BB1_9 Depth=1
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	movq	40(%r13,%rax), %rdi
	movq	24(%rdi), %rax
	cmpq	32(%rdi), %rax
	je	LBB1_21
## BB#30:                               ##   in Loop: Header=BB1_9 Depth=1
	incq	%rax
	movq	%rax, 24(%rdi)
	jmp	LBB1_22
	.align	4, 0x90
LBB1_21:                                ##   in Loop: Header=BB1_9 Depth=1
	movq	(%rdi), %rax
	movq	80(%rax), %rax
Ltmp33:
	callq	*%rax
Ltmp34:
LBB1_22:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv.exit.thread.backedge
                                        ##   in Loop: Header=BB1_9 Depth=1
	incq	%rbx
	xorl	%ecx, %ecx
	cmpq	%r12, %rbx
	jl	LBB1_9
LBB1_23:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6sbumpcEv.exit
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	movq	$0, 24(%r13,%rax)
	movl	%ecx, %esi
	orl	$4, %esi
	testq	%rbx, %rbx
	cmovnel	%ecx, %esi
	movq	(%r13), %rax
	movq	-24(%rax), %rax
	leaq	(%r13,%rax), %rdi
	orl	32(%r13,%rax), %esi
Ltmp36:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp37:
	jmp	LBB1_15
LBB1_29:                                ## %.loopexit.split-lp
Ltmp40:
	movq	%rax, %r14
	jmp	LBB1_13
LBB1_28:                                ## %.loopexit
Ltmp35:
	movq	%rax, %r14
LBB1_13:
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%r13), %rax
	movq	%r13, %rdi
	addq	-24(%rax), %rdi
Ltmp41:
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp42:
## BB#14:
	callq	___cxa_end_catch
LBB1_15:
	movq	%r13, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB1_12:
Ltmp28:
	movq	%rax, %r14
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB1_13
LBB1_25:
Ltmp43:
	movq	%rax, %rbx
Ltmp44:
	callq	___cxa_end_catch
Ltmp45:
## BB#26:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB1_27:
Ltmp46:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset16 = Ltmp22-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp25-Ltmp22                  ##   Call between Ltmp22 and Ltmp25
	.long	Lset17
Lset18 = Ltmp40-Lfunc_begin1            ##     jumps to Ltmp40
	.long	Lset18
	.byte	1                       ##   On action: 1
Lset19 = Ltmp26-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset19
Lset20 = Ltmp27-Ltmp26                  ##   Call between Ltmp26 and Ltmp27
	.long	Lset20
Lset21 = Ltmp28-Lfunc_begin1            ##     jumps to Ltmp28
	.long	Lset21
	.byte	1                       ##   On action: 1
Lset22 = Ltmp29-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset22
Lset23 = Ltmp34-Ltmp29                  ##   Call between Ltmp29 and Ltmp34
	.long	Lset23
Lset24 = Ltmp35-Lfunc_begin1            ##     jumps to Ltmp35
	.long	Lset24
	.byte	1                       ##   On action: 1
Lset25 = Ltmp36-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset25
Lset26 = Ltmp37-Ltmp36                  ##   Call between Ltmp36 and Ltmp37
	.long	Lset26
Lset27 = Ltmp40-Lfunc_begin1            ##     jumps to Ltmp40
	.long	Lset27
	.byte	1                       ##   On action: 1
Lset28 = Ltmp37-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset28
Lset29 = Ltmp41-Ltmp37                  ##   Call between Ltmp37 and Ltmp41
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp41-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset30
Lset31 = Ltmp42-Ltmp41                  ##   Call between Ltmp41 and Ltmp42
	.long	Lset31
Lset32 = Ltmp43-Lfunc_begin1            ##     jumps to Ltmp43
	.long	Lset32
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp42-Lfunc_begin1            ## >> Call Site 7 <<
	.long	Lset33
Lset34 = Ltmp44-Ltmp42                  ##   Call between Ltmp42 and Ltmp44
	.long	Lset34
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset35 = Ltmp44-Lfunc_begin1            ## >> Call Site 8 <<
	.long	Lset35
Lset36 = Ltmp45-Ltmp44                  ##   Call between Ltmp44 and Ltmp45
	.long	Lset36
Lset37 = Ltmp46-Lfunc_begin1            ##     jumps to Ltmp46
	.long	Lset37
	.byte	1                       ##   On action: 1
Lset38 = Ltmp45-Lfunc_begin1            ## >> Call Site 9 <<
	.long	Lset38
Lset39 = Lfunc_end1-Ltmp45              ##   Call between Ltmp45 and Lfunc_end1
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp55:
	.cfi_def_cfa_offset 16
Ltmp56:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp57:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp58:
	.cfi_offset %rbx, -32
Ltmp59:
	.cfi_offset %r14, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%rbx)
	addq	$64, %rax
	movq	%rax, 120(%rbx)
	leaq	16(%rbx), %r14
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	leaq	80(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r14, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	leaq	120(%rbx), %r14
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp89:
	.cfi_def_cfa_offset 16
Ltmp90:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp91:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$80, %rsp
Ltmp92:
	.cfi_offset %rbx, -32
Ltmp93:
	.cfi_offset %r14, -24
	leaq	-64(%rbp), %rdi
	callq	__Z10get_resultv
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	$0, -80(%rbp)
Ltmp60:
	leaq	L_.str.1(%rip), %rsi
	leaq	-96(%rbp), %rdi
	movl	$3, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp61:
## BB#1:                                ## %_ZNSt3__18literals15string_literalsli1sEPKcm.exit
	movzwl	-64(%rbp), %ecx
	movl	%ecx, %edx
	andl	$254, %edx
	shrq	%rdx
	movw	%cx, %ax
	andw	$1, %ax
	cmovneq	-56(%rbp), %rdx
	movzbl	-96(%rbp), %esi
	movq	%rsi, %rdi
	shrq	%rdi
	andb	$1, %sil
	cmovneq	-88(%rbp), %rdi
	cmpq	%rdi, %rdx
	jne	LBB3_8
## BB#2:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2.i
	testb	%sil, %sil
	leaq	-63(%rbp), %rdi
	leaq	-95(%rbp), %rsi
	cmovneq	-80(%rbp), %rsi
	testw	%ax, %ax
	cmovneq	-48(%rbp), %rdi
	movb	$1, %al
	jne	LBB3_9
## BB#3:                                ## %.preheader.i
	testq	%rdx, %rdx
	je	LBB3_11
## BB#4:                                ## %.lr.ph.i.preheader
	movl	%ecx, %edx
	shrl	$8, %edx
	movzbl	(%rsi), %edi
	movzbl	%dl, %edx
	cmpl	%edi, %edx
	jne	LBB3_8
## BB#5:                                ## %.lr.ph.preheader
	shrl	%ecx
	andl	$127, %ecx
	movl	$1, %edx
	subq	%rcx, %rdx
	leaq	-62(%rbp), %rcx
	incq	%rsi
	.align	4, 0x90
LBB3_6:                                 ## %.lr.ph
                                        ## =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	je	LBB3_11
## BB#7:                                ## %..lr.ph.i_crit_edge
                                        ##   in Loop: Header=BB3_6 Depth=1
	movzbl	(%rsi), %edi
	movzbl	(%rcx), %ebx
	incq	%rdx
	incq	%rcx
	incq	%rsi
	cmpl	%edi, %ebx
	je	LBB3_6
LBB3_8:
	xorl	%eax, %eax
	jmp	LBB3_11
LBB3_9:
	testq	%rdx, %rdx
	je	LBB3_11
## BB#10:
	callq	_memcmp
	testl	%eax, %eax
	sete	%al
LBB3_11:                                ## %.loopexit
	movzbl	%al, %ebx
	cmpl	$2, -40(%rbp)
	jne	LBB3_13
## BB#12:                               ## %select.false
	orl	$2, %ebx
LBB3_13:                                ## %select.end
	leaq	-96(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	testl	%ebx, %ebx
	je	LBB3_21
## BB#14:                               ## %select.end
	cmpl	$3, %ebx
	jne	LBB3_27
## BB#15:
Ltmp75:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movl	$3, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	movq	%rax, %rbx
Ltmp76:
## BB#16:
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp77:
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp78:
## BB#17:                               ## %.noexc15
Ltmp79:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-24(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp80:
## BB#18:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp81:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp82:
## BB#19:                               ## %.noexc8
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp84:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp85:
## BB#20:                               ## %.noexc9
Ltmp86:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp87:
	jmp	LBB3_27
LBB3_21:
Ltmp62:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	xorl	%esi, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	movq	%rax, %rbx
Ltmp63:
## BB#22:
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp64:
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp65:
## BB#23:                               ## %.noexc14
Ltmp66:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-32(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp67:
## BB#24:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp68:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r14b
Ltmp69:
## BB#25:                               ## %.noexc
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp71:
	movsbl	%r14b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp72:
## BB#26:                               ## %.noexc2
Ltmp73:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp74:
LBB3_27:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit7
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB3_28:
Ltmp88:
	movq	%rax, %rbx
	jmp	LBB3_29
LBB3_30:
Ltmp83:
	movq	%rax, %rbx
	leaq	-24(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB3_29
LBB3_31:
Ltmp70:
	movq	%rax, %rbx
	leaq	-32(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB3_29:
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	93                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	91                      ## Call site table length
Lset40 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset40
Lset41 = Ltmp60-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp60
	.long	Lset41
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset42 = Ltmp60-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset42
Lset43 = Ltmp78-Ltmp60                  ##   Call between Ltmp60 and Ltmp78
	.long	Lset43
Lset44 = Ltmp88-Lfunc_begin2            ##     jumps to Ltmp88
	.long	Lset44
	.byte	0                       ##   On action: cleanup
Lset45 = Ltmp79-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset45
Lset46 = Ltmp82-Ltmp79                  ##   Call between Ltmp79 and Ltmp82
	.long	Lset46
Lset47 = Ltmp83-Lfunc_begin2            ##     jumps to Ltmp83
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp84-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset48
Lset49 = Ltmp65-Ltmp84                  ##   Call between Ltmp84 and Ltmp65
	.long	Lset49
Lset50 = Ltmp88-Lfunc_begin2            ##     jumps to Ltmp88
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp66-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset51
Lset52 = Ltmp69-Ltmp66                  ##   Call between Ltmp66 and Ltmp69
	.long	Lset52
Lset53 = Ltmp70-Lfunc_begin2            ##     jumps to Ltmp70
	.long	Lset53
	.byte	0                       ##   On action: cleanup
Lset54 = Ltmp71-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset54
Lset55 = Ltmp74-Ltmp71                  ##   Call between Ltmp71 and Ltmp74
	.long	Lset55
Lset56 = Ltmp88-Lfunc_begin2            ##     jumps to Ltmp88
	.long	Lset56
	.byte	0                       ##   On action: cleanup
Lset57 = Ltmp74-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset57
Lset58 = Lfunc_end2-Ltmp74              ##   Call between Ltmp74 and Lfunc_end2
	.long	Lset58
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.globl	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp94:
	.cfi_def_cfa_offset 16
Ltmp95:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp96:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp97:
	.cfi_offset %rbx, -40
Ltmp98:
	.cfi_offset %r14, -32
Ltmp99:
	.cfi_offset %r15, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	leaq	(%rdi,%rax), %r15
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	leaq	24(%rcx), %rdx
	movq	%rdx, (%rdi,%rax)
	leaq	120(%rdi,%rax), %r14
	addq	$64, %rcx
	movq	%rcx, 120(%rdi,%rax)
	leaq	16(%rdi,%rax), %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 16(%rdi,%rax)
	leaq	80(%rdi,%rax), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%r15, %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp103:
	.cfi_offset %rbx, -40
Ltmp104:
	.cfi_offset %r14, -32
Ltmp105:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%rbx)
	leaq	120(%rbx), %r14
	addq	$64, %rax
	movq	%rax, 120(%rbx)
	leaq	16(%rbx), %r15
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	leaq	80(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%r15, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp106:
	.cfi_def_cfa_offset 16
Ltmp107:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp108:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp109:
	.cfi_offset %rbx, -40
Ltmp110:
	.cfi_offset %r14, -32
Ltmp111:
	.cfi_offset %r15, -24
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	leaq	(%rdi,%rax), %r15
	movq	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	leaq	24(%rcx), %rdx
	movq	%rdx, (%rdi,%rax)
	leaq	120(%rdi,%rax), %r14
	addq	$64, %rcx
	movq	%rcx, 120(%rdi,%rax)
	leaq	16(%rdi,%rax), %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 16(%rdi,%rax)
	leaq	80(%rdi,%rax), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	addq	$8, %rsi
	movq	%r15, %rdi
	callq	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED2Ev
	movq	%r14, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp112:
	.cfi_def_cfa_offset 16
Ltmp113:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp114:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp115:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp116:
	.cfi_def_cfa_offset 16
Ltmp117:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp118:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp119:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rbx)
	leaq	64(%rbx), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp120:
	.cfi_def_cfa_offset 16
Ltmp121:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp122:
	.cfi_def_cfa_register %rbp
	movq	48(%rsi), %r11
	movq	88(%rsi), %r9
	cmpq	%r11, %r9
	jae	LBB10_2
## BB#1:
	movq	%r11, 88(%rsi)
	movq	%r11, %r9
LBB10_2:
	movl	%r8d, %eax
	andl	$24, %eax
	je	LBB10_3
## BB#4:
	cmpl	$1, %ecx
	jne	LBB10_6
## BB#5:
	cmpl	$24, %eax
	je	LBB10_3
LBB10_6:
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	je	LBB10_15
## BB#7:
	cmpl	$2, %ecx
	je	LBB10_11
## BB#8:
	cmpl	$1, %ecx
	jne	LBB10_3
## BB#9:
	testb	$8, %r8b
	jne	LBB10_10
## BB#14:
	movq	%r11, %r10
	subq	40(%rsi), %r10
	jmp	LBB10_15
LBB10_11:
	testb	$1, 64(%rsi)
	jne	LBB10_12
## BB#13:
	leaq	64(%rsi), %rax
	incq	%rax
	movq	%r9, %r10
	subq	%rax, %r10
	jmp	LBB10_15
LBB10_10:
	movq	24(%rsi), %r10
	subq	16(%rsi), %r10
	jmp	LBB10_15
LBB10_12:
	movq	%r9, %r10
	subq	80(%rsi), %r10
LBB10_15:
	addq	%rdx, %r10
	js	LBB10_3
## BB#16:
	testb	$1, 64(%rsi)
	jne	LBB10_17
## BB#18:
	leaq	64(%rsi), %rcx
	incq	%rcx
	jmp	LBB10_19
LBB10_17:
	movq	80(%rsi), %rcx
LBB10_19:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r9, %rax
	subq	%rcx, %rax
	cmpq	%r10, %rax
	jl	LBB10_3
## BB#20:
	movl	%r8d, %ecx
	andl	$8, %ecx
	testq	%r10, %r10
	je	LBB10_25
## BB#21:
	testl	%ecx, %ecx
	je	LBB10_23
## BB#22:
	cmpq	$0, 24(%rsi)
	je	LBB10_3
LBB10_23:
	testb	$16, %r8b
	je	LBB10_25
## BB#24:
	testq	%r11, %r11
	jne	LBB10_25
LBB10_3:
	movq	$0, 120(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, (%rdi)
	movq	$-1, 128(%rdi)
LBB10_30:
	movq	%rdi, %rax
	popq	%rbp
	retq
LBB10_25:                               ## %._crit_edge
	testl	%ecx, %ecx
	je	LBB10_27
## BB#26:
	movq	16(%rsi), %rax
	addq	%r10, %rax
	movq	%rax, 24(%rsi)
	movq	%r9, 32(%rsi)
LBB10_27:
	testb	$16, %r8b
	je	LBB10_29
## BB#28:
	movslq	%r10d, %rax
	addq	40(%rsi), %rax
	movq	%rax, 48(%rsi)
LBB10_29:
	movq	$0, 120(%rdi)
	movq	$0, 112(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 96(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 64(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, (%rdi)
	movq	%r10, 128(%rdi)
	jmp	LBB10_30
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp123:
	.cfi_def_cfa_offset 16
Ltmp124:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp125:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp126:
	.cfi_offset %rbx, -24
	movl	%edx, %r8d
	movq	%rdi, %rbx
	movq	(%rsi), %rax
	movq	144(%rbp), %rdx
	xorl	%ecx, %ecx
	callq	*32(%rax)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp127:
	.cfi_def_cfa_offset 16
Ltmp128:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp129:
	.cfi_def_cfa_register %rbp
	movq	48(%rdi), %rax
	movq	88(%rdi), %rcx
	cmpq	%rax, %rcx
	jae	LBB12_2
## BB#1:
	movq	%rax, 88(%rdi)
	movq	%rax, %rcx
LBB12_2:
	movl	$-1, %eax
	testb	$8, 96(%rdi)
	je	LBB12_7
## BB#3:
	movq	24(%rdi), %rdx
	movq	32(%rdi), %rsi
	cmpq	%rcx, %rsi
	jae	LBB12_5
## BB#4:
	movq	%rcx, 32(%rdi)
	movq	%rcx, %rsi
LBB12_5:                                ## %._crit_edge
	cmpq	%rsi, %rdx
	jae	LBB12_7
## BB#6:
	movzbl	(%rdx), %eax
LBB12_7:
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp130:
	.cfi_def_cfa_offset 16
Ltmp131:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp132:
	.cfi_def_cfa_register %rbp
	movq	48(%rdi), %rax
	movq	88(%rdi), %r10
	cmpq	%rax, %r10
	jae	LBB13_2
## BB#1:
	movq	%rax, 88(%rdi)
	movq	%rax, %r10
LBB13_2:
	movq	16(%rdi), %r8
	movq	24(%rdi), %rdx
	movl	$-1, %eax
	cmpq	%rdx, %r8
	jae	LBB13_8
## BB#3:
	cmpl	$-1, %esi
	je	LBB13_4
## BB#5:
	testb	$16, 96(%rdi)
	jne	LBB13_7
## BB#6:
	movzbl	-1(%rdx), %r9d
	movzbl	%sil, %ecx
	cmpl	%r9d, %ecx
	jne	LBB13_8
LBB13_7:
	decq	%rdx
	movq	%r8, 16(%rdi)
	movq	%rdx, 24(%rdi)
	movq	%r10, 32(%rdi)
	movb	%sil, (%rdx)
	movl	%esi, %eax
LBB13_8:
	popq	%rbp
	retq
LBB13_4:
	decq	%rdx
	movq	%rdx, 24(%rdi)
	movq	%r10, 32(%rdi)
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp138:
	.cfi_def_cfa_offset 16
Ltmp139:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp140:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp141:
	.cfi_offset %rbx, -56
Ltmp142:
	.cfi_offset %r12, -48
Ltmp143:
	.cfi_offset %r13, -40
Ltmp144:
	.cfi_offset %r14, -32
Ltmp145:
	.cfi_offset %r15, -24
	movl	%esi, %r14d
	movq	%rdi, %rbx
	xorl	%r12d, %r12d
	cmpl	$-1, %r14d
	je	LBB14_19
## BB#1:
	movq	24(%rbx), %r15
	movq	48(%rbx), %r13
	subq	16(%rbx), %r15
	movq	56(%rbx), %rax
	cmpq	%rax, %r13
	je	LBB14_3
## BB#2:                                ## %._crit_edge
	leaq	88(%rbx), %rcx
	movq	88(%rbx), %rdi
	leaq	96(%rbx), %rdx
	jmp	LBB14_12
LBB14_3:
	movl	$-1, %r12d
	testb	$16, 96(%rbx)
	je	LBB14_19
## BB#4:
	movq	40(%rbx), %rax
	movq	%rax, -72(%rbp)         ## 8-byte Spill
	movq	88(%rbx), %rax
	movq	%rax, -64(%rbp)         ## 8-byte Spill
	leaq	64(%rbx), %rdi
	movq	%rdi, -56(%rbp)         ## 8-byte Spill
Ltmp133:
	xorl	%esi, %esi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp134:
## BB#5:
	movl	$22, %esi
	movq	-56(%rbp), %rdi         ## 8-byte Reload
	testb	$1, (%rdi)
	je	LBB14_7
## BB#6:
	movq	(%rdi), %rsi
	andq	$-2, %rsi
	decq	%rsi
LBB14_7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
Ltmp135:
	xorl	%edx, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp136:
## BB#8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	movq	-72(%rbp), %rax         ## 8-byte Reload
	subq	%rax, %r13
	leaq	88(%rbx), %rcx
	movq	-64(%rbp), %rdi         ## 8-byte Reload
	subq	%rax, %rdi
	movq	-56(%rbp), %rsi         ## 8-byte Reload
	movzbl	(%rsi), %eax
	testb	$1, %al
	jne	LBB14_9
## BB#10:
	incq	%rsi
	shrq	%rax
	jmp	LBB14_11
LBB14_9:
	movq	72(%rbx), %rax
	movq	80(%rbx), %rsi
LBB14_11:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	leaq	96(%rbx), %rdx
	addq	%rsi, %rax
	movq	%rsi, 40(%rbx)
	movq	%rax, 56(%rbx)
	movslq	%r13d, %r13
	addq	%rsi, %r13
	movq	%r13, 48(%rbx)
	addq	%rsi, %rdi
	movq	%rdi, 88(%rbx)
LBB14_12:
	leaq	1(%r13), %rsi
	movq	%rsi, -48(%rbp)
	cmpq	%rdi, %rsi
	leaq	-48(%rbp), %rdi
	cmovbq	%rcx, %rdi
	movq	(%rdi), %rdi
	movq	%rdi, (%rcx)
	testb	$8, (%rdx)
	je	LBB14_17
## BB#13:
	testb	$1, 64(%rbx)
	jne	LBB14_14
## BB#15:
	leaq	64(%rbx), %rcx
	incq	%rcx
	jmp	LBB14_16
LBB14_14:
	movq	80(%rbx), %rcx
LBB14_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	addq	%rcx, %r15
	movq	%rcx, 16(%rbx)
	movq	%r15, 24(%rbx)
	movq	%rdi, 32(%rbx)
LBB14_17:
	cmpq	%rax, %r13
	je	LBB14_21
## BB#18:
	movq	%rsi, 48(%rbx)
	movb	%r14b, (%r13)
	movzbl	%r14b, %r12d
LBB14_19:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB14_21:
	movq	(%rbx), %rax
	movq	104(%rax), %rax
	movzbl	%r14b, %esi
	movq	%rbx, %rdi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   ## TAILCALL
LBB14_20:
Ltmp137:
	movq	%rax, %rdi
	callq	___cxa_begin_catch
	callq	___cxa_end_catch
	jmp	LBB14_19
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table14:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset59 = Ltmp133-Lfunc_begin3           ## >> Call Site 1 <<
	.long	Lset59
Lset60 = Ltmp136-Ltmp133                ##   Call between Ltmp133 and Ltmp136
	.long	Lset60
Lset61 = Ltmp137-Lfunc_begin3           ##     jumps to Ltmp137
	.long	Lset61
	.byte	1                       ##   On action: 1
Lset62 = Ltmp136-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset62
Lset63 = Lfunc_end3-Ltmp136             ##   Call between Ltmp136 and Lfunc_end3
	.long	Lset63
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp146:
	.cfi_def_cfa_offset 16
Ltmp147:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp148:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	pushq	%rax
Ltmp149:
	.cfi_offset %rbx, -40
Ltmp150:
	.cfi_offset %r14, -32
Ltmp151:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	leaq	64(%rbx), %r14
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	$0, 88(%rbx)
	movl	96(%rbx), %eax
	testb	$8, %al
	je	LBB15_5
## BB#1:
	movzbl	(%r14), %ecx
	testb	$1, %cl
	jne	LBB15_2
## BB#3:
	shrq	%rcx
	leaq	1(%r14,%rcx), %rcx
	leaq	1(%r14), %rdx
	jmp	LBB15_4
LBB15_2:
	movq	80(%rbx), %rdx
	movq	72(%rbx), %rcx
	addq	%rdx, %rcx
LBB15_4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	%rcx, 88(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rcx, 32(%rbx)
LBB15_5:
	testb	$16, %al
	je	LBB15_14
## BB#6:
	movzbl	(%r14), %r15d
	testb	$1, %r15b
	jne	LBB15_8
## BB#7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4.thread
	shrq	%r15
	leaq	1(%r14,%r15), %rax
	movq	%rax, 88(%rbx)
	movl	$22, %esi
	jmp	LBB15_9
LBB15_8:
	movq	72(%rbx), %r15
	movq	80(%rbx), %rax
	addq	%r15, %rax
	movq	%rax, 88(%rbx)
	movq	64(%rbx), %rsi
	andq	$-2, %rsi
	decq	%rsi
LBB15_9:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movzbl	(%r14), %eax
	testb	$1, %al
	jne	LBB15_10
## BB#11:
	incq	%r14
	shrq	%rax
	jmp	LBB15_12
LBB15_10:
	movq	72(%rbx), %rax
	movq	80(%rbx), %r14
LBB15_12:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	%r14, %rcx
	addq	%rax, %r14
	movq	%rcx, 48(%rbx)
	movq	%rcx, 40(%rbx)
	movq	%r14, 56(%rbx)
	testb	$3, 96(%rbx)
	je	LBB15_14
## BB#13:
	movslq	%r15d, %rax
	addq	%rax, %rcx
	movq	%rcx, 48(%rbx)
LBB15_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"foo 2"

L_.str.1:                               ## @.str.1
	.asciz	"foo"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	120
	.quad	0
	.quad	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-120
	.quad	-120
	.quad	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE ## @_ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE:
	.quad	120
	.quad	0
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev
	.quad	-120
	.quad	-120
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_istreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_istringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_istreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE


.subsections_via_symbols
