	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z5to_bejPh
	.align	4, 0x90
__Z5to_bejPh:                           ## @_Z5to_bejPh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movq	%rsi, -16(%rbp)
	movl	-4(%rbp), %edi
	shrl	$24, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, (%rsi)
	movl	-4(%rbp), %edi
	shrl	$16, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 1(%rsi)
	movl	-4(%rbp), %edi
	shrl	$8, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 2(%rsi)
	movl	-4(%rbp), %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 3(%rsi)
	movq	-16(%rbp), %rsi
	addq	$4, %rsi
	movq	%rsi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z5to_betPh
	.align	4, 0x90
__Z5to_betPh:                           ## @_Z5to_betPh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	movw	%di, %ax
	movw	%ax, -2(%rbp)
	movq	%rsi, -16(%rbp)
	movzwl	-2(%rbp), %edi
	sarl	$8, %edi
	andl	$255, %edi
	movb	%dil, %cl
	movq	-16(%rbp), %rsi
	movb	%cl, (%rsi)
	movzwl	-2(%rbp), %edi
	andl	$255, %edi
	movb	%dil, %cl
	movq	-16(%rbp), %rsi
	movb	%cl, 1(%rsi)
	movq	-16(%rbp), %rsi
	addq	$2, %rsi
	movq	%rsi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z5to_lejPh
	.align	4, 0x90
__Z5to_lejPh:                           ## @_Z5to_lejPh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movq	%rsi, -16(%rbp)
	movl	-4(%rbp), %edi
	shrl	$24, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 3(%rsi)
	movl	-4(%rbp), %edi
	shrl	$16, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 2(%rsi)
	movl	-4(%rbp), %edi
	shrl	$8, %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, 1(%rsi)
	movl	-4(%rbp), %edi
	andl	$255, %edi
	movb	%dil, %al
	movq	-16(%rbp), %rsi
	movb	%al, (%rsi)
	movq	-16(%rbp), %rsi
	addq	$4, %rsi
	movq	%rsi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z5to_letPh
	.align	4, 0x90
__Z5to_letPh:                           ## @_Z5to_letPh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	movw	%di, %ax
	movw	%ax, -2(%rbp)
	movq	%rsi, -16(%rbp)
	movzwl	-2(%rbp), %edi
	sarl	$8, %edi
	andl	$255, %edi
	movb	%dil, %cl
	movq	-16(%rbp), %rsi
	movb	%cl, 1(%rsi)
	movzwl	-2(%rbp), %edi
	andl	$255, %edi
	movb	%dil, %cl
	movq	-16(%rbp), %rsi
	movb	%cl, (%rsi)
	movq	-16(%rbp), %rsi
	addq	$2, %rsi
	movq	%rsi, %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z9from_le32PKh
	.align	4, 0x90
__Z9from_le32PKh:                       ## @_Z9from_le32PKh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp12:
	.cfi_def_cfa_offset 16
Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp14:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	$0, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	(%rdi), %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	1(%rdi), %eax
	shll	$8, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	2(%rdi), %eax
	shll	$16, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	3(%rdi), %eax
	shll	$24, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z9from_le16PKh
	.align	4, 0x90
__Z9from_le16PKh:                       ## @_Z9from_le16PKh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp15:
	.cfi_def_cfa_offset 16
Ltmp16:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp17:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movw	$0, -10(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	(%rdi), %eax
	movzwl	-10(%rbp), %ecx
	addl	%eax, %ecx
	movw	%cx, %dx
	movw	%dx, -10(%rbp)
	movq	-8(%rbp), %rdi
	movb	1(%rdi), %sil
	movzbl	%sil, %eax
	movw	%ax, %dx
	movzwl	%dx, %eax
	shll	$8, %eax
	movzwl	-10(%rbp), %ecx
	addl	%eax, %ecx
	movw	%cx, %dx
	movw	%dx, -10(%rbp)
	movzwl	-10(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z9from_be32PKh
	.align	4, 0x90
__Z9from_be32PKh:                       ## @_Z9from_be32PKh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp20:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	$0, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	3(%rdi), %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	2(%rdi), %eax
	shll	$8, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	1(%rdi), %eax
	shll	$16, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	(%rdi), %eax
	shll	$24, %eax
	addl	-12(%rbp), %eax
	movl	%eax, -12(%rbp)
	movl	-12(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__Z9from_be16PKh
	.align	4, 0x90
__Z9from_be16PKh:                       ## @_Z9from_be16PKh
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movw	$0, -10(%rbp)
	movq	-8(%rbp), %rdi
	movzbl	1(%rdi), %eax
	movzwl	-10(%rbp), %ecx
	addl	%eax, %ecx
	movw	%cx, %dx
	movw	%dx, -10(%rbp)
	movq	-8(%rbp), %rdi
	movb	(%rdi), %sil
	movzbl	%sil, %eax
	movw	%ax, %dx
	movzwl	%dx, %eax
	shll	$8, %eax
	movzwl	-10(%rbp), %ecx
	addl	%eax, %ecx
	movw	%cx, %dx
	movw	%dx, -10(%rbp)
	movzwl	-10(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp44:
	.cfi_def_cfa_offset 16
Ltmp45:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp46:
	.cfi_def_cfa_register %rbp
	subq	$448, %rsp              ## imm = 0x1C0
	movq	___stack_chk_guard@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, -8(%rbp)
	leaq	l_.ref.tmp(%rip), %rax
	movq	%rax, -128(%rbp)
	movq	$6, -120(%rbp)
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rcx
	movq	%rsp, %rdx
	movq	%rcx, 8(%rdx)
	movq	%rax, (%rdx)
	leaq	-24(%rbp), %rax
	movl	$1122867, %esi          ## imm = 0x112233
	movl	$17493, %edi            ## imm = 0x4455
	movl	$26231, %r8d            ## imm = 0x6677
	movl	$136, %r9d
	movl	$153, %r10d
	movl	%edi, -300(%rbp)        ## 4-byte Spill
	movq	%rax, %rdi
	movl	%esi, -304(%rbp)        ## 4-byte Spill
	movl	-300(%rbp), %edx        ## 4-byte Reload
	movl	%r8d, %ecx
	movl	%r8d, -308(%rbp)        ## 4-byte Spill
	movl	%r9d, %r8d
	movl	%r9d, -312(%rbp)        ## 4-byte Spill
	movl	%r10d, %r9d
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	movl	%r10d, -324(%rbp)       ## 4-byte Spill
	callq	__ZN4uuidC1E10big_endianjtthhSt16initializer_listIhE
	leaq	l_.ref.tmp.1(%rip), %rax
	movq	%rax, -152(%rbp)
	movq	$6, -144(%rbp)
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rdi
	movq	%rsp, %r11
	movq	%rdi, 8(%r11)
	movq	%rax, (%r11)
	leaq	-40(%rbp), %rdi
	movl	-304(%rbp), %esi        ## 4-byte Reload
	movl	-300(%rbp), %edx        ## 4-byte Reload
	movl	-308(%rbp), %ecx        ## 4-byte Reload
	movl	-312(%rbp), %r8d        ## 4-byte Reload
	movl	-324(%rbp), %r9d        ## 4-byte Reload
	callq	__ZN4uuidC1E13little_endianjtthhSt16initializer_listIhE
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	-320(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, -336(%rbp)        ## 8-byte Spill
	callq	__ZNK4uuid2asE13little_endian
Ltmp24:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-336(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp25:
	movq	%rax, -344(%rbp)        ## 8-byte Spill
	jmp	LBB8_1
LBB8_1:
	movq	-344(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -104(%rbp)
	movq	-96(%rbp), %rdi
Ltmp26:
	callq	*%rcx
Ltmp27:
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	jmp	LBB8_2
LBB8_2:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit
	jmp	LBB8_3
LBB8_3:
	leaq	-176(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-224(%rbp), %rdi
	leaq	-24(%rbp), %rsi
	movq	%rdi, -360(%rbp)        ## 8-byte Spill
	callq	__ZNK4uuid2asE10big_endian
Ltmp29:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-360(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp30:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB8_4
LBB8_4:
	movq	-368(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -88(%rbp)
	movq	-80(%rbp), %rdi
Ltmp31:
	callq	*%rcx
Ltmp32:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	jmp	LBB8_5
LBB8_5:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit1
	jmp	LBB8_6
LBB8_6:
	leaq	-224(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-256(%rbp), %rdi
	leaq	-40(%rbp), %rsi
	movq	%rdi, -384(%rbp)        ## 8-byte Spill
	callq	__ZNK4uuid2asE13little_endian
Ltmp34:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-384(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp35:
	movq	%rax, -392(%rbp)        ## 8-byte Spill
	jmp	LBB8_7
LBB8_7:
	movq	-392(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -72(%rbp)
	movq	-64(%rbp), %rdi
Ltmp36:
	callq	*%rcx
Ltmp37:
	movq	%rax, -400(%rbp)        ## 8-byte Spill
	jmp	LBB8_8
LBB8_8:                                 ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit2
	jmp	LBB8_9
LBB8_9:
	leaq	-256(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	leaq	-288(%rbp), %rdi
	leaq	-40(%rbp), %rsi
	movq	%rdi, -408(%rbp)        ## 8-byte Spill
	callq	__ZNK4uuid2asE10big_endian
Ltmp39:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	movq	-408(%rbp), %rsi        ## 8-byte Reload
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
Ltmp40:
	movq	%rax, -416(%rbp)        ## 8-byte Spill
	jmp	LBB8_10
LBB8_10:
	movq	-416(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rcx
	movq	%rcx, -56(%rbp)
	movq	-48(%rbp), %rdi
Ltmp41:
	callq	*%rcx
Ltmp42:
	movq	%rax, -424(%rbp)        ## 8-byte Spill
	jmp	LBB8_11
LBB8_11:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit3
	jmp	LBB8_12
LBB8_12:
	leaq	-288(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	___stack_chk_guard@GOTPCREL(%rip), %rdi
	movq	(%rdi), %rdi
	cmpq	-8(%rbp), %rdi
	jne	LBB8_19
## BB#13:                               ## %SP_return
	xorl	%eax, %eax
	addq	$448, %rsp              ## imm = 0x1C0
	popq	%rbp
	retq
LBB8_14:
Ltmp28:
	leaq	-176(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -192(%rbp)
	movl	%ecx, -196(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB8_18
LBB8_15:
Ltmp33:
	leaq	-224(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -192(%rbp)
	movl	%ecx, -196(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB8_18
LBB8_16:
Ltmp38:
	leaq	-256(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -192(%rbp)
	movl	%ecx, -196(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB8_18
LBB8_17:
Ltmp43:
	leaq	-288(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -192(%rbp)
	movl	%ecx, -196(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB8_18:
	movq	-192(%rbp), %rdi
	callq	__Unwind_Resume
LBB8_19:                                ## %CallStackCheckFailBlk
	callq	___stack_chk_fail
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table8:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\367\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp24-Lfunc_begin0             ##   Call between Lfunc_begin0 and Ltmp24
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp24-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp27-Ltmp24                   ##   Call between Ltmp24 and Ltmp27
	.long	Lset3
Lset4 = Ltmp28-Lfunc_begin0             ##     jumps to Ltmp28
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp27-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp29-Ltmp27                   ##   Call between Ltmp27 and Ltmp29
	.long	Lset6
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset7 = Ltmp29-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset7
Lset8 = Ltmp32-Ltmp29                   ##   Call between Ltmp29 and Ltmp32
	.long	Lset8
Lset9 = Ltmp33-Lfunc_begin0             ##     jumps to Ltmp33
	.long	Lset9
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp32-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset10
Lset11 = Ltmp34-Ltmp32                  ##   Call between Ltmp32 and Ltmp34
	.long	Lset11
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp34-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset12
Lset13 = Ltmp37-Ltmp34                  ##   Call between Ltmp34 and Ltmp37
	.long	Lset13
Lset14 = Ltmp38-Lfunc_begin0            ##     jumps to Ltmp38
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp37-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset15
Lset16 = Ltmp39-Ltmp37                  ##   Call between Ltmp37 and Ltmp39
	.long	Lset16
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset17 = Ltmp39-Lfunc_begin0            ## >> Call Site 8 <<
	.long	Lset17
Lset18 = Ltmp42-Ltmp39                  ##   Call between Ltmp39 and Ltmp42
	.long	Lset18
Lset19 = Ltmp43-Lfunc_begin0            ##     jumps to Ltmp43
	.long	Lset19
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp42-Lfunc_begin0            ## >> Call Site 9 <<
	.long	Lset20
Lset21 = Lfunc_end0-Ltmp42              ##   Call between Ltmp42 and Lfunc_end0
	.long	Lset21
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN4uuidC1E10big_endianjtthhSt16initializer_listIhE
	.weak_def_can_be_hidden	__ZN4uuidC1E10big_endianjtthhSt16initializer_listIhE
	.align	4, 0x90
__ZN4uuidC1E10big_endianjtthhSt16initializer_listIhE: ## @_ZN4uuidC1E10big_endianjtthhSt16initializer_listIhE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp47:
	.cfi_def_cfa_offset 16
Ltmp48:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp49:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp50:
	.cfi_offset %rbx, -40
Ltmp51:
	.cfi_offset %r14, -32
Ltmp52:
	.cfi_offset %r15, -24
	movb	%r9b, %al
	movb	%r8b, %r10b
	movw	%cx, %r11w
	movw	%dx, %bx
	leaq	16(%rbp), %r14
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	movw	%bx, -46(%rbp)
	movw	%r11w, -48(%rbp)
	movb	%r10b, -49(%rbp)
	movb	%al, -50(%rbp)
	movq	-40(%rbp), %rdi
	movl	-44(%rbp), %esi
	movw	-46(%rbp), %r11w
	movw	-48(%rbp), %bx
	movb	-49(%rbp), %al
	movzwl	%r11w, %edx
	movzwl	%bx, %ecx
	movzbl	%al, %r8d
	movzbl	-50(%rbp), %r9d
	movq	(%r14), %r15
	movq	%r15, (%rsp)
	movq	8(%r14), %r14
	movq	%r14, 8(%rsp)
	callq	__ZN4uuidC2E10big_endianjtthhSt16initializer_listIhE
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN4uuidC1E13little_endianjtthhSt16initializer_listIhE
	.weak_def_can_be_hidden	__ZN4uuidC1E13little_endianjtthhSt16initializer_listIhE
	.align	4, 0x90
__ZN4uuidC1E13little_endianjtthhSt16initializer_listIhE: ## @_ZN4uuidC1E13little_endianjtthhSt16initializer_listIhE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp53:
	.cfi_def_cfa_offset 16
Ltmp54:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp55:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp56:
	.cfi_offset %rbx, -40
Ltmp57:
	.cfi_offset %r14, -32
Ltmp58:
	.cfi_offset %r15, -24
	movb	%r9b, %al
	movb	%r8b, %r10b
	movw	%cx, %r11w
	movw	%dx, %bx
	leaq	16(%rbp), %r14
	movq	%rdi, -40(%rbp)
	movl	%esi, -44(%rbp)
	movw	%bx, -46(%rbp)
	movw	%r11w, -48(%rbp)
	movb	%r10b, -49(%rbp)
	movb	%al, -50(%rbp)
	movq	-40(%rbp), %rdi
	movl	-44(%rbp), %esi
	movw	-46(%rbp), %r11w
	movw	-48(%rbp), %bx
	movb	-49(%rbp), %al
	movzwl	%r11w, %edx
	movzwl	%bx, %ecx
	movzbl	%al, %r8d
	movzbl	-50(%rbp), %r9d
	movq	(%r14), %r15
	movq	%r15, (%rsp)
	movq	8(%r14), %r14
	movq	%r14, 8(%rsp)
	callq	__ZN4uuidC2E13little_endianjtthhSt16initializer_listIhE
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp59:
	.cfi_def_cfa_offset 16
Ltmp60:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp61:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB11_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB11_3
LBB11_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB11_3:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB11_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB11_6
LBB11_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB11_6:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK4uuid2asE13little_endian
	.weak_def_can_be_hidden	__ZNK4uuid2asE13little_endian
	.align	4, 0x90
__ZNK4uuid2asE13little_endian:          ## @_ZNK4uuid2asE13little_endian
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp92:
	.cfi_def_cfa_offset 16
Ltmp93:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp94:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -352(%rbp)
	leaq	-616(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movl	$16, -324(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$112, %rdx
	movq	%rdx, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 112(%rcx)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 112(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %r8
	addq	$24, %r8
	movq	%r8, (%rcx)
	addq	$64, %rdx
	movq	%rdx, 112(%rcx)
	movq	%rcx, %rdx
	addq	$8, %rdx
	movq	%rcx, -32(%rbp)
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r8
	addq	$8, %r8
	movq	%r8, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-32(%rbp), %rdx
	movq	-40(%rbp), %r8
	movq	(%r8), %r9
	movq	%r9, (%rdx)
	movq	8(%r8), %r8
	movq	-24(%r9), %r9
	movq	%r8, (%rdx,%r9)
	movq	(%rdx), %r8
	movq	-24(%r8), %r8
	addq	%r8, %rdx
	movq	-48(%rbp), %r8
	movq	%rdx, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdx
Ltmp62:
	movq	%rdi, -640(%rbp)        ## 8-byte Spill
	movq	%rdx, %rdi
	movq	%rsi, -648(%rbp)        ## 8-byte Spill
	movq	%r8, %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdx, -664(%rbp)        ## 8-byte Spill
	movq	%rcx, -672(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp63:
	jmp	LBB12_1
LBB12_1:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-672(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 112(%rdi)
	addq	$8, %rdi
	movl	-324(%rbp), %eax
	orl	$16, %eax
	movq	%rdi, -288(%rbp)
	movl	%eax, -292(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	movl	%eax, -236(%rbp)
	movq	-232(%rbp), %rdx
Ltmp65:
	movq	%rdx, %rdi
	movq	%rdx, -680(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp66:
	jmp	LBB12_2
LBB12_2:                                ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	-200(%rbp), %r8
	movq	%r8, -192(%rbp)
	movq	-192(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -184(%rbp)
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -696(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-696(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movl	$0, -172(%rbp)
LBB12_3:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -172(%rbp)
	jae	LBB12_5
## BB#4:                                ##   in Loop: Header=BB12_3 Depth=1
	movl	-172(%rbp), %eax
	movl	%eax, %ecx
	movq	-168(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-172(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -172(%rbp)
	jmp	LBB12_3
LBB12_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-264(%rbp), %rcx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	$0, 88(%rdi)
	movl	-236(%rbp), %eax
	movl	%eax, 96(%rdi)
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, -104(%rbp)
	movq	-104(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -96(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-704(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movl	$0, -84(%rbp)
LBB12_6:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -84(%rbp)
	jae	LBB12_8
## BB#7:                                ##   in Loop: Header=BB12_6 Depth=1
	movl	-84(%rbp), %eax
	movl	%eax, %ecx
	movq	-80(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-84(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -84(%rbp)
	jmp	LBB12_6
LBB12_8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3.i.i.i
Ltmp68:
	leaq	-264(%rbp), %rsi
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp69:
	jmp	LBB12_14
LBB12_9:
Ltmp70:
	movl	%edx, %ecx
	movq	%rax, -272(%rbp)
	movl	%ecx, -276(%rbp)
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-688(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-272(%rbp), %rax
	movl	-276(%rbp), %ecx
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movl	%ecx, -716(%rbp)        ## 4-byte Spill
	jmp	LBB12_12
LBB12_10:
Ltmp64:
	movl	%edx, %ecx
	movq	%rax, -336(%rbp)
	movl	%ecx, -340(%rbp)
	jmp	LBB12_13
LBB12_11:
Ltmp67:
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movl	%ecx, -716(%rbp)        ## 4-byte Spill
	jmp	LBB12_12
LBB12_12:                               ## %.body.i
	movl	-716(%rbp), %eax        ## 4-byte Reload
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -336(%rbp)
	movl	%eax, -340(%rbp)
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB12_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-336(%rbp), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
	jmp	LBB12_28
LBB12_14:                               ## %_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej.exit
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp71:
	movl	$4, %eax
	movl	%eax, %edx
	xorl	%eax, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp72:
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB12_15
LBB12_15:
Ltmp73:
	movl	$45, %esi
	movq	-736(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp74:
	movq	%rax, -744(%rbp)        ## 8-byte Spill
	jmp	LBB12_16
LBB12_16:
Ltmp75:
	movl	$6, %eax
	movl	%eax, %edx
	movl	$4, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp76:
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	jmp	LBB12_17
LBB12_17:
Ltmp77:
	movl	$45, %esi
	movq	-752(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp78:
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	jmp	LBB12_18
LBB12_18:
Ltmp79:
	movl	$8, %eax
	movl	%eax, %edx
	movl	$6, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp80:
	movq	%rax, -768(%rbp)        ## 8-byte Spill
	jmp	LBB12_19
LBB12_19:
Ltmp81:
	movl	$45, %esi
	movq	-768(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp82:
	movq	%rax, -776(%rbp)        ## 8-byte Spill
	jmp	LBB12_20
LBB12_20:
Ltmp83:
	movl	$8, %eax
	movl	%eax, %edx
	movl	$10, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp84:
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	jmp	LBB12_21
LBB12_21:
Ltmp85:
	movl	$45, %esi
	movq	-784(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp86:
	movq	%rax, -792(%rbp)        ## 8-byte Spill
	jmp	LBB12_22
LBB12_22:
Ltmp87:
	movl	$10, %eax
	movl	%eax, %edx
	movl	$16, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp88:
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	jmp	LBB12_23
LBB12_23:
	leaq	-616(%rbp), %rax
	movq	%rax, -8(%rbp)
	leaq	-608(%rbp), %rsi
Ltmp89:
	movq	-640(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp90:
	jmp	LBB12_24
LBB12_24:                               ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit
	jmp	LBB12_25
LBB12_25:
	leaq	-616(%rbp), %rdi
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
LBB12_26:
Ltmp91:
	leaq	-616(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -624(%rbp)
	movl	%ecx, -628(%rbp)
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#27:
	movq	-624(%rbp), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
LBB12_28:                               ## %unwind_resume
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table12:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset22 = Ltmp62-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset22
Lset23 = Ltmp63-Ltmp62                  ##   Call between Ltmp62 and Ltmp63
	.long	Lset23
Lset24 = Ltmp64-Lfunc_begin1            ##     jumps to Ltmp64
	.long	Lset24
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp65-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset25
Lset26 = Ltmp66-Ltmp65                  ##   Call between Ltmp65 and Ltmp66
	.long	Lset26
Lset27 = Ltmp67-Lfunc_begin1            ##     jumps to Ltmp67
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp66-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset28
Lset29 = Ltmp68-Ltmp66                  ##   Call between Ltmp66 and Ltmp68
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp68-Lfunc_begin1            ## >> Call Site 4 <<
	.long	Lset30
Lset31 = Ltmp69-Ltmp68                  ##   Call between Ltmp68 and Ltmp69
	.long	Lset31
Lset32 = Ltmp70-Lfunc_begin1            ##     jumps to Ltmp70
	.long	Lset32
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp71-Lfunc_begin1            ## >> Call Site 5 <<
	.long	Lset33
Lset34 = Ltmp90-Ltmp71                  ##   Call between Ltmp71 and Ltmp90
	.long	Lset34
Lset35 = Ltmp91-Lfunc_begin1            ##     jumps to Ltmp91
	.long	Lset35
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp90-Lfunc_begin1            ## >> Call Site 6 <<
	.long	Lset36
Lset37 = Lfunc_end1-Ltmp90              ##   Call between Ltmp90 and Lfunc_end1
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp95:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp96:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB13_1
LBB13_1:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp97:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp98:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB13_3
LBB13_2:
Ltmp99:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB13_3:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table13:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset38 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset38
Lset39 = Ltmp95-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp95
	.long	Lset39
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset40 = Ltmp95-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset40
Lset41 = Ltmp98-Ltmp95                  ##   Call between Ltmp95 and Ltmp98
	.long	Lset41
Lset42 = Ltmp99-Lfunc_begin2            ##     jumps to Ltmp99
	.long	Lset42
	.byte	0                       ##   On action: cleanup
Lset43 = Ltmp98-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset43
Lset44 = Lfunc_end2-Ltmp98              ##   Call between Ltmp98 and Lfunc_end2
	.long	Lset44
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK4uuid2asE10big_endian
	.weak_def_can_be_hidden	__ZNK4uuid2asE10big_endian
	.align	4, 0x90
__ZNK4uuid2asE10big_endian:             ## @_ZNK4uuid2asE10big_endian
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp133:
	.cfi_def_cfa_offset 16
Ltmp134:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp135:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -352(%rbp)
	leaq	-616(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movl	$16, -324(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, %rdx
	addq	$112, %rdx
	movq	%rdx, -312(%rbp)
	movq	%rdx, -304(%rbp)
	movq	__ZTVNSt3__18ios_baseE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 112(%rcx)
	movq	__ZTVNSt3__19basic_iosIcNS_11char_traitsIcEEEE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, 112(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %r8
	addq	$24, %r8
	movq	%r8, (%rcx)
	addq	$64, %rdx
	movq	%rdx, 112(%rcx)
	movq	%rcx, %rdx
	addq	$8, %rdx
	movq	%rcx, -32(%rbp)
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %r8
	addq	$8, %r8
	movq	%r8, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	-32(%rbp), %rdx
	movq	-40(%rbp), %r8
	movq	(%r8), %r9
	movq	%r9, (%rdx)
	movq	8(%r8), %r8
	movq	-24(%r9), %r9
	movq	%r8, (%rdx,%r9)
	movq	(%rdx), %r8
	movq	-24(%r8), %r8
	addq	%r8, %rdx
	movq	-48(%rbp), %r8
	movq	%rdx, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdx
Ltmp103:
	movq	%rdi, -640(%rbp)        ## 8-byte Spill
	movq	%rdx, %rdi
	movq	%rsi, -648(%rbp)        ## 8-byte Spill
	movq	%r8, %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdx, -664(%rbp)        ## 8-byte Spill
	movq	%rcx, -672(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base4initEPv
Ltmp104:
	jmp	LBB14_1
LBB14_1:                                ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEEC2EPNS_15basic_streambufIcS2_EE.exit.i
	movq	-664(%rbp), %rax        ## 8-byte Reload
	movq	$0, 136(%rax)
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movl	%eax, 144(%rcx)
	movq	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	movq	%rdx, %rsi
	addq	$24, %rsi
	movq	-672(%rbp), %rdi        ## 8-byte Reload
	movq	%rsi, (%rdi)
	addq	$64, %rdx
	movq	%rdx, 112(%rdi)
	addq	$8, %rdi
	movl	-324(%rbp), %eax
	orl	$16, %eax
	movq	%rdi, -288(%rbp)
	movl	%eax, -292(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rdx, -232(%rbp)
	movl	%eax, -236(%rbp)
	movq	-232(%rbp), %rdx
Ltmp106:
	movq	%rdx, %rdi
	movq	%rdx, -680(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEEC2Ev
Ltmp107:
	jmp	LBB14_2
LBB14_2:                                ## %.noexc.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	%rcx, (%rdi)
	addq	$64, %rdi
	movq	%rdi, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %r8
	movq	%r8, -200(%rbp)
	movq	-200(%rbp), %r8
	movq	%r8, -192(%rbp)
	movq	-192(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -184(%rbp)
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	movq	%rcx, -696(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-696(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rdx
	movq	%rdx, -152(%rbp)
	movq	-152(%rbp), %rdx
	movq	%rdx, -144(%rbp)
	movq	-144(%rbp), %rdx
	movq	%rdx, -168(%rbp)
	movl	$0, -172(%rbp)
LBB14_3:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -172(%rbp)
	jae	LBB14_5
## BB#4:                                ##   in Loop: Header=BB14_3 Depth=1
	movl	-172(%rbp), %eax
	movl	%eax, %ecx
	movq	-168(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-172(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -172(%rbp)
	jmp	LBB14_3
LBB14_5:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit.i.i.i
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-264(%rbp), %rcx
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	movq	$0, 88(%rdi)
	movl	-236(%rbp), %eax
	movl	%eax, 96(%rdi)
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %r8
	movq	%r8, -112(%rbp)
	movq	-112(%rbp), %r8
	movq	%r8, -104(%rbp)
	movq	-104(%rbp), %r8
	movq	%r8, %r9
	movq	%r9, -96(%rbp)
	movq	%r8, %rdi
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-704(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rdx
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	%rdx, -56(%rbp)
	movq	-56(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movl	$0, -84(%rbp)
LBB14_6:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -84(%rbp)
	jae	LBB14_8
## BB#7:                                ##   in Loop: Header=BB14_6 Depth=1
	movl	-84(%rbp), %eax
	movl	%eax, %ecx
	movq	-80(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-84(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -84(%rbp)
	jmp	LBB14_6
LBB14_8:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ev.exit3.i.i.i
Ltmp109:
	leaq	-264(%rbp), %rsi
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
Ltmp110:
	jmp	LBB14_14
LBB14_9:
Ltmp111:
	movl	%edx, %ecx
	movq	%rax, -272(%rbp)
	movl	%ecx, -276(%rbp)
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-688(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-680(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	movq	-272(%rbp), %rax
	movl	-276(%rbp), %ecx
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movl	%ecx, -716(%rbp)        ## 4-byte Spill
	jmp	LBB14_12
LBB14_10:
Ltmp105:
	movl	%edx, %ecx
	movq	%rax, -336(%rbp)
	movl	%ecx, -340(%rbp)
	jmp	LBB14_13
LBB14_11:
Ltmp108:
	movl	%edx, %ecx
	movq	%rax, -712(%rbp)        ## 8-byte Spill
	movl	%ecx, -716(%rbp)        ## 4-byte Spill
	jmp	LBB14_12
LBB14_12:                               ## %.body.i
	movl	-716(%rbp), %eax        ## 4-byte Reload
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rdx
	addq	$8, %rdx
	movq	%rcx, -336(%rbp)
	movl	%eax, -340(%rbp)
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, %rdi
	movq	%rdx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
LBB14_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	addq	$112, %rax
	movq	%rax, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	movq	-336(%rbp), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
	jmp	LBB14_28
LBB14_14:                               ## %_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEC1Ej.exit
	leaq	-264(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp112:
	xorl	%eax, %eax
	movl	%eax, %edx
	movl	$4, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp113:
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB14_15
LBB14_15:
Ltmp114:
	movl	$45, %esi
	movq	-736(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp115:
	movq	%rax, -744(%rbp)        ## 8-byte Spill
	jmp	LBB14_16
LBB14_16:
Ltmp116:
	movl	$4, %eax
	movl	%eax, %edx
	movl	$6, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp117:
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	jmp	LBB14_17
LBB14_17:
Ltmp118:
	movl	$45, %esi
	movq	-752(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp119:
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	jmp	LBB14_18
LBB14_18:
Ltmp120:
	movl	$6, %eax
	movl	%eax, %edx
	movl	$8, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp121:
	movq	%rax, -768(%rbp)        ## 8-byte Spill
	jmp	LBB14_19
LBB14_19:
Ltmp122:
	movl	$45, %esi
	movq	-768(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp123:
	movq	%rax, -776(%rbp)        ## 8-byte Spill
	jmp	LBB14_20
LBB14_20:
Ltmp124:
	movl	$8, %eax
	movl	%eax, %edx
	movl	$10, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp125:
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	jmp	LBB14_21
LBB14_21:
Ltmp126:
	movl	$45, %esi
	movq	-784(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
Ltmp127:
	movq	%rax, -792(%rbp)        ## 8-byte Spill
	jmp	LBB14_22
LBB14_22:
Ltmp128:
	movl	$10, %eax
	movl	%eax, %edx
	movl	$16, %eax
	movl	%eax, %ecx
	leaq	-616(%rbp), %rsi
	movq	-648(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
Ltmp129:
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	jmp	LBB14_23
LBB14_23:
	leaq	-616(%rbp), %rax
	movq	%rax, -8(%rbp)
	leaq	-608(%rbp), %rsi
Ltmp130:
	movq	-640(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
Ltmp131:
	jmp	LBB14_24
LBB14_24:                               ## %_ZNKSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv.exit
	jmp	LBB14_25
LBB14_25:
	leaq	-616(%rbp), %rdi
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
LBB14_26:
Ltmp132:
	leaq	-616(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -624(%rbp)
	movl	%ecx, -628(%rbp)
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
## BB#27:
	movq	-624(%rbp), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
LBB14_28:                               ## %unwind_resume
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__Unwind_Resume
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table14:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset45 = Ltmp103-Lfunc_begin3           ## >> Call Site 1 <<
	.long	Lset45
Lset46 = Ltmp104-Ltmp103                ##   Call between Ltmp103 and Ltmp104
	.long	Lset46
Lset47 = Ltmp105-Lfunc_begin3           ##     jumps to Ltmp105
	.long	Lset47
	.byte	0                       ##   On action: cleanup
Lset48 = Ltmp106-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset48
Lset49 = Ltmp107-Ltmp106                ##   Call between Ltmp106 and Ltmp107
	.long	Lset49
Lset50 = Ltmp108-Lfunc_begin3           ##     jumps to Ltmp108
	.long	Lset50
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp107-Lfunc_begin3           ## >> Call Site 3 <<
	.long	Lset51
Lset52 = Ltmp109-Ltmp107                ##   Call between Ltmp107 and Ltmp109
	.long	Lset52
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset53 = Ltmp109-Lfunc_begin3           ## >> Call Site 4 <<
	.long	Lset53
Lset54 = Ltmp110-Ltmp109                ##   Call between Ltmp109 and Ltmp110
	.long	Lset54
Lset55 = Ltmp111-Lfunc_begin3           ##     jumps to Ltmp111
	.long	Lset55
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp112-Lfunc_begin3           ## >> Call Site 5 <<
	.long	Lset56
Lset57 = Ltmp131-Ltmp112                ##   Call between Ltmp112 and Ltmp131
	.long	Lset57
Lset58 = Ltmp132-Lfunc_begin3           ##     jumps to Ltmp132
	.long	Lset58
	.byte	0                       ##   On action: cleanup
Lset59 = Ltmp131-Lfunc_begin3           ## >> Call Site 6 <<
	.long	Lset59
Lset60 = Lfunc_end3-Ltmp131             ##   Call between Ltmp131 and Lfunc_end3
	.long	Lset60
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN4uuidC2E10big_endianjtthhSt16initializer_listIhE
	.weak_def_can_be_hidden	__ZN4uuidC2E10big_endianjtthhSt16initializer_listIhE
	.align	4, 0x90
__ZN4uuidC2E10big_endianjtthhSt16initializer_listIhE: ## @_ZN4uuidC2E10big_endianjtthhSt16initializer_listIhE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp136:
	.cfi_def_cfa_offset 16
Ltmp137:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp138:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
Ltmp139:
	.cfi_offset %rbx, -48
Ltmp140:
	.cfi_offset %r12, -40
Ltmp141:
	.cfi_offset %r14, -32
Ltmp142:
	.cfi_offset %r15, -24
	movb	%r9b, %al
	movb	%r8b, %r10b
	movw	%cx, %r11w
	movw	%dx, %bx
	leaq	16(%rbp), %r14
	movq	%rdi, -192(%rbp)
	movl	%esi, -196(%rbp)
	movw	%bx, -198(%rbp)
	movw	%r11w, -200(%rbp)
	movb	%r10b, -201(%rbp)
	movb	%al, -202(%rbp)
	movq	-192(%rbp), %rdi
	movl	-196(%rbp), %ecx
	movq	%rdi, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r15
	addq	%r15, %rdi
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	movl	%ecx, %edi
	movq	-256(%rbp), %rsi        ## 8-byte Reload
	movq	%r14, -264(%rbp)        ## 8-byte Spill
	callq	__Z5to_bejPh
	movq	%rax, -216(%rbp)
	movw	-198(%rbp), %r11w
	movq	-216(%rbp), %rsi
	movzwl	%r11w, %edi
	callq	__Z5to_betPh
	movq	%rax, -216(%rbp)
	movw	-200(%rbp), %r11w
	movq	-216(%rbp), %rsi
	movzwl	%r11w, %edi
	callq	__Z5to_betPh
	leaq	-80(%rbp), %rsi
	leaq	-56(%rbp), %r14
	movq	%rax, -216(%rbp)
	movb	-201(%rbp), %r10b
	movq	-216(%rbp), %rax
	movq	%rax, %r15
	addq	$1, %r15
	movq	%r15, -216(%rbp)
	movb	%r10b, (%rax)
	movb	-202(%rbp), %r10b
	movq	-216(%rbp), %rax
	movq	%rax, %r15
	addq	$1, %r15
	movq	%r15, -216(%rbp)
	movb	%r10b, (%rax)
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %r15
	movq	%r15, -232(%rbp)
	movq	8(%rax), %r15
	movq	%r15, -224(%rbp)
	movq	-232(%rbp), %r15
	movq	-224(%rbp), %r12
	movq	%r15, -56(%rbp)
	movq	%r12, -48(%rbp)
	movq	%r14, -40(%rbp)
	movq	-40(%rbp), %r14
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r15, -248(%rbp)
	movq	8(%rax), %r15
	movq	%r15, -240(%rbp)
	movq	-248(%rbp), %r15
	movq	-240(%rbp), %r12
	movq	%r15, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	(%rsi), %r15
	addq	8(%rsi), %r15
	movq	-216(%rbp), %rsi
	movq	%r14, -144(%rbp)
	movq	%r15, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-144(%rbp), %rsi
	movq	%rsi, -136(%rbp)
	movq	-136(%rbp), %rsi
	movq	-152(%rbp), %r14
	movq	%r14, -88(%rbp)
	movq	-88(%rbp), %r14
	movq	-160(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	-96(%rbp), %r15
	movq	%rsi, -104(%rbp)
	movq	%r14, -112(%rbp)
	movq	%r15, -120(%rbp)
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r14
	subq	%r14, %rsi
	movq	%rsi, -128(%rbp)
	cmpq	$0, -128(%rbp)
	jbe	LBB15_2
## BB#1:
	movq	-120(%rbp), %rdi
	movq	-104(%rbp), %rsi
	movq	-128(%rbp), %rdx
	callq	_memmove
LBB15_2:                                ## %_ZNSt3__14copyIPKhPhEET0_T_S5_S4_.exit
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN4uuidC2E13little_endianjtthhSt16initializer_listIhE
	.weak_def_can_be_hidden	__ZN4uuidC2E13little_endianjtthhSt16initializer_listIhE
	.align	4, 0x90
__ZN4uuidC2E13little_endianjtthhSt16initializer_listIhE: ## @_ZN4uuidC2E13little_endianjtthhSt16initializer_listIhE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp143:
	.cfi_def_cfa_offset 16
Ltmp144:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp145:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
	subq	$240, %rsp
Ltmp146:
	.cfi_offset %rbx, -48
Ltmp147:
	.cfi_offset %r12, -40
Ltmp148:
	.cfi_offset %r14, -32
Ltmp149:
	.cfi_offset %r15, -24
	movb	%r9b, %al
	movb	%r8b, %r10b
	movw	%cx, %r11w
	movw	%dx, %bx
	leaq	16(%rbp), %r14
	movq	%rdi, -192(%rbp)
	movl	%esi, -196(%rbp)
	movw	%bx, -198(%rbp)
	movw	%r11w, -200(%rbp)
	movb	%r10b, -201(%rbp)
	movb	%al, -202(%rbp)
	movq	-192(%rbp), %rdi
	movl	-196(%rbp), %ecx
	movq	%rdi, -168(%rbp)
	movq	$0, -176(%rbp)
	movq	-168(%rbp), %rdi
	movq	-176(%rbp), %r15
	addq	%r15, %rdi
	movq	%rdi, -256(%rbp)        ## 8-byte Spill
	movl	%ecx, %edi
	movq	-256(%rbp), %rsi        ## 8-byte Reload
	movq	%r14, -264(%rbp)        ## 8-byte Spill
	callq	__Z5to_lejPh
	movq	%rax, -216(%rbp)
	movw	-198(%rbp), %r11w
	movq	-216(%rbp), %rsi
	movzwl	%r11w, %edi
	callq	__Z5to_letPh
	movq	%rax, -216(%rbp)
	movw	-200(%rbp), %r11w
	movq	-216(%rbp), %rsi
	movzwl	%r11w, %edi
	callq	__Z5to_letPh
	leaq	-80(%rbp), %rsi
	leaq	-56(%rbp), %r14
	movq	%rax, -216(%rbp)
	movb	-201(%rbp), %r10b
	movq	-216(%rbp), %rax
	movq	%rax, %r15
	addq	$1, %r15
	movq	%r15, -216(%rbp)
	movb	%r10b, (%rax)
	movb	-202(%rbp), %r10b
	movq	-216(%rbp), %rax
	movq	%rax, %r15
	addq	$1, %r15
	movq	%r15, -216(%rbp)
	movb	%r10b, (%rax)
	movq	-264(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %r15
	movq	%r15, -232(%rbp)
	movq	8(%rax), %r15
	movq	%r15, -224(%rbp)
	movq	-232(%rbp), %r15
	movq	-224(%rbp), %r12
	movq	%r15, -56(%rbp)
	movq	%r12, -48(%rbp)
	movq	%r14, -40(%rbp)
	movq	-40(%rbp), %r14
	movq	(%r14), %r14
	movq	(%rax), %r15
	movq	%r15, -248(%rbp)
	movq	8(%rax), %r15
	movq	%r15, -240(%rbp)
	movq	-248(%rbp), %r15
	movq	-240(%rbp), %r12
	movq	%r15, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	-64(%rbp), %rsi
	movq	(%rsi), %r15
	addq	8(%rsi), %r15
	movq	-216(%rbp), %rsi
	movq	%r14, -144(%rbp)
	movq	%r15, -152(%rbp)
	movq	%rsi, -160(%rbp)
	movq	-144(%rbp), %rsi
	movq	%rsi, -136(%rbp)
	movq	-136(%rbp), %rsi
	movq	-152(%rbp), %r14
	movq	%r14, -88(%rbp)
	movq	-88(%rbp), %r14
	movq	-160(%rbp), %r15
	movq	%r15, -96(%rbp)
	movq	-96(%rbp), %r15
	movq	%rsi, -104(%rbp)
	movq	%r14, -112(%rbp)
	movq	%r15, -120(%rbp)
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %r14
	subq	%r14, %rsi
	movq	%rsi, -128(%rbp)
	cmpq	$0, -128(%rbp)
	jbe	LBB16_2
## BB#1:
	movq	-120(%rbp), %rdi
	movq	-104(%rbp), %rsi
	movq	-128(%rbp), %rdx
	callq	_memmove
LBB16_2:                                ## %_ZNSt3__14copyIPKhPhEET0_T_S5_S4_.exit
	addq	$240, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.weak_def_can_be_hidden	__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.align	4, 0x90
__ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c: ## @_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_c
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp150:
	.cfi_def_cfa_offset 16
Ltmp151:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp152:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movb	%sil, %al
	leaq	-9(%rbp), %rsi
	movl	$1, %ecx
	movl	%ecx, %edx
	movq	%rdi, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
	.weak_def_can_be_hidden	__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
	.align	4, 0x90
__ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm: ## @_ZNK4uuid5chunkERNSt3__113basic_ostreamIcNS0_11char_traitsIcEEEEmm
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp153:
	.cfi_def_cfa_offset 16
Ltmp154:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp155:
	.cfi_def_cfa_register %rbp
	subq	$272, %rsp              ## imm = 0x110
	movq	%rdi, -192(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rdx, -208(%rbp)
	movq	%rcx, -216(%rbp)
	movq	-192(%rbp), %rcx
	movq	-208(%rbp), %rdx
	cmpq	-216(%rbp), %rdx
	seta	%al
	andb	$1, %al
	movb	%al, -217(%rbp)
	movq	%rcx, -240(%rbp)        ## 8-byte Spill
LBB18_1:                                ## =>This Inner Loop Header: Depth=1
	movq	-208(%rbp), %rax
	cmpq	-216(%rbp), %rax
	je	LBB18_7
## BB#2:                                ##   in Loop: Header=BB18_1 Depth=1
	testb	$1, -217(%rbp)
	je	LBB18_4
## BB#3:                                ##   in Loop: Header=BB18_1 Depth=1
	movq	-208(%rbp), %rax
	addq	$-1, %rax
	movq	%rax, -208(%rbp)
LBB18_4:                                ##   in Loop: Header=BB18_1 Depth=1
	movq	-200(%rbp), %rax
	movq	%rax, -176(%rbp)
	leaq	__ZNSt3__13hexERNS_8ios_baseE(%rip), %rax
	movq	%rax, -184(%rbp)
	movq	-176(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	movq	%rcx, %rsi
	addq	%rdx, %rsi
	movq	%rsi, %rdi
	movq	%rcx, -248(%rbp)        ## 8-byte Spill
	callq	*%rax
	movl	$2, -164(%rbp)
	leaq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movl	$2, -156(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movl	$2, -140(%rbp)
	movq	-136(%rbp), %rcx
	movl	$2, (%rcx)
	movl	-160(%rbp), %r8d
	movl	%r8d, -224(%rbp)
	movq	-248(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	leaq	-224(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movslq	-224(%rbp), %rsi
	movq	%rdx, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdx
	movq	24(%rdx), %rsi
	movq	%rsi, -24(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, 24(%rdx)
	movq	-32(%rbp), %rdx
	movb	$48, -73(%rbp)
	leaq	-72(%rbp), %rsi
	movq	%rsi, -64(%rbp)
	movb	$48, -65(%rbp)
	movq	-64(%rbp), %rsi
	movq	%rsi, -48(%rbp)
	movb	$48, -49(%rbp)
	movq	-48(%rbp), %rsi
	movb	$48, (%rsi)
	movb	-72(%rbp), %r9b
	movb	%r9b, -232(%rbp)
	movq	%rdx, -104(%rbp)
	leaq	-232(%rbp), %rdx
	movq	%rdx, -112(%rbp)
	movq	-104(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movb	-232(%rbp), %r9b
	movq	%rdx, -88(%rbp)
	movb	%r9b, -89(%rbp)
	movq	-88(%rbp), %rdx
	movb	144(%rdx), %r9b
	movb	%r9b, -90(%rbp)
	movsbl	-89(%rbp), %r8d
	movl	%r8d, 144(%rdx)
	movq	-104(%rbp), %rdi
	movq	-208(%rbp), %rdx
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-120(%rbp), %r10
	movb	(%r10,%rdx), %r9b
	movzbl	%r9b, %r8d
	movw	%r8w, %r11w
	movzwl	%r11w, %esi
	movq	%rax, -256(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEt
	testb	$1, -217(%rbp)
	movq	%rax, -264(%rbp)        ## 8-byte Spill
	jne	LBB18_6
## BB#5:                                ##   in Loop: Header=BB18_1 Depth=1
	movq	-208(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -208(%rbp)
LBB18_6:                                ##   in Loop: Header=BB18_1 Depth=1
	jmp	LBB18_1
LBB18_7:
	movq	-200(%rbp), %rax
	addq	$272, %rsp              ## imm = 0x110
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp156:
	.cfi_def_cfa_offset 16
Ltmp157:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp158:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rsi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	movq	-16(%rbp), %rsi         ## 8-byte Reload
	addq	$112, %rsi
	movq	%rsi, %rdi
	callq	__ZNSt3__19basic_iosIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp159:
	.cfi_def_cfa_offset 16
Ltmp160:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp161:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp162:
	.cfi_def_cfa_offset 16
Ltmp163:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp164:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp165:
	.cfi_def_cfa_offset 16
Ltmp166:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp167:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	(%rdi), %rax
	movq	-24(%rax), %rax
	addq	%rax, %rdi
	popq	%rbp
	jmp	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp168:
	.cfi_def_cfa_offset 16
Ltmp169:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp170:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strERKNS_12basic_stringIcS2_S4_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp171:
	.cfi_def_cfa_offset 16
Ltmp172:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp173:
	.cfi_def_cfa_register %rbp
	subq	$1312, %rsp             ## imm = 0x520
	movq	%rdi, -1064(%rbp)
	movq	%rsi, -1072(%rbp)
	movq	-1064(%rbp), %rsi
	movq	%rsi, %rdi
	addq	$64, %rdi
	movq	-1072(%rbp), %rax
	movq	%rsi, -1088(%rbp)       ## 8-byte Spill
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSERKS5_
	movq	-1088(%rbp), %rsi       ## 8-byte Reload
	movq	$0, 88(%rsi)
	movl	96(%rsi), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	movq	%rax, -1096(%rbp)       ## 8-byte Spill
	je	LBB24_14
## BB#1:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rax
	movq	%rax, -1048(%rbp)
	movq	-1048(%rbp), %rax
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	movq	%rcx, -1024(%rbp)
	movq	-1024(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1104(%rbp)       ## 8-byte Spill
	je	LBB24_3
## BB#2:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rcx
	movq	%rcx, -968(%rbp)
	movq	-968(%rbp), %rcx
	movq	%rcx, -960(%rbp)
	movq	-960(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
	jmp	LBB24_4
LBB24_3:
	movq	-1104(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1016(%rbp)
	movq	-1016(%rbp), %rcx
	movq	%rcx, -1008(%rbp)
	movq	-1008(%rbp), %rcx
	movq	%rcx, -1000(%rbp)
	movq	-1000(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -992(%rbp)
	movq	-992(%rbp), %rcx
	movq	%rcx, -984(%rbp)
	movq	-984(%rbp), %rcx
	movq	%rcx, -1112(%rbp)       ## 8-byte Spill
LBB24_4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-1112(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -584(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, -576(%rbp)
	movq	-576(%rbp), %rdx
	movq	%rdx, -568(%rbp)
	movq	-568(%rbp), %rdx
	movq	%rdx, -560(%rbp)
	movq	-560(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1120(%rbp)       ## 8-byte Spill
	movq	%rcx, -1128(%rbp)       ## 8-byte Spill
	je	LBB24_6
## BB#5:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	movq	%rcx, -512(%rbp)
	movq	-512(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
	jmp	LBB24_7
LBB24_6:
	movq	-1128(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -552(%rbp)
	movq	-552(%rbp), %rcx
	movq	%rcx, -544(%rbp)
	movq	-544(%rbp), %rcx
	movq	%rcx, -536(%rbp)
	movq	-536(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1136(%rbp)       ## 8-byte Spill
LBB24_7:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit3
	movq	-1136(%rbp), %rax       ## 8-byte Reload
	movq	-1120(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	%rcx, 88(%rax)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rdx
	movq	%rdx, -88(%rbp)
	movq	-88(%rbp), %rdx
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1144(%rbp)       ## 8-byte Spill
	movq	%rcx, -1152(%rbp)       ## 8-byte Spill
	je	LBB24_9
## BB#8:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
	jmp	LBB24_10
LBB24_9:
	movq	-1152(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -1160(%rbp)       ## 8-byte Spill
LBB24_10:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit7
	movq	-1160(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rdx
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1168(%rbp)       ## 8-byte Spill
	movq	%rcx, -1176(%rbp)       ## 8-byte Spill
	je	LBB24_12
## BB#11:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
	jmp	LBB24_13
LBB24_12:
	movq	-1176(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -1184(%rbp)       ## 8-byte Spill
LBB24_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit6
	movq	-1184(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	88(%rcx), %rdx
	movq	-1144(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -232(%rbp)
	movq	-1168(%rbp), %rdi       ## 8-byte Reload
	movq	%rdi, -240(%rbp)
	movq	%rax, -248(%rbp)
	movq	%rdx, -256(%rbp)
	movq	-232(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-256(%rbp), %rdx
	movq	%rdx, 32(%rax)
LBB24_14:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	je	LBB24_36
## BB#15:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -336(%rbp)
	movq	-336(%rbp), %rax
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rcx
	movq	%rcx, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1192(%rbp)       ## 8-byte Spill
	je	LBB24_17
## BB#16:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
	jmp	LBB24_18
LBB24_17:
	movq	-1192(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1200(%rbp)       ## 8-byte Spill
LBB24_18:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit5
	movq	-1200(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1080(%rbp)
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	%rcx, -416(%rbp)
	movq	-416(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1208(%rbp)       ## 8-byte Spill
	je	LBB24_20
## BB#19:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	%rcx, -352(%rbp)
	movq	-352(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
	jmp	LBB24_21
LBB24_20:
	movq	-1208(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -1216(%rbp)       ## 8-byte Spill
LBB24_21:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit4
	movq	-1216(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rax
	addq	-1080(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movq	%rax, 88(%rcx)
	addq	$64, %rcx
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rax
	movq	%rax, -496(%rbp)
	movq	-496(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	-480(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -1224(%rbp)       ## 8-byte Spill
	movq	%rax, -1232(%rbp)       ## 8-byte Spill
	je	LBB24_23
## BB#22:
	movq	-1232(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB24_24
LBB24_23:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -1240(%rbp)       ## 8-byte Spill
	jmp	LBB24_24
LBB24_24:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-1240(%rbp), %rax       ## 8-byte Reload
	xorl	%edx, %edx
	subq	$1, %rax
	movq	-1224(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -592(%rbp)
	movq	%rax, -600(%rbp)
	movq	-592(%rbp), %rdi
	movq	-600(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -712(%rbp)
	movq	-712(%rbp), %rcx
	movq	%rcx, -704(%rbp)
	movq	-704(%rbp), %rcx
	movq	%rcx, -696(%rbp)
	movq	-696(%rbp), %rsi
	movq	%rsi, -688(%rbp)
	movq	-688(%rbp), %rsi
	movq	%rsi, -680(%rbp)
	movq	-680(%rbp), %rsi
	movzbl	(%rsi), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -1248(%rbp)       ## 8-byte Spill
	movq	%rcx, -1256(%rbp)       ## 8-byte Spill
	je	LBB24_26
## BB#25:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -632(%rbp)
	movq	-632(%rbp), %rcx
	movq	%rcx, -624(%rbp)
	movq	-624(%rbp), %rcx
	movq	%rcx, -616(%rbp)
	movq	-616(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
	jmp	LBB24_27
LBB24_26:
	movq	-1256(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -672(%rbp)
	movq	-672(%rbp), %rcx
	movq	%rcx, -664(%rbp)
	movq	-664(%rbp), %rcx
	movq	%rcx, -656(%rbp)
	movq	-656(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -648(%rbp)
	movq	-648(%rbp), %rcx
	movq	%rcx, -640(%rbp)
	movq	-640(%rbp), %rcx
	movq	%rcx, -1264(%rbp)       ## 8-byte Spill
LBB24_27:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-1264(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -824(%rbp)
	movq	-824(%rbp), %rcx
	movq	%rcx, -816(%rbp)
	movq	-816(%rbp), %rcx
	movq	%rcx, -808(%rbp)
	movq	-808(%rbp), %rdx
	movq	%rdx, -800(%rbp)
	movq	-800(%rbp), %rdx
	movq	%rdx, -792(%rbp)
	movq	-792(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1272(%rbp)       ## 8-byte Spill
	movq	%rcx, -1280(%rbp)       ## 8-byte Spill
	je	LBB24_29
## BB#28:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -744(%rbp)
	movq	-744(%rbp), %rcx
	movq	%rcx, -736(%rbp)
	movq	-736(%rbp), %rcx
	movq	%rcx, -728(%rbp)
	movq	-728(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
	jmp	LBB24_30
LBB24_29:
	movq	-1280(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -784(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -776(%rbp)
	movq	-776(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	-768(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -760(%rbp)
	movq	-760(%rbp), %rcx
	movq	%rcx, -752(%rbp)
	movq	-752(%rbp), %rcx
	movq	%rcx, -1288(%rbp)       ## 8-byte Spill
LBB24_30:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-1288(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -720(%rbp)
	movq	-720(%rbp), %rax
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -904(%rbp)
	movq	-904(%rbp), %rcx
	movq	%rcx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	%rdx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -1296(%rbp)       ## 8-byte Spill
	movq	%rcx, -1304(%rbp)       ## 8-byte Spill
	je	LBB24_32
## BB#31:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -848(%rbp)
	movq	-848(%rbp), %rcx
	movq	%rcx, -840(%rbp)
	movq	-840(%rbp), %rcx
	movq	%rcx, -832(%rbp)
	movq	-832(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
	jmp	LBB24_33
LBB24_32:
	movq	-1304(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -872(%rbp)
	movq	-872(%rbp), %rcx
	movq	%rcx, -864(%rbp)
	movq	-864(%rbp), %rcx
	movq	%rcx, -856(%rbp)
	movq	-856(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -1312(%rbp)       ## 8-byte Spill
LBB24_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-1312(%rbp), %rax       ## 8-byte Reload
	movq	-1296(%rbp), %rcx       ## 8-byte Reload
	addq	%rax, %rcx
	movq	-1248(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -912(%rbp)
	movq	-1272(%rbp), %rdx       ## 8-byte Reload
	movq	%rdx, -920(%rbp)
	movq	%rcx, -928(%rbp)
	movq	-912(%rbp), %rcx
	movq	-920(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-928(%rbp), %rsi
	movq	%rsi, 56(%rcx)
	movq	-1088(%rbp), %rcx       ## 8-byte Reload
	movl	96(%rcx), %edi
	andl	$3, %edi
	cmpl	$0, %edi
	je	LBB24_35
## BB#34:
	movq	-1088(%rbp), %rax       ## 8-byte Reload
	movq	-1080(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -936(%rbp)
	movl	%edx, -940(%rbp)
	movq	-936(%rbp), %rax
	movl	-940(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
LBB24_35:
	jmp	LBB24_36
LBB24_36:
	addq	$1312, %rsp             ## imm = 0x520
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp174:
	.cfi_def_cfa_offset 16
Ltmp175:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp176:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp177:
	.cfi_def_cfa_offset 16
Ltmp178:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp179:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp180:
	.cfi_def_cfa_offset 16
Ltmp181:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp182:
	.cfi_def_cfa_register %rbp
	subq	$800, %rsp              ## imm = 0x320
	movq	%rdi, %rax
	movq	%rsi, -624(%rbp)
	movq	%rdx, -632(%rbp)
	movl	%ecx, -636(%rbp)
	movl	%r8d, -640(%rbp)
	movq	-624(%rbp), %rdx
	movq	88(%rdx), %rsi
	movq	%rdx, %r9
	movq	%r9, -616(%rbp)
	movq	-616(%rbp), %r9
	cmpq	48(%r9), %rsi
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	movq	%rdi, -664(%rbp)        ## 8-byte Spill
	movq	%rdx, -672(%rbp)        ## 8-byte Spill
	jae	LBB27_2
## BB#1:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB27_2:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$0, %eax
	jne	LBB27_4
## BB#3:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -32(%rbp)
	movq	$-1, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %r8
	movq	%rdi, -16(%rbp)
	movq	%r8, -24(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-24(%rbp), %rcx
	movq	-680(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_4:
	movl	-640(%rbp), %eax
	andl	$24, %eax
	cmpl	$24, %eax
	jne	LBB27_7
## BB#5:
	cmpl	$1, -636(%rbp)
	jne	LBB27_7
## BB#6:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	$-1, -72(%rbp)
	movq	-64(%rbp), %rdi
	movq	-72(%rbp), %r8
	movq	%rdi, -48(%rbp)
	movq	%r8, -56(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-56(%rbp), %rcx
	movq	-688(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_7:
	movl	-636(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -692(%rbp)        ## 4-byte Spill
	je	LBB27_8
	jmp	LBB27_38
LBB27_38:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -696(%rbp)        ## 4-byte Spill
	je	LBB27_9
	jmp	LBB27_39
LBB27_39:
	movl	-692(%rbp), %eax        ## 4-byte Reload
	subl	$2, %eax
	movl	%eax, -700(%rbp)        ## 4-byte Spill
	je	LBB27_13
	jmp	LBB27_17
LBB27_8:
	movq	$0, -648(%rbp)
	jmp	LBB27_18
LBB27_9:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_11
## BB#10:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	24(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	jmp	LBB27_12
LBB27_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	48(%rax), %rax
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	40(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
LBB27_12:
	jmp	LBB27_18
LBB27_13:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rdx
	movq	%rdx, -192(%rbp)
	movq	-192(%rbp), %rdx
	movq	%rdx, -184(%rbp)
	movq	-184(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -712(%rbp)        ## 8-byte Spill
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	je	LBB27_15
## BB#14:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB27_16
LBB27_15:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB27_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit1
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, -648(%rbp)
	jmp	LBB27_18
LBB27_17:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -240(%rbp)
	movq	$-1, -248(%rbp)
	movq	-240(%rbp), %rdi
	movq	-248(%rbp), %r8
	movq	%rdi, -224(%rbp)
	movq	%r8, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -736(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-232(%rbp), %rcx
	movq	-736(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_18:
	movq	-632(%rbp), %rax
	addq	-648(%rbp), %rax
	movq	%rax, -648(%rbp)
	cmpq	$0, -648(%rbp)
	jl	LBB27_23
## BB#19:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	addq	$64, %rax
	movq	%rax, -360(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -344(%rbp)
	movq	-344(%rbp), %rdx
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	%rdx, -328(%rbp)
	movq	-328(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rcx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	je	LBB27_21
## BB#20:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
	jmp	LBB27_22
LBB27_21:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rcx
	movq	%rcx, -312(%rbp)
	movq	-312(%rbp), %rcx
	movq	%rcx, -304(%rbp)
	movq	-304(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -296(%rbp)
	movq	-296(%rbp), %rcx
	movq	%rcx, -288(%rbp)
	movq	-288(%rbp), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
LBB27_22:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -256(%rbp)
	movq	-256(%rbp), %rax
	movq	-744(%rbp), %rcx        ## 8-byte Reload
	subq	%rax, %rcx
	cmpq	-648(%rbp), %rcx
	jge	LBB27_24
LBB27_23:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -384(%rbp)
	movq	$-1, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	-392(%rbp), %r8
	movq	%rdi, -368(%rbp)
	movq	%r8, -376(%rbp)
	movq	-368(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -768(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-376(%rbp), %rcx
	movq	-768(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_24:
	cmpq	$0, -648(%rbp)
	je	LBB27_32
## BB#25:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_28
## BB#26:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -400(%rbp)
	movq	-400(%rbp), %rax
	cmpq	$0, 24(%rax)
	jne	LBB27_28
## BB#27:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -424(%rbp)
	movq	$-1, -432(%rbp)
	movq	-424(%rbp), %rdi
	movq	-432(%rbp), %r8
	movq	%rdi, -408(%rbp)
	movq	%r8, -416(%rbp)
	movq	-408(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -776(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-416(%rbp), %rcx
	movq	-776(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_28:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB27_31
## BB#29:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rax
	cmpq	$0, 48(%rax)
	jne	LBB27_31
## BB#30:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-664(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -464(%rbp)
	movq	$-1, -472(%rbp)
	movq	-464(%rbp), %rdi
	movq	-472(%rbp), %r8
	movq	%rdi, -448(%rbp)
	movq	%r8, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, %r8
	movq	%rdi, -784(%rbp)        ## 8-byte Spill
	movq	%r8, %rdi
	callq	_memset
	movq	-456(%rbp), %rcx
	movq	-784(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
	jmp	LBB27_37
LBB27_31:
	jmp	LBB27_32
LBB27_32:
	movl	-640(%rbp), %eax
	andl	$8, %eax
	cmpl	$0, %eax
	je	LBB27_34
## BB#33:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -488(%rbp)
	movq	-488(%rbp), %rdx
	movq	16(%rdx), %rdx
	addq	-648(%rbp), %rdx
	movq	-672(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -496(%rbp)
	movq	%rcx, -504(%rbp)
	movq	%rdx, -512(%rbp)
	movq	%rdi, -520(%rbp)
	movq	-496(%rbp), %rax
	movq	-504(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-512(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-520(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB27_34:
	movl	-640(%rbp), %eax
	andl	$16, %eax
	cmpl	$0, %eax
	je	LBB27_36
## BB#35:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-672(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	-672(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -536(%rbp)
	movq	-536(%rbp), %rdx
	movq	56(%rdx), %rdx
	movq	%rax, -544(%rbp)
	movq	%rcx, -552(%rbp)
	movq	%rdx, -560(%rbp)
	movq	-544(%rbp), %rax
	movq	-552(%rbp), %rcx
	movq	%rcx, 48(%rax)
	movq	%rcx, 40(%rax)
	movq	-560(%rbp), %rcx
	movq	%rcx, 56(%rax)
	movq	-672(%rbp), %rax        ## 8-byte Reload
	movq	-648(%rbp), %rcx
	movl	%ecx, %esi
	movq	%rax, -568(%rbp)
	movl	%esi, -572(%rbp)
	movq	-568(%rbp), %rax
	movl	-572(%rbp), %esi
	movq	48(%rax), %rcx
	movslq	%esi, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 48(%rax)
LBB27_36:
	xorl	%esi, %esi
	movl	$128, %eax
	movl	%eax, %edx
	movq	-648(%rbp), %rcx
	movq	-664(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -600(%rbp)
	movq	%rcx, -608(%rbp)
	movq	-600(%rbp), %rcx
	movq	-608(%rbp), %r8
	movq	%rcx, -584(%rbp)
	movq	%r8, -592(%rbp)
	movq	-584(%rbp), %rcx
	movq	%rcx, %r8
	movq	%r8, %rdi
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-592(%rbp), %rcx
	movq	-792(%rbp), %rdx        ## 8-byte Reload
	movq	%rcx, 128(%rdx)
LBB27_37:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	addq	$800, %rsp              ## imm = 0x320
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp183:
	.cfi_def_cfa_offset 16
Ltmp184:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp185:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, %rax
	leaq	16(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%rsi, -16(%rbp)
	movl	%edx, -20(%rbp)
	movq	-16(%rbp), %rsi
	movq	(%rsi), %r9
	movq	32(%r9), %r9
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	128(%rcx), %rdx
	movl	-20(%rbp), %r10d
	movl	%r8d, %ecx
	movl	%r10d, %r8d
	movq	%rax, -32(%rbp)         ## 8-byte Spill
	callq	*%r9
	movq	-32(%rbp), %rax         ## 8-byte Reload
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp186:
	.cfi_def_cfa_offset 16
Ltmp187:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp188:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -120(%rbp)        ## 8-byte Spill
	jae	LBB29_2
## BB#1:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	48(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB29_2:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB29_8
## BB#3:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	32(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	cmpq	88(%rcx), %rax
	jae	LBB29_5
## BB#4:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-120(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -16(%rbp)
	movq	-16(%rbp), %rdx
	movq	24(%rdx), %rdx
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -24(%rbp)
	movq	%rcx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-24(%rbp), %rax
	movq	-32(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-40(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-48(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB29_5:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	24(%rax), %rax
	movq	-120(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	32(%rcx), %rax
	jae	LBB29_7
## BB#6:
	movq	-120(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	24(%rax), %rax
	movsbl	(%rax), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -100(%rbp)
	jmp	LBB29_9
LBB29_7:
	jmp	LBB29_8
LBB29_8:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -100(%rbp)
LBB29_9:
	movl	-100(%rbp), %eax
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp189:
	.cfi_def_cfa_offset 16
Ltmp190:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp191:
	.cfi_def_cfa_register %rbp
	subq	$192, %rsp
	movq	%rdi, -160(%rbp)
	movl	%esi, -164(%rbp)
	movq	-160(%rbp), %rdi
	movq	88(%rdi), %rax
	movq	%rdi, %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	cmpq	48(%rcx), %rax
	movq	%rdi, -176(%rbp)        ## 8-byte Spill
	jae	LBB30_2
## BB#1:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	48(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB30_2:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -120(%rbp)
	movq	-120(%rbp), %rax
	movq	16(%rax), %rax
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	cmpq	24(%rcx), %rax
	jae	LBB30_9
## BB#3:
	movl	-164(%rbp), %edi
	movl	%edi, -180(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-180(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB30_4
	jmp	LBB30_5
LBB30_4:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -8(%rbp)
	movq	%rcx, -16(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-8(%rbp), %rax
	movq	-16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-24(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-32(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -148(%rbp)
	jmp	LBB30_10
LBB30_5:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB30_7
## BB#6:
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	24(%rcx), %rcx
	movsbl	%al, %edi
	movsbl	-1(%rcx), %esi
	callq	__ZNSt3__111char_traitsIcE2eqEcc
	testb	$1, %al
	jne	LBB30_7
	jmp	LBB30_8
LBB30_7:
	movq	-176(%rbp), %rax        ## 8-byte Reload
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	-176(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -80(%rbp)
	movq	-80(%rbp), %rdx
	movq	24(%rdx), %rdx
	addq	$-1, %rdx
	movq	-176(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -88(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rdi, -112(%rbp)
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-104(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-112(%rbp), %rcx
	movq	%rcx, 32(%rax)
	movl	-164(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE12to_char_typeEi
	movq	-176(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	24(%rcx), %rcx
	movb	%al, (%rcx)
	movl	-164(%rbp), %edi
	movl	%edi, -148(%rbp)
	jmp	LBB30_10
LBB30_8:
	jmp	LBB30_9
LBB30_9:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -148(%rbp)
LBB30_10:
	movl	-148(%rbp), %eax
	addq	$192, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp197:
	.cfi_def_cfa_offset 16
Ltmp198:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp199:
	.cfi_def_cfa_register %rbp
	subq	$896, %rsp              ## imm = 0x380
	movq	%rdi, -632(%rbp)
	movl	%esi, -636(%rbp)
	movq	-632(%rbp), %rdi
	movl	-636(%rbp), %esi
	movq	%rdi, -712(%rbp)        ## 8-byte Spill
	movl	%esi, -716(%rbp)        ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-716(%rbp), %edi        ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB31_38
## BB#1:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -616(%rbp)
	movq	-616(%rbp), %rax
	movq	24(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -608(%rbp)
	movq	-608(%rbp), %rcx
	movq	16(%rcx), %rcx
	subq	%rcx, %rax
	movq	%rax, -648(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -576(%rbp)
	movq	-576(%rbp), %rax
	movq	48(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -568(%rbp)
	movq	-568(%rbp), %rcx
	cmpq	56(%rcx), %rax
	jne	LBB31_26
## BB#2:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	jne	LBB31_4
## BB#3:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	jmp	LBB31_39
LBB31_4:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -560(%rbp)
	movq	-560(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -728(%rbp)        ## 8-byte Spill
## BB#5:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -328(%rbp)
	movq	-328(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -736(%rbp)        ## 8-byte Spill
## BB#6:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	-736(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -656(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rdx
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rdx, -744(%rbp)        ## 8-byte Spill
	movq	%rax, -752(%rbp)        ## 8-byte Spill
## BB#7:
	movq	-744(%rbp), %rax        ## 8-byte Reload
	movq	-752(%rbp), %rcx        ## 8-byte Reload
	subq	%rcx, %rax
	movq	%rax, -680(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
Ltmp192:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE9push_backEc
Ltmp193:
	jmp	LBB31_8
LBB31_8:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	addq	$64, %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rdx
	movq	%rdx, -40(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -32(%rbp)
	movq	-32(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rax, -760(%rbp)        ## 8-byte Spill
	movq	%rcx, -768(%rbp)        ## 8-byte Spill
	je	LBB31_10
## BB#9:
	movq	-768(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	(%rcx), %rcx
	andq	$-2, %rcx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB31_11
LBB31_10:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -776(%rbp)        ## 8-byte Spill
	jmp	LBB31_11
LBB31_11:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE8capacityEv.exit
	movq	-776(%rbp), %rax        ## 8-byte Reload
	decq	%rax
	movq	-760(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rdi
Ltmp194:
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEmc
Ltmp195:
	jmp	LBB31_12
LBB31_12:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6resizeEm.exit
	jmp	LBB31_13
LBB31_13:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -784(%rbp)        ## 8-byte Spill
	je	LBB31_15
## BB#14:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
	jmp	LBB31_16
LBB31_15:
	movq	-784(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -792(%rbp)        ## 8-byte Spill
LBB31_16:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit2
	movq	-792(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -688(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-688(%rbp), %rcx
	movq	-688(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	addq	$64, %rsi
	movq	%rsi, -272(%rbp)
	movq	-272(%rbp), %rsi
	movq	%rsi, -264(%rbp)
	movq	-264(%rbp), %rdi
	movq	%rdi, -256(%rbp)
	movq	-256(%rbp), %rdi
	movq	%rdi, -248(%rbp)
	movq	-248(%rbp), %rdi
	movzbl	(%rdi), %r8d
	andl	$1, %r8d
	cmpl	$0, %r8d
	movq	%rax, -800(%rbp)        ## 8-byte Spill
	movq	%rcx, -808(%rbp)        ## 8-byte Spill
	movq	%rdx, -816(%rbp)        ## 8-byte Spill
	movq	%rsi, -824(%rbp)        ## 8-byte Spill
	je	LBB31_18
## BB#17:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	%rcx, -208(%rbp)
	movq	-208(%rbp), %rcx
	movq	%rcx, -200(%rbp)
	movq	-200(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
	jmp	LBB31_19
LBB31_18:
	movq	-824(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -832(%rbp)        ## 8-byte Spill
LBB31_19:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-832(%rbp), %rax        ## 8-byte Reload
	movq	-816(%rbp), %rcx        ## 8-byte Reload
	addq	%rax, %rcx
	movq	-800(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -280(%rbp)
	movq	-808(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -288(%rbp)
	movq	%rcx, -296(%rbp)
	movq	-280(%rbp), %rcx
	movq	-288(%rbp), %rsi
	movq	%rsi, 48(%rcx)
	movq	%rsi, 40(%rcx)
	movq	-296(%rbp), %rsi
	movq	%rsi, 56(%rcx)
## BB#20:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-656(%rbp), %rcx
	movl	%ecx, %edx
	movq	%rax, -304(%rbp)
	movl	%edx, -308(%rbp)
	movq	-304(%rbp), %rax
	movl	-308(%rbp), %edx
	movq	48(%rax), %rcx
	movslq	%edx, %rsi
	addq	%rsi, %rcx
	movq	%rcx, 48(%rax)
## BB#21:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -320(%rbp)
	movq	-320(%rbp), %rax
	movq	40(%rax), %rax
	movq	%rax, -840(%rbp)        ## 8-byte Spill
## BB#22:
	movq	-840(%rbp), %rax        ## 8-byte Reload
	addq	-680(%rbp), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	jmp	LBB31_25
LBB31_23:
Ltmp196:
	movl	%edx, %ecx
	movq	%rax, -664(%rbp)
	movl	%ecx, -668(%rbp)
## BB#24:
	movq	-664(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	%rax, -848(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	%eax, -620(%rbp)
	callq	___cxa_end_catch
	jmp	LBB31_39
LBB31_25:
	jmp	LBB31_26
LBB31_26:
	leaq	-368(%rbp), %rax
	leaq	-696(%rbp), %rcx
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdx
	movq	48(%rdx), %rdx
	addq	$1, %rdx
	movq	%rdx, -696(%rbp)
	movq	-712(%rbp), %rdx        ## 8-byte Reload
	addq	$88, %rdx
	movq	%rcx, -392(%rbp)
	movq	%rdx, -400(%rbp)
	movq	-392(%rbp), %rcx
	movq	-400(%rbp), %rdx
	movq	%rcx, -376(%rbp)
	movq	%rdx, -384(%rbp)
	movq	-376(%rbp), %rcx
	movq	-384(%rbp), %rdx
	movq	%rax, -344(%rbp)
	movq	%rcx, -352(%rbp)
	movq	%rdx, -360(%rbp)
	movq	-352(%rbp), %rax
	movq	(%rax), %rax
	movq	-360(%rbp), %rcx
	cmpq	(%rcx), %rax
	jae	LBB31_28
## BB#27:
	movq	-384(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
	jmp	LBB31_29
LBB31_28:
	movq	-376(%rbp), %rax
	movq	%rax, -856(%rbp)        ## 8-byte Spill
LBB31_29:                               ## %_ZNSt3__13maxIPcEERKT_S4_S4_.exit
	movq	-856(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rax
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
	movl	96(%rcx), %edx
	andl	$8, %edx
	cmpl	$0, %edx
	je	LBB31_34
## BB#30:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	addq	$64, %rax
	movq	%rax, -520(%rbp)
	movq	-520(%rbp), %rax
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	-504(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	-496(%rbp), %rcx
	movq	%rcx, -488(%rbp)
	movq	-488(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -864(%rbp)        ## 8-byte Spill
	je	LBB31_32
## BB#31:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
	jmp	LBB31_33
LBB31_32:
	movq	-864(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	%rcx, -464(%rbp)
	movq	-464(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -456(%rbp)
	movq	-456(%rbp), %rcx
	movq	%rcx, -448(%rbp)
	movq	-448(%rbp), %rcx
	movq	%rcx, -872(%rbp)        ## 8-byte Spill
LBB31_33:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-872(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -704(%rbp)
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movq	-704(%rbp), %rcx
	movq	-704(%rbp), %rdx
	addq	-648(%rbp), %rdx
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	movq	88(%rsi), %rdi
	movq	%rax, -528(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rdx, -544(%rbp)
	movq	%rdi, -552(%rbp)
	movq	-528(%rbp), %rax
	movq	-536(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	-544(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-552(%rbp), %rcx
	movq	%rcx, 32(%rax)
LBB31_34:
	movq	-712(%rbp), %rax        ## 8-byte Reload
	movl	-636(%rbp), %ecx
	movb	%cl, %dl
	movq	%rax, -592(%rbp)
	movb	%dl, -593(%rbp)
	movq	-592(%rbp), %rax
	movq	48(%rax), %rsi
	cmpq	56(%rax), %rsi
	movq	%rax, -880(%rbp)        ## 8-byte Spill
	jne	LBB31_36
## BB#35:
	movq	-880(%rbp), %rax        ## 8-byte Reload
	movq	(%rax), %rcx
	movq	104(%rcx), %rcx
	movsbl	-593(%rbp), %edi
	movq	%rcx, -888(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movq	-880(%rbp), %rdi        ## 8-byte Reload
	movl	%eax, %esi
	movq	-888(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
	movl	%eax, -580(%rbp)
	jmp	LBB31_37
LBB31_36:
	movb	-593(%rbp), %al
	movq	-880(%rbp), %rcx        ## 8-byte Reload
	movq	48(%rcx), %rdx
	movq	%rdx, %rsi
	addq	$1, %rsi
	movq	%rsi, 48(%rcx)
	movb	%al, (%rdx)
	movsbl	-593(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE11to_int_typeEc
	movl	%eax, -580(%rbp)
LBB31_37:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputcEc.exit
	movl	-580(%rbp), %eax
	movl	%eax, -620(%rbp)
	jmp	LBB31_39
LBB31_38:
	movl	-636(%rbp), %edi
	callq	__ZNSt3__111char_traitsIcE7not_eofEi
	movl	%eax, -620(%rbp)
LBB31_39:
	movl	-620(%rbp), %eax
	addq	$896, %rsp              ## imm = 0x380
	popq	%rbp
	retq
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table31:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\242\200\200"          ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	26                      ## Call site table length
Lset61 = Ltmp192-Lfunc_begin4           ## >> Call Site 1 <<
	.long	Lset61
Lset62 = Ltmp195-Ltmp192                ##   Call between Ltmp192 and Ltmp195
	.long	Lset62
Lset63 = Ltmp196-Lfunc_begin4           ##     jumps to Ltmp196
	.long	Lset63
	.byte	1                       ##   On action: 1
Lset64 = Ltmp195-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset64
Lset65 = Lfunc_end4-Ltmp195             ##   Call between Ltmp195 and Lfunc_end4
	.long	Lset65
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp200:
	.cfi_def_cfa_offset 16
Ltmp201:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp202:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$64, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11to_int_typeEc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11to_int_typeEc: ## @_ZNSt3__111char_traitsIcE11to_int_typeEc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp203:
	.cfi_def_cfa_offset 16
Ltmp204:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp205:
	.cfi_def_cfa_register %rbp
	movb	%dil, %al
	movb	%al, -1(%rbp)
	movzbl	-1(%rbp), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp206:
	.cfi_def_cfa_offset 16
Ltmp207:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp208:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE7not_eofEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE7not_eofEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE7not_eofEi:   ## @_ZNSt3__111char_traitsIcE7not_eofEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp209:
	.cfi_def_cfa_offset 16
Ltmp210:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp211:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movl	%edi, -8(%rbp)          ## 4-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movl	-8(%rbp), %edi          ## 4-byte Reload
	movl	%eax, %esi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB36_1
	jmp	LBB36_2
LBB36_1:
	callq	__ZNSt3__111char_traitsIcE3eofEv
	xorl	$-1, %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
	jmp	LBB36_3
LBB36_2:
	movl	-4(%rbp), %eax
	movl	%eax, -12(%rbp)         ## 4-byte Spill
LBB36_3:
	movl	-12(%rbp), %eax         ## 4-byte Reload
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE2eqEcc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE2eqEcc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE2eqEcc:       ## @_ZNSt3__111char_traitsIcE2eqEcc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp212:
	.cfi_def_cfa_offset 16
Ltmp213:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp214:
	.cfi_def_cfa_register %rbp
	movb	%sil, %al
	movb	%dil, %cl
	movb	%cl, -1(%rbp)
	movb	%al, -2(%rbp)
	movsbl	-1(%rbp), %esi
	movsbl	-2(%rbp), %edi
	cmpl	%edi, %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE12to_char_typeEi
	.align	4, 0x90
__ZNSt3__111char_traitsIcE12to_char_typeEi: ## @_ZNSt3__111char_traitsIcE12to_char_typeEi
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp215:
	.cfi_def_cfa_offset 16
Ltmp216:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp217:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %edi
	movb	%dil, %al
	movsbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp239:
	.cfi_def_cfa_offset 16
Ltmp240:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp241:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp218:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp219:
	jmp	LBB39_1
LBB39_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB39_3
	jmp	LBB39_26
LBB39_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB39_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB39_7
LBB39_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB39_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB39_8
	jmp	LBB39_13
LBB39_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp221:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp222:
	jmp	LBB39_9
LBB39_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp223:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp224:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB39_10
LBB39_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp225:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp226:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB39_12
LBB39_11:
Ltmp227:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB39_21
LBB39_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB39_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp228:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp229:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB39_15
LBB39_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB39_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp230:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp231:
	jmp	LBB39_17
LBB39_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB39_18
LBB39_18:
	jmp	LBB39_25
LBB39_19:
Ltmp220:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB39_22
LBB39_20:
Ltmp232:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB39_21
LBB39_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB39_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp233:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp234:
	jmp	LBB39_23
LBB39_23:
	callq	___cxa_end_catch
LBB39_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB39_25:
	jmp	LBB39_26
LBB39_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB39_24
LBB39_27:
Ltmp235:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp236:
	callq	___cxa_end_catch
Ltmp237:
	jmp	LBB39_28
LBB39_28:
	jmp	LBB39_29
LBB39_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB39_30:
Ltmp238:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table39:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset66 = Ltmp218-Lfunc_begin5           ## >> Call Site 1 <<
	.long	Lset66
Lset67 = Ltmp219-Ltmp218                ##   Call between Ltmp218 and Ltmp219
	.long	Lset67
Lset68 = Ltmp220-Lfunc_begin5           ##     jumps to Ltmp220
	.long	Lset68
	.byte	5                       ##   On action: 3
Lset69 = Ltmp221-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset69
Lset70 = Ltmp222-Ltmp221                ##   Call between Ltmp221 and Ltmp222
	.long	Lset70
Lset71 = Ltmp232-Lfunc_begin5           ##     jumps to Ltmp232
	.long	Lset71
	.byte	5                       ##   On action: 3
Lset72 = Ltmp223-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset72
Lset73 = Ltmp226-Ltmp223                ##   Call between Ltmp223 and Ltmp226
	.long	Lset73
Lset74 = Ltmp227-Lfunc_begin5           ##     jumps to Ltmp227
	.long	Lset74
	.byte	3                       ##   On action: 2
Lset75 = Ltmp228-Lfunc_begin5           ## >> Call Site 4 <<
	.long	Lset75
Lset76 = Ltmp231-Ltmp228                ##   Call between Ltmp228 and Ltmp231
	.long	Lset76
Lset77 = Ltmp232-Lfunc_begin5           ##     jumps to Ltmp232
	.long	Lset77
	.byte	5                       ##   On action: 3
Lset78 = Ltmp231-Lfunc_begin5           ## >> Call Site 5 <<
	.long	Lset78
Lset79 = Ltmp233-Ltmp231                ##   Call between Ltmp231 and Ltmp233
	.long	Lset79
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset80 = Ltmp233-Lfunc_begin5           ## >> Call Site 6 <<
	.long	Lset80
Lset81 = Ltmp234-Ltmp233                ##   Call between Ltmp233 and Ltmp234
	.long	Lset81
Lset82 = Ltmp235-Lfunc_begin5           ##     jumps to Ltmp235
	.long	Lset82
	.byte	0                       ##   On action: cleanup
Lset83 = Ltmp234-Lfunc_begin5           ## >> Call Site 7 <<
	.long	Lset83
Lset84 = Ltmp236-Ltmp234                ##   Call between Ltmp234 and Ltmp236
	.long	Lset84
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset85 = Ltmp236-Lfunc_begin5           ## >> Call Site 8 <<
	.long	Lset85
Lset86 = Ltmp237-Ltmp236                ##   Call between Ltmp236 and Ltmp237
	.long	Lset86
Lset87 = Ltmp238-Lfunc_begin5           ##     jumps to Ltmp238
	.long	Lset87
	.byte	5                       ##   On action: 3
Lset88 = Ltmp237-Lfunc_begin5           ## >> Call Site 9 <<
	.long	Lset88
Lset89 = Lfunc_end5-Ltmp237             ##   Call between Ltmp237 and Lfunc_end5
	.long	Lset89
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp245:
	.cfi_def_cfa_offset 16
Ltmp246:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp247:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB40_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB40_26
LBB40_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB40_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB40_5
LBB40_4:
	movq	$0, -368(%rbp)
LBB40_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB40_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB40_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB40_26
LBB40_8:
	jmp	LBB40_9
LBB40_9:
	cmpq	$0, -368(%rbp)
	jle	LBB40_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB40_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB40_13
LBB40_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB40_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp242:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp243:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB40_14
LBB40_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB40_15
LBB40_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB40_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB40_19
LBB40_17:
Ltmp244:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB40_27
LBB40_18:
	movl	$0, -416(%rbp)
LBB40_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB40_20
	jmp	LBB40_29
LBB40_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB40_26
	jmp	LBB40_28
LBB40_20:
	jmp	LBB40_21
LBB40_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB40_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB40_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB40_26
LBB40_24:
	jmp	LBB40_25
LBB40_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB40_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB40_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB40_28:
Lfunc_end6:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table40:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset90 = Lfunc_begin6-Lfunc_begin6      ## >> Call Site 1 <<
	.long	Lset90
Lset91 = Ltmp242-Lfunc_begin6           ##   Call between Lfunc_begin6 and Ltmp242
	.long	Lset91
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset92 = Ltmp242-Lfunc_begin6           ## >> Call Site 2 <<
	.long	Lset92
Lset93 = Ltmp243-Ltmp242                ##   Call between Ltmp242 and Ltmp243
	.long	Lset93
Lset94 = Ltmp244-Lfunc_begin6           ##     jumps to Ltmp244
	.long	Lset94
	.byte	0                       ##   On action: cleanup
Lset95 = Ltmp243-Lfunc_begin6           ## >> Call Site 3 <<
	.long	Lset95
Lset96 = Lfunc_end6-Ltmp243             ##   Call between Ltmp243 and Lfunc_end6
	.long	Lset96
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__13hexERNS_8ios_baseE
	.globl	__ZNSt3__13hexERNS_8ios_baseE
	.weak_definition	__ZNSt3__13hexERNS_8ios_baseE
	.align	4, 0x90
__ZNSt3__13hexERNS_8ios_baseE:          ## @_ZNSt3__13hexERNS_8ios_baseE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp248:
	.cfi_def_cfa_offset 16
Ltmp249:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp250:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -48(%rbp)
	movq	-48(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movl	$8, -28(%rbp)
	movl	$74, -32(%rbp)
	movq	-24(%rbp), %rdi
	movl	8(%rdi), %eax
	movl	%eax, -36(%rbp)
	movl	-32(%rbp), %eax
	movq	%rdi, -8(%rbp)
	movl	%eax, -12(%rbp)
	movq	-8(%rbp), %rcx
	movl	-12(%rbp), %eax
	xorl	$-1, %eax
	andl	8(%rcx), %eax
	movl	%eax, 8(%rcx)
	movl	-28(%rbp), %eax
	andl	-32(%rbp), %eax
	orl	8(%rdi), %eax
	movl	%eax, 8(%rdi)
	movq	-48(%rbp), %rax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.weak_def_can_be_hidden	__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.align	4, 0x90
__ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv: ## @_ZNKSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE3strEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp251:
	.cfi_def_cfa_offset 16
Ltmp252:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp253:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$712, %rsp              ## imm = 0x2C8
Ltmp254:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	%rsi, -616(%rbp)
	movq	-616(%rbp), %rsi
	movl	96(%rsi), %ecx
	andl	$16, %ecx
	cmpl	$0, %ecx
	movq	%rax, -672(%rbp)        ## 8-byte Spill
	movq	%rdi, -680(%rbp)        ## 8-byte Spill
	movq	%rsi, -688(%rbp)        ## 8-byte Spill
	je	LBB42_4
## BB#1:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	88(%rax), %rcx
	movq	%rax, -608(%rbp)
	movq	-608(%rbp), %rax
	cmpq	48(%rax), %rcx
	jae	LBB42_3
## BB#2:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -264(%rbp)
	movq	-264(%rbp), %rax
	movq	48(%rax), %rax
	movq	-688(%rbp), %rcx        ## 8-byte Reload
	movq	%rax, 88(%rcx)
LBB42_3:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-72(%rbp), %rcx
	leaq	-96(%rbp), %rdi
	leaq	-624(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -56(%rbp)
	movq	-56(%rbp), %r9
	movq	40(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	88(%r10), %r11
	addq	$64, %r10
	movq	%r10, -48(%rbp)
	movq	-48(%rbp), %r10
	movq	%r10, -32(%rbp)
	movq	-32(%rbp), %r10
	movq	%r10, -24(%rbp)
	movq	-24(%rbp), %r10
	movq	%r10, -16(%rbp)
	movq	-680(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -176(%rbp)
	movq	%r9, -184(%rbp)
	movq	%r11, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	-176(%rbp), %r8
	movq	-184(%rbp), %r9
	movq	-192(%rbp), %r11
	movq	-200(%rbp), %rbx
	movq	%r8, -136(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r11, -152(%rbp)
	movq	%rbx, -160(%rbp)
	movq	-136(%rbp), %r8
	movq	%r8, -128(%rbp)
	movq	-128(%rbp), %r9
	movq	%r9, -104(%rbp)
	movq	-104(%rbp), %r9
	movq	%rdi, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	-80(%rbp), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -696(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-144(%rbp), %rsi
	movq	-152(%rbp), %rdx
	movq	-696(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB42_11
LBB42_4:
	movq	-688(%rbp), %rax        ## 8-byte Reload
	movl	96(%rax), %ecx
	andl	$8, %ecx
	cmpl	$0, %ecx
	je	LBB42_6
## BB#5:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-280(%rbp), %rcx
	leaq	-304(%rbp), %rdi
	leaq	-640(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -208(%rbp)
	movq	-208(%rbp), %r9
	movq	16(%r9), %r9
	movq	-688(%rbp), %r10        ## 8-byte Reload
	movq	%r10, -216(%rbp)
	movq	-216(%rbp), %r10
	movq	32(%r10), %r10
	movq	-688(%rbp), %r11        ## 8-byte Reload
	addq	$64, %r11
	movq	%r11, -256(%rbp)
	movq	-256(%rbp), %r11
	movq	%r11, -240(%rbp)
	movq	-240(%rbp), %r11
	movq	%r11, -232(%rbp)
	movq	-232(%rbp), %r11
	movq	%r11, -224(%rbp)
	movq	-680(%rbp), %r11        ## 8-byte Reload
	movq	%r11, -384(%rbp)
	movq	%r9, -392(%rbp)
	movq	%r10, -400(%rbp)
	movq	%r8, -408(%rbp)
	movq	-384(%rbp), %r8
	movq	-392(%rbp), %r9
	movq	-400(%rbp), %r10
	movq	-408(%rbp), %rbx
	movq	%r8, -344(%rbp)
	movq	%r9, -352(%rbp)
	movq	%r10, -360(%rbp)
	movq	%rbx, -368(%rbp)
	movq	-344(%rbp), %r8
	movq	%r8, -336(%rbp)
	movq	-336(%rbp), %r9
	movq	%r9, -312(%rbp)
	movq	-312(%rbp), %r9
	movq	%rdi, -296(%rbp)
	movq	%r9, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rcx, -272(%rbp)
	movq	%r8, -704(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-352(%rbp), %rsi
	movq	-360(%rbp), %rdx
	movq	-704(%rbp), %rdi        ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	jmp	LBB42_11
LBB42_6:
	jmp	LBB42_7
LBB42_7:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-504(%rbp), %rcx
	leaq	-528(%rbp), %rdi
	leaq	-656(%rbp), %r8
	movq	-688(%rbp), %r9         ## 8-byte Reload
	addq	$64, %r9
	movq	%r9, -448(%rbp)
	movq	-448(%rbp), %r9
	movq	%r9, -432(%rbp)
	movq	-432(%rbp), %r9
	movq	%r9, -424(%rbp)
	movq	-424(%rbp), %r9
	movq	%r9, -416(%rbp)
	movq	-680(%rbp), %r9         ## 8-byte Reload
	movq	%r9, -592(%rbp)
	movq	%r8, -600(%rbp)
	movq	-592(%rbp), %r8
	movq	-600(%rbp), %r10
	movq	%r8, -568(%rbp)
	movq	%r10, -576(%rbp)
	movq	-568(%rbp), %r8
	movq	%r8, -560(%rbp)
	movq	-560(%rbp), %r10
	movq	%r10, -536(%rbp)
	movq	-536(%rbp), %r10
	movq	%rdi, -520(%rbp)
	movq	%r10, -512(%rbp)
	movq	-512(%rbp), %rdi
	movq	%rcx, -496(%rbp)
	movq	%r8, -712(%rbp)         ## 8-byte Spill
	callq	_memset
	movq	-712(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rdx
	movq	%rdx, -464(%rbp)
	movq	-464(%rbp), %rdx
	movq	%rdx, -456(%rbp)
	movq	-456(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movl	$0, -484(%rbp)
LBB42_8:                                ## =>This Inner Loop Header: Depth=1
	cmpl	$3, -484(%rbp)
	jae	LBB42_10
## BB#9:                                ##   in Loop: Header=BB42_8 Depth=1
	movl	-484(%rbp), %eax
	movl	%eax, %ecx
	movq	-480(%rbp), %rdx
	movq	$0, (%rdx,%rcx,8)
	movl	-484(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -484(%rbp)
	jmp	LBB42_8
LBB42_10:                               ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS4_.exit
	jmp	LBB42_11
LBB42_11:
	movq	-672(%rbp), %rax        ## 8-byte Reload
	addq	$712, %rsp              ## imm = 0x2C8
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.weak_def_can_be_hidden	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.align	4, 0x90
__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_: ## @_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initIPcEENS_9enable_ifIXsr21__is_forward_iteratorIT_EE5valueEvE4typeES9_S9_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp255:
	.cfi_def_cfa_offset 16
Ltmp256:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp257:
	.cfi_def_cfa_register %rbp
	subq	$464, %rsp              ## imm = 0x1D0
	movq	%rdi, -392(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	-392(%rbp), %rdx
	movq	-400(%rbp), %rsi
	movq	-408(%rbp), %rdi
	movq	%rsi, -368(%rbp)
	movq	%rdi, -376(%rbp)
	movq	-368(%rbp), %rsi
	movq	-376(%rbp), %rdi
	movq	%rsi, -352(%rbp)
	movq	%rdi, -360(%rbp)
	movq	-360(%rbp), %rsi
	movq	-352(%rbp), %rdi
	subq	%rdi, %rsi
	movq	%rsi, -416(%rbp)
	movq	-416(%rbp), %rsi
	movq	%rdx, -336(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdi, -328(%rbp)
	movq	-328(%rbp), %rdi
	movq	%rdi, -320(%rbp)
	movq	-320(%rbp), %rdi
	movq	%rdi, -312(%rbp)
	movq	-312(%rbp), %rdi
	movq	%rdi, -288(%rbp)
	movq	-288(%rbp), %rdi
	movq	%rdi, -280(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdi, -264(%rbp)
	movq	$-1, -344(%rbp)
	movq	-344(%rbp), %rdi
	subq	$16, %rdi
	cmpq	%rdi, %rsi
	movq	%rdx, -448(%rbp)        ## 8-byte Spill
	jbe	LBB43_2
## BB#1:
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNKSt3__121__basic_string_commonILb1EE20__throw_length_errorEv
LBB43_2:
	cmpq	$23, -416(%rbp)
	jae	LBB43_4
## BB#3:
	movq	-416(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -256(%rbp)
	movq	-248(%rbp), %rax
	movq	-256(%rbp), %rdx
	shlq	$1, %rdx
	movb	%dl, %sil
	movq	%rax, -240(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rax
	movb	%sil, (%rax)
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, -424(%rbp)
	jmp	LBB43_8
LBB43_4:
	movq	-416(%rbp), %rax
	movq	%rax, -16(%rbp)
	cmpq	$23, -16(%rbp)
	jae	LBB43_6
## BB#5:
	movl	$23, %eax
	movl	%eax, %ecx
	movq	%rcx, -456(%rbp)        ## 8-byte Spill
	jmp	LBB43_7
LBB43_6:
	movq	-16(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$15, %rax
	andq	$-16, %rax
	movq	%rax, -456(%rbp)        ## 8-byte Spill
LBB43_7:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE11__recommendEm.exit
	movq	-456(%rbp), %rax        ## 8-byte Reload
	subq	$1, %rax
	movq	%rax, -432(%rbp)
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	-432(%rbp), %rdx
	addq	$1, %rdx
	movq	%rcx, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	$0, -112(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rdi
	callq	__Znwm
	movq	%rax, -424(%rbp)
	movq	-424(%rbp), %rax
	movq	-448(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -152(%rbp)
	movq	%rax, -160(%rbp)
	movq	-152(%rbp), %rax
	movq	-160(%rbp), %rdx
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-136(%rbp), %rax
	movq	%rdx, 16(%rax)
	movq	-432(%rbp), %rax
	addq	$1, %rax
	movq	%rcx, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rdx
	orq	$1, %rdx
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rdx, (%rax)
	movq	-416(%rbp), %rax
	movq	%rcx, -216(%rbp)
	movq	%rax, -224(%rbp)
	movq	-216(%rbp), %rax
	movq	-224(%rbp), %rdx
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rdx, 8(%rax)
LBB43_8:
	jmp	LBB43_9
LBB43_9:                                ## =>This Inner Loop Header: Depth=1
	movq	-400(%rbp), %rax
	cmpq	-408(%rbp), %rax
	je	LBB43_12
## BB#10:                               ##   in Loop: Header=BB43_9 Depth=1
	movq	-424(%rbp), %rdi
	movq	-400(%rbp), %rsi
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
## BB#11:                               ##   in Loop: Header=BB43_9 Depth=1
	movq	-400(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -400(%rbp)
	movq	-424(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -424(%rbp)
	jmp	LBB43_9
LBB43_12:
	leaq	-433(%rbp), %rsi
	movq	-424(%rbp), %rdi
	movb	$0, -433(%rbp)
	callq	__ZNSt3__111char_traitsIcE6assignERcRKc
	addq	$464, %rsp              ## imm = 0x1D0
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6assignERcRKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6assignERcRKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6assignERcRKc: ## @_ZNSt3__111char_traitsIcE6assignERcRKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp258:
	.cfi_def_cfa_offset 16
Ltmp259:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp260:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movb	(%rsi), %al
	movq	-8(%rbp), %rsi
	movb	%al, (%rsi)
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.align	4, 0x90
__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev: ## @_ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp261:
	.cfi_def_cfa_offset 16
Ltmp262:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp263:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movq	24(%rdi), %rax
	movq	(%rsi), %rcx
	movq	-24(%rcx), %rcx
	movq	%rax, (%rsi,%rcx)
	movq	%rsi, %rax
	addq	$8, %rax
	movq	%rdi, -24(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	movq	%rsi, -32(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-32(%rbp), %rax         ## 8-byte Reload
	movq	-24(%rbp), %rcx         ## 8-byte Reload
	addq	$8, %rcx
	movq	%rax, %rdi
	movq	%rcx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED2Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__const
l_.ref.tmp:                             ## @.ref.tmp
	.ascii	"\252\273\314\335\356\377"

l_.ref.tmp.1:                           ## @.ref.tmp.1
	.ascii	"\252\273\314\335\356\377"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev

	.globl	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTTNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+24
	.quad	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE+64
	.quad	__ZTVNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+64

	.globl	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE ## @_ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.weak_def_can_be_hidden	__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE
	.align	4
__ZTCNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_ostreamIcS2_EE:
	.quad	112
	.quad	0
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.quad	-112
	.quad	-112
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.quad	__ZTv0_n24_NSt3__113basic_ostreamIcNS_11char_traitsIcEEED0Ev

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__119basic_ostringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__113basic_ostreamIcNS_11char_traitsIcEEEE

	.globl	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_def_can_be_hidden	__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	3
__ZTVNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	0
	.quad	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6setbufEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffExNS_8ios_base7seekdirEj
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposI11__mbstate_tEEj
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.quad	__ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKcl
	.quad	__ZNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.asciz	"NSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE ## @_ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak_definition	__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4
__ZTINSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__115basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.quad	__ZTINSt3__115basic_streambufIcNS_11char_traitsIcEEEE


.subsections_via_symbols
