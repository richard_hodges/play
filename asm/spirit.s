	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.align	4, 0x90
__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @_Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp25:
	.cfi_def_cfa_offset 16
Ltmp26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp27:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$104, %rsp
Ltmp28:
	.cfi_offset %rbx, -40
Ltmp29:
	.cfi_offset %r14, -32
Ltmp30:
	.cfi_offset %r15, -24
	movq	%rdi, %r15
	movzbl	(%rsi), %ebx
	leaq	1(%rsi), %r14
	movb	%bl, %al
	andb	$1, %al
	cmovneq	16(%rsi), %r14
	shrq	%rbx
	testb	%al, %al
	movq	%r14, -96(%rbp)
	cmovneq	8(%rsi), %rbx
	movb	__ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %al
	testb	%al, %al
	jne	LBB0_6
## BB#1:
	leaq	__ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rdi
	callq	___cxa_guard_acquire
	testl	%eax, %eax
	je	LBB0_6
## BB#2:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -128(%rbp)
	movq	$0, -112(%rbp)
Ltmp0:
	leaq	L_.str(%rip), %rsi
	leaq	-128(%rbp), %rdi
	movl	$12, %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp1:
## BB#3:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKc.exit
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rax
	movq	%rax, __ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip)
Ltmp3:
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+8(%rip), %rdi
	leaq	-128(%rbp), %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
Ltmp4:
## BB#4:                                ## %.noexc5
	movq	$0, __ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+32(%rip)
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+32(%rip), %rdi
Ltmp6:
	leaq	-32(%rbp), %rsi
	callq	__ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEEaSINS9_2qi6detail13parser_binderINSS_11alternativeINSC_INSS_15any_real_parserIdNSS_20strict_real_policiesIdEEEENSC_INSS_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEENS_11enable_if_cIXntsr11is_integralIT_EE5valueERSQ_E4typeES1A_
Ltmp7:
## BB#5:
	leaq	-128(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	__ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED1Ev@GOTPCREL(%rip), %rdi
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rsi
	movq	___dso_handle@GOTPCREL(%rip), %rdx
	callq	___cxa_atexit
	leaq	__ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rdi
	callq	___cxa_guard_release
LBB0_6:
	addq	%rbx, %r14
	movl	$0, 8(%r15)
	movl	$0, (%r15)
	movq	%r14, -80(%rbp)
	movq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rdi
	movq	32(%rdi), %rax
	testq	%rax, %rax
	movq	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5dummy7nonnullEv@GOTPCREL(%rip), %rcx
	cmoveq	%rax, %rcx
	testq	%rcx, %rcx
	je	LBB0_20
## BB#7:
	movq	%r15, -72(%rbp)
	testq	%rax, %rax
	je	LBB0_8
## BB#18:                               ## %_ZNK5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEclES6_S8_SL_SO_.exit.i.i.i
	andq	$-2, %rax
	movq	8(%rax), %rax
	addq	$40, %rdi
Ltmp12:
	leaq	__ZN5boost6spiritL6unusedE(%rip), %r8
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	leaq	-72(%rbp), %rcx
	callq	*%rax
Ltmp13:
## BB#19:
	testb	%al, %al
	je	LBB0_20
## BB#31:
	movq	%r15, %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_20:
Ltmp19:
	leaq	L___func__._Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.64(%rip), %rcx
	movl	$39, %edx
	callq	___assert_rtn
Ltmp20:
## BB#21:
LBB0_8:
Ltmp14:
	leaq	L_.str.101(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNSt13runtime_errorC2EPKc
Ltmp15:
## BB#9:                                ## %.noexc
	movq	__ZTVN5boost17bad_function_callE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, -48(%rbp)
Ltmp16:
	leaq	-48(%rbp), %rdi
	callq	__ZN5boost15throw_exceptionINS_17bad_function_callEEEvRKT_
Ltmp17:
## BB#16:
LBB0_26:
Ltmp21:
	movq	%rax, %r14
LBB0_27:                                ## %.body
	movl	(%r15), %edi
	addq	$8, %r15
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp22:
	leaq	-88(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp23:
## BB#28:
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB0_30:
Ltmp24:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB0_17:
Ltmp18:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt13runtime_errorD2Ev
	jmp	LBB0_27
LBB0_22:
Ltmp2:
	movq	%rax, %r14
	jmp	LBB0_25
LBB0_23:
Ltmp5:
	movq	%rax, %r14
	jmp	LBB0_24
LBB0_10:
Ltmp8:
	movq	%rax, %r14
	movq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+32(%rip), %rax
	testq	%rax, %rax
	je	LBB0_15
## BB#11:
	testb	$1, %al
	jne	LBB0_14
## BB#12:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB0_14
## BB#13:
Ltmp9:
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+40(%rip), %rdi
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp10:
LBB0_14:                                ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i.i.i.i
	movq	$0, __ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+32(%rip)
LBB0_15:                                ## %_ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEED1Ev.exit.i.i
	leaq	__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p+8(%rip), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_24:                                ## %.body6
	leaq	-128(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB0_25:
	leaq	__ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p(%rip), %rdi
	callq	___cxa_guard_abort
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB0_29:
Ltmp11:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset0 = Ltmp0-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset1
Lset2 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset2
	.byte	0                       ##   On action: cleanup
Lset3 = Ltmp3-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset4
Lset5 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset5
	.byte	0                       ##   On action: cleanup
Lset6 = Ltmp6-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp7-Ltmp6                     ##   Call between Ltmp6 and Ltmp7
	.long	Lset7
Lset8 = Ltmp8-Lfunc_begin0              ##     jumps to Ltmp8
	.long	Lset8
	.byte	0                       ##   On action: cleanup
Lset9 = Ltmp12-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset9
Lset10 = Ltmp15-Ltmp12                  ##   Call between Ltmp12 and Ltmp15
	.long	Lset10
Lset11 = Ltmp21-Lfunc_begin0            ##     jumps to Ltmp21
	.long	Lset11
	.byte	0                       ##   On action: cleanup
Lset12 = Ltmp16-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset12
Lset13 = Ltmp17-Ltmp16                  ##   Call between Ltmp16 and Ltmp17
	.long	Lset13
Lset14 = Ltmp18-Lfunc_begin0            ##     jumps to Ltmp18
	.long	Lset14
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp22-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset15
Lset16 = Ltmp23-Ltmp22                  ##   Call between Ltmp22 and Ltmp23
	.long	Lset16
Lset17 = Ltmp24-Lfunc_begin0            ##     jumps to Ltmp24
	.long	Lset17
	.byte	1                       ##   On action: 1
Lset18 = Ltmp23-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset18
Lset19 = Ltmp9-Ltmp23                   ##   Call between Ltmp23 and Ltmp9
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset20 = Ltmp9-Lfunc_begin0             ## >> Call Site 8 <<
	.long	Lset20
Lset21 = Ltmp10-Ltmp9                   ##   Call between Ltmp9 and Ltmp10
	.long	Lset21
Lset22 = Ltmp11-Lfunc_begin0            ##     jumps to Ltmp11
	.long	Lset22
	.byte	1                       ##   On action: 1
Lset23 = Ltmp10-Lfunc_begin0            ## >> Call Site 9 <<
	.long	Lset23
Lset24 = Lfunc_end0-Ltmp10              ##   Call between Ltmp10 and Lfunc_end0
	.long	Lset24
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED1Ev
	.weak_def_can_be_hidden	__ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED1Ev
	.align	4, 0x90
__ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED1Ev: ## @_ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED1Ev
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp34:
	.cfi_def_cfa_offset 16
Ltmp35:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp36:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp37:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	LBB1_5
## BB#1:
	testb	$1, %al
	jne	LBB1_4
## BB#2:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB1_4
## BB#3:
	leaq	40(%rbx), %rdi
Ltmp31:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp32:
LBB1_4:                                 ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i.i.i
	movq	$0, 32(%rbx)
LBB1_5:                                 ## %_ZN5boost6spirit2qi4ruleINSt3__111__wrap_iterIPKcEEFNS_7variantIiJdEEEvENS0_11unused_typeESB_SB_ED2Ev.exit
	addq	$8, %rbx
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev ## TAILCALL
LBB1_6:
Ltmp33:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table1:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset25 = Ltmp31-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset25
Lset26 = Ltmp32-Ltmp31                  ##   Call between Ltmp31 and Ltmp32
	.long	Lset26
Lset27 = Ltmp33-Lfunc_begin1            ##     jumps to Ltmp33
	.long	Lset27
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp194:
	.cfi_def_cfa_offset 16
Ltmp195:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp196:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$640, %rsp              ## imm = 0x280
Ltmp197:
	.cfi_offset %rbx, -32
Ltmp198:
	.cfi_offset %r14, -24
	movq	___stack_chk_guard@GOTPCREL(%rip), %r14
	movq	(%r14), %r14
	movq	%r14, -24(%rbp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -272(%rbp)
	movq	$0, -256(%rbp)
	leaq	L_.str.65(%rip), %rsi
	leaq	-272(%rbp), %rbx
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp38:
	leaq	-40(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp39:
## BB#1:
	movl	-40(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	cmpl	%edi, %eax
	jne	LBB2_2
## BB#7:
	leaq	-32(%rbp), %rcx
Ltmp47:
	leaq	-240(%rbp), %rdx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp48:
## BB#8:                                ## %_ZN5boost7variantIiJdEED1Ev.exit20
	leaq	-272(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -304(%rbp)
	movq	$0, -288(%rbp)
	leaq	L_.str.67(%rip), %rsi
	leaq	-304(%rbp), %rbx
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp50:
	leaq	-56(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp51:
## BB#9:
	movl	-56(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	cmpl	%edi, %eax
	jne	LBB2_10
## BB#17:
	leaq	-48(%rbp), %rcx
Ltmp59:
	leaq	-240(%rbp), %rdx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp60:
## BB#18:                               ## %_ZN5boost7variantIiJdEED1Ev.exit30
	leaq	-304(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -336(%rbp)
	movq	$0, -320(%rbp)
	leaq	L_.str.69(%rip), %rsi
	leaq	-336(%rbp), %rbx
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp62:
	leaq	-72(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp63:
## BB#19:
	movl	-72(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	cmpl	%edi, %eax
	jne	LBB2_20
## BB#27:
	leaq	-64(%rbp), %rcx
Ltmp71:
	leaq	-240(%rbp), %rdx
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp72:
## BB#28:                               ## %_ZN5boost7variantIiJdEED1Ev.exit40
	leaq	-336(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -368(%rbp)
	movq	$0, -352(%rbp)
	leaq	L_.str.71(%rip), %rsi
	leaq	-368(%rbp), %rbx
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp74:
	leaq	-88(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp75:
## BB#29:
	movl	-88(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_30
## BB#37:
	leaq	-80(%rbp), %rcx
Ltmp83:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp84:
## BB#38:                               ## %_ZN5boost7variantIiJdEED1Ev.exit50
	leaq	-368(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -400(%rbp)
	movq	$0, -384(%rbp)
	leaq	L_.str.73(%rip), %rsi
	leaq	-400(%rbp), %rbx
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp86:
	leaq	-104(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp87:
## BB#39:
	movl	-104(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_40
## BB#47:
	leaq	-96(%rbp), %rcx
Ltmp95:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp96:
## BB#48:                               ## %_ZN5boost7variantIiJdEED1Ev.exit60
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -432(%rbp)
	movq	$0, -416(%rbp)
	leaq	L_.str.75(%rip), %rsi
	leaq	-432(%rbp), %rbx
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp98:
	leaq	-120(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp99:
## BB#49:
	movl	-120(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_50
## BB#57:
	leaq	-112(%rbp), %rcx
Ltmp107:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp108:
## BB#58:                               ## %_ZN5boost7variantIiJdEED1Ev.exit70
	leaq	-432(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -464(%rbp)
	movq	$0, -448(%rbp)
	leaq	L_.str.77(%rip), %rsi
	leaq	-464(%rbp), %rbx
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp110:
	leaq	-136(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp111:
## BB#59:
	movl	-136(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_60
## BB#67:
	leaq	-128(%rbp), %rcx
Ltmp119:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp120:
## BB#68:                               ## %_ZN5boost7variantIiJdEED1Ev.exit80
	leaq	-464(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -496(%rbp)
	movq	$0, -480(%rbp)
	leaq	L_.str.79(%rip), %rsi
	leaq	-496(%rbp), %rbx
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp122:
	leaq	-152(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp123:
## BB#69:
	movl	-152(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_70
## BB#77:
	leaq	-144(%rbp), %rcx
Ltmp131:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp132:
## BB#78:                               ## %_ZN5boost7variantIiJdEED1Ev.exit90
	leaq	-496(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -528(%rbp)
	movq	$0, -512(%rbp)
	leaq	L_.str.81(%rip), %rsi
	leaq	-528(%rbp), %rbx
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp134:
	leaq	-168(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp135:
## BB#79:
	movl	-168(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_80
## BB#87:
	leaq	-160(%rbp), %rcx
Ltmp143:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp144:
## BB#88:                               ## %_ZN5boost7variantIiJdEED1Ev.exit100
	leaq	-528(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -560(%rbp)
	movq	$0, -544(%rbp)
	leaq	L_.str.83(%rip), %rsi
	leaq	-560(%rbp), %rbx
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp146:
	leaq	-184(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp147:
## BB#89:
	movl	-184(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_90
## BB#97:
	leaq	-176(%rbp), %rcx
Ltmp155:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp156:
## BB#98:                               ## %_ZN5boost7variantIiJdEED1Ev.exit110
	leaq	-560(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -592(%rbp)
	movq	$0, -576(%rbp)
	leaq	L_.str.85(%rip), %rsi
	leaq	-592(%rbp), %rbx
	movl	$4, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp158:
	leaq	-200(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp159:
## BB#99:
	movl	-200(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_100
## BB#107:
	leaq	-192(%rbp), %rcx
Ltmp167:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp168:
## BB#108:                              ## %_ZN5boost7variantIiJdEED1Ev.exit120
	leaq	-592(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -624(%rbp)
	movq	$0, -608(%rbp)
	leaq	L_.str.87(%rip), %rsi
	leaq	-624(%rbp), %rbx
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp170:
	leaq	-216(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp171:
## BB#109:
	movl	-216(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_110
## BB#117:
	leaq	-208(%rbp), %rcx
Ltmp179:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp180:
## BB#118:                              ## %_ZN5boost7variantIiJdEED1Ev.exit130
	leaq	-624(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -656(%rbp)
	movq	$0, -640(%rbp)
	leaq	L_.str.89(%rip), %rsi
	leaq	-656(%rbp), %rbx
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp182:
	leaq	-232(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp183:
## BB#119:
	movl	-232(%rbp), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	xorl	%edi, %eax
	cmpl	$1, %eax
	jne	LBB2_120
## BB#127:
	leaq	-224(%rbp), %rcx
Ltmp191:
	leaq	-240(%rbp), %rdx
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp192:
## BB#128:                              ## %_ZN5boost7variantIiJdEED1Ev.exit140
	leaq	-656(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	cmpq	-24(%rbp), %r14
	jne	LBB2_133
## BB#129:                              ## %_ZN5boost7variantIiJdEED1Ev.exit140
	xorl	%eax, %eax
	addq	$640, %rsp              ## imm = 0x280
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB2_12:
Ltmp40:
	movq	%rax, %rbx
	jmp	LBB2_13
LBB2_2:
Ltmp41:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.66(%rip), %rcx
	movl	$46, %edx
	callq	___assert_rtn
Ltmp42:
## BB#3:
LBB2_6:
Ltmp49:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_22:
Ltmp52:
	movq	%rax, %rbx
	jmp	LBB2_23
LBB2_10:
Ltmp53:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.68(%rip), %rcx
	movl	$47, %edx
	callq	___assert_rtn
Ltmp54:
## BB#11:
LBB2_16:
Ltmp61:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_32:
Ltmp64:
	movq	%rax, %rbx
	jmp	LBB2_33
LBB2_20:
Ltmp65:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.70(%rip), %rcx
	movl	$48, %edx
	callq	___assert_rtn
Ltmp66:
## BB#21:
LBB2_26:
Ltmp73:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_42:
Ltmp76:
	movq	%rax, %rbx
	jmp	LBB2_43
LBB2_30:
Ltmp77:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.72(%rip), %rcx
	movl	$50, %edx
	callq	___assert_rtn
Ltmp78:
## BB#31:
LBB2_36:
Ltmp85:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_52:
Ltmp88:
	movq	%rax, %rbx
	jmp	LBB2_53
LBB2_40:
Ltmp89:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.74(%rip), %rcx
	movl	$51, %edx
	callq	___assert_rtn
Ltmp90:
## BB#41:
LBB2_46:
Ltmp97:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_62:
Ltmp100:
	movq	%rax, %rbx
	jmp	LBB2_63
LBB2_50:
Ltmp101:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.76(%rip), %rcx
	movl	$52, %edx
	callq	___assert_rtn
Ltmp102:
## BB#51:
LBB2_56:
Ltmp109:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_72:
Ltmp112:
	movq	%rax, %rbx
	jmp	LBB2_73
LBB2_60:
Ltmp113:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.78(%rip), %rcx
	movl	$53, %edx
	callq	___assert_rtn
Ltmp114:
## BB#61:
LBB2_66:
Ltmp121:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_82:
Ltmp124:
	movq	%rax, %rbx
	jmp	LBB2_83
LBB2_70:
Ltmp125:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.80(%rip), %rcx
	movl	$54, %edx
	callq	___assert_rtn
Ltmp126:
## BB#71:
LBB2_76:
Ltmp133:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_92:
Ltmp136:
	movq	%rax, %rbx
	jmp	LBB2_93
LBB2_80:
Ltmp137:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.82(%rip), %rcx
	movl	$55, %edx
	callq	___assert_rtn
Ltmp138:
## BB#81:
LBB2_86:
Ltmp145:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_102:
Ltmp148:
	movq	%rax, %rbx
	jmp	LBB2_103
LBB2_90:
Ltmp149:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.84(%rip), %rcx
	movl	$56, %edx
	callq	___assert_rtn
Ltmp150:
## BB#91:
LBB2_96:
Ltmp157:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_112:
Ltmp160:
	movq	%rax, %rbx
	jmp	LBB2_113
LBB2_100:
Ltmp161:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.86(%rip), %rcx
	movl	$57, %edx
	callq	___assert_rtn
Ltmp162:
## BB#101:
LBB2_106:
Ltmp169:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_122:
Ltmp172:
	movq	%rax, %rbx
	jmp	LBB2_123
LBB2_110:
Ltmp173:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.88(%rip), %rcx
	movl	$58, %edx
	callq	___assert_rtn
Ltmp174:
## BB#111:
LBB2_116:
Ltmp181:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_130:
Ltmp184:
	movq	%rax, %rbx
	jmp	LBB2_131
LBB2_120:
Ltmp185:
	leaq	L___func__.main(%rip), %rdi
	leaq	L_.str.63(%rip), %rsi
	leaq	L_.str.90(%rip), %rcx
	movl	$59, %edx
	callq	___assert_rtn
Ltmp186:
## BB#121:
LBB2_126:
Ltmp193:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_133:                               ## %_ZN5boost7variantIiJdEED1Ev.exit140
	callq	___stack_chk_fail
LBB2_5:
Ltmp43:
	movq	%rax, %rbx
	movl	-40(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-32(%rbp), %rcx
Ltmp44:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp45:
LBB2_13:
	leaq	-272(%rbp), %rdi
	jmp	LBB2_132
LBB2_15:
Ltmp55:
	movq	%rax, %rbx
	movl	-56(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-48(%rbp), %rcx
Ltmp56:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp57:
LBB2_23:
	leaq	-304(%rbp), %rdi
	jmp	LBB2_132
LBB2_25:
Ltmp67:
	movq	%rax, %rbx
	movl	-72(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-64(%rbp), %rcx
Ltmp68:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp69:
LBB2_33:
	leaq	-336(%rbp), %rdi
	jmp	LBB2_132
LBB2_35:
Ltmp79:
	movq	%rax, %rbx
	movl	-88(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-80(%rbp), %rcx
Ltmp80:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp81:
LBB2_43:
	leaq	-368(%rbp), %rdi
	jmp	LBB2_132
LBB2_45:
Ltmp91:
	movq	%rax, %rbx
	movl	-104(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-96(%rbp), %rcx
Ltmp92:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp93:
LBB2_53:
	leaq	-400(%rbp), %rdi
	jmp	LBB2_132
LBB2_55:
Ltmp103:
	movq	%rax, %rbx
	movl	-120(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-112(%rbp), %rcx
Ltmp104:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp105:
LBB2_63:
	leaq	-432(%rbp), %rdi
	jmp	LBB2_132
LBB2_65:
Ltmp115:
	movq	%rax, %rbx
	movl	-136(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-128(%rbp), %rcx
Ltmp116:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp117:
LBB2_73:
	leaq	-464(%rbp), %rdi
	jmp	LBB2_132
LBB2_75:
Ltmp127:
	movq	%rax, %rbx
	movl	-152(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-144(%rbp), %rcx
Ltmp128:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp129:
LBB2_83:
	leaq	-496(%rbp), %rdi
	jmp	LBB2_132
LBB2_85:
Ltmp139:
	movq	%rax, %rbx
	movl	-168(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-160(%rbp), %rcx
Ltmp140:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp141:
LBB2_93:
	leaq	-528(%rbp), %rdi
	jmp	LBB2_132
LBB2_95:
Ltmp151:
	movq	%rax, %rbx
	movl	-184(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-176(%rbp), %rcx
Ltmp152:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp153:
LBB2_103:
	leaq	-560(%rbp), %rdi
	jmp	LBB2_132
LBB2_105:
Ltmp163:
	movq	%rax, %rbx
	movl	-200(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-192(%rbp), %rcx
Ltmp164:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp165:
LBB2_113:
	leaq	-592(%rbp), %rdi
	jmp	LBB2_132
LBB2_115:
Ltmp175:
	movq	%rax, %rbx
	movl	-216(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-208(%rbp), %rcx
Ltmp176:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp177:
LBB2_123:
	leaq	-624(%rbp), %rdi
	jmp	LBB2_132
LBB2_125:
Ltmp187:
	movq	%rax, %rbx
	movl	-232(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	leaq	-224(%rbp), %rcx
Ltmp188:
	leaq	-240(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp189:
LBB2_131:
	leaq	-656(%rbp), %rdi
LBB2_132:
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB2_4:
Ltmp46:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_14:
Ltmp58:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_24:
Ltmp70:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_34:
Ltmp82:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_44:
Ltmp94:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_54:
Ltmp106:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_64:
Ltmp118:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_74:
Ltmp130:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_84:
Ltmp142:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_94:
Ltmp154:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_104:
Ltmp166:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_114:
Ltmp178:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB2_124:
Ltmp190:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table2:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.ascii	"\360\006"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\347\006"              ## Call site table length
Lset28 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset28
Lset29 = Ltmp38-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp38
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp38-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset30
Lset31 = Ltmp39-Ltmp38                  ##   Call between Ltmp38 and Ltmp39
	.long	Lset31
Lset32 = Ltmp40-Lfunc_begin2            ##     jumps to Ltmp40
	.long	Lset32
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp47-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset33
Lset34 = Ltmp48-Ltmp47                  ##   Call between Ltmp47 and Ltmp48
	.long	Lset34
Lset35 = Ltmp49-Lfunc_begin2            ##     jumps to Ltmp49
	.long	Lset35
	.byte	1                       ##   On action: 1
Lset36 = Ltmp48-Lfunc_begin2            ## >> Call Site 4 <<
	.long	Lset36
Lset37 = Ltmp50-Ltmp48                  ##   Call between Ltmp48 and Ltmp50
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset38 = Ltmp50-Lfunc_begin2            ## >> Call Site 5 <<
	.long	Lset38
Lset39 = Ltmp51-Ltmp50                  ##   Call between Ltmp50 and Ltmp51
	.long	Lset39
Lset40 = Ltmp52-Lfunc_begin2            ##     jumps to Ltmp52
	.long	Lset40
	.byte	0                       ##   On action: cleanup
Lset41 = Ltmp59-Lfunc_begin2            ## >> Call Site 6 <<
	.long	Lset41
Lset42 = Ltmp60-Ltmp59                  ##   Call between Ltmp59 and Ltmp60
	.long	Lset42
Lset43 = Ltmp61-Lfunc_begin2            ##     jumps to Ltmp61
	.long	Lset43
	.byte	1                       ##   On action: 1
Lset44 = Ltmp60-Lfunc_begin2            ## >> Call Site 7 <<
	.long	Lset44
Lset45 = Ltmp62-Ltmp60                  ##   Call between Ltmp60 and Ltmp62
	.long	Lset45
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset46 = Ltmp62-Lfunc_begin2            ## >> Call Site 8 <<
	.long	Lset46
Lset47 = Ltmp63-Ltmp62                  ##   Call between Ltmp62 and Ltmp63
	.long	Lset47
Lset48 = Ltmp64-Lfunc_begin2            ##     jumps to Ltmp64
	.long	Lset48
	.byte	0                       ##   On action: cleanup
Lset49 = Ltmp71-Lfunc_begin2            ## >> Call Site 9 <<
	.long	Lset49
Lset50 = Ltmp72-Ltmp71                  ##   Call between Ltmp71 and Ltmp72
	.long	Lset50
Lset51 = Ltmp73-Lfunc_begin2            ##     jumps to Ltmp73
	.long	Lset51
	.byte	1                       ##   On action: 1
Lset52 = Ltmp72-Lfunc_begin2            ## >> Call Site 10 <<
	.long	Lset52
Lset53 = Ltmp74-Ltmp72                  ##   Call between Ltmp72 and Ltmp74
	.long	Lset53
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset54 = Ltmp74-Lfunc_begin2            ## >> Call Site 11 <<
	.long	Lset54
Lset55 = Ltmp75-Ltmp74                  ##   Call between Ltmp74 and Ltmp75
	.long	Lset55
Lset56 = Ltmp76-Lfunc_begin2            ##     jumps to Ltmp76
	.long	Lset56
	.byte	0                       ##   On action: cleanup
Lset57 = Ltmp83-Lfunc_begin2            ## >> Call Site 12 <<
	.long	Lset57
Lset58 = Ltmp84-Ltmp83                  ##   Call between Ltmp83 and Ltmp84
	.long	Lset58
Lset59 = Ltmp85-Lfunc_begin2            ##     jumps to Ltmp85
	.long	Lset59
	.byte	1                       ##   On action: 1
Lset60 = Ltmp84-Lfunc_begin2            ## >> Call Site 13 <<
	.long	Lset60
Lset61 = Ltmp86-Ltmp84                  ##   Call between Ltmp84 and Ltmp86
	.long	Lset61
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset62 = Ltmp86-Lfunc_begin2            ## >> Call Site 14 <<
	.long	Lset62
Lset63 = Ltmp87-Ltmp86                  ##   Call between Ltmp86 and Ltmp87
	.long	Lset63
Lset64 = Ltmp88-Lfunc_begin2            ##     jumps to Ltmp88
	.long	Lset64
	.byte	0                       ##   On action: cleanup
Lset65 = Ltmp95-Lfunc_begin2            ## >> Call Site 15 <<
	.long	Lset65
Lset66 = Ltmp96-Ltmp95                  ##   Call between Ltmp95 and Ltmp96
	.long	Lset66
Lset67 = Ltmp97-Lfunc_begin2            ##     jumps to Ltmp97
	.long	Lset67
	.byte	1                       ##   On action: 1
Lset68 = Ltmp96-Lfunc_begin2            ## >> Call Site 16 <<
	.long	Lset68
Lset69 = Ltmp98-Ltmp96                  ##   Call between Ltmp96 and Ltmp98
	.long	Lset69
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset70 = Ltmp98-Lfunc_begin2            ## >> Call Site 17 <<
	.long	Lset70
Lset71 = Ltmp99-Ltmp98                  ##   Call between Ltmp98 and Ltmp99
	.long	Lset71
Lset72 = Ltmp100-Lfunc_begin2           ##     jumps to Ltmp100
	.long	Lset72
	.byte	0                       ##   On action: cleanup
Lset73 = Ltmp107-Lfunc_begin2           ## >> Call Site 18 <<
	.long	Lset73
Lset74 = Ltmp108-Ltmp107                ##   Call between Ltmp107 and Ltmp108
	.long	Lset74
Lset75 = Ltmp109-Lfunc_begin2           ##     jumps to Ltmp109
	.long	Lset75
	.byte	1                       ##   On action: 1
Lset76 = Ltmp108-Lfunc_begin2           ## >> Call Site 19 <<
	.long	Lset76
Lset77 = Ltmp110-Ltmp108                ##   Call between Ltmp108 and Ltmp110
	.long	Lset77
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset78 = Ltmp110-Lfunc_begin2           ## >> Call Site 20 <<
	.long	Lset78
Lset79 = Ltmp111-Ltmp110                ##   Call between Ltmp110 and Ltmp111
	.long	Lset79
Lset80 = Ltmp112-Lfunc_begin2           ##     jumps to Ltmp112
	.long	Lset80
	.byte	0                       ##   On action: cleanup
Lset81 = Ltmp119-Lfunc_begin2           ## >> Call Site 21 <<
	.long	Lset81
Lset82 = Ltmp120-Ltmp119                ##   Call between Ltmp119 and Ltmp120
	.long	Lset82
Lset83 = Ltmp121-Lfunc_begin2           ##     jumps to Ltmp121
	.long	Lset83
	.byte	1                       ##   On action: 1
Lset84 = Ltmp120-Lfunc_begin2           ## >> Call Site 22 <<
	.long	Lset84
Lset85 = Ltmp122-Ltmp120                ##   Call between Ltmp120 and Ltmp122
	.long	Lset85
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset86 = Ltmp122-Lfunc_begin2           ## >> Call Site 23 <<
	.long	Lset86
Lset87 = Ltmp123-Ltmp122                ##   Call between Ltmp122 and Ltmp123
	.long	Lset87
Lset88 = Ltmp124-Lfunc_begin2           ##     jumps to Ltmp124
	.long	Lset88
	.byte	0                       ##   On action: cleanup
Lset89 = Ltmp131-Lfunc_begin2           ## >> Call Site 24 <<
	.long	Lset89
Lset90 = Ltmp132-Ltmp131                ##   Call between Ltmp131 and Ltmp132
	.long	Lset90
Lset91 = Ltmp133-Lfunc_begin2           ##     jumps to Ltmp133
	.long	Lset91
	.byte	1                       ##   On action: 1
Lset92 = Ltmp132-Lfunc_begin2           ## >> Call Site 25 <<
	.long	Lset92
Lset93 = Ltmp134-Ltmp132                ##   Call between Ltmp132 and Ltmp134
	.long	Lset93
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset94 = Ltmp134-Lfunc_begin2           ## >> Call Site 26 <<
	.long	Lset94
Lset95 = Ltmp135-Ltmp134                ##   Call between Ltmp134 and Ltmp135
	.long	Lset95
Lset96 = Ltmp136-Lfunc_begin2           ##     jumps to Ltmp136
	.long	Lset96
	.byte	0                       ##   On action: cleanup
Lset97 = Ltmp143-Lfunc_begin2           ## >> Call Site 27 <<
	.long	Lset97
Lset98 = Ltmp144-Ltmp143                ##   Call between Ltmp143 and Ltmp144
	.long	Lset98
Lset99 = Ltmp145-Lfunc_begin2           ##     jumps to Ltmp145
	.long	Lset99
	.byte	1                       ##   On action: 1
Lset100 = Ltmp144-Lfunc_begin2          ## >> Call Site 28 <<
	.long	Lset100
Lset101 = Ltmp146-Ltmp144               ##   Call between Ltmp144 and Ltmp146
	.long	Lset101
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset102 = Ltmp146-Lfunc_begin2          ## >> Call Site 29 <<
	.long	Lset102
Lset103 = Ltmp147-Ltmp146               ##   Call between Ltmp146 and Ltmp147
	.long	Lset103
Lset104 = Ltmp148-Lfunc_begin2          ##     jumps to Ltmp148
	.long	Lset104
	.byte	0                       ##   On action: cleanup
Lset105 = Ltmp155-Lfunc_begin2          ## >> Call Site 30 <<
	.long	Lset105
Lset106 = Ltmp156-Ltmp155               ##   Call between Ltmp155 and Ltmp156
	.long	Lset106
Lset107 = Ltmp157-Lfunc_begin2          ##     jumps to Ltmp157
	.long	Lset107
	.byte	1                       ##   On action: 1
Lset108 = Ltmp156-Lfunc_begin2          ## >> Call Site 31 <<
	.long	Lset108
Lset109 = Ltmp158-Ltmp156               ##   Call between Ltmp156 and Ltmp158
	.long	Lset109
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset110 = Ltmp158-Lfunc_begin2          ## >> Call Site 32 <<
	.long	Lset110
Lset111 = Ltmp159-Ltmp158               ##   Call between Ltmp158 and Ltmp159
	.long	Lset111
Lset112 = Ltmp160-Lfunc_begin2          ##     jumps to Ltmp160
	.long	Lset112
	.byte	0                       ##   On action: cleanup
Lset113 = Ltmp167-Lfunc_begin2          ## >> Call Site 33 <<
	.long	Lset113
Lset114 = Ltmp168-Ltmp167               ##   Call between Ltmp167 and Ltmp168
	.long	Lset114
Lset115 = Ltmp169-Lfunc_begin2          ##     jumps to Ltmp169
	.long	Lset115
	.byte	1                       ##   On action: 1
Lset116 = Ltmp168-Lfunc_begin2          ## >> Call Site 34 <<
	.long	Lset116
Lset117 = Ltmp170-Ltmp168               ##   Call between Ltmp168 and Ltmp170
	.long	Lset117
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset118 = Ltmp170-Lfunc_begin2          ## >> Call Site 35 <<
	.long	Lset118
Lset119 = Ltmp171-Ltmp170               ##   Call between Ltmp170 and Ltmp171
	.long	Lset119
Lset120 = Ltmp172-Lfunc_begin2          ##     jumps to Ltmp172
	.long	Lset120
	.byte	0                       ##   On action: cleanup
Lset121 = Ltmp179-Lfunc_begin2          ## >> Call Site 36 <<
	.long	Lset121
Lset122 = Ltmp180-Ltmp179               ##   Call between Ltmp179 and Ltmp180
	.long	Lset122
Lset123 = Ltmp181-Lfunc_begin2          ##     jumps to Ltmp181
	.long	Lset123
	.byte	1                       ##   On action: 1
Lset124 = Ltmp180-Lfunc_begin2          ## >> Call Site 37 <<
	.long	Lset124
Lset125 = Ltmp182-Ltmp180               ##   Call between Ltmp180 and Ltmp182
	.long	Lset125
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset126 = Ltmp182-Lfunc_begin2          ## >> Call Site 38 <<
	.long	Lset126
Lset127 = Ltmp183-Ltmp182               ##   Call between Ltmp182 and Ltmp183
	.long	Lset127
Lset128 = Ltmp184-Lfunc_begin2          ##     jumps to Ltmp184
	.long	Lset128
	.byte	0                       ##   On action: cleanup
Lset129 = Ltmp191-Lfunc_begin2          ## >> Call Site 39 <<
	.long	Lset129
Lset130 = Ltmp192-Ltmp191               ##   Call between Ltmp191 and Ltmp192
	.long	Lset130
Lset131 = Ltmp193-Lfunc_begin2          ##     jumps to Ltmp193
	.long	Lset131
	.byte	1                       ##   On action: 1
Lset132 = Ltmp41-Lfunc_begin2           ## >> Call Site 40 <<
	.long	Lset132
Lset133 = Ltmp42-Ltmp41                 ##   Call between Ltmp41 and Ltmp42
	.long	Lset133
Lset134 = Ltmp43-Lfunc_begin2           ##     jumps to Ltmp43
	.long	Lset134
	.byte	0                       ##   On action: cleanup
Lset135 = Ltmp53-Lfunc_begin2           ## >> Call Site 41 <<
	.long	Lset135
Lset136 = Ltmp54-Ltmp53                 ##   Call between Ltmp53 and Ltmp54
	.long	Lset136
Lset137 = Ltmp55-Lfunc_begin2           ##     jumps to Ltmp55
	.long	Lset137
	.byte	0                       ##   On action: cleanup
Lset138 = Ltmp65-Lfunc_begin2           ## >> Call Site 42 <<
	.long	Lset138
Lset139 = Ltmp66-Ltmp65                 ##   Call between Ltmp65 and Ltmp66
	.long	Lset139
Lset140 = Ltmp67-Lfunc_begin2           ##     jumps to Ltmp67
	.long	Lset140
	.byte	0                       ##   On action: cleanup
Lset141 = Ltmp77-Lfunc_begin2           ## >> Call Site 43 <<
	.long	Lset141
Lset142 = Ltmp78-Ltmp77                 ##   Call between Ltmp77 and Ltmp78
	.long	Lset142
Lset143 = Ltmp79-Lfunc_begin2           ##     jumps to Ltmp79
	.long	Lset143
	.byte	0                       ##   On action: cleanup
Lset144 = Ltmp89-Lfunc_begin2           ## >> Call Site 44 <<
	.long	Lset144
Lset145 = Ltmp90-Ltmp89                 ##   Call between Ltmp89 and Ltmp90
	.long	Lset145
Lset146 = Ltmp91-Lfunc_begin2           ##     jumps to Ltmp91
	.long	Lset146
	.byte	0                       ##   On action: cleanup
Lset147 = Ltmp101-Lfunc_begin2          ## >> Call Site 45 <<
	.long	Lset147
Lset148 = Ltmp102-Ltmp101               ##   Call between Ltmp101 and Ltmp102
	.long	Lset148
Lset149 = Ltmp103-Lfunc_begin2          ##     jumps to Ltmp103
	.long	Lset149
	.byte	0                       ##   On action: cleanup
Lset150 = Ltmp113-Lfunc_begin2          ## >> Call Site 46 <<
	.long	Lset150
Lset151 = Ltmp114-Ltmp113               ##   Call between Ltmp113 and Ltmp114
	.long	Lset151
Lset152 = Ltmp115-Lfunc_begin2          ##     jumps to Ltmp115
	.long	Lset152
	.byte	0                       ##   On action: cleanup
Lset153 = Ltmp125-Lfunc_begin2          ## >> Call Site 47 <<
	.long	Lset153
Lset154 = Ltmp126-Ltmp125               ##   Call between Ltmp125 and Ltmp126
	.long	Lset154
Lset155 = Ltmp127-Lfunc_begin2          ##     jumps to Ltmp127
	.long	Lset155
	.byte	0                       ##   On action: cleanup
Lset156 = Ltmp137-Lfunc_begin2          ## >> Call Site 48 <<
	.long	Lset156
Lset157 = Ltmp138-Ltmp137               ##   Call between Ltmp137 and Ltmp138
	.long	Lset157
Lset158 = Ltmp139-Lfunc_begin2          ##     jumps to Ltmp139
	.long	Lset158
	.byte	0                       ##   On action: cleanup
Lset159 = Ltmp149-Lfunc_begin2          ## >> Call Site 49 <<
	.long	Lset159
Lset160 = Ltmp150-Ltmp149               ##   Call between Ltmp149 and Ltmp150
	.long	Lset160
Lset161 = Ltmp151-Lfunc_begin2          ##     jumps to Ltmp151
	.long	Lset161
	.byte	0                       ##   On action: cleanup
Lset162 = Ltmp161-Lfunc_begin2          ## >> Call Site 50 <<
	.long	Lset162
Lset163 = Ltmp162-Ltmp161               ##   Call between Ltmp161 and Ltmp162
	.long	Lset163
Lset164 = Ltmp163-Lfunc_begin2          ##     jumps to Ltmp163
	.long	Lset164
	.byte	0                       ##   On action: cleanup
Lset165 = Ltmp173-Lfunc_begin2          ## >> Call Site 51 <<
	.long	Lset165
Lset166 = Ltmp174-Ltmp173               ##   Call between Ltmp173 and Ltmp174
	.long	Lset166
Lset167 = Ltmp175-Lfunc_begin2          ##     jumps to Ltmp175
	.long	Lset167
	.byte	0                       ##   On action: cleanup
Lset168 = Ltmp185-Lfunc_begin2          ## >> Call Site 52 <<
	.long	Lset168
Lset169 = Ltmp186-Ltmp185               ##   Call between Ltmp185 and Ltmp186
	.long	Lset169
Lset170 = Ltmp187-Lfunc_begin2          ##     jumps to Ltmp187
	.long	Lset170
	.byte	0                       ##   On action: cleanup
Lset171 = Ltmp186-Lfunc_begin2          ## >> Call Site 53 <<
	.long	Lset171
Lset172 = Ltmp44-Ltmp186                ##   Call between Ltmp186 and Ltmp44
	.long	Lset172
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset173 = Ltmp44-Lfunc_begin2           ## >> Call Site 54 <<
	.long	Lset173
Lset174 = Ltmp45-Ltmp44                 ##   Call between Ltmp44 and Ltmp45
	.long	Lset174
Lset175 = Ltmp46-Lfunc_begin2           ##     jumps to Ltmp46
	.long	Lset175
	.byte	1                       ##   On action: 1
Lset176 = Ltmp56-Lfunc_begin2           ## >> Call Site 55 <<
	.long	Lset176
Lset177 = Ltmp57-Ltmp56                 ##   Call between Ltmp56 and Ltmp57
	.long	Lset177
Lset178 = Ltmp58-Lfunc_begin2           ##     jumps to Ltmp58
	.long	Lset178
	.byte	1                       ##   On action: 1
Lset179 = Ltmp68-Lfunc_begin2           ## >> Call Site 56 <<
	.long	Lset179
Lset180 = Ltmp69-Ltmp68                 ##   Call between Ltmp68 and Ltmp69
	.long	Lset180
Lset181 = Ltmp70-Lfunc_begin2           ##     jumps to Ltmp70
	.long	Lset181
	.byte	1                       ##   On action: 1
Lset182 = Ltmp80-Lfunc_begin2           ## >> Call Site 57 <<
	.long	Lset182
Lset183 = Ltmp81-Ltmp80                 ##   Call between Ltmp80 and Ltmp81
	.long	Lset183
Lset184 = Ltmp82-Lfunc_begin2           ##     jumps to Ltmp82
	.long	Lset184
	.byte	1                       ##   On action: 1
Lset185 = Ltmp92-Lfunc_begin2           ## >> Call Site 58 <<
	.long	Lset185
Lset186 = Ltmp93-Ltmp92                 ##   Call between Ltmp92 and Ltmp93
	.long	Lset186
Lset187 = Ltmp94-Lfunc_begin2           ##     jumps to Ltmp94
	.long	Lset187
	.byte	1                       ##   On action: 1
Lset188 = Ltmp104-Lfunc_begin2          ## >> Call Site 59 <<
	.long	Lset188
Lset189 = Ltmp105-Ltmp104               ##   Call between Ltmp104 and Ltmp105
	.long	Lset189
Lset190 = Ltmp106-Lfunc_begin2          ##     jumps to Ltmp106
	.long	Lset190
	.byte	1                       ##   On action: 1
Lset191 = Ltmp116-Lfunc_begin2          ## >> Call Site 60 <<
	.long	Lset191
Lset192 = Ltmp117-Ltmp116               ##   Call between Ltmp116 and Ltmp117
	.long	Lset192
Lset193 = Ltmp118-Lfunc_begin2          ##     jumps to Ltmp118
	.long	Lset193
	.byte	1                       ##   On action: 1
Lset194 = Ltmp128-Lfunc_begin2          ## >> Call Site 61 <<
	.long	Lset194
Lset195 = Ltmp129-Ltmp128               ##   Call between Ltmp128 and Ltmp129
	.long	Lset195
Lset196 = Ltmp130-Lfunc_begin2          ##     jumps to Ltmp130
	.long	Lset196
	.byte	1                       ##   On action: 1
Lset197 = Ltmp140-Lfunc_begin2          ## >> Call Site 62 <<
	.long	Lset197
Lset198 = Ltmp141-Ltmp140               ##   Call between Ltmp140 and Ltmp141
	.long	Lset198
Lset199 = Ltmp142-Lfunc_begin2          ##     jumps to Ltmp142
	.long	Lset199
	.byte	1                       ##   On action: 1
Lset200 = Ltmp152-Lfunc_begin2          ## >> Call Site 63 <<
	.long	Lset200
Lset201 = Ltmp153-Ltmp152               ##   Call between Ltmp152 and Ltmp153
	.long	Lset201
Lset202 = Ltmp154-Lfunc_begin2          ##     jumps to Ltmp154
	.long	Lset202
	.byte	1                       ##   On action: 1
Lset203 = Ltmp164-Lfunc_begin2          ## >> Call Site 64 <<
	.long	Lset203
Lset204 = Ltmp165-Ltmp164               ##   Call between Ltmp164 and Ltmp165
	.long	Lset204
Lset205 = Ltmp166-Lfunc_begin2          ##     jumps to Ltmp166
	.long	Lset205
	.byte	1                       ##   On action: 1
Lset206 = Ltmp176-Lfunc_begin2          ## >> Call Site 65 <<
	.long	Lset206
Lset207 = Ltmp177-Ltmp176               ##   Call between Ltmp176 and Ltmp177
	.long	Lset207
Lset208 = Ltmp178-Lfunc_begin2          ##     jumps to Ltmp178
	.long	Lset208
	.byte	1                       ##   On action: 1
Lset209 = Ltmp188-Lfunc_begin2          ## >> Call Site 66 <<
	.long	Lset209
Lset210 = Ltmp189-Ltmp188               ##   Call between Ltmp188 and Ltmp189
	.long	Lset210
Lset211 = Ltmp190-Lfunc_begin2          ##     jumps to Ltmp190
	.long	Lset211
	.byte	1                       ##   On action: 1
Lset212 = Ltmp189-Lfunc_begin2          ## >> Call Site 67 <<
	.long	Lset212
Lset213 = Lfunc_end2-Ltmp189            ##   Call between Ltmp189 and Lfunc_end2
	.long	Lset213
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.globl	__ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEEaSINS9_2qi6detail13parser_binderINSS_11alternativeINSC_INSS_15any_real_parserIdNSS_20strict_real_policiesIdEEEENSC_INSS_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEENS_11enable_if_cIXntsr11is_integralIT_EE5valueERSQ_E4typeES1A_
	.weak_def_can_be_hidden	__ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEEaSINS9_2qi6detail13parser_binderINSS_11alternativeINSC_INSS_15any_real_parserIdNSS_20strict_real_policiesIdEEEENSC_INSS_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEENS_11enable_if_cIXntsr11is_integralIT_EE5valueERSQ_E4typeES1A_
	.align	4, 0x90
__ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEEaSINS9_2qi6detail13parser_binderINSS_11alternativeINSC_INSS_15any_real_parserIdNSS_20strict_real_policiesIdEEEENSC_INSS_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEENS_11enable_if_cIXntsr11is_integralIT_EE5valueERSQ_E4typeES1A_: ## @_ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEEaSINS9_2qi6detail13parser_binderINSS_11alternativeINSC_INSS_15any_real_parserIdNSS_20strict_real_policiesIdEEEENSC_INSS_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEENS_11enable_if_cIXntsr11is_integralIT_EE5valueERSQ_E4typeES1A_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp208:
	.cfi_def_cfa_offset 16
Ltmp209:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp210:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	subq	$40, %rsp
Ltmp211:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE9assign_toINS9_2qi6detail13parser_binderINSR_11alternativeINSC_INSR_15any_real_parserIdNSR_20strict_real_policiesIdEEEENSC_INSR_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEEvT_E13stored_vtable@GOTPCREL(%rip), %rax
	movq	%rax, -40(%rbp)
Ltmp199:
	leaq	-40(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE4swapERSP_
Ltmp200:
## BB#1:
	movq	-40(%rbp), %rax
	testq	%rax, %rax
	je	LBB4_6
## BB#2:
	testb	$1, %al
	jne	LBB4_5
## BB#3:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB4_5
## BB#4:
	leaq	-32(%rbp), %rdi
Ltmp205:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp206:
LBB4_5:                                 ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i.i.1
	movq	$0, -40(%rbp)
LBB4_6:                                 ## %_ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEED1Ev.exit2
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	retq
LBB4_7:
Ltmp201:
	movq	%rax, %rbx
	movq	-40(%rbp), %rax
	testq	%rax, %rax
	je	LBB4_12
## BB#8:
	testb	$1, %al
	jne	LBB4_11
## BB#9:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB4_11
## BB#10:
	leaq	-32(%rbp), %rdi
Ltmp202:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp203:
LBB4_11:                                ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i.i
	movq	$0, -40(%rbp)
LBB4_12:                                ## %_ZN5boost8functionIFbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEEED1Ev.exit
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB4_13:
Ltmp207:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB4_14:
Ltmp204:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table4:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset214 = Ltmp199-Lfunc_begin3          ## >> Call Site 1 <<
	.long	Lset214
Lset215 = Ltmp200-Ltmp199               ##   Call between Ltmp199 and Ltmp200
	.long	Lset215
Lset216 = Ltmp201-Lfunc_begin3          ##     jumps to Ltmp201
	.long	Lset216
	.byte	0                       ##   On action: cleanup
Lset217 = Ltmp205-Lfunc_begin3          ## >> Call Site 2 <<
	.long	Lset217
Lset218 = Ltmp206-Ltmp205               ##   Call between Ltmp205 and Ltmp206
	.long	Lset218
Lset219 = Ltmp207-Lfunc_begin3          ##     jumps to Ltmp207
	.long	Lset219
	.byte	1                       ##   On action: 1
Lset220 = Ltmp202-Lfunc_begin3          ## >> Call Site 3 <<
	.long	Lset220
Lset221 = Ltmp203-Ltmp202               ##   Call between Ltmp202 and Ltmp203
	.long	Lset221
Lset222 = Ltmp204-Lfunc_begin3          ##     jumps to Ltmp204
	.long	Lset222
	.byte	1                       ##   On action: 1
Lset223 = Ltmp203-Lfunc_begin3          ## >> Call Site 4 <<
	.long	Lset223
Lset224 = Lfunc_end3-Ltmp203            ##   Call between Ltmp203 and Lfunc_end3
	.long	Lset224
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE4swapERSP_
	.weak_def_can_be_hidden	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE4swapERSP_
	.align	4, 0x90
__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE4swapERSP_: ## @_ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE4swapERSP_
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp225:
	.cfi_def_cfa_offset 16
Ltmp226:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp227:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$32, %rsp
Ltmp228:
	.cfi_offset %rbx, -32
Ltmp229:
	.cfi_offset %r14, -24
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%rbx, %r14
	je	LBB5_9
## BB#1:
	movq	$0, -48(%rbp)
Ltmp212:
	leaq	-48(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
Ltmp213:
## BB#2:
Ltmp214:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
Ltmp215:
## BB#3:
Ltmp216:
	leaq	-48(%rbp), %rsi
	movq	%r14, %rdi
	callq	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
Ltmp217:
## BB#4:
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	LBB5_9
## BB#5:
	testb	$1, %al
	jne	LBB5_8
## BB#6:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB5_8
## BB#7:
	leaq	-40(%rbp), %rdi
Ltmp222:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp223:
LBB5_8:                                 ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i.1
	movq	$0, -48(%rbp)
LBB5_9:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB5_10:
Ltmp218:
	movq	%rax, %rbx
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	LBB5_15
## BB#11:
	testb	$1, %al
	jne	LBB5_14
## BB#12:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB5_14
## BB#13:
	leaq	-40(%rbp), %rdi
Ltmp219:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp220:
LBB5_14:                                ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i.i.i
	movq	$0, -48(%rbp)
LBB5_15:                                ## %_ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEED1Ev.exit
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB5_16:
Ltmp224:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB5_17:
Ltmp221:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset225 = Ltmp212-Lfunc_begin4          ## >> Call Site 1 <<
	.long	Lset225
Lset226 = Ltmp217-Ltmp212               ##   Call between Ltmp212 and Ltmp217
	.long	Lset226
Lset227 = Ltmp218-Lfunc_begin4          ##     jumps to Ltmp218
	.long	Lset227
	.byte	0                       ##   On action: cleanup
Lset228 = Ltmp222-Lfunc_begin4          ## >> Call Site 2 <<
	.long	Lset228
Lset229 = Ltmp223-Ltmp222               ##   Call between Ltmp222 and Ltmp223
	.long	Lset229
Lset230 = Ltmp224-Lfunc_begin4          ##     jumps to Ltmp224
	.long	Lset230
	.byte	1                       ##   On action: 1
Lset231 = Ltmp219-Lfunc_begin4          ## >> Call Site 3 <<
	.long	Lset231
Lset232 = Ltmp220-Ltmp219               ##   Call between Ltmp219 and Ltmp220
	.long	Lset232
Lset233 = Ltmp221-Lfunc_begin4          ##     jumps to Ltmp221
	.long	Lset233
	.byte	1                       ##   On action: 1
Lset234 = Ltmp220-Lfunc_begin4          ## >> Call Site 4 <<
	.long	Lset234
Lset235 = Lfunc_end4-Ltmp220            ##   Call between Ltmp220 and Lfunc_end4
	.long	Lset235
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE6manageERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeE
	.weak_definition	__ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE6manageERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeE
	.align	4, 0x90
__ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE6manageERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeE: ## @_ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE6manageERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp230:
	.cfi_def_cfa_offset 16
Ltmp231:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp232:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp233:
	.cfi_offset %rbx, -32
Ltmp234:
	.cfi_offset %r14, -24
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$4, %edx
	je	LBB6_7
## BB#1:
	cmpl	$2, %edx
	jb	LBB6_8
## BB#2:
	je	LBB6_8
## BB#3:
	cmpl	$3, %edx
	jne	LBB6_7
## BB#4:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	__ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE@GOTPCREL(%rip), %rsi
	callq	_strcmp
	testl	%eax, %eax
	je	LBB6_5
## BB#6:
	movq	$0, (%rbx)
	jmp	LBB6_8
LBB6_7:
	movq	__ZTIN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE@GOTPCREL(%rip), %rax
	movq	%rax, (%rbx)
	movw	$0, 8(%rbx)
LBB6_8:                                 ## %_ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE7managerERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeENS1_16function_obj_tagE.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB6_5:
	movq	%r14, (%rbx)
	jmp	LBB6_8
	.cfi_endproc

	.globl	__ZN5boost6detail8function21function_obj_invoker4INS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEbRNSt3__111__wrap_iterIPKcEERKSS_RNS3_7contextINS9_IRNS_7variantIiJdEEESG_EENS8_6vectorIJEEEEERKNS3_11unused_typeEE6invokeERNS1_15function_bufferEST_SV_S14_S17_
	.weak_definition	__ZN5boost6detail8function21function_obj_invoker4INS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEbRNSt3__111__wrap_iterIPKcEERKSS_RNS3_7contextINS9_IRNS_7variantIiJdEEESG_EENS8_6vectorIJEEEEERKNS3_11unused_typeEE6invokeERNS1_15function_bufferEST_SV_S14_S17_
	.align	4, 0x90
__ZN5boost6detail8function21function_obj_invoker4INS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEbRNSt3__111__wrap_iterIPKcEERKSS_RNS3_7contextINS9_IRNS_7variantIiJdEEESG_EENS8_6vectorIJEEEEERKNS3_11unused_typeEE6invokeERNS1_15function_bufferEST_SV_S14_S17_: ## @_ZN5boost6detail8function21function_obj_invoker4INS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEbRNSt3__111__wrap_iterIPKcEERKSS_RNS3_7contextINS9_IRNS_7variantIiJdEEESG_EENS8_6vectorIJEEEEERKNS3_11unused_typeEE6invokeERNS1_15function_bufferEST_SV_S14_S17_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp235:
	.cfi_def_cfa_offset 16
Ltmp236:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp237:
	.cfi_def_cfa_register %rbp
	movq	(%rcx), %r9
	popq	%rbp
	jmp	__ZNK5boost6spirit2qi11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS4_INS1_14any_int_parserIiLj10ELj1ELin1EEENS3_4nil_EEEEEE5parseINSt3__111__wrap_iterIPKcEENS0_7contextINS4_IRNS_7variantIiJdEEESB_EENS3_6vectorIJEEEEENS0_11unused_typeESN_EEbRT_RKSU_RT0_RKT1_RT2_ ## TAILCALL
	.cfi_endproc

	.globl	__ZNK5boost6spirit2qi11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS4_INS1_14any_int_parserIiLj10ELj1ELin1EEENS3_4nil_EEEEEE5parseINSt3__111__wrap_iterIPKcEENS0_7contextINS4_IRNS_7variantIiJdEEESB_EENS3_6vectorIJEEEEENS0_11unused_typeESN_EEbRT_RKSU_RT0_RKT1_RT2_
	.weak_def_can_be_hidden	__ZNK5boost6spirit2qi11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS4_INS1_14any_int_parserIiLj10ELj1ELin1EEENS3_4nil_EEEEEE5parseINSt3__111__wrap_iterIPKcEENS0_7contextINS4_IRNS_7variantIiJdEEESB_EENS3_6vectorIJEEEEENS0_11unused_typeESN_EEbRT_RKSU_RT0_RKT1_RT2_
	.align	4, 0x90
__ZNK5boost6spirit2qi11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS4_INS1_14any_int_parserIiLj10ELj1ELin1EEENS3_4nil_EEEEEE5parseINSt3__111__wrap_iterIPKcEENS0_7contextINS4_IRNS_7variantIiJdEEESB_EENS3_6vectorIJEEEEENS0_11unused_typeESN_EEbRT_RKSU_RT0_RKT1_RT2_: ## @_ZNK5boost6spirit2qi11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS4_INS1_14any_int_parserIiLj10ELj1ELin1EEENS3_4nil_EEEEEE5parseINSt3__111__wrap_iterIPKcEENS0_7contextINS4_IRNS_7variantIiJdEEESB_EENS3_6vectorIJEEEEENS0_11unused_typeESN_EEbRT_RKSU_RT0_RKT1_RT2_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp238:
	.cfi_def_cfa_offset 16
Ltmp239:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp240:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp241:
	.cfi_offset %rbx, -56
Ltmp242:
	.cfi_offset %r12, -48
Ltmp243:
	.cfi_offset %r13, -40
Ltmp244:
	.cfi_offset %r14, -32
Ltmp245:
	.cfi_offset %r15, -24
	movq	%r9, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	leaq	-56(%rbp), %rdx
	leaq	-48(%rbp), %rcx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	__ZN5boost6spirit2qi6detail9real_implIdNS1_20strict_real_policiesIdEEE5parseINSt3__111__wrap_iterIPKcEEdEEbRT_RKSD_RT0_RKS5_
	testb	%al, %al
	je	LBB8_2
## BB#1:                                ## %_ZNK5boost6spirit2qi6detail20alternative_functionINSt3__111__wrap_iterIPKcEENS0_7contextINS_6fusion4consIRNS_7variantIiJdEEENSA_4nil_EEENSA_6vectorIJEEEEENS0_11unused_typeESD_EclINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEEEEbRKT_.exit.thread.i.i.i
	leaq	-56(%rbp), %rdi
	movq	%r14, %rsi
	callq	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEdvE4callIdEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	movb	$1, %al
	jmp	LBB8_77
LBB8_2:
	movq	(%r15), %rax
	movq	(%rbx), %rcx
	cmpq	%rcx, %rax
	je	LBB8_76
## BB#3:
	movzbl	(%rax), %esi
	cmpl	$45, %esi
	je	LBB8_5
## BB#4:
	movzbl	%sil, %edx
	cmpl	$43, %edx
	movq	%rax, %rdx
	jne	LBB8_42
LBB8_5:                                 ## %_ZN5boost6spirit2qi12extract_signINSt3__111__wrap_iterIPKcEEEEbRT_RKS8_.exit.i.i.i.i.i.i.i.i.i.i.i
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movq	(%rbx), %rcx
	movzbl	%sil, %esi
	cmpl	$45, %esi
	jne	LBB8_42
## BB#6:
	xorl	%esi, %esi
	cmpq	%rcx, %rdx
	je	LBB8_7
## BB#8:                                ## %.lr.ph49.i.i.preheader
	movl	$1, %ebx
	subq	%rcx, %rbx
	addq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rdx, %rdi
	.align	4, 0x90
LBB8_9:                                 ## %.lr.ph49.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	1(%rax,%rsi), %edx
	cmpl	$48, %edx
	jne	LBB8_10
## BB#78:                               ##   in Loop: Header=BB8_9 Depth=1
	leaq	2(%rax,%rsi), %rdi
	incq	%rsi
	movq	%rbx, %rdx
	addq	%rsi, %rdx
	jne	LBB8_9
	jmp	LBB8_12
LBB8_42:                                ## %_ZN5boost6spirit2qi12extract_signINSt3__111__wrap_iterIPKcEEEEbRT_RKS8_.exit.i.i.i.i.i.i.i.i.i.i.i._crit_edge
	xorl	%edi, %edi
	cmpq	%rcx, %rdx
	je	LBB8_75
	.align	4, 0x90
LBB8_43:                                ## %.lr.ph39.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %esi
	cmpl	$48, %esi
	jne	LBB8_45
## BB#44:                               ##   in Loop: Header=BB8_43 Depth=1
	incq	%rdx
	incq	%rdi
	cmpq	%rdx, %rcx
	jne	LBB8_43
	jmp	LBB8_46
LBB8_45:                                ## %.critedge.i.i
	cmpq	%rcx, %rdx
	je	LBB8_46
## BB#48:
	movsbl	(%rdx), %esi
	movb	%sil, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$10, %ebx
	jae	LBB8_46
## BB#49:
	addl	$-48, %esi
	leaq	1(%rdx), %r8
	cmpq	%rcx, %r8
	je	LBB8_73
## BB#50:                               ## %.lr.ph.i.i.preheader
	movq	%rcx, %r10
	subq	%rdx, %r10
	leaq	-1(%r10), %rbx
	movq	%rbx, -72(%rbp)         ## 8-byte Spill
	leaq	-3(%r10), %r9
	leaq	-2(%r10), %r10
	xorl	%r12d, %r12d
LBB8_51:                                ## %.lr.ph.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movsbl	1(%rdx,%r12), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_73
## BB#52:                               ##   in Loop: Header=BB8_51 Depth=1
	leaq	(%rdi,%r12), %r11
	cmpq	$7, %r11
	ja	LBB8_54
## BB#53:                               ##   in Loop: Header=BB8_51 Depth=1
	leal	(%rsi,%rsi,4), %esi
	leal	-48(%r13,%rsi,2), %esi
	jmp	LBB8_57
LBB8_54:                                ##   in Loop: Header=BB8_51 Depth=1
	cmpl	$214748364, %esi        ## imm = 0xCCCCCCC
	jg	LBB8_79
## BB#55:                               ##   in Loop: Header=BB8_51 Depth=1
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r13d, %ebx
	cmpl	%ebx, %esi
	jg	LBB8_79
## BB#56:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.5.i.i
                                        ##   in Loop: Header=BB8_51 Depth=1
	leal	-48(%rsi,%r13), %esi
LBB8_57:                                ##   in Loop: Header=BB8_51 Depth=1
	cmpq	%r12, %r10
	je	LBB8_72
## BB#58:                               ##   in Loop: Header=BB8_51 Depth=1
	leaq	2(%rdx,%r12), %r8
	movsbl	(%r8), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_73
## BB#59:                               ##   in Loop: Header=BB8_51 Depth=1
	incq	%r11
	cmpq	$7, %r11
	ja	LBB8_61
## BB#60:                               ##   in Loop: Header=BB8_51 Depth=1
	leal	(%rsi,%rsi,4), %esi
	leal	-48(%r13,%rsi,2), %esi
	jmp	LBB8_64
LBB8_61:                                ##   in Loop: Header=BB8_51 Depth=1
	cmpl	$214748364, %esi        ## imm = 0xCCCCCCC
	jg	LBB8_79
## BB#62:                               ##   in Loop: Header=BB8_51 Depth=1
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r13d, %ebx
	cmpl	%ebx, %esi
	jg	LBB8_79
## BB#63:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.1.i.i
                                        ##   in Loop: Header=BB8_51 Depth=1
	leal	-48(%rsi,%r13), %esi
LBB8_64:                                ##   in Loop: Header=BB8_51 Depth=1
	cmpq	%r12, %r9
	je	LBB8_72
## BB#65:                               ##   in Loop: Header=BB8_51 Depth=1
	leaq	3(%rdx,%r12), %r8
	movsbl	(%r8), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_73
## BB#66:                               ##   in Loop: Header=BB8_51 Depth=1
	incq	%r11
	cmpq	$7, %r11
	ja	LBB8_68
## BB#67:                               ##   in Loop: Header=BB8_51 Depth=1
	leal	(%rsi,%rsi,4), %esi
	leal	-48(%r13,%rsi,2), %esi
	jmp	LBB8_71
LBB8_68:                                ##   in Loop: Header=BB8_51 Depth=1
	cmpl	$214748364, %esi        ## imm = 0xCCCCCCC
	jg	LBB8_79
## BB#69:                               ##   in Loop: Header=BB8_51 Depth=1
	addl	%esi, %esi
	leal	(%rsi,%rsi,4), %esi
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r13d, %ebx
	cmpl	%ebx, %esi
	jg	LBB8_79
## BB#70:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.i.i
                                        ##   in Loop: Header=BB8_51 Depth=1
	leal	-48(%rsi,%r13), %esi
LBB8_71:                                ##   in Loop: Header=BB8_51 Depth=1
	leaq	4(%rdx,%r12), %r8
	addq	$3, %r12
	cmpq	%r12, -72(%rbp)         ## 8-byte Folded Reload
	jne	LBB8_51
	jmp	LBB8_73
LBB8_46:                                ## %.critedge.thread.i.i
	testq	%rdi, %rdi
	je	LBB8_75
## BB#47:
	movl	$0, -60(%rbp)
	movq	%rdx, (%r15)
	jmp	LBB8_41
LBB8_7:
	movq	%rdx, %rdi
	jmp	LBB8_11
LBB8_10:
	leaq	1(%rax,%rsi), %rdx
LBB8_11:                                ## %.critedge.i.i.4
	cmpq	%rcx, %rdx
	je	LBB8_12
## BB#14:
	movsbl	(%rdx), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$10, %ebx
	jae	LBB8_12
## BB#15:
	movl	$48, %edi
	subl	%r8d, %edi
	leaq	1(%rdx), %r8
	cmpq	%rcx, %r8
	je	LBB8_39
## BB#16:                               ## %.lr.ph.i.i.6.preheader
	movq	%rcx, %r10
	subq	%rdx, %r10
	leaq	-1(%r10), %rbx
	movq	%rbx, -72(%rbp)         ## 8-byte Spill
	leaq	-3(%r10), %r9
	leaq	-2(%r10), %r10
	xorl	%r12d, %r12d
LBB8_17:                                ## %.lr.ph.i.i.6
                                        ## =>This Inner Loop Header: Depth=1
	movsbl	1(%rdx,%r12), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_39
## BB#18:                               ##   in Loop: Header=BB8_17 Depth=1
	leaq	(%rsi,%r12), %r11
	cmpq	$7, %r11
	ja	LBB8_20
## BB#19:                               ##   in Loop: Header=BB8_17 Depth=1
	leal	(%rdi,%rdi,4), %edi
	leal	48(%rdi,%rdi), %edi
	jmp	LBB8_23
LBB8_20:                                ##   in Loop: Header=BB8_17 Depth=1
	cmpl	$-214748364, %edi       ## imm = 0xFFFFFFFFF3333334
	jl	LBB8_74
## BB#21:                               ##   in Loop: Header=BB8_17 Depth=1
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	leal	2147483600(%r13), %ebx
	cmpl	%ebx, %edi
	jl	LBB8_74
## BB#22:                               ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.9.i.i
                                        ##   in Loop: Header=BB8_17 Depth=1
	addl	$48, %edi
LBB8_23:                                ##   in Loop: Header=BB8_17 Depth=1
	subl	%r13d, %edi
	cmpq	%r12, %r10
	je	LBB8_38
## BB#24:                               ##   in Loop: Header=BB8_17 Depth=1
	leaq	2(%rdx,%r12), %r8
	movsbl	(%r8), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_39
## BB#25:                               ##   in Loop: Header=BB8_17 Depth=1
	incq	%r11
	cmpq	$7, %r11
	ja	LBB8_27
## BB#26:                               ##   in Loop: Header=BB8_17 Depth=1
	leal	(%rdi,%rdi,4), %edi
	leal	48(%rdi,%rdi), %edi
	jmp	LBB8_30
LBB8_27:                                ##   in Loop: Header=BB8_17 Depth=1
	cmpl	$-214748364, %edi       ## imm = 0xFFFFFFFFF3333334
	jl	LBB8_74
## BB#28:                               ##   in Loop: Header=BB8_17 Depth=1
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	leal	2147483600(%r13), %ebx
	cmpl	%ebx, %edi
	jl	LBB8_74
## BB#29:                               ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.3.i.i
                                        ##   in Loop: Header=BB8_17 Depth=1
	addl	$48, %edi
LBB8_30:                                ##   in Loop: Header=BB8_17 Depth=1
	subl	%r13d, %edi
	cmpq	%r12, %r9
	je	LBB8_38
## BB#31:                               ##   in Loop: Header=BB8_17 Depth=1
	leaq	3(%rdx,%r12), %r8
	movsbl	(%r8), %r13d
	movb	%r13b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB8_39
## BB#32:                               ##   in Loop: Header=BB8_17 Depth=1
	incq	%r11
	cmpq	$7, %r11
	ja	LBB8_34
## BB#33:                               ##   in Loop: Header=BB8_17 Depth=1
	leal	(%rdi,%rdi,4), %edi
	leal	48(%rdi,%rdi), %edi
	jmp	LBB8_37
LBB8_34:                                ##   in Loop: Header=BB8_17 Depth=1
	cmpl	$-214748364, %edi       ## imm = 0xFFFFFFFFF3333334
	jl	LBB8_74
## BB#35:                               ##   in Loop: Header=BB8_17 Depth=1
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	leal	2147483600(%r13), %ebx
	cmpl	%ebx, %edi
	jl	LBB8_74
## BB#36:                               ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.i.i
                                        ##   in Loop: Header=BB8_17 Depth=1
	addl	$48, %edi
LBB8_37:                                ##   in Loop: Header=BB8_17 Depth=1
	subl	%r13d, %edi
	leaq	4(%rdx,%r12), %r8
	addq	$3, %r12
	cmpq	%r12, -72(%rbp)         ## 8-byte Folded Reload
	jne	LBB8_17
	jmp	LBB8_39
LBB8_12:                                ## %.critedge.thread.i.i.5
	testq	%rsi, %rsi
	je	LBB8_75
## BB#13:
	movl	$0, -60(%rbp)
	movq	%rdi, (%r15)
	jmp	LBB8_41
LBB8_74:                                ## %_ZN5boost6spirit2qi6detail13int_extractorILj10ENS2_20negative_accumulatorILj10EEELin1ELb0EE4callIciEEbT_mRT0_.exit11.i.i
	movl	%edi, -60(%rbp)
	jmp	LBB8_75
LBB8_79:                                ## %_ZN5boost6spirit2qi6detail13int_extractorILj10ENS2_20positive_accumulatorILj10EEELin1ELb0EE4callIciEEbT_mRT0_.exit.i.i
	movl	%esi, -60(%rbp)
LBB8_75:                                ## %_ZN5boost6spirit2qi6detail11extract_intIiLj10ELj1ELin1ENS2_20negative_accumulatorILj10EEELb0ELb0EE5parseINSt3__111__wrap_iterIPKcEEiEEbRT_RKSD_RT0_.exit.thread
	movq	%rax, (%r15)
LBB8_76:
	xorl	%eax, %eax
	jmp	LBB8_77
LBB8_72:                                ## %._crit_edge.27.i.i
	movq	%rcx, %r8
LBB8_73:                                ## %._crit_edge.30.i.i
	movl	%esi, -60(%rbp)
	jmp	LBB8_40
LBB8_38:                                ## %._crit_edge.37.i.i
	movq	%rcx, %r8
LBB8_39:                                ## %._crit_edge.40.i.i
	movl	%edi, -60(%rbp)
LBB8_40:                                ## %_ZNK5boost6spirit2qi6detail20alternative_functionINSt3__111__wrap_iterIPKcEENS0_7contextINS_6fusion4consIRNS_7variantIiJdEEENSA_4nil_EEENSA_6vectorIJEEEEENS0_11unused_typeESD_EclINS1_14any_int_parserIiLj10ELj1ELin1EEEEEbRKT_.exit.i.i.i.i
	movq	%r8, (%r15)
LBB8_41:                                ## %_ZNK5boost6spirit2qi6detail20alternative_functionINSt3__111__wrap_iterIPKcEENS0_7contextINS_6fusion4consIRNS_7variantIiJdEEENSA_4nil_EEENSA_6vectorIJEEEEENS0_11unused_typeESD_EclINS1_14any_int_parserIiLj10ELj1ELin1EEEEEbRKT_.exit.i.i.i.i
	leaq	-60(%rbp), %rdi
	movq	%r14, %rsi
	callq	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEivE4callIiEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	movb	$1, %al
LBB8_77:                                ## %_ZN5boost6fusion3anyINS0_4consINS_6spirit2qi15any_real_parserIdNS4_20strict_real_policiesIdEEEENS2_INS4_14any_int_parserIiLj10ELj1ELin1EEENS0_4nil_EEEEENS4_6detail20alternative_functionINSt3__111__wrap_iterIPKcEENS3_7contextINS2_IRNS_7variantIiJdEEESB_EENS0_6vectorIJEEEEENS3_11unused_typeESN_EEEEbRKT_T0_.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__literal8,8byte_literals
	.align	3
LCPI9_0:
	.quad	9221120237041090560     ## double NaN
LCPI9_3:
	.quad	4644108417307246592     ## double 307
LCPI9_4:
	.quad	4607182418800017408     ## double 1
	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI9_1:
	.long	1127219200              ## 0x43300000
	.long	1160773632              ## 0x45300000
	.long	0                       ## 0x0
	.long	0                       ## 0x0
LCPI9_2:
	.quad	4841369599423283200     ## double 4.503600e+15
	.quad	4985484787499139072     ## double 1.934281e+25
LCPI9_5:
	.quad	-9223372036854775808    ## 0x8000000000000000
	.quad	-9223372036854775808    ## 0x8000000000000000
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6spirit2qi6detail9real_implIdNS1_20strict_real_policiesIdEEE5parseINSt3__111__wrap_iterIPKcEEdEEbRT_RKSD_RT0_RKS5_
	.weak_def_can_be_hidden	__ZN5boost6spirit2qi6detail9real_implIdNS1_20strict_real_policiesIdEEE5parseINSt3__111__wrap_iterIPKcEEdEEbRT_RKSD_RT0_RKS5_
	.align	4, 0x90
__ZN5boost6spirit2qi6detail9real_implIdNS1_20strict_real_policiesIdEEE5parseINSt3__111__wrap_iterIPKcEEdEEbRT_RKSD_RT0_RKS5_: ## @_ZN5boost6spirit2qi6detail9real_implIdNS1_20strict_real_policiesIdEEE5parseINSt3__111__wrap_iterIPKcEEdEEbRT_RKSD_RT0_RKS5_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp246:
	.cfi_def_cfa_offset 16
Ltmp247:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp248:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
Ltmp249:
	.cfi_offset %rbx, -56
Ltmp250:
	.cfi_offset %r12, -48
Ltmp251:
	.cfi_offset %r13, -40
Ltmp252:
	.cfi_offset %r14, -32
Ltmp253:
	.cfi_offset %r15, -24
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %r13
	movq	(%r12), %r10
	cmpq	%r10, %r13
	je	LBB9_1
## BB#2:
	movzbl	(%r13), %eax
	cmpl	$45, %eax
	sete	%r14b
	je	LBB9_5
## BB#3:
	movzbl	%al, %eax
	cmpl	$43, %eax
	jne	LBB9_4
LBB9_5:
	movq	%rdx, -56(%rbp)         ## 8-byte Spill
	leaq	1(%r13), %rcx
	movq	%rcx, (%rbx)
	movq	(%r12), %r10
	jmp	LBB9_6
LBB9_1:
	xorl	%eax, %eax
	jmp	LBB9_233
LBB9_4:
	movq	%rdx, -56(%rbp)         ## 8-byte Spill
	xorl	%r14d, %r14d
	movq	%r13, %rcx
LBB9_6:                                 ## %_ZN5boost6spirit2qi13real_policiesIdE10parse_signINSt3__111__wrap_iterIPKcEEEEbRT_RKSA_.exit
	xorl	%r15d, %r15d
	movq	%rcx, %rdx
	subq	%r10, %rdx
	je	LBB9_26
## BB#7:                                ## %.lr.ph39.i.i.i.preheader
	xorl	%esi, %esi
	movq	%rcx, %rax
	.align	4, 0x90
LBB9_8:                                 ## %.lr.ph39.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movsbq	(%rcx,%rsi), %r15
	cmpq	$48, %r15
	jne	LBB9_10
## BB#9:                                ##   in Loop: Header=BB9_8 Depth=1
	leaq	1(%rcx,%rsi), %rax
	incq	%rsi
	movq	%rdx, %rdi
	addq	%rsi, %rdi
	jne	LBB9_8
	jmp	LBB9_11
LBB9_10:                                ## %.critedge.i.i.i
	leaq	(%rcx,%rsi), %r11
	cmpq	%r10, %r11
	je	LBB9_11
## BB#13:
	movb	%r15b, %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$10, %edx
	jae	LBB9_11
## BB#14:
	addq	$-48, %r15
	leaq	1(%r11), %rax
	cmpq	%r10, %rax
	je	LBB9_50
## BB#15:                               ## %.lr.ph.i.i.i.preheader
	movq	%r10, %rdx
	subq	%r11, %rdx
	leaq	-1(%rdx), %rdi
	movq	%rdi, -104(%rbp)        ## 8-byte Spill
	leaq	-3(%rdx), %rdi
	movq	%rdi, -96(%rbp)         ## 8-byte Spill
	leaq	-2(%rdx), %rdx
	movq	%rdx, -88(%rbp)         ## 8-byte Spill
	xorl	%r9d, %r9d
LBB9_16:                                ## %.lr.ph.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movsbq	1(%r11,%r9), %rdi
	movb	%dil, %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_50
## BB#17:                               ##   in Loop: Header=BB9_16 Depth=1
	movq	%rbx, -80(%rbp)         ## 8-byte Spill
	movl	%r14d, %ebx
	leaq	(%rsi,%r9), %r14
	cmpq	$17, %r14
	ja	LBB9_28
## BB#18:                               ##   in Loop: Header=BB9_16 Depth=1
	leaq	(%r15,%r15,4), %rax
	leaq	-48(%rdi,%rax,2), %r15
	jmp	LBB9_32
LBB9_28:                                ##   in Loop: Header=BB9_16 Depth=1
	movabsq	$1844674407370955161, %rax ## imm = 0x1999999999999999
	cmpq	%rax, %r15
	ja	LBB9_29
## BB#30:                               ##   in Loop: Header=BB9_16 Depth=1
	addq	%r15, %r15
	leaq	(%r15,%r15,4), %r15
	movl	$47, %eax
	subq	%rdi, %rax
	cmpq	%rax, %r15
	ja	LBB9_29
## BB#31:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addImcEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.5.i.i.i
                                        ##   in Loop: Header=BB9_16 Depth=1
	leaq	-48(%r15,%rdi), %r15
LBB9_32:                                ##   in Loop: Header=BB9_16 Depth=1
	cmpq	%r9, -88(%rbp)          ## 8-byte Folded Reload
	je	LBB9_48
## BB#33:                               ##   in Loop: Header=BB9_16 Depth=1
	leaq	2(%r11,%r9), %rax
	movsbq	(%rax), %r8
	movb	%r8b, %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_49
## BB#34:                               ##   in Loop: Header=BB9_16 Depth=1
	incq	%r14
	cmpq	$17, %r14
	ja	LBB9_36
## BB#35:                               ##   in Loop: Header=BB9_16 Depth=1
	leaq	(%r15,%r15,4), %rax
	leaq	-48(%r8,%rax,2), %r15
	jmp	LBB9_39
LBB9_36:                                ##   in Loop: Header=BB9_16 Depth=1
	movabsq	$1844674407370955161, %rax ## imm = 0x1999999999999999
	cmpq	%rax, %r15
	ja	LBB9_29
## BB#37:                               ##   in Loop: Header=BB9_16 Depth=1
	addq	%r15, %r15
	leaq	(%r15,%r15,4), %r15
	movl	$47, %eax
	subq	%r8, %rax
	cmpq	%rax, %r15
	ja	LBB9_29
## BB#38:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addImcEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.1.i.i.i
                                        ##   in Loop: Header=BB9_16 Depth=1
	leaq	-48(%r15,%r8), %r15
LBB9_39:                                ##   in Loop: Header=BB9_16 Depth=1
	cmpq	%r9, -96(%rbp)          ## 8-byte Folded Reload
	je	LBB9_48
## BB#40:                               ##   in Loop: Header=BB9_16 Depth=1
	leaq	3(%r11,%r9), %rax
	movsbq	(%rax), %rdi
	movb	%dil, %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_49
## BB#41:                               ##   in Loop: Header=BB9_16 Depth=1
	incq	%r14
	cmpq	$17, %r14
	ja	LBB9_43
## BB#42:                               ##   in Loop: Header=BB9_16 Depth=1
	leaq	(%r15,%r15,4), %rax
	leaq	-48(%rdi,%rax,2), %r15
	movl	%ebx, %r14d
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	jmp	LBB9_47
LBB9_43:                                ##   in Loop: Header=BB9_16 Depth=1
	movabsq	$1844674407370955161, %rax ## imm = 0x1999999999999999
	cmpq	%rax, %r15
	movl	%ebx, %r14d
	ja	LBB9_44
## BB#45:                               ##   in Loop: Header=BB9_16 Depth=1
	addq	%r15, %r15
	leaq	(%r15,%r15,4), %r15
	movl	$47, %eax
	subq	%rdi, %rax
	cmpq	%rax, %r15
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	ja	LBB9_19
## BB#46:                               ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addImcEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.i.i.i
                                        ##   in Loop: Header=BB9_16 Depth=1
	leaq	-48(%r15,%rdi), %r15
LBB9_47:                                ##   in Loop: Header=BB9_16 Depth=1
	leaq	4(%r11,%r9), %rax
	addq	$3, %r9
	cmpq	%r9, -104(%rbp)         ## 8-byte Folded Reload
	jne	LBB9_16
	jmp	LBB9_50
LBB9_11:                                ## %.critedge.thread.i.i.i
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	LBB9_19
## BB#12:
	movq	%rax, (%rbx)
	movb	$1, %r8b
	xorl	%r15d, %r15d
	jmp	LBB9_61
LBB9_29:
	movl	%ebx, %r14d
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	jmp	LBB9_19
LBB9_48:                                ## %._crit_edge.27.i.i.i
	movq	%r10, %rax
LBB9_49:                                ## %._crit_edge.30.i.i.i
	movl	%ebx, %r14d
	movq	-80(%rbp), %rbx         ## 8-byte Reload
LBB9_50:                                ## %._crit_edge.30.i.i.i
	movq	%rax, (%rbx)
	movb	$1, %r8b
	jmp	LBB9_61
LBB9_44:
	movq	-80(%rbp), %rbx         ## 8-byte Reload
LBB9_19:                                ## %_ZN5boost6spirit2qi12extract_uintImLj10ELj1ELin1ELb0ELb0EE4callINSt3__111__wrap_iterIPKcEEEEbRT_RKSA_Rm.exit
	movq	%rcx, (%rbx)
	movq	(%r12), %rsi
	cmpq	%rsi, %rcx
	je	LBB9_26
## BB#20:
	movb	(%rcx), %al
	movb	%al, %dl
	orb	$32, %dl
	movzbl	%dl, %edx
	cmpl	$110, %edx
	jne	LBB9_26
## BB#21:
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB9_26
## BB#22:
	leaq	1(%rcx), %rdx
	cmpq	%rsi, %rdx
	je	LBB9_26
## BB#23:
	movb	(%rdx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$65, %eax
	jne	LBB9_26
## BB#24:
	leaq	2(%rcx), %rdx
	cmpq	%rsi, %rdx
	je	LBB9_26
## BB#25:
	movb	(%rdx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB9_26
## BB#51:
	addq	$3, %rcx
	movq	%rcx, (%rbx)
	movq	(%r12), %rax
	cmpq	%rax, %rcx
	je	LBB9_56
## BB#52:
	movzbl	(%rcx), %edx
	cmpl	$40, %edx
	jne	LBB9_56
LBB9_53:                                ## %.preheader.i.6
                                        ## =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	leaq	1(%rdx), %rcx
	cmpq	%rax, %rcx
	je	LBB9_26
## BB#54:                               ##   in Loop: Header=BB9_53 Depth=1
	movzbl	1(%rdx), %esi
	cmpl	$41, %esi
	jne	LBB9_53
## BB#55:                               ## %.critedge2.i.8
	addq	$2, %rdx
	movq	%rdx, (%rbx)
LBB9_56:                                ## %_ZN5boost6spirit2qi14ureal_policiesIdE9parse_nanINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_.exit10
	movabsq	$9221120237041090560, %rax ## imm = 0x7FF8000000000000
	movq	%rax, -48(%rbp)
	movsd	LCPI9_0(%rip), %xmm0    ## xmm0 = mem[0],zero
	jmp	LBB9_57
LBB9_26:                                ## %.loopexit68
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	__ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_
	testb	%al, %al
	je	LBB9_60
## BB#27:                               ## %.loopexit68._crit_edge
	movsd	-48(%rbp), %xmm0        ## xmm0 = mem[0],zero
LBB9_57:
	testb	%r14b, %r14b
	movq	-56(%rbp), %rax         ## 8-byte Reload
	je	LBB9_59
## BB#58:
	xorpd	LCPI9_5(%rip), %xmm0
LBB9_59:
	movsd	%xmm0, (%rax)
	jmp	LBB9_232
LBB9_60:                                ## %.loopexit68._ZN5boost6spirit2qi12extract_uintImLj10ELj1ELin1ELb0ELb0EE4callINSt3__111__wrap_iterIPKcEEEEbRT_RKSA_Rm.exit.thread_crit_edge
	movq	(%rbx), %rax
	xorl	%r8d, %r8d
LBB9_61:                                ## %_ZN5boost6spirit2qi12extract_uintImLj10ELj1ELin1ELb0ELb0EE4callINSt3__111__wrap_iterIPKcEEEEbRT_RKSA_Rm.exit.thread
	movq	%rax, %rdi
	movq	(%r12), %rcx
	cmpq	%rcx, %rdi
	je	LBB9_114
## BB#62:
	movzbl	(%rdi), %edx
	cmpl	$46, %edx
	jne	LBB9_114
## BB#63:
	leaq	1(%rdi), %r11
	movq	%r11, (%rbx)
	movq	(%r12), %rax
	cmpq	%rax, %r11
	je	LBB9_108
## BB#64:                               ## %.lr.ph.i.i.i.12.preheader
	movq	%rbx, %r9
	movl	%r14d, -88(%rbp)        ## 4-byte Spill
	movq	%rax, -104(%rbp)        ## 8-byte Spill
	subq	%rdi, %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, -96(%rbp)         ## 8-byte Spill
	leaq	-3(%rax), %rcx
	movq	%rcx, -80(%rbp)         ## 8-byte Spill
	leaq	-2(%rax), %r14
	xorl	%esi, %esi
	movq	%r15, %rcx
	movq	%r11, %rax
LBB9_65:                                ## %.lr.ph.i.i.i.12
                                        ## =>This Inner Loop Header: Depth=1
	movsbq	1(%rdi,%rsi), %rdx
	movb	%dl, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_84
## BB#66:                               ##   in Loop: Header=BB9_65 Depth=1
	movabsq	$1844674407370955161, %rbx ## imm = 0x1999999999999999
	cmpq	%rbx, %rcx
	ja	LBB9_68
## BB#67:                               ##   in Loop: Header=BB9_65 Depth=1
	addq	%rcx, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movl	$47, %ebx
	subq	%rdx, %rbx
	cmpq	%rbx, %rcx
	ja	LBB9_68
## BB#69:                               ##   in Loop: Header=BB9_65 Depth=1
	leaq	-48(%rcx,%rdx), %rcx
	cmpq	%rsi, %r14
	je	LBB9_82
## BB#70:                               ##   in Loop: Header=BB9_65 Depth=1
	leaq	2(%rdi,%rsi), %rax
	movsbq	(%rax), %r10
	movb	%r10b, %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_81
## BB#71:                               ##   in Loop: Header=BB9_65 Depth=1
	movabsq	$1844674407370955161, %rdx ## imm = 0x1999999999999999
	cmpq	%rdx, %rcx
	ja	LBB9_234
## BB#72:                               ##   in Loop: Header=BB9_65 Depth=1
	addq	%rcx, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movl	$47, %edx
	subq	%r10, %rdx
	cmpq	%rdx, %rcx
	movq	%r9, %rbx
	ja	LBB9_73
## BB#74:                               ##   in Loop: Header=BB9_65 Depth=1
	movq	%rbx, %r9
	leaq	-48(%rcx,%r10), %rcx
	cmpq	%rsi, -80(%rbp)         ## 8-byte Folded Reload
	je	LBB9_80
## BB#75:                               ##   in Loop: Header=BB9_65 Depth=1
	leaq	3(%rdi,%rsi), %rax
	movsbq	(%rax), %rdx
	movb	%dl, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_79
## BB#76:                               ##   in Loop: Header=BB9_65 Depth=1
	movabsq	$1844674407370955161, %rbx ## imm = 0x1999999999999999
	cmpq	%rbx, %rcx
	ja	LBB9_68
## BB#77:                               ##   in Loop: Header=BB9_65 Depth=1
	addq	%rcx, %rcx
	leaq	(%rcx,%rcx,4), %rcx
	movl	$47, %ebx
	subq	%rdx, %rbx
	cmpq	%rbx, %rcx
	ja	LBB9_68
## BB#78:                               ##   in Loop: Header=BB9_65 Depth=1
	leaq	-48(%rcx,%rdx), %rcx
	leaq	4(%rdi,%rsi), %rax
	addq	$3, %rsi
	cmpq	%rsi, -96(%rbp)         ## 8-byte Folded Reload
	jne	LBB9_65
	jmp	LBB9_84
LBB9_114:
	testb	%r8b, %r8b
	je	LBB9_235
## BB#115:
	cmpq	%rcx, %rax
	je	LBB9_235
## BB#116:
	movb	(%rax), %cl
	orb	$32, %cl
	movzbl	%cl, %ecx
	cmpl	$101, %ecx
	jne	LBB9_235
## BB#117:
	xorl	%r10d, %r10d
	jmp	LBB9_118
LBB9_68:
	movq	%r9, %rbx
	jmp	LBB9_73
LBB9_82:                                ## %._crit_edge.28.i.i.i
	incq	%rsi
	jmp	LBB9_83
LBB9_81:                                ## %._crit_edge.24.i.i.i.17
	incq	%rsi
	jmp	LBB9_84
LBB9_234:                               ## %split736
	movq	%r9, %rbx
LBB9_73:
	movq	%rax, (%rbx)
	movl	-88(%rbp), %r14d        ## 4-byte Reload
	jmp	LBB9_86
LBB9_80:                                ## %._crit_edge.20.i.i.i
	addq	$2, %rsi
LBB9_83:                                ## %._crit_edge.32.i.i.i
	movq	-104(%rbp), %rax        ## 8-byte Reload
	jmp	LBB9_84
LBB9_79:                                ## %._crit_edge.i.i.i.16
	addq	$2, %rsi
LBB9_84:                                ## %._crit_edge.32.i.i.i
	testq	%rsi, %rsi
	movl	-88(%rbp), %r14d        ## 4-byte Reload
	movq	%r9, %rbx
	je	LBB9_107
## BB#85:
	movq	%rax, (%rbx)
LBB9_86:
	movl	%eax, %r10d
	subl	%r11d, %r10d
	movq	(%r12), %r9
	movq	%rax, %rdi
	subq	%r9, %rdi
	je	LBB9_87
## BB#88:                               ## %.lr.ph21.i.i.i.i.i.preheader
	movq	%rbx, %r8
	xorl	%ebx, %ebx
	movq	%rax, %r11
	.align	4, 0x90
LBB9_89:                                ## %.lr.ph21.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	(%rax,%rbx), %edx
	cmpl	$48, %edx
	jne	LBB9_91
## BB#90:                               ##   in Loop: Header=BB9_89 Depth=1
	leaq	1(%rax,%rbx), %r11
	incq	%rbx
	movq	%rdi, %rdx
	addq	%rbx, %rdx
	jne	LBB9_89
	jmp	LBB9_92
LBB9_87:
	movq	%rax, %r11
	jmp	LBB9_110
LBB9_91:                                ## %.critedge.i.i.i.i.i
	leaq	(%rax,%rbx), %rdi
	cmpq	%r9, %rdi
	je	LBB9_92
## BB#94:
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_92
## BB#95:
	leaq	1(%rdi), %r11
	cmpq	%r9, %r11
	movq	-56(%rbp), %r15         ## 8-byte Reload
	movq	%r8, %rbx
	je	LBB9_106
## BB#96:                               ## %.lr.ph.i.i.i.i.i.preheader
	movq	%r9, %r8
	negq	%r8
	addq	$4, %rdi
LBB9_99:                                ## %.lr.ph.i.i.i.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movb	-3(%rdi), %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$9, %edx
	ja	LBB9_106
## BB#100:                              ##   in Loop: Header=BB9_99 Depth=1
	leaq	(%rdi,%r8), %rdx
	cmpq	$2, %rdx
	je	LBB9_105
## BB#101:                              ##   in Loop: Header=BB9_99 Depth=1
	movq	%rbx, %rsi
	movq	%r15, %rax
	leaq	-2(%rdi), %r11
	movb	(%r11), %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_104
## BB#102:                              ##   in Loop: Header=BB9_99 Depth=1
	cmpq	$1, %rdx
	movq	%rax, %r15
	movq	%rsi, %rbx
	je	LBB9_105
## BB#103:                              ##   in Loop: Header=BB9_99 Depth=1
	leaq	-1(%rdi), %r11
	movb	(%r11), %dl
	addb	$-48, %dl
	movzbl	%dl, %edx
	cmpl	$10, %edx
	jae	LBB9_106
## BB#98:                               ##   in Loop: Header=BB9_99 Depth=1
	movq	%rdi, %r11
	leaq	3(%rdi,%r8), %rdx
	addq	$3, %rdi
	cmpq	$3, %rdx
	jne	LBB9_99
	jmp	LBB9_106
LBB9_92:                                ## %.critedge.thread.i.i.i.i.i
	testq	%rbx, %rbx
	je	LBB9_97
## BB#93:
	movq	%r8, %rbx
	movq	%r11, (%rbx)
	movq	%r11, %rax
	jmp	LBB9_110
LBB9_107:                               ## %_ZN5boost6spirit2qi6detail11extract_intImLj10ELj1ELin1ENS2_20positive_accumulatorILj10EEELb1ELb1EE5parseINSt3__111__wrap_iterIPKcEEmEEbRT_RKSD_RT0_.exit.i
	movq	%r11, (%rbx)
LBB9_108:                               ## %_ZN5boost6spirit2qi14ureal_policiesIdE12parse_frac_nINSt3__111__wrap_iterIPKcEEmEEbRT_RKSA_RT0_Ri.exit
	testb	%r8b, %r8b
	je	LBB9_235
## BB#109:
	xorl	%r10d, %r10d
	movq	%r11, %rax
	movq	%r15, %rcx
	jmp	LBB9_110
LBB9_235:
	movq	%r13, (%rbx)
	xorl	%eax, %eax
	jmp	LBB9_233
LBB9_97:                                ## %_ZN5boost6spirit2qi6detail11extract_intINS0_11unused_typeELj10ELj1ELin1ENS2_20positive_accumulatorILj10EEELb0ELb0EE5parseINSt3__111__wrap_iterIPKcEEEEbRT_RKSE_S4_.exit.i.i.i
	movq	%r8, %rbx
	movq	%rax, (%rbx)
	movq	%rax, %r11
LBB9_110:                               ## %_ZN5boost6spirit2qi14ureal_policiesIdE12parse_frac_nINSt3__111__wrap_iterIPKcEEmEEbRT_RKSA_RT0_Ri.exit.thread
	movq	-56(%rbp), %r15         ## 8-byte Reload
LBB9_111:                               ## %_ZN5boost6spirit2qi14ureal_policiesIdE12parse_frac_nINSt3__111__wrap_iterIPKcEEmEEbRT_RKSA_RT0_Ri.exit.thread
	movq	(%r12), %rsi
	cmpq	%rsi, %rax
	je	LBB9_202
## BB#112:
	movb	(%rax), %dl
	orb	$32, %dl
	movzbl	%dl, %edx
	cmpl	$101, %edx
	jne	LBB9_202
## BB#113:
	movq	%r15, -56(%rbp)         ## 8-byte Spill
	movq	%rcx, %r15
LBB9_118:                               ## %.critedge61
	leaq	1(%rax), %r13
	movq	%r13, (%rbx)
	movq	(%r12), %r9
	cmpq	%r9, %r13
	je	LBB9_134
## BB#119:
	movzbl	(%r13), %ecx
	cmpl	$45, %ecx
	je	LBB9_121
## BB#120:
	movzbl	%cl, %edx
	cmpl	$43, %edx
	movq	%r13, %rsi
	jne	LBB9_160
LBB9_121:                               ## %_ZN5boost6spirit2qi12extract_signINSt3__111__wrap_iterIPKcEEEEbRT_RKS8_.exit.i.i
	leaq	2(%rax), %rsi
	movq	%rsi, (%rbx)
	movq	(%r12), %r9
	movzbl	%cl, %ecx
	cmpl	$45, %ecx
	jne	LBB9_160
## BB#122:
	movl	%r10d, -96(%rbp)        ## 4-byte Spill
	xorl	%edi, %edi
	cmpq	%r9, %rsi
	je	LBB9_123
## BB#124:                              ## %.lr.ph49.i.i.preheader
	movl	$2, %edx
	subq	%r9, %rdx
	addq	%rax, %rdx
	xorl	%edi, %edi
	movq	%rsi, %rcx
	.align	4, 0x90
LBB9_125:                               ## %.lr.ph49.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	2(%rax,%rdi), %esi
	cmpl	$48, %esi
	jne	LBB9_126
## BB#236:                              ##   in Loop: Header=BB9_125 Depth=1
	leaq	3(%rax,%rdi), %rcx
	incq	%rdi
	movq	%rdx, %rsi
	addq	%rdi, %rsi
	jne	LBB9_125
	jmp	LBB9_129
LBB9_202:
	testl	%r10d, %r10d
	je	LBB9_211
## BB#203:
	testl	%r10d, %r10d
	jle	LBB9_204
## BB#207:
	cmpl	$308, %r10d             ## imm = 0x134
	jl	LBB9_210
## BB#208:
	movabsq	$-3689348814741910323, %rdx ## imm = 0xCCCCCCCCCCCCCCCD
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$2, %rdx
	movabsq	$4611686018427387902, %rax ## imm = 0x3FFFFFFFFFFFFFFE
	andq	%rdx, %rax
	leaq	(%rax,%rax,4), %rax
	subq	%rax, %rcx
	movd	%rax, %xmm0
	movdqa	LCPI9_1(%rip), %xmm1    ## xmm1 = [1127219200,1160773632,0,0]
	punpckldq	%xmm1, %xmm0    ## xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movapd	LCPI9_2(%rip), %xmm2    ## xmm2 = [4.503600e+15,1.934281e+25]
	subpd	%xmm2, %xmm0
	haddpd	%xmm0, %xmm0
	movd	%rcx, %xmm3
	punpckldq	%xmm1, %xmm3    ## xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	subpd	%xmm2, %xmm3
	haddpd	%xmm3, %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, -80(%rbp)        ## 16-byte Spill
	movsd	LCPI9_3(%rip), %xmm0    ## xmm0 = mem[0],zero
	movl	%r10d, %ebx
	callq	___exp10
	movl	%ebx, %eax
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm0, -48(%rbp)
	addl	$-307, %eax             ## imm = 0xFFFFFFFFFFFFFECD
	cmpl	$307, %eax              ## imm = 0x133
	jg	LBB9_229
## BB#209:                              ## %.critedge5.i
	movl	%eax, %eax
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	___exp10
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, -80(%rbp)        ## 16-byte Spill
	movapd	-80(%rbp), %xmm0        ## 16-byte Reload
	movsd	%xmm0, -48(%rbp)
	jmp	LBB9_229
LBB9_160:                               ## %_ZN5boost6spirit2qi12extract_signINSt3__111__wrap_iterIPKcEEEEbRT_RKS8_.exit.i.i._crit_edge
	movl	%r10d, -96(%rbp)        ## 4-byte Spill
	movl	%r14d, -88(%rbp)        ## 4-byte Spill
	xorl	%edi, %edi
	cmpq	%r9, %rsi
	je	LBB9_133
	.align	4, 0x90
LBB9_161:                               ## %.lr.ph39.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ecx
	cmpl	$48, %ecx
	jne	LBB9_163
## BB#162:                              ##   in Loop: Header=BB9_161 Depth=1
	incq	%rsi
	incq	%rdi
	cmpq	%rsi, %r9
	jne	LBB9_161
	jmp	LBB9_164
LBB9_211:
	cmpq	$1, %rcx
	jne	LBB9_228
## BB#212:
	cmpq	%rsi, %r11
	je	LBB9_225
## BB#213:
	movb	(%r11), %al
	movb	%al, %cl
	orb	$32, %cl
	movzbl	%cl, %ecx
	cmpl	$110, %ecx
	jne	LBB9_225
## BB#214:
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB9_225
## BB#215:
	leaq	1(%r11), %rax
	cmpq	%rsi, %rax
	je	LBB9_225
## BB#216:
	movb	(%rax), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$65, %eax
	jne	LBB9_225
## BB#217:
	leaq	2(%r11), %rax
	cmpq	%rsi, %rax
	je	LBB9_225
## BB#218:
	movb	(%rax), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB9_225
## BB#219:
	addq	$3, %r11
	movq	%r11, (%rbx)
	movq	(%r12), %rax
	cmpq	%rax, %r11
	je	LBB9_224
## BB#220:
	movzbl	(%r11), %ecx
	cmpl	$40, %ecx
	jne	LBB9_224
LBB9_221:                               ## %.preheader.i
                                        ## =>This Inner Loop Header: Depth=1
	movq	%r11, %rcx
	leaq	1(%rcx), %r11
	cmpq	%rax, %r11
	je	LBB9_225
## BB#222:                              ##   in Loop: Header=BB9_221 Depth=1
	movzbl	1(%rcx), %edx
	cmpl	$41, %edx
	jne	LBB9_221
## BB#223:                              ## %.critedge2.i
	addq	$2, %rcx
	movq	%rcx, (%rbx)
LBB9_224:                               ## %_ZN5boost6spirit2qi14ureal_policiesIdE9parse_nanINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_.exit
	movabsq	$9221120237041090560, %rax ## imm = 0x7FF8000000000000
	movq	%rax, -48(%rbp)
	movsd	LCPI9_0(%rip), %xmm0    ## xmm0 = mem[0],zero
	jmp	LBB9_229
LBB9_204:
	cmpl	$-309, %r10d            ## imm = 0xFFFFFFFFFFFFFECB
	jg	LBB9_206
## BB#205:                              ## %..critedge_crit_edge
	movsd	-48(%rbp), %xmm0        ## xmm0 = mem[0],zero
	jmp	LBB9_229
LBB9_163:                               ## %.critedge.i.i.24
	cmpq	%r9, %rsi
	je	LBB9_164
## BB#166:
	movsbl	(%rsi), %r14d
	movb	%r14b, %cl
	addb	$-48, %cl
	movzbl	%cl, %ecx
	cmpl	$10, %ecx
	jae	LBB9_164
## BB#167:
	movq	%rbx, %r10
	addl	$-48, %r14d
	leaq	1(%rsi), %rcx
	cmpq	%r9, %rcx
	je	LBB9_191
## BB#168:                              ## %.lr.ph.i.i.preheader
	movq	%r9, %rbx
	subq	%rsi, %rbx
	leaq	-1(%rbx), %rdx
	movq	%rdx, -112(%rbp)        ## 8-byte Spill
	leaq	-3(%rbx), %rdx
	movq	%rdx, -104(%rbp)        ## 8-byte Spill
	leaq	-2(%rbx), %rdx
	movq	%rdx, -80(%rbp)         ## 8-byte Spill
	xorl	%r11d, %r11d
LBB9_169:                               ## %.lr.ph.i.i
                                        ## =>This Inner Loop Header: Depth=1
	movsbl	1(%rsi,%r11), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_191
## BB#170:                              ##   in Loop: Header=BB9_169 Depth=1
	leaq	(%rdi,%r11), %r12
	cmpq	$7, %r12
	ja	LBB9_172
## BB#171:                              ##   in Loop: Header=BB9_169 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	-48(%r8,%rcx,2), %r14d
	jmp	LBB9_175
LBB9_172:                               ##   in Loop: Header=BB9_169 Depth=1
	cmpl	$214748364, %r14d       ## imm = 0xCCCCCCC
	movq	%r10, %rbx
	jg	LBB9_133
## BB#173:                              ##   in Loop: Header=BB9_169 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %ecx
	movq	%rbx, %rdx
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r8d, %ebx
	cmpl	%ebx, %ecx
	movq	%rdx, %rbx
	jg	LBB9_133
## BB#174:                              ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.5.i.i
                                        ##   in Loop: Header=BB9_169 Depth=1
	movq	%rbx, %r10
	leal	-48(%rcx,%r8), %r14d
LBB9_175:                               ##   in Loop: Header=BB9_169 Depth=1
	cmpq	%r11, -80(%rbp)         ## 8-byte Folded Reload
	je	LBB9_190
## BB#176:                              ##   in Loop: Header=BB9_169 Depth=1
	leaq	2(%rsi,%r11), %rcx
	movsbl	(%rcx), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_191
## BB#177:                              ##   in Loop: Header=BB9_169 Depth=1
	incq	%r12
	cmpq	$7, %r12
	ja	LBB9_179
## BB#178:                              ##   in Loop: Header=BB9_169 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	-48(%r8,%rcx,2), %r14d
	jmp	LBB9_182
LBB9_179:                               ##   in Loop: Header=BB9_169 Depth=1
	cmpl	$214748364, %r14d       ## imm = 0xCCCCCCC
	movq	%r10, %rbx
	jg	LBB9_133
## BB#180:                              ##   in Loop: Header=BB9_169 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %ecx
	movq	%rbx, %rdx
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r8d, %ebx
	cmpl	%ebx, %ecx
	movq	%rdx, %rbx
	jg	LBB9_133
## BB#181:                              ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.1.i.i
                                        ##   in Loop: Header=BB9_169 Depth=1
	movq	%rbx, %r10
	leal	-48(%rcx,%r8), %r14d
LBB9_182:                               ##   in Loop: Header=BB9_169 Depth=1
	cmpq	%r11, -104(%rbp)        ## 8-byte Folded Reload
	je	LBB9_190
## BB#183:                              ##   in Loop: Header=BB9_169 Depth=1
	leaq	3(%rsi,%r11), %rcx
	movsbl	(%rcx), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_191
## BB#184:                              ##   in Loop: Header=BB9_169 Depth=1
	incq	%r12
	cmpq	$7, %r12
	ja	LBB9_186
## BB#185:                              ##   in Loop: Header=BB9_169 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	-48(%r8,%rcx,2), %r14d
	jmp	LBB9_189
LBB9_186:                               ##   in Loop: Header=BB9_169 Depth=1
	cmpl	$214748364, %r14d       ## imm = 0xCCCCCCC
	movq	%r10, %rbx
	jg	LBB9_133
## BB#187:                              ##   in Loop: Header=BB9_169 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %ecx
	movq	%rbx, %rdx
	movl	$-2147483601, %ebx      ## imm = 0xFFFFFFFF8000002F
	subl	%r8d, %ebx
	cmpl	%ebx, %ecx
	movq	%rdx, %rbx
	jg	LBB9_133
## BB#188:                              ## %_ZN5boost6spirit2qi6detail20positive_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.i.i
                                        ##   in Loop: Header=BB9_169 Depth=1
	movq	%rbx, %r10
	leal	-48(%rcx,%r8), %r14d
LBB9_189:                               ##   in Loop: Header=BB9_169 Depth=1
	leaq	4(%rsi,%r11), %rcx
	addq	$3, %r11
	cmpq	%r11, -112(%rbp)        ## 8-byte Folded Reload
	jne	LBB9_169
	jmp	LBB9_191
LBB9_164:                               ## %.critedge.thread.i.i
	testq	%rdi, %rdi
	je	LBB9_133
## BB#165:
	movq	%rsi, (%rbx)
	xorl	%r14d, %r14d
	jmp	LBB9_192
LBB9_210:
	movd	%rcx, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	movl	%r10d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	___exp10
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm0, -48(%rbp)
	jmp	LBB9_229
LBB9_228:
	movd	%rcx, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movlpd	%xmm0, -48(%rbp)
	jmp	LBB9_229
LBB9_225:                               ## %.loopexit
	leaq	-48(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	__ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_
	testb	%al, %al
	je	LBB9_227
## BB#226:                              ## %.loopexit._crit_edge
	movsd	-48(%rbp), %xmm0        ## xmm0 = mem[0],zero
	jmp	LBB9_229
LBB9_123:
	movq	%rbx, -80(%rbp)         ## 8-byte Spill
	movq	-56(%rbp), %r10         ## 8-byte Reload
	movq	%rsi, %rcx
	jmp	LBB9_127
LBB9_206:                               ## %.critedge.i.4
	negl	%r10d
	movd	%rcx, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	movl	%r10d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	___exp10
	mulsd	-80(%rbp), %xmm0        ## 16-byte Folded Reload
	movsd	%xmm0, -48(%rbp)
	jmp	LBB9_229
LBB9_126:
	movq	%rbx, -80(%rbp)         ## 8-byte Spill
	movq	-56(%rbp), %r10         ## 8-byte Reload
	leaq	2(%rax,%rdi), %rsi
LBB9_127:                               ## %.critedge.i.i.29
	cmpq	%r9, %rsi
	je	LBB9_128
## BB#131:
	movl	%r14d, -88(%rbp)        ## 4-byte Spill
	movsbl	(%rsi), %edx
	movb	%dl, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$10, %ebx
	jae	LBB9_132
## BB#135:
	movl	$48, %r14d
	subl	%edx, %r14d
	leaq	1(%rsi), %rcx
	cmpq	%r9, %rcx
	je	LBB9_159
## BB#136:                              ## %.lr.ph.i.i.31.preheader
	movq	%r9, %rbx
	subq	%rsi, %rbx
	leaq	-1(%rbx), %rdx
	movq	%rdx, -112(%rbp)        ## 8-byte Spill
	leaq	-3(%rbx), %rdx
	movq	%rdx, -104(%rbp)        ## 8-byte Spill
	leaq	-2(%rbx), %rdx
	xorl	%r11d, %r11d
LBB9_137:                               ## %.lr.ph.i.i.31
                                        ## =>This Inner Loop Header: Depth=1
	movsbl	1(%rsi,%r11), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_159
## BB#138:                              ##   in Loop: Header=BB9_137 Depth=1
	leaq	(%rdi,%r11), %r12
	cmpq	$7, %r12
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	ja	LBB9_140
## BB#139:                              ##   in Loop: Header=BB9_137 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	48(%rcx,%rcx), %r14d
	subl	%r8d, %r14d
	jmp	LBB9_143
LBB9_140:                               ##   in Loop: Header=BB9_137 Depth=1
	cmpl	$-214748364, %r14d      ## imm = 0xFFFFFFFFF3333334
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	jl	LBB9_133
## BB#141:                              ##   in Loop: Header=BB9_137 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %r14d
	leal	2147483600(%r8), %ecx
	cmpl	%ecx, %r14d
	jl	LBB9_133
## BB#142:                              ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.9.i.i
                                        ##   in Loop: Header=BB9_137 Depth=1
	addl	$48, %r14d
	subl	%r8d, %r14d
	movq	-56(%rbp), %r10         ## 8-byte Reload
LBB9_143:                               ##   in Loop: Header=BB9_137 Depth=1
	cmpq	%r11, %rdx
	je	LBB9_158
## BB#144:                              ##   in Loop: Header=BB9_137 Depth=1
	leaq	2(%rsi,%r11), %rcx
	movsbl	(%rcx), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_159
## BB#145:                              ##   in Loop: Header=BB9_137 Depth=1
	incq	%r12
	cmpq	$7, %r12
	ja	LBB9_147
## BB#146:                              ##   in Loop: Header=BB9_137 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	48(%rcx,%rcx), %r14d
	subl	%r8d, %r14d
	jmp	LBB9_150
LBB9_147:                               ##   in Loop: Header=BB9_137 Depth=1
	cmpl	$-214748364, %r14d      ## imm = 0xFFFFFFFFF3333334
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	jl	LBB9_133
## BB#148:                              ##   in Loop: Header=BB9_137 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %r14d
	leal	2147483600(%r8), %ecx
	cmpl	%ecx, %r14d
	jl	LBB9_133
## BB#149:                              ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.3.i.i
                                        ##   in Loop: Header=BB9_137 Depth=1
	addl	$48, %r14d
	subl	%r8d, %r14d
	movq	-56(%rbp), %r10         ## 8-byte Reload
LBB9_150:                               ##   in Loop: Header=BB9_137 Depth=1
	cmpq	%r11, -104(%rbp)        ## 8-byte Folded Reload
	je	LBB9_158
## BB#151:                              ##   in Loop: Header=BB9_137 Depth=1
	leaq	3(%rsi,%r11), %rcx
	movsbl	(%rcx), %r8d
	movb	%r8b, %bl
	addb	$-48, %bl
	movzbl	%bl, %ebx
	cmpl	$9, %ebx
	ja	LBB9_159
## BB#152:                              ##   in Loop: Header=BB9_137 Depth=1
	incq	%r12
	cmpq	$7, %r12
	ja	LBB9_154
## BB#153:                              ##   in Loop: Header=BB9_137 Depth=1
	leal	(%r14,%r14,4), %ecx
	leal	48(%rcx,%rcx), %r14d
	jmp	LBB9_157
LBB9_154:                               ##   in Loop: Header=BB9_137 Depth=1
	cmpl	$-214748364, %r14d      ## imm = 0xFFFFFFFFF3333334
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	jl	LBB9_133
## BB#155:                              ##   in Loop: Header=BB9_137 Depth=1
	addl	%r14d, %r14d
	leal	(%r14,%r14,4), %r14d
	leal	2147483600(%r8), %ecx
	cmpl	%ecx, %r14d
	jl	LBB9_133
## BB#156:                              ## %_ZN5boost6spirit2qi6detail20negative_accumulatorILj10EE3addIicEEbRT_T0_N4mpl_5bool_ILb1EEE.exit.i.i.i.i
                                        ##   in Loop: Header=BB9_137 Depth=1
	movq	-56(%rbp), %r10         ## 8-byte Reload
	addl	$48, %r14d
LBB9_157:                               ##   in Loop: Header=BB9_137 Depth=1
	subl	%r8d, %r14d
	leaq	4(%rsi,%r11), %rcx
	addq	$3, %r11
	cmpq	%r11, -112(%rbp)        ## 8-byte Folded Reload
	jne	LBB9_137
	jmp	LBB9_159
LBB9_128:
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	movq	-80(%rbp), %rbx         ## 8-byte Reload
LBB9_129:                               ## %.critedge.thread.i.i.30
	movl	%r14d, -88(%rbp)        ## 4-byte Spill
	testq	%rdi, %rdi
	jne	LBB9_130
	jmp	LBB9_133
LBB9_227:
	movabsq	$4607182418800017408, %rax ## imm = 0x3FF0000000000000
	movq	%rax, -48(%rbp)
	movsd	LCPI9_4(%rip), %xmm0    ## xmm0 = mem[0],zero
	jmp	LBB9_229
LBB9_132:
	testq	%rdi, %rdi
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	movq	-80(%rbp), %rbx         ## 8-byte Reload
	je	LBB9_133
LBB9_130:
	movq	%rcx, (%rbx)
	xorl	%r14d, %r14d
	jmp	LBB9_192
LBB9_133:                               ## %_ZN5boost6spirit2qi6detail11extract_intIiLj10ELj1ELin1ENS2_20negative_accumulatorILj10EEELb0ELb0EE5parseINSt3__111__wrap_iterIPKcEEiEEbRT_RKSD_RT0_.exit.thread
	movq	%r13, (%rbx)
	movl	-88(%rbp), %r14d        ## 4-byte Reload
LBB9_134:                               ## %_ZN5boost6spirit2qi14ureal_policiesIdE11parse_exp_nINSt3__111__wrap_iterIPKcEEEEbRT_RKSA_Ri.exit
	movq	%rax, (%rbx)
	movd	%r15, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movlpd	%xmm0, -48(%rbp)
	movq	-56(%rbp), %r15         ## 8-byte Reload
	jmp	LBB9_229
LBB9_105:                               ## %._crit_edge.12.i.i.i.i.i
	movq	%r9, %r11
	jmp	LBB9_106
LBB9_104:                               ## %._crit_edge.10.i.i.i.i.i
	movq	%rax, %r15
	movq	%rsi, %rbx
LBB9_106:                               ## %._crit_edge.14.i.i.i.i.i
	movq	%r11, (%rbx)
	movq	%r11, %rax
	jmp	LBB9_111
LBB9_158:                               ## %._crit_edge.37.i.i
	movq	%r9, %rcx
LBB9_159:                               ## %._crit_edge.40.i.i
	movq	%r10, -56(%rbp)         ## 8-byte Spill
	movq	-80(%rbp), %rax         ## 8-byte Reload
	movq	%rcx, (%rax)
	jmp	LBB9_192
LBB9_190:                               ## %._crit_edge.27.i.i
	movq	%r9, %rcx
LBB9_191:                               ## %._crit_edge.30.i.i
	movq	%rcx, (%r10)
LBB9_192:
	subl	-96(%rbp), %r14d        ## 4-byte Folded Reload
	js	LBB9_196
## BB#193:
	cmpl	$308, %r14d             ## imm = 0x134
	jle	LBB9_195
## BB#194:
	xorl	%eax, %eax
	jmp	LBB9_233
LBB9_196:
	cmpl	$-308, %r14d            ## imm = 0xFFFFFFFFFFFFFECC
	jg	LBB9_201
## BB#197:
	movabsq	$-3689348814741910323, %rcx ## imm = 0xCCCCCCCCCCCCCCCD
	movq	%r15, %rax
	mulq	%rcx
	shrq	$2, %rdx
	movabsq	$4611686018427387902, %rax ## imm = 0x3FFFFFFFFFFFFFFE
	andq	%rdx, %rax
	leaq	(%rax,%rax,4), %rax
	subq	%rax, %r15
	movd	%rax, %xmm0
	movdqa	LCPI9_1(%rip), %xmm1    ## xmm1 = [1127219200,1160773632,0,0]
	punpckldq	%xmm1, %xmm0    ## xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movapd	LCPI9_2(%rip), %xmm2    ## xmm2 = [4.503600e+15,1.934281e+25]
	subpd	%xmm2, %xmm0
	haddpd	%xmm0, %xmm0
	movd	%r15, %xmm3
	punpckldq	%xmm1, %xmm3    ## xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	subpd	%xmm2, %xmm3
	haddpd	%xmm3, %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, -80(%rbp)        ## 16-byte Spill
	movsd	LCPI9_3(%rip), %xmm0    ## xmm0 = mem[0],zero
	callq	___exp10
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm0, -48(%rbp)
	movl	$-307, %eax             ## imm = 0xFFFFFFFFFFFFFECD
	subl	%r14d, %eax
	cmpl	$307, %eax              ## imm = 0x133
	jle	LBB9_199
## BB#198:
	xorl	%eax, %eax
	jmp	LBB9_233
LBB9_195:                               ## %.critedge.i.i
	movd	%r15, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	movl	%r14d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	___exp10
	mulsd	-80(%rbp), %xmm0        ## 16-byte Folded Reload
	jmp	LBB9_200
LBB9_201:
	movd	%r15, %xmm0
	punpckldq	LCPI9_1(%rip), %xmm0 ## xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	subpd	LCPI9_2(%rip), %xmm0
	haddpd	%xmm0, %xmm0
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	negl	%r14d
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
	callq	___exp10
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	LBB9_200
LBB9_199:                               ## %.critedge5.i.i
	movl	%eax, %eax
	movapd	%xmm0, -80(%rbp)        ## 16-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	callq	___exp10
	movapd	-80(%rbp), %xmm1        ## 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, -80(%rbp)        ## 16-byte Spill
	movapd	-80(%rbp), %xmm0        ## 16-byte Reload
LBB9_200:                               ## %.critedge
	movsd	%xmm0, -48(%rbp)
	movl	-88(%rbp), %r14d        ## 4-byte Reload
	movq	-56(%rbp), %r15         ## 8-byte Reload
LBB9_229:                               ## %.critedge
	testb	%r14b, %r14b
	je	LBB9_231
## BB#230:
	xorpd	LCPI9_5(%rip), %xmm0
LBB9_231:                               ## %.critedge
	movsd	%xmm0, (%r15)
LBB9_232:                               ## %_ZN5boost6spirit6traits5scaleIdmEEbiiRT_T0_.exit
	movb	$1, %al
LBB9_233:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_
	.weak_def_can_be_hidden	__ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_
	.align	4, 0x90
__ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_: ## @_ZN5boost6spirit2qi14ureal_policiesIdE9parse_infINSt3__111__wrap_iterIPKcEEdEEbRT_RKSA_RT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp254:
	.cfi_def_cfa_offset 16
Ltmp255:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp256:
	.cfi_def_cfa_register %rbp
	movq	(%rdi), %r9
	movq	(%rsi), %rcx
	cmpq	%rcx, %r9
	je	LBB10_1
## BB#2:
	movb	(%r9), %al
	orb	$32, %al
	movzbl	%al, %eax
	cmpl	$105, %eax
	jne	LBB10_3
## BB#4:
	cmpq	%rcx, %r9
	je	LBB10_5
## BB#6:
	movb	(%r9), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$73, %eax
	jne	LBB10_7
## BB#21:
	leaq	1(%r9), %r8
	cmpq	%rcx, %r8
	je	LBB10_22
## BB#23:
	movb	(%r8), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB10_24
## BB#25:
	leaq	2(%r9), %r8
	cmpq	%rcx, %r8
	je	LBB10_26
## BB#27:
	movb	(%r8), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$70, %eax
	jne	LBB10_28
## BB#8:
	leaq	3(%r9), %rcx
	movq	%rcx, (%rdi)
	movq	(%rsi), %rsi
	cmpq	%rsi, %rcx
	je	LBB10_19
## BB#9:
	movb	(%rcx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$73, %eax
	jne	LBB10_19
## BB#10:
	leaq	4(%r9), %rcx
	cmpq	%rsi, %rcx
	je	LBB10_19
## BB#11:
	movb	(%rcx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$78, %eax
	jne	LBB10_19
## BB#12:
	leaq	5(%r9), %rcx
	cmpq	%rsi, %rcx
	je	LBB10_19
## BB#13:
	movb	(%rcx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$73, %eax
	jne	LBB10_19
## BB#14:
	leaq	6(%r9), %rcx
	cmpq	%rsi, %rcx
	je	LBB10_19
## BB#15:
	movb	(%rcx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$84, %eax
	jne	LBB10_19
## BB#16:
	leaq	7(%r9), %rcx
	cmpq	%rsi, %rcx
	je	LBB10_19
## BB#17:
	movb	(%rcx), %al
	andb	$-33, %al
	movzbl	%al, %eax
	cmpl	$89, %eax
	jne	LBB10_19
## BB#18:                               ## %.critedge.i
	addq	$8, %r9
	movq	%r9, (%rdi)
LBB10_19:                               ## %_ZN5boost6spirit2qi6detail12string_parseIcNSt3__111__wrap_iterIPKcEEKNS0_11unused_typeEEEbPKT_SD_RT0_RKSE_RT1_.exit
	movabsq	$9218868437227405312, %rax ## imm = 0x7FF0000000000000
	movq	%rax, (%rdx)
	movb	$1, %al
	popq	%rbp
	retq
LBB10_1:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_3:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_5:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_7:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_22:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_24:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_26:
	xorl	%eax, %eax
	popq	%rbp
	retq
LBB10_28:
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEdvE4callIdEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	.weak_def_can_be_hidden	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEdvE4callIdEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	.align	4, 0x90
__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEdvE4callIdEEvRKT_RS4_N4mpl_5bool_ILb0EEE: ## @_ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEdvE4callIdEEvRKT_RS4_N4mpl_5bool_ILb0EEE
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp268:
	.cfi_def_cfa_offset 16
Ltmp269:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp270:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp271:
	.cfi_offset %rbx, -40
Ltmp272:
	.cfi_offset %r14, -32
Ltmp273:
	.cfi_offset %r15, -24
	movq	%rsi, %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -32(%rbp)
	leaq	-40(%rbp), %r15
	movq	(%rdi), %rcx
	movq	%rcx, -40(%rbp)
	movl	$1, -48(%rbp)
	cmpl	$1, (%rax)
	jne	LBB11_2
## BB#1:
	movq	%r15, -64(%rbp)
	addq	$8, %rax
Ltmp259:
	leaq	-64(%rbp), %rdx
	movl	$1, %edi
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp260:
	jmp	LBB11_3
LBB11_2:
	movq	%rax, -80(%rbp)
	movl	$1, -72(%rbp)
Ltmp257:
	leaq	-80(%rbp), %rdx
	movl	$1, %edi
	movl	$1, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp258:
LBB11_3:                                ## %_ZN5boost7variantIiJdEEaSEOS1_.exit
	movl	-48(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp265:
	leaq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp266:
## BB#4:                                ## %_ZN5boost7variantIiJdEED1Ev.exit5
	cmpq	-32(%rbp), %rbx
	jne	LBB11_10
## BB#5:                                ## %_ZN5boost7variantIiJdEED1Ev.exit5
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB11_7:
Ltmp261:
	movq	%rax, %r14
	movl	-48(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp262:
	leaq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp263:
## BB#8:                                ## %_ZN5boost7variantIiJdEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB11_9:
Ltmp267:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB11_10:                               ## %_ZN5boost7variantIiJdEED1Ev.exit5
	callq	___stack_chk_fail
LBB11_6:
Ltmp264:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table11:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset236 = Ltmp259-Lfunc_begin5          ## >> Call Site 1 <<
	.long	Lset236
Lset237 = Ltmp258-Ltmp259               ##   Call between Ltmp259 and Ltmp258
	.long	Lset237
Lset238 = Ltmp261-Lfunc_begin5          ##     jumps to Ltmp261
	.long	Lset238
	.byte	0                       ##   On action: cleanup
Lset239 = Ltmp265-Lfunc_begin5          ## >> Call Site 2 <<
	.long	Lset239
Lset240 = Ltmp266-Ltmp265               ##   Call between Ltmp265 and Ltmp266
	.long	Lset240
Lset241 = Ltmp267-Lfunc_begin5          ##     jumps to Ltmp267
	.long	Lset241
	.byte	1                       ##   On action: 1
Lset242 = Ltmp262-Lfunc_begin5          ## >> Call Site 3 <<
	.long	Lset242
Lset243 = Ltmp263-Ltmp262               ##   Call between Ltmp262 and Ltmp263
	.long	Lset243
Lset244 = Ltmp264-Lfunc_begin5          ##     jumps to Ltmp264
	.long	Lset244
	.byte	1                       ##   On action: 1
Lset245 = Ltmp263-Lfunc_begin5          ## >> Call Site 4 <<
	.long	Lset245
Lset246 = Lfunc_end5-Ltmp263            ##   Call between Ltmp263 and Lfunc_end5
	.long	Lset246
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp274:
	.cfi_def_cfa_offset 16
Ltmp275:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp276:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB12_4
## BB#1:
	leaq	LJTI12_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB12_5:
	movq	(%rdx), %rcx
	movl	(%rcx), %ecx
	movl	%ecx, (%rax)
	popq	%rbp
	retq
LBB12_2:
	movq	(%rdx), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	popq	%rbp
	retq
LBB12_3:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
LBB12_4:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L12_0_set_5 = LBB12_5-LJTI12_0
L12_0_set_2 = LBB12_2-LJTI12_0
L12_0_set_3 = LBB12_3-LJTI12_0
LJTI12_0:
	.long	L12_0_set_5
	.long	L12_0_set_2
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3
	.long	L12_0_set_3

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp277:
	.cfi_def_cfa_offset 16
Ltmp278:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp279:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp280:
	.cfi_def_cfa_offset 16
Ltmp281:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp282:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant13forced_returnIvEET_v
	.weak_def_can_be_hidden	__ZN5boost6detail7variant13forced_returnIvEET_v
	.align	4, 0x90
__ZN5boost6detail7variant13forced_returnIvEET_v: ## @_ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp283:
	.cfi_def_cfa_offset 16
Ltmp284:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp285:
	.cfi_def_cfa_register %rbp
	leaq	L___func__._ZN5boost6detail7variant13forced_returnIvEET_v(%rip), %rdi
	leaq	L_.str.99(%rip), %rsi
	leaq	L_.str.100(%rip), %rcx
	movl	$49, %edx
	callq	___assert_rtn
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception6
## BB#0:
	pushq	%rbp
Ltmp292:
	.cfi_def_cfa_offset 16
Ltmp293:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp294:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
	subq	$16, %rsp
Ltmp295:
	.cfi_offset %rbx, -32
Ltmp296:
	.cfi_offset %r14, -24
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movl	%esi, %eax
	cmpl	$19, %esi
	ja	LBB16_10
## BB#1:
	leaq	LJTI16_0(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmpq	*%rax
LBB16_3:
	movq	(%rbx), %rcx
	movl	(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$8, %rcx
Ltmp289:
	leaq	-32(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp290:
## BB#4:                                ## %_ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPviNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PT1_T2_i.exit
	movq	(%rbx), %rax
	movl	(%r14), %ecx
	movl	%ecx, 8(%rax)
	jmp	LBB16_5
LBB16_7:
	movq	(%rbx), %rcx
	movl	(%rcx), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
	addq	$8, %rcx
Ltmp286:
	leaq	-24(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp287:
## BB#8:                                ## %_ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvdNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PT1_T2_i.exit
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	movq	%rcx, 8(%rax)
LBB16_5:
	movq	(%rbx), %rax
	movl	8(%rbx), %ecx
	movl	%ecx, (%rax)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB16_9:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
LBB16_10:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rdx
	movq	%r14, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS_7variantIiJdEE13move_assignerEPvNSD_18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
LBB16_2:
Ltmp291:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB16_6:
Ltmp288:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end6:
	.cfi_endproc
	.align	2, 0x90
L16_0_set_3 = LBB16_3-LJTI16_0
L16_0_set_7 = LBB16_7-LJTI16_0
L16_0_set_9 = LBB16_9-LJTI16_0
LJTI16_0:
	.long	L16_0_set_3
	.long	L16_0_set_7
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.long	L16_0_set_9
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table16:
Lexception6:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\257\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset247 = Ltmp289-Lfunc_begin6          ## >> Call Site 1 <<
	.long	Lset247
Lset248 = Ltmp290-Ltmp289               ##   Call between Ltmp289 and Ltmp290
	.long	Lset248
Lset249 = Ltmp291-Lfunc_begin6          ##     jumps to Ltmp291
	.long	Lset249
	.byte	1                       ##   On action: 1
Lset250 = Ltmp286-Lfunc_begin6          ## >> Call Site 2 <<
	.long	Lset250
Lset251 = Ltmp287-Ltmp286               ##   Call between Ltmp286 and Ltmp287
	.long	Lset251
Lset252 = Ltmp288-Lfunc_begin6          ##     jumps to Ltmp288
	.long	Lset252
	.byte	1                       ##   On action: 1
Lset253 = Ltmp287-Lfunc_begin6          ## >> Call Site 3 <<
	.long	Lset253
Lset254 = Lfunc_end6-Ltmp287            ##   Call between Ltmp287 and Lfunc_end6
	.long	Lset254
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS_7variantIiJdEE13move_assignerEPvNS4_18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp297:
	.cfi_def_cfa_offset 16
Ltmp298:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp299:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS_7variantIiJdEE13move_assignerEPvNSD_18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS_7variantIiJdEE13move_assignerEPvNSD_18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS_7variantIiJdEE13move_assignerEPvNSD_18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS_7variantIiJdEE13move_assignerEPvNSD_18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp300:
	.cfi_def_cfa_offset 16
Ltmp301:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp302:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp303:
	.cfi_def_cfa_offset 16
Ltmp304:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp305:
	.cfi_def_cfa_register %rbp
	movq	%rcx, %rax
	movl	%esi, %ecx
	cmpl	$19, %esi
	ja	LBB19_3
## BB#1:
	leaq	LJTI19_0(%rip), %rsi
	movslq	(%rsi,%rcx,4), %rcx
	addq	%rsi, %rcx
	jmpq	*%rcx
LBB19_4:
	popq	%rbp
	retq
LBB19_2:
	xorl	%ecx, %ecx
	movl	$1, %r8d
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
LBB19_3:
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_endproc
	.align	2, 0x90
L19_0_set_4 = LBB19_4-LJTI19_0
L19_0_set_2 = LBB19_2-LJTI19_0
LJTI19_0:
	.long	L19_0_set_4
	.long	L19_0_set_4
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2
	.long	L19_0_set_2

	.globl	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.weak_def_can_be_hidden	__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.align	4, 0x90
__ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l: ## @_ZN5boost6detail7variant22visitation_impl_invokeINS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT_11result_typeEiRS8_T0_PNS1_22apply_visitor_unrolledET1_l
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp306:
	.cfi_def_cfa_offset 16
Ltmp307:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp308:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.weak_def_can_be_hidden	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.align	4, 0x90
__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_: ## @_ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi20EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_5l_endEEESA_EENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSH_T2_NS3_5bool_ILb1EEET3_PT_PT0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp309:
	.cfi_def_cfa_offset 16
Ltmp310:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp311:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6detail7variant13forced_returnIvEET_v
	.cfi_endproc

	.globl	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEivE4callIiEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	.weak_def_can_be_hidden	__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEivE4callIiEEvRKT_RS4_N4mpl_5bool_ILb0EEE
	.align	4, 0x90
__ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEivE4callIiEEvRKT_RS4_N4mpl_5bool_ILb0EEE: ## @_ZN5boost6spirit6traits30assign_to_attribute_from_valueINS_7variantIiJdEEEivE4callIiEEvRKT_RS4_N4mpl_5bool_ILb0EEE
Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception7
## BB#0:
	pushq	%rbp
Ltmp323:
	.cfi_def_cfa_offset 16
Ltmp324:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp325:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp326:
	.cfi_offset %rbx, -40
Ltmp327:
	.cfi_offset %r14, -32
Ltmp328:
	.cfi_offset %r15, -24
	movq	%rsi, %rax
	movq	___stack_chk_guard@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rbx
	movq	%rbx, -32(%rbp)
	leaq	-40(%rbp), %r15
	movl	(%rdi), %ecx
	movl	%ecx, -40(%rbp)
	movl	$0, -48(%rbp)
	cmpl	$0, (%rax)
	je	LBB22_1
## BB#2:
	movq	%rax, -80(%rbp)
	movl	$0, -72(%rbp)
Ltmp312:
	leaq	-80(%rbp), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS_7variantIiJdEE13move_assignerEPvNSK_18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp313:
	jmp	LBB22_3
LBB22_1:
	movq	%r15, -64(%rbp)
	addq	$8, %rax
Ltmp314:
	leaq	-64(%rbp), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_12move_storageEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp315:
LBB22_3:                                ## %_ZN5boost7variantIiJdEEaSEOS1_.exit
	movl	-48(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp320:
	leaq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp321:
## BB#4:                                ## %_ZN5boost7variantIiJdEED1Ev.exit5
	cmpq	-32(%rbp), %rbx
	jne	LBB22_10
## BB#5:                                ## %_ZN5boost7variantIiJdEED1Ev.exit5
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB22_7:
Ltmp316:
	movq	%rax, %r14
	movl	-48(%rbp), %edi
	movl	%edi, %esi
	sarl	$31, %esi
	xorl	%edi, %esi
Ltmp317:
	leaq	-56(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%r15, %rcx
	callq	__ZN5boost6detail7variant15visitation_implIN4mpl_4int_ILi0EEENS1_20visitation_impl_stepINS_3mpl6l_iterINS7_6l_itemINS3_5long_ILl2EEEiNS9_INSA_ILl1EEEdNS7_5l_endEEEEEEENS8_ISD_EEEENS1_9destroyerEPvNS_7variantIiJdEE18has_fallback_type_EEENT1_11result_typeEiiRSO_T2_NS3_5bool_ILb0EEET3_PT_PT0_
Ltmp318:
## BB#8:                                ## %_ZN5boost7variantIiJdEED1Ev.exit
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB22_9:
Ltmp322:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB22_10:                               ## %_ZN5boost7variantIiJdEED1Ev.exit5
	callq	___stack_chk_fail
LBB22_6:
Ltmp319:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end7:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table22:
Lexception7:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset255 = Ltmp312-Lfunc_begin7          ## >> Call Site 1 <<
	.long	Lset255
Lset256 = Ltmp315-Ltmp312               ##   Call between Ltmp312 and Ltmp315
	.long	Lset256
Lset257 = Ltmp316-Lfunc_begin7          ##     jumps to Ltmp316
	.long	Lset257
	.byte	0                       ##   On action: cleanup
Lset258 = Ltmp320-Lfunc_begin7          ## >> Call Site 2 <<
	.long	Lset258
Lset259 = Ltmp321-Ltmp320               ##   Call between Ltmp320 and Ltmp321
	.long	Lset259
Lset260 = Ltmp322-Lfunc_begin7          ##     jumps to Ltmp322
	.long	Lset260
	.byte	1                       ##   On action: 1
Lset261 = Ltmp317-Lfunc_begin7          ## >> Call Site 3 <<
	.long	Lset261
Lset262 = Ltmp318-Ltmp317               ##   Call between Ltmp317 and Ltmp318
	.long	Lset262
Lset263 = Ltmp319-Lfunc_begin7          ##     jumps to Ltmp319
	.long	Lset263
	.byte	1                       ##   On action: 1
Lset264 = Ltmp318-Lfunc_begin7          ## >> Call Site 4 <<
	.long	Lset264
Lset265 = Lfunc_end7-Ltmp318            ##   Call between Ltmp318 and Lfunc_end7
	.long	Lset265
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
	.weak_def_can_be_hidden	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
	.align	4, 0x90
__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_: ## @_ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE11move_assignERSP_
Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception8
## BB#0:
	pushq	%rbp
Ltmp340:
	.cfi_def_cfa_offset 16
Ltmp341:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp342:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp343:
	.cfi_offset %rbx, -32
Ltmp344:
	.cfi_offset %r14, -24
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpq	%r14, %rbx
	je	LBB23_11
## BB#1:
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	LBB23_6
## BB#2:
	movq	%rax, (%r14)
	testb	$1, %al
	jne	LBB23_3
## BB#4:
	andq	$-2, %rax
	movq	(%rax), %rax
	leaq	8(%rbx), %rdi
	leaq	8(%r14), %rsi
Ltmp329:
	movl	$1, %edx
	callq	*%rax
Ltmp330:
## BB#5:
	movq	$0, (%rbx)
	jmp	LBB23_11
LBB23_6:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	LBB23_11
## BB#7:
	testb	$1, %al
	jne	LBB23_10
## BB#8:
	andq	$-2, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	LBB23_10
## BB#9:
	leaq	8(%r14), %rdi
Ltmp331:
	movl	$2, %edx
	movq	%rdi, %rsi
	callq	*%rax
Ltmp332:
LBB23_10:                               ## %_ZNK5boost6detail8function13basic_vtable4IbRNSt3__111__wrap_iterIPKcEERKS7_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSD_4nil_EEENSD_6vectorIJEEEEERKNSB_11unused_typeEE5clearERNS1_15function_bufferE.exit.i
	movq	$0, (%r14)
	jmp	LBB23_11
LBB23_3:
	movq	24(%rbx), %rax
	movq	%rax, 24(%r14)
	movq	8(%rbx), %rax
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%r14)
	movq	%rax, 8(%r14)
	movq	$0, (%rbx)
LBB23_11:                               ## %_ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5clearEv.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB23_15:
Ltmp333:
	movq	%rax, %rdi
	callq	___cxa_begin_catch
	movq	$0, (%r14)
Ltmp334:
	callq	___cxa_rethrow
Ltmp335:
## BB#16:
LBB23_12:
Ltmp336:
	movq	%rax, %rbx
Ltmp337:
	callq	___cxa_end_catch
Ltmp338:
## BB#13:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB23_14:
Ltmp339:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end8:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table23:
Lexception8:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	73                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset266 = Ltmp329-Lfunc_begin8          ## >> Call Site 1 <<
	.long	Lset266
Lset267 = Ltmp332-Ltmp329               ##   Call between Ltmp329 and Ltmp332
	.long	Lset267
Lset268 = Ltmp333-Lfunc_begin8          ##     jumps to Ltmp333
	.long	Lset268
	.byte	1                       ##   On action: 1
Lset269 = Ltmp332-Lfunc_begin8          ## >> Call Site 2 <<
	.long	Lset269
Lset270 = Ltmp334-Ltmp332               ##   Call between Ltmp332 and Ltmp334
	.long	Lset270
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset271 = Ltmp334-Lfunc_begin8          ## >> Call Site 3 <<
	.long	Lset271
Lset272 = Ltmp335-Ltmp334               ##   Call between Ltmp334 and Ltmp335
	.long	Lset272
Lset273 = Ltmp336-Lfunc_begin8          ##     jumps to Ltmp336
	.long	Lset273
	.byte	0                       ##   On action: cleanup
Lset274 = Ltmp337-Lfunc_begin8          ## >> Call Site 4 <<
	.long	Lset274
Lset275 = Ltmp338-Ltmp337               ##   Call between Ltmp337 and Ltmp338
	.long	Lset275
Lset276 = Ltmp339-Lfunc_begin8          ##     jumps to Ltmp339
	.long	Lset276
	.byte	1                       ##   On action: 1
Lset277 = Ltmp338-Lfunc_begin8          ## >> Call Site 5 <<
	.long	Lset277
Lset278 = Lfunc_end8-Ltmp338            ##   Call between Ltmp338 and Lfunc_end8
	.long	Lset278
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5dummy7nonnullEv
	.weak_definition	__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5dummy7nonnullEv
	.align	4, 0x90
__ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5dummy7nonnullEv: ## @_ZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE5dummy7nonnullEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp345:
	.cfi_def_cfa_offset 16
Ltmp346:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp347:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost15throw_exceptionINS_17bad_function_callEEEvRKT_
	.weak_def_can_be_hidden	__ZN5boost15throw_exceptionINS_17bad_function_callEEEvRKT_
	.align	4, 0x90
__ZN5boost15throw_exceptionINS_17bad_function_callEEEvRKT_: ## @_ZN5boost15throw_exceptionINS_17bad_function_callEEEvRKT_
Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception9
## BB#0:
	pushq	%rbp
Ltmp356:
	.cfi_def_cfa_offset 16
Ltmp357:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp358:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
Ltmp359:
	.cfi_offset %rbx, -40
Ltmp360:
	.cfi_offset %r14, -32
Ltmp361:
	.cfi_offset %r15, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %r15
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	__ZNSt13runtime_errorC2ERKS_
	movq	$0, -40(%rbp)
	movq	$0, -48(%rbp)
	movq	$0, -56(%rbp)
	movl	$-1, -32(%rbp)
	movq	__ZTVN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE@GOTPCREL(%rip), %rax
	leaq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
	addq	$56, %rax
	movq	%rax, -64(%rbp)
	movb	$1, %bl
Ltmp348:
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS4_
Ltmp349:
## BB#1:                                ## %_ZN5boost24enable_current_exceptionINS_16exception_detail19error_info_injectorINS_17bad_function_callEEEEENS1_10clone_implIT_EERKS6_.exit
	xorl	%ebx, %ebx
Ltmp350:
	movq	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE@GOTPCREL(%rip), %rsi
	movq	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev@GOTPCREL(%rip), %rdx
	movq	%r15, %rdi
	callq	___cxa_throw
Ltmp351:
## BB#9:
LBB25_2:
Ltmp352:
	movq	%rax, %r14
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, -64(%rbp)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB25_6
## BB#3:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp353:
	callq	*%rax
Ltmp354:
## BB#4:                                ## %.noexc.i.i.i.i.i
	testb	%al, %al
	je	LBB25_6
## BB#5:
	movq	$0, -56(%rbp)
LBB25_6:
	leaq	-80(%rbp), %rdi
	callq	__ZNSt13runtime_errorD2Ev
	testb	%bl, %bl
	je	LBB25_8
## BB#7:
	movq	%r15, %rdi
	callq	___cxa_free_exception
LBB25_8:
	movq	%r14, %rdi
	callq	__Unwind_Resume
LBB25_10:
Ltmp355:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end9:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table25:
Lexception9:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\274"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	52                      ## Call site table length
Lset279 = Lfunc_begin9-Lfunc_begin9     ## >> Call Site 1 <<
	.long	Lset279
Lset280 = Ltmp348-Lfunc_begin9          ##   Call between Lfunc_begin9 and Ltmp348
	.long	Lset280
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset281 = Ltmp348-Lfunc_begin9          ## >> Call Site 2 <<
	.long	Lset281
Lset282 = Ltmp351-Ltmp348               ##   Call between Ltmp348 and Ltmp351
	.long	Lset282
Lset283 = Ltmp352-Lfunc_begin9          ##     jumps to Ltmp352
	.long	Lset283
	.byte	0                       ##   On action: cleanup
Lset284 = Ltmp353-Lfunc_begin9          ## >> Call Site 3 <<
	.long	Lset284
Lset285 = Ltmp354-Ltmp353               ##   Call between Ltmp353 and Ltmp354
	.long	Lset285
Lset286 = Ltmp355-Lfunc_begin9          ##     jumps to Ltmp355
	.long	Lset286
	.byte	1                       ##   On action: 1
Lset287 = Ltmp354-Lfunc_begin9          ## >> Call Site 4 <<
	.long	Lset287
Lset288 = Lfunc_end9-Ltmp354            ##   Call between Ltmp354 and Lfunc_end9
	.long	Lset288
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost17bad_function_callD1Ev
	.weak_def_can_be_hidden	__ZN5boost17bad_function_callD1Ev
	.align	4, 0x90
__ZN5boost17bad_function_callD1Ev:      ## @_ZN5boost17bad_function_callD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp362:
	.cfi_def_cfa_offset 16
Ltmp363:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp364:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
	.cfi_endproc

	.globl	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.align	4, 0x90
__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev: ## @_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception10
## BB#0:
	pushq	%rbp
Ltmp368:
	.cfi_def_cfa_offset 16
Ltmp369:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp370:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp371:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	LBB27_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp365:
	callq	*%rax
Ltmp366:
## BB#2:                                ## %.noexc.i.i.i.i.i
	testb	%al, %al
	je	LBB27_4
## BB#3:
	movq	$0, 24(%rbx)
LBB27_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
LBB27_5:
Ltmp367:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end10:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table27:
Lexception10:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset289 = Ltmp365-Lfunc_begin10         ## >> Call Site 1 <<
	.long	Lset289
Lset290 = Ltmp366-Ltmp365               ##   Call between Ltmp365 and Ltmp366
	.long	Lset290
Lset291 = Ltmp367-Lfunc_begin10         ##     jumps to Ltmp367
	.long	Lset291
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.align	4, 0x90
__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev: ## @_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception11
## BB#0:
	pushq	%rbp
Ltmp375:
	.cfi_def_cfa_offset 16
Ltmp376:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp377:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp378:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	LBB28_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp372:
	callq	*%rax
Ltmp373:
## BB#2:                                ## %.noexc.i.i.i.i
	testb	%al, %al
	je	LBB28_4
## BB#3:
	movq	$0, 24(%rbx)
LBB28_4:                                ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
LBB28_5:
Ltmp374:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end11:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table28:
Lexception11:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset292 = Ltmp372-Lfunc_begin11         ## >> Call Site 1 <<
	.long	Lset292
Lset293 = Ltmp373-Ltmp372               ##   Call between Ltmp372 and Ltmp373
	.long	Lset293
Lset294 = Ltmp374-Lfunc_begin11         ##     jumps to Ltmp374
	.long	Lset294
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS4_
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS4_
	.align	4, 0x90
__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS4_: ## @_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS4_
Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception12
## BB#0:
	pushq	%rbp
Ltmp409:
	.cfi_def_cfa_offset 16
Ltmp410:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp411:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp412:
	.cfi_offset %rbx, -56
Ltmp413:
	.cfi_offset %r12, -48
Ltmp414:
	.cfi_offset %r13, -40
Ltmp415:
	.cfi_offset %r14, -32
Ltmp416:
	.cfi_offset %r15, -24
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	__ZTVN5boost16exception_detail10clone_baseE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 56(%r14)
	callq	__ZNSt13runtime_errorC2ERKS_
	movq	__ZTVN5boost17bad_function_callE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%r14)
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %r13
	addq	$16, %r13
	movq	%r13, 16(%r14)
	movq	24(%rbx), %rdi
	movq	%rdi, 24(%r14)
	testq	%rdi, %rdi
	je	LBB29_2
## BB#1:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
Ltmp379:
	callq	*%rax
Ltmp380:
LBB29_2:
	movl	48(%rbx), %eax
	movl	%eax, 48(%r14)
	movq	32(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rcx, 40(%r14)
	movq	%rax, 32(%r14)
	movq	__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%r14)
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%r14)
	addq	$136, %rax
	movq	%rax, 56(%r14)
	movq	24(%rbx), %rsi
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	LBB29_11
## BB#3:
	movq	(%rsi), %rax
	movq	40(%rax), %rax
Ltmp382:
	leaq	-48(%rbp), %rdi
	callq	*%rax
Ltmp383:
## BB#4:                                ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEE7releaseEv.exit.i.i.i
	movq	-48(%rbp), %r12
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	LBB29_11
## BB#5:
	movq	(%r12), %rax
	movq	24(%rax), %rax
Ltmp385:
	movq	%r12, %rdi
	callq	*%rax
Ltmp386:
## BB#6:                                ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEEaSERKS3_.exit.i
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB29_10
## BB#7:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp391:
	callq	*%rax
Ltmp392:
## BB#8:                                ## %.noexc.i.i.3.i
	testb	%al, %al
	je	LBB29_10
## BB#9:
	movq	$0, -48(%rbp)
LBB29_10:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit4.i
	movq	%r12, %r15
LBB29_11:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit4.i
	movl	48(%rbx), %eax
	movl	%eax, 48(%r14)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 32(%r14)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	LBB29_15
## BB#12:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp394:
	callq	*%rax
Ltmp395:
## BB#13:                               ## %.noexc8.i
	testb	%al, %al
	je	LBB29_15
## BB#14:
	movq	$0, 24(%r14)
LBB29_15:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEE7releaseEv.exit.i.i.7.i
	movq	%r15, 24(%r14)
	testq	%r15, %r15
	je	LBB29_18
## BB#16:
	movq	(%r15), %rax
	movq	24(%rax), %rax
Ltmp397:
	movq	%r15, %rdi
	callq	*%rax
Ltmp398:
## BB#17:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEEaSERKS3_.exit10.i
	movq	(%r15), %rax
	movq	32(%rax), %rax
Ltmp406:
	movq	%r15, %rdi
	callq	*%rax
Ltmp407:
LBB29_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB29_37:                               ## %.body
Ltmp381:
	movq	%rax, %rbx
	jmp	LBB29_38
LBB29_31:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread18.i
Ltmp384:
	movq	%rax, %rbx
	jmp	LBB29_32
LBB29_28:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.i
Ltmp396:
	movq	%rax, %rbx
	testq	%r15, %r15
	je	LBB29_32
	jmp	LBB29_29
LBB29_39:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread19.i
Ltmp399:
	movq	%rax, %rbx
	jmp	LBB29_29
LBB29_27:
Ltmp408:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB29_20:
Ltmp387:
	movq	%rax, %rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	jne	LBB29_22
## BB#21:
	movq	%r12, %r15
	jmp	LBB29_29
LBB29_19:
Ltmp393:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB29_22:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp388:
	callq	*%rax
Ltmp389:
## BB#23:                               ## %.noexc.i.i.5.i
	testb	%al, %al
	je	LBB29_24
## BB#25:
	movq	$0, -48(%rbp)
	movq	%r12, %r15
	jmp	LBB29_29
LBB29_26:
Ltmp390:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB29_24:
	movq	%r12, %r15
LBB29_29:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread.i
	movq	(%r15), %rax
	movq	32(%rax), %rax
Ltmp400:
	movq	%r15, %rdi
	callq	*%rax
Ltmp401:
LBB29_32:                               ## %.body2
	movq	%r13, 16(%r14)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	LBB29_38
## BB#33:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp403:
	callq	*%rax
Ltmp404:
## BB#34:                               ## %.noexc.i.i.i.i
	testb	%al, %al
	je	LBB29_38
## BB#35:
	movq	$0, 24(%r14)
LBB29_38:                               ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED2Ev.exit
	movq	%r14, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB29_30:
Ltmp402:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB29_36:
Ltmp405:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end12:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table29:
Lexception12:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.ascii	"\230\001"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\217\001"              ## Call site table length
Lset295 = Ltmp379-Lfunc_begin12         ## >> Call Site 1 <<
	.long	Lset295
Lset296 = Ltmp380-Ltmp379               ##   Call between Ltmp379 and Ltmp380
	.long	Lset296
Lset297 = Ltmp381-Lfunc_begin12         ##     jumps to Ltmp381
	.long	Lset297
	.byte	0                       ##   On action: cleanup
Lset298 = Ltmp382-Lfunc_begin12         ## >> Call Site 2 <<
	.long	Lset298
Lset299 = Ltmp383-Ltmp382               ##   Call between Ltmp382 and Ltmp383
	.long	Lset299
Lset300 = Ltmp384-Lfunc_begin12         ##     jumps to Ltmp384
	.long	Lset300
	.byte	0                       ##   On action: cleanup
Lset301 = Ltmp385-Lfunc_begin12         ## >> Call Site 3 <<
	.long	Lset301
Lset302 = Ltmp386-Ltmp385               ##   Call between Ltmp385 and Ltmp386
	.long	Lset302
Lset303 = Ltmp387-Lfunc_begin12         ##     jumps to Ltmp387
	.long	Lset303
	.byte	0                       ##   On action: cleanup
Lset304 = Ltmp391-Lfunc_begin12         ## >> Call Site 4 <<
	.long	Lset304
Lset305 = Ltmp392-Ltmp391               ##   Call between Ltmp391 and Ltmp392
	.long	Lset305
Lset306 = Ltmp393-Lfunc_begin12         ##     jumps to Ltmp393
	.long	Lset306
	.byte	1                       ##   On action: 1
Lset307 = Ltmp394-Lfunc_begin12         ## >> Call Site 5 <<
	.long	Lset307
Lset308 = Ltmp395-Ltmp394               ##   Call between Ltmp394 and Ltmp395
	.long	Lset308
Lset309 = Ltmp396-Lfunc_begin12         ##     jumps to Ltmp396
	.long	Lset309
	.byte	0                       ##   On action: cleanup
Lset310 = Ltmp397-Lfunc_begin12         ## >> Call Site 6 <<
	.long	Lset310
Lset311 = Ltmp398-Ltmp397               ##   Call between Ltmp397 and Ltmp398
	.long	Lset311
Lset312 = Ltmp399-Lfunc_begin12         ##     jumps to Ltmp399
	.long	Lset312
	.byte	0                       ##   On action: cleanup
Lset313 = Ltmp406-Lfunc_begin12         ## >> Call Site 7 <<
	.long	Lset313
Lset314 = Ltmp407-Ltmp406               ##   Call between Ltmp406 and Ltmp407
	.long	Lset314
Lset315 = Ltmp408-Lfunc_begin12         ##     jumps to Ltmp408
	.long	Lset315
	.byte	1                       ##   On action: 1
Lset316 = Ltmp388-Lfunc_begin12         ## >> Call Site 8 <<
	.long	Lset316
Lset317 = Ltmp389-Ltmp388               ##   Call between Ltmp388 and Ltmp389
	.long	Lset317
Lset318 = Ltmp390-Lfunc_begin12         ##     jumps to Ltmp390
	.long	Lset318
	.byte	1                       ##   On action: 1
Lset319 = Ltmp400-Lfunc_begin12         ## >> Call Site 9 <<
	.long	Lset319
Lset320 = Ltmp401-Ltmp400               ##   Call between Ltmp400 and Ltmp401
	.long	Lset320
Lset321 = Ltmp402-Lfunc_begin12         ##     jumps to Ltmp402
	.long	Lset321
	.byte	1                       ##   On action: 1
Lset322 = Ltmp403-Lfunc_begin12         ## >> Call Site 10 <<
	.long	Lset322
Lset323 = Ltmp404-Ltmp403               ##   Call between Ltmp403 and Ltmp404
	.long	Lset323
Lset324 = Ltmp405-Lfunc_begin12         ##     jumps to Ltmp405
	.long	Lset324
	.byte	1                       ##   On action: 1
Lset325 = Ltmp404-Lfunc_begin12         ## >> Call Site 11 <<
	.long	Lset325
Lset326 = Lfunc_end12-Ltmp404           ##   Call between Ltmp404 and Lfunc_end12
	.long	Lset326
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.align	4, 0x90
__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev: ## @_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception13
## BB#0:
	pushq	%rbp
Ltmp420:
	.cfi_def_cfa_offset 16
Ltmp421:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp422:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp423:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	LBB30_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp417:
	callq	*%rax
Ltmp418:
## BB#2:                                ## %.noexc.i.i.i.i.i.i
	testb	%al, %al
	je	LBB30_4
## BB#3:
	movq	$0, 24(%rbx)
LBB30_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev.exit
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB30_5:
Ltmp419:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end13:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table30:
Lexception13:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset327 = Ltmp417-Lfunc_begin13         ## >> Call Site 1 <<
	.long	Lset327
Lset328 = Ltmp418-Ltmp417               ##   Call between Ltmp417 and Ltmp418
	.long	Lset328
Lset329 = Ltmp419-Lfunc_begin13         ##     jumps to Ltmp419
	.long	Lset329
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.weak_def_can_be_hidden	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.align	4, 0x90
__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv: ## @_ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception14
## BB#0:
	pushq	%rbp
Ltmp427:
	.cfi_def_cfa_offset 16
Ltmp428:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp429:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp430:
	.cfi_offset %rbx, -32
Ltmp431:
	.cfi_offset %r14, -24
	movq	%rdi, %r14
	movl	$64, %edi
	callq	__Znwm
	movq	%rax, %rbx
Ltmp424:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE
Ltmp425:
## BB#1:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	addq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB31_2:
Ltmp426:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	__ZdlPv
	movq	%r14, %rdi
	callq	__Unwind_Resume
Lfunc_end14:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table31:
Lexception14:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset330 = Lfunc_begin14-Lfunc_begin14   ## >> Call Site 1 <<
	.long	Lset330
Lset331 = Ltmp424-Lfunc_begin14         ##   Call between Lfunc_begin14 and Ltmp424
	.long	Lset331
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset332 = Ltmp424-Lfunc_begin14         ## >> Call Site 2 <<
	.long	Lset332
Lset333 = Ltmp425-Ltmp424               ##   Call between Ltmp424 and Ltmp425
	.long	Lset333
Lset334 = Ltmp426-Lfunc_begin14         ##     jumps to Ltmp426
	.long	Lset334
	.byte	0                       ##   On action: cleanup
Lset335 = Ltmp425-Lfunc_begin14         ## >> Call Site 3 <<
	.long	Lset335
Lset336 = Lfunc_end14-Ltmp425           ##   Call between Ltmp425 and Lfunc_end14
	.long	Lset336
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.weak_def_can_be_hidden	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.align	4, 0x90
__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv: ## @_ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception15
## BB#0:
	pushq	%rbp
Ltmp435:
	.cfi_def_cfa_offset 16
Ltmp436:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp437:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp438:
	.cfi_offset %rbx, -32
Ltmp439:
	.cfi_offset %r14, -24
	movq	%rdi, %r14
	movl	$64, %edi
	callq	___cxa_allocate_exception
	movq	%rax, %rbx
	movq	__ZTVN5boost16exception_detail10clone_baseE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 56(%rbx)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	__ZNSt13runtime_errorC2ERKS_
	movq	__ZTVN5boost17bad_function_callE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%rbx)
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	24(%r14), %rdi
	movq	%rdi, 24(%rbx)
	testq	%rdi, %rdi
	je	LBB32_2
## BB#1:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
Ltmp432:
	callq	*%rax
Ltmp433:
LBB32_2:
	movl	48(%r14), %eax
	movl	%eax, 48(%rbx)
	movq	32(%r14), %rax
	movq	40(%r14), %rcx
	movq	%rcx, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%rbx)
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%rbx)
	addq	$136, %rax
	movq	%rax, 56(%rbx)
	movq	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE@GOTPCREL(%rip), %rsi
	movq	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev@GOTPCREL(%rip), %rdx
	movq	%rbx, %rdi
	callq	___cxa_throw
LBB32_3:                                ## %.body
Ltmp434:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	callq	___cxa_free_exception
	movq	%r14, %rdi
	callq	__Unwind_Resume
Lfunc_end15:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table32:
Lexception15:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset337 = Lfunc_begin15-Lfunc_begin15   ## >> Call Site 1 <<
	.long	Lset337
Lset338 = Ltmp432-Lfunc_begin15         ##   Call between Lfunc_begin15 and Ltmp432
	.long	Lset338
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset339 = Ltmp432-Lfunc_begin15         ## >> Call Site 2 <<
	.long	Lset339
Lset340 = Ltmp433-Ltmp432               ##   Call between Ltmp432 and Ltmp433
	.long	Lset340
Lset341 = Ltmp434-Lfunc_begin15         ##     jumps to Ltmp434
	.long	Lset341
	.byte	0                       ##   On action: cleanup
Lset342 = Ltmp433-Lfunc_begin15         ## >> Call Site 3 <<
	.long	Lset342
Lset343 = Lfunc_end15-Ltmp433           ##   Call between Ltmp433 and Lfunc_end15
	.long	Lset343
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.weak_def_can_be_hidden	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.align	4, 0x90
__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev: ## @_ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception16
## BB#0:
	pushq	%rbp
Ltmp443:
	.cfi_def_cfa_offset 16
Ltmp444:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp445:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp446:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	LBB33_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp440:
	callq	*%rax
Ltmp441:
## BB#2:                                ## %.noexc.i.i.i.i.i.i
	testb	%al, %al
	je	LBB33_4
## BB#3:
	movq	$0, 24(%rbx)
LBB33_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
LBB33_5:
Ltmp442:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end16:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table33:
Lexception16:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset344 = Ltmp440-Lfunc_begin16         ## >> Call Site 1 <<
	.long	Lset344
Lset345 = Ltmp441-Ltmp440               ##   Call between Ltmp440 and Ltmp441
	.long	Lset345
Lset346 = Ltmp442-Lfunc_begin16         ##     jumps to Ltmp442
	.long	Lset346
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.weak_def_can_be_hidden	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.align	4, 0x90
__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev: ## @_ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception17
## BB#0:
	pushq	%rbp
Ltmp450:
	.cfi_def_cfa_offset 16
Ltmp451:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp452:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp453:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	LBB34_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp447:
	callq	*%rax
Ltmp448:
## BB#2:                                ## %.noexc.i.i.i.i.i.i.i
	testb	%al, %al
	je	LBB34_4
## BB#3:
	movq	$0, 24(%rbx)
LBB34_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev.exit
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB34_5:
Ltmp449:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end17:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table34:
Lexception17:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset347 = Ltmp447-Lfunc_begin17         ## >> Call Site 1 <<
	.long	Lset347
Lset348 = Ltmp448-Ltmp447               ##   Call between Ltmp447 and Ltmp448
	.long	Lset348
Lset349 = Ltmp449-Lfunc_begin17         ##     jumps to Ltmp449
	.long	Lset349
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZTv0_n24_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.weak_def_can_be_hidden	__ZTv0_n24_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.align	4, 0x90
__ZTv0_n24_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv: ## @_ZTv0_n24_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception18
## BB#0:
	pushq	%rbp
Ltmp457:
	.cfi_def_cfa_offset 16
Ltmp458:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp459:
	.cfi_def_cfa_register %rbp
	pushq	%r14
	pushq	%rbx
Ltmp460:
	.cfi_offset %rbx, -32
Ltmp461:
	.cfi_offset %r14, -24
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	addq	-24(%rax), %rbx
	movl	$64, %edi
	callq	__Znwm
	movq	%rax, %r14
Ltmp454:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE
Ltmp455:
## BB#1:                                ## %_ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv.exit
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	addq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
LBB35_2:
Ltmp456:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	__ZdlPv
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end18:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table35:
Lexception18:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset350 = Lfunc_begin18-Lfunc_begin18   ## >> Call Site 1 <<
	.long	Lset350
Lset351 = Ltmp454-Lfunc_begin18         ##   Call between Lfunc_begin18 and Ltmp454
	.long	Lset351
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset352 = Ltmp454-Lfunc_begin18         ## >> Call Site 2 <<
	.long	Lset352
Lset353 = Ltmp455-Ltmp454               ##   Call between Ltmp454 and Ltmp455
	.long	Lset353
Lset354 = Ltmp456-Lfunc_begin18         ##     jumps to Ltmp456
	.long	Lset354
	.byte	0                       ##   On action: cleanup
Lset355 = Ltmp455-Lfunc_begin18         ## >> Call Site 3 <<
	.long	Lset355
Lset356 = Lfunc_end18-Ltmp455           ##   Call between Ltmp455 and Lfunc_end18
	.long	Lset356
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZTv0_n32_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.weak_def_can_be_hidden	__ZTv0_n32_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.align	4, 0x90
__ZTv0_n32_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv: ## @_ZTv0_n32_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp462:
	.cfi_def_cfa_offset 16
Ltmp463:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp464:
	.cfi_def_cfa_register %rbp
	movq	(%rdi), %rax
	addq	-32(%rax), %rdi
	callq	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.cfi_endproc

	.globl	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.weak_def_can_be_hidden	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.align	4, 0x90
__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev: ## @_ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception19
## BB#0:
	pushq	%rbp
Ltmp468:
	.cfi_def_cfa_offset 16
Ltmp469:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp470:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp471:
	.cfi_offset %rbx, -24
	movq	(%rdi), %rax
	movq	-40(%rax), %rax
	leaq	(%rdi,%rax), %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 16(%rdi,%rax)
	movq	24(%rdi,%rax), %rdi
	testq	%rdi, %rdi
	je	LBB37_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp465:
	callq	*%rax
Ltmp466:
## BB#2:                                ## %.noexc.i.i.i.i.i.i
	testb	%al, %al
	je	LBB37_4
## BB#3:
	movq	$0, 24(%rbx)
LBB37_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
LBB37_5:
Ltmp467:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end19:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table37:
Lexception19:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset357 = Ltmp465-Lfunc_begin19         ## >> Call Site 1 <<
	.long	Lset357
Lset358 = Ltmp466-Ltmp465               ##   Call between Ltmp465 and Ltmp466
	.long	Lset358
Lset359 = Ltmp467-Lfunc_begin19         ##     jumps to Ltmp467
	.long	Lset359
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.weak_def_can_be_hidden	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.align	4, 0x90
__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev: ## @_ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception20
## BB#0:
	pushq	%rbp
Ltmp475:
	.cfi_def_cfa_offset 16
Ltmp476:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp477:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp478:
	.cfi_offset %rbx, -24
	movq	(%rdi), %rax
	movq	-40(%rax), %rax
	leaq	(%rdi,%rax), %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, 16(%rdi,%rax)
	movq	24(%rdi,%rax), %rdi
	testq	%rdi, %rdi
	je	LBB38_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp472:
	callq	*%rax
Ltmp473:
## BB#2:                                ## %.noexc.i.i.i.i.i.i.i
	testb	%al, %al
	je	LBB38_4
## BB#3:
	movq	$0, 24(%rbx)
LBB38_4:                                ## %_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev.exit
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB38_5:
Ltmp474:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end20:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table38:
Lexception20:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset360 = Ltmp472-Lfunc_begin20         ## >> Call Site 1 <<
	.long	Lset360
Lset361 = Ltmp473-Ltmp472               ##   Call between Ltmp472 and Ltmp473
	.long	Lset361
Lset362 = Ltmp474-Lfunc_begin20         ##     jumps to Ltmp474
	.long	Lset362
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost16exception_detail10clone_baseD1Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_baseD1Ev
	.align	4, 0x90
__ZN5boost16exception_detail10clone_baseD1Ev: ## @_ZN5boost16exception_detail10clone_baseD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp479:
	.cfi_def_cfa_offset 16
Ltmp480:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp481:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost16exception_detail10clone_baseD0Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_baseD0Ev
	.align	4, 0x90
__ZN5boost16exception_detail10clone_baseD0Ev: ## @_ZN5boost16exception_detail10clone_baseD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp482:
	.cfi_def_cfa_offset 16
Ltmp483:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp484:
	.cfi_def_cfa_register %rbp
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
	.weak_def_can_be_hidden	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
	.align	4, 0x90
__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev: ## @_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception21
## BB#0:
	pushq	%rbp
Ltmp488:
	.cfi_def_cfa_offset 16
Ltmp489:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp490:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp491:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 16(%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	LBB41_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp485:
	callq	*%rax
Ltmp486:
## BB#2:                                ## %.noexc.i.i.i.i.i
	testb	%al, %al
	je	LBB41_4
## BB#3:
	movq	$0, 24(%rbx)
LBB41_4:                                ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev.exit
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB41_5:
Ltmp487:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end21:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table41:
Lexception21:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset363 = Ltmp485-Lfunc_begin21         ## >> Call Site 1 <<
	.long	Lset363
Lset364 = Ltmp486-Ltmp485               ##   Call between Ltmp485 and Ltmp486
	.long	Lset364
Lset365 = Ltmp487-Lfunc_begin21         ##     jumps to Ltmp487
	.long	Lset365
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.weak_def_can_be_hidden	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.align	4, 0x90
__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev: ## @_ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception22
## BB#0:
	pushq	%rbp
Ltmp495:
	.cfi_def_cfa_offset 16
Ltmp496:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp497:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp498:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	LBB42_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp492:
	callq	*%rax
Ltmp493:
## BB#2:                                ## %.noexc.i.i.i.i.i
	testb	%al, %al
	je	LBB42_4
## BB#3:
	movq	$0, 24(%rbx)
LBB42_4:                                ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZNSt13runtime_errorD2Ev ## TAILCALL
LBB42_5:
Ltmp494:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end22:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table42:
Lexception22:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset366 = Ltmp492-Lfunc_begin22         ## >> Call Site 1 <<
	.long	Lset366
Lset367 = Ltmp493-Ltmp492               ##   Call between Ltmp492 and Ltmp493
	.long	Lset367
Lset368 = Ltmp494-Lfunc_begin22         ##     jumps to Ltmp494
	.long	Lset368
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
	.weak_def_can_be_hidden	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
	.align	4, 0x90
__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev: ## @_ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception23
## BB#0:
	pushq	%rbp
Ltmp502:
	.cfi_def_cfa_offset 16
Ltmp503:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp504:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp505:
	.cfi_offset %rbx, -24
	movq	%rdi, %rax
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	8(%rax), %rdi
	leaq	-16(%rax), %rbx
	testq	%rdi, %rdi
	je	LBB43_4
## BB#1:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp499:
	callq	*%rax
Ltmp500:
## BB#2:                                ## %.noexc.i.i.i.i.i.i
	testb	%al, %al
	je	LBB43_4
## BB#3:
	movq	$0, 24(%rbx)
LBB43_4:                                ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev.exit
	movq	%rbx, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
LBB43_5:
Ltmp501:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end23:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table43:
Lexception23:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	21                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	13                      ## Call site table length
Lset369 = Ltmp499-Lfunc_begin23         ## >> Call Site 1 <<
	.long	Lset369
Lset370 = Ltmp500-Ltmp499               ##   Call between Ltmp499 and Ltmp500
	.long	Lset370
Lset371 = Ltmp501-Lfunc_begin23         ##     jumps to Ltmp501
	.long	Lset371
	.byte	1                       ##   On action: 1
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost17bad_function_callD0Ev
	.weak_def_can_be_hidden	__ZN5boost17bad_function_callD0Ev
	.align	4, 0x90
__ZN5boost17bad_function_callD0Ev:      ## @_ZN5boost17bad_function_callD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp506:
	.cfi_def_cfa_offset 16
Ltmp507:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp508:
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
Ltmp509:
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__ZdlPv                 ## TAILCALL
	.cfi_endproc

	.globl	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE
	.weak_def_can_be_hidden	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE
	.align	4, 0x90
__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE: ## @_ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEC1ERKS5_NS5_9clone_tagE
Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception24
## BB#0:
	pushq	%rbp
Ltmp540:
	.cfi_def_cfa_offset 16
Ltmp541:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp542:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	pushq	%rax
Ltmp543:
	.cfi_offset %rbx, -56
Ltmp544:
	.cfi_offset %r12, -48
Ltmp545:
	.cfi_offset %r13, -40
Ltmp546:
	.cfi_offset %r14, -32
Ltmp547:
	.cfi_offset %r15, -24
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	__ZTVN5boost16exception_detail10clone_baseE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, 56(%r14)
	callq	__ZNSt13runtime_errorC2ERKS_
	movq	__ZTVN5boost17bad_function_callE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rax, (%r14)
	movq	__ZTVN5boost9exceptionE@GOTPCREL(%rip), %r13
	addq	$16, %r13
	movq	%r13, 16(%r14)
	movq	24(%rbx), %rdi
	movq	%rdi, 24(%r14)
	testq	%rdi, %rdi
	je	LBB45_2
## BB#1:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
Ltmp510:
	callq	*%rax
Ltmp511:
LBB45_2:
	movl	48(%rbx), %eax
	movl	%eax, 48(%r14)
	movq	32(%rbx), %rax
	movq	40(%rbx), %rcx
	movq	%rcx, 40(%r14)
	movq	%rax, 32(%r14)
	movq	__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE@GOTPCREL(%rip), %rax
	leaq	24(%rax), %rcx
	movq	%rcx, (%r14)
	leaq	80(%rax), %rcx
	movq	%rcx, 16(%r14)
	addq	$136, %rax
	movq	%rax, 56(%r14)
	movq	24(%rbx), %rsi
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	LBB45_11
## BB#3:
	movq	(%rsi), %rax
	movq	40(%rax), %rax
Ltmp513:
	leaq	-48(%rbp), %rdi
	callq	*%rax
Ltmp514:
## BB#4:                                ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEE7releaseEv.exit.i.i.i
	movq	-48(%rbp), %r12
	xorl	%r15d, %r15d
	testq	%r12, %r12
	je	LBB45_11
## BB#5:
	movq	(%r12), %rax
	movq	24(%rax), %rax
Ltmp516:
	movq	%r12, %rdi
	callq	*%rax
Ltmp517:
## BB#6:                                ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEEaSERKS3_.exit.i
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	LBB45_10
## BB#7:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp522:
	callq	*%rax
Ltmp523:
## BB#8:                                ## %.noexc.i.i.3.i
	testb	%al, %al
	je	LBB45_10
## BB#9:
	movq	$0, -48(%rbp)
LBB45_10:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit4.i
	movq	%r12, %r15
LBB45_11:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit4.i
	movl	48(%rbx), %eax
	movl	%eax, 48(%r14)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 32(%r14)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	LBB45_15
## BB#12:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp525:
	callq	*%rax
Ltmp526:
## BB#13:                               ## %.noexc8.i
	testb	%al, %al
	je	LBB45_15
## BB#14:
	movq	$0, 24(%r14)
LBB45_15:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEE7releaseEv.exit.i.i.7.i
	movq	%r15, 24(%r14)
	testq	%r15, %r15
	je	LBB45_18
## BB#16:
	movq	(%r15), %rax
	movq	24(%rax), %rax
Ltmp528:
	movq	%r15, %rdi
	callq	*%rax
Ltmp529:
## BB#17:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEEaSERKS3_.exit10.i
	movq	(%r15), %rax
	movq	32(%rax), %rax
Ltmp537:
	movq	%r15, %rdi
	callq	*%rax
Ltmp538:
LBB45_18:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB45_37:                               ## %.body
Ltmp512:
	movq	%rax, %rbx
	jmp	LBB45_38
LBB45_31:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread18.i
Ltmp515:
	movq	%rax, %rbx
	jmp	LBB45_32
LBB45_28:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.i
Ltmp527:
	movq	%rax, %rbx
	testq	%r15, %r15
	je	LBB45_32
	jmp	LBB45_29
LBB45_39:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread19.i
Ltmp530:
	movq	%rax, %rbx
	jmp	LBB45_29
LBB45_27:
Ltmp539:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB45_20:
Ltmp518:
	movq	%rax, %rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	jne	LBB45_22
## BB#21:
	movq	%r12, %r15
	jmp	LBB45_29
LBB45_19:
Ltmp524:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB45_22:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp519:
	callq	*%rax
Ltmp520:
## BB#23:                               ## %.noexc.i.i.5.i
	testb	%al, %al
	je	LBB45_24
## BB#25:
	movq	$0, -48(%rbp)
	movq	%r12, %r15
	jmp	LBB45_29
LBB45_26:
Ltmp521:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB45_24:
	movq	%r12, %r15
LBB45_29:                               ## %_ZN5boost16exception_detail12refcount_ptrINS0_20error_info_containerEED1Ev.exit6.thread.i
	movq	(%r15), %rax
	movq	32(%rax), %rax
Ltmp531:
	movq	%r15, %rdi
	callq	*%rax
Ltmp532:
LBB45_32:                               ## %.body2
	movq	%r13, 16(%r14)
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	LBB45_38
## BB#33:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
Ltmp534:
	callq	*%rax
Ltmp535:
## BB#34:                               ## %.noexc.i.i.i.i
	testb	%al, %al
	je	LBB45_38
## BB#35:
	movq	$0, 24(%r14)
LBB45_38:                               ## %_ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED2Ev.exit
	movq	%r14, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB45_30:
Ltmp533:
	movq	%rax, %rdi
	callq	___clang_call_terminate
LBB45_36:
Ltmp536:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end24:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table45:
Lexception24:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.ascii	"\230\001"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\217\001"              ## Call site table length
Lset372 = Ltmp510-Lfunc_begin24         ## >> Call Site 1 <<
	.long	Lset372
Lset373 = Ltmp511-Ltmp510               ##   Call between Ltmp510 and Ltmp511
	.long	Lset373
Lset374 = Ltmp512-Lfunc_begin24         ##     jumps to Ltmp512
	.long	Lset374
	.byte	0                       ##   On action: cleanup
Lset375 = Ltmp513-Lfunc_begin24         ## >> Call Site 2 <<
	.long	Lset375
Lset376 = Ltmp514-Ltmp513               ##   Call between Ltmp513 and Ltmp514
	.long	Lset376
Lset377 = Ltmp515-Lfunc_begin24         ##     jumps to Ltmp515
	.long	Lset377
	.byte	0                       ##   On action: cleanup
Lset378 = Ltmp516-Lfunc_begin24         ## >> Call Site 3 <<
	.long	Lset378
Lset379 = Ltmp517-Ltmp516               ##   Call between Ltmp516 and Ltmp517
	.long	Lset379
Lset380 = Ltmp518-Lfunc_begin24         ##     jumps to Ltmp518
	.long	Lset380
	.byte	0                       ##   On action: cleanup
Lset381 = Ltmp522-Lfunc_begin24         ## >> Call Site 4 <<
	.long	Lset381
Lset382 = Ltmp523-Ltmp522               ##   Call between Ltmp522 and Ltmp523
	.long	Lset382
Lset383 = Ltmp524-Lfunc_begin24         ##     jumps to Ltmp524
	.long	Lset383
	.byte	1                       ##   On action: 1
Lset384 = Ltmp525-Lfunc_begin24         ## >> Call Site 5 <<
	.long	Lset384
Lset385 = Ltmp526-Ltmp525               ##   Call between Ltmp525 and Ltmp526
	.long	Lset385
Lset386 = Ltmp527-Lfunc_begin24         ##     jumps to Ltmp527
	.long	Lset386
	.byte	0                       ##   On action: cleanup
Lset387 = Ltmp528-Lfunc_begin24         ## >> Call Site 6 <<
	.long	Lset387
Lset388 = Ltmp529-Ltmp528               ##   Call between Ltmp528 and Ltmp529
	.long	Lset388
Lset389 = Ltmp530-Lfunc_begin24         ##     jumps to Ltmp530
	.long	Lset389
	.byte	0                       ##   On action: cleanup
Lset390 = Ltmp537-Lfunc_begin24         ## >> Call Site 7 <<
	.long	Lset390
Lset391 = Ltmp538-Ltmp537               ##   Call between Ltmp537 and Ltmp538
	.long	Lset391
Lset392 = Ltmp539-Lfunc_begin24         ##     jumps to Ltmp539
	.long	Lset392
	.byte	1                       ##   On action: 1
Lset393 = Ltmp519-Lfunc_begin24         ## >> Call Site 8 <<
	.long	Lset393
Lset394 = Ltmp520-Ltmp519               ##   Call between Ltmp519 and Ltmp520
	.long	Lset394
Lset395 = Ltmp521-Lfunc_begin24         ##     jumps to Ltmp521
	.long	Lset395
	.byte	1                       ##   On action: 1
Lset396 = Ltmp531-Lfunc_begin24         ## >> Call Site 9 <<
	.long	Lset396
Lset397 = Ltmp532-Ltmp531               ##   Call between Ltmp531 and Ltmp532
	.long	Lset397
Lset398 = Ltmp533-Lfunc_begin24         ##     jumps to Ltmp533
	.long	Lset398
	.byte	1                       ##   On action: 1
Lset399 = Ltmp534-Lfunc_begin24         ## >> Call Site 10 <<
	.long	Lset399
Lset400 = Ltmp535-Ltmp534               ##   Call between Ltmp534 and Ltmp535
	.long	Lset400
Lset401 = Ltmp536-Lfunc_begin24         ##     jumps to Ltmp536
	.long	Lset401
	.byte	1                       ##   On action: 1
Lset402 = Ltmp535-Lfunc_begin24         ## >> Call Site 11 <<
	.long	Lset402
Lset403 = Lfunc_end24-Ltmp535           ##   Call between Ltmp535 and Lfunc_end24
	.long	Lset403
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__const
__ZN5boost6spiritL6unusedE:             ## @_ZN5boost6spiritL6unusedE
	.space	1

.zerofill __DATA,__bss,__ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p,64,3 ## @_ZZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p
.zerofill __DATA,__bss,__ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p,8,3 ## @_ZGVZ5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE1p
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"unnamed-rule"

L___func__._Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE: ## @__func__._Z5parseRKNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.asciz	"parse"

L_.str.63:                              ## @.str.63
	.asciz	"./spirit.cpp"

L_.str.64:                              ## @.str.64
	.asciz	"parse(f,l,p,a)"

L_.str.65:                              ## @.str.65
	.asciz	"42"

L___func__.main:                        ## @__func__.main
	.asciz	"main"

L_.str.66:                              ## @.str.66
	.asciz	"0 == parse(\"42\").which()"

L_.str.67:                              ## @.str.67
	.asciz	"-42"

L_.str.68:                              ## @.str.68
	.asciz	"0 == parse(\"-42\").which()"

L_.str.69:                              ## @.str.69
	.asciz	"+42"

L_.str.70:                              ## @.str.70
	.asciz	"0 == parse(\"+42\").which()"

L_.str.71:                              ## @.str.71
	.asciz	"42."

L_.str.72:                              ## @.str.72
	.asciz	"1 == parse(\"42.\").which()"

L_.str.73:                              ## @.str.73
	.asciz	"0."

L_.str.74:                              ## @.str.74
	.asciz	"1 == parse(\"0.\").which()"

L_.str.75:                              ## @.str.75
	.asciz	".0"

L_.str.76:                              ## @.str.76
	.asciz	"1 == parse(\".0\").which()"

L_.str.77:                              ## @.str.77
	.asciz	"0.0"

L_.str.78:                              ## @.str.78
	.asciz	"1 == parse(\"0.0\").which()"

L_.str.79:                              ## @.str.79
	.asciz	"1e1"

L_.str.80:                              ## @.str.80
	.asciz	"1 == parse(\"1e1\").which()"

L_.str.81:                              ## @.str.81
	.asciz	"1e+1"

L_.str.82:                              ## @.str.82
	.asciz	"1 == parse(\"1e+1\").which()"

L_.str.83:                              ## @.str.83
	.asciz	"1e-1"

L_.str.84:                              ## @.str.84
	.asciz	"1 == parse(\"1e-1\").which()"

L_.str.85:                              ## @.str.85
	.asciz	"-1e1"

L_.str.86:                              ## @.str.86
	.asciz	"1 == parse(\"-1e1\").which()"

L_.str.87:                              ## @.str.87
	.asciz	"-1e+1"

L_.str.88:                              ## @.str.88
	.asciz	"1 == parse(\"-1e+1\").which()"

L_.str.89:                              ## @.str.89
	.asciz	"-1e-1"

L_.str.90:                              ## @.str.90
	.asciz	"1 == parse(\"-1e-1\").which()"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE9assign_toINS9_2qi6detail13parser_binderINSR_11alternativeINSC_INSR_15any_real_parserIdNSR_20strict_real_policiesIdEEEENSC_INSR_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEEvT_E13stored_vtable ## @_ZZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE9assign_toINS9_2qi6detail13parser_binderINSR_11alternativeINSC_INSR_15any_real_parserIdNSR_20strict_real_policiesIdEEEENSC_INSR_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEEvT_E13stored_vtable
	.weak_definition	__ZZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE9assign_toINS9_2qi6detail13parser_binderINSR_11alternativeINSC_INSR_15any_real_parserIdNSR_20strict_real_policiesIdEEEENSC_INSR_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEEvT_E13stored_vtable
	.align	3
__ZZN5boost9function4IbRNSt3__111__wrap_iterIPKcEERKS5_RNS_6spirit7contextINS_6fusion4consIRNS_7variantIiJdEEENSB_4nil_EEENSB_6vectorIJEEEEERKNS9_11unused_typeEE9assign_toINS9_2qi6detail13parser_binderINSR_11alternativeINSC_INSR_15any_real_parserIdNSR_20strict_real_policiesIdEEEENSC_INSR_14any_int_parserIiLj10ELj1ELin1EEESG_EEEEEEN4mpl_5bool_ILb0EEEEEEEvT_E13stored_vtable:
	.quad	__ZN5boost6detail8function15functor_managerINS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEE6manageERKNS1_15function_bufferERSP_NS1_30functor_manager_operation_typeE
	.quad	__ZN5boost6detail8function21function_obj_invoker4INS_6spirit2qi6detail13parser_binderINS4_11alternativeINS_6fusion4consINS4_15any_real_parserIdNS4_20strict_real_policiesIdEEEENS9_INS4_14any_int_parserIiLj10ELj1ELin1EEENS8_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEEbRNSt3__111__wrap_iterIPKcEERKSS_RNS3_7contextINS9_IRNS_7variantIiJdEEESG_EENS8_6vectorIJEEEEERKNS3_11unused_typeEE6invokeERNS1_15function_bufferEST_SV_S14_S17_

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE ## @_ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE
	.weak_definition	__ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE
	.align	4
__ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE:
	.asciz	"N5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE ## @_ZTIN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE
	.weak_definition	__ZTIN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE
	.align	3
__ZTIN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost6spirit2qi6detail13parser_binderINS1_11alternativeINS_6fusion4consINS1_15any_real_parserIdNS1_20strict_real_policiesIdEEEENS6_INS1_14any_int_parserIiLj10ELj1ELin1EEENS5_4nil_EEEEEEEN4mpl_5bool_ILb0EEEEE

	.section	__TEXT,__cstring,cstring_literals
L___func__._ZN5boost6detail7variant13forced_returnIvEET_v: ## @__func__._ZN5boost6detail7variant13forced_returnIvEET_v
	.asciz	"forced_return"

L_.str.99:                              ## @.str.99
	.asciz	"/Users/rhodges/local/include/boost/variant/detail/forced_return.hpp"

L_.str.100:                             ## @.str.100
	.asciz	"false"

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE ## @_ZTSN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.weak_definition	__ZTSN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.align	4
__ZTSN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE:
	.asciz	"N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE"

	.globl	__ZTSN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE ## @_ZTSN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.weak_definition	__ZTSN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.align	4
__ZTSN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE:
	.asciz	"N5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE"

	.globl	__ZTSN5boost17bad_function_callE ## @_ZTSN5boost17bad_function_callE
	.weak_definition	__ZTSN5boost17bad_function_callE
	.align	4
__ZTSN5boost17bad_function_callE:
	.asciz	"N5boost17bad_function_callE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost17bad_function_callE ## @_ZTIN5boost17bad_function_callE
	.weak_definition	__ZTIN5boost17bad_function_callE
	.align	4
__ZTIN5boost17bad_function_callE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSN5boost17bad_function_callE
	.quad	__ZTISt13runtime_error

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost9exceptionE ## @_ZTSN5boost9exceptionE
	.weak_definition	__ZTSN5boost9exceptionE
	.align	4
__ZTSN5boost9exceptionE:
	.asciz	"N5boost9exceptionE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost9exceptionE ## @_ZTIN5boost9exceptionE
	.weak_definition	__ZTIN5boost9exceptionE
	.align	3
__ZTIN5boost9exceptionE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost9exceptionE

	.globl	__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE ## @_ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.weak_definition	__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.align	4
__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTSN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTIN5boost17bad_function_callE
	.quad	2                       ## 0x2
	.quad	__ZTIN5boost9exceptionE
	.quad	4098                    ## 0x1002

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost16exception_detail10clone_baseE ## @_ZTSN5boost16exception_detail10clone_baseE
	.weak_definition	__ZTSN5boost16exception_detail10clone_baseE
	.align	4
__ZTSN5boost16exception_detail10clone_baseE:
	.asciz	"N5boost16exception_detail10clone_baseE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost16exception_detail10clone_baseE ## @_ZTIN5boost16exception_detail10clone_baseE
	.weak_definition	__ZTIN5boost16exception_detail10clone_baseE
	.align	3
__ZTIN5boost16exception_detail10clone_baseE:
	.quad	__ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	__ZTSN5boost16exception_detail10clone_baseE

	.globl	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE ## @_ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.weak_definition	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.align	4
__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE:
	.quad	__ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	__ZTSN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.long	0                       ## 0x0
	.long	2                       ## 0x2
	.quad	__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.quad	2                       ## 0x2
	.quad	__ZTIN5boost16exception_detail10clone_baseE
	.quad	-6141                   ## 0xffffffffffffe803

	.globl	__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE ## @_ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.weak_def_can_be_hidden	__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.align	3
__ZTVN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE:
	.quad	56
	.quad	0
	.quad	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.quad	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.quad	__ZN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.quad	__ZNKSt13runtime_error4whatEv
	.quad	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.quad	__ZNK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.quad	-16
	.quad	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.quad	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.quad	__ZThn16_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev
	.quad	-56
	.quad	-56
	.quad	-56
	.quad	-56
	.quad	__ZTIN5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEEE
	.quad	__ZTv0_n24_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE5cloneEv
	.quad	__ZTv0_n32_NK5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEE7rethrowEv
	.quad	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED1Ev
	.quad	__ZTv0_n40_N5boost16exception_detail10clone_implINS0_19error_info_injectorINS_17bad_function_callEEEED0Ev

	.globl	__ZTVN5boost16exception_detail10clone_baseE ## @_ZTVN5boost16exception_detail10clone_baseE
	.weak_def_can_be_hidden	__ZTVN5boost16exception_detail10clone_baseE
	.align	3
__ZTVN5boost16exception_detail10clone_baseE:
	.quad	0
	.quad	__ZTIN5boost16exception_detail10clone_baseE
	.quad	___cxa_pure_virtual
	.quad	___cxa_pure_virtual
	.quad	__ZN5boost16exception_detail10clone_baseD1Ev
	.quad	__ZN5boost16exception_detail10clone_baseD0Ev

	.globl	__ZTVN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE ## @_ZTVN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.weak_def_can_be_hidden	__ZTVN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.align	3
__ZTVN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE:
	.quad	0
	.quad	__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.quad	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.quad	__ZN5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev
	.quad	__ZNKSt13runtime_error4whatEv
	.quad	-16
	.quad	__ZTIN5boost16exception_detail19error_info_injectorINS_17bad_function_callEEE
	.quad	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED1Ev
	.quad	__ZThn16_N5boost16exception_detail19error_info_injectorINS_17bad_function_callEED0Ev

	.globl	__ZTVN5boost17bad_function_callE ## @_ZTVN5boost17bad_function_callE
	.weak_def_can_be_hidden	__ZTVN5boost17bad_function_callE
	.align	3
__ZTVN5boost17bad_function_callE:
	.quad	0
	.quad	__ZTIN5boost17bad_function_callE
	.quad	__ZN5boost17bad_function_callD1Ev
	.quad	__ZN5boost17bad_function_callD0Ev
	.quad	__ZNKSt13runtime_error4whatEv

	.globl	__ZTVN5boost9exceptionE ## @_ZTVN5boost9exceptionE
	.weak_def_can_be_hidden	__ZTVN5boost9exceptionE
	.align	3
__ZTVN5boost9exceptionE:
	.quad	0
	.quad	__ZTIN5boost9exceptionE
	.quad	___cxa_pure_virtual
	.quad	___cxa_pure_virtual

	.section	__TEXT,__cstring,cstring_literals
L_.str.101:                             ## @.str.101
	.asciz	"call to empty boost::function"


.subsections_via_symbols
