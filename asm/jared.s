	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
___cxx_global_var_init:                 ## @__cxx_global_var_init
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6system16generic_categoryEv
	movq	%rax, __ZN5boost6systemL14posix_categoryE(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.align	4, 0x90
___cxx_global_var_init.1:               ## @__cxx_global_var_init.1
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6system16generic_categoryEv
	movq	%rax, __ZN5boost6systemL10errno_ecatE(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.align	4, 0x90
___cxx_global_var_init.2:               ## @__cxx_global_var_init.2
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	callq	__ZN5boost6system15system_categoryEv
	movq	%rax, __ZN5boost6systemL11native_ecatE(%rip)
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK5boost6system12system_error4whatEv
	.weak_def_can_be_hidden	__ZNK5boost6system12system_error4whatEv
	.align	4, 0x90
__ZNK5boost6system12system_error4whatEv: ## @_ZNK5boost6system12system_error4whatEv
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp22:
	.cfi_def_cfa_offset 16
Ltmp23:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp24:
	.cfi_def_cfa_register %rbp
	subq	$768, %rsp              ## imm = 0x300
	movq	%rdi, -568(%rbp)
	movq	-568(%rbp), %rdi
	movq	%rdi, %rax
	addq	$32, %rax
	movq	%rax, -552(%rbp)
	movq	-552(%rbp), %rax
	movq	%rax, -544(%rbp)
	movq	-544(%rbp), %rax
	movq	%rax, -536(%rbp)
	movq	-536(%rbp), %rcx
	movq	%rcx, -528(%rbp)
	movq	-528(%rbp), %rcx
	movq	%rcx, -520(%rbp)
	movq	-520(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rdi, -616(%rbp)        ## 8-byte Spill
	movq	%rax, -624(%rbp)        ## 8-byte Spill
	je	LBB3_2
## BB#1:
	movq	-624(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -488(%rbp)
	movq	-488(%rbp), %rcx
	movq	%rcx, -480(%rbp)
	movq	-480(%rbp), %rcx
	movq	%rcx, -472(%rbp)
	movq	-472(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -632(%rbp)        ## 8-byte Spill
	jmp	LBB3_3
LBB3_2:
	movq	-624(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -512(%rbp)
	movq	-512(%rbp), %rcx
	movq	%rcx, -504(%rbp)
	movq	-504(%rbp), %rcx
	movq	%rcx, -496(%rbp)
	movq	-496(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -632(%rbp)        ## 8-byte Spill
LBB3_3:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv.exit
	movq	-632(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, %rax
	jne	LBB3_30
## BB#4:
	movq	-616(%rbp), %rax        ## 8-byte Reload
	addq	$32, %rax
	movq	-616(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, -640(%rbp)        ## 8-byte Spill
	callq	__ZNKSt13runtime_error4whatEv
	movq	-640(%rbp), %rdi        ## 8-byte Reload
	movq	%rdi, -112(%rbp)
	movq	%rax, -120(%rbp)
	movq	-112(%rbp), %rdi
Ltmp9:
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6assignEPKc
Ltmp10:
	movq	%rax, -648(%rbp)        ## 8-byte Spill
	jmp	LBB3_5
LBB3_5:                                 ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEaSEPKc.exit
	jmp	LBB3_6
LBB3_6:
	movq	-616(%rbp), %rax        ## 8-byte Reload
	addq	$32, %rax
	movq	%rax, -104(%rbp)
	movq	-104(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -656(%rbp)        ## 8-byte Spill
	je	LBB3_8
## BB#7:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -664(%rbp)        ## 8-byte Spill
	jmp	LBB3_9
LBB3_8:
	movq	-656(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -664(%rbp)        ## 8-byte Spill
LBB3_9:                                 ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5emptyEv.exit1
	movq	-664(%rbp), %rax        ## 8-byte Reload
	cmpq	$0, %rax
	je	LBB3_14
## BB#10:
	movq	-616(%rbp), %rax        ## 8-byte Reload
	addq	$32, %rax
	movq	%rax, -8(%rbp)
	leaq	L_.str(%rip), %rax
	movq	%rax, -16(%rbp)
	movq	-8(%rbp), %rdi
Ltmp11:
	movq	%rax, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKc
Ltmp12:
	movq	%rax, -672(%rbp)        ## 8-byte Spill
	jmp	LBB3_11
LBB3_11:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLEPKc.exit
	jmp	LBB3_12
LBB3_12:
	jmp	LBB3_14
LBB3_13:
Ltmp15:
	movl	%edx, %ecx
	movq	%rax, -576(%rbp)
	movl	%ecx, -580(%rbp)
	jmp	LBB3_25
LBB3_14:
	movq	-616(%rbp), %rax        ## 8-byte Reload
	addq	$32, %rax
	movq	-616(%rbp), %rcx        ## 8-byte Reload
	addq	$16, %rcx
Ltmp13:
	leaq	-608(%rbp), %rdi
	movq	%rcx, %rsi
	movq	%rax, -680(%rbp)        ## 8-byte Spill
	callq	__ZNK5boost6system10error_code7messageEv
Ltmp14:
	jmp	LBB3_15
LBB3_15:
	leaq	-608(%rbp), %rax
	movq	-680(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -336(%rbp)
	movq	%rax, -344(%rbp)
	movq	-336(%rbp), %rax
	movq	-344(%rbp), %rdx
	movq	%rax, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	-320(%rbp), %rdi
	movq	-328(%rbp), %rax
	movq	%rax, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rdx
	movq	%rdx, -288(%rbp)
	movq	-288(%rbp), %rdx
	movq	%rdx, -280(%rbp)
	movq	-280(%rbp), %rdx
	movzbl	(%rdx), %esi
	andl	$1, %esi
	cmpl	$0, %esi
	movq	%rdi, -688(%rbp)        ## 8-byte Spill
	movq	%rax, -696(%rbp)        ## 8-byte Spill
	je	LBB3_17
## BB#16:
	movq	-696(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -232(%rbp)
	movq	-232(%rbp), %rcx
	movq	%rcx, -224(%rbp)
	movq	-224(%rbp), %rcx
	movq	%rcx, -216(%rbp)
	movq	-216(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
	jmp	LBB3_18
LBB3_17:
	movq	-696(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -272(%rbp)
	movq	-272(%rbp), %rcx
	movq	%rcx, -264(%rbp)
	movq	-264(%rbp), %rcx
	movq	%rcx, -256(%rbp)
	movq	-256(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -248(%rbp)
	movq	-248(%rbp), %rcx
	movq	%rcx, -240(%rbp)
	movq	-240(%rbp), %rcx
	movq	%rcx, -704(%rbp)        ## 8-byte Spill
LBB3_18:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit.i.i
	movq	-704(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rsi
	movq	-328(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-200(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rcx, -184(%rbp)
	movq	-184(%rbp), %rcx
	movq	%rcx, -176(%rbp)
	movq	-176(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -712(%rbp)        ## 8-byte Spill
	movq	%rax, -720(%rbp)        ## 8-byte Spill
	je	LBB3_20
## BB#19:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
	jmp	LBB3_21
LBB3_20:
	movq	-720(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rcx
	movq	%rcx, -160(%rbp)
	movq	-160(%rbp), %rcx
	movq	%rcx, -152(%rbp)
	movq	-152(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -728(%rbp)        ## 8-byte Spill
LBB3_21:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendERKS5_.exit.i
Ltmp16:
	movq	-728(%rbp), %rax        ## 8-byte Reload
	movq	-688(%rbp), %rdi        ## 8-byte Reload
	movq	-712(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEPKcm
Ltmp17:
	movq	%rax, -736(%rbp)        ## 8-byte Spill
	jmp	LBB3_22
LBB3_22:                                ## %_ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEpLERKS5_.exit
	jmp	LBB3_23
LBB3_23:
	leaq	-608(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB3_29
LBB3_24:
Ltmp18:
	leaq	-608(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -576(%rbp)
	movl	%ecx, -580(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
LBB3_25:
	movq	-576(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-616(%rbp), %rdi        ## 8-byte Reload
	movq	%rax, -744(%rbp)        ## 8-byte Spill
	callq	__ZNKSt13runtime_error4whatEv
	movq	%rax, -560(%rbp)
Ltmp19:
	callq	___cxa_end_catch
Ltmp20:
	jmp	LBB3_26
LBB3_26:
	jmp	LBB3_34
LBB3_27:
Ltmp21:
	movl	%edx, %ecx
	movq	%rax, -576(%rbp)
	movl	%ecx, -580(%rbp)
## BB#28:
	movq	-576(%rbp), %rdi
	callq	___cxa_call_unexpected
LBB3_29:
	jmp	LBB3_30
LBB3_30:
	movq	-616(%rbp), %rax        ## 8-byte Reload
	addq	$32, %rax
	movq	%rax, -464(%rbp)
	movq	-464(%rbp), %rax
	movq	%rax, -456(%rbp)
	movq	-456(%rbp), %rax
	movq	%rax, -448(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -440(%rbp)
	movq	-440(%rbp), %rcx
	movq	%rcx, -432(%rbp)
	movq	-432(%rbp), %rcx
	movq	%rcx, -424(%rbp)
	movq	-424(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rax, -752(%rbp)        ## 8-byte Spill
	je	LBB3_32
## BB#31:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -376(%rbp)
	movq	-376(%rbp), %rcx
	movq	%rcx, -368(%rbp)
	movq	-368(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	movq	-360(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
	jmp	LBB3_33
LBB3_32:
	movq	-752(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -416(%rbp)
	movq	-416(%rbp), %rcx
	movq	%rcx, -408(%rbp)
	movq	-408(%rbp), %rcx
	movq	%rcx, -400(%rbp)
	movq	-400(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -392(%rbp)
	movq	-392(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	movq	-384(%rbp), %rcx
	movq	%rcx, -760(%rbp)        ## 8-byte Spill
LBB3_33:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE5c_strEv.exit
	movq	-760(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -352(%rbp)
	movq	-352(%rbp), %rax
	movq	%rax, -560(%rbp)
LBB3_34:
	movq	-560(%rbp), %rax
	addq	$768, %rsp              ## imm = 0x300
	popq	%rbp
	retq
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table3:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\313\200"              ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	65                      ## Call site table length
Lset0 = Ltmp9-Lfunc_begin0              ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp14-Ltmp9                    ##   Call between Ltmp9 and Ltmp14
	.long	Lset1
Lset2 = Ltmp15-Lfunc_begin0             ##     jumps to Ltmp15
	.long	Lset2
	.byte	3                       ##   On action: 2
Lset3 = Ltmp16-Lfunc_begin0             ## >> Call Site 2 <<
	.long	Lset3
Lset4 = Ltmp17-Ltmp16                   ##   Call between Ltmp16 and Ltmp17
	.long	Lset4
Lset5 = Ltmp18-Lfunc_begin0             ##     jumps to Ltmp18
	.long	Lset5
	.byte	3                       ##   On action: 2
Lset6 = Ltmp17-Lfunc_begin0             ## >> Call Site 3 <<
	.long	Lset6
Lset7 = Ltmp19-Ltmp17                   ##   Call between Ltmp17 and Ltmp19
	.long	Lset7
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp19-Lfunc_begin0             ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp20-Ltmp19                   ##   Call between Ltmp19 and Ltmp20
	.long	Lset9
Lset10 = Ltmp21-Lfunc_begin0            ##     jumps to Ltmp21
	.long	Lset10
	.byte	1                       ##   On action: 1
Lset11 = Ltmp20-Lfunc_begin0            ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Lfunc_end0-Ltmp20              ##   Call between Ltmp20 and Lfunc_end0
	.long	Lset12
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	127                     ## >> Action Record 1 <<
                                        ##   Filter TypeInfo -1
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
                                        ## >> Filter TypeInfos <<
	.byte	0
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNK5boost6system10error_code7messageEv
	.weak_def_can_be_hidden	__ZNK5boost6system10error_code7messageEv
	.align	4, 0x90
__ZNK5boost6system10error_code7messageEv: ## @_ZNK5boost6system10error_code7messageEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp25:
	.cfi_def_cfa_offset 16
Ltmp26:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp27:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, %rax
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	8(%rsi), %rcx
	movq	(%rcx), %rdx
	movq	24(%rdx), %rdx
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rsi, %rdi
	movq	%rax, -24(%rbp)         ## 8-byte Spill
	movq	%rcx, -32(%rbp)         ## 8-byte Spill
	movq	%rdx, -40(%rbp)         ## 8-byte Spill
	callq	__ZNK5boost6system10error_code5valueEv
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	movq	-32(%rbp), %rsi         ## 8-byte Reload
	movl	%eax, %edx
	movq	-40(%rbp), %rcx         ## 8-byte Reload
	callq	*%rcx
	movq	-24(%rbp), %rax         ## 8-byte Reload
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp31:
	.cfi_def_cfa_offset 16
Ltmp32:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp33:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	leaq	L_.str.3(%rip), %rsi
	leaq	-24(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -48(%rbp)         ## 8-byte Spill
	callq	__ZN5boost10filesystem4pathC1EPKc
Ltmp28:
	movq	-48(%rbp), %rdi         ## 8-byte Reload
	callq	__ZN5boost10filesystem16create_directoryERKNS0_4pathE
Ltmp29:
	movb	%al, -49(%rbp)          ## 1-byte Spill
	jmp	LBB5_1
LBB5_1:
	leaq	-24(%rbp), %rdi
	callq	__ZN5boost10filesystem4pathD1Ev
	xorl	%eax, %eax
	addq	$64, %rsp
	popq	%rbp
	retq
LBB5_2:
Ltmp30:
	leaq	-24(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -32(%rbp)
	movl	%ecx, -36(%rbp)
	callq	__ZN5boost10filesystem4pathD1Ev
## BB#3:
	movq	-32(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset13 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset13
Lset14 = Ltmp28-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp28
	.long	Lset14
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp28-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset15
Lset16 = Ltmp29-Ltmp28                  ##   Call between Ltmp28 and Ltmp29
	.long	Lset16
Lset17 = Ltmp30-Lfunc_begin1            ##     jumps to Ltmp30
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp29-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset18
Lset19 = Lfunc_end1-Ltmp29              ##   Call between Ltmp29 and Lfunc_end1
	.long	Lset19
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN5boost10filesystem4pathC1EPKc
	.weak_def_can_be_hidden	__ZN5boost10filesystem4pathC1EPKc
	.align	4, 0x90
__ZN5boost10filesystem4pathC1EPKc:      ## @_ZN5boost10filesystem4pathC1EPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp34:
	.cfi_def_cfa_offset 16
Ltmp35:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp36:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN5boost10filesystem4pathC2EPKc
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10filesystem16create_directoryERKNS0_4pathE
	.weak_def_can_be_hidden	__ZN5boost10filesystem16create_directoryERKNS0_4pathE
	.align	4, 0x90
__ZN5boost10filesystem16create_directoryERKNS0_4pathE: ## @_ZN5boost10filesystem16create_directoryERKNS0_4pathE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp37:
	.cfi_def_cfa_offset 16
Ltmp38:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp39:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	xorl	%eax, %eax
	movl	%eax, %esi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost10filesystem6detail16create_directoryERKNS0_4pathEPNS_6system10error_codeE
	andb	$1, %al
	movzbl	%al, %eax
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10filesystem4pathD1Ev
	.weak_def_can_be_hidden	__ZN5boost10filesystem4pathD1Ev
	.align	4, 0x90
__ZN5boost10filesystem4pathD1Ev:        ## @_ZN5boost10filesystem4pathD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp40:
	.cfi_def_cfa_offset 16
Ltmp41:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp42:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost10filesystem4pathD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6system12system_errorD1Ev
	.weak_def_can_be_hidden	__ZN5boost6system12system_errorD1Ev
	.align	4, 0x90
__ZN5boost6system12system_errorD1Ev:    ## @_ZN5boost6system12system_errorD1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp43:
	.cfi_def_cfa_offset 16
Ltmp44:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp45:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN5boost6system12system_errorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6system12system_errorD0Ev
	.weak_def_can_be_hidden	__ZN5boost6system12system_errorD0Ev
	.align	4, 0x90
__ZN5boost6system12system_errorD0Ev:    ## @_ZN5boost6system12system_errorD0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp46:
	.cfi_def_cfa_offset 16
Ltmp47:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp48:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZN5boost6system12system_errorD1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK5boost6system10error_code5valueEv
	.weak_def_can_be_hidden	__ZNK5boost6system10error_code5valueEv
	.align	4, 0x90
__ZNK5boost6system10error_code5valueEv: ## @_ZNK5boost6system10error_code5valueEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp49:
	.cfi_def_cfa_offset 16
Ltmp50:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp51:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movl	(%rdi), %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10filesystem4pathC2EPKc
	.weak_def_can_be_hidden	__ZN5boost10filesystem4pathC2EPKc
	.align	4, 0x90
__ZN5boost10filesystem4pathC2EPKc:      ## @_ZN5boost10filesystem4pathC2EPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp52:
	.cfi_def_cfa_offset 16
Ltmp53:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp54:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	xorl	%eax, %eax
	movl	$24, %ecx
	movl	%ecx, %edx
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rsi, -56(%rbp)
	movq	%rdi, -64(%rbp)
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdi
	movq	%rsi, -40(%rbp)
	movq	%rdi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	%rsi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, %r8
	movq	%r8, -8(%rbp)
	movq	%rsi, -88(%rbp)         ## 8-byte Spill
	movl	%eax, %esi
	callq	_memset
	movq	-48(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	%rsi, -96(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
	movq	-88(%rbp), %rdi         ## 8-byte Reload
	movq	-96(%rbp), %rsi         ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
	addq	$96, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp55:
	.cfi_def_cfa_offset 16
Ltmp56:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp57:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost10filesystem4pathD2Ev
	.weak_def_can_be_hidden	__ZN5boost10filesystem4pathD2Ev
	.align	4, 0x90
__ZN5boost10filesystem4pathD2Ev:        ## @_ZN5boost10filesystem4pathD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp58:
	.cfi_def_cfa_offset 16
Ltmp59:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp60:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN5boost6system12system_errorD2Ev
	.weak_def_can_be_hidden	__ZN5boost6system12system_errorD2Ev
	.align	4, 0x90
__ZN5boost6system12system_errorD2Ev:    ## @_ZN5boost6system12system_errorD2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp61:
	.cfi_def_cfa_offset 16
Ltmp62:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp63:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVN5boost6system12system_errorE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$32, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt13runtime_errorD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__StaticInit,regular,pure_instructions
	.align	4, 0x90
__GLOBAL__sub_I_jared.cpp:              ## @_GLOBAL__sub_I_jared.cpp
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp64:
	.cfi_def_cfa_offset 16
Ltmp65:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp66:
	.cfi_def_cfa_register %rbp
	callq	___cxx_global_var_init
	callq	___cxx_global_var_init.1
	callq	___cxx_global_var_init.2
	popq	%rbp
	retq
	.cfi_endproc

.zerofill __DATA,__bss,__ZN5boost6systemL14posix_categoryE,8,3 ## @_ZN5boost6systemL14posix_categoryE
.zerofill __DATA,__bss,__ZN5boost6systemL10errno_ecatE,8,3 ## @_ZN5boost6systemL10errno_ecatE
.zerofill __DATA,__bss,__ZN5boost6systemL11native_ecatE,8,3 ## @_ZN5boost6systemL11native_ecatE
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	": "

L_.str.3:                               ## @.str.3
	.asciz	"hello"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVN5boost6system12system_errorE ## @_ZTVN5boost6system12system_errorE
	.weak_def_can_be_hidden	__ZTVN5boost6system12system_errorE
	.align	3
__ZTVN5boost6system12system_errorE:
	.quad	0
	.quad	__ZTIN5boost6system12system_errorE
	.quad	__ZN5boost6system12system_errorD1Ev
	.quad	__ZN5boost6system12system_errorD0Ev
	.quad	__ZNK5boost6system12system_error4whatEv

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSN5boost6system12system_errorE ## @_ZTSN5boost6system12system_errorE
	.weak_definition	__ZTSN5boost6system12system_errorE
	.align	4
__ZTSN5boost6system12system_errorE:
	.asciz	"N5boost6system12system_errorE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTIN5boost6system12system_errorE ## @_ZTIN5boost6system12system_errorE
	.weak_definition	__ZTIN5boost6system12system_errorE
	.align	4
__ZTIN5boost6system12system_errorE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSN5boost6system12system_errorE
	.quad	__ZTISt13runtime_error

	.section	__DATA,__mod_init_func,mod_init_funcs
	.align	3
	.quad	__GLOBAL__sub_I_jared.cpp

.subsections_via_symbols
