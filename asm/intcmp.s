	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI0_0:
	.byte	255                     ## 0xff
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	255                     ## 0xff
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	255                     ## 0xff
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	255                     ## 0xff
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.byte	0                       ## 0x0
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp65:
	.cfi_def_cfa_offset 16
Ltmp66:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp67:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
Ltmp68:
	.cfi_offset %rbx, -56
Ltmp69:
	.cfi_offset %r12, -48
Ltmp70:
	.cfi_offset %r13, -40
Ltmp71:
	.cfi_offset %r14, -32
Ltmp72:
	.cfi_offset %r15, -24
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, -112(%rbp)
	movq	$0, -96(%rbp)
	leaq	L_.str(%rip), %rsi
	leaq	-112(%rbp), %rbx
	movl	$12, %edx
	movq	%rbx, %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp0:
	leaq	-88(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113random_deviceC1ERKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE
Ltmp1:
## BB#1:
	leaq	-112(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
Ltmp3:
	leaq	-88(%rbp), %rdi
	callq	__ZNSt3__113random_deviceclEv
	movl	%eax, %ebx
Ltmp4:
## BB#2:
Ltmp5:
	movl	$80000000, %edi         ## imm = 0x4C4B400
	callq	__Znwm
	movq	%rax, %r14
Ltmp6:
## BB#3:                                ## %.noexc
	movl	$80000000, %esi         ## imm = 0x4C4B400
	movq	%r14, %rdi
	callq	___bzero
	movl	%ebx, %eax
	leaq	(%rax,%rax,2), %rax
	shrq	$32, %rax
	movl	%ebx, %ecx
	subl	%eax, %ecx
	shrl	%ecx
	addl	%eax, %ecx
	shrl	$30, %ecx
	imull	$2147483647, %ecx, %eax ## imm = 0x7FFFFFFF
	subl	%eax, %ebx
	movl	$1, %r12d
	cmovnel	%ebx, %r12d
	movl	%r12d, -72(%rbp)
	movabsq	$549755813761, %rax     ## imm = 0x7FFFFFFF81
	movq	%rax, -80(%rbp)
	xorl	%ebx, %ebx
	leaq	-72(%rbp), %r15
	leaq	-80(%rbp), %r13
	.align	4, 0x90
LBB0_4:                                 ## =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	__ZNSt3__124uniform_int_distributionIiEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEiRT_RKNS1_10param_typeE
	movl	%eax, (%r14,%rbx)
	addq	$4, %rbx
	cmpq	$80000000, %rbx         ## imm = 0x4C4B400
	jne	LBB0_4
## BB#5:
Ltmp8:
	movl	$20000000, %edi         ## imm = 0x1312D00
	callq	__Znwm
	movq	%rax, %r15
Ltmp9:
## BB#6:                                ## %.noexc11
	movl	$20000000, %esi         ## imm = 0x1312D00
	movq	%r15, %rdi
	callq	___bzero
	movl	%r12d, -80(%rbp)
	movw	$32641, -72(%rbp)       ## imm = 0x7F81
	xorl	%ebx, %ebx
	leaq	-80(%rbp), %r12
	leaq	-72(%rbp), %r13
	.align	4, 0x90
LBB0_7:                                 ## =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	callq	__ZNSt3__124uniform_int_distributionIaEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEaRT_RKNS1_10param_typeE
	movb	%al, (%r15,%rbx)
	incq	%rbx
	cmpq	$20000000, %rbx         ## imm = 0x1312D00
	jne	LBB0_7
## BB#8:                                ## %min.iters.checked
	movl	$12, %ebx
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	pxor	%xmm3, %xmm3
	movq	%rax, -120(%rbp)        ## 8-byte Spill
	pxor	%xmm0, %xmm0
	.align	4, 0x90
LBB0_9:                                 ## %vector.body
                                        ## =>This Inner Loop Header: Depth=1
	movdqu	-48(%r14,%rbx,4), %xmm1
	movdqu	-32(%r14,%rbx,4), %xmm2
	psrld	$31, %xmm1
	psrld	$31, %xmm2
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm2
	movdqu	-16(%r14,%rbx,4), %xmm3
	movdqu	(%r14,%rbx,4), %xmm0
	psrld	$31, %xmm3
	psrld	$31, %xmm0
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm0
	addq	$16, %rbx
	cmpq	$20000012, %rbx         ## imm = 0x1312D0C
	jne	LBB0_9
## BB#10:                               ## %middle.block
	paddd	%xmm3, %xmm0
	pshufd	$78, %xmm0, %xmm1       ## xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	phaddd	%xmm1, %xmm1
	movd	%xmm1, %r13d
	movl	$12, %ebx
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	pxor	%xmm5, %xmm5
	movq	%rax, %r12
	movdqa	LCPI0_0(%rip), %xmm0    ## xmm0 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pxor	%xmm3, %xmm3
	pxor	%xmm4, %xmm4
	.align	4, 0x90
LBB0_11:                                ## %vector.body87
                                        ## =>This Inner Loop Header: Depth=1
	movd	-12(%r15,%rbx), %xmm1   ## xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm1    ## xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	punpcklwd	%xmm5, %xmm1    ## xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3]
	movd	-8(%r15,%rbx), %xmm2    ## xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm2    ## xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3],xmm2[4],xmm5[4],xmm2[5],xmm5[5],xmm2[6],xmm5[6],xmm2[7],xmm5[7]
	punpcklwd	%xmm5, %xmm2    ## xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3]
	psrld	$7, %xmm1
	psrld	$7, %xmm2
	pand	%xmm0, %xmm1
	pand	%xmm0, %xmm2
	paddd	%xmm3, %xmm1
	paddd	%xmm4, %xmm2
	movd	-4(%r15,%rbx), %xmm3    ## xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm3    ## xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3],xmm3[4],xmm5[4],xmm3[5],xmm5[5],xmm3[6],xmm5[6],xmm3[7],xmm5[7]
	punpcklwd	%xmm5, %xmm3    ## xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3]
	movd	(%r15,%rbx), %xmm4      ## xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm4    ## xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1],xmm4[2],xmm5[2],xmm4[3],xmm5[3],xmm4[4],xmm5[4],xmm4[5],xmm5[5],xmm4[6],xmm5[6],xmm4[7],xmm5[7]
	punpcklwd	%xmm5, %xmm4    ## xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1],xmm4[2],xmm5[2],xmm4[3],xmm5[3]
	psrld	$7, %xmm3
	psrld	$7, %xmm4
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm4
	paddd	%xmm1, %xmm3
	paddd	%xmm2, %xmm4
	addq	$16, %rbx
	cmpq	$20000012, %rbx         ## imm = 0x1312D0C
	jne	LBB0_11
## BB#12:                               ## %middle.block88
	movdqa	%xmm4, -160(%rbp)       ## 16-byte Spill
	movdqa	%xmm3, -144(%rbp)       ## 16-byte Spill
	callq	__ZNSt3__16chrono12steady_clock3nowEv
	movq	%rax, %rcx
	subq	%r12, %rcx
	subq	-120(%rbp), %r12        ## 8-byte Folded Reload
	movabsq	$2361183241434822607, %rsi ## imm = 0x20C49BA5E353F7CF
	movq	%r12, %rax
	imulq	%rsi
	movq	%rdx, %r12
	movq	%rcx, %rax
	imulq	%rsi
	movq	%rdx, -120(%rbp)        ## 8-byte Spill
Ltmp11:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.1(%rip), %rsi
	movl	$12, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp12:
## BB#13:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit
Ltmp13:
	movq	%rax, %rdi
	movl	%r13d, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
Ltmp14:
## BB#14:
Ltmp15:
	leaq	L_.str.2(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp16:
## BB#15:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit32
	movdqa	-160(%rbp), %xmm1       ## 16-byte Reload
	paddd	-144(%rbp), %xmm1       ## 16-byte Folded Reload
	pshufd	$78, %xmm1, %xmm0       ## xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	phaddd	%xmm0, %xmm0
	movd	%xmm0, %esi
Ltmp17:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEi
	movq	%rax, %r13
Ltmp18:
## BB#16:
	movq	(%r13), %rax
	movq	-24(%rax), %rsi
	addq	%r13, %rsi
Ltmp19:
	leaq	-56(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp20:
## BB#17:                               ## %.noexc53
Ltmp21:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-56(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp22:
## BB#18:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp23:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %bl
Ltmp24:
## BB#19:                               ## %.noexc35
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp26:
	movsbl	%bl, %esi
	movq	%r13, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp27:
## BB#20:                               ## %.noexc36
Ltmp28:
	movq	%r13, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp29:
## BB#21:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit34
Ltmp30:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.3(%rip), %rsi
	movl	$12, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp31:
## BB#22:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit40
	movq	%r12, %rcx
	shrq	$63, %rcx
	sarq	$7, %r12
	addq	%rcx, %r12
Ltmp32:
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEx
Ltmp33:
## BB#23:
Ltmp34:
	leaq	L_.str.4(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %r12
Ltmp35:
## BB#24:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit42
	movq	(%r12), %rax
	movq	-24(%rax), %rsi
	addq	%r12, %rsi
Ltmp36:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp37:
## BB#25:                               ## %.noexc57
Ltmp38:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp39:
## BB#26:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp40:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %bl
Ltmp41:
## BB#27:                               ## %.noexc45
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp43:
	movsbl	%bl, %esi
	movq	%r12, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp44:
## BB#28:                               ## %.noexc46
Ltmp45:
	movq	%r12, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp46:
## BB#29:                               ## %_ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEPFRS3_S4_E.exit44
Ltmp47:
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rdi
	leaq	L_.str.5(%rip), %rsi
	movl	$12, %edx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Ltmp48:
## BB#30:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit50
	movq	-120(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, %rcx
	shrq	$63, %rcx
	sarq	$7, %rsi
	addq	%rcx, %rsi
Ltmp49:
	movq	%rax, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEElsEx
Ltmp50:
## BB#31:
Ltmp51:
	leaq	L_.str.4(%rip), %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	movq	%rax, %rbx
Ltmp52:
## BB#32:                               ## %_ZNSt3__1lsINS_11char_traitsIcEEEERNS_13basic_ostreamIcT_EES6_PKc.exit52
	movq	(%rbx), %rax
	movq	-24(%rax), %rsi
	addq	%rbx, %rsi
Ltmp53:
	leaq	-64(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp54:
## BB#33:                               ## %.noexc29
Ltmp55:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-64(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp56:
## BB#34:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp57:
	movl	$10, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, %r12b
Ltmp58:
## BB#35:                               ## %.noexc26
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
Ltmp60:
	movsbl	%r12b, %esi
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
Ltmp61:
## BB#36:                               ## %.noexc27
Ltmp62:
	movq	%rbx, %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
Ltmp63:
## BB#37:                               ## %_ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev.exit21
	movq	%r15, %rdi
	callq	__ZdlPv
	movq	%r14, %rdi
	callq	__ZdlPv
	leaq	-88(%rbp), %rdi
	callq	__ZNSt3__113random_deviceD1Ev
	xorl	%eax, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB0_41:
Ltmp64:
	movq	%rax, %rbx
LBB0_42:                                ## %_ZNSt3__16vectorIaNS_9allocatorIaEEED1Ev.exit
	movq	%r15, %rdi
	callq	__ZdlPv
LBB0_43:                                ## %_ZNSt3__16vectorIiNS_9allocatorIiEEED1Ev.exit
	movq	%r14, %rdi
	callq	__ZdlPv
LBB0_44:
	leaq	-88(%rbp), %rdi
	callq	__ZNSt3__113random_deviceD1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB0_39:
Ltmp7:
	movq	%rax, %rbx
	jmp	LBB0_44
LBB0_46:
Ltmp25:
	movq	%rax, %rbx
	leaq	-56(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB0_42
LBB0_47:
Ltmp42:
	movq	%rax, %rbx
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB0_42
LBB0_48:
Ltmp59:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	jmp	LBB0_42
LBB0_38:
Ltmp2:
	movq	%rax, %rbx
	leaq	-112(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB0_40:
Ltmp10:
	movq	%rax, %rbx
	jmp	LBB0_43
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\271\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.ascii	"\266\001"              ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp3-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp6-Ltmp3                     ##   Call between Ltmp3 and Ltmp6
	.long	Lset6
Lset7 = Ltmp7-Lfunc_begin0              ##     jumps to Ltmp7
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp6-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp8-Ltmp6                     ##   Call between Ltmp6 and Ltmp8
	.long	Lset9
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset10 = Ltmp8-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset10
Lset11 = Ltmp9-Ltmp8                    ##   Call between Ltmp8 and Ltmp9
	.long	Lset11
Lset12 = Ltmp10-Lfunc_begin0            ##     jumps to Ltmp10
	.long	Lset12
	.byte	0                       ##   On action: cleanup
Lset13 = Ltmp9-Lfunc_begin0             ## >> Call Site 6 <<
	.long	Lset13
Lset14 = Ltmp11-Ltmp9                   ##   Call between Ltmp9 and Ltmp11
	.long	Lset14
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset15 = Ltmp11-Lfunc_begin0            ## >> Call Site 7 <<
	.long	Lset15
Lset16 = Ltmp20-Ltmp11                  ##   Call between Ltmp11 and Ltmp20
	.long	Lset16
Lset17 = Ltmp64-Lfunc_begin0            ##     jumps to Ltmp64
	.long	Lset17
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp21-Lfunc_begin0            ## >> Call Site 8 <<
	.long	Lset18
Lset19 = Ltmp24-Ltmp21                  ##   Call between Ltmp21 and Ltmp24
	.long	Lset19
Lset20 = Ltmp25-Lfunc_begin0            ##     jumps to Ltmp25
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp26-Lfunc_begin0            ## >> Call Site 9 <<
	.long	Lset21
Lset22 = Ltmp37-Ltmp26                  ##   Call between Ltmp26 and Ltmp37
	.long	Lset22
Lset23 = Ltmp64-Lfunc_begin0            ##     jumps to Ltmp64
	.long	Lset23
	.byte	0                       ##   On action: cleanup
Lset24 = Ltmp38-Lfunc_begin0            ## >> Call Site 10 <<
	.long	Lset24
Lset25 = Ltmp41-Ltmp38                  ##   Call between Ltmp38 and Ltmp41
	.long	Lset25
Lset26 = Ltmp42-Lfunc_begin0            ##     jumps to Ltmp42
	.long	Lset26
	.byte	0                       ##   On action: cleanup
Lset27 = Ltmp43-Lfunc_begin0            ## >> Call Site 11 <<
	.long	Lset27
Lset28 = Ltmp54-Ltmp43                  ##   Call between Ltmp43 and Ltmp54
	.long	Lset28
Lset29 = Ltmp64-Lfunc_begin0            ##     jumps to Ltmp64
	.long	Lset29
	.byte	0                       ##   On action: cleanup
Lset30 = Ltmp55-Lfunc_begin0            ## >> Call Site 12 <<
	.long	Lset30
Lset31 = Ltmp58-Ltmp55                  ##   Call between Ltmp55 and Ltmp58
	.long	Lset31
Lset32 = Ltmp59-Lfunc_begin0            ##     jumps to Ltmp59
	.long	Lset32
	.byte	0                       ##   On action: cleanup
Lset33 = Ltmp60-Lfunc_begin0            ## >> Call Site 13 <<
	.long	Lset33
Lset34 = Ltmp63-Ltmp60                  ##   Call between Ltmp60 and Ltmp63
	.long	Lset34
Lset35 = Ltmp64-Lfunc_begin0            ##     jumps to Ltmp64
	.long	Lset35
	.byte	0                       ##   On action: cleanup
Lset36 = Ltmp63-Lfunc_begin0            ## >> Call Site 14 <<
	.long	Lset36
Lset37 = Lfunc_end0-Ltmp63              ##   Call between Ltmp63 and Lfunc_end0
	.long	Lset37
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	callq	__ZSt9terminatev

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI2_0:
	.quad	32                      ## 0x20
	.quad	16                      ## 0x10
LCPI2_1:
	.quad	2                       ## 0x2
	.quad	2                       ## 0x2
LCPI2_2:
	.long	2147418112              ## 0x7fff0000
	.long	2147352576              ## 0x7ffe0000
	.long	65535                   ## 0xffff
	.long	131071                  ## 0x1ffff
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124uniform_int_distributionIiEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEiRT_RKNS1_10param_typeE
	.weak_def_can_be_hidden	__ZNSt3__124uniform_int_distributionIiEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEiRT_RKNS1_10param_typeE
	.align	4, 0x90
__ZNSt3__124uniform_int_distributionIiEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEiRT_RKNS1_10param_typeE: ## @_ZNSt3__124uniform_int_distributionIiEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEiRT_RKNS1_10param_typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp73:
	.cfi_def_cfa_offset 16
Ltmp74:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp75:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$120, %rsp
Ltmp76:
	.cfi_offset %rbx, -40
Ltmp77:
	.cfi_offset %r14, -32
Ltmp78:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movl	(%r14), %ecx
	movl	4(%r14), %eax
	movl	%eax, %r15d
	subl	%ecx, %r15d
	je	LBB2_12
## BB#1:
	incl	%r15d
	je	LBB2_2
## BB#3:
	bsrl	%r15d, %eax
	xorl	$31, %eax
	movl	$32, %edi
	subq	%rax, %rdi
	movl	$33, %ecx
	subl	%edi, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%r15d, %eax
	cmpl	$1, %eax
	sbbq	$0, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	movabsq	$-8608480567731124087, %rcx ## imm = 0x8888888888888889
	movq	%rdi, %rax
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$30, %rdx, %rax
	cmpq	%rax, %rdi
	setne	%al
	movzbl	%al, %ebx
	addq	%rdx, %rbx
	movq	%rbx, -112(%rbp)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	movq	%rax, %r9
	movq	%r9, -120(%rbp)
	movl	$2147483646, %esi       ## imm = 0x7FFFFFFE
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	movb	%r9b, %cl
	shrl	%cl, %eax
	movb	%r9b, %cl
	shll	%cl, %eax
	cmpq	$32, %r9
	cmovael	%r8d, %eax
	movl	%eax, -96(%rbp)
	subl	%eax, %esi
	movl	%eax, %eax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rax, %rsi
	jbe	LBB2_6
## BB#4:
	incq	%rbx
	movq	%rbx, -112(%rbp)
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	movq	%rax, %r9
	movq	%r9, -120(%rbp)
	cmpq	$31, %r9
	ja	LBB2_13
## BB#5:
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	movb	%r9b, %cl
	shrl	%cl, %eax
	movb	%r9b, %cl
	shll	%cl, %eax
	movl	%eax, -96(%rbp)
LBB2_6:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	subq	%rdx, %rbx
	movq	%rbx, -104(%rbp)
	cmpq	$30, %r9
	ja	LBB2_7
## BB#8:
	leal	1(%r9), %ecx
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	shrl	%cl, %eax
	shll	%cl, %eax
	movb	$1, %r8b
	jmp	LBB2_9
LBB2_2:
	movq	%rsi, -80(%rbp)
	movaps	LCPI2_0(%rip), %xmm0    ## xmm0 = [32,16]
	movups	%xmm0, -72(%rbp)
	movaps	LCPI2_1(%rip), %xmm0    ## xmm0 = [2,2]
	movups	%xmm0, -56(%rbp)
	movaps	LCPI2_2(%rip), %xmm0    ## xmm0 = [2147418112,2147352576,65535,131071]
	movups	%xmm0, -40(%rbp)
	leaq	-80(%rbp), %rdi
	callq	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	jmp	LBB2_12
LBB2_7:
	xorl	%eax, %eax
	jmp	LBB2_9
LBB2_13:                                ## %.thread.i.i
	movl	$0, -96(%rbp)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	subq	%rdx, %rbx
	movq	%rbx, -104(%rbp)
	xorl	%eax, %eax
LBB2_9:                                 ## %_ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjEC1ERS2_m.exit
	movl	%eax, -92(%rbp)
	movl	$32, %ecx
	subl	%r9d, %ecx
	movl	$-1, %eax
	movl	$-1, %edx
	shrl	%cl, %edx
	xorl	%ecx, %ecx
	testq	%r9, %r9
	cmovnel	%edx, %ecx
	movl	%ecx, -88(%rbp)
	movl	$31, %ecx
	subl	%r9d, %ecx
	movl	$-1, %edx
	shrl	%cl, %edx
	testb	%r8b, %r8b
	cmovel	%eax, %edx
	movl	%edx, -84(%rbp)
	leaq	-136(%rbp), %rbx
	.align	4, 0x90
LBB2_10:                                ## =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	cmpl	%r15d, %eax
	jae	LBB2_10
## BB#11:
	addl	(%r14), %eax
LBB2_12:
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	.weak_def_can_be_hidden	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	.align	4, 0x90
__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE: ## @_ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp79:
	.cfi_def_cfa_offset 16
Ltmp80:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp81:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r12
	pushq	%rbx
Ltmp82:
	.cfi_offset %rbx, -48
Ltmp83:
	.cfi_offset %r12, -40
Ltmp84:
	.cfi_offset %r14, -32
Ltmp85:
	.cfi_offset %r15, -24
	movq	32(%rdi), %r9
	xorl	%r10d, %r10d
	testq	%r9, %r9
	movl	$0, %eax
	je	LBB3_10
## BB#1:                                ## %.preheader4.lr.ph
	movl	40(%rdi), %r15d
	movq	16(%rdi), %rcx
	movl	48(%rdi), %r8d
	xorl	%eax, %eax
	movl	$3163493265, %r10d      ## imm = 0xBC8F1391
	cmpq	$31, %rcx
	ja	LBB3_6
## BB#2:
	movl	$2147483647, %r14d      ## imm = 0x7FFFFFFF
	xorl	%r11d, %r11d
	.align	4, 0x90
LBB3_3:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%rdi), %r12
	movl	(%r12), %edx
	movq	%rdx, %rsi
	imulq	%r10, %rsi
	shrq	$47, %rsi
	imull	$44488, %esi, %ebx      ## imm = 0xADC8
	subl	%ebx, %edx
	imull	$48271, %edx, %edx      ## imm = 0xBC8F
	imull	$3399, %esi, %esi       ## imm = 0xD47
	subl	%esi, %edx
	movl	$0, %esi
	cmovbl	%r14d, %esi
	leal	(%rsi,%rdx), %ebx
	movl	%ebx, (%r12)
	leal	-1(%rsi,%rdx), %ebx
	cmpl	%r15d, %ebx
	jae	LBB3_3
## BB#4:                                ##   in Loop: Header=BB3_3 Depth=1
	movl	%eax, %edx
	shll	%cl, %edx
	andl	%r8d, %ebx
	movl	%ebx, %eax
	addl	%edx, %eax
	incq	%r11
	cmpq	%r9, %r11
	jb	LBB3_3
## BB#5:
	movq	%r9, %r10
	jmp	LBB3_10
LBB3_6:
	movl	$2147483647, %r11d      ## imm = 0x7FFFFFFF
	.align	4, 0x90
LBB3_7:                                 ## =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rcx
	movl	(%rcx), %ebx
	movq	%rbx, %rdx
	imulq	%r10, %rdx
	shrq	$47, %rdx
	imull	$44488, %edx, %esi      ## imm = 0xADC8
	subl	%esi, %ebx
	imull	$48271, %ebx, %esi      ## imm = 0xBC8F
	imull	$3399, %edx, %edx       ## imm = 0xD47
	subl	%edx, %esi
	movl	$0, %edx
	cmovbl	%r11d, %edx
	leal	(%rdx,%rsi), %ebx
	movl	%ebx, (%rcx)
	leal	-1(%rdx,%rsi), %ecx
	cmpl	%r15d, %ecx
	jae	LBB3_7
## BB#8:                                ##   in Loop: Header=BB3_7 Depth=1
	incq	%rax
	cmpq	%r9, %rax
	jb	LBB3_7
## BB#9:                                ## %.preheader3.loopexit14
	andl	%ecx, %r8d
	movq	%r9, %r10
	movl	%r8d, %eax
LBB3_10:                                ## %.preheader3
	movq	24(%rdi), %r9
	cmpq	%r9, %r10
	jae	LBB3_19
## BB#11:                               ## %.preheader.lr.ph
	movl	44(%rdi), %r15d
	movq	16(%rdi), %rcx
	movl	52(%rdi), %r8d
	cmpq	$30, %rcx
	ja	LBB3_15
## BB#12:
	incl	%ecx
	movl	$3163493265, %r11d      ## imm = 0xBC8F1391
	movl	$2147483647, %r14d      ## imm = 0x7FFFFFFF
	.align	4, 0x90
LBB3_13:                                ## =>This Inner Loop Header: Depth=1
	movq	(%rdi), %r12
	movl	(%r12), %esi
	movq	%rsi, %rdx
	imulq	%r11, %rdx
	shrq	$47, %rdx
	imull	$44488, %edx, %ebx      ## imm = 0xADC8
	subl	%ebx, %esi
	imull	$48271, %esi, %esi      ## imm = 0xBC8F
	imull	$3399, %edx, %edx       ## imm = 0xD47
	subl	%edx, %esi
	movl	$0, %edx
	cmovbl	%r14d, %edx
	leal	(%rdx,%rsi), %ebx
	movl	%ebx, (%r12)
	leal	-1(%rdx,%rsi), %ebx
	cmpl	%r15d, %ebx
	jae	LBB3_13
## BB#14:                               ##   in Loop: Header=BB3_13 Depth=1
	shll	%cl, %eax
	andl	%r8d, %ebx
	addl	%ebx, %eax
	incq	%r10
	cmpq	%r9, %r10
	jb	LBB3_13
	jmp	LBB3_19
LBB3_15:
	movl	$3163493265, %r11d      ## imm = 0xBC8F1391
	movl	$2147483647, %ecx       ## imm = 0x7FFFFFFF
	.align	4, 0x90
LBB3_16:                                ## =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdx
	movl	(%rdx), %ebx
	movq	%rbx, %rsi
	imulq	%r11, %rsi
	shrq	$47, %rsi
	imull	$44488, %esi, %eax      ## imm = 0xADC8
	subl	%eax, %ebx
	imull	$48271, %ebx, %eax      ## imm = 0xBC8F
	imull	$3399, %esi, %esi       ## imm = 0xD47
	subl	%esi, %eax
	movl	$0, %esi
	cmovbl	%ecx, %esi
	leal	(%rsi,%rax), %ebx
	movl	%ebx, (%rdx)
	leal	-1(%rsi,%rax), %edx
	cmpl	%r15d, %edx
	jae	LBB3_16
## BB#17:                               ##   in Loop: Header=BB3_16 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jb	LBB3_16
## BB#18:                               ## %._crit_edge.loopexit13
	andl	%edx, %r8d
	movl	%r8d, %eax
LBB3_19:                                ## %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__literal16,16byte_literals
	.align	4
LCPI4_0:
	.quad	32                      ## 0x20
	.quad	16                      ## 0x10
LCPI4_1:
	.quad	2                       ## 0x2
	.quad	2                       ## 0x2
LCPI4_2:
	.long	2147418112              ## 0x7fff0000
	.long	2147352576              ## 0x7ffe0000
	.long	65535                   ## 0xffff
	.long	131071                  ## 0x1ffff
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124uniform_int_distributionIaEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEaRT_RKNS1_10param_typeE
	.weak_def_can_be_hidden	__ZNSt3__124uniform_int_distributionIaEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEaRT_RKNS1_10param_typeE
	.align	4, 0x90
__ZNSt3__124uniform_int_distributionIaEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEaRT_RKNS1_10param_typeE: ## @_ZNSt3__124uniform_int_distributionIaEclINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEEEaRT_RKNS1_10param_typeE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp86:
	.cfi_def_cfa_offset 16
Ltmp87:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp88:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%rbx
	subq	$120, %rsp
Ltmp89:
	.cfi_offset %rbx, -40
Ltmp90:
	.cfi_offset %r14, -32
Ltmp91:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movzbl	1(%r14), %eax
	movzbl	(%r14), %ecx
	cmpl	%ecx, %eax
	je	LBB4_12
## BB#1:
	movsbl	%al, %r15d
	movsbl	%cl, %eax
	subl	%eax, %r15d
	incl	%r15d
	je	LBB4_2
## BB#3:
	bsrl	%r15d, %eax
	xorl	$31, %eax
	movl	$32, %edi
	subq	%rax, %rdi
	movl	$33, %ecx
	subl	%edi, %ecx
	movl	$-1, %eax
	shrl	%cl, %eax
	andl	%r15d, %eax
	cmpl	$1, %eax
	sbbq	$0, %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdi, -128(%rbp)
	movabsq	$-8608480567731124087, %rcx ## imm = 0x8888888888888889
	movq	%rdi, %rax
	mulq	%rcx
	shrq	$4, %rdx
	imulq	$30, %rdx, %rax
	cmpq	%rax, %rdi
	setne	%al
	movzbl	%al, %ebx
	addq	%rdx, %rbx
	movq	%rbx, -112(%rbp)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	movq	%rax, %r9
	movq	%r9, -120(%rbp)
	movl	$2147483646, %esi       ## imm = 0x7FFFFFFE
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	movb	%r9b, %cl
	shrl	%cl, %eax
	movb	%r9b, %cl
	shll	%cl, %eax
	cmpq	$32, %r9
	cmovael	%r8d, %eax
	movl	%eax, -96(%rbp)
	subl	%eax, %esi
	movl	%eax, %eax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rax, %rsi
	jbe	LBB4_6
## BB#4:
	incq	%rbx
	movq	%rbx, -112(%rbp)
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	movq	%rax, %r9
	movq	%r9, -120(%rbp)
	cmpq	$31, %r9
	ja	LBB4_13
## BB#5:
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	movb	%r9b, %cl
	shrl	%cl, %eax
	movb	%r9b, %cl
	shll	%cl, %eax
	movl	%eax, -96(%rbp)
LBB4_6:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	subq	%rdx, %rbx
	movq	%rbx, -104(%rbp)
	cmpq	$30, %r9
	ja	LBB4_7
## BB#8:
	leal	1(%r9), %ecx
	movl	$2147483646, %eax       ## imm = 0x7FFFFFFE
	shrl	%cl, %eax
	shll	%cl, %eax
	movb	$1, %r8b
	jmp	LBB4_9
LBB4_2:
	movq	%rsi, -80(%rbp)
	movaps	LCPI4_0(%rip), %xmm0    ## xmm0 = [32,16]
	movups	%xmm0, -72(%rbp)
	movaps	LCPI4_1(%rip), %xmm0    ## xmm0 = [2,2]
	movups	%xmm0, -56(%rbp)
	movaps	LCPI4_2(%rip), %xmm0    ## xmm0 = [2147418112,2147352576,65535,131071]
	movups	%xmm0, -40(%rbp)
	leaq	-80(%rbp), %rdi
	callq	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	jmp	LBB4_12
LBB4_7:
	xorl	%eax, %eax
	jmp	LBB4_9
LBB4_13:                                ## %.thread.i.i
	movl	$0, -96(%rbp)
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rbx
	subq	%rdx, %rbx
	movq	%rbx, -104(%rbp)
	xorl	%eax, %eax
LBB4_9:                                 ## %_ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjEC1ERS2_m.exit
	movl	%eax, -92(%rbp)
	movl	$32, %ecx
	subl	%r9d, %ecx
	movl	$-1, %eax
	movl	$-1, %edx
	shrl	%cl, %edx
	xorl	%ecx, %ecx
	testq	%r9, %r9
	cmovnel	%edx, %ecx
	movl	%ecx, -88(%rbp)
	movl	$31, %ecx
	subl	%r9d, %ecx
	movl	$-1, %edx
	shrl	%cl, %edx
	testb	%r8b, %r8b
	cmovel	%eax, %edx
	movl	%edx, -84(%rbp)
	leaq	-136(%rbp), %rbx
	.align	4, 0x90
LBB4_10:                                ## =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	__ZNSt3__125__independent_bits_engineINS_26linear_congruential_engineIjLj48271ELj0ELj2147483647EEEjE6__evalENS_17integral_constantIbLb1EEE
	movl	%eax, %ecx
	cmpl	%r15d, %ecx
	jae	LBB4_10
## BB#11:
	movzbl	(%r14), %eax
	addl	%ecx, %eax
LBB4_12:
	movsbl	%al, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp113:
	.cfi_def_cfa_offset 16
Ltmp114:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp115:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp116:
	.cfi_offset %rbx, -56
Ltmp117:
	.cfi_offset %r12, -48
Ltmp118:
	.cfi_offset %r13, -40
Ltmp119:
	.cfi_offset %r14, -32
Ltmp120:
	.cfi_offset %r15, -24
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
Ltmp92:
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp93:
## BB#1:
	cmpb	$0, -64(%rbp)
	je	LBB5_10
## BB#2:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %r12
	movq	40(%rbx,%rax), %rdi
	movl	8(%rbx,%rax), %r13d
	movl	144(%rbx,%rax), %eax
	cmpl	$-1, %eax
	jne	LBB5_7
## BB#3:
Ltmp95:
	movq	%rdi, -72(%rbp)         ## 8-byte Spill
	leaq	-48(%rbp), %rdi
	movq	%r12, %rsi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp96:
## BB#4:                                ## %.noexc
Ltmp97:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp98:
## BB#5:
	movq	(%rax), %rcx
	movq	56(%rcx), %rcx
Ltmp99:
	movl	$32, %esi
	movq	%rax, %rdi
	callq	*%rcx
	movb	%al, -73(%rbp)          ## 1-byte Spill
Ltmp100:
## BB#6:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movsbl	-73(%rbp), %eax         ## 1-byte Folded Reload
	movl	%eax, 144(%r12)
	movq	-72(%rbp), %rdi         ## 8-byte Reload
LBB5_7:
	addq	%r15, %r14
	andl	$176, %r13d
	cmpl	$32, %r13d
	movq	%r15, %rdx
	cmoveq	%r14, %rdx
Ltmp102:
	movsbl	%al, %r9d
	movq	%r15, %rsi
	movq	%r14, %rcx
	movq	%r12, %r8
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp103:
## BB#8:
	testq	%rax, %rax
	jne	LBB5_10
## BB#9:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	leaq	(%rbx,%rax), %rdi
	movl	32(%rbx,%rax), %esi
	orl	$5, %esi
Ltmp104:
	callq	__ZNSt3__18ios_base5clearEj
Ltmp105:
LBB5_10:                                ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB5_15:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB5_11:
Ltmp106:
	movq	%rax, %r14
	jmp	LBB5_12
LBB5_20:
Ltmp94:
	movq	%rax, %r14
	jmp	LBB5_13
LBB5_19:
Ltmp101:
	movq	%rax, %r14
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
LBB5_12:                                ## %.body
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB5_13:
	movq	%rbx, %r15
	movq	%r14, %rdi
	callq	___cxa_begin_catch
	movq	(%rbx), %rax
	addq	-24(%rax), %r15
Ltmp107:
	movq	%r15, %rdi
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp108:
## BB#14:
	callq	___cxa_end_catch
	jmp	LBB5_15
LBB5_16:
Ltmp109:
	movq	%rax, %rbx
Ltmp110:
	callq	___cxa_end_catch
Ltmp111:
## BB#17:
	movq	%rbx, %rdi
	callq	__Unwind_Resume
LBB5_18:
Ltmp112:
	movq	%rax, %rdi
	callq	___clang_call_terminate
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table5:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	125                     ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset38 = Ltmp92-Lfunc_begin1            ## >> Call Site 1 <<
	.long	Lset38
Lset39 = Ltmp93-Ltmp92                  ##   Call between Ltmp92 and Ltmp93
	.long	Lset39
Lset40 = Ltmp94-Lfunc_begin1            ##     jumps to Ltmp94
	.long	Lset40
	.byte	1                       ##   On action: 1
Lset41 = Ltmp95-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset41
Lset42 = Ltmp96-Ltmp95                  ##   Call between Ltmp95 and Ltmp96
	.long	Lset42
Lset43 = Ltmp106-Lfunc_begin1           ##     jumps to Ltmp106
	.long	Lset43
	.byte	1                       ##   On action: 1
Lset44 = Ltmp97-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset44
Lset45 = Ltmp100-Ltmp97                 ##   Call between Ltmp97 and Ltmp100
	.long	Lset45
Lset46 = Ltmp101-Lfunc_begin1           ##     jumps to Ltmp101
	.long	Lset46
	.byte	1                       ##   On action: 1
Lset47 = Ltmp102-Lfunc_begin1           ## >> Call Site 4 <<
	.long	Lset47
Lset48 = Ltmp105-Ltmp102                ##   Call between Ltmp102 and Ltmp105
	.long	Lset48
Lset49 = Ltmp106-Lfunc_begin1           ##     jumps to Ltmp106
	.long	Lset49
	.byte	1                       ##   On action: 1
Lset50 = Ltmp105-Lfunc_begin1           ## >> Call Site 5 <<
	.long	Lset50
Lset51 = Ltmp107-Ltmp105                ##   Call between Ltmp105 and Ltmp107
	.long	Lset51
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset52 = Ltmp107-Lfunc_begin1           ## >> Call Site 6 <<
	.long	Lset52
Lset53 = Ltmp108-Ltmp107                ##   Call between Ltmp107 and Ltmp108
	.long	Lset53
Lset54 = Ltmp109-Lfunc_begin1           ##     jumps to Ltmp109
	.long	Lset54
	.byte	0                       ##   On action: cleanup
Lset55 = Ltmp108-Lfunc_begin1           ## >> Call Site 7 <<
	.long	Lset55
Lset56 = Ltmp110-Ltmp108                ##   Call between Ltmp108 and Ltmp110
	.long	Lset56
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset57 = Ltmp110-Lfunc_begin1           ## >> Call Site 8 <<
	.long	Lset57
Lset58 = Ltmp111-Ltmp110                ##   Call between Ltmp110 and Ltmp111
	.long	Lset58
Lset59 = Ltmp112-Lfunc_begin1           ##     jumps to Ltmp112
	.long	Lset59
	.byte	1                       ##   On action: 1
Lset60 = Ltmp111-Lfunc_begin1           ## >> Call Site 9 <<
	.long	Lset60
Lset61 = Lfunc_end1-Ltmp111             ##   Call between Ltmp111 and Lfunc_end1
	.long	Lset61
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	1                       ## >> Action Record 1 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp124:
	.cfi_def_cfa_offset 16
Ltmp125:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp126:
	.cfi_def_cfa_register %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
Ltmp127:
	.cfi_offset %rbx, -56
Ltmp128:
	.cfi_offset %r12, -48
Ltmp129:
	.cfi_offset %r13, -40
Ltmp130:
	.cfi_offset %r14, -32
Ltmp131:
	.cfi_offset %r15, -24
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	LBB6_9
## BB#1:
	movq	%r15, %rax
	subq	%rsi, %rax
	movq	24(%r8), %rcx
	xorl	%ebx, %ebx
	subq	%rax, %rcx
	cmovgq	%rcx, %rbx
	movq	%r12, %r14
	subq	%rsi, %r14
	testq	%r14, %r14
	jle	LBB6_3
## BB#2:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, -72(%rbp)         ## 8-byte Spill
	movq	%r12, -80(%rbp)         ## 8-byte Spill
	movq	%r8, %r12
	movl	%r9d, %r15d
	callq	*96(%rax)
	movl	%r15d, %r9d
	movq	%r12, %r8
	movq	-80(%rbp), %r12         ## 8-byte Reload
	movq	-72(%rbp), %r15         ## 8-byte Reload
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r14, %rcx
	jne	LBB6_9
LBB6_3:
	testq	%rbx, %rbx
	jle	LBB6_6
## BB#4:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	%r8, -72(%rbp)          ## 8-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	movq	$0, -48(%rbp)
	movsbl	%r9b, %edx
	leaq	-64(%rbp), %rdi
	movq	%rbx, %rsi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	testb	$1, -64(%rbp)
	leaq	-63(%rbp), %rsi
	cmovneq	-48(%rbp), %rsi
	movq	(%r13), %rax
	movq	96(%rax), %rax
Ltmp121:
	movq	%r13, %rdi
	movq	%rbx, %rdx
	callq	*%rax
	movq	%rax, %r14
Ltmp122:
## BB#5:                                ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	xorl	%eax, %eax
	cmpq	%rbx, %r14
	movq	-72(%rbp), %r8          ## 8-byte Reload
	jne	LBB6_9
LBB6_6:
	subq	%r12, %r15
	testq	%r15, %r15
	jle	LBB6_8
## BB#7:
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r8, %rbx
	callq	*96(%rax)
	movq	%rbx, %r8
	movq	%rax, %rcx
	xorl	%eax, %eax
	cmpq	%r15, %rcx
	jne	LBB6_9
LBB6_8:
	movq	$0, 24(%r8)
	movq	%r13, %rax
LBB6_9:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
LBB6_10:
Ltmp123:
	movq	%rax, %rbx
	leaq	-64(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movq	%rbx, %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table6:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset62 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset62
Lset63 = Ltmp121-Lfunc_begin2           ##   Call between Lfunc_begin2 and Ltmp121
	.long	Lset63
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset64 = Ltmp121-Lfunc_begin2           ## >> Call Site 2 <<
	.long	Lset64
Lset65 = Ltmp122-Ltmp121                ##   Call between Ltmp121 and Ltmp122
	.long	Lset65
Lset66 = Ltmp123-Lfunc_begin2           ##     jumps to Ltmp123
	.long	Lset66
	.byte	0                       ##   On action: cleanup
Lset67 = Ltmp122-Lfunc_begin2           ## >> Call Site 3 <<
	.long	Lset67
Lset68 = Lfunc_end2-Ltmp122             ##   Call between Ltmp122 and Lfunc_end2
	.long	Lset68
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"/dev/urandom"

L_.str.1:                               ## @.str.1
	.asciz	"totals    : "

L_.str.2:                               ## @.str.2
	.asciz	", "

L_.str.3:                               ## @.str.3
	.asciz	"int time  : "

L_.str.4:                               ## @.str.4
	.asciz	"us"

L_.str.5:                               ## @.str.5
	.asciz	"byte time : "


.subsections_via_symbols
