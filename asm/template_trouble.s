	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_main
	.align	4, 0x90
_main:                                  ## @main
Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception0
## BB#0:
	pushq	%rbp
Ltmp18:
	.cfi_def_cfa_offset 16
Ltmp19:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp20:
	.cfi_def_cfa_register %rbp
	subq	$96, %rsp
	leaq	L_.str(%rip), %rsi
	leaq	-16(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)         ## 8-byte Spill
	callq	__ZN7MyClassIiLi1EEC1IJRA13_KcEEEDpOT_
Ltmp0:
	leaq	-32(%rbp), %rdi
	movq	-88(%rbp), %rsi         ## 8-byte Reload
	callq	__ZN7MyClassIiLi1EEC1ERS0_
Ltmp1:
	jmp	LBB0_1
LBB0_1:
Ltmp3:
	leaq	L_.str.1(%rip), %rsi
	leaq	-64(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EEC1IJRA8_KcEEEDpOT_
Ltmp4:
	jmp	LBB0_2
LBB0_2:
Ltmp6:
	leaq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEC1ERS0_
Ltmp7:
	jmp	LBB0_3
LBB0_3:
	leaq	-32(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEaSERKS0_
Ltmp9:
	leaq	-16(%rbp), %rdi
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	callq	__ZNK7MyClassIiLi1EE5printEv
Ltmp10:
	jmp	LBB0_4
LBB0_4:
Ltmp11:
	leaq	-32(%rbp), %rdi
	callq	__ZNK7MyClassIiLi1EE5printEv
Ltmp12:
	jmp	LBB0_5
LBB0_5:
Ltmp13:
	leaq	-64(%rbp), %rdi
	callq	__ZNK7MyClassIiLi1EE5printEv
Ltmp14:
	jmp	LBB0_6
LBB0_6:
Ltmp15:
	leaq	-80(%rbp), %rdi
	callq	__ZNK7MyClassIiLi1EE5printEv
Ltmp16:
	jmp	LBB0_7
LBB0_7:
	leaq	-80(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
	leaq	-64(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
	leaq	-32(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
	leaq	-16(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
	xorl	%eax, %eax
	addq	$96, %rsp
	popq	%rbp
	retq
LBB0_8:
Ltmp2:
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	jmp	LBB0_14
LBB0_9:
Ltmp5:
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	jmp	LBB0_13
LBB0_10:
Ltmp8:
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	jmp	LBB0_12
LBB0_11:
Ltmp17:
	leaq	-80(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -40(%rbp)
	movl	%ecx, -44(%rbp)
	callq	__ZN7MyClassIiLi1EED1Ev
LBB0_12:
	leaq	-64(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
LBB0_13:
	leaq	-32(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
LBB0_14:
	leaq	-16(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED1Ev
## BB#15:
	movq	-40(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end0:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table0:
Lexception0:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\320"                  ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	78                      ## Call site table length
Lset0 = Lfunc_begin0-Lfunc_begin0       ## >> Call Site 1 <<
	.long	Lset0
Lset1 = Ltmp0-Lfunc_begin0              ##   Call between Lfunc_begin0 and Ltmp0
	.long	Lset1
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset2 = Ltmp0-Lfunc_begin0              ## >> Call Site 2 <<
	.long	Lset2
Lset3 = Ltmp1-Ltmp0                     ##   Call between Ltmp0 and Ltmp1
	.long	Lset3
Lset4 = Ltmp2-Lfunc_begin0              ##     jumps to Ltmp2
	.long	Lset4
	.byte	0                       ##   On action: cleanup
Lset5 = Ltmp3-Lfunc_begin0              ## >> Call Site 3 <<
	.long	Lset5
Lset6 = Ltmp4-Ltmp3                     ##   Call between Ltmp3 and Ltmp4
	.long	Lset6
Lset7 = Ltmp5-Lfunc_begin0              ##     jumps to Ltmp5
	.long	Lset7
	.byte	0                       ##   On action: cleanup
Lset8 = Ltmp6-Lfunc_begin0              ## >> Call Site 4 <<
	.long	Lset8
Lset9 = Ltmp7-Ltmp6                     ##   Call between Ltmp6 and Ltmp7
	.long	Lset9
Lset10 = Ltmp8-Lfunc_begin0             ##     jumps to Ltmp8
	.long	Lset10
	.byte	0                       ##   On action: cleanup
Lset11 = Ltmp9-Lfunc_begin0             ## >> Call Site 5 <<
	.long	Lset11
Lset12 = Ltmp16-Ltmp9                   ##   Call between Ltmp9 and Ltmp16
	.long	Lset12
Lset13 = Ltmp17-Lfunc_begin0            ##     jumps to Ltmp17
	.long	Lset13
	.byte	0                       ##   On action: cleanup
Lset14 = Ltmp16-Lfunc_begin0            ## >> Call Site 6 <<
	.long	Lset14
Lset15 = Lfunc_end0-Ltmp16              ##   Call between Ltmp16 and Lfunc_end0
	.long	Lset15
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZN7MyClassIiLi1EEC1IJRA13_KcEEEDpOT_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC1IJRA13_KcEEEDpOT_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC1IJRA13_KcEEEDpOT_: ## @_ZN7MyClassIiLi1EEC1IJRA13_KcEEEDpOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp21:
	.cfi_def_cfa_offset 16
Ltmp22:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp23:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEC2IJRA13_KcEEEDpOT_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC1ERS0_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC1ERS0_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC1ERS0_:             ## @_ZN7MyClassIiLi1EEC1ERS0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp24:
	.cfi_def_cfa_offset 16
Ltmp25:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp26:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEC1ERKS0_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC1IJRA8_KcEEEDpOT_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC1IJRA8_KcEEEDpOT_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC1IJRA8_KcEEEDpOT_:  ## @_ZN7MyClassIiLi1EEC1IJRA8_KcEEEDpOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp27:
	.cfi_def_cfa_offset 16
Ltmp28:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp29:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEC2IJRA8_KcEEEDpOT_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEaSERKS0_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEaSERKS0_
	.align	4, 0x90
__ZN7MyClassIiLi1EEaSERKS0_:            ## @_ZN7MyClassIiLi1EEaSERKS0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp30:
	.cfi_def_cfa_offset 16
Ltmp31:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp32:
	.cfi_def_cfa_register %rbp
	subq	$224, %rsp
	leaq	-176(%rbp), %rax
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdi
	movq	%rsi, -152(%rbp)
	movq	%rdi, -160(%rbp)
	movq	-152(%rbp), %rdi
	movq	-160(%rbp), %rcx
	movq	%rax, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %rcx
	movq	%rax, -120(%rbp)
	movq	%rcx, -128(%rbp)
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-128(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rax)
	cmpq	$0, 8(%rax)
	movq	%rdi, -200(%rbp)        ## 8-byte Spill
	movq	%rsi, -208(%rbp)        ## 8-byte Spill
	movq	%rax, -216(%rbp)        ## 8-byte Spill
	je	LBB4_2
## BB#1:
	movq	-216(%rbp), %rax        ## 8-byte Reload
	movq	8(%rax), %rdi
	callq	__ZNSt3__119__shared_weak_count12__add_sharedEv
LBB4_2:                                 ## %_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEaSERKS7_.exit
	leaq	-176(%rbp), %rax
	leaq	-48(%rbp), %rcx
	leaq	-96(%rbp), %rdx
	movq	%rax, -104(%rbp)
	movq	-200(%rbp), %rsi        ## 8-byte Reload
	movq	%rsi, -112(%rbp)
	movq	-104(%rbp), %rdi
	movq	-112(%rbp), %r8
	movq	%rdi, -80(%rbp)
	movq	%r8, -88(%rbp)
	movq	-80(%rbp), %r8
	movq	%r8, -72(%rbp)
	movq	-72(%rbp), %r8
	movq	(%r8), %r8
	movq	%r8, -96(%rbp)
	movq	-88(%rbp), %r8
	movq	%r8, -56(%rbp)
	movq	-56(%rbp), %r8
	movq	(%r8), %r8
	movq	-80(%rbp), %r9
	movq	%r8, (%r9)
	movq	%rdx, -64(%rbp)
	movq	-64(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-88(%rbp), %r8
	movq	%rdx, (%r8)
	addq	$8, %rdi
	movq	-112(%rbp), %rdx
	addq	$8, %rdx
	movq	%rdi, -32(%rbp)
	movq	%rdx, -40(%rbp)
	movq	-32(%rbp), %rdx
	movq	%rdx, -24(%rbp)
	movq	-24(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -48(%rbp)
	movq	-40(%rbp), %rdx
	movq	%rdx, -8(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	-32(%rbp), %rdi
	movq	%rdx, (%rdi)
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	-40(%rbp), %rdx
	movq	%rcx, (%rdx)
	movq	%rax, %rdi
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
	movq	-208(%rbp), %rax        ## 8-byte Reload
	addq	$224, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNK7MyClassIiLi1EE5printEv
	.weak_def_can_be_hidden	__ZNK7MyClassIiLi1EE5printEv
	.align	4, 0x90
__ZNK7MyClassIiLi1EE5printEv:           ## @_ZNK7MyClassIiLi1EE5printEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp33:
	.cfi_def_cfa_offset 16
Ltmp34:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp35:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	__ZNSt3__14coutE@GOTPCREL(%rip), %rax
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rdi
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	movq	(%rdi), %rsi
	movq	%rax, %rdi
	callq	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	leaq	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_(%rip), %rsi
	movq	%rax, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	callq	*-16(%rbp)
	movq	%rax, -40(%rbp)         ## 8-byte Spill
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EED1Ev
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EED1Ev
	.align	4, 0x90
__ZN7MyClassIiLi1EED1Ev:                ## @_ZN7MyClassIiLi1EED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp36:
	.cfi_def_cfa_offset 16
Ltmp37:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp38:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZN7MyClassIiLi1EED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
	.align	4, 0x90
__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev: ## @_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp39:
	.cfi_def_cfa_offset 16
Ltmp40:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp41:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED2Ev
	.align	4, 0x90
__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED2Ev: ## @_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp42:
	.cfi_def_cfa_offset 16
Ltmp43:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp44:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	cmpq	$0, 8(%rdi)
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	je	LBB8_2
## BB#1:
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	8(%rax), %rdi
	callq	__ZNSt3__119__shared_weak_count16__release_sharedEv
LBB8_2:
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EED2Ev
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EED2Ev
	.align	4, 0x90
__ZN7MyClassIiLi1EED2Ev:                ## @_ZN7MyClassIiLi1EED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp45:
	.cfi_def_cfa_offset 16
Ltmp46:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp47:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC2IJRA13_KcEEEDpOT_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC2IJRA13_KcEEEDpOT_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC2IJRA13_KcEEEDpOT_: ## @_ZN7MyClassIiLi1EEC2IJRA13_KcEEEDpOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp48:
	.cfi_def_cfa_offset 16
Ltmp49:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp50:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA13_KcEEES7_DpOT_
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA13_KcEEES7_DpOT_
	.weak_def_can_be_hidden	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA13_KcEEES7_DpOT_
	.align	4, 0x90
__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA13_KcEEES7_DpOT_: ## @_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA13_KcEEES7_DpOT_
Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception1
## BB#0:
	pushq	%rbp
Ltmp56:
	.cfi_def_cfa_offset 16
Ltmp57:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp58:
	.cfi_def_cfa_register %rbp
	subq	$1824, %rsp             ## imm = 0x720
	movq	%rdi, %rax
	movq	%rsi, -1664(%rbp)
	leaq	-1672(%rbp), %rsi
	movq	%rsi, -1656(%rbp)
	movq	%rsi, -1648(%rbp)
	movq	%rsi, -1464(%rbp)
	movq	$1, -1472(%rbp)
	movq	$0, -1480(%rbp)
	movq	-1472(%rbp), %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rcx, -1456(%rbp)
	movq	%rdi, -1744(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -1752(%rbp)       ## 8-byte Spill
	movq	%rsi, -1760(%rbp)       ## 8-byte Spill
	callq	__Znwm
	leaq	-1712(%rbp), %rcx
	movq	%rcx, -1432(%rbp)
	movq	-1760(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -1440(%rbp)
	movq	$1, -1448(%rbp)
	movq	-1432(%rbp), %rdi
	movq	-1440(%rbp), %rdx
	movq	%rdi, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	movq	$1, -1424(%rbp)
	movq	-1408(%rbp), %rdx
	movq	-1416(%rbp), %rdi
	movq	%rdi, (%rdx)
	movq	-1424(%rbp), %rdi
	movq	%rdi, 8(%rdx)
	leaq	-1696(%rbp), %rdx
	movq	%rdx, -1384(%rbp)
	movq	%rax, -1392(%rbp)
	movq	%rcx, -1400(%rbp)
	movq	-1384(%rbp), %rax
	movq	-1392(%rbp), %rdi
	movq	%rax, -1344(%rbp)
	movq	%rdi, -1352(%rbp)
	movq	%rcx, -1360(%rbp)
	movq	-1344(%rbp), %rax
	movq	-1352(%rbp), %rdi
	movq	%rcx, -1336(%rbp)
	movq	-1712(%rbp), %rcx
	movq	-1704(%rbp), %r8
	movq	%r8, -1368(%rbp)
	movq	%rcx, -1376(%rbp)
	movq	-1376(%rbp), %rcx
	movq	-1368(%rbp), %r8
	movq	%rcx, -1312(%rbp)
	movq	%r8, -1304(%rbp)
	movq	%rax, -1320(%rbp)
	movq	%rdi, -1328(%rbp)
	movq	-1320(%rbp), %rax
	movq	-1312(%rbp), %rcx
	movq	-1304(%rbp), %r8
	movq	%rcx, -1264(%rbp)
	movq	%r8, -1256(%rbp)
	movq	%rax, -1272(%rbp)
	movq	%rdi, -1280(%rbp)
	movq	-1272(%rbp), %rax
	leaq	-1280(%rbp), %rcx
	movq	%rcx, -1248(%rbp)
	movq	-1280(%rbp), %rcx
	leaq	-1264(%rbp), %rdi
	movq	%rdi, -1192(%rbp)
	movq	-1264(%rbp), %rdi
	movq	-1256(%rbp), %r8
	movq	%r8, -1288(%rbp)
	movq	%rdi, -1296(%rbp)
	movq	-1296(%rbp), %rdi
	movq	-1288(%rbp), %r8
	movq	%rdi, -1224(%rbp)
	movq	%r8, -1216(%rbp)
	movq	%rax, -1232(%rbp)
	movq	%rcx, -1240(%rbp)
	movq	-1232(%rbp), %rax
	leaq	-1240(%rbp), %rcx
	movq	%rcx, -1208(%rbp)
	movq	-1240(%rbp), %rcx
	movq	%rcx, (%rax)
	leaq	-1224(%rbp), %rcx
	movq	%rcx, -1200(%rbp)
	movq	-1224(%rbp), %rcx
	movq	-1216(%rbp), %rdi
	movq	%rdi, 16(%rax)
	movq	%rcx, 8(%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1016(%rbp)
	movq	%rdx, -1008(%rbp)
	movq	-1696(%rbp), %rax
	leaq	-1720(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-1664(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	%rax, -864(%rbp)
	movq	%rcx, -872(%rbp)
	movq	-864(%rbp), %rax
	movq	%rax, -808(%rbp)
	movq	%rcx, -816(%rbp)
	movq	-808(%rbp), %rax
	movq	%rax, -784(%rbp)
	movq	$0, -792(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	$0, -776(%rbp)
	movq	-768(%rbp), %rdx
	movq	__ZTVNSt3__114__shared_countE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, (%rdx)
	movq	-776(%rbp), %rdi
	movq	%rdi, 8(%rdx)
	movq	__ZTVNSt3__119__shared_weak_countE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, (%rcx)
	movq	-792(%rbp), %rdx
	movq	%rdx, 16(%rcx)
	movq	__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	%rax, %rcx
	addq	$24, %rcx
	leaq	-800(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	%rdx, -464(%rbp)
	leaq	-472(%rbp), %rdi
	movq	%rdi, -448(%rbp)
	movq	%rdx, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	-400(%rbp), %rdi
	movq	%rdi, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	%rdi, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdi, -280(%rbp)
	movq	%rdx, -288(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdx, -272(%rbp)
	movq	%rdx, (%rdi)
	movq	-472(%rbp), %rdx
	movq	%rdx, -832(%rbp)
	movq	-816(%rbp), %rdx
	movq	%rdx, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -240(%rbp)
	leaq	-248(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	movq	%rdx, (%rdi)
	movq	-248(%rbp), %rdx
	movq	%rdx, -840(%rbp)
	movq	-832(%rbp), %rdi
	movq	%rdi, -744(%rbp)
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	-744(%rbp), %rdx
	movq	-752(%rbp), %rdi
	movq	%rdx, -672(%rbp)
	movq	%rdi, -680(%rbp)
	movq	%rcx, -688(%rbp)
	leaq	-672(%rbp), %rdx
	movq	%rdx, -656(%rbp)
	movq	-672(%rbp), %rdx
	movq	%rdx, -704(%rbp)
	leaq	-680(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-680(%rbp), %rdx
	movq	%rdx, -712(%rbp)
	movq	-704(%rbp), %rdi
	movq	%rdi, -616(%rbp)
	movq	%rdx, -624(%rbp)
	movq	%rcx, -648(%rbp)
	leaq	-616(%rbp), %rdx
	movq	%rdx, -600(%rbp)
	movq	%rdx, -592(%rbp)
	movq	-616(%rbp), %rdx
	movq	%rdx, -520(%rbp)
	leaq	-624(%rbp), %rdx
	movq	%rdx, -512(%rbp)
	movq	%rdx, -504(%rbp)
	movq	-624(%rbp), %rdx
	movq	%rdx, -496(%rbp)
	movq	%rcx, -576(%rbp)
	movq	%rdx, -584(%rbp)
	movq	-576(%rbp), %rcx
	movq	%rcx, -560(%rbp)
	movq	%rdx, -568(%rbp)
	movq	-560(%rbp), %rcx
	movq	%rcx, -552(%rbp)
	movq	%rcx, -544(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rcx, -528(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-568(%rbp), %rdx
Ltmp51:
	movq	%rdx, %rdi
	movq	%rax, -1768(%rbp)       ## 8-byte Spill
	movq	%rcx, -1776(%rbp)       ## 8-byte Spill
	movq	%rdx, -1784(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp52:
	movq	%rax, -1792(%rbp)       ## 8-byte Spill
	jmp	LBB11_1
LBB11_1:                                ## %.noexc.i.i
Ltmp53:
	movq	-1776(%rbp), %rdi       ## 8-byte Reload
	movq	-1784(%rbp), %rsi       ## 8-byte Reload
	movq	-1792(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp54:
	jmp	LBB11_3
LBB11_2:
Ltmp55:
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	movq	-1768(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__119__shared_weak_countD2Ev
	movq	-848(%rbp), %rax
	movl	-852(%rbp), %ecx
	movq	%rax, -1800(%rbp)       ## 8-byte Spill
	movl	%ecx, -1804(%rbp)       ## 4-byte Spill
	jmp	LBB11_5
LBB11_3:                                ## %_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC1IJRA13_KcEEES7_DpOT_.exit
	jmp	LBB11_4
LBB11_4:
	leaq	-1696(%rbp), %rax
	movb	$0, -1733(%rbp)
	movq	-1744(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rax, -912(%rbp)
	movq	-912(%rbp), %rdx
	movq	%rdx, -904(%rbp)
	movq	-904(%rbp), %rdx
	movq	%rdx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -936(%rbp)
	movq	-936(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -928(%rbp)
	movq	-928(%rbp), %rdx
	movq	%rdx, -920(%rbp)
	movq	-920(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rax
	movq	%rax, -968(%rbp)
	movq	-968(%rbp), %rdx
	movq	%rdx, -960(%rbp)
	movq	-960(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -984(%rbp)
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	%rax, -944(%rbp)
	movq	-944(%rbp), %rax
	movq	$0, (%rax)
	movq	-984(%rbp), %rax
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	%rcx, -992(%rbp)
	movq	%rax, -1000(%rbp)
	movb	$1, -1733(%rbp)
	testb	$1, -1733(%rbp)
	jne	LBB11_9
	jmp	LBB11_8
LBB11_5:                                ## %.body
	leaq	-1696(%rbp), %rax
	movq	-1800(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -1728(%rbp)
	movl	-1804(%rbp), %edx       ## 4-byte Reload
	movl	%edx, -1732(%rbp)
	movq	%rax, -1184(%rbp)
	movq	-1184(%rbp), %rax
	movq	%rax, -1176(%rbp)
	movq	-1176(%rbp), %rax
	movq	%rax, -1152(%rbp)
	movq	$0, -1160(%rbp)
	movq	-1152(%rbp), %rax
	movq	%rax, -1144(%rbp)
	movq	-1144(%rbp), %rcx
	movq	%rcx, -1136(%rbp)
	movq	-1136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -1168(%rbp)
	movq	-1160(%rbp), %rcx
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rsi
	movq	%rsi, -1048(%rbp)
	movq	-1048(%rbp), %rsi
	movq	%rcx, (%rsi)
	cmpq	$0, -1168(%rbp)
	movq	%rax, -1816(%rbp)       ## 8-byte Spill
	je	LBB11_7
## BB#6:
	movq	-1816(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	addq	$8, %rcx
	movq	-1168(%rbp), %rdx
	movq	%rcx, -1120(%rbp)
	movq	%rdx, -1128(%rbp)
	movq	-1120(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-1128(%rbp), %rsi
	movq	8(%rcx), %rcx
	movq	%rdx, -1096(%rbp)
	movq	%rsi, -1104(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	-1096(%rbp), %rcx
	movq	-1104(%rbp), %rdx
	movq	-1112(%rbp), %rsi
	movq	%rcx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	movq	%rsi, -1088(%rbp)
	movq	-1080(%rbp), %rcx
	movq	%rcx, -1064(%rbp)
	movq	-1064(%rbp), %rdi
	callq	__ZdlPv
LBB11_7:                                ## %_ZNSt3__110unique_ptrINS_20__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS5_IS7_EEEENS_22__allocator_destructorINS5_IS9_EEEEED1Ev.exit2
	jmp	LBB11_12
LBB11_8:
	movq	-1744(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
LBB11_9:
	leaq	-1696(%rbp), %rax
	movq	%rax, -1640(%rbp)
	movq	-1640(%rbp), %rax
	movq	%rax, -1632(%rbp)
	movq	-1632(%rbp), %rax
	movq	%rax, -1608(%rbp)
	movq	$0, -1616(%rbp)
	movq	-1608(%rbp), %rax
	movq	%rax, -1600(%rbp)
	movq	-1600(%rbp), %rcx
	movq	%rcx, -1592(%rbp)
	movq	-1592(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -1624(%rbp)
	movq	-1616(%rbp), %rcx
	movq	%rax, -1512(%rbp)
	movq	-1512(%rbp), %rdx
	movq	%rdx, -1504(%rbp)
	movq	-1504(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -1624(%rbp)
	movq	%rax, -1824(%rbp)       ## 8-byte Spill
	je	LBB11_11
## BB#10:
	movq	-1824(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1496(%rbp)
	movq	-1496(%rbp), %rcx
	movq	%rcx, -1488(%rbp)
	movq	-1488(%rbp), %rcx
	addq	$8, %rcx
	movq	-1624(%rbp), %rdx
	movq	%rcx, -1576(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	-1576(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-1584(%rbp), %rsi
	movq	8(%rcx), %rcx
	movq	%rdx, -1552(%rbp)
	movq	%rsi, -1560(%rbp)
	movq	%rcx, -1568(%rbp)
	movq	-1552(%rbp), %rcx
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%rcx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rsi, -1544(%rbp)
	movq	-1536(%rbp), %rcx
	movq	%rcx, -1520(%rbp)
	movq	-1520(%rbp), %rdi
	callq	__ZdlPv
LBB11_11:                               ## %_ZNSt3__110unique_ptrINS_20__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS5_IS7_EEEENS_22__allocator_destructorINS5_IS9_EEEEED1Ev.exit
	movq	-1752(%rbp), %rax       ## 8-byte Reload
	addq	$1824, %rsp             ## imm = 0x720
	popq	%rbp
	retq
LBB11_12:
	movq	-1728(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end1:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table11:
Lexception1:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset16 = Lfunc_begin1-Lfunc_begin1      ## >> Call Site 1 <<
	.long	Lset16
Lset17 = Ltmp51-Lfunc_begin1            ##   Call between Lfunc_begin1 and Ltmp51
	.long	Lset17
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset18 = Ltmp51-Lfunc_begin1            ## >> Call Site 2 <<
	.long	Lset18
Lset19 = Ltmp54-Ltmp51                  ##   Call between Ltmp51 and Ltmp54
	.long	Lset19
Lset20 = Ltmp55-Lfunc_begin1            ##     jumps to Ltmp55
	.long	Lset20
	.byte	0                       ##   On action: cleanup
Lset21 = Ltmp54-Lfunc_begin1            ## >> Call Site 3 <<
	.long	Lset21
Lset22 = Lfunc_end1-Ltmp54              ##   Call between Ltmp54 and Lfunc_end1
	.long	Lset22
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	___clang_call_terminate
	.globl	___clang_call_terminate
	.weak_def_can_be_hidden	___clang_call_terminate
	.align	4, 0x90
___clang_call_terminate:                ## @__clang_call_terminate
## BB#0:
	pushq	%rax
	callq	___cxa_begin_catch
	movq	%rax, (%rsp)            ## 8-byte Spill
	callq	__ZSt9terminatev

	.globl	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev
	.weak_def_can_be_hidden	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev
	.align	4, 0x90
__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev: ## @_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp59:
	.cfi_def_cfa_offset 16
Ltmp60:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp61:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED0Ev
	.weak_def_can_be_hidden	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED0Ev
	.align	4, 0x90
__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED0Ev: ## @_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED0Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp62:
	.cfi_def_cfa_offset 16
Ltmp63:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp64:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	callq	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev
	movq	-16(%rbp), %rdi         ## 8-byte Reload
	callq	__ZdlPv
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE16__on_zero_sharedEv
	.weak_def_can_be_hidden	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE16__on_zero_sharedEv
	.align	4, 0x90
__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE16__on_zero_sharedEv: ## @_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE16__on_zero_sharedEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp65:
	.cfi_def_cfa_offset 16
Ltmp66:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp67:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rdi
	addq	$24, %rdi
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__on_zero_shared_weakEv
	.weak_def_can_be_hidden	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__on_zero_shared_weakEv
	.align	4, 0x90
__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__on_zero_shared_weakEv: ## @_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__on_zero_shared_weakEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp68:
	.cfi_def_cfa_offset 16
Ltmp69:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp70:
	.cfi_def_cfa_register %rbp
	subq	$128, %rsp
	leaq	-128(%rbp), %rax
	movq	%rdi, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, %rcx
	addq	$24, %rcx
	movq	%rcx, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rax, -56(%rbp)
	movq	%rcx, -64(%rbp)
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rcx, -40(%rbp)
	movq	%rdx, -48(%rbp)
	movq	%rdi, %rcx
	addq	$24, %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rax, -80(%rbp)
	movq	%rcx, -88(%rbp)
	movq	$1, -96(%rbp)
	movq	-88(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rdi
	callq	__ZdlPv
	addq	$128, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE6lengthEPKc
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE6lengthEPKc
	.align	4, 0x90
__ZNSt3__111char_traitsIcE6lengthEPKc:  ## @_ZNSt3__111char_traitsIcE6lengthEPKc
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp71:
	.cfi_def_cfa_offset 16
Ltmp72:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp73:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	_strlen
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev
	.weak_def_can_be_hidden	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev
	.align	4, 0x90
__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev: ## @_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp74:
	.cfi_def_cfa_offset 16
Ltmp75:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp76:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE@GOTPCREL(%rip), %rax
	addq	$16, %rax
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	addq	$24, %rax
	movq	%rdi, -16(%rbp)         ## 8-byte Spill
	movq	%rax, %rdi
	callq	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED1Ev
	movq	-16(%rbp), %rax         ## 8-byte Reload
	movq	%rax, %rdi
	callq	__ZNSt3__119__shared_weak_countD2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED1Ev
	.weak_def_can_be_hidden	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED1Ev
	.align	4, 0x90
__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED1Ev: ## @_ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED1Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp77:
	.cfi_def_cfa_offset 16
Ltmp78:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp79:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED2Ev
	.weak_def_can_be_hidden	__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED2Ev
	.align	4, 0x90
__ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED2Ev: ## @_ZNSt3__117__compressed_pairINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_ED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp80:
	.cfi_def_cfa_offset 16
Ltmp81:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp82:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__128__libcpp_compressed_pair_impINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_Lj1EED2Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__128__libcpp_compressed_pair_impINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_Lj1EED2Ev
	.weak_def_can_be_hidden	__ZNSt3__128__libcpp_compressed_pair_impINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_Lj1EED2Ev
	.align	4, 0x90
__ZNSt3__128__libcpp_compressed_pair_impINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_Lj1EED2Ev: ## @_ZNSt3__128__libcpp_compressed_pair_impINS_9allocatorINS_12basic_stringIcNS_11char_traitsIcEENS1_IcEEEEEES6_Lj1EED2Ev
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp83:
	.cfi_def_cfa_offset 16
Ltmp84:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp85:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC1ERKS0_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC1ERKS0_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC1ERKS0_:            ## @_ZN7MyClassIiLi1EEC1ERKS0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp86:
	.cfi_def_cfa_offset 16
Ltmp87:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp88:
	.cfi_def_cfa_register %rbp
	subq	$16, %rsp
	movq	%rdi, -8(%rbp)
	movq	%rsi, -16(%rbp)
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	callq	__ZN7MyClassIiLi1EEC2ERKS0_
	addq	$16, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC2ERKS0_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC2ERKS0_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC2ERKS0_:            ## @_ZN7MyClassIiLi1EEC2ERKS0_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp89:
	.cfi_def_cfa_offset 16
Ltmp90:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp91:
	.cfi_def_cfa_register %rbp
	subq	$64, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-40(%rbp), %rsi
	movq	-48(%rbp), %rdi
	movq	%rsi, -24(%rbp)
	movq	%rdi, -32(%rbp)
	movq	-24(%rbp), %rsi
	movq	-32(%rbp), %rdi
	movq	%rsi, -8(%rbp)
	movq	%rdi, -16(%rbp)
	movq	-8(%rbp), %rsi
	movq	-16(%rbp), %rdi
	movq	(%rdi), %rdi
	movq	%rdi, (%rsi)
	movq	-16(%rbp), %rdi
	movq	8(%rdi), %rdi
	movq	%rdi, 8(%rsi)
	cmpq	$0, 8(%rsi)
	movq	%rsi, -56(%rbp)         ## 8-byte Spill
	je	LBB23_2
## BB#1:
	movq	-56(%rbp), %rax         ## 8-byte Reload
	movq	8(%rax), %rdi
	callq	__ZNSt3__119__shared_weak_count12__add_sharedEv
LBB23_2:                                ## %_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEC1ERKS7_.exit
	addq	$64, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZN7MyClassIiLi1EEC2IJRA8_KcEEEDpOT_
	.weak_def_can_be_hidden	__ZN7MyClassIiLi1EEC2IJRA8_KcEEEDpOT_
	.align	4, 0x90
__ZN7MyClassIiLi1EEC2IJRA8_KcEEEDpOT_:  ## @_ZN7MyClassIiLi1EEC2IJRA8_KcEEEDpOT_
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp92:
	.cfi_def_cfa_offset 16
Ltmp93:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp94:
	.cfi_def_cfa_register %rbp
	subq	$48, %rsp
	movq	%rdi, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-32(%rbp), %rdi
	movq	-40(%rbp), %rsi
	movq	%rsi, -24(%rbp)
	movq	-24(%rbp), %rsi
	movq	%rsi, -16(%rbp)
	movq	-16(%rbp), %rsi
	movq	%rsi, -8(%rbp)
	movq	-8(%rbp), %rsi
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA8_KcEEES7_DpOT_
	addq	$48, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA8_KcEEES7_DpOT_
	.weak_def_can_be_hidden	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA8_KcEEES7_DpOT_
	.align	4, 0x90
__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA8_KcEEES7_DpOT_: ## @_ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEE11make_sharedIJRA8_KcEEES7_DpOT_
Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception2
## BB#0:
	pushq	%rbp
Ltmp100:
	.cfi_def_cfa_offset 16
Ltmp101:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp102:
	.cfi_def_cfa_register %rbp
	subq	$1824, %rsp             ## imm = 0x720
	movq	%rdi, %rax
	movq	%rsi, -1664(%rbp)
	leaq	-1672(%rbp), %rsi
	movq	%rsi, -1656(%rbp)
	movq	%rsi, -1648(%rbp)
	movq	%rsi, -1464(%rbp)
	movq	$1, -1472(%rbp)
	movq	$0, -1480(%rbp)
	movq	-1472(%rbp), %rcx
	shlq	$4, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	%rcx, -1456(%rbp)
	movq	%rdi, -1744(%rbp)       ## 8-byte Spill
	movq	%rcx, %rdi
	movq	%rax, -1752(%rbp)       ## 8-byte Spill
	movq	%rsi, -1760(%rbp)       ## 8-byte Spill
	callq	__Znwm
	leaq	-1712(%rbp), %rcx
	movq	%rcx, -1432(%rbp)
	movq	-1760(%rbp), %rsi       ## 8-byte Reload
	movq	%rsi, -1440(%rbp)
	movq	$1, -1448(%rbp)
	movq	-1432(%rbp), %rdi
	movq	-1440(%rbp), %rdx
	movq	%rdi, -1408(%rbp)
	movq	%rdx, -1416(%rbp)
	movq	$1, -1424(%rbp)
	movq	-1408(%rbp), %rdx
	movq	-1416(%rbp), %rdi
	movq	%rdi, (%rdx)
	movq	-1424(%rbp), %rdi
	movq	%rdi, 8(%rdx)
	leaq	-1696(%rbp), %rdx
	movq	%rdx, -1384(%rbp)
	movq	%rax, -1392(%rbp)
	movq	%rcx, -1400(%rbp)
	movq	-1384(%rbp), %rax
	movq	-1392(%rbp), %rdi
	movq	%rax, -1344(%rbp)
	movq	%rdi, -1352(%rbp)
	movq	%rcx, -1360(%rbp)
	movq	-1344(%rbp), %rax
	movq	-1352(%rbp), %rdi
	movq	%rcx, -1336(%rbp)
	movq	-1712(%rbp), %rcx
	movq	-1704(%rbp), %r8
	movq	%r8, -1368(%rbp)
	movq	%rcx, -1376(%rbp)
	movq	-1376(%rbp), %rcx
	movq	-1368(%rbp), %r8
	movq	%rcx, -1312(%rbp)
	movq	%r8, -1304(%rbp)
	movq	%rax, -1320(%rbp)
	movq	%rdi, -1328(%rbp)
	movq	-1320(%rbp), %rax
	movq	-1312(%rbp), %rcx
	movq	-1304(%rbp), %r8
	movq	%rcx, -1264(%rbp)
	movq	%r8, -1256(%rbp)
	movq	%rax, -1272(%rbp)
	movq	%rdi, -1280(%rbp)
	movq	-1272(%rbp), %rax
	leaq	-1280(%rbp), %rcx
	movq	%rcx, -1248(%rbp)
	movq	-1280(%rbp), %rcx
	leaq	-1264(%rbp), %rdi
	movq	%rdi, -1192(%rbp)
	movq	-1264(%rbp), %rdi
	movq	-1256(%rbp), %r8
	movq	%r8, -1288(%rbp)
	movq	%rdi, -1296(%rbp)
	movq	-1296(%rbp), %rdi
	movq	-1288(%rbp), %r8
	movq	%rdi, -1224(%rbp)
	movq	%r8, -1216(%rbp)
	movq	%rax, -1232(%rbp)
	movq	%rcx, -1240(%rbp)
	movq	-1232(%rbp), %rax
	leaq	-1240(%rbp), %rcx
	movq	%rcx, -1208(%rbp)
	movq	-1240(%rbp), %rcx
	movq	%rcx, (%rax)
	leaq	-1224(%rbp), %rcx
	movq	%rcx, -1200(%rbp)
	movq	-1224(%rbp), %rcx
	movq	-1216(%rbp), %rdi
	movq	%rdi, 16(%rax)
	movq	%rcx, 8(%rax)
	movq	%rdx, -1024(%rbp)
	movq	%rdx, -1016(%rbp)
	movq	%rdx, -1008(%rbp)
	movq	-1696(%rbp), %rax
	leaq	-1720(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	%rsi, -40(%rbp)
	movq	-32(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	%rsi, -24(%rbp)
	movq	-1664(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	%rax, -864(%rbp)
	movq	%rcx, -872(%rbp)
	movq	-864(%rbp), %rax
	movq	%rax, -808(%rbp)
	movq	%rcx, -816(%rbp)
	movq	-808(%rbp), %rax
	movq	%rax, -784(%rbp)
	movq	$0, -792(%rbp)
	movq	-784(%rbp), %rcx
	movq	%rcx, -768(%rbp)
	movq	$0, -776(%rbp)
	movq	-768(%rbp), %rdx
	movq	__ZTVNSt3__114__shared_countE@GOTPCREL(%rip), %rdi
	addq	$16, %rdi
	movq	%rdi, (%rdx)
	movq	-776(%rbp), %rdi
	movq	%rdi, 8(%rdx)
	movq	__ZTVNSt3__119__shared_weak_countE@GOTPCREL(%rip), %rdx
	addq	$16, %rdx
	movq	%rdx, (%rcx)
	movq	-792(%rbp), %rdx
	movq	%rdx, 16(%rcx)
	movq	__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE@GOTPCREL(%rip), %rcx
	addq	$16, %rcx
	movq	%rcx, (%rax)
	movq	%rax, %rcx
	addq	$24, %rcx
	leaq	-800(%rbp), %rdx
	movq	%rdx, -480(%rbp)
	movq	%rdx, -464(%rbp)
	leaq	-472(%rbp), %rdi
	movq	%rdi, -448(%rbp)
	movq	%rdx, -456(%rbp)
	movq	-448(%rbp), %rdi
	movq	%rdi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	-400(%rbp), %rdi
	movq	%rdi, -384(%rbp)
	movq	%rdx, -392(%rbp)
	movq	-384(%rbp), %rdi
	movq	%rdi, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	-336(%rbp), %rdi
	movq	%rdx, -296(%rbp)
	movq	%rdi, -280(%rbp)
	movq	%rdx, -288(%rbp)
	movq	-280(%rbp), %rdi
	movq	%rdx, -272(%rbp)
	movq	%rdx, (%rdi)
	movq	-472(%rbp), %rdx
	movq	%rdx, -832(%rbp)
	movq	-816(%rbp), %rdx
	movq	%rdx, -264(%rbp)
	movq	%rdx, -256(%rbp)
	movq	%rdx, -240(%rbp)
	leaq	-248(%rbp), %rdi
	movq	%rdi, -224(%rbp)
	movq	%rdx, -232(%rbp)
	movq	-224(%rbp), %rdi
	movq	%rdi, -176(%rbp)
	movq	%rdx, -184(%rbp)
	movq	-176(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	%rdx, -168(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rdi, -56(%rbp)
	movq	%rdx, -64(%rbp)
	movq	-56(%rbp), %rdi
	movq	%rdx, -48(%rbp)
	movq	%rdx, (%rdi)
	movq	-248(%rbp), %rdx
	movq	%rdx, -840(%rbp)
	movq	-832(%rbp), %rdi
	movq	%rdi, -744(%rbp)
	movq	%rdx, -752(%rbp)
	movq	%rcx, -760(%rbp)
	movq	-744(%rbp), %rdx
	movq	-752(%rbp), %rdi
	movq	%rdx, -672(%rbp)
	movq	%rdi, -680(%rbp)
	movq	%rcx, -688(%rbp)
	leaq	-672(%rbp), %rdx
	movq	%rdx, -656(%rbp)
	movq	-672(%rbp), %rdx
	movq	%rdx, -704(%rbp)
	leaq	-680(%rbp), %rdx
	movq	%rdx, -488(%rbp)
	movq	-680(%rbp), %rdx
	movq	%rdx, -712(%rbp)
	movq	-704(%rbp), %rdi
	movq	%rdi, -616(%rbp)
	movq	%rdx, -624(%rbp)
	movq	%rcx, -648(%rbp)
	leaq	-616(%rbp), %rdx
	movq	%rdx, -600(%rbp)
	movq	%rdx, -592(%rbp)
	movq	-616(%rbp), %rdx
	movq	%rdx, -520(%rbp)
	leaq	-624(%rbp), %rdx
	movq	%rdx, -512(%rbp)
	movq	%rdx, -504(%rbp)
	movq	-624(%rbp), %rdx
	movq	%rdx, -496(%rbp)
	movq	%rcx, -576(%rbp)
	movq	%rdx, -584(%rbp)
	movq	-576(%rbp), %rcx
	movq	%rcx, -560(%rbp)
	movq	%rdx, -568(%rbp)
	movq	-560(%rbp), %rcx
	movq	%rcx, -552(%rbp)
	movq	%rcx, -544(%rbp)
	movq	%rcx, -536(%rbp)
	movq	%rcx, -528(%rbp)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	movq	$0, (%rcx)
	movq	-568(%rbp), %rdx
Ltmp95:
	movq	%rdx, %rdi
	movq	%rax, -1768(%rbp)       ## 8-byte Spill
	movq	%rcx, -1776(%rbp)       ## 8-byte Spill
	movq	%rdx, -1784(%rbp)       ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE6lengthEPKc
Ltmp96:
	movq	%rax, -1792(%rbp)       ## 8-byte Spill
	jmp	LBB25_1
LBB25_1:                                ## %.noexc.i.i
Ltmp97:
	movq	-1776(%rbp), %rdi       ## 8-byte Reload
	movq	-1784(%rbp), %rsi       ## 8-byte Reload
	movq	-1792(%rbp), %rdx       ## 8-byte Reload
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEPKcm
Ltmp98:
	jmp	LBB25_3
LBB25_2:
Ltmp99:
	movl	%edx, %ecx
	movq	%rax, -848(%rbp)
	movl	%ecx, -852(%rbp)
	movq	-1768(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__119__shared_weak_countD2Ev
	movq	-848(%rbp), %rax
	movl	-852(%rbp), %ecx
	movq	%rax, -1800(%rbp)       ## 8-byte Spill
	movl	%ecx, -1804(%rbp)       ## 4-byte Spill
	jmp	LBB25_5
LBB25_3:                                ## %_ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEC1IJRA8_KcEEES7_DpOT_.exit
	jmp	LBB25_4
LBB25_4:
	leaq	-1696(%rbp), %rax
	movb	$0, -1733(%rbp)
	movq	-1744(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -888(%rbp)
	movq	-888(%rbp), %rdx
	movq	%rdx, -880(%rbp)
	movq	-880(%rbp), %rdx
	movq	$0, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rax, -912(%rbp)
	movq	-912(%rbp), %rdx
	movq	%rdx, -904(%rbp)
	movq	-904(%rbp), %rdx
	movq	%rdx, -896(%rbp)
	movq	-896(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -936(%rbp)
	movq	-936(%rbp), %rdx
	addq	$24, %rdx
	movq	%rdx, -928(%rbp)
	movq	-928(%rbp), %rdx
	movq	%rdx, -920(%rbp)
	movq	-920(%rbp), %rdx
	movq	%rdx, (%rcx)
	movq	%rax, -976(%rbp)
	movq	-976(%rbp), %rax
	movq	%rax, -968(%rbp)
	movq	-968(%rbp), %rdx
	movq	%rdx, -960(%rbp)
	movq	-960(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, -984(%rbp)
	movq	%rax, -952(%rbp)
	movq	-952(%rbp), %rax
	movq	%rax, -944(%rbp)
	movq	-944(%rbp), %rax
	movq	$0, (%rax)
	movq	-984(%rbp), %rax
	movq	%rax, 8(%rcx)
	movq	(%rcx), %rax
	movq	%rcx, -992(%rbp)
	movq	%rax, -1000(%rbp)
	movb	$1, -1733(%rbp)
	testb	$1, -1733(%rbp)
	jne	LBB25_9
	jmp	LBB25_8
LBB25_5:                                ## %.body
	leaq	-1696(%rbp), %rax
	movq	-1800(%rbp), %rcx       ## 8-byte Reload
	movq	%rcx, -1728(%rbp)
	movl	-1804(%rbp), %edx       ## 4-byte Reload
	movl	%edx, -1732(%rbp)
	movq	%rax, -1184(%rbp)
	movq	-1184(%rbp), %rax
	movq	%rax, -1176(%rbp)
	movq	-1176(%rbp), %rax
	movq	%rax, -1152(%rbp)
	movq	$0, -1160(%rbp)
	movq	-1152(%rbp), %rax
	movq	%rax, -1144(%rbp)
	movq	-1144(%rbp), %rcx
	movq	%rcx, -1136(%rbp)
	movq	-1136(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -1168(%rbp)
	movq	-1160(%rbp), %rcx
	movq	%rax, -1056(%rbp)
	movq	-1056(%rbp), %rsi
	movq	%rsi, -1048(%rbp)
	movq	-1048(%rbp), %rsi
	movq	%rcx, (%rsi)
	cmpq	$0, -1168(%rbp)
	movq	%rax, -1816(%rbp)       ## 8-byte Spill
	je	LBB25_7
## BB#6:
	movq	-1816(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1040(%rbp)
	movq	-1040(%rbp), %rcx
	movq	%rcx, -1032(%rbp)
	movq	-1032(%rbp), %rcx
	addq	$8, %rcx
	movq	-1168(%rbp), %rdx
	movq	%rcx, -1120(%rbp)
	movq	%rdx, -1128(%rbp)
	movq	-1120(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-1128(%rbp), %rsi
	movq	8(%rcx), %rcx
	movq	%rdx, -1096(%rbp)
	movq	%rsi, -1104(%rbp)
	movq	%rcx, -1112(%rbp)
	movq	-1096(%rbp), %rcx
	movq	-1104(%rbp), %rdx
	movq	-1112(%rbp), %rsi
	movq	%rcx, -1072(%rbp)
	movq	%rdx, -1080(%rbp)
	movq	%rsi, -1088(%rbp)
	movq	-1080(%rbp), %rcx
	movq	%rcx, -1064(%rbp)
	movq	-1064(%rbp), %rdi
	callq	__ZdlPv
LBB25_7:                                ## %_ZNSt3__110unique_ptrINS_20__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS5_IS7_EEEENS_22__allocator_destructorINS5_IS9_EEEEED1Ev.exit2
	jmp	LBB25_12
LBB25_8:
	movq	-1744(%rbp), %rdi       ## 8-byte Reload
	callq	__ZNSt3__110shared_ptrINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEED1Ev
LBB25_9:
	leaq	-1696(%rbp), %rax
	movq	%rax, -1640(%rbp)
	movq	-1640(%rbp), %rax
	movq	%rax, -1632(%rbp)
	movq	-1632(%rbp), %rax
	movq	%rax, -1608(%rbp)
	movq	$0, -1616(%rbp)
	movq	-1608(%rbp), %rax
	movq	%rax, -1600(%rbp)
	movq	-1600(%rbp), %rcx
	movq	%rcx, -1592(%rbp)
	movq	-1592(%rbp), %rcx
	movq	(%rcx), %rcx
	movq	%rcx, -1624(%rbp)
	movq	-1616(%rbp), %rcx
	movq	%rax, -1512(%rbp)
	movq	-1512(%rbp), %rdx
	movq	%rdx, -1504(%rbp)
	movq	-1504(%rbp), %rdx
	movq	%rcx, (%rdx)
	cmpq	$0, -1624(%rbp)
	movq	%rax, -1824(%rbp)       ## 8-byte Spill
	je	LBB25_11
## BB#10:
	movq	-1824(%rbp), %rax       ## 8-byte Reload
	movq	%rax, -1496(%rbp)
	movq	-1496(%rbp), %rcx
	movq	%rcx, -1488(%rbp)
	movq	-1488(%rbp), %rcx
	addq	$8, %rcx
	movq	-1624(%rbp), %rdx
	movq	%rcx, -1576(%rbp)
	movq	%rdx, -1584(%rbp)
	movq	-1576(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-1584(%rbp), %rsi
	movq	8(%rcx), %rcx
	movq	%rdx, -1552(%rbp)
	movq	%rsi, -1560(%rbp)
	movq	%rcx, -1568(%rbp)
	movq	-1552(%rbp), %rcx
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%rcx, -1528(%rbp)
	movq	%rdx, -1536(%rbp)
	movq	%rsi, -1544(%rbp)
	movq	-1536(%rbp), %rcx
	movq	%rcx, -1520(%rbp)
	movq	-1520(%rbp), %rdi
	callq	__ZdlPv
LBB25_11:                               ## %_ZNSt3__110unique_ptrINS_20__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS5_IS7_EEEENS_22__allocator_destructorINS5_IS9_EEEEED1Ev.exit
	movq	-1752(%rbp), %rax       ## 8-byte Reload
	addq	$1824, %rsp             ## imm = 0x720
	popq	%rbp
	retq
LBB25_12:
	movq	-1728(%rbp), %rdi
	callq	__Unwind_Resume
Lfunc_end2:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table25:
Lexception2:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset23 = Lfunc_begin2-Lfunc_begin2      ## >> Call Site 1 <<
	.long	Lset23
Lset24 = Ltmp95-Lfunc_begin2            ##   Call between Lfunc_begin2 and Ltmp95
	.long	Lset24
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset25 = Ltmp95-Lfunc_begin2            ## >> Call Site 2 <<
	.long	Lset25
Lset26 = Ltmp98-Ltmp95                  ##   Call between Ltmp95 and Ltmp98
	.long	Lset26
Lset27 = Ltmp99-Lfunc_begin2            ##     jumps to Ltmp99
	.long	Lset27
	.byte	0                       ##   On action: cleanup
Lset28 = Ltmp98-Lfunc_begin2            ## >> Call Site 3 <<
	.long	Lset28
Lset29 = Lfunc_end2-Ltmp98              ##   Call between Ltmp98 and Lfunc_end2
	.long	Lset29
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.weak_def_can_be_hidden	__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.align	4, 0x90
__ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE: ## @_ZNSt3__1lsIcNS_11char_traitsIcEENS_9allocatorIcEEEERNS_13basic_ostreamIT_T0_EES9_RKNS_12basic_stringIS6_S7_T1_EE
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp103:
	.cfi_def_cfa_offset 16
Ltmp104:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp105:
	.cfi_def_cfa_register %rbp
	subq	$256, %rsp              ## imm = 0x100
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	-200(%rbp), %rdi
	movq	-208(%rbp), %rsi
	movq	%rsi, -192(%rbp)
	movq	-192(%rbp), %rsi
	movq	%rsi, -184(%rbp)
	movq	-184(%rbp), %rsi
	movq	%rsi, -176(%rbp)
	movq	-176(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-168(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-160(%rbp), %rax
	movzbl	(%rax), %ecx
	andl	$1, %ecx
	cmpl	$0, %ecx
	movq	%rdi, -216(%rbp)        ## 8-byte Spill
	movq	%rsi, -224(%rbp)        ## 8-byte Spill
	je	LBB26_2
## BB#1:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -112(%rbp)
	movq	-112(%rbp), %rcx
	movq	%rcx, -104(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
	jmp	LBB26_3
LBB26_2:
	movq	-224(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -152(%rbp)
	movq	-152(%rbp), %rcx
	movq	%rcx, -144(%rbp)
	movq	-144(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rcx
	movq	%rcx, -232(%rbp)        ## 8-byte Spill
LBB26_3:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-232(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rsi
	movq	-208(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -56(%rbp)
	movq	-56(%rbp), %rcx
	movzbl	(%rcx), %edx
	andl	$1, %edx
	cmpl	$0, %edx
	movq	%rsi, -240(%rbp)        ## 8-byte Spill
	movq	%rax, -248(%rbp)        ## 8-byte Spill
	je	LBB26_5
## BB#4:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -24(%rbp)
	movq	-24(%rbp), %rcx
	movq	%rcx, -16(%rbp)
	movq	-16(%rbp), %rcx
	movq	%rcx, -8(%rbp)
	movq	-8(%rbp), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
	jmp	LBB26_6
LBB26_5:
	movq	-248(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	%rcx, -32(%rbp)
	movq	-32(%rbp), %rcx
	movzbl	(%rcx), %edx
	sarl	$1, %edx
	movslq	%edx, %rcx
	movq	%rcx, -256(%rbp)        ## 8-byte Spill
LBB26_6:                                ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4sizeEv.exit
	movq	-256(%rbp), %rax        ## 8-byte Reload
	movq	-216(%rbp), %rdi        ## 8-byte Reload
	movq	-240(%rbp), %rsi        ## 8-byte Reload
	movq	%rax, %rdx
	callq	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	addq	$256, %rsp              ## imm = 0x100
	popq	%rbp
	retq
	.cfi_endproc

	.private_extern	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.globl	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.weak_definition	__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.align	4, 0x90
__ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_: ## @_ZNSt3__14endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception3
## BB#0:
	pushq	%rbp
Ltmp111:
	.cfi_def_cfa_offset 16
Ltmp112:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp113:
	.cfi_def_cfa_register %rbp
	subq	$144, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rdi, %rax
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
	movq	%rdi, -32(%rbp)
	movb	$10, -33(%rbp)
	movq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rcx
	movq	%rcx, %rdi
	movq	%rax, -80(%rbp)         ## 8-byte Spill
	movq	%rcx, -88(%rbp)         ## 8-byte Spill
	callq	__ZNKSt3__18ios_base6getlocEv
	movq	-88(%rbp), %rax         ## 8-byte Reload
	movq	%rax, -24(%rbp)
Ltmp106:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp107:
	movq	%rax, -96(%rbp)         ## 8-byte Spill
	jmp	LBB27_1
LBB27_1:                                ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i
	movb	-33(%rbp), %al
	movq	-96(%rbp), %rcx         ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp108:
	movl	%edi, -100(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-100(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -112(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-112(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp109:
	movb	%al, -113(%rbp)         ## 1-byte Spill
	jmp	LBB27_3
LBB27_2:
Ltmp110:
	leaq	-48(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rdi
	callq	__Unwind_Resume
LBB27_3:                                ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-80(%rbp), %rdi         ## 8-byte Reload
	movb	-113(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %esi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE3putEc
	movq	-72(%rbp), %rdi
	movq	%rax, -128(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE5flushEv
	movq	-72(%rbp), %rdi
	movq	%rax, -136(%rbp)        ## 8-byte Spill
	movq	%rdi, %rax
	addq	$144, %rsp
	popq	%rbp
	retq
Lfunc_end3:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table27:
Lexception3:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset30 = Lfunc_begin3-Lfunc_begin3      ## >> Call Site 1 <<
	.long	Lset30
Lset31 = Ltmp106-Lfunc_begin3           ##   Call between Lfunc_begin3 and Ltmp106
	.long	Lset31
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset32 = Ltmp106-Lfunc_begin3           ## >> Call Site 2 <<
	.long	Lset32
Lset33 = Ltmp109-Ltmp106                ##   Call between Ltmp106 and Ltmp109
	.long	Lset33
Lset34 = Ltmp110-Lfunc_begin3           ##     jumps to Ltmp110
	.long	Lset34
	.byte	0                       ##   On action: cleanup
Lset35 = Ltmp109-Lfunc_begin3           ## >> Call Site 3 <<
	.long	Lset35
Lset36 = Lfunc_end3-Ltmp109             ##   Call between Ltmp109 and Lfunc_end3
	.long	Lset36
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.weak_def_can_be_hidden	__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
	.align	4, 0x90
__ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m: ## @_ZNSt3__124__put_character_sequenceIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_PKS4_m
Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception4
## BB#0:
	pushq	%rbp
Ltmp135:
	.cfi_def_cfa_offset 16
Ltmp136:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp137:
	.cfi_def_cfa_register %rbp
	subq	$384, %rsp              ## imm = 0x180
	movq	%rdi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movq	%rdx, -200(%rbp)
	movq	-184(%rbp), %rsi
Ltmp114:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryC1ERS3_
Ltmp115:
	jmp	LBB28_1
LBB28_1:
	leaq	-216(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-176(%rbp), %rax
	movb	(%rax), %cl
	movb	%cl, -249(%rbp)         ## 1-byte Spill
## BB#2:
	movb	-249(%rbp), %al         ## 1-byte Reload
	testb	$1, %al
	jne	LBB28_3
	jmp	LBB28_26
LBB28_3:
	leaq	-240(%rbp), %rax
	movq	-184(%rbp), %rcx
	movq	%rax, -160(%rbp)
	movq	%rcx, -168(%rbp)
	movq	-160(%rbp), %rax
	movq	-168(%rbp), %rcx
	movq	%rax, -144(%rbp)
	movq	%rcx, -152(%rbp)
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %rcx
	movq	(%rcx), %rdx
	movq	-24(%rdx), %rdx
	addq	%rdx, %rcx
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	40(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	-192(%rbp), %rsi
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -88(%rbp)
	movq	-88(%rbp), %rax
	movl	8(%rax), %edi
	movq	%rsi, -264(%rbp)        ## 8-byte Spill
	movl	%edi, -268(%rbp)        ## 4-byte Spill
## BB#4:
	movl	-268(%rbp), %eax        ## 4-byte Reload
	andl	$176, %eax
	cmpl	$32, %eax
	jne	LBB28_6
## BB#5:
	movq	-192(%rbp), %rax
	addq	-200(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
	jmp	LBB28_7
LBB28_6:
	movq	-192(%rbp), %rax
	movq	%rax, -280(%rbp)        ## 8-byte Spill
LBB28_7:
	movq	-280(%rbp), %rax        ## 8-byte Reload
	movq	-192(%rbp), %rcx
	addq	-200(%rbp), %rcx
	movq	-184(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	-24(%rsi), %rsi
	addq	%rsi, %rdx
	movq	-184(%rbp), %rsi
	movq	(%rsi), %rdi
	movq	-24(%rdi), %rdi
	addq	%rdi, %rsi
	movq	%rsi, -72(%rbp)
	movq	-72(%rbp), %rsi
	movq	%rax, -288(%rbp)        ## 8-byte Spill
	movq	%rcx, -296(%rbp)        ## 8-byte Spill
	movq	%rdx, -304(%rbp)        ## 8-byte Spill
	movq	%rsi, -312(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__111char_traitsIcE3eofEv
	movq	-312(%rbp), %rcx        ## 8-byte Reload
	movl	144(%rcx), %esi
	movl	%eax, %edi
	callq	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	testb	$1, %al
	jne	LBB28_8
	jmp	LBB28_13
LBB28_8:
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movb	$32, -33(%rbp)
	movq	-32(%rbp), %rsi
Ltmp117:
	leaq	-48(%rbp), %rdi
	callq	__ZNKSt3__18ios_base6getlocEv
Ltmp118:
	jmp	LBB28_9
LBB28_9:                                ## %.noexc
	leaq	-48(%rbp), %rax
	movq	%rax, -24(%rbp)
Ltmp119:
	movq	__ZNSt3__15ctypeIcE2idE@GOTPCREL(%rip), %rsi
	movq	%rax, %rdi
	callq	__ZNKSt3__16locale9use_facetERNS0_2idE
Ltmp120:
	movq	%rax, -320(%rbp)        ## 8-byte Spill
	jmp	LBB28_10
LBB28_10:                               ## %_ZNSt3__19use_facetINS_5ctypeIcEEEERKT_RKNS_6localeE.exit.i.i
	movb	-33(%rbp), %al
	movq	-320(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -8(%rbp)
	movb	%al, -9(%rbp)
	movq	-8(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	56(%rsi), %rsi
	movsbl	-9(%rbp), %edi
Ltmp121:
	movl	%edi, -324(%rbp)        ## 4-byte Spill
	movq	%rdx, %rdi
	movl	-324(%rbp), %r8d        ## 4-byte Reload
	movq	%rsi, -336(%rbp)        ## 8-byte Spill
	movl	%r8d, %esi
	movq	-336(%rbp), %rdx        ## 8-byte Reload
	callq	*%rdx
Ltmp122:
	movb	%al, -337(%rbp)         ## 1-byte Spill
	jmp	LBB28_12
LBB28_11:
Ltmp123:
	movl	%edx, %ecx
	movq	%rax, -56(%rbp)
	movl	%ecx, -60(%rbp)
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movq	-56(%rbp), %rax
	movl	-60(%rbp), %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB28_21
LBB28_12:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE5widenEc.exit.i
	leaq	-48(%rbp), %rdi
	callq	__ZNSt3__16localeD1Ev
	movb	-337(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %ecx
	movq	-312(%rbp), %rdi        ## 8-byte Reload
	movl	%ecx, 144(%rdi)
LBB28_13:                               ## %_ZNKSt3__19basic_iosIcNS_11char_traitsIcEEE4fillEv.exit
	movq	-312(%rbp), %rax        ## 8-byte Reload
	movl	144(%rax), %ecx
	movb	%cl, %dl
	movb	%dl, -357(%rbp)         ## 1-byte Spill
## BB#14:
	movq	-240(%rbp), %rdi
Ltmp124:
	movb	-357(%rbp), %al         ## 1-byte Reload
	movsbl	%al, %r9d
	movq	-264(%rbp), %rsi        ## 8-byte Reload
	movq	-288(%rbp), %rdx        ## 8-byte Reload
	movq	-296(%rbp), %rcx        ## 8-byte Reload
	movq	-304(%rbp), %r8         ## 8-byte Reload
	callq	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Ltmp125:
	movq	%rax, -368(%rbp)        ## 8-byte Spill
	jmp	LBB28_15
LBB28_15:
	leaq	-248(%rbp), %rax
	movq	-368(%rbp), %rcx        ## 8-byte Reload
	movq	%rcx, -248(%rbp)
	movq	%rax, -80(%rbp)
	movq	-80(%rbp), %rax
	cmpq	$0, (%rax)
	jne	LBB28_25
## BB#16:
	movq	-184(%rbp), %rax
	movq	(%rax), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rax
	movq	%rax, -112(%rbp)
	movl	$5, -116(%rbp)
	movq	-112(%rbp), %rax
	movq	%rax, -96(%rbp)
	movl	$5, -100(%rbp)
	movq	-96(%rbp), %rax
	movl	32(%rax), %edx
	orl	$5, %edx
Ltmp126:
	movq	%rax, %rdi
	movl	%edx, %esi
	callq	__ZNSt3__18ios_base5clearEj
Ltmp127:
	jmp	LBB28_17
LBB28_17:                               ## %_ZNSt3__19basic_iosIcNS_11char_traitsIcEEE8setstateEj.exit
	jmp	LBB28_18
LBB28_18:
	jmp	LBB28_25
LBB28_19:
Ltmp116:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
	jmp	LBB28_22
LBB28_20:
Ltmp128:
	movl	%edx, %ecx
	movq	%rax, -352(%rbp)        ## 8-byte Spill
	movl	%ecx, -356(%rbp)        ## 4-byte Spill
	jmp	LBB28_21
LBB28_21:                               ## %.body
	movl	-356(%rbp), %eax        ## 4-byte Reload
	movq	-352(%rbp), %rcx        ## 8-byte Reload
	leaq	-216(%rbp), %rdi
	movq	%rcx, -224(%rbp)
	movl	%eax, -228(%rbp)
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
LBB28_22:
	movq	-224(%rbp), %rdi
	callq	___cxa_begin_catch
	movq	-184(%rbp), %rdi
	movq	(%rdi), %rcx
	movq	-24(%rcx), %rcx
	addq	%rcx, %rdi
Ltmp129:
	movq	%rax, -376(%rbp)        ## 8-byte Spill
	callq	__ZNSt3__18ios_base33__set_badbit_and_consider_rethrowEv
Ltmp130:
	jmp	LBB28_23
LBB28_23:
	callq	___cxa_end_catch
LBB28_24:
	movq	-184(%rbp), %rax
	addq	$384, %rsp              ## imm = 0x180
	popq	%rbp
	retq
LBB28_25:
	jmp	LBB28_26
LBB28_26:
	leaq	-216(%rbp), %rdi
	callq	__ZNSt3__113basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	jmp	LBB28_24
LBB28_27:
Ltmp131:
	movl	%edx, %ecx
	movq	%rax, -224(%rbp)
	movl	%ecx, -228(%rbp)
Ltmp132:
	callq	___cxa_end_catch
Ltmp133:
	jmp	LBB28_28
LBB28_28:
	jmp	LBB28_29
LBB28_29:
	movq	-224(%rbp), %rdi
	callq	__Unwind_Resume
LBB28_30:
Ltmp134:
	movl	%edx, %ecx
	movq	%rax, %rdi
	movl	%ecx, -380(%rbp)        ## 4-byte Spill
	callq	___clang_call_terminate
Lfunc_end4:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table28:
Lexception4:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.asciz	"\201\201\200\200"      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	117                     ## Call site table length
Lset37 = Ltmp114-Lfunc_begin4           ## >> Call Site 1 <<
	.long	Lset37
Lset38 = Ltmp115-Ltmp114                ##   Call between Ltmp114 and Ltmp115
	.long	Lset38
Lset39 = Ltmp116-Lfunc_begin4           ##     jumps to Ltmp116
	.long	Lset39
	.byte	5                       ##   On action: 3
Lset40 = Ltmp117-Lfunc_begin4           ## >> Call Site 2 <<
	.long	Lset40
Lset41 = Ltmp118-Ltmp117                ##   Call between Ltmp117 and Ltmp118
	.long	Lset41
Lset42 = Ltmp128-Lfunc_begin4           ##     jumps to Ltmp128
	.long	Lset42
	.byte	5                       ##   On action: 3
Lset43 = Ltmp119-Lfunc_begin4           ## >> Call Site 3 <<
	.long	Lset43
Lset44 = Ltmp122-Ltmp119                ##   Call between Ltmp119 and Ltmp122
	.long	Lset44
Lset45 = Ltmp123-Lfunc_begin4           ##     jumps to Ltmp123
	.long	Lset45
	.byte	3                       ##   On action: 2
Lset46 = Ltmp124-Lfunc_begin4           ## >> Call Site 4 <<
	.long	Lset46
Lset47 = Ltmp127-Ltmp124                ##   Call between Ltmp124 and Ltmp127
	.long	Lset47
Lset48 = Ltmp128-Lfunc_begin4           ##     jumps to Ltmp128
	.long	Lset48
	.byte	5                       ##   On action: 3
Lset49 = Ltmp127-Lfunc_begin4           ## >> Call Site 5 <<
	.long	Lset49
Lset50 = Ltmp129-Ltmp127                ##   Call between Ltmp127 and Ltmp129
	.long	Lset50
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset51 = Ltmp129-Lfunc_begin4           ## >> Call Site 6 <<
	.long	Lset51
Lset52 = Ltmp130-Ltmp129                ##   Call between Ltmp129 and Ltmp130
	.long	Lset52
Lset53 = Ltmp131-Lfunc_begin4           ##     jumps to Ltmp131
	.long	Lset53
	.byte	0                       ##   On action: cleanup
Lset54 = Ltmp130-Lfunc_begin4           ## >> Call Site 7 <<
	.long	Lset54
Lset55 = Ltmp132-Ltmp130                ##   Call between Ltmp130 and Ltmp132
	.long	Lset55
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset56 = Ltmp132-Lfunc_begin4           ## >> Call Site 8 <<
	.long	Lset56
Lset57 = Ltmp133-Ltmp132                ##   Call between Ltmp132 and Ltmp133
	.long	Lset57
Lset58 = Ltmp134-Lfunc_begin4           ##     jumps to Ltmp134
	.long	Lset58
	.byte	5                       ##   On action: 3
Lset59 = Ltmp133-Lfunc_begin4           ## >> Call Site 9 <<
	.long	Lset59
Lset60 = Lfunc_end4-Ltmp133             ##   Call between Ltmp133 and Lfunc_end4
	.long	Lset60
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.byte	0                       ## >> Action Record 1 <<
                                        ##   Cleanup
	.byte	0                       ##   No further actions
	.byte	1                       ## >> Action Record 2 <<
                                        ##   Catch TypeInfo 1
	.byte	125                     ##   Continue to action 1
	.byte	1                       ## >> Action Record 3 <<
                                        ##   Catch TypeInfo 1
	.byte	0                       ##   No further actions
                                        ## >> Catch TypeInfos <<
	.long	0                       ## TypeInfo 1
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.private_extern	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.globl	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.weak_def_can_be_hidden	__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
	.align	4, 0x90
__ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_: ## @_ZNSt3__116__pad_and_outputIcNS_11char_traitsIcEEEENS_19ostreambuf_iteratorIT_T0_EES6_PKS4_S8_S8_RNS_8ios_baseES4_
Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 155, ___gxx_personality_v0
	.cfi_lsda 16, Lexception5
## BB#0:
	pushq	%rbp
Ltmp141:
	.cfi_def_cfa_offset 16
Ltmp142:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp143:
	.cfi_def_cfa_register %rbp
	subq	$480, %rsp              ## imm = 0x1E0
	movb	%r9b, %al
	movq	%rdi, -312(%rbp)
	movq	%rsi, -320(%rbp)
	movq	%rdx, -328(%rbp)
	movq	%rcx, -336(%rbp)
	movq	%r8, -344(%rbp)
	movb	%al, -345(%rbp)
	cmpq	$0, -312(%rbp)
	jne	LBB29_2
## BB#1:
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB29_26
LBB29_2:
	movq	-336(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -360(%rbp)
	movq	-344(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	-296(%rbp), %rax
	movq	24(%rax), %rax
	movq	%rax, -368(%rbp)
	movq	-368(%rbp), %rax
	cmpq	-360(%rbp), %rax
	jle	LBB29_4
## BB#3:
	movq	-360(%rbp), %rax
	movq	-368(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, -368(%rbp)
	jmp	LBB29_5
LBB29_4:
	movq	$0, -368(%rbp)
LBB29_5:
	movq	-328(%rbp), %rax
	movq	-320(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB29_9
## BB#6:
	movq	-312(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -224(%rbp)
	movq	%rcx, -232(%rbp)
	movq	%rdx, -240(%rbp)
	movq	-224(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-232(%rbp), %rsi
	movq	-240(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB29_8
## BB#7:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB29_26
LBB29_8:
	jmp	LBB29_9
LBB29_9:
	cmpq	$0, -368(%rbp)
	jle	LBB29_21
## BB#10:
	xorl	%esi, %esi
	movl	$24, %eax
	movl	%eax, %edx
	leaq	-400(%rbp), %rcx
	movq	-368(%rbp), %rdi
	movb	-345(%rbp), %r8b
	movq	%rcx, -200(%rbp)
	movq	%rdi, -208(%rbp)
	movb	%r8b, -209(%rbp)
	movq	-200(%rbp), %rcx
	movq	-208(%rbp), %rdi
	movb	-209(%rbp), %r8b
	movq	%rcx, -176(%rbp)
	movq	%rdi, -184(%rbp)
	movb	%r8b, -185(%rbp)
	movq	-176(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	movq	-168(%rbp), %rdi
	movq	%rdi, -160(%rbp)
	movq	-160(%rbp), %rdi
	movq	%rdi, -152(%rbp)
	movq	-152(%rbp), %rdi
	movq	%rdi, %r9
	movq	%r9, -144(%rbp)
	movq	%rcx, -424(%rbp)        ## 8-byte Spill
	callq	_memset
	movq	-184(%rbp), %rsi
	movq	-424(%rbp), %rdi        ## 8-byte Reload
	movsbl	-185(%rbp), %edx
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6__initEmc
	leaq	-400(%rbp), %rcx
	movq	-312(%rbp), %rsi
	movq	%rcx, -136(%rbp)
	movq	-136(%rbp), %rcx
	movq	%rcx, -128(%rbp)
	movq	-128(%rbp), %rcx
	movq	%rcx, -120(%rbp)
	movq	-120(%rbp), %rdi
	movq	%rdi, -112(%rbp)
	movq	-112(%rbp), %rdi
	movq	%rdi, -104(%rbp)
	movq	-104(%rbp), %rdi
	movzbl	(%rdi), %eax
	andl	$1, %eax
	cmpl	$0, %eax
	movq	%rsi, -432(%rbp)        ## 8-byte Spill
	movq	%rcx, -440(%rbp)        ## 8-byte Spill
	je	LBB29_12
## BB#11:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rcx
	movq	%rcx, -48(%rbp)
	movq	-48(%rbp), %rcx
	movq	%rcx, -40(%rbp)
	movq	-40(%rbp), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
	jmp	LBB29_13
LBB29_12:
	movq	-440(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -96(%rbp)
	movq	-96(%rbp), %rcx
	movq	%rcx, -88(%rbp)
	movq	-88(%rbp), %rcx
	movq	%rcx, -80(%rbp)
	movq	-80(%rbp), %rcx
	addq	$1, %rcx
	movq	%rcx, -72(%rbp)
	movq	-72(%rbp), %rcx
	movq	%rcx, -64(%rbp)
	movq	-64(%rbp), %rcx
	movq	%rcx, -448(%rbp)        ## 8-byte Spill
LBB29_13:                               ## %_ZNKSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE4dataEv.exit
	movq	-448(%rbp), %rax        ## 8-byte Reload
	movq	%rax, -32(%rbp)
	movq	-368(%rbp), %rcx
	movq	-432(%rbp), %rdx        ## 8-byte Reload
	movq	%rdx, -8(%rbp)
	movq	%rax, -16(%rbp)
	movq	%rcx, -24(%rbp)
	movq	-8(%rbp), %rax
	movq	(%rax), %rsi
	movq	96(%rsi), %rsi
	movq	-16(%rbp), %rdi
Ltmp138:
	movq	%rdi, -456(%rbp)        ## 8-byte Spill
	movq	%rax, %rdi
	movq	-456(%rbp), %rax        ## 8-byte Reload
	movq	%rsi, -464(%rbp)        ## 8-byte Spill
	movq	%rax, %rsi
	movq	%rcx, %rdx
	movq	-464(%rbp), %rcx        ## 8-byte Reload
	callq	*%rcx
Ltmp139:
	movq	%rax, -472(%rbp)        ## 8-byte Spill
	jmp	LBB29_14
LBB29_14:                               ## %_ZNSt3__115basic_streambufIcNS_11char_traitsIcEEE5sputnEPKcl.exit
	jmp	LBB29_15
LBB29_15:
	movq	-472(%rbp), %rax        ## 8-byte Reload
	cmpq	-368(%rbp), %rax
	je	LBB29_18
## BB#16:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	movl	$1, -416(%rbp)
	jmp	LBB29_19
LBB29_17:
Ltmp140:
	leaq	-400(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rax, -408(%rbp)
	movl	%ecx, -412(%rbp)
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	jmp	LBB29_27
LBB29_18:
	movl	$0, -416(%rbp)
LBB29_19:
	leaq	-400(%rbp), %rdi
	callq	__ZNSt3__112basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	movl	-416(%rbp), %eax
	testl	%eax, %eax
	movl	%eax, -476(%rbp)        ## 4-byte Spill
	je	LBB29_20
	jmp	LBB29_29
LBB29_29:
	movl	-476(%rbp), %eax        ## 4-byte Reload
	subl	$1, %eax
	movl	%eax, -480(%rbp)        ## 4-byte Spill
	je	LBB29_26
	jmp	LBB29_28
LBB29_20:
	jmp	LBB29_21
LBB29_21:
	movq	-336(%rbp), %rax
	movq	-328(%rbp), %rcx
	subq	%rcx, %rax
	movq	%rax, -376(%rbp)
	cmpq	$0, -376(%rbp)
	jle	LBB29_25
## BB#22:
	movq	-312(%rbp), %rax
	movq	-328(%rbp), %rcx
	movq	-376(%rbp), %rdx
	movq	%rax, -248(%rbp)
	movq	%rcx, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	-248(%rbp), %rax
	movq	(%rax), %rcx
	movq	96(%rcx), %rcx
	movq	-256(%rbp), %rsi
	movq	-264(%rbp), %rdx
	movq	%rax, %rdi
	callq	*%rcx
	cmpq	-376(%rbp), %rax
	je	LBB29_24
## BB#23:
	movq	$0, -312(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
	jmp	LBB29_26
LBB29_24:
	jmp	LBB29_25
LBB29_25:
	movq	-344(%rbp), %rax
	movq	%rax, -272(%rbp)
	movq	$0, -280(%rbp)
	movq	-272(%rbp), %rax
	movq	24(%rax), %rcx
	movq	%rcx, -288(%rbp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rax)
	movq	-312(%rbp), %rax
	movq	%rax, -304(%rbp)
LBB29_26:
	movq	-304(%rbp), %rax
	addq	$480, %rsp              ## imm = 0x1E0
	popq	%rbp
	retq
LBB29_27:
	movq	-408(%rbp), %rdi
	callq	__Unwind_Resume
LBB29_28:
Lfunc_end5:
	.cfi_endproc
	.section	__TEXT,__gcc_except_tab
	.align	2
GCC_except_table29:
Lexception5:
	.byte	255                     ## @LPStart Encoding = omit
	.byte	155                     ## @TType Encoding = indirect pcrel sdata4
	.byte	41                      ## @TType base offset
	.byte	3                       ## Call site Encoding = udata4
	.byte	39                      ## Call site table length
Lset61 = Lfunc_begin5-Lfunc_begin5      ## >> Call Site 1 <<
	.long	Lset61
Lset62 = Ltmp138-Lfunc_begin5           ##   Call between Lfunc_begin5 and Ltmp138
	.long	Lset62
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
Lset63 = Ltmp138-Lfunc_begin5           ## >> Call Site 2 <<
	.long	Lset63
Lset64 = Ltmp139-Ltmp138                ##   Call between Ltmp138 and Ltmp139
	.long	Lset64
Lset65 = Ltmp140-Lfunc_begin5           ##     jumps to Ltmp140
	.long	Lset65
	.byte	0                       ##   On action: cleanup
Lset66 = Ltmp139-Lfunc_begin5           ## >> Call Site 3 <<
	.long	Lset66
Lset67 = Lfunc_end5-Ltmp139             ##   Call between Ltmp139 and Lfunc_end5
	.long	Lset67
	.long	0                       ##     has no landing pad
	.byte	0                       ##   On action: cleanup
	.align	2

	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.globl	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE11eq_int_typeEii
	.align	4, 0x90
__ZNSt3__111char_traitsIcE11eq_int_typeEii: ## @_ZNSt3__111char_traitsIcE11eq_int_typeEii
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp144:
	.cfi_def_cfa_offset 16
Ltmp145:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp146:
	.cfi_def_cfa_register %rbp
	movl	%edi, -4(%rbp)
	movl	%esi, -8(%rbp)
	movl	-4(%rbp), %esi
	cmpl	-8(%rbp), %esi
	sete	%al
	andb	$1, %al
	movzbl	%al, %eax
	popq	%rbp
	retq
	.cfi_endproc

	.globl	__ZNSt3__111char_traitsIcE3eofEv
	.weak_def_can_be_hidden	__ZNSt3__111char_traitsIcE3eofEv
	.align	4, 0x90
__ZNSt3__111char_traitsIcE3eofEv:       ## @_ZNSt3__111char_traitsIcE3eofEv
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp147:
	.cfi_def_cfa_offset 16
Ltmp148:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp149:
	.cfi_def_cfa_register %rbp
	movl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	popq	%rbp
	retq
	.cfi_endproc

	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"hello, world"

L_.str.1:                               ## @.str.1
	.asciz	"goodbye"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE ## @_ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.weak_def_can_be_hidden	__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.align	3
__ZTVNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE:
	.quad	0
	.quad	__ZTINSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.quad	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED1Ev
	.quad	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEED0Ev
	.quad	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE16__on_zero_sharedEv
	.quad	__ZNKSt3__119__shared_weak_count13__get_deleterERKSt9type_info
	.quad	__ZNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEE21__on_zero_shared_weakEv

	.section	__TEXT,__const
__ZNSt3__1L19piecewise_constructE:      ## @_ZNSt3__1L19piecewise_constructE
	.space	1

	.section	__TEXT,__const_coal,coalesced
	.globl	__ZTSNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE ## @_ZTSNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.weak_definition	__ZTSNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.align	4
__ZTSNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE:
	.asciz	"NSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE"

	.section	__DATA,__datacoal_nt,coalesced
	.globl	__ZTINSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE ## @_ZTINSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.weak_definition	__ZTINSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.align	4
__ZTINSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE:
	.quad	__ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	__ZTSNSt3__120__shared_ptr_emplaceINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS4_IS6_EEEE
	.quad	__ZTINSt3__119__shared_weak_countE


.subsections_via_symbols
