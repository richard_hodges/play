/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <vector>
#include <iostream>

struct Gromit
{
    Gromit(int index) : index(index) {};
    
    void speak(int i) { std::cout << name() << " speaking " << i << std::endl; }
    void eat(float f) { std::cout << name() << " eating " << f << std::endl; }
    void sleep(char c, double f) { std::cout << name() << " sleeping " << c << " " << f << std::endl; }
    void sit() { std::cout << name() << " sitting" << std::endl; }
    
private:
    std::string name() const {
        return "Gromit " + std::to_string(index);
    }
    int index;
};

template<class Container, class F>
void apply(Container& container, F f)
{
    for (const auto& p : container)
    {
        f(p);
    }
    
}

int main() {
    std::vector<std::unique_ptr<Gromit>> dogs;
    for(int i = 0; i < 5; i++)
        dogs.emplace_back(new Gromit(i));
    
    using namespace std::placeholders;
    
    apply(dogs, std::bind(&Gromit::speak, _1, 1));
    apply(dogs, std::bind(&Gromit::eat, _1, 2.0f));
    apply(dogs, std::bind(&Gromit::sleep, _1, 'z', 3.0));
    apply(dogs, std::bind(&Gromit::sit, _1));
}
