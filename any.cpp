/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <memory>
#include <utility>
#include <string>
#include <iomanip>
#include <typeinfo>

namespace cpputil {
    
    
    
    class any
    {
        struct concept;
        template<class T> struct model;
        
        template<class T, class...Args>
        static auto construct(Args&&... args) -> std::unique_ptr<model<T>>;
        
        struct concept {
            
            virtual std::ostream& write(std::ostream& os) const = 0;
            virtual std::unique_ptr<concept> clone() const = 0;
            virtual ~concept() = default;
        };
        
        
        
        template<class T>
        struct model final
        : concept
        , T
        {
            template<class...Args>
            model(Args&&... args)
            : T(std::forward<Args>(args)...)
            {}
            
            std::unique_ptr<concept> clone() const override
            {
                return construct<T>(static_cast<const T&>(*this));
            }
            
            std::ostream& write(std::ostream& os) const override;
        };
        
        
        std::unique_ptr<concept> _ptr;
        
    public:
        
        template<class T
        , std::enable_if_t<not std::is_same<any, std::decay_t<T>>::value, int> = 0
        > any(T&& t)
        : _ptr(construct<T>(std::forward<T>(t)))
        {}
        
        any() = default;
        any(any&&) = default;
        any& operator=(any&&) = default;
        any(const any& r)
        : _ptr(r._ptr ? r._ptr->clone() : nullptr)
        {}
        
        any& operator=(const any& r)
        {
            any tmp = r;
            std::swap(tmp._ptr, _ptr);
            return *this;
        }
        
        template<class T>
        T& get()
        {
            return dynamic_cast<T&>(*_ptr);
        }
        
        friend inline auto operator<<(std::ostream& os, const any& a) -> std::ostream&;
        
    };
    
    inline auto operator<<(std::ostream& os, const any& a) -> std::ostream&
    {
        if (a._ptr) {
            return a._ptr->write(os);
        }
        else {
            auto& x = os << "{ empty }";
            return x;
        }
    }
    
    template<class T, class...Args>
    auto any::construct(Args&&... args) -> std::unique_ptr<model<T>>
    {
        return std::make_unique<model<T>>(std::forward<Args>(args)...);
    }
    
    template<class T>
    std::ostream& any::model<T>::write(std::ostream& os) const
    {
        const auto& t = static_cast<const T&>(*this);
        return os << t;
    }
    
}

template<class Tag>
struct X {
    using tag_type = Tag;
    
    X()
    : id(counter++)
    {}
    X(const X&) : X() {}
    X(X&& r) {
        id = r.id;
        r.id = -1;
    }
    X& operator=(const X&) { id = counter++;
        return *this;
    }
    
    X& operator=(X&& r) {
        std::swap(id, r.id);
        return *this;
    }
    
    
    void write(std::ostream& os) const {
        os << "X<" << tag_type::name << ">{ ";
        os << std::quoted(std::to_string(id));
        os << " }";
    }
    
    static int counter;
    
    int id = -2;
};

template<class T>
int X<T>::counter = 0;

template<class T>
std::ostream& operator<<(std::ostream& os, const X<T>& x) {
    x.write(os);
    return os;
}

struct a_tag {
    constexpr static const char* const name = "a_tag";
};

struct b_tag {
    constexpr static const char* const name = "b_tag";
};


auto main() -> int
{
    using namespace std;
    try
    {
        cpputil::any a1 = X<a_tag>();
        auto a2 = a1;
        cout << a1 << ", " << a2 << endl;
        
        auto& a3 = a1.template get<const X<a_tag>>();
        a3.id = 100;
        cout << typeid(a3).name() << endl;
        cout << a3 << endl;
        
        auto& v = a1.get<X<b_tag>>();
        cout << typeid(v).name() << endl;
        cout << v << endl;
        }
        catch(const std::exception& e) {
            std::cout << e.what() << endl;
        }
        return 0;
        }
        
