//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <type_traits>
#include <utility>
#include <functional>
#include <system_error>

struct rpc_socket
{
    template<
    // They type of handler
    class Handler,
    // check that the handler is a function-like object that takes 1 parameter of type std::error_code
    typename = decltype(std::declval<Handler>()(std::declval<std::error_code>()))
    >
    void async_connect(Handler handler)
    {
        std::cout << "handler says: "; handler(std::error_code(1, std::generic_category()));
    }
    
};

using namespace std;

auto main() -> int
{
    rpc_socket s;
    s.async_connect([](const error_code& ec) {
        cout << ec << endl;
    });
    s.async_connect([](error_code ec) {
        cout << ec << endl;
    });
    
    auto f = [](error_code ec, int a) {
        cout << "a is " << a << " and error is " << ec << endl;
    };
    // fails to compile - fails the check
    //s.async_connect(f);
    
    s.async_connect(std::bind(f, std::placeholders::_1, 4));
    
	return 0;
}

