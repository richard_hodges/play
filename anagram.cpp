//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <iterator>
#include <algorithm>

std::unordered_map<std::string, std::unordered_set<std::string>> anagram_map;

using namespace std;

auto main() -> int
{
    while (cin) {
        string word;
        cin >> word;
        auto sorted = word;
        sort(begin(sorted), end(sorted));
        anagram_map[sorted].insert(word);
    }

    // now we have sets of distinct words indexed by sorted letters
    
    for (const auto& map_entry : anagram_map)
    {
        const auto& anagrams = map_entry.second;
        if (anagrams.size() > 1)
        {
            // this is the code path where we have anagrams for a set of letters
            auto sep = "";
            for (const auto& word : anagrams) {
                cout << sep << word;
                sep = " ";
            }
            cout << endl;
        }
            
    }
	return 0;
}

