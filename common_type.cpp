/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <type_traits>

using namespace std;

struct Z;
struct X{operator Z();};
struct Y{operator X();};
struct Z{operator Y();};

static_assert( is_same<common_type_t<X,Y>,
              common_type_t<Y,X>>::value, "" ); // PASS

static_assert( is_same<common_type_t<X,Z>,
              common_type_t<Z,X>>::value, "" ); // PASS

static_assert( is_same<common_type_t<Y,Z>,
              common_type_t<Z,Y>>::value, "" ); // PASS

static_assert( is_same<common_type_t< X, common_type_t<Y,Z>    >,
              common_type_t<    common_type_t<X,Y>, Z >>::value, "" ); // FAIL...

int main()
{
    
}