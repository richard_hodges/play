/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
 mkdir -p ${dirname}/bin > /dev/null
 filename="${dirname}/bin/${filename}"
 else
 filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
 */
#include <iostream>
#include <iomanip>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <string>

// prints the explanatory string of an exception. If the exception is nested,
// recurses to print the explanatory of the exception it holds
void print_exception(const std::exception& e, int level =  0)
{
    std::cerr << std::string(level, ' ') << "exception: " << e.what() << '\n';
    try {
        std::rethrow_if_nested(e);
    } catch(const std::exception& e) {
        print_exception(e, level+1);
    } catch(...) {}
}

struct upstream_error
: std::runtime_error
{
    using std::runtime_error::runtime_error;
};

[[noreturn]]
void throw_upstream_error_impl(std::vector<std::string>::const_reverse_iterator begin,
                                        std::vector<std::string>::const_reverse_iterator end)
{
    try {
        if (std::current_exception()) {
            std::throw_with_nested(upstream_error(*begin));
        }
        else {
            throw upstream_error(*begin);
        }
    }
    catch(...) {
        if (++begin == end)
            throw;
        else
            throw_upstream_error_impl(begin, end);
    }
}

[[noreturn]]
void throw_upstream_error(const std::vector<std::string>& vec)
{
    if (vec.empty())
        throw std::invalid_argument("no error messages");
    throw_upstream_error_impl(vec.rbegin(), vec.rend());
}


void what_as_csv_impl(std::ostringstream& ss, const std::exception& e, const char* sep = "")
{
    ss << sep << std::quoted(e.what());
    try {
        std::rethrow_if_nested(e);
    }
    catch(const std::exception& e) {
        what_as_csv_impl(ss, e, ", ");
    }
    catch(...)
    {}
}

auto what_as_csv(const std::exception& e) -> std::string
{
    std::ostringstream ss;
    what_as_csv_impl(ss, e);
    return ss.str();
}

auto csv_split(const std::string& s) -> std::vector<std::string>
{
    std::istringstream ss(s);
    std::vector<std::string> result;
    bool first = true;
    while(ss) {
        if (!first) {
            char c;
            ss >> c;
        }
        else {
            first = false;
        }
        std::string buf;
        ss >> std::quoted(buf);
        std::cout << "[" << buf << "]\n";
        if (ss) {
            result.push_back(buf);
        }
    }
    return result;
}
        
        

using namespace std;

int main()
{
    stringstream ss;
    string in1 = "first error with spaces, and embedded \"quotes\" too";
    string in2 = "second error with spaces, and embedded \"quotes\" too";
    string in3 = "third error with spaces, and embedded \"quotes\" too";
    
    
    ostringstream oss;
    oss << quoted(in1) << ", " << quoted(in2) << ", " << quoted(in3);
    auto error_string = oss.str();
    cout << "error string is: " << error_string << endl;
    
    std::string out;
    
    ss << error_string;
    ss >> std::quoted(out);
    std::cout << out << std::endl;
    while(ss) {
        char c;
        ss >> c >> std::quoted(out);
        std::cout << out << std::endl;
    }
    
    auto error_vec = csv_split(error_string);
    cout << "vector size: " << error_vec.size() << endl;
    try {
        throw_upstream_error(error_vec);
    }
    catch(const std::exception& e)
    {
        print_exception(e);
        auto s = what_as_csv(e);
        cout << "caught these: " << s << endl;
    }
    return 0;
}
