/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=gnu++14"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>

namespace boost{
#  ifdef __GNUC__
    __extension__ typedef __int128 int128_type;
    __extension__ typedef unsigned __int128 uint128_type;
#  else
    typedef __int128 int128_type;
    typedef unsigned __int128 uint128_type;
#  endif
};

typedef __int128 Cint128_1;
__extension__ typedef __int128 Cint128_2;

int main(int argc, char* argv[]) {
#ifdef __SIZEOF_INT128__
    std::cout << "__SIZEOF_INT128__ = " << __SIZEOF_INT128__ << std::endl;
    std::cout << "sizeof(Cint128_1) = " << sizeof(boost::int128_type) << std::endl;
    std::cout << "sizeof(Cint128_2) = " << sizeof(boost::uint128_type) << std::endl;
#else
    std::cout << "Nope! no __SIZEOF_INT128__" << std::endl;
#endif
}
