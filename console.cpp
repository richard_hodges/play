//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ -lboost_system - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <memory>
#include <chrono>
#include <system_error>
#include <thread>
#include <stdlib.h>
#include <unistd.h>

#include <boost/asio.hpp>


using namespace std;

namespace asio = boost::asio;

asio::io_service io_service;
asio::local::stream_protocol::iostream log_stream;

int run_program(int argc, char** argv)
{
    for (int i = 0 ; i < 100 ; ++i) {
        cout << "logging first" << endl;
        log_stream << i << " hello" << endl;
        cout << "logged" << endl;
        this_thread::sleep_for(chrono::milliseconds(400));
    }
    return 0;
}


auto main(int argc, char** argv) -> int
{
    if (argc == 3
        && strcmp(argv[1], "--repeat") == 0)
    {
        auto socket_name = string(argv[2]);
        
        cout << "listening on " << socket_name << endl;
        
        ::unlink(socket_name.c_str()); // Remove previous binding.
        asio::local::stream_protocol::endpoint ep(socket_name);
        asio::local::stream_protocol::acceptor acceptor(io_service, ep);
        asio::local::stream_protocol::socket socket(io_service);
        acceptor.accept(socket);
        cout << "accepted" << endl;
        while (1) {
            char buf[100];
            auto bytes_read = socket.read_some(asio::buffer(buf));
            if (bytes_read > 0) {
                cout.write(buf, bytes_read);
                cout.flush();
            }
            else {
                socket.close();
                exit(0);
            }
        }
    }
    else {
        const auto socket_name = "/tmp/foo"s;

        cout << "forking" << endl;
        auto client_pid = fork();
        if (client_pid == 0) {
            cout << "in client" << endl;
            ostringstream ss;
            ss << "xterm -e " << argv[0] << " --repeat " << socket_name;
            auto s = ss.str();
            auto err = system(s.c_str());
            if (err) {
                throw system_error(errno, system_category(), "logger child execution");
            }
        }
        else if (client_pid == -1) {
            throw system_error(errno, system_category(), "forking");
        }
        else {
            cout << "pause to allow xterm client to start" << endl;
            this_thread::sleep_for(chrono::seconds(2));
            cout << "making endpoint " << socket_name << endl;
            asio::local::stream_protocol::endpoint endpoint(socket_name);
            cout << "connecting to " << endpoint << endl;
            
            log_stream.connect(endpoint);
            cout << "connected" << endl;
            
            auto ret = run_program(argc, argv);
            
            log_stream.close();
            exit(ret);
        }
    }
	return 0;
}

