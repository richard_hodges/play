/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <type_traits>
#include <utility>
#include <memory>
#include <string>
#include <vector>


namespace traits
{
    
    struct has_plus_equals
    {
        template<class U, class V>
        auto static test(U*p) -> decltype((*p) += std::declval<V>(), void(), std::true_type());
        
        template<class U, class V>
        auto static test(...) -> decltype(std::false_type());
    };
    
    template<class U, class V>
    static constexpr bool plus_equals_valid = decltype(has_plus_equals::test<U, V>(nullptr))::value;

    template<class Left, class Right>
    struct has_shift_left
    {
        template<class U>
        auto static test(U*p) -> decltype((*p) << std::declval<Right>(), void(), std::true_type());
        
        template<class U>
        auto static test(...) -> decltype(std::false_type());
        
        static constexpr bool value = decltype(test<Left>(nullptr))::value;
        using type = bool;
    };
    
    template<class U, class V>
    static constexpr bool shift_left_valid = has_shift_left<U,V>::value;
    
    template <typename T, template <typename> class Tmpl>  // #1 see note
    struct is_derived
    {
        typedef char yes[1];
        typedef char no[2];
        
        static no & test(...);
        
        template <typename U>
        static yes & test(Tmpl<U> const &);
        
        static bool const value = sizeof(test(std::declval<T>())) == sizeof(yes);
    };
    
    template<class Pointer> struct pointee {
        using type = std::remove_reference_t<decltype(*std::declval<Pointer&>())>;
    };
    
    template<class Pointer> using pointee_t = typename pointee<Pointer>::type;

    template<class T> struct negation;
    template<bool Value> struct negation<std::integral_constant<bool, Value>> : std::integral_constant<bool, not Value> {};


}

struct null_pointer : std::logic_error
{
    using std::logic_error::logic_error;
};






template<class PointerType>
struct propagate_const
{
    using pointer_type = PointerType;
    using element_type = traits::pointee_t<pointer_type>;
    using reference = element_type&;
    using const_reference = std::add_const_t<element_type>&;
    
    propagate_const(pointer_type p) : _ptr(std::move(p)) {}
    
    const_reference operator*() const {
        return *_ptr;
    }
    
    auto operator*()
    -> std::enable_if_t<not std::is_const<element_type>::value, reference>
    {
        return *_ptr;
    }
    
private:
    pointer_type _ptr;
};

namespace traits {
    template<class Pointer> struct pointee<propagate_const<Pointer>> {
        using type = pointee_t<Pointer>;
    };
    
    template<class Pointer> struct pointee<const propagate_const<Pointer>> {
        using type = std::add_const_t<pointee_t<Pointer>>;
    };
}

template<class T, class D>
auto clone(const std::unique_ptr<T, D>& source)
-> std::decay_t<decltype(source)>
{
    return { new T(*source), source.get_deleter() };
};

template<class T, class D = std::default_delete<T> >
struct cloneable_ptr
{
    using pointer_type = std::unique_ptr<T, D>;
    
    cloneable_ptr(std::nullptr_t) {};
    cloneable_ptr() : cloneable_ptr(nullptr) {}
    cloneable_ptr(pointer_type&& ptr) : _ptr(std::move(ptr)) {}
    cloneable_ptr(const pointer_type& ptr) : _ptr(clone(ptr)) {}
    cloneable_ptr(const cloneable_ptr& r) : _ptr(clone(r._ptr)) {}
    cloneable_ptr(cloneable_ptr&& r) noexcept : _ptr(std::move(r._ptr)) {}
    cloneable_ptr& operator=(const cloneable_ptr& r)
    {
        cloneable_ptr tmp(r);
        swap(*this, tmp);
        return *this;
    }

    cloneable_ptr& operator=(cloneable_ptr&& r)
    {
        cloneable_ptr tmp(std::move(r));
        swap(*this, tmp);
        return *this;
    }
    
    T& operator*() const {
        auto& o = *_ptr;
        if (std::addressof(o) == nullptr)
            throw null_pointer("cloneable_ptr");
        return o;
    }
    
    T& operator*() {
        auto& o = *_ptr;
        if (std::addressof(o) == nullptr)
            throw null_pointer("cloneable_ptr");
        return o;
    }
    
private:
    
    friend void swap(cloneable_ptr& l, cloneable_ptr& r) noexcept {
        std::swap(l._ptr, r._ptr);
    }
    
private:
    pointer_type _ptr;
};

template<class Pointer>
struct pointer_traits
{
    using pointer_type = Pointer;
    using element_of_ptr = traits::pointee_t<pointer_type>;
    using element_of_const_ptr = traits::pointee_t<std::add_const_t<pointer_type>>;
    using const_element_of_ptr = std::add_const_t<element_of_ptr>;
    using const_element_of_const_ptr = std::add_const_t<element_of_const_ptr>;
    static constexpr bool propagates_constness = std::integral_constant<bool, !std::is_same<element_of_ptr, element_of_const_ptr>::value>::value;
};

template<class Pointer>
auto pointee(const Pointer& p) -> typename pointer_traits<Pointer>::element_of_const_ptr&
{
    return *p;
}

template<class Pointer>
auto pointee(Pointer& p)
-> std::enable_if_t<
pointer_traits<Pointer>::propagates_constness,
typename pointer_traits<Pointer>::element_of_ptr
>&
{
    return *p;
}

template<class PointerType>
struct imbue_value_semantics
{
    // attributes
    using pointer_type = PointerType;
    using element_of_ptr = traits::pointee_t<pointer_type>;
    using element_of_const_ptr = traits::pointee_t<std::add_const_t<pointer_type>>;
    using const_element_of_ptr = std::add_const_t<element_of_ptr>;
    using const_element_of_const_ptr = std::add_const_t<element_of_const_ptr>;
    using propagates_constness = std::integral_constant<bool, !std::is_same<element_of_ptr, element_of_const_ptr>::value>;
    
    // utilities
    template<class T> static constexpr bool ConstructibleFrom = std::is_constructible<pointer_type, T>::value;
    template<class T> static constexpr bool Assignable = std::is_assignable<pointer_type, T>::value;
    template<class T> static constexpr bool WontThrowOnAssign = std::is_nothrow_assignable<pointer_type, T>::value;
    template<class T> static constexpr bool WontThrowOnConstruction = std::is_nothrow_constructible<pointer_type, T>::value;
    template<bool B> using EnableIf = std::enable_if_t<B>;

    
    // constructors
    imbue_value_semantics() : _ptr() {}
    imbue_value_semantics(std::nullptr_t) : _ptr(nullptr) {}

    // nonstandard copy constructors
    template<class Right, EnableIf<ConstructibleFrom<const Right&>>* = nullptr>
    imbue_value_semantics(const Right& ptr) noexcept(WontThrowOnConstruction<const Right&>)
    : _ptr(ptr) {}
    
    template<class Right, EnableIf<ConstructibleFrom<Right&&>>* = nullptr>
    imbue_value_semantics(Right&& ptr) noexcept(WontThrowOnConstruction<Right&&>)
    : _ptr(std::move(ptr)) {}
    
    // nonstandard assignment
    template<class Right, EnableIf<Assignable<const Right&>>* = nullptr>
    imbue_value_semantics& operator=(const Right& ptr) noexcept(WontThrowOnAssign<const Right&>)
    {
        _ptr = ptr;
        return *this;
    }
    
    template<class Right, EnableIf<Assignable<Right&&>>* = nullptr>
    imbue_value_semantics& operator=(Right&& ptr) noexcept(WontThrowOnAssign<Right&&>)
    {
        _ptr = std::move(ptr);
        return *this;
    }
    
    // conversion to element
    
    operator element_of_const_ptr&() const
    {
        return pointee(_ptr);
    }
    
    operator element_of_ptr&()
    {
        return pointee(_ptr);
    }
    
    
    
    template<class Arg>
    auto operator += (Arg&& arg)
    -> std::enable_if_t<traits::plus_equals_valid<element_of_ptr, Arg>, const imbue_value_semantics&>
    {
        pointee(_ptr) += std::forward<Arg>(arg);
        return *this;
    }
    
    template<class Left, std::enable_if_t< traits::shift_left_valid<Left, element_of_const_ptr> >* = nullptr>
    friend Left& operator<<(Left& l, const imbue_value_semantics& r)
    {
        return l << pointee(r._ptr);
    }

private:
    pointer_type _ptr;
};

template<class Pointer>
auto make_value_semantics(Pointer&& ptr)
{
    return imbue_value_semantics<Pointer>(std::forward<Pointer>(ptr));
}

template<class T> struct value { using type = T; };
template<class P> struct value<imbue_value_semantics<P>> { using type = typename imbue_value_semantics<P>::element_of_ptr; };
template<class P> struct value<const imbue_value_semantics<P>> { using type = typename imbue_value_semantics<P>::element_of_const_ptr; };

template<class T> using value_t = typename value<T>::type;






template<class T>
struct shared_object;


template<class T> static constexpr bool is_a_shared_object = traits::is_derived<std::remove_cv_t<T>, shared_object>::value;

template<class T>
struct shared_object
{
    
    shared_object() : _impl(nullptr) {}
    shared_object(std::nullptr_t) : _impl(nullptr) {}
    
    template<class U, std::enable_if_t<not is_a_shared_object<U> >* = nullptr>
    shared_object(U&& t) : _impl(std::make_shared<U>(std::forward<U>(t))) {};
    
    template<class U>
    shared_object(std::shared_ptr<U> sp) : _impl(std::move(sp)) {};
    
    template<class U>
    shared_object(propagate_const<std::shared_ptr<U>> sp) : _impl(sp) {};
    
    template<class U>
    auto operator += (U&& s)
    -> std::enable_if_t<traits::plus_equals_valid<T, U>, shared_object&>
    {
        if (std::addressof(*_impl) == nullptr)
            throw null_pointer("null pointer");
        *_impl += s;
        return *this;
    }

    template<class Left, std::enable_if_t< traits::shift_left_valid<Left, T> >* = nullptr>
    friend Left& operator<<(Left& l, const shared_object& pt)
    {
        if (std::addressof(*(pt._impl)) == nullptr)
            throw null_pointer("null pointer");
        return l << (*(pt._impl));
    }
    
    operator T&()
    {
        return value();
    }
    
    operator const T&() const
    {
        return value();
    }
    
protected:
    
    auto value()
    -> std::enable_if_t<not std::is_const<T>::value, T&>
    {
        auto& r = *_impl;
        if (std::addressof(r) == nullptr)
            throw null_pointer("null pointer");
        return r;
    }
    
    const T& value() const {
        const auto& r = *_impl;
        if (std::addressof(r) == nullptr)
            throw null_pointer("null pointer");
        return r;
    }
    
private:
    propagate_const<std::shared_ptr<T>> _impl;
};


struct shared_string : shared_object<std::string>
{
    using shared_object<std::string>::shared_object;
    
};

template<class T, std::enable_if_t<std::is_const<T>::value>* = nullptr >
std::string check_const(T&)
{
    return std::string("const");
}

template<class T, std::enable_if_t<not std::is_const<T>::value>* = nullptr >
std::string check_const(T&)
{
    return std::string("not const");
}

template<class VSPtr>
void immutable_test_impl(const VSPtr& p)
{
    using namespace std;
    
    cout << p << endl;
}

template<class VSPtr>
auto mutable_test_impl(VSPtr& p)
-> std::enable_if_t<std::is_const<value_t<VSPtr>>::value, void>
{
    
}

template<class VSPtr>
auto mutable_test_impl(VSPtr& p)
-> std::enable_if_t<!std::is_const<value_t<VSPtr>>::value, void>
{
    using namespace std;
    p += std::string(" there");
    cout << p << endl;
}

template<class VSPtr>
void test_it(VSPtr& p)
{
    using namespace std;

    cout << typeid(VSPtr).name() << endl;

    immutable_test_impl(p);
    mutable_test_impl(p);

    cout << endl;
}

void test_imbue_value_semantics()
{
    using namespace std;
    
    auto s = "hello"s;
    test_it(s);

    auto test_string_ptr1 = std::make_unique<string>("hello"s);
    auto test_string_ptr2 = std::make_unique<const string>("hello"s);

    auto p_str = make_value_semantics(test_string_ptr1.get());
    test_it(p_str);
    
    auto p_cstr = make_value_semantics(test_string_ptr2.get());
    test_it(p_cstr);
    
    imbue_value_semantics<std::shared_ptr<std::string>> sp_str(std::make_shared<string>("hello"));
    test_it(sp_str);
    auto sp_str2 = sp_str;
    test_it(sp_str2);
    
    auto sp_cstr = make_value_semantics(std::make_shared<const string>("hello"));
    test_it(sp_cstr);

    imbue_value_semantics<const std::shared_ptr<std::string>> csp_str(std::make_shared<string>("hello"));
    test_it(csp_str);
    
    imbue_value_semantics<const std::shared_ptr<const std::string>> csp_cstr(std::make_shared<string>("hello"));
    test_it(csp_cstr);
    
    imbue_value_semantics<std::unique_ptr<std::string>> up_str(make_unique<string>("Hello"));
    test_it(up_str);
    
    imbue_value_semantics<std::unique_ptr<const std::string>> up_cstr(make_unique<string>("Hello"));
    test_it(up_cstr);
    
    imbue_value_semantics<const std::unique_ptr<std::string>> cup_str(make_unique<string>("Hello"));
    test_it(cup_str);
    
    imbue_value_semantics<const std::unique_ptr<const std::string>> cup_cstr(make_unique<string>("Hello"));
    test_it(cup_cstr);
    
}

int main()
{
    using namespace std;
    
    test_imbue_value_semantics();
    
    // a vector of mutable shared_strings
    vector<shared_string> v;
    
    // a vector of immutable shared_strings
    vector<const shared_string> cv;
    
    // make a shared_string
    v.emplace_back(make_shared<string>("hello"));
    
    // refer to the *same one* in cv
    cv.emplace_back(v[0]);
    
    for (const auto& p : v)
    {
//        *p += " there";  // immutable reference to mutable shared string - not allowed
        cout << check_const(p) << " " << p;
        cout << endl;
    }
    
    for (auto& p : v)
    {
        cout << check_const(p) << " " << p;
        p += " there";    // mutable reference to mutable shared string - allowed
        cout << " becomes " << p;
        cout << endl;
    }
    
    for (auto&p : cv)
    {
        cout << check_const(p) << " " << p;
//        p += " world";     // mutable
        cout << endl;
    }
    
    std::string s = cv[0];
    cout << s << endl;
    
    shared_string ss;
    try {
        s += ss;
        cout << ss << endl;
    }
    catch(const std::exception& e) {
        cout << "exception: " << e.what() << endl;
    }
    
    cloneable_ptr<string> cs = make_unique<string>("Hello"s);
    cloneable_ptr<string> cs2 = cs;
    (*cs2) += " world";
    cout << (*cs) << ", " << *(cs2) << endl;
    

	return 0;
}

