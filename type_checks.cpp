//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <type_traits>
#include <utility>
#include <array>

template<class Type, size_t Size>
struct my_vector
{
    static constexpr size_t num_components = Size;
    
    template<class...Args, typename = std::enable_if_t< (sizeof...(Args) == Size) > >
    my_vector(Args&&...args) : _data { std::forward<Args>(args)... } {}
    
    template<size_t I >
    Type get() const {
        return _data[I];
    }
    
    std::array<Type, Size> _data;
};

template<class Type, size_t Size, size_t I, class VectorType = my_vector<Type, Size>, typename = void>
struct get;
template<class Type, size_t Size, size_t I >
struct get<Type, Size, I, my_vector<Type, Size>, std::enable_if_t<I <= Size> >
{
static Type apply(const my_vector<Type, Size>& v) {
        return v.get<I>();
    }
};

template<class Type, size_t Size>
Type x(const my_vector<Type, Size>& v)
{
    return get<Type, Size, 0, my_vector<Type, Size>>::apply(v);
}

template<class Type, size_t Size>
Type y(const my_vector<Type, Size>& v)
{
    return get<Type, Size, 1, my_vector<Type, Size>>::apply(v);
}

template<class Type, size_t Size>
Type z(const my_vector<Type, Size>& v)
{
    return get<Type, Size, 2, my_vector<Type, Size>>::apply(v);
}

template<size_t I, class Type, size_t Size>
Type more(const my_vector<Type, Size>& v)
{
    return get<Type, Size, I+3, my_vector<Type, Size>>::apply(v);
}


template<class T> using Vec2 = my_vector<T, 2>;
template<class T> using Vec3 = my_vector<T, 3>;
template<class T> using Vec6 = my_vector<T, 6>;

using namespace std;

auto main() -> int
{
    Vec2<int> v2 { 1, 2 };
    Vec3<int> v3 { 1, 2, 3 };
    Vec6<int> v6 { 1, 2, 3, 4, 5, 6 };
    
    cout << "v2: " << x(v2) << ", " << y(v2) << endl;
    cout << "v3: " << x(v3) << ", " << y(v3) << ", " << z(v3) << endl;
    cout << "v6: "
    << x(v6) << ", " << y(v6) << ", " << z(v6) << ", "
    << more<0>(v6) << ", " << more<1>(v6) << ", " << more<2>(v6)
    << endl;
    
	return 0;
}

