//
//  main.cpp
//  skeleton
//
//  Created by Richard Hodges on 11/05/2016.
//  Copyright © 2016 Richard Hodges. All rights reserved.
//

#include <iostream>
#include <string>
#include <algorithm>

// return true if there has been a change
bool replace_if_match(std::string& buffer,
                      const std::string& crib,
                      std::string guess)
{
    bool change = false;
    
    std::sort(std::begin(guess), std::end(guess));
    guess.erase(std::unique(std::begin(guess), std::end(guess)),
                std::end(guess));
    
    for (auto iblank = std::find(std::begin(buffer), std::end(buffer), '_') ;
         iblank != std::end(buffer) ;
         iblank = std::find(std::next(iblank), std::end(buffer), '_'))
    {
        auto i = std::distance(std::begin(buffer), iblank);
        auto c = crib.at(i);
        if (std::binary_search(std::begin(guess), std::end(guess), c))
        {
            *iblank = c;
            change = true;
        }
    }
    
    return change;
}


int main(int argc, const char * argv[]) {
    // I have not yet implemented the random word
    std::string random_word{"timeless"};
    
    // set number of guesses
    int guesses{5};
    
    // set guessed word to length of random word in underscores
    std::string guessed_word(random_word.length(),'_');
    
    // enter guessing loop as long as
    // guesses is greater than or equal to zero
    while( guesses > 0 && random_word != guessed_word )
    {
        // display guessed word in progress
        std::cout << guessed_word << '\n';
        
        // display number of guesses left
        std::cout << "Number of Guesses Left: " << guesses << '\n';
        
        // prompt user to enter a guess
        std::cout << "Enter a string as guess." << '\n';
        static std::string guess;
        std::cin >> guess;
        
        // the guessed word will be modified in the if statement via function; therefore, the value must be stored to look back on
        
        // guess fails to succeed if the guessed word does not change
        if( !replace_if_match( guessed_word, random_word, guess ) )
        {
            guesses--;
        }
        std::cout << guessed_word << std::endl;
    }
    if( guessed_word == random_word )
    {
        std::cout << "You winza!!!! " << std::endl;
    }
    else
    { 
        std::cout << "Game over yeah!!!! " << std::endl;
    }
}
