#include <thread>
#include <memory>
#include <mutex>
#include <iostream>
#include <atomic>

using namespace std;

struct T {
    void op() { /* some stuff */ }
    ~T() noexcept { /* some stuff */ }
};

std::shared_ptr<T> t {
    new T,
    [](T*p)
    {
        std::cout << "*t certainly destroyed but on an indeterminate thread\n";
    }
};

        std::mutex mtx;
std::weak_ptr<T> w{t};
enum action { destroy, op};

void request(action a) {
    if (a == action::destroy)
    {
        std::atomic_store(&t, shared_ptr<T>());
    } else if (a == action::op)
    {
        if (auto l = w.lock())  // lock() is atomic as of defect report LWG 2316
        {
            l->op();
        }
    }
}

int main() {
    // At some point in time and different points in the program,
    // two different users make two different concurrent requests
    std::thread th1{request, destroy}; std::thread th2{request, op};
    
    // ....
    th2.join();
    th1.join();
}
