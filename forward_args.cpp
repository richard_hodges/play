/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <memory>
#include <iostream>

struct foo
{
    std::size_t _size;
    std::unique_ptr<int[]> data;
public:
    explicit foo(std::size_t s) : _size(s) { }
    foo(std::size_t s, int v)
    : _size(s)
    {
        data.reset(new int[_size]);
        std::fill(&data[0], &data[0] + _size, v);
    }
    
    foo(std::initializer_list<int> d)
    : _size(d.size())
    , data{new int[_size]}
    {
        std::copy(d.begin(), d.end(), &data[0]);
    }
    
    std::size_t size() const { return _size; }
};

template <typename... Args>
auto forward_args(Args&&... args)
{
    return foo(std::forward<Args>(args)...).size();
    //--------^---------------------------^
}

template <class T>
auto forward_args(std::initializer_list<T> li)
{
    return foo(li).size();
    //--------^---------------------------^
}

int main()
{
    std::cout << forward_args(1, 2) << " " << forward_args(1) << " "
    << forward_args(2) << "\n";
    std::cout << forward_args({1,2,3});
}

