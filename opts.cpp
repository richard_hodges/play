/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-O3 -std=c++1z -I${HOME}/local/include -L${HOME}/local/lib -lboost_program_options -lboost_system"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" --foo=1 --foo=2 && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <boost/program_options.hpp>
#include <vector>
#include <iterator>
#include <algorithm>

int main(int argc, const char**argv)
{
    std::vector<std::string> args { argv, argv + argc };
    
    std::copy(args.begin(), args.end(),
              std::ostream_iterator<std::string>(std::cout,
                                                 ", "));
    std::cout << std::endl;
    return 0;
}

