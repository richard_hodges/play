/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-O3 -std=c++14"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <utility>
#include <sstream>
#include <string>

namespace detail{

    template<size_t N> struct boolean_value;
    template<size_t N> using boolean_value_t = typename boolean_value<N>::type;
    template<size_t N> constexpr auto to_int(boolean_value_t<N> b) { return static_cast<int>(b); };
    template<size_t N> constexpr auto to_boolean_value(int i) { return static_cast<boolean_value_t<N>>(i); };

    template<> struct boolean_value<1> {
        enum type { bit0, bit1 };
    };

    template<> struct boolean_value<2> {
        enum type { bit00, bit01, bit10, bit11 };
    };

    template<> struct boolean_value<3> {
        enum type { bit000, bit001, bit010, bit011, bit100, bit101, bit110, bit111 };
    };

    template<class...Args, size_t...Is>
    static constexpr auto make_bitfield(std::tuple<Args...> t, std::index_sequence<Is...>)
    {
#if __cplusplus > 201402L
        int accum = (0 | ... | (std::get<Is>(t) ? (1 << Is) : 0));
#else
        int accum = 0;
        using expand = int[];
        (void) expand { (std::get<Is>(t) ? accum |= (1 << Is) : 0) ... };
#endif
        return to_boolean_value<sizeof...(Is)>(accum);
    }

}

template<class...Args>
constexpr
auto mcase(Args&&...args)
{
    return detail::make_bitfield(std::make_tuple(bool(std::forward<Args>(args))...),
                                 std::index_sequence_for<Args...>());
}

// little function to defeat the optimiser, otherwise clang inlines the whole program!
auto get_result()
{
    using namespace std;

    istringstream ss("foo 2");
    auto result = tuple<string, int>();
    ss >> get<0>(result) >> get<1>(result);
    return result;
}

int main()
{
    using namespace std;
    const auto result = get_result();
    const auto& s1 = std::get<0>(result);
    const auto& v1 = std::get<1>(result);

    switch(mcase(s1 == "foo"s, v1 == 2))
    {
        case mcase(true, true):
            cout << mcase(true, true) << endl;
            break;

        case mcase(false, false):
            cout << mcase(false, false) << endl;
            break;
    }
	return 0;
}

