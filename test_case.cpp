/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++14 -I${HOME}/local/include"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

namespace MyMessageNamespace
{
    class ParticularMessage
    {
    public:
        template <int N>
        void SetSomething(int myValue) {};
    };
}

template <typename MyMessageT>
struct MyMessage
{
    using MyMessageType = MyMessageT;
};

using MyMessages = boost::mpl::list<MyMessage<MyMessageNamespace::ParticularMessage>>;

BOOST_AUTO_TEST_CASE_TEMPLATE(MyTestCase, MyMessage, MyMessages)
{
    typename MyMessage::MyMessageType message;
    message.template SetSomething<1>(20);
}
