/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -O3 -march=native"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <string>
#include <algorithm>
#include <vector>
#include <utility>
#include <tuple>
#include <chrono>
#include <random>
#include <iterator>
#include <iostream>
#include <sstream>
#include <iomanip>


template<class Vector>
void sort_numeric_single_conversion(Vector& vec)
{
    auto first = vec.begin();
    auto last = vec.end();
    auto size = vec.size();
    
    using element = std::tuple<double, std::size_t>;
    std::vector<element> elements;
    elements.reserve(size);
    for (auto current = first ; current != last ; ++current)
    {
        elements.emplace_back(stod(*current), current - first);
    }
    std::sort(std::begin(elements), std::end(elements), [](auto& l, auto &r) {
        return std::get<double>(l) < std::get<double>(r);
    });
    
    Vector buffer;
    buffer.reserve(size);
    for(auto& elem : elements)
    {
        auto isource = std::get<std::size_t>(elem);
        buffer.push_back(std::move(vec[isource]));
    }
    vec = std::move(buffer);
}

template<class Vector>
void sort_numeric_naive(Vector& vec)
{
    std::sort(vec.begin(), vec.end(), [](auto& l, auto& r) {
        return stod(l) < stod(r);
    });
}

std::vector<std::string> build_test_array(std::size_t size)
{
    std::vector<std::string> result;
    std::random_device rd;
    std::default_random_engine eng(rd());
    auto dist = std::uniform_real_distribution<double>(-1000000.0, 1000000.0);
    std::generate_n(std::back_inserter(result), size, [&] {
        return std::to_string(dist(eng));
    });
    return result;
}

template<class F>
auto time(F f)
{
    auto now = std::chrono::high_resolution_clock::now();
    f();
    auto then = std::chrono::high_resolution_clock::now();
    return then - now;
}

template<class Duration>
std::string to_ms(Duration d)
{
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(d);
    std::ostringstream ss;
    ss << std::setw(7) << ms.count() << "ms";
    return ss.str();
    
}

int main()
{
    for(auto size : { 10, 100, 1000, 10000, 100000, 1000000, 10000000 })
    {
        auto v1 = build_test_array(size);
        auto v2 = v1;
        
        auto t1 = time([&] { sort_numeric_single_conversion(v1); });
        auto t2 = time([&] { sort_numeric_naive(v2); });
        
        std::cout << "size: " << std::setw(7) << size << " single: " << to_ms(t1) << ", naive: " << to_ms(t2) << std::endl;
    }
    
}
