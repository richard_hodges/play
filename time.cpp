//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <ctime>
#include <vector>

using namespace std;

template<class...Ts>
struct Base {
    
};

template<class...Ts>
struct Derived : std::vector<Ts...>
{
    using std::vector<Ts...>::vector;
};

double mytimer(struct timeval *start, struct timeval *end)
{
    return double(end->tv_sec - start->tv_sec) + double(end->tv_usec - start->tv_usec)*1e-6;
}

auto main() -> int
{
    Derived<int> x;
    
	cout << "Hello, World" << endl;
	return 0;
}

