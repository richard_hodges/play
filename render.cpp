/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-O3 -std=c++1z -I${HOME}/local/include"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <vector>
#include <string>
#include <condition_variable>
#include <thread>
#include <memory>
#include <cassert>
#include <sstream>


//
// a simple scene object with *value semantics*
// the actual scene data is stored in an inner class, an instance of which is maintained by a unique_ptr
// we could have used a shared_ptr but there is no reason to since we will be taking copies of the scene
// data in order to render it out of line.
// doing it this way means that although the copy might be expensive, it is only performed once
// moves are extremely fast
struct scene
{
    // a type to allow us to create an unitialised scene explicitly
    struct none_type {};
    
    // a flag object
    static constexpr const none_type none = none_type();

    // this is the actual expensive scene data (simulated)
    struct expensive_large_scene_data
    {
        int hero_x = 0,
        hero_y = 0;
    };
    
    // a printer function (to help debugging)
    friend std::ostream& operator<<(std::ostream& os, const expensive_large_scene_data& s)
    {
        os << "(" << s.hero_x << ", " << s.hero_y << ")";
        return os;
    }
    
    // construct empty
    scene(none_type) {
        // no not initialise the pointer
    }

    // construct and initialise a default scene
    scene() : _data(std::make_unique<expensive_large_scene_data>()) {}
    
    // copy constructor must explicitly clone the pointer (if populated)
    scene(const scene& r)
    : _data(r
            ? std::make_unique<expensive_large_scene_data>(r.data())
            : nullptr)
    {}

    // move constructor
    scene(scene&& r)
    : _data(std::move(r._data))
    {}

    // copy-assignment - take care here too.
    scene& operator=(const scene& r)
    {
        _data = r
        ? std::make_unique<expensive_large_scene_data>(r.data())
        : nullptr;
        return *this;
    }

    // move-assignment is simple
    scene& operator=(scene&& r)
    {
        _data = std::move(r._data);
        return *this;
    }
    
    // no need for a destructor - we're using unique_ptr

    bool valid() const {
        return bool(_data.get());
    }
    
    // convertible to bool so we can check whether it is empty easily
    
    operator bool() const {
        return valid();
    }
    
    void reset() {
        _data.reset();
    }
    
    // accessor
    
    const expensive_large_scene_data& data() const {
        assert(_data.get());
        return *_data;
    }
    
    expensive_large_scene_data& data() {
        assert(_data.get());
        return *_data;
    }
 
private:
    std::unique_ptr<expensive_large_scene_data> _data;
};


std::ostream& operator<<(std::ostream& os, const scene& s)
{
    return os << s.data();
}


// helper function to serialise access to cout
void emit(const std::string& display_id, const std::string& s)
{
    static std::mutex _m;
    auto lock = std::unique_lock<std::mutex>(_m);
    std::cout << display_id << " : " << s << std::endl;
    std::cout.flush();
}

// this renderer renders the scene from the perspective of the hero. it takes a second to draw
struct first_person_view
{
    // a function that renders a scene
    // this one takes a second to complete
    void operator()(const scene& s) const
    {
        using namespace std;
        
        const auto& data = s.data();
        std::ostringstream ss;
        
        
        ss << "I am at ";
        this_thread::sleep_for(chrono::milliseconds(500));
        
        ss << data.hero_x << ", ";
        
        this_thread::sleep_for(chrono::milliseconds(500));
        
        ss << data.hero_y;
        emit("first_person_view", ss.str());
    }
};

// this renderer renders the scene from the perspective of a top-dowm map. it takes half a second to draw
struct map_view
{
    // a function that renders a scene
    // this one takes half a second to complete
    void operator()(const scene& s) const
    {
        using namespace std;
        
        const auto& data = s.data();
        std::ostringstream ss;
        
        ss << "the hero is at ";
        ss << data.hero_x << ", ";
        
        this_thread::sleep_for(chrono::milliseconds(500));
        
        ss << data.hero_y;
        emit("map_view", ss.str());
    }
};



// the renderer
template<class RenderFunction>
struct renderer
{
    using mutex_type = std::mutex;
    using lock_type = std::unique_lock<mutex_type>;
    
    using render_function = RenderFunction;

    // start thread in constructor - do not copy this object (you can't anyway because of the mutex)
    renderer(render_function f)
    : _render_function(std::move(f))
    {
        // defer thread start until all data members initialised
        _render_thread = std::thread(std::bind(&renderer::loop, this));
    }
    
    // shut down cleanly on destruction
    ~renderer()
    {
        auto lock = lock_type(_mutex);
        _cancelled = true;
        lock.unlock();

        if (_render_thread.joinable())
        {
            _render_thread.join();
        }
    }

    // notify the renderer that a new scene is ready
    void notify(const scene& s)
    {
        auto lock = lock_type(_mutex);
        _pending_scene = s;
        lock.unlock();
        _cv.notify_all();
    }
    
private:
    void loop()
    {
        for(;;)
        {
            auto lock = lock_type(_mutex);
            _cv.wait(lock, [this] {
                // wait for either a cancel event or for a new scene to be ready
                return _cancelled or _pending_scene;
            });
            
            if (_cancelled) return;

            // move the pending scene to our scene-render buffer - this is very cheap
            _current_scene = std::move(_pending_scene);
            _pending_scene.reset();
            lock.unlock();
            
            // unlock early to allow mainline code to continue
            

            // now take our time rendering the scene
            _render_function(_current_scene);
            _current_scene.reset();
        }
    }
    
private:
    render_function _render_function;
    mutex_type _mutex;
    std::condition_variable _cv;
    bool _cancelled = false;
    scene _pending_scene = scene(scene::none);
    scene _current_scene = scene(scene::none);
    std::thread _render_thread;
};

template<class...R>
void notify(const scene& the_scene, R&... each_renderer)
{
    using expand = int[];
    (void) expand { 0, (each_renderer.notify(the_scene), 0)... };
}

int main()
{
    using namespace std;

    // create my scene
    scene my_scene;
    
    // instantiate my renderer
    renderer<first_person_view> my_renderer { first_person_view() };
    renderer<map_view> map_renderer { map_view() };

    // tell the renderer that the scene may be rendered
    notify(my_scene, my_renderer, map_renderer);

    // ... while it is doing that...
    for (int x = 0 ; x < 10 ; ++x)
    {
        for(int y = 0 ; y < 10 ; ++y)
        {
            // perform a scene update. the calculations for this update
            // take 300ms (faster than the renderer)
            my_scene.data().hero_x = x;
            my_scene.data().hero_y = y;
            this_thread::sleep_for(chrono::milliseconds(300));
            // tell the renderer that there is a new scene to render
            notify(my_scene, my_renderer, map_renderer);
        }
    }
    
	return 0;
}

