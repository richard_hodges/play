//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <tuple>
#include <array>
#include <utility>

using namespace std;

template<class Tag>
struct tagged_flag
{
    tagged_flag()
    : value { false }
    {}
    
    tagged_flag(bool i)
    : value(i)
    {}
    
    operator bool&() { return value; }
    operator const bool&() const { return value; }
    
    bool value;
};

template<class Tuple, std::size_t...Is>
bool any_set_impl(const Tuple& t, std::index_sequence<Is...>)
{
    bool values[] = { std::get<Is>(t)... };
    for (auto v : values) {
        if (v)
            return true;
    }
    return false;
}

template<class...Tags>
struct flagset
{
    bool any_set() const {
        return any_set_impl(values, std::index_sequence_for<Tags...>());
    }
    
    template<class Tag> bool& operator[](Tag) { return get<tagged_flag<Tag>>(values); }
    template<class Tag> bool& at() { return get<tagged_flag<Tag>>(values); }
    template<class Tag>
    void set() {
        get<tagged_flag<Tag>>(values) = true;
    }
    std::tuple<tagged_flag<Tags>...> values;
    
};

struct A {};
struct B {};
struct C {};

auto main() -> int
{
    flagset<A, B, C> f;
    cout << f.any_set() << endl;
    f[B()] = true;
    cout << f.any_set() << endl;
    f.at<C>() = true;
    cout << f.any_set() << endl;
    
	return 0;
}

