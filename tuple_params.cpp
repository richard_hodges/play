//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then tail -n +1 $0 | c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ - || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>

//
// utility to check whether a type is in a list of types
//
template<class T, class...Ts> struct is_in;

template<class T, class U>
struct is_in<T, U>
: std::is_same<T, U>::type {};

template<class T, class U, class...Rest>
struct is_in<T, U, Rest...>
: std::integral_constant<bool, std::is_same<T, U>::value || is_in<T, Rest...>::value>
{};

//
// a wrapper around fundamental types so we can 'name' types
//

template<class Type, class Tag>
struct fundamental {
    using value_type = Type;
    using tag_type = Tag;
    
    fundamental(Type x) : _x(x) {}
    
    operator const Type&() const { return _x; }
    operator Type&() { return _x; }
    
    Type _x;
};

//
// a utility to figure out a fundamental type's value or to take it's default value if it's not present
//
template<class Fundamental, class Tuple, typename = void>
struct value_of_impl
{
    static typename Fundamental::value_type apply(const Tuple& t)
    {
        return Fundamental::tag_type::dflt;
    }
};

template<class Fundamental, class...Types>
struct value_of_impl<Fundamental, std::tuple<Types...>, std::enable_if_t<is_in<Fundamental, Types...>::value>>
{
    static typename Fundamental::value_type apply(const std::tuple<Types...>& t)
    {
        return typename Fundamental::value_type(std::get<Fundamental>(t));
    }
};

template<class Fundamental, class Tuple>
decltype(auto) value_of(const Tuple& t)
{
    return value_of_impl<Fundamental, Tuple>::apply(t);
}


//
// some tag names to differentiate parameter 'name' types
//
struct a_tag { static constexpr int dflt = 0; };
struct b_tag { static constexpr int dflt = 1; };
struct c_tag { static constexpr int dflt = 2; };

//
// define some parameter 'names'
//
using a = fundamental<int, a_tag>;
using b = fundamental<int, b_tag>;
using c = fundamental<int, c_tag>;

//
// the canonical implementation of the function
//
void func(int a, int b, int c)
{
    std::cout << a << ", " << b << ", " << c << std::endl;
}

//
// a version that forwards the values of fundamental types in a tuple, or their default values if not present
//
template<class...Fundamentals>
void func(std::tuple<Fundamentals...> t)
{
    func(value_of<a>(t),
         value_of<b>(t),
         value_of<c>(t));
}

//
// a version that converts a variadic argument list of fundamentals into a tuple (that we can search)
//
template<class...Fundamentals>
void func(Fundamentals&&...fs)
{
    return func(std::make_tuple(fs...));
}

//
// a test
//

using namespace std;

auto main() -> int
{
    func();
    func(a(5));
    func(c(10), a(5));
    func(b(20), c(10), a(5));
    
    
	return 0;
}

