/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -O2"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <string>

std::string
rerev1(std::string s)
{
    if (s.empty())
        return s;
    return rerev1(s.substr(1)) + s[0];
}

std::string
rerev2(std::string s, std::string b = "")
{
    if (s.empty())
        return b;
    return rerev2(s.substr(1), s[0] + b);
}

int
main()
{
    std::cout << rerev1("testing") << std::endl;
    std::cout << rerev2("testing") << std::endl;
}

