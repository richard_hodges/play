/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -O3"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <cstdint>
#include <vector>
#include <random>
#include <iostream>
#include <chrono>
#include <algorithm>
#include <numeric>

template<class T>
std::vector<T> generate_test_data(std::size_t seed)
{
    auto v = std::vector<T>(20000000);
    std::default_random_engine eng(seed);
    std::uniform_int_distribution<T> dist(-127, 127);
    std::generate(std::begin(v), std::end(v),
                  [&eng, &dist] {
                      return dist(eng);
                  });
    return v;
}

auto inc_if_under_zero = [](int count, auto val) {
    return (val < 0) ? count + 1 : count;
};

int main()
{
    std::random_device rd;
    auto seed = rd();
    auto int_data = generate_test_data<std::int32_t>(seed);
    auto byte_data = generate_test_data<std::int8_t>(seed);
    
    auto t0 = std::chrono::high_resolution_clock::now();
    
    auto less_zero_32 = std::accumulate(std::begin(int_data),
                                        std::end(int_data),
                                        0,
                                        inc_if_under_zero);
    auto t1 = std::chrono::high_resolution_clock::now();
    
    auto less_zero_8 = std::accumulate(std::begin(byte_data),
                                       std::end(byte_data),
                                       0,
                                       inc_if_under_zero);
    
    auto t2 = std::chrono::high_resolution_clock::now();
    
    auto int_time = std::chrono::duration_cast<std::chrono::microseconds>(t1-t0).count();
    auto byte_time = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count();

    std::cout << "totals    : " << less_zero_32 << ", " << less_zero_8 << std::endl;
    std::cout << "int time  : " << int_time << "us" << std::endl;
    std::cout << "byte time : " << byte_time << "us" << std::endl;
}

