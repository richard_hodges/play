/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z -I${HOME}/local/include -O2 -L${HOME}/local/lib -lboost_chrono -lboost_system"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

// qcpptest.hpp

#ifndef INCLUDED_QCPPTEST_H
#define INCLUDED_QCPPTEST_H

#include <boost/variant.hpp>

class IShape {
public:
    virtual void rotate() = 0;
    virtual void spin() = 0;
};

class Square : public IShape {
public:
    void rotate() {
        // std::cout << "Square:I am rotating" << std::endl;
    }
    void spin() {
        // std::cout << "Square:I am spinning" << std::endl;
    }
};

class Circle : public IShape {
public:
    void rotate() {
        // std::cout << "Circle:I am rotating" << std::endl;
    }
    void spin() {
        // std::cout << "Circle:I am spinning" << std::endl;
    }
};

// template variation

// enum class M {ADD, DEL};
struct ADD {};
struct DEL {};

class TSquare {
    int i;
public:
    void visit(const ADD& add) {
        this->i++;
        // std::cout << "TSquare:I am rotating" << std::endl;
    }
    void visit(const DEL& del) {
        this->i++;
        // std::cout << "TSquare:I am spinning" << std::endl;
    }
    
    void spin() {
        this->i++;
        // std::cout << "TSquare:I am rotating" << std::endl;
    }
    void rotate() {
        this->i++;
        // std::cout << "TSquare:I am spinning" << std::endl;
    }
};

class TCircle {
    int i;
public:
    void visit(const ADD& add) {
        this->i++;
        // std::cout << "TCircle:I am rotating" << std::endl;
    }
    void visit(const DEL& del) {
        this->i++;
        // std::cout << "TCircle:I am spinning" << std::endl;
    }
    void spin() {
        this->i++;
        // std::cout << "TSquare:I am rotating" << std::endl;
    }
    void rotate() {
        this->i++;
        // std::cout << "TSquare:I am spinning" << std::endl;
    }
};

class MultiVisitor : public boost::static_visitor<void> {
public:
    template <typename T, typename U>
    
    void operator()(T& t, const U& u) {
        // std::cout << "visit" << std::endl;
        t.visit(u);
    }
};

// separate visitors, single dispatch

class RotateVisitor : public boost::static_visitor<void> {
public:
    template <class T>
    void operator()(T& x) {
        x.rotate();
    }
};

class SpinVisitor : public boost::static_visitor<void> {
public:
    template <class T>
    void operator()(T& x) {
        x.spin();
    }
};

#endif

// qcpptest.cpp

#include <iostream>
//#include "qcpptest.hpp"
#include <vector>
#include <boost/chrono.hpp>

using MV = boost::variant<ADD, DEL>;
// MV const add = M::ADD;
// MV const del = M::DEL;
static MV const add = ADD();
static MV const del = DEL();

void make_virtual_shapes(int iters) {
    // std::cout << "make_virtual_shapes" << std::endl;
    std::vector<IShape*> shapes;
    shapes.push_back(new Square());
    shapes.push_back(new Circle());
    
    boost::chrono::high_resolution_clock::time_point start =
    boost::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < iters; i++) {
        for (IShape* shape : shapes) {
            shape->rotate();
            shape->spin();
        }
    }
    
    boost::chrono::nanoseconds nanos =
    boost::chrono::high_resolution_clock::now() - start;
    std::cout << "make_virtual_shapes took " << nanos.count() * 1e-6
    << " millis\n";
}

void make_template_shapes(int iters) {
    // std::cout << "make_template_shapes" << std::endl;
    using TShapes = boost::variant<TSquare, TCircle>;
    // using MV = boost::variant< M >;
    
    // xyz
    std::vector<TShapes> tshapes;
    tshapes.push_back(TSquare());
    tshapes.push_back(TCircle());
    MultiVisitor mv;
    
    boost::chrono::high_resolution_clock::time_point start =
    boost::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < iters; i++) {
        for (TShapes& shape : tshapes) {
            boost::apply_visitor(mv, shape, add);
            boost::apply_visitor(mv, shape, del);
            // boost::apply_visitor(sv, shape);
        }
    }
    boost::chrono::nanoseconds nanos =
    boost::chrono::high_resolution_clock::now() - start;
    std::cout << "make_template_shapes took " << nanos.count() * 1e-6
    << " millis\n";
}

void make_template_shapes_single(int iters) {
    // std::cout << "make_template_shapes_single" << std::endl;
    using TShapes = boost::variant<TSquare, TCircle>;
    // xyz
    std::vector<TShapes> tshapes;
    tshapes.push_back(TSquare());
    tshapes.push_back(TCircle());
    SpinVisitor sv;
    RotateVisitor rv;
    
    boost::chrono::high_resolution_clock::time_point start =
    boost::chrono::high_resolution_clock::now();
    
    for (int i = 0; i < iters; i++) {
        for (TShapes& shape : tshapes) {
            boost::apply_visitor(rv, shape);
            boost::apply_visitor(sv, shape);
        }
    }
    boost::chrono::nanoseconds nanos =
    boost::chrono::high_resolution_clock::now() - start;
    std::cout << "make_template_shapes_single took " << nanos.count() * 1e-6
    << " millis\n";
}

int main(int argc, const char* argv[]) {
    std::cout << "Hello, cmake" << std::endl;
    
    int iters = atoi(argv[1]);
    
    make_virtual_shapes(iters);
    make_template_shapes(iters);
    make_template_shapes_single(iters);
    
    return 0;
}
