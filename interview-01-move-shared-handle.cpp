/*/../bin/ls > /dev/null
 # copyright (c) 2015 Richard Hodges hodges.r@gmail.com
 # all rights reserved
 # please ask for permission before copying
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

// QUESTION:1 - what is all that above???

#include <iostream>
#include <system_error>
#include <thread>
#include <memory>
#include <functional>
#include <future>
#include <memory>
#include <cassert>
#include <stdexcept>

using namespace std;

// QUESTION:2 - What is this?
using HandlerFunction = std::function<void (const std::error_code& ec)>;


namespace {

// QUESTION:3 - why the mutex around std::cout?
    std::mutex cout_mutex;
    void say_something(const std::string& s) {
        std::unique_lock<std::mutex> lock(cout_mutex);
        std::cout << "thread " << std::this_thread::get_id() << " says " << s << std::endl;
    }
}

struct message_concept {
    // QUESTION:21 (easy) what's the next line doing and why is it important?
    virtual ~message_concept() = default;
    
    virtual void start() = 0;
    virtual void halt() = 0;
    virtual void speak_after_delay(std::chrono::milliseconds delay, std::string message) = 0;
};

template<class Implementation>
class message_model
: public message_concept
// QUESTION:22 what does the next line allow, what does it imply about its owner? What are the dangers?
, public std::enable_shared_from_this<message_model<Implementation>>
{
public:
    /// allow construction of model using constructor of Implementation
// QUESTION:4 - What is this?
    template<class...Args>
    message_model(Args&&...args)
    : _impl(std::forward<Args>(args)...)
    {}
    
    void start() {
        // we can perform initialisation that depends on shared_from_this() here
        // QUESTION:5 - Why have this method? It's empty.
    }
    
    void halt() {
        // QUESTION:6 - Why might I have bothered with this?
        std::unique_lock<std::mutex> lock(_mutex);
        _halted = true;
    }
    
    void speak_after_delay(std::chrono::milliseconds delay, std::string message)
    {
        // QUESTION:7 - Explain why I can be so sure of my assertion
        assert(!_halted);   // sanity check.
        _impl.exec_in(delay, [this, self = this->shared_from_this(), message = std::move(message)] {
            // only communicate the action back to the caller if our handle still exists
            // I can prove that this will be the case #challenge-2
            // QUESTION:23 Explain why challenge-2 is provable
            std::unique_lock<std::mutex> lock(_mutex);
            if (!_halted) {
                say_something(message);
            }
        });
    }
    
private:
    std::mutex _mutex;
    bool _halted = false;
    Implementation _impl;
};

// QUESTION:8 - What is this? What does it do? What does it allow?
struct handle_services
{
    struct bits {

        bits& operator++() {
            std::unique_lock<std::mutex> lock(mutex);
            ++ops_in_progress;
            return *this;
        }

        bits& operator--() {
            std::unique_lock<std::mutex> lock(mutex);
            --ops_in_progress;
            lock.unlock();
            cv.notify_all();
            return *this;
        }
        
        void wait() {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, std::bind(&bits::all_done, this));
        }
        
        bool all_done() const {
            return ops_in_progress == 0;
        }

    private:
        std::mutex mutex;
        std::condition_variable cv;
        size_t ops_in_progress = 0;
    };
    
    static bits& the_bits() {
        // QUESTION:9 - What is this?? What's relevant about c++11 here?
        static bits _;
        return _;
    }
    
    // QUESTION:10 - What is this?
    struct deleter {
        deleter(bits& my_bits)
        : _my_bits(my_bits)
        {
            ++_my_bits.get();
        }
    
        template<class T>
        void operator()(T* p) const {
            delete p;
            --_my_bits.get();
        }
        
        // QUESTION:24 bonus points if you know what this is and why it's better than the alternative
        std::reference_wrapper<bits> _my_bits;
    };
    
    static deleter make_deleter()
    {
        // QUESTION:11 - what's happening here? Why did I do it this way?
        return { the_bits() };
    }
    
    static void wait()
    {
        // QUESTION:12 - What is this and why do I do it?
        the_bits().wait();
    }
    

};

template<class Concept>
struct moveonly_handle
{
    using concept_ptr_type = std::shared_ptr<Concept>;


    // default constructor allows us to create null handles...
    // QUESTION:13 - Given the obvious danger of null handles, why might we want to allow their creation?
    moveonly_handle() = default;
    
    ///... but we need some way to detect that they are null handles...
    bool empty() const {
        return !_concept;
    }
    
    // public move and move-assign because we like being awesome
    // QUESTION:25 why do we like being awesome, and what awesomeness is created here?
    moveonly_handle(moveonly_handle&&) noexcept = default;
    moveonly_handle& operator=(moveonly_handle&&) = default;

    // define move constructor, preventing compiler from generating copy constructor
    // QUESTION:26 why might I want to do this? After all, shared_ptr is meant to be shared, right?
    ~moveonly_handle() {
        // QUESTION:13 - What is this and why do I do it?
        if (_concept) {
            _concept->halt();
            _concept.reset();
        }
    }
    
    Concept& concept() {
        // QUESTION:14 - Why might this be important?
        if (!_concept) {
            throw std::logic_error("null handle");
        }
        return *_concept;
    }
    
    const Concept& concept() const {
        if (!_concept) {
            throw std::logic_error("null handle");
        }
        return *_concept;
    }
    
    // private constructor - we want to only allow construction of valid handles via create<> functions
    // or via moves
    // QUESTION:14 - why?
protected:
    moveonly_handle(concept_ptr_type concept_ptr)
    : _concept(std::move(concept_ptr))
    {
        _concept->start();
    }

private:
    std::shared_ptr<Concept> _concept;
};

struct delayed_chat_handle : moveonly_handle<message_concept>
{
    using inherited = moveonly_handle<message_concept>;
    using concept_ptr_type = inherited::concept_ptr_type;
    
    delayed_chat_handle() {}
    
    template<class Implementation, class...Args>
    static
    delayed_chat_handle
    create(Args&&...args) {
        // QUESTION:16 - What is this and why do I do it?
        auto model = std::shared_ptr<message_model<Implementation>>(new message_model<Implementation>(std::forward<Args>(args)...),
                                                                    handle_services::make_deleter());
        return { model };
    }
    
    void speak_after_delay(std::chrono::milliseconds millis, std::string message)
    {
        // QUESTION:17 - What is std::move?
        concept().speak_after_delay(millis, std::move(message));
    }

private:
    delayed_chat_handle(concept_ptr_type p)
    : inherited(std::move(p))
    {}

};

// QUESTION:18 - What is this and why do I do it? What have I achieved here? How does it help me and my colleagues? Your answer should be long

struct delayed_op {
    
    void exec_in(std::chrono::milliseconds millis, std::function<void()> op)
    {
        std::thread([this, millis, op = std::move(op)] {
            std::this_thread::sleep_for(millis);
            op();
        }).detach();
    }
};


auto main() -> int
{
    {
        delayed_chat_handle you_will_see_me = delayed_chat_handle::create<delayed_op>();
        delayed_chat_handle you_will_not_see_me = delayed_chat_handle::create<delayed_op>();
        you_will_see_me.speak_after_delay(std::chrono::milliseconds(1500), "taste the freedom, bitches!");
        you_will_see_me.speak_after_delay(std::chrono::milliseconds(3000), "nothing to say");
        you_will_see_me.speak_after_delay(std::chrono::milliseconds(1000), "hello there!");
        you_will_see_me.speak_after_delay(std::chrono::milliseconds(500), "good day!");
        delayed_chat_handle empty;
        try {
            empty.speak_after_delay(std::chrono::milliseconds(500), "really, I don't want to be seen");
        }
        catch (const std::logic_error& e)
        {
            say_something(e.what());
        }
        say_something("waiting 2 seconds");
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
        say_something("destroying handles");
    }
    
    say_something("waiting for shutdown");
    auto from = std::chrono::high_resolution_clock::now();
    // QUESTION:19 - What is this and why do I do it?
    handle_services::wait();
    auto until = std::chrono::high_resolution_clock::now();
    auto waited_for = std::chrono::duration_cast<std::chrono::milliseconds>(until - from).count();
    std::cout << "we had to wait for " << (double(waited_for) / 1000) << " seconds" << std::endl;
    // QUESTION:20 - why did we have to wait at all?
	return 0;
}

