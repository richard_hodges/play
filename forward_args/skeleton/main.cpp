//
//  main.cpp
//  skeleton
//
//  Created by Richard Hodges on 11/05/2016.
//  Copyright © 2016 Richard Hodges. All rights reserved.
//

#include <memory>
#include <iterator>

#include <iostream>
#include <utility>

template<class F, class...Args>
struct is_callable
{
    template<class U> static auto test(U* p) -> decltype((*p)(std::declval<Args>()...), void(), std::true_type());
    template<class U> static auto test(...) -> decltype(std::false_type());
    
    static constexpr bool value = decltype(test<F>(0))::value;
};

template<class F, class...Args, typename std::enable_if<is_callable<F, Args&&...>::value>::type* = nullptr>
void test_call(F, Args&&...args)
{
    std::cout << "callable" << std::endl;
}

template<class F, class...Args, typename std::enable_if<not is_callable<F, Args&&...>::value>::type* = nullptr>
void test_call(F, Args&&...args)
{
    std::cout << "not callable" << std::endl;
}

extern void f3(int, const std::string&)
{
    
}

int main()
{
    auto f1 = [](int, std::string) {};
    test_call(f1, 0, "hello");
    test_call(f1, "bad", "hello");
    
    std::function<void(int, const std::string&)> f2;
    test_call(f2, 0, "hello");
    test_call(f2, "bad", "hello");
    
    test_call(f3, 0, "hello");
    test_call(f3, "bad", "hello");
    
}

template<class T> auto heap_copy(std::initializer_list<T> li)
{
    using value_type = T;
    const auto size = li.size();
    auto p = std::make_unique<value_type[]>(size);
    if (p)
    {
        std::copy(std::begin(li), std::end(li), p.get());
    }
    return p;
}

template<class Iter> auto heap_copy(Iter first, std::size_t size)
{
    using value_type = typename std::iterator_traits<Iter>::value_type;
    auto p = std::make_unique<value_type[]>(size);
    if (p)
    {
        std::copy(first, first + size, p.get());
    }
    return p;
}

template<class T> auto heap_fill(std::size_t size, T initial)
{
    using value_type = T;
    auto p = std::make_unique<value_type[]>(size);
    if (p)
    {
        std::fill(p.get(), p.get() + size, initial);
    }
    return p;
}


struct foo
{
    using value_type = int;
    
    std::size_t _size;

    using data_ptr_type = std::unique_ptr<int[]>;
    data_ptr_type _data;

public:
    foo(std::size_t s = 0)
    : _size(s)
    , _data{ heap_fill(_size, value_type()) }
    {}
    
    foo(std::size_t s, int v)
    : _size(s)
    , _data { heap_fill(_size, v) }
    {
    }
    
    foo(std::initializer_list<int> d)
    : _size(d.size())
    , _data{ heap_copy(d) }
    {
    }
    
    foo(const foo& r)
    : _size(r._size)
    , _data{ heap_copy(r._data.get(), _size) }
    {}
    
    foo(foo&&) noexcept = default;
    
    foo& operator=(const foo& r) {
        return rswap(foo(r));
    }
    
    foo& operator=(foo&& r) noexcept {
        return rswap(std::move(r));
    }
    
    std::size_t size() const { return _size; }
    auto begin() const { return _data.get(); }
    auto end() const { return begin() + size(); }
    
private:
    foo& rswap(foo&& other) noexcept
    {
        using std::swap;
        swap(_size, other._size);
        swap(_data, other._data);
        return *this;
    }
};

std::ostream& operator<<(std::ostream& os, const foo& f)
{
    auto sep = " ";
    os << "[";
    for(const auto& e : f) {
        os << sep << e;
        sep = ", ";
    }
    return os << " ]";
}

template <typename... Args>
auto forward_args(Args&&... args)
{
    return foo(std::forward<Args>(args)...);
    //--------^---------------------------^
}

template <class T>
auto forward_args(std::initializer_list<T> li)
{
    return foo(li);
    //--------^---------------------------^
}
/*
int main()
{
    auto a = forward_args({1,2,3,4,5,6,7,8,9,0});
    auto b = a;
    auto c = forward_args({0,1,2,3,4,5,6,7,8,9});
    b = c;
    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;
}
*/