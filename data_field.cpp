/*/../bin/ls > /dev/null
 filename=$(basename $BASH_SOURCE)
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/bin > /dev/null
    filename="${dirname}/bin/${filename}"
 else
    filename="./${filename}"
 fi
 filename=${filename%.*}
 if [ $0 -nt ${filename} ]
 then
	c++ -o "${filename}" -std=c++1y $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <iomanip>
#include <string>
#include <cassert>
#include <sstream>
#include <stdexcept>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/variant.hpp>
#include <boost/optional.hpp>

/// The type in which we store dates
using date = boost::gregorian::date;

/// The types in which we can store fields. use of boost::variant means we can write static visitors and eliminate bugs.
using field_value = boost::variant<std::string, date>;

/// Invalid field instruction conde syntax
struct invalid_field_instruction_code : std::logic_error {
    using std::logic_error::logic_error;
};

/// wrong date or date format (we could split this into a logic an runtime error depending on why the error occurred)
struct invalid_date : std::runtime_error
{
    using std::runtime_error::runtime_error;
};

/// A thing that generates instructions and parses responses to those instructions
struct field_instruction_concept
{
    virtual std::string generate() const = 0;
    virtual field_value parse(const std::string& buffer) const = 0;
    virtual ~field_instruction_concept() = default;
};

/// a model that generates and parses dates entered in an appropriate format (specified in the constructor)
struct date_instruction : field_instruction_concept
{
    date_instruction(const std::string& ddmmyy) : ddmmyy(ddmmyy) {}
    std::string generate() const override {
        using namespace std::string_literals;
        return "{{Datefield=>"s + ddmmyy + "}}"s;
    }
    field_value parse(const std::string& buffer) const override {
        using namespace std::string_literals;
        int day = -1;
        int month = -1;
        int year = -1;
        size_t curpos = 0;
        for ( ; curpos != std::string::npos && curpos != ddmmyy.size() ; )
        {
            auto next = ddmmyy.find_first_not_of(ddmmyy[curpos], curpos);
            auto part = ddmmyy.substr(curpos, next - curpos);
            auto bit = buffer.substr(curpos, next - curpos);
            if (bit.length() != part.length()) {
                throw invalid_date("bit lengths dont match");
            }
            if (part == "dd") {
                day = atoi(bit.c_str());
            }
            else if (part == "MM") {
                month = atoi(bit.c_str());
            }
            else if (part == "yyyy") {
                year = atoi(bit.c_str());
            }
            else if (part == "/") {
                goto loop;
            }
            else {
                throw invalid_date("shitfest:"s + ddmmyy + ":" + buffer);
            }
        loop:
            curpos = next;
        }
        if (day < 1 || month < 1 || year < 0 || day > 31 || month > 12) {
            throw invalid_date("bit missing");
        }
        return { date(year, month, day) };
    }
private:
    const std::string ddmmyy;
};

/// A model that generates textfield instructions and parses their responses
struct textfield_instruction : field_instruction_concept
{
    textfield_instruction() {}
    std::string generate() const override {
        using namespace std::string_literals;
        return "{{Textfield}}"s;
    }
    field_value parse(const std::string& buffer) const override {
        return { buffer };
    }

private:
    const std::string ddmmyy;
};

/// parse an instruction syntax code into an actual instruction object
const field_instruction_concept& make_field_instruction(const std::string instruction_code)
{
    using namespace std::string_literals;

    if (instruction_code == "dateDDMMYYYY") {
        static const date_instruction _("dd/MM/yyyy"s);
        return _;
    }
    
    if (instruction_code == "dateMMDDYYYY") {
        static const date_instruction _("MM/dd/yyyy"s);
        return _;
    }
    
    if (instruction_code == "textfield") {
        static const textfield_instruction _;
        return _;
    }
    
    throw invalid_field_instruction_code("make_field_instruction_code"s + instruction_code);
}

/// encode a field instructions sequence into mangled JSON
std::string encode_data_field_requirement(const std::string& field_name,
                                          const std::string& label,
                                          const field_instruction_concept& instruction)
{
    std::ostringstream ss;
    ss << std::quoted(field_name, '\'');
    ss << ":";
    ss << std::quoted(label + instruction.generate(), '\'');
    return ss.str();
}

/// test it all in a jury-rigged test harness

static int checks = 0;
static int passes = 0;

template<class T>
void check_equal(const T& x, const T& y, int line) {
    ++checks;
    if (x != y) {
        std::cerr << "failure on line: " << line << ", not equal:" << std::endl;
        std::cerr << "expected: " << x << std::endl;
        std::cerr << "got: " << y << std::endl;
    }
    else {
        ++passes;
    }
}

void check_true(bool condition, int line) {
    ++checks;
    if (!condition) {
        std::cerr << "failure on line: " << line << ", not true" << std::endl;
    }
    else {
        ++passes;
    }
}

int main()
{
    using namespace std::string_literals;
    const auto generate1 = encode_data_field_requirement("tpf_relation_dateofbirth",
                                                         "Date of Birth",
                                                         make_field_instruction("dateDDMMYYYY"));
    const auto expected1 = R"JSON('tpf_relation_dateofbirth':'Date of Birth{{Datefield=>dd/MM/yyyy}}')JSON"s;
    
    check_equal(expected1, generate1, __LINE__);
    
    const auto& instruction = make_field_instruction("dateDDMMYYYY");
    boost::optional<field_value> v;
    try {
        ++checks;
        v = instruction.parse("20/07/2015");
        ++passes;
    }
    catch (const std::exception& e) {
        std::cerr << "exception in parse: " << e.what() << std::endl;
    }
    check_true(bool(v), __LINE__);
    const auto& val = v.get();
    check_true(val.type() == typeid(date), __LINE__);
    std::cout << "parsed a date: " << val << std::endl;
    

    std::cout << "checks: " << checks << ", passes: " << passes << std::endl;
    return 0;
}

