//bin/[ 0 -eq 0 && filename=$(basename $BASH_SOURCE)
//bin/[ 0 -eq 0 && filename=${filename%.*}
//bin/[ 0 -eq 0 && if [ $0 -nt ${filename} ]; then c++ -o "${filename}" -x c++ -std=c++1y -stdlib=libc++ $(basename $BASH_SOURCE) || exit; fi
//bin/[ "$?" -eq "0" && (./"${filename}" && exit) || echo $? && exit
//bin/[ 0 -eq 0 && exit

#include <iostream>
#include <sstream>
#include <thread>
#include <chrono>

#include <mutex>
#include <functional>
#include <type_traits>
#include <cassert>

template<class Data, class Mutex = std::mutex>
struct sequenced
{
    using mutex_type = Mutex;
    using lock_type = std::unique_lock<Mutex>;
    using data_type = Data;
    struct read_access_type;
    struct write_access_type;

    struct re_locker {
        
        re_locker(re_locker&& other)
        : _lock_ptr(other._lock_ptr)
        {
            other._lock_ptr = nullptr;
        }
        
        ~re_locker() {
            if (_lock_ptr)
                _lock_ptr->lock();
        }
        
        void reset() {
            _lock_ptr = nullptr;
        }
        
    private:
        lock_type* _lock_ptr;

        friend sequenced;
        re_locker(lock_type& lock)
        : _lock_ptr(&lock)
        {
            _lock_ptr->unlock();
        }
    };

private:
    struct access_type {
        bool owns_lock() const {
            return _lock.owns_lock();
        }
        
        void unlock() {
            _lock.unlock();
        }
        
        lock_type& get_lock() const {
            return _lock;
        }

        re_locker scoped_unlock() {
            return re_locker(this->get_lock());
        }
        
    protected:
        access_type(lock_type lock)
        : _lock(std::move(lock))
        {}

        mutable lock_type _lock;
    };
    
public:
    struct read_access_type : access_type
    {
        const data_type& operator*() {
            return _data;
        }
        
        const data_type* operator->() {
            return &_data;
        }
        
        const data_type& get() {
            return _data;
        }
        
        void wait(std::condition_variable& cv) {
            assert(this->get_lock().owns_lock());
            cv.wait(this->get_lock());
        }
        
        template<class Pred>
        decltype(auto) wait(std::condition_variable& cv, Pred pred)
        {
            assert(this->get_lock().owns_lock());
            return cv.wait(this->get_lock(), std::bind(std::move(pred), std::cref(get())));
        }

        
    private:
        friend sequenced;
        read_access_type(lock_type lock, const data_type& data)
        : access_type(std::move(lock))
        , _data(data)
        {}
        
        const data_type& _data;
    };
    
    struct write_access_type : access_type
    {
        data_type& operator*() {
            return _data;
        }
        
        data_type* operator->() {
            return &_data;
        }

        data_type& get() {
            return _data;
        }
        
        re_locker unlock_notify_one(std::condition_variable& cv) {
            re_locker relock(this->get_lock());
            cv.notify_one();
            return relock;
        }

        re_locker unlock_notify_all(std::condition_variable& cv) {
            re_locker relock(this->get_lock());
            cv.notify_all();
            return relock;
        }
    private:
        friend sequenced;
        write_access_type(lock_type&& lock, data_type& data)
        : access_type(std::move(lock))
        , _data(data)
        {}
        
        data_type& _data;
    };
    
    template<class...Args>
    sequenced(Args&&...args) : _data { std::forward<Args>(args)... } {}
    
    read_access_type read_access() const {
        return read_access_type(lock_type(_mutex), _data);
    }
    
    template<class F>
    decltype(auto) with_read_access(F f) const
    {
        return f(read_access());
    }
    
    write_access_type write_access() {
        return write_access_type(lock_type(_mutex), _data);
    }

    template<class F>
    decltype(auto)
    with_write_access(F f)
    {
        return f(write_access());
    }
    
private:
    mutable mutex_type _mutex;
    data_type _data;
};

using namespace std;

struct ABC {
    int a;
    string b;
    double c;
};
ostream& operator<<(ostream& os, const ABC& abc) {
    os << abc.a << " " << abc.b << " " << abc.c;
    return os;
}

auto main() -> int
{
    using sequenced_abc = sequenced<ABC>;
    
    sequenced_abc abc1(10, "funky", 5.0);
    
    cout << abc1.with_read_access([](sequenced_abc::read_access_type access) -> string {
        std::ostringstream ss;
        ss << to_string(access->a) + access->b + to_string(access->c);
        return ss.str();
    }) << endl;
    
    auto read = abc1.read_access();
    condition_variable says_hello;
    
    auto writer = thread([&]
    {
        auto write = abc1.write_access();
        while (write->a > 0) {
            if (--write->a == 0)
                write->b = "hello";
            cout << this_thread::get_id() << " : " << "setting value to " << write.get() << endl;
            auto relock = write.unlock_notify_one(says_hello);
        }
        
    });
    
    read.wait(says_hello, [](const auto& data) {
        cout << this_thread::get_id() << " : " << "reading value " << data << endl;
        return data.b == "hello";
    });

    read.unlock();
    
    writer.join();
    
	return 0;
}

