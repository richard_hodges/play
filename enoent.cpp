/*/../bin/ls > /dev/null
 filename=$(basename "${BASH_SOURCE}")
 filename=${filename%.*}
 dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
 if [ "${dirname}" == "$(pwd)" ]
 then
    mkdir -p ${dirname}/asm > /dev/null
    mkdir -p ${dirname}/bin > /dev/null
    asmname="${dirname}/asm/${filename}.s"
    filename="${dirname}/bin/${filename}"
 else
    asmname="./${filename}.s"
    filename="./${filename}"
 fi
 if [ $0 -nt ${filename} ]
 then
    flags="-std=c++1z"
	c++ ${flags} -S -o "${asmname}" $BASH_SOURCE || exit
	c++ ${flags} -o "${filename}" $BASH_SOURCE || exit
 fi
 ("${filename}" && exit) || echo $? && exit
 exit
*/

#include <iostream>
#include <cstdlib>
#include <system_error>

int main()
{
    int err = ENOENT;
    
    auto cond = std::system_category().default_error_condition(ENOENT);
    auto err = std::system_error(cond);
    

    if (err != static_cast<int>(std::errc::no_such_file_or_directory)) {
        std::cout << "error" << std::endl;
    }
    
    using namespace std;
	cout << "Hello, World" << endl;
	return 0;
}

